<div class="box grid-box">
    <div class="box-header with-border">
        <h3 class="box-title"> Đại lý còn có công nợ</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover grid-table" id="grid-table611d480fae266">
            <thead>
            <tr>
                <th class="column-id">Mã đại lý</th>
                <th class="column-username">Tên đăng nhập</th>
                <th class="column-name">Tên</th>
                <th class="column-phone">Số điện thoại</th>
                <th class="column-code_count">Số mã đã tạo</th>
                <th class="column-code_activated_count">Số mã đã kích hoạt</th>
                <th class="column-owed">Công nợ</th>
                <th class="column-quantity_code">Số mã được tạo</th>
            </tr>
            </thead>
            <tbody>
            @foreach($managerOwes as $managerOwe)
                @if($managerOwe->owed < 0)
                    <tr data-key="2">
                        <td class="column-id">
                            {{ substr_replace('DL000000', $managerOwe->id, -1, strlen((string) $managerOwe->id)) }}
                        </td>
                        <td class="column-username">
                            {{ $managerOwe->username }}
                        </td>
                        <td class="column-name">
                            {{ $managerOwe->name }}
                        </td>
                        <td class="column-phone">
                            {{ $managerOwe->phone }}
                        </td>
                        <td class="column-code_count">
                            {{ $managerOwe->code_count }}
                        </td>
                        <td class="column-code_activated_count">
                            {{ $managerOwe->code_activated_count }}
                        </td>
                        <td class="column-owed">
                            {{  number_format($managerOwe->owed, 0, null, ',') . '₫' }}
                        </td>
                        <td class="column-quantity_code">
                            {{ $managerOwe->fixed_quantity ? $managerOwe->codeQuantities->sum('quantity_code') : 'Không giới hạn' }}
                        </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>


    <div class="box-footer clearfix">

    </div>
    <!-- /.box-body -->
</div>
