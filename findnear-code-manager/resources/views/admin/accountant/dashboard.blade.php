<div class="box grid-box">
    <div class="box-header with-border">
        <h3 class="box-title"> Thống kê</h3>
    </div>
{{--    <div class="row text-center box-body">--}}
{{--        <form method="get" action="{{ route('.accountant') }}">--}}
{{--            <div class="form-group">--}}
{{--                <label class="col-sm-2 control-label">Lọc theo ngày</label>--}}
{{--                <div class="col-sm-8" style="width: 390px">--}}
{{--                    <div class="input-group input-group-sm">--}}
{{--                        <div class="input-group-addon">--}}
{{--                            <i class="fa fa-calendar"></i>--}}
{{--                        </div>--}}
{{--                        <input type="date"--}}
{{--                               class="form-control"--}}
{{--                               id="date"--}}
{{--                               placeholder="Lọc theo ngày"--}}
{{--                               name="date"--}}
{{--                               value="{{ request()->input("date")}}"--}}
{{--                               autocomplete="off"--}}
{{--                        />--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="btn-group pull-left">--}}
{{--                    <button class="btn btn-info submit btn-sm"><i class="fa fa-search"></i>&nbsp;&nbsp;Tìm kiếm</button>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </form>--}}
{{--    </div>--}}
    <div class="box-body">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="panel panel-primary text-center">
                    <div class="panel-heading">
                        <h3 class="panel-title text-bold">Cần đóng</h3>
                    </div>
                    <div class="panel-body">
                        <h2>{{ number_format($mustPaid, 0, null, ',') . '₫' }}</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="panel panel-success text-center">
                    <div class="panel-heading">
                        <h3 class="panel-title text-bold">Đã đóng</h3>
                    </div>
                    <div class="panel-body">
                        <h2>{{ number_format($paid, 0, null, ',') . '₫' }}</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="panel panel-danger text-center">
                    <div class="panel-heading">
                        <h3 class="panel-title text-bold">Công nợ</h3>
                    </div>
                    <div class="panel-body">
                        <h2>{{ number_format($owed, 0, null, ',') . '₫' }}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
