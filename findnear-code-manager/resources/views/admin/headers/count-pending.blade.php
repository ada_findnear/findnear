<div class="show-code" style="text-align: center; font-size: 16px;">
    @if( isset($active) && isset($inactive))
        <span>Số mã chờ duyệt: </span>
        <span style="padding-right: 10px; padding-left: 5px; color: blue;">{{($active ?? 0) + ($inactive ?? 0)}}</span>
    @endif
</div>
