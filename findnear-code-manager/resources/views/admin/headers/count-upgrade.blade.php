<div class="show-code" style="text-align: center; font-size: 16px;">
    @if(isset($mine) && $mine)
        @if( isset($active) && isset($inactive))
            <span>Số mã đã tạo: </span>
            <span style="padding-right: 10px; padding-left: 5px; color: blue;">{{($active ?? 0) + ($inactive ?? 0)}}</span>
        @endif
        <span style="padding-left: 10px">Số mã đã kích hoạt:</span>
        <span style="padding: 5px; color: red;">{{$active ?? 0}}</span>
        <span style="padding-left: 10px">Số mã đã xoá:</span>
        <span style="padding: 5px; color: red;">{{$deleted ?? 0}}</span>
    @endif
    <span style="padding-left: 10px">Số mã bạn đã nâng cấp:</span>
    <span style="padding: 5px; color: red;">{{$canCreateShop ?? 0}}</span>
</div>
