drop procedure if exists doWhile;
DELIMITER //  
CREATE PROCEDURE doWhile()   
BEGIN
DECLARE i INT DEFAULT 0;
DECLARE j INT DEFAULT 0;
WHILE (i <= 9999) DO 
	set j = 0;
	WHILE (j <= 9) DO
        INSERT INTO users_tmp (code) 
        values (concat(LPAD(i,4,'0'),j,j,j,j));
    	SET j = j+1;
    END WHILE;
    SET i = i+1;
END WHILE;
END;
//  

CALL doWhile(); 