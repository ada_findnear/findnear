<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUpgradeQuantitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('upgrade_quantities', function (Blueprint $table) {
            $table->id();
            $table->integer('admin_user_id');
            $table->integer('quantity')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('admin_users', function (Blueprint $table) {
            $table->dropColumn('allow_create_shop_counter');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('code_quantities');
        Schema::table('admin_users', function (Blueprint $table) {
            $table->integer('allow_create_shop_counter')->default(0);
        });
    }
}
