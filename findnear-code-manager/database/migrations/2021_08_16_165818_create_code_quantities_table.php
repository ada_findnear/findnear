<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCodeQuantitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('code_quantities', function (Blueprint $table) {
            $table->id();
            $table->integer('admin_user_id');
            $table->integer('quantity_code')->default(0);
            $table->integer('price')->default(0);
            $table->timestamps();
        });

        Schema::table('admin_users', function (Blueprint $table) {
            $table->dropColumn('quantity_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('code_quantities');
        Schema::table('admin_users', function (Blueprint $table) {
            $table->integer('quantity_code')->default(0);
            $table->integer('percent')->default(0);
        });
    }
}
