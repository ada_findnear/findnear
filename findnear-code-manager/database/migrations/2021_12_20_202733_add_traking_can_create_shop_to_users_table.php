<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTrakingCanCreateShopToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->unsignedInteger('user_allow_create_shop_id')->nullable()->after('can_create_shop');
            $table->timestamp('allow_create_shop_at')->nullable()->after('user_allow_create_shop_id');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('user_allow_create_shop_id')->references('id')->on('admin_users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->dropColumn('user_allow_create_shop_id');
            $table->dropColumn('allow_create_shop_at');
        });
    }
}
