<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnPhoneToAdminUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_users', function (Blueprint $table) {
            $table->string('phone')->nullable();
            $table->boolean('fixed_quantity')->default(0);
            $table->bigInteger('quantity_code')->default(0);
            $table->string('password_default')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin_users', function (Blueprint $table) {
            $table->dropColumn('phone');
            $table->dropColumn('fixed_quantity');
            $table->dropColumn('quantity_code');
            $table->dropColumn('password_default');
        });
    }
}
