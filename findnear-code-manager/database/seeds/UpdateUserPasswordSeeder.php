<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UpdateUserPasswordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $managers = User::whereRaw('password = password_default')->get();
        foreach($managers as $k =>$v){
            $passwordDefault = randomPassword();
            $v->owner_id = 1;
            $v->password_default = $passwordDefault;
            $v->password = Hash::make($passwordDefault);
            $v->save();
        }
    }
}
