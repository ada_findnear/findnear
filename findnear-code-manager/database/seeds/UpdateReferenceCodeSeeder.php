<?php

use App\Models\Manager;
use Illuminate\Database\Seeder;

class UpdateReferenceCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $managers = Manager::all();
        foreach($managers as $k =>$v){
            $v->reference_code = randomRefCode();
            $v->save();
        }
    }
}
