<?php

namespace App\Imports;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class ImportCodeNice implements ToModel, WithChunkReading, ShouldQueue
{
    public function model(array $row)
    {
        $codes = [];
        foreach ($row as $column) {
            $code = $column;
            $validate = Validator::make(
                ['code' => $code],
                ['code' => 'required|size:8|unique:users,code']
            );

            if (!$validate->fails()) {
                $passwordDefault = randomPassword();
                $codes[] = [
                    'code' => $code,
                    'owner_id' => ADMIN_ID,
                    'password_default' => $passwordDefault,
                    'password' => Hash::make($passwordDefault),
                    'is_nice' => 1,
                    'created_at' => gmdate('Y-m-d H:i:s')
                ];
            }
        }
//        Log::info(implode(",", array_column($codes, 'code')));
        foreach (array_chunk($codes,1000) as $code)  
        {
            User::insert($code);
        }
    }

    public function chunkSize(): int
    {
        return 100;
    }
}
