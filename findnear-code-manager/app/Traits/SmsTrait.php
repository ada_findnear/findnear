<?php

namespace App\Traits;

trait SmsTrait{
    public function sendRegisterInfo($id, $phone_number, $code, $password){
        $ch = \curl_init();
        $content = "FindNear xin thong bao ma dang ky: $code, mat khau $password. Vui long tai app FindNear tren CH Play va App Store de su dung dich vu. FindNear xin chan thanh cam on. Tham khao: https://findnear.vn";
        $post = [
            'u' => config('services.sms.u'),
            'pwd' => config('services.sms.pwd'),
            'from' =>  config('services.sms.from'),
            'sms' => $content,
            'json' => 1,
            'bid' => $id,
            'phone' => $phone_number,
          ];
        \curl_setopt($ch, CURLOPT_URL, config('services.sms.url'));
        \curl_setopt($ch, CURLOPT_POST, 1);
        \curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        \curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $response = \curl_exec($ch);
        
        $error    = \curl_error($ch);
        $errno    = \curl_errno($ch);

        \curl_close ($ch);
        if (0 !== $errno) {
            throw new \RuntimeException($error, $errno);
        }
        return $response;
    }
}