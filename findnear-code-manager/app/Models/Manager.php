<?php

namespace App\Models;

use Encore\Admin\Auth\Database\HasPermissions;
use Encore\Admin\Facades\Admin as FAdmin;
use Encore\Admin\Traits\DefaultDatetimeFormat;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Kalnoy\Nestedset\NodeTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class Manager extends Model
{
    use HasPermissions;
    use DefaultDatetimeFormat;
    use NodeTrait;
    use SoftDeletes;
    use LogsActivity;

    protected $fillable = ['username','path', 'phone', 'fixed_quantity', 
    'password_default', 'password', 'name', 'avatar', 'parent_id', 'percent', 'can_see_password',
    'can_create_shop'];

    protected $appends = [
        'sum_code_child',
        'paid',
        'owed',
        'must_paid',
        'code_count',
        'code_activated_count',
    ];

    protected static $logAttributes = ['percent', 'quantity_code', 'fixed_quantity'];
    protected static $logAttributesToIgnore = [ 'password_default', 'password', 'avatar'];
    protected static $logName = 'Đại lý';
    protected static $logOnlyDirty = true;
    /**
     * Create a new Eloquent model instance.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->setTable(config('admin.database.users_table'));

        parent::__construct($attributes);
    }

    /**
     * Get avatar attribute.
     *
     * @param string $avatar
     *
     * @return string
     */
    public function getAvatarAttribute($avatar)
    {
        if (url()->isValidUrl($avatar)) {
            return $avatar;
        }

        $disk = config('admin.upload.disk');

        if ($avatar && array_key_exists($disk, config('filesystems.disks'))) {
            return Storage::disk(config('admin.upload.disk'))->url($avatar);
        }

        $default = config('admin.default_avatar') ?: '/vendor/laravel-admin/AdminLTE/dist/img/user2-160x160.jpg';

        return admin_asset($default);
    }

    /**
     * A user has and belongs to many roles.
     *
     * @return BelongsToMany
     */
    public function roles(): BelongsToMany
    {
        $pivotTable = config('admin.database.role_users_table');

        $relatedModel = config('admin.database.roles_model');

        return $this->belongsToMany($relatedModel, $pivotTable, 'user_id', 'role_id');
    }

    /**
     * A User has and belongs to many permissions.
     *
     * @return BelongsToMany
     */
    public function permissions(): BelongsToMany
    {
        $pivotTable = config('admin.database.user_permissions_table');

        $relatedModel = config('admin.database.permissions_model');

        return $this->belongsToMany($relatedModel, $pivotTable, 'user_id', 'permission_id');
    }

    public function codes(): HasMany
    {
        return $this->hasMany(User::class, 'owner_id', 'id');
    }
    public function parent(): BelongsTo
    {
        return $this->belongsTo(Manager::class, 'parent_id', 'id');
    }

    public function codesActivated(): HasMany
    {
        return $this->hasMany(User::class, 'owner_id', 'id')->where('users.active', User::ACTIVATED_STATUS);
    }

    public function codesUpgraded(): HasMany
    {
        return $this->hasMany(User::class, 'user_allow_create_shop_id', 'id');
    }

    public function canCreateShopCount()
    {
        $ids = $this->descendants()->pluck('id');
        $ids[] = $this->getKey();
        return User::whereIn('owner_id', $ids)->where('users.can_create_shop', User::ACTIVATED_STATUS)->count();
    }

    public function codeQuantities(): HasMany
    {
        return $this->hasMany(CodeQuantity::class, 'admin_user_id', 'id');
    }

    public function upgradeQuantities(): HasMany
    {
        return $this->hasMany(UpgradeQuantity::class, 'admin_user_id', 'id');
    }

    public function paymentManagers(): HasMany
    {
        $query = $this->hasMany(PaymentManager::class, 'manager_id', 'id');
        if (Auth::check()) {
            $query->where('owner_id', FAdmin::user()->can(PERMISSION_ACCOUNTANT) ? ADMIN_ID :  Auth::id());
        }
        return $query;
    }

    public function getCodeCountAttribute()
    {
        $ids = $this->descendants()->pluck('id');
        $ids[] = $this->getKey();
        return User::whereIn('owner_id', $ids)->count();
    }

    public function getCodeActivatedCountAttribute()
    {
        $ids = $this->descendants()->pluck('id');
        $ids[] = $this->getKey();
        return User::whereIn('owner_id', $ids)->where('active', User::ACTIVATED_STATUS)->count();
    }

    public function getSumCodeChildAttribute()
    {
        $ids = $this->children()->pluck('id');
        return CodeQuantity::whereIn('admin_user_id', $ids)->sum('quantity_code');
    }

    public function getPaidAttribute()
    {
        return $this->paymentManagers()->sum('amount');
    }

    public function getMustPaidAttribute()
    {
        if (!$this->fixed_quantity) {
            return $this->percent*$this->getCodeActivatedCountAttribute();
        }
        $sumPrice = 0;

        $activeCode = $this->getCodeActivatedCountAttribute();

        $codeQuantities = $this->codeQuantities;


        foreach ($codeQuantities as $codeQuantity) {

            if ($activeCode <= $codeQuantity->quantity_code){
                $sumPrice += $activeCode*$codeQuantity->price;
                break;
            } else {
                $sumPrice += $codeQuantity->quantity_code*$codeQuantity->price;
                $activeCode = $activeCode - $codeQuantity->quantity_code;
            }
        }

        return $sumPrice;
    }

    public function getOwedAttribute()
    {
        return $this->getPaidAttribute() - $this->getMustPaidAttribute();
    }


}
