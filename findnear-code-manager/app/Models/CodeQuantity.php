<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class CodeQuantity extends Model
{
    use SoftDeletes;
    use LogsActivity;

    protected $fillable = ['admin_user_id', 'quantity_code', 'price'];

    protected $hidden = [];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y (H:s)'
    ];

    protected $appends = [
        'sum_must_paid'
    ];

    protected static $logAttributes = ['admin_user_id', 'quantity_code', 'price', 'manager.username'];

    protected static $logName = 'Đại lý';

    protected static $logOnlyDirty = true;

    public function manager()
    {
        return $this->belongsTo(Manager::class, 'admin_user_id', 'id')->withTrashed();
    }

    public function getSumMustPaidAttribute()
    {
        return $this->quantity_code*$this->price;
    }
}
