<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    const ACTIVATED_STATUS = 1;
    const NOT_ACTIVATED_STATUS = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'is_nice', 'active', 'expired_at', 'owner_id', 'password_default', 'password', 'sms_sent', 'sms_content', 'can_create_shop'
        , 'activated_ip', 'login_ip'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token', 'email', 'password', 'api_token', 'name'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $dates = ['expired_at'];

    public function userDetail(): BelongsTo
    {
        return $this->belongsTo(UserDetail::class, 'user_detail_id', 'id');
    }
    public function sms_log(): HasOne
    {
        return $this->hasOne(SmsLog::class, 'user_id', 'id');
    }
    public function owner(): BelongsTo
    {
        return $this->belongsTo(Manager::class, 'owner_id', 'id');
    }
    public function userAllowCreateShop(): BelongsTo
    {
        return $this->belongsTo(Manager::class, 'user_allow_create_shop_id', 'id');
    }
	
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
	
	
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
