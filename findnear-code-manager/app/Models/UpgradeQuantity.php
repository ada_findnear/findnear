<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class UpgradeQuantity extends Model
{
    use SoftDeletes;
    use LogsActivity;

    protected $fillable = ['admin_user_id', 'quantity', 'price'];
    protected $hidden = [];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y (H:s)'
    ];

    protected static $logAttributes = ['admin_user_id', 'quantity', 'manager.username'];

    protected static $logName = 'Đại lý';

    protected static $logOnlyDirty = true;

    public function manager()
    {
        return $this->belongsTo(Manager::class, 'admin_user_id', 'id')->withTrashed();
    }

}
