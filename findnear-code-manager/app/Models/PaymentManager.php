<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentManager extends Model
{
    protected $fillable = ['owner_id', 'desc', 'amount', 'manager_id'];

    protected $casts = [
      'created_at' => 'datetime:d-m-Y (H:s)'
    ];
}
