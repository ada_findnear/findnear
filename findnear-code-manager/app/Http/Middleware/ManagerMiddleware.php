<?php

namespace App\Http\Middleware;

use App\Models\Manager;
use Closure;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Facades\Admin as FAdmin;
use Encore\Admin\Middleware\Pjax;
use Illuminate\Support\Facades\Auth;

class ManagerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $manager = $request->route()->parameter('manager');

        if($manager) {
            $parent = Manager::findOrFail(FAdmin::user()->can(PERMISSION_ACCOUNTANT) ? ADMIN_ID : Auth::id());
            $children = Manager::findOrFail($manager);
            if (!$children->isDescendantOf($parent)) {
                $response = response(Admin::content()->withError(trans('admin.deny')));
                Pjax::respond($response);
            }
        }
        return $next($request);
    }
}
