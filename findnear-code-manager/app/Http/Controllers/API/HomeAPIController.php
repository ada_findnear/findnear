<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Manager;
use App\Models\User;
use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class HomeAPIController extends Controller
{
    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return
     */
    function log(Request $request)
    {
        try {
            Log::channel('zalo')->debug(date('d M Y H:i:s')."\r\n" .print_r($request, true));
        } catch (Exception $e) {
            return $this->sendError($e->getMessage(), 401);
        }
    }
}
