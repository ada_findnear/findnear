<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Manager;
use App\Models\SmsLog;
use App\Models\User;
use App\Models\UserDetail;
use App\Traits\SmsTrait;
use Exception;
use Illuminate\Http\Request;

class CodeAPIController extends Controller
{
    use SmsTrait;
    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return
     */
    function register(Request $request)
    {
        try {
            // return $this->sendError('Bạn không thể đăng ký, vui lòng liên hệ bộ phận hỗ trợ!', 2, 401);
            $this->validate($request, [
                'phone' => 'required|string|min:9|max:12',
                'dealer_code' => 'required'
            ]);
            $user = null;
            $manager = Manager::where('reference_code',$request->get('dealer_code',''))->first();
            if(!$manager){
                return $this->sendError('Đại lý không tồn tại!', 3, 401);
            }
            $detail = UserDetail::where('phone', $request->get('phone'))->first();
            if($detail){
                $phone = $detail->phone;
                if(SmsLog::where('phone', $phone)->whereRaw('DATE(updated_at) = DATE(NOW())')->count() > 0){
                    return $this->sendError('Bạn đã gửi yêu cầu hôm nay, vui lòng thử lại sau!', 2, 401);
                }
                $user = $detail->user;
                // return $this->sendError('Số điện thoại đã được sử dụng, vui lòng liên hệ bộ phận chăm sóc khách hàng!', 2, 401);
            }
            if(!$user){
                $user = User::where('owner_id', $manager->id)
                ->where('active' , 0)
                ->whereNull('user_detail_id')->first();
            }
            if($user){
                //sending to client
                if (!$detail) {
                    $userDetail = new UserDetail();
                    $userDetail->phone = $request->get('phone');
                    $userDetail->save();
                    $user->user_detail_id = $userDetail->id;
                    $user->save();
                }
                $response = $this->sendRegisterInfo($user->id, $request->get('phone'), $user->code, $user->password_default);
                $user->sms_log()->updateOrCreate(
                    ['phone' => $request->get('phone')],
                    [ 'sms_response' => $response]
                );
                return $this->sendResponse($user, 'User retrieved successfully');
            }
            else{
                return $this->sendError('Không còn tài khoản khả dụng, vui lòng liên hệ đại lý!', 1, 401);
            }
        } catch (Exception $e) {
            return $this->sendError($e->getMessage(), 401);
        }
    }
}
