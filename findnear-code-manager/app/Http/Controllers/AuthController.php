<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;


use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    use \Illuminate\Foundation\Auth\AuthenticatesUsers;
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.verify', ['except' => ['login', 'resetPassword']]);
    }
    
    public function username()
    {
        return 'code';
    }


    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {

        $credentials = request(['code', 'password']);

        if (!$token = JWTAuth::customClaims(['usercode' => request('code')])->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized!'], 401);
        }
        $user = Auth::user();
        $user->login_ip = request('ip','unknown');
        $user->save();
        return $this->respondWithToken($token);
    }


    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user = auth()->user();
        $user->can_create_shop = $user->can_create_shop && !$user->can_create_shop;
        return response()->json();
    }
 /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(Request $request)
    {
        $user = auth()->user();
        $password = $request->get('password','');
        $auth_code = $request->get('auth_code','');
        if(!$user || $password == ''){
            return response()->json(['error' => 'Request is not valid'], 400);
        }
        if($user->active == 0 && $auth_code == ''){
            return response()->json(['error' => 'Auth code is not valid'], 400);
        }
        if($user->active == 1 && $auth_code != $user->auth_code){
            return response()->json(['error' => 'Auth code is not match'], 400);
        }
        $user->password = Hash::make($password);
        if(!$user->active && $auth_code != ''){
            $user->auth_code = $auth_code;
            $user->active = true;
            $user->activated_at = gmdate('Y-m-d H:i:s');
            $user->activated_ip = $request->get('ip','unknown');
            $user->name = $request->get('name', '');
            $user->expired_at = strtotime('+1 year');
        }
        $user->save();
        return response()->json(['message' => 'Password has been changed!', 'data' => $user]);
    }

    public function resetPassword(Request $request){
        $password = $request->get('password','');
        $auth_code = $request->get('auth_code','');
        $validator = Validator::make($request->all(), [
            'auth_code' => 'required|min:6|max:6',
            'code' => 'required|min:8|max:8',
            'password' => 'required|confirmed|min:6',
        ]);
        if($validator->fails()){
            return response()->json(['error' => 'Request is not valid'], 400);
        }
        $user = User::where('code', $request->get('code',''))->where('auth_code', $auth_code)->first();
        if(!$user || $user->active === false){
            return response()->json(['error' => 'Account is not valid'], 400);
        }
        $user->password = Hash::make($password);
        $user->save();
        return response()->json(['message' => 'Password is reset', 'data' => $user]);
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();


        return response()->json(['message' => 'Successfully logged out']);
    }


    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }


    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
	
	
}
