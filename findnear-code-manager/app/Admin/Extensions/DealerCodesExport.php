<?php

namespace App\Admin\Extensions;

use Carbon\Carbon;
use Encore\Admin\Grid\Exporters\ExcelExporter;
use Maatwebsite\Excel\Concerns\WithMapping;

class DealerCodesExport extends ExcelExporter implements WithMapping
{
    protected $fileName = 'code.xlsx';

    protected $headings = ['ID', 'Code', 'Đại lý','Ngày tạo ID', 'Đại lý nâng cấp', 'Ngày nâng cấp'];

    public function map($row): array
    {
       $code = 'DL000000';
       return [
           $row->id,
           $row->code,
           substr_replace($code, $row->owner_id, -1, strlen($row->owner_id)),
           empty($row->activated_at)? '': Carbon::createFromFormat('U', strtotime($row->activated_at))->addHours(7)->format('d/m/Y (H:i)'),//,
           empty($row->user_allow_create_shop_id)? '': substr_replace($code, $row->user_allow_create_shop_id, -1, strlen($row->user_allow_create_shop_id)),
           empty($row->user_allow_create_shop_id)? '': Carbon::createFromFormat('U', strtotime($row->allow_create_shop_at))->addHours(7)->format('d/m/Y (H:i)'),
       ];
    }
}
