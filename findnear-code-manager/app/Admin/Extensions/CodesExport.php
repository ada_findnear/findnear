<?php

namespace App\Admin\Extensions;

use Encore\Admin\Grid\Exporters\ExcelExporter;
use Maatwebsite\Excel\Concerns\WithMapping;

class CodesExport extends ExcelExporter implements WithMapping
{
    protected $fileName = 'code.xlsx';

    protected $headings = ['ID', 'Code', 'Mật khẩu mặc định', 'Số điện thoại', 'Họ tên', 'Ngành nghề DV'];

    public function map($row): array
    {
       return [
           $row->id,
           $row->code,
           $row->password_default,
           $row->userDetail->phone ?? '',
           $row->userDetail->name ?? '',
           $row->userDetail->job ?? '',
           $row->expired_at,
       ];
    }
}
