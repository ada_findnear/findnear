<?php

namespace App\Admin\Controllers;

use App\Admin\Actions\CreateCodeAction;
use App\Admin\Actions\FilterCodeAction;
use App\Admin\Actions\ImportCodeAction;
use App\Admin\Actions\Post\AccountRenewal;
use App\Admin\Actions\Post\AddDealerInfo;
use App\Admin\Actions\Post\AddInfo;
use App\Admin\Actions\Post\AllowCreateShop;
use App\Admin\Actions\Post\Delete;
use App\Admin\Actions\Post\DenyCreateShop;
use App\Admin\Actions\Post\Recover;
use App\Admin\Actions\Post\ResetPassword;
use App\Admin\Actions\UpdateDealerAction;
use App\Admin\Extensions\DealerCodesExport;
use App\Models\Manager;
use App\Models\User;
use Encore\Admin\Actions\Action;
use Encore\Admin\Admin;
use Encore\Admin\Auth\Permission;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin as FAdmin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Encore\Admin\Widgets\Box;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class DealerCodeController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Quản lý mã';


    /**
     * Get content title.
     *
     * @return string
     */
    protected function title()
    {
        if (FAdmin::user()->isRole('administrator')){
            return 'Quản lý mã';
        }
        return $this->title;
    }
    /**
     * Make a grid builder.
     *
     * @param bool $nice
     * @return Grid
     */
    protected function grid(bool $pending = false, bool $mine = false): Grid
    {
        Admin::style('td.column-active i {font-size: 22px; color: grey; padding-left: 16px;}');
        Admin::style('td.column-active i.fa-toggle-on {font-size: 22px; color: green;}');

        $grid = new Grid(new User());
        // $grid->disableActions();
        $grid->disableCreateButton();

        $grid->header(function ($query) use ($mine){
            $active = (clone $query)->where('active', 1)->count();
            $inactive = (clone $query)->where('active', 0)->count();
            $deleted = (clone $query)->whereNotNull('deleted_at')
                ->count();
            $canCreateShop = (clone $query)->where('can_create_shop', 1)->where('user_allow_create_shop_id',FAdmin::user()->id)
                ->count();

            if (FAdmin::user()->isAdministrator()) {
                return view('admin.headers.count-pending', compact('active'));
            }
            return view('admin.headers.count-upgrade', compact('mine', 'active','inactive', 'deleted', 'canCreateShop'));
        });
        
        if (FAdmin::user()->isAdministrator() && $pending) {
            $grid->model()->where('waiting_for_admin_approve', true);
        }
        $grid->tools(function (Grid\Tools $tools){
            $tools->append(new CreateCodeAction());
        });
        if ($mine) {
            $grid->model()->where('owner_id', Auth::id());
        }
        $grid->filter(function ($filter) {
            // Remove the default id filter
            $filter->disableIdFilter();
            $filter->where(function ($query) {
                return $query->where('code', 'like', "%{$this->input}%");
            }, 'Tìm theo một phần mã');
            $filter->between('code', 'Tìm theo mã');
            $filter->between('owner_id', 'Tìm theo mã đại lý');
            $filter->like('userDetail.phone', 'Số điện thoại');
            $filter->equal('active', 'Trạng thái')->radio([
                0    => 'Chưa kích hoạt',
                1    => 'Đã kích hoạt',
            ]);
            $filter->between('activated_at', 'Thời gian kích hoạt của mã')->datetime();
            $filter->equal('can_create_shop', __('admin.can_create_shop'))->radio([
                0    => 'Chưa kích hoạt',
                1    => 'Đã kích hoạt',
            ]);
            $filter->between('allow_create_shop_at', 'Thời gian cho phép tạo shop')->date();
            $filter->between('user_allow_create_shop_id', 'Đại lý cho phép tạo shop');

            $filter->like('userDetail.name', 'Họ và tên');
            $filter->between('expired_at', 'Thời gian hết hạn của mã')->datetime();
        });
        $grid->model()->orderBy('active')
            ->orderBy('created_at', 'DESC');


        $grid = $this->actionGrid($grid);

        $grid->rows(function (Grid\Row $row) {
            $row->column('number', $row->number + 1);
            $owner_id = $row->user_allow_create_shop_id;
            $upgraded_dealer = '';

            if(!empty($owner_id)){
                $code = 'DL000000';
                $id = $owner_id . "";
                $owner_id = substr_replace($code, $id, -1, strlen($id));
                $class = $row->waiting_for_admin_approve ? ' - text-danger':'text-success';
                $upgraded_dealer = "<span class='$class'>" . $owner_id . ($row->waiting_for_admin_approve ? ' - Pending':''). "</span>";
            }

            $row->column('upgraded_dealer', $upgraded_dealer);
            $row->column('userAllowCreateShop', $row->number + 1);
            $row->column('expired_at', $row->expired_at == "" ? 'Chưa đăng nhập' : formatDateTime($row->expired_at));
            $row->column('activated_at', $row->activated_at == "" ? '' : formatDateTime($row->activated_at, 14));
            $row->column('created_at', $row->created_at == "" ? '' : $this->formatCreatedDateTime($row->created_at));
            $row->column('allow_create_shop_at', $row->allow_create_shop_at == "" ? '' : formatDateTime($row->allow_create_shop_at));
        });

        $grid->column('number', __('admin.user.stt'));
        $grid->column('code', __('admin.code'))->display(function ($code) {
            if ($this->owner_id === Auth::id()) {
                return $code;
            }
            return "<span class='text-danger'>" . $code . "</span>";
        })->copyable();
        $grid->column('owner_id', __('admin.dealer'))->display(function ($owner_id) {
            $code = 'DL000000';
            $id = $owner_id . "";
            $owner_id = substr_replace($code, $id, -1, strlen($id));
            return "<span class='text-danger'>" . $owner_id . "</span>";
        })->copyable();
        // $grid->column('can_create_shop', __('admin.can_create_shop'))
        //     ->icon([
        //         0 => 'toggle-off',
        //         1 => 'toggle-on',
        //     ], $default = '')->sortable();

            if(ADMIN_ID  ==  Auth::id() || FAdmin::user()->can_see_password){
                $grid->column('password_default', __('admin.password_default'))->display(function ($password_default) {
                    if (Hash::check($password_default, $this->password)) {
                        return $password_default;
                    } else {
                        return __('admin.change_password');
                    }
                })->copyable();
            }
            $grid->column('userDetail.phone', __('admin.phone'));
            $grid->column('userDetail.name', __('admin.full_name'));
            $grid->column('userDetail.job', __('admin.job'));
            $grid->column('created_at', __('admin.created_at'))->sortable();
            $grid->column('active', __('admin.active'))
                ->icon([
                    0 => 'toggle-off',
                    1 => 'toggle-on',
                ], $default = '')->sortable();
            $grid->column('activated_at', __('admin.activated_at'))->sortable();
            $grid->column('can_create_shop', __('admin.can_create_shop'))
                ->icon([
                    0 => 'toggle-off',
                    1 => 'toggle-on',
                ], $default = '')->sortable();
            $grid->column('allow_create_shop_at', __('admin.allow_create_shop_at'))->sortable();
            $grid->column('upgraded_dealer', __('admin.allow_create_shop_dealer'))->copyable()->sortable();
            $grid->column('expired_at', __('admin.expired_at'))->sortable();
    
        $grid->exporter(new DealerCodesExport());

        if(count(request()->input()) == 0 && FAdmin::user()->can_see_password){
            $grid->model()->where('id','<',0);
        }
        return $grid;
    }

    private function formatCreatedDateTime($date): string
    {
        return Carbon::createFromFormat('U', strtotime($date))->addHours(14)->format('d/m/Y (H:i)');
    }

    private function actionGrid(Grid $grid): Grid
    {
        $grid->actions(function ($actions) {
            $actions->add(new AddDealerInfo());
            $row = $actions->row;
            $actions->disableDelete();
            $actions->disableEdit();
            $actions->disableView();
            // $actions->add(new AddInfo());
            if (FAdmin::user()->isAdministrator() || (FAdmin::user()->can_create_shop) && empty($row->user_allow_create_shop_id)) {
                $actions->add(new AllowCreateShop());
            }
        });

        $grid->disableBatchActions();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show|Box
     */
    protected function detail($id)
    {
        $user = User::findOrFail($id);
        $userDetail = $user->userDetail;
        if ($userDetail) {
            $show = new Show($userDetail);

            $show->panel()
                ->tools(function ($tools) {
                    $tools->disableEdit();
                    $tools->disableDelete();
                });

            $show->field('name', __('admin.detail_user.name'));
            $show->field('phone', __('admin.detail_user.phone'));
            $show->field('email', __('admin.detail_user.email'));
            $show->field('job', __('admin.detail_user.job'));
            $show->field('address', __('admin.detail_user.address'));
            $show->field('auth_code', __('admin.auth_code'));
            $show->divider();
            $show->field('user', __('admin.detail_user.code'))->as(function ($user) {
                return $user->auth_code ?? __('admin.detail_user.auth_code_null');
            });
            return $show;
        }

        return new Box('Thông tin khách hàng', 'Chưa cập nhật thông tin khách hàng! Vui lòng cập nhật thông tin khách hàng tại màn hình danh sách khách hàng');
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new User);

        $form->tools(function (Form\Tools $tools) {
            // Disable `Delete` btn.
            $tools->disableDelete();
        });

        $form->footer(function ($footer) {
            // disable `View` checkbox
            $footer->disableViewCheck();

            // disable `Continue editing` checkbox
            $footer->disableEditingCheck();

            // disable `Continue Creating` checkbox
            $footer->disableCreatingCheck();
        });

        if ($form->isEditing()) {
            $id = request()->route()->parameter('dealer_code');
            $model = $form->model()->find($id);
            $userDetail = $model->userDetail;
            if ($userDetail) {
                $form->text('userDetail.name', __('admin.detail_user.name'))->default($userDetail->name);
                $form->text('userDetail.phone', __('admin.detail_user.phone'))->default($userDetail->phone);
                $form->email('userDetail.email', __('admin.detail_user.email'))->default($userDetail->email);
                $form->text('userDetail.job', __('admin.detail_user.job'))->default($userDetail->job);
                $form->text('userDetail.address', __('admin.detail_user.address'))->default($userDetail->address);
            } else {
                $form->text('userDetail.name', __('admin.detail_user.name'));
                $form->text('userDetail.phone', __('admin.detail_user.phone'));
                $form->email('userDetail.email', __('admin.detail_user.email'));
                $form->text('userDetail.job', __('admin.detail_user.job'));
                $form->text('userDetail.address', __('admin.detail_user.address'));
            }

            if (FAdmin::user()->isRole('administrator') || FAdmin::user()->can(PERMISSION_CUSTOMER_SERVICE)) {
                $states = [
                    'on' => ['value' => 1, 'text' => 'mở', 'color' => 'success'],
                    'off' => ['value' => 0, 'text' => 'khoá', 'color' => 'danger'],
                ];
                $form->text('auth_code', __('admin.detail_user.code'))->default($model->auth_code);
                $form->switch('active', trans('admin.user.active'))->states($states);
            }
        }
        return $form;
    }

    /**
     * @param Content $content
     * @return Content
     */
    public function pending(Content $content): Content
    {
        return $content
            ->title($this->title())
            ->description(__('admin.pending_code'))
            ->body($this->grid(true));
    }
    /**
     * @param Content $content
     * @return Content
     */
    public function mine(Content $content): Content
    {
        return $content
            ->title($this->title())
            ->description(__('admin.pending_code'))
            ->body($this->grid(false,true));
    }
}
