<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Manager;
use Carbon\Carbon;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccountantController extends Controller
{
    public function index(Content $content, Request $request)
    {
        $date = $request->get('date');
        return $content
            ->title('Dashboard')
            ->description('...')
            ->row($this->chart($date))
            ->row($this->manager());
    }

    private function chart($date)
    {
        $manager = Manager::where('parent_id', ADMIN_ID)->get();

//        if ($date) {
//            $date = Carbon::parse($date)->format('Y-m-d');
//            $manager = $manager->whereHas('paymentManagers', function ($query) use ($date) {
//                $query->whereDate('created_at', $date);
//            });
//        }

//        $manager = $manager->get();
        $mustPaid = $manager->sum('must_paid');
        $paid = $manager->sum('paid');
        $owed = $manager->sum('owed');

        return view('admin.accountant.dashboard', compact('mustPaid', 'paid', 'owed'));
    }

    private function manager()
    {
        $managerOwes = Manager::where('parent_id', ADMIN_ID)->get();
        return view('admin.accountant.manager', compact('managerOwes'));
    }
}
