<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Support\Carbon;
use Spatie\Activitylog\Models\Activity;

class ActivityController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Danh sách hoạt động';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Activity());

        $grid->model()->with('subject')
            ->orderBy('created_at', 'DESC');

        $grid->disableActions();

        $grid->disableCreateButton();

        $grid->disableRowSelector();

        $grid->disableColumnSelector();

        $grid->disableExport();

        $grid->rows(function (Grid\Row $row) {
            $row->column('created_at', $this->formatDateTime($row->created_at));
        });

        $grid->column('id', __('STT'));
        $grid->column('log_name', __('Loại'))->label('default');
        $grid->column('description', __('Trạng thái'))->display(function ($description) {
            $map = [
                'updated' => 'Cập nhật',
                'created' => 'Tạo mới',
                'deleted' => 'Xoá',
            ];
            return $map[$description];
        })->label([
            'updated' => 'info',
            'created' => 'success',
            'deleted' => 'danger',
        ]);
        $grid->column('subject_id', __('Tên đối tượng'))->display(function () {
            switch ($this->subject_type) {
                case 'App\Models\Manager':
                    return $this->subject->username;
                case 'App\Models\CodeQuantity':
                    return $this->subject->manager->username ?? '';
                default:
                    return "";
            }
        });
        $grid->column('causer_id', __('Người thay đổi'))->display(function () {
            return $this->causer->username;
        });
        $activityController = $this;
        $grid->column('properties', __('Thông tin thay đổi'))->display(function () use ($activityController) {
            $attributes = $this->getExtraProperty('attributes');
            $causer = $this->causer;
            $subject = $this->subject;
            $old = $this->getExtraProperty('old');
            return $activityController->getProperties($this->description, $this->subject_type, $causer, $subject, $attributes, $old);
        });
        $grid->column('created_at', __('Thời gian'));

        $grid->actions(function ($actions) {
            $actions->disableDelete();
            $actions->disableEdit();
        });

        return $grid;
    }

    public function getProperties($description, $subjectType, $causer, $subject, $attributes, $old)
    {
        switch ($description) {
            case "updated":
            {
                switch ($subjectType) {
                    case 'App\Models\Manager':
                        return "$causer->name đã thay đổi thông tin của $subject->name";
                    default:

                }
                break;
            }
            case "created":
            {
                switch ($subjectType) {
                    case 'App\Models\Manager':
                        return "$causer->name đã tạo mới tài khoản $subject->name";
                    case 'App\Models\CodeQuantity':
                        return "$causer->name đã cấp mới cho " . $subject->manager->name . " " . number_format(@$attributes['quantity_code']) . ' mã, số tiền mỗi mã: ' . number_format(@$attributes['price'] . '000');
                    case 'App\Models\UpgradeQuantity':
                        return "$causer->name đã cấp mới cho " . $subject->manager->name . " " . $attributes['quantity'] . ' quyền nâng cấp mã';
                    default:

                }
                break;
            }
            case "deleted":
            {
                switch ($subjectType) {
                    case 'App\Models\Manager':
                        return "$causer->name đã xoá tài khoản " . $subject->name;
                    case 'App\Models\CodeQuantity':
                        return "$causer->name đã xoá tài khoản ";
                    default:

                }
                break;
            }
        }
        return '';
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Activity::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('log_name', __('Log name'));
        $show->field('description', __('Description'));
        $show->field('subject_type', __('Subject type'));
        $show->field('subject_id', __('Subject id'));
        $show->field('causer_type', __('Causer type'));
        $show->field('causer_id', __('Causer id'));
        $show->field('properties', __('Properties'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Activity());

        $form->text('log_name', __('Log name'));
        $form->textarea('description', __('Description'));
        $form->text('subject_type', __('Subject type'));
        $form->number('subject_id', __('Subject id'));
        $form->text('causer_type', __('Causer type'));
        $form->number('causer_id', __('Causer id'));
        $form->text('properties', __('Properties'));

        return $form;
    }

    private function formatDateTime($date): string
    {
        return Carbon::createFromFormat('U', strtotime($date))->format('d/m/Y (H:s)');
    }
}
