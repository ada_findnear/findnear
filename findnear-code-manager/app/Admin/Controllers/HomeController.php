<?php

namespace App\Admin\Controllers;

use App\Admin\Forms\GenerateCodeForm;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;

class HomeController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->title('Quản lý')
            ->description('Quản lý cài đặt')
            ->row(function (Row $row) {

            });
    }
}
