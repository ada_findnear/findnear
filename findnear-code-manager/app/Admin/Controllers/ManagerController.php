<?php

namespace App\Admin\Controllers;

use App\Admin\Actions\ChangePriceAction;
use App\Admin\Actions\Manager\AllowManagerCreateShop;
use App\Admin\Actions\Manager\DenyManagerCreateShop;
use App\Admin\Actions\ManagerChildren;
use App\Admin\Actions\Post\ResetPassword;
use App\Models\CodeQuantity;
use App\Models\Manager;
use App\Models\UpgradeQuantity;
use Carbon\Carbon;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Facades\Admin as FAdmin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Middleware\Pjax;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ManagerController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Đại lí cấp dưới';

    const ROLE_MANAGER = 5;

    /**
     * Index interface.
     *
     * @param Content $content
     *
     * @return Content
     */
    public function index(Content $content)
    {
        $managerChildrenId = @$_GET['q'] ?? null;
        if ($managerChildrenId) {
            $parent = Manager::findOrFail(FAdmin::user()->can(PERMISSION_ACCOUNTANT) ? ADMIN_ID : Auth::id());
            $children = Manager::findOrFail($managerChildrenId);
            $parent->fixTree();
            if (!$children->isChildOf($parent) && !FAdmin::user()->can(PERMISSION_ACCOUNTANT)) {
                $response = response(Admin::content()->withError(trans('admin.deny')));
                Pjax::respond($response);
            }
        }

        return $content
            ->title($this->title())
            ->description($this->description['index'] ?? trans('admin.list') . (isset($children) ? 'đại lí cấp dưới của ' . $children->parent->name : ''))
            ->body($this->grid($managerChildrenId));
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid($managerChildrenId = null, $isChart = false)
    {
        $grid = new Grid((new Manager()));

        $authId =FAdmin::user()->can(PERMISSION_ACCOUNTANT) ? ADMIN_ID :  Auth::id();
        $parentId = $managerChildrenId ?? $authId;
        $grid->filter(function ($filter) use ($parentId){

            // Remove the default id filter
            $filter->disableIdFilter();

            // if(ADMIN_ID ==  Auth::id()){
            //     $filter->where(function ($query) use ($parentId) {
            //         if(!$this->input){
            //             return $query->where('parent_id', $parentId);
            //         }
            //         else{
            //             return $query->where('id', $this->input);
            //         }
            //     }, 'Tìm theo mã');
            // }
            $filter->equal('id', 'Tìm theo mã');
            $filter->equal('username', 'Tìm theo tên đăng nhập');
            $filter->equal('reference_code', 'Tìm theo mã giới thiệu');
        });


        if ($managerChildrenId) {
            $grid->disableCreateButton();
        }

        if ($isChart) {
            $grid->model()->withCount('codesActivated')->orderBy('codes_activated_count', 'DESC');
        } else {
            $input = array_filter(request()->all(), function($var , $k){
                return $var != null && !(in_array($k,['_pjax','page','per_page']));
            }, ARRAY_FILTER_USE_BOTH);
            if(count($input) == 0 || $managerChildrenId || ! FAdmin::user()->can(PERMISSION_ACCOUNTANT)){
                $grid->model()->where('parent_id', $parentId);
            }
        }
        // if(FAdmin::user()->can(PERMISSION_ACCOUNTANT)){
        //     $grid->model()->withTrashed();
        // }

        $grid->rows(function (Grid\Row $row) {
            $user = Manager::find($row->getKey());
            $row->column('allow_create_shop_counter', $user->upgradeQuantities()->sum('quantity') - $user->codesUpgraded()->count());
            $row->column('canCreateShopCount', $user->codesUpgraded()->count());
            $username = '<span class="'. ($user->can_create_shop ?'text-success fw-bold':'').'">'.$user->username.'</span>';
            $row->column('username', $username);
            
            $code = 'DL000000';
            $id = $user->id . "";
            $id = '<span class="'. ($user->can_create_shop ?'text-success fw-bold':'').'">'. substr_replace($code, $id, -1, strlen($id)) .'</span>';
            $row->column('id', $id);
        });

        $grid->column('id', __('admin.managers.id'));
        $grid->column('username', __('admin.username'));
        $grid->column('password_default', __('admin.password_default'))->display(function ($password_default) {
            if (Hash::check($password_default, $this->password)) {
                return $password_default;
            } else {
                return __('admin.change_password');
            }
        })->copyable();
        $grid->column('reference_code', __('admin.reference_code'));
        $grid->column('allow_create_shop_counter', __('admin.allow_create_shop_counter'));
        
        $grid->column('name', __('admin.name'));
        $grid->column('phone', __('admin.phone'));

        $grid->column('code_count', __('admin.codes_count'));
        $grid->column('code_activated_count', __('admin.codes_activated_count'));

        $grid->column('quantity_code', __('admin.quantity_code'))->display(function ($quantity_code) {
            if ($this->fixed_quantity) {
                return $this->codeQuantities->sum('quantity_code');;
            }
            return 'Không giới hạn';
        });
        $grid->column('canCreateShopCount', __('admin.can_create_shop_count'));
        // $grid->column('deleted_at', __('admin.deleted_at'))->display(function ($deleted_at) {
        //     if ($deleted_at) {
        //         return Carbon::createFromFormat('U', strtotime($deleted_at))->addHours(7)->format('d/m/Y (H:i)');
        //     }
        //     return '';
        // });

        $grid = $this->__disableGrid($grid);

        return $grid;
    }

    private function __disableGrid(Grid $grid): Grid
    {

        $grid->actions(function ($actions) {
            $actions->disableDelete();
            $actions->add(new ResetPassword());
            $actions->add(new ManagerChildren());
            if (FAdmin::user()->isAdministrator()) {
                $actions->add(new AllowManagerCreateShop());
                $actions->add(new DenyManagerCreateShop());
            }
        });

        $grid->disableExport();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $manager = Manager::with(['paymentManagers'])->withCount(['codes', 'codesActivated', 'codeQuantities'])->findOrFail($id);

        $show = new Show($manager);
        $show->field('id', __('admin.id'));
        $show->field('username', __('admin.username'));
        $show->field('name', __('admin.name'));
        $show->field('reference_code', __('admin.reference_code'));

        $show->divider();

        $show->field('code_count', __('admin.codes_count'));
        $show->field('code_activated_count', __('admin.codes_activated_count'));
        $show->field('quantity_code', __('admin.quantity_code'))->as(function ($quantity_code) use ($manager) {
            if ($manager->fixed_quantity) {
                return $manager->codeQuantities->sum('quantity_code');
            }
            return 'Không giới hạn';
        });

        $show->divider();
        if (!$manager->fixed_quantity) {
            $show->field('percent', __('admin.price'))->as(function ($percent) {
                return number_format($percent, 0, null, ',') . '₫';
            });
        }

        $show->field('paid', __('admin.paid'))->as(function ($paid) {
            return number_format($paid, 0, null, ',') . '₫';
        });
        $show->field('must_paid', __('admin.must_paid'))->as(function ($must_paid) {
            return number_format($must_paid, 0, null, ',') . '₫';
        });
        $show->field('owed', __('admin.owed'))->as(function ($owed) {
            return number_format($owed, 0, null, ',') . '₫';
        });

        $show->codeQuantities(__('admin.quantity_code_provided'), function ($codeQuantities) {
            $codeQuantities->quantity_code(__('admin.quantity_code'));
            $codeQuantities->price(__('admin.price'))->display(function ($percent) {
                return number_format($percent, 0, null, ',') . '₫';
            });
            $codeQuantities->created_at(__('admin.created_at'));
            self::__hideOptionGrid($codeQuantities);
        });

        $show->paymentManagers(__('admin.history_payment'), function ($paymentManagers) {
            $paymentManagers->resource('/payment-managers');
            $paymentManagers->amount(__('admin.amount_pay'))->display(function ($percent) {
                return number_format($percent, 0, null, ',') . '₫';
            });
            $paymentManagers->created_at(__('admin.pay_date'));
            $paymentManagers->disableTools();

            $paymentManagers->disableRowSelector();
            $paymentManagers->disableActions();
            $paymentManagers->disableExport();
        });

        return $show;
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     *
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->title($this->title())
            ->description($this->description['edit'] ?? trans('admin.edit'))
            ->body($this->form($id)->edit($id));
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        $form = new Form(new Manager());

        $userTable = config('admin.database.users_table');
        $connection = config('admin.database.connection');

        $form->text('name', __('admin.name'));

        $form->text('username', trans('admin.username'))
            ->creationRules(['required', "unique:{$connection}.{$userTable}"])
            ->updateRules(['required', "unique:{$connection}.{$userTable},username,{{id}}"]);
        if(Auth::id() == ADMIN_ID){
            $form->text('reference_code', __('admin.reference_code'))
                ->creationRules(['required','max:100', "unique:admin_users"])
                ->updateRules(['required','max:100', "unique:admin_users,reference_code,{{id}}"])
                ->default(function () {
                    return randomPassword();
                });
        }
        else{
            $form->text('reference_code', __('admin.reference_code'))
                ->creationRules(['required','max:100', "unique:admin_users"])
                ->updateRules(['required','max:100', "unique:admin_users,reference_code,{{id}}"])
                ->default(function () {
                    return randomPassword();
                })->readonly();
        }
        $states = [
            'on' => ['value' => 1, 'text' => 'cho phép', 'color' => 'success'],
            'off' => ['value' => 0, 'text' => 'chặn', 'color' => 'danger'],
        ];
        if(Auth::id() == ADMIN_ID){
            $form->switch('can_see_password', trans('admin.can_see_password'))->states($states);
        }
        $form->text('password_default', __('admin.password'))->rules('required')
            ->default(function () {
                return Str::random('6');
            });

        $form->text('phone', __('admin.phone'))->rules('nullable|numeric|digits_between:9,11');

        $form->hidden('parent_id')->default(FAdmin::user()->can(PERMISSION_ACCOUNTANT) ? ADMIN_ID : Auth::id());
        $form->hidden('password');

        if (!$id) {
            $form->hidden('permissions');
        }

        $user = Auth::user();

        if ($user->fixed_quantity) {
            $manager = Manager::withCount(['codes'])->findOrFail(Auth::id());

            $code = $manager->codeQuantities->sum('quantity_code') - ($manager->codes_count + $manager->sum_code_child);

            $form->html(__('admin.code_can_generate') . $code);
            if ($id) {
                $form->radioButton('add_quantity_code', __('admin.add_quantity_code'))->options([
                    1 => __('admin.add_code'),
                    0 => __('admin.no_add_code')
                ])->when(1, function (Form $form) use ($code) {
                    $form->number('codeQuantity.quantity_code', __('admin.quantity_code'))->rules(['required', 'numeric', 'min:1', 'max:' . $code]);
                    $form->currency('codeQuantity.price', __('admin.price'))->symbol('₫')->rules('required');
                })->default(1);
            } else {
                $form->number('codeQuantity.quantity_code', __('admin.quantity_code'))->rules(['required', 'numeric', 'min:1', 'max:' . $code]);
                $form->currency('codeQuantity.price', __('admin.price'))->symbol('₫')->rules('required');
            }
        } else {
            if ($id) {
                $form->radioButton('add_quantity_code', __('admin.add_quantity_code'))->options([
                    1 => __('admin.add_code'),
                    0 => __('admin.no_add_code')
                ])->when(1, function (Form $form) {
                    $form->number('codeQuantity.quantity_code', __('admin.quantity_code'))->rules(['required', 'numeric', 'min:1']);
                    $form->currency('codeQuantity.price', __('admin.price'))->symbol('₫')->rules('required');
                });

            } else {
                $form->number('codeQuantity.quantity_code', __('admin.quantity_code'))->rules(['required', 'numeric', 'min:1']);
                $form->currency('codeQuantity.price', __('admin.price'))->symbol('₫')->rules('required');
            }

        }
        $form->hidden('fixed_quantity')->default(1);

        if ($id) {
            $form->html($this->__gridCodeQuantities($id)->render(), __('admin.quantity_code_provided'));
        }

        if ($id) {
            $form->radioButton('add_quantity_upgrade', __('admin.add_quantity_upgrade'))->options([
                1 => __('admin.add_upgrade'),
                0 => __('admin.no_add_upgrade')
            ])->when(1, function (Form $form) {
                $form->number('upgradeQuantity.quantity', __('admin.quantity_upgrade'))->rules(['required', 'numeric', 'min:1']);
            });
            $form->html($this->__gridUpgradeQuantities($id)->render(), __('admin.quantity_code_provided'));
        } 
        $form->ignore(['password_confirmation', 'add_quantity_code']);

        $form->saving(function (Form $form) {
            if ($form->password_default) {
                $form->password = Hash::make($form->password_default);
            }

            $form->permissions = [self::ROLE_MANAGER];
        });
        $form->saved(function (Form $form) {
            if ($form->fixed_quantity) {
                if ($form->codeQuantity) {
                    $codeQuantity = $form->codeQuantity;
                    $codeQuantity['admin_user_id'] = $form->model()->id;
                    CodeQuantity::create($codeQuantity);
                }
                if ($form->upgradeQuantity) {
                    $upgradeQuantity = $form->upgradeQuantity;
                    $upgradeQuantity['admin_user_id'] = $form->model()->id;
                    UpgradeQuantity::create($upgradeQuantity);
                }
            }
        });

        return $form;
    }

    private function __gridCodeQuantities($idManager)
    {
        $grid = new Grid(new CodeQuantity());
        $grid->model()->where('admin_user_id', $idManager);
        $grid = $this->__hideOptionGrid($grid);

        $grid->column('quantity_code', __('admin.quantity_code'));
        $grid->column('price', __('admin.price'))->as(function ($price) {
            return number_format($price, 0, null, ',') . '₫';
        });
        $grid->column('created_at', __('admin.created_at'));

        return $grid;
    }

    private function __gridUpgradeQuantities($idManager)
    {
        $grid = new Grid(new UpgradeQuantity());
        $grid->model()->where('admin_user_id', $idManager);
        $grid = $this->__hideOptionGrid($grid);

        $grid->column('quantity', __('admin.quantity_upgrade'));
        $grid->column('created_at', __('admin.created_at'));

        return $grid;
    }

    /**
     * @param $grid
     * @return mixed
     */
    private function __hideOptionGrid($grid)
    {
        $grid->disablePagination();

        $grid->disableCreateButton();

        $grid->disableFilter();

        $grid->disableTools();

        $grid->disableRowSelector();

        $grid->disableExport();

        $grid->actions(function ($actions) {
            $actions->disableDelete();
            $actions->disableView();
            $actions->disableEdit();
            $actions->add(new ChangePriceAction());
        });
        // $grid->disableActions();

        return $grid;
    }

    /**
     * charts
     *
     * @param Content $content
     *
     * @return Content
     */
    public function charts(Content $content)
    {
        \Encore\Admin\Admin::style("
        table tbody tr:nth-of-type(1) {background-color: #f18787;}
        table tbody tr:nth-of-type(2) {background-color: #f9f96c;}
        table tbody tr:nth-of-type(3) {background-color: #85f79a;}
        table tbody tr:nth-of-type(4) {background-color: #85a8f7;}
        table tbody tr:nth-of-type(5) {background-color: #85a8f7;}
        table tbody tr:nth-of-type(6) {background-color: #85a8f7;}
        table tbody tr:nth-of-type(7) {background-color: #85a8f7;}
        table tbody tr:nth-of-type(8) {background-color: #85a8f7;}

        ");
        return $content
            ->title(__('admin.charts'))
            ->description('...')
            ->body($this->grid(null, true));
    }
    
    /**
     * charts
     *
     * @param Content $content
     *
     * @return Content
     */
    public function report(Content $content)
    {
        $grid = new Grid((new Manager()));

        $authId =FAdmin::user()->can(PERMISSION_ACCOUNTANT) ? ADMIN_ID :  Auth::id();
        $grid->filter(function ($filter){

            // Remove the default id filter
            $filter->disableIdFilter();

            // if(ADMIN_ID ==  Auth::id()){
            //     $filter->where(function ($query) use ($parentId) {
            //         if(!$this->input){
            //             return $query->where('parent_id', $parentId);
            //         }
            //         else{
            //             return $query->where('id', $this->input);
            //         }
            //     }, 'Tìm theo mã');
            // }
            $filter->equal('id', 'Tìm theo mã');
            $filter->equal('username', 'Tìm theo tên đăng nhập');
        });
        
        $grid->model()->where('can_create_shop', 1);
        $input = array_filter(request()->all(), function($var , $k){
            return $var != null && !(in_array($k,['_pjax','page','per_page']));
        }, ARRAY_FILTER_USE_BOTH);
        if(count($input) == 0 || $authId || ! FAdmin::user()->can(PERMISSION_ACCOUNTANT)){
            $grid->model()->where('parent_id', $authId);
        }
        // if(FAdmin::user()->can(PERMISSION_ACCOUNTANT)){
        //     $grid->model()->withTrashed();
        // }

        $grid->rows(function (Grid\Row $row) {
            $user = Manager::find($row->getKey());
            $row->column('allow_create_shop_counter', $user->upgradeQuantities()->sum('quantity') - $user->codesUpgraded()->count());
            $row->column('canCreateShopCount', $user->codesUpgraded()->count());
        });

        $grid->column('id', __('admin.managers.id'))->display(function ($value) {
            $code = 'DL000000';
            $id = $value . "";
            return substr_replace($code, $id, -1, strlen($id));
        })->sortable();
        $grid->column('username', __('admin.username'))->sortable();
        $grid->column('allow_create_shop_counter', __('admin.allow_create_shop_counter'));
        
        $grid->column('canCreateShopCount', __('admin.can_create_shop_count'));
        // $grid->column('deleted_at', __('admin.deleted_at'))->display(function ($deleted_at) {
        //     if ($deleted_at) {
        //         return Carbon::createFromFormat('U', strtotime($deleted_at))->addHours(7)->format('d/m/Y (H:i)');
        //     }
        //     return '';
        // });

        $grid->disableActions();
        $grid->disableCreateButton();

        $grid->disableRowSelector();
        return $content
            ->title(__('admin.dealer_report'))
            ->description('Báo cáo đại lý')
            ->body($grid);
    }
}
