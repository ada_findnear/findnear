<?php

namespace App\Admin\Controllers;

use App\Admin\Actions\CreateCodeAction;
use App\Admin\Actions\FilterCodeAction;
use App\Admin\Actions\ImportCodeAction;
use App\Admin\Actions\Post\AccountRenewal;
use App\Admin\Actions\Post\AddInfo;
use App\Admin\Actions\Post\AllowCreateShop;
use App\Admin\Actions\Post\Delete;
use App\Admin\Actions\Post\DenyCreateShop;
use App\Admin\Actions\Post\Recover;
use App\Admin\Actions\Post\ResetPassword;
use App\Admin\Actions\UpdateDealerAction;
use App\Admin\Extensions\CodesExport;
use App\Models\User;
use Encore\Admin\Actions\Action;
use Encore\Admin\Admin;
use Encore\Admin\Auth\Permission;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin as FAdmin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Encore\Admin\Widgets\Box;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class CodeController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Mã đã tạo';


    /**
     * Make a grid builder.
     *
     * @param bool $nice
     * @return Grid
     */
    protected function grid(bool $nice = false, bool $filterExcel = false): Grid
    {
        Admin::style('td.column-active i {font-size: 22px; color: grey; padding-left: 16px;}');
        Admin::style('td.column-active i.fa-toggle-on {font-size: 22px; color: green;}');

        $grid = new Grid(new User());

        $grid->header(function ($query) {
            if (!FAdmin::user()->isAdministrator() && !FAdmin::user()->can(PERMISSION_CUSTOMER_SERVICE)) {
                
                $active = (clone $query)->where('active', 1)
                ->where('owner_id', Auth::id())->count();
                $inactive =  (clone $query)->where('active', 0)
                ->where('owner_id', Auth::id())->count();
                $deleted = (clone $query)->whereNotNull('deleted_at')
                    ->where('owner_id', Auth::id())->get()
                    ->count();
                $canCreateShop = (clone $query)->where('can_create_shop', 1)
                ->where('owner_id', Auth::id())->get()
                    ->count();
            } else {
                $active = (clone $query)->where('active', 1)->count();
                $inactive = (clone $query)->where('active', 0)->count();
                $deleted = (clone $query)->whereNotNull('deleted_at')
                    ->count();
                $canCreateShop = (clone $query)->where('can_create_shop', 1)
                    ->count();
            }

            if (!FAdmin::user()->isAdministrator() && (FAdmin::user()->can(PERMISSION_CUSTOMER_SERVICE) || FAdmin::user()->can(PERMISSION_ACCOUNTANT))) {
                return view('admin.headers.count-user', compact('active', 'deleted', 'canCreateShop'));
            }
            return view('admin.headers.count-user', compact('active','inactive', 'deleted', 'canCreateShop'));
        });

        $grid->filter(function ($filter) {
            // Remove the default id filter
            $filter->disableIdFilter();
            $filter->where(function ($query) {
                return $query->where('code', 'like', "%{$this->input}%");
            }, 'Tìm theo một phần mã');
            $filter->between('code', 'Tìm theo mã');
            $filter->between('owner_id', 'Tìm theo mã đại lý');
            $filter->like('userDetail.phone', 'Số điện thoại');
            $filter->equal('active', 'Trạng thái')->radio([
                0    => 'Chưa kích hoạt',
                1    => 'Đã kích hoạt',
            ]);
            $filter->between('activated_at', 'Thời gian kích hoạt của mã')->datetime();
            $filter->equal('can_create_shop', __('admin.can_create_shop'))->radio([
                0    => 'Chưa kích hoạt',
                1    => 'Đã kích hoạt',
            ]);
            $filter->between('allow_create_shop_at', 'Thời gian cho phép tạo shop')->date();
            $filter->between('user_allow_create_shop_id', 'Đại lý cho phép tạo shop');

            $filter->like('userDetail.name', 'Họ và tên');
            $filter->between('expired_at', 'Thời gian hết hạn của mã')->datetime();
        });
        if (!FAdmin::user()->isAdministrator() && !FAdmin::user()->can(PERMISSION_CUSTOMER_SERVICE)) {
            $grid->model()->where('owner_id', Auth::id());
        }
        if (!FAdmin::user()->isAdministrator() && (FAdmin::user()->can(PERMISSION_CUSTOMER_SERVICE) || FAdmin::user()->can(PERMISSION_ACCOUNTANT))) {
            $grid->model()->where('active', 1);
        }
        if ($nice) {
            $grid->model()->where('is_nice', 1);
        }
        if ($filterExcel) {
            $grid->model()->join('filter_codes','users.code','filter_codes.filter_code');
        }
        $grid->model()->orderBy('active')
            ->orderBy('created_at', 'DESC');

        if (!FAdmin::user()->can(PERMISSION_CUSTOMER_SERVICE) || FAdmin::user()->isAdministrator()) {
            $grid->tools(function (Grid\Tools $tools) use ($nice, $filterExcel) {
                if ($nice) {
                    $tools->append(new ImportCodeAction());
                    $tools->append(new FilterCodeAction());
                    $tools->append(new UpdateDealerAction());
                } else {
                    $tools->append(new CreateCodeAction());
                }
            });
        }

        $grid = $this->actionGrid($grid);

        $grid->rows(function (Grid\Row $row) {
            $row->column('number', $row->number + 1);
            $row->column('expired_at', $row->expired_at == "" ? 'Chưa đăng nhập' : formatDateTime($row->expired_at));
            $row->column('activated_at', $row->activated_at == "" ? '' : formatDateTime($row->activated_at, 14));
            $row->column('created_at', $row->created_at == "" ? '' : $this->formatCreatedDateTime($row->created_at));
            $row->column('allow_create_shop_at', $row->allow_create_shop_at == "" ? '' : formatDateTime($row->allow_create_shop_at));
        });

        $grid->column('number', __('admin.user.stt'));
        $grid->column('code', __('admin.code'))->display(function ($code) {
            if ($this->owner_id === Auth::id()) {
                return $code;
            }
            return "<span class='text-danger'>" . $code . "</span>";
        })->copyable();
        $grid->column('owner_id', __('admin.dealer'))->display(function ($owner_id) {
            $code = 'DL000000';
            $id = $owner_id . "";
            $owner_id = substr_replace($code, $id, -1, strlen($id));
            return "<span class='text-danger'>" . $owner_id . "</span>";
        })->copyable();
        if(ADMIN_ID  ==  Auth::id()){
            $grid->column('password_default', __('admin.password_default'))->display(function ($password_default) {
                if (Hash::check($password_default, $this->password)) {
                    return $password_default;
                } else {
                    return __('admin.change_password');
                }
            })->copyable();
        }
        $grid->column('userDetail.phone', __('admin.phone'));
        $grid->column('userDetail.name', __('admin.full_name'));
        $grid->column('userDetail.job', __('admin.job'));
        $grid->column('active', __('admin.active'))
            ->icon([
                0 => 'toggle-off',
                1 => 'toggle-on',
            ], $default = '')->sortable();
        $grid->column('created_at', __('admin.created_at'))->sortable();
        $grid->column('activated_at', __('admin.activated_at'))->sortable();
        $grid->column('activated_ip', __('admin.activated_ip'))->sortable();
        $grid->column('login_ip', __('admin.login_ip'))->sortable();
        $grid->column('can_create_shop', __('admin.can_create_shop'))
            ->icon([
                0 => 'toggle-off',
                1 => 'toggle-on',
            ], $default = '')->sortable();
        $grid->column('allow_create_shop_at', __('admin.allow_create_shop_at'))->sortable();
        $grid->column('user_allow_create_shop_id', __('admin.allow_create_shop_user'))
        ->display(function ($owner_id) {
            if(!$owner_id){
                return '';
            }
            $code = 'DL000000';
            $id = $owner_id . "";
            $owner_id = substr_replace($code, $id, -1, strlen($id));
            return $owner_id;
        })
        ->sortable();
        $grid->column('expired_at', __('admin.expired_at'))->sortable();

        $grid->exporter(new CodesExport());

        return $grid;
    }

    private function formatCreatedDateTime($date): string
    {
        return Carbon::createFromFormat('U', strtotime($date))->addHours(14)->format('d/m/Y (H:i)');
    }

    private function actionGrid(Grid $grid): Grid
    {
        $grid->actions(function ($actions) {
            $row = $actions->row;
            $actions->disableDelete();
            $actions->disableEdit();

            $actions->add(new AddInfo());
            if (FAdmin::user()->isAdministrator() || FAdmin::user()->can_create_shop) {
                $actions->add(new AllowCreateShop());
                if(FAdmin::user()->isAdministrator()){
                    $actions->add(new DenyCreateShop());
                }
            }
            if (FAdmin::user()->isAdministrator() || FAdmin::user()->can(PERMISSION_CUSTOMER_SERVICE)) {
                // $actions->add(new Recover());
                $actions->add(new Delete());
                $actions->add(new ResetPassword());
                $actions->add(new AccountRenewal());
            }
        });

        $grid->disableCreateButton();
        $grid->disableBatchActions();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show|Box
     */
    protected function detail($id)
    {
        $user = User::findOrFail($id);
        $userDetail = $user->userDetail;
        if ($userDetail) {
            $show = new Show($userDetail);

            $show->panel()
                ->tools(function ($tools) {
                    $tools->disableEdit();
                    $tools->disableDelete();
                });

            $show->field('name', __('admin.detail_user.name'));
            $show->field('phone', __('admin.detail_user.phone'));
            $show->field('email', __('admin.detail_user.email'));
            $show->field('job', __('admin.detail_user.job'));
            $show->field('address', __('admin.detail_user.address'));
            $show->field('auth_code', __('admin.auth_code'));
            $show->divider();
            $show->field('user', __('admin.detail_user.code'))->as(function ($user) {
                return $user->auth_code ?? __('admin.detail_user.auth_code_null');
            });
            return $show;
        }

        return new Box('Thông tin khách hàng', 'Chưa cập nhật thông tin khách hàng! Vui lòng cập nhật thông tin khách hàng tại màn hình danh sách khách hàng');
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new User);

        $form->tools(function (Form\Tools $tools) {
            // Disable `Delete` btn.
            $tools->disableDelete();
        });

        $form->footer(function ($footer) {
            // disable `View` checkbox
            $footer->disableViewCheck();

            // disable `Continue editing` checkbox
            $footer->disableEditingCheck();

            // disable `Continue Creating` checkbox
            $footer->disableCreatingCheck();
        });

        if ($form->isEditing()) {
            $id = request()->route()->parameter('code');
            $model = $form->model()->find($id);
            $userDetail = $model->userDetail;
            if ($userDetail) {
                $form->text('userDetail.name', __('admin.detail_user.name'))->default($userDetail->name);
                $form->text('userDetail.phone', __('admin.detail_user.phone'))->default($userDetail->phone);
                $form->email('userDetail.email', __('admin.detail_user.email'))->default($userDetail->email);
                $form->text('userDetail.job', __('admin.detail_user.job'))->default($userDetail->job);
                $form->text('userDetail.address', __('admin.detail_user.address'))->default($userDetail->address);
            } else {
                $form->text('userDetail.name', __('admin.detail_user.name'));
                $form->text('userDetail.phone', __('admin.detail_user.phone'));
                $form->email('userDetail.email', __('admin.detail_user.email'));
                $form->text('userDetail.job', __('admin.detail_user.job'));
                $form->text('userDetail.address', __('admin.detail_user.address'));
            }

            if (FAdmin::user()->isRole('administrator') || FAdmin::user()->can(PERMISSION_CUSTOMER_SERVICE)) {
                $states = [
                    'on' => ['value' => 1, 'text' => 'mở', 'color' => 'success'],
                    'off' => ['value' => 0, 'text' => 'khoá', 'color' => 'danger'],
                ];
                $form->text('auth_code', __('admin.detail_user.code'))->default($model->auth_code);
                $form->switch('active', trans('admin.user.active'))->states($states);
            }
        }
        // if(FAdmin::user()->isAdministrator() || FAdmin::user()->can(PERMISSION_CUSTOMER_SERVICE) || FAdmin::user()->can(PERMISSION_ACCOUNTANT))
        // {
        //     $form->saved(function (Form $form) {
        //         return redirect('/dealer-code');
        //     });
        // }

        return $form;
    }

    /**
     * @param Content $content
     * @return Content
     */
    public function nice(Content $content): Content
    {
        return $content
            ->title($this->title())
            ->description(__('admin.nice_code'))
            ->body($this->grid(true));
    }
    /**
     * @param Content $content
     * @return Content
     */
    public function filteredNice(Content $content): Content
    {
        return $content
            ->title($this->title())
            ->description(__('admin.filtered_nice_code'))
            ->body($this->grid(true, true));
    }
}
