<?php

namespace App\Admin\Controllers;

use App\Models\Manager;
use App\Models\PaymentManager;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Facades\Admin as FAdmin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Middleware\Pjax;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\MessageBag;

class PaymentManagerController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'PaymentManager';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new PaymentManager());

        $grid->column('id', __('Id'));
        $grid->column('owner_id', __('Owner id'));
        $grid->column('manager_id', __('Manager id'));
        $grid->column('desc', __('Desc'));
        $grid->column('amount', __('Amount'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(PaymentManager::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('owner_id', __('Owner id'));
        $show->field('manager_id', __('Manager id'));
        $show->field('desc', __('Desc'));
        $show->field('amount', __('Amount'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Create interface.
     *
     * @param Content $content
     *
     * @return Content
     */
    public function create(Content $content)
    {
        $managerChildrenId = @$_GET['manager_id'] ?? null;
        if ($managerChildrenId) {
            $parent = Manager::findOrFail(FAdmin::user()->can(PERMISSION_ACCOUNTANT) ? ADMIN_ID : Auth::id());
            $children = Manager::findOrFail($managerChildrenId);
            $parent->fixTree();
            if (!$children->isChildOf($parent)) {
                $response = response(Admin::content()->withError(trans('admin.deny')));
                Pjax::respond($response);
            }
        } else {
            $response = response(Admin::content()->withError(trans('admin.deny')));
            Pjax::respond($response);
        }
        $manager = Manager::findOrFail($managerChildrenId);
        return $content
            ->title($this->title())
            ->description($this->description['create'] ?? trans('admin.create'))
            ->body($this->form($managerChildrenId, $manager->name));
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($managerId = null, $managerName = null)
    {
        $form = new Form(new PaymentManager());

        $form->hidden('owner_id')->default(FAdmin::user()->can(PERMISSION_ACCOUNTANT) ? ADMIN_ID : Auth::id());
        $form->text('manager_id', __('admin.manager_id'))->readonly()->default($managerId)->append('Đại lý: ' . $managerName);
        $form->textarea('desc', __('admin.description'));
        $form->currency('amount', __('admin.amount_pay'))->symbol('₫')->rules('required|min:1');

        $form->disableEditingCheck();
        $form->disableCreatingCheck();
        $form->disableViewCheck();
        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
            $tools->disableView();
            $tools->disableList();
        });

        $form->saved(function () {
            $success = new MessageBag([
                'title' => 'Thành công',
                'message' => 'Thêm thanh toán thành công.'
            ]);
            return back()->with(compact('success'));
        });

        return $form;
    }
}
