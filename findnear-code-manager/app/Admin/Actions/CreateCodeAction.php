<?php

namespace App\Admin\Actions;

use App\Models\Manager;
use App\Models\User;
use Carbon\Carbon;
use Encore\Admin\Actions\Action;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class CreateCodeAction extends Action
{
    public $name = 'Tạo mới';

    protected $selector = '.create-user-action';

    public function handle(Request $request)
    {
        $amount = $request->get('amount');
        $codes = [];

        while ($amount) {
            $code = randomCode();
            $passwordDefault = randomPassword();

            $codes[] = [
                'code' => $code,
                'owner_id' => Auth::id(),
                'password_default' => $passwordDefault,
                'password' => Hash::make($passwordDefault),
                'created_at' => gmdate('Y-m-d H:i:s')
            ];
            $amount--;
        }

        DB::beginTransaction();
        try {
            if (count($codes)) {
                User::insert($codes);
            }
            DB::commit();
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            DB::rollBack();
            return $this->response()->error(__('admin.create_failed'))->refresh();
        }

        return $this->response()->success(__('admin.create_succeeded'))->refresh();
    }

    public function form()
    {
        $user = Auth::user();


        if ($user->fixed_quantity) {
            $manager = Manager::with(['codeQuantities'])->withCount(['codes'])->findOrFail(Auth::id());

            $code = $manager->codeQuantities->sum('quantity_code') - ($manager->codes_count + $manager->sum_code_child);

            $this->name = 'Tạo mới ('. __('admin.code_can_generate') . $code . ')';

            $this->text('amount', __('admin.amount_code'))
                ->default(1)->rules(['required', 'numeric', 'min:1', 'max:' . ($code < 100 ? $code : 100)]);
        } else {
            $this->text('amount', __('admin.amount_code'))->default(1)
                ->rules(['required', 'numeric', 'min:1', 'max:100']);
        }
    }

   public function actionScript()
   {
       return <<<SCRIPT
        $('button[type="submit"]').click(() => {
            console.log(123123)
            $('button[type="submit"]').hide();
            setTimeout(() => {
                $('button[type="submit"]').show();
            }, 1500)
        })
SCRIPT;
   }

    public function html()
    {
        return '<a class="btn btn-sm btn-success create-user-action"><i class="fa fa-plus"></i> ' . __('admin.create') . '</a>';
    }
}
