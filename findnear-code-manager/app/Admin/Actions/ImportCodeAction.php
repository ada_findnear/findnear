<?php

namespace App\Admin\Actions;

use App\Imports\ImportCodeNice;
use App\Models\Manager;
use App\Models\User;
use Encore\Admin\Actions\Action;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class ImportCodeAction extends Action
{
    public $name = 'Import excel mã đẹp';

    protected $selector = '.create-user-action';

    public function handle(Request $request)
    {
        try {
            $file = $request->file('file_import');
            Excel::queueImport(new ImportCodeNice(), $file);
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            return $this->response()->error(__('admin.create_failed'))->refresh();
        }
        return $this->response()->success(__('admin.create_succeeded'))->refresh();
    }

    public function form()
    {
        $this->file('file_import')->rules(['mimes:xlsx,xls']);
    }

    public function html()
    {
        return '<a class="btn btn-sm btn-danger create-user-action"><i class="fa fa-upload"></i> ' . __('admin.import') . '</a>';
    }
}
