<?php

namespace App\Admin\Actions\Manager;

use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;

class AllowManagerCreateShop extends RowAction
{
    public $name = 'Cho phép phân quyền tạo shop';

    public function handle(Model $model)
    {
        $model->can_create_shop = true;
        $model->save();

        return $this->response()->success('Cho phép thành công!')->refresh();
    }
}
