<?php

namespace App\Admin\Actions\Manager;

use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;

class DenyManagerCreateShop extends RowAction
{
    public $name = 'Chặn phân quyền tạo shop';

    public function handle(Model $model)
    {
        $model->can_create_shop = false;
        $model->save();

        return $this->response()->success('chặn thành công!')->refresh();
    }
}
