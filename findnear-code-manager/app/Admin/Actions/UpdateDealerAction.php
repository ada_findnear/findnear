<?php

namespace App\Admin\Actions;

use App\Imports\ImportCodeDealer;
use App\Models\Manager;
use App\Models\User;
use Encore\Admin\Actions\Action;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class UpdateDealerAction extends Action
{
    public $name = 'Import excel mã - đại lý';

    protected $selector = '.update-user-dealer-action';

    public function handle(Request $request)
    {
        try {
            $file = $request->file('file_import');
            $array = Excel::toArray(new User(), $file);
            $valid_data = array_filter($array[0], function ($row) {
                return count($row) > 1 && $row[0] != '' && $row[1] != '';
            });
            foreach (array_chunk($valid_data, 1000) as $l)  
            {
                foreach($l as $ele){
                    $id = intval(str_replace('DL','', $ele[1]));
                    User::where('code',$ele[0])->update(['owner_id' => $id]);
                }
            }
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            return $this->response()->error(__('admin.update_failed'))->refresh();
        }
        return $this->response()->success(__('admin.update_succeeded'))->refresh();
    }

    public function form()
    {
        $this->file('file_import')->rules(['mimes:xlsx,xls']);
    }

    public function html()
    {
        return '<a class="btn btn-sm btn-warning update-user-dealer-action"><i class="fa fa-upload"></i> ' . __('admin.udpate_dealers') . '</a>';
    }
}
