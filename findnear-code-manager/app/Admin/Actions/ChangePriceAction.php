<?php

namespace App\Admin\Actions;

use App\Models\CodeQuantity;
use App\Models\Manager;
use App\Models\User;
use Carbon\Carbon;
use Encore\Admin\Actions\RowAction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class ChangePriceAction extends RowAction
{
    public $name = 'Sửa giá';

    protected $selector = '.change-price-action';

    public function handle(CodeQuantity $code, Request $request)
    {
        $price = $request->get('price');
        if(!$code){
            return $this->response()->error(__('admin.save_failed'))->refresh();
        }
        $code->price = $price;
        $code->save();
        return $this->response()->success(__('admin.save_succeeded'))->refresh();
    }

    public function form()
    {
        $this->name = 'Sửa giá';
        $this->hidden('id')
            ->value($this->getKey());
        $this->text('price', __('admin.new_price'))
            ->default($this->row('price'))->rules(['required', 'numeric']);
    }

   public function actionScript()
   {
       return <<<SCRIPT
        $('button[type="submit"]').click(() => {
            console.log(123123)
            $('button[type="submit"]').hide();
            setTimeout(() => {
                $('button[type="submit"]').show();
            }, 1500)
        })
SCRIPT;
   }

    public function html()
    {
        return '<a class="btn btn-sm btn-success change-price-action"><i class="fa fa-plus"></i> ' . __('admin.submit') . '</a>';
    }
}
