<?php

namespace App\Admin\Actions\Post;

use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ResetPassword extends RowAction
{
    public $name = 'Reset mật khẩu';

    public function handle(Model $model)
    {
        $password = randomPassword();
        $model->password_default = $password;
        $model->password = bcrypt($password);
        $model->save();

        return $this->response()->success('Khôi phục mật khẩu thành công!')->refresh();
    }
}
