<?php

namespace App\Admin\Actions\Post;

use Carbon\Carbon;
use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class AccountRenewal extends RowAction
{
    public $name = 'Gia hạn tài khoản';

    public function handle(Model $model)
    {
        $now = gmdate('Y-m-d H:i:s');
        $expiredAt = $model->expired_at < $now ? $now : Carbon::parse($model->expired_at);
        $model->expired_at = $expiredAt->addYear();
        $model->save();

        return $this->response()->success('Gia hạn thành công!')->refresh();
    }
}
