<?php

namespace App\Admin\Actions\Post;

use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;

class AddInfo extends RowAction
{
    public $name = 'Thêm thông tin';

//    public function handle(Model $model)
//    {
//        // $model ...
//
//        return $this->response()->success('Success message.')->refresh();
//    }

    public function href()
    {
        return '/code/' . $this->getKey() . '/edit';
    }

}
