<?php

namespace App\Admin\Actions\Post;

use DateTime;
use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class Delete extends RowAction
{
    public $name = 'Cài đặt lại';

    public function handle(Model $model)
    {
        //TODO: improve deleting data;
        $id = $model->id;
        $model->password = Hash::make($model->password_default);
        $model->save();
        DB::statement("update users set total_point =0, spent_point = 0, name='', current_point =0, can_create_shop = 0 ,deleted_at = null, active = 0 , api_token = null, device_token = null, expired_at = null where id = $id;");
        DB::statement("DELETE from user_details where id in (select user_detail_id from users where id = $id);");
        DB::statement("DELETE from vnp_products where user_id = $id;");
        DB::statement("DELETE from vnp_shops where user_id = $id;");
        DB::statement("DELETE from point_histories where user_id = $id;");
        DB::statement("DELETE from game_histories where user_id = $id");
        DB::statement("DELETE from blocks where user_id = $id");
        DB::statement("DELETE from carts where user_id = $id");
        DB::statement("DELETE from comments where user_id = $id");
        DB::statement("DELETE from delivery_addresses where user_id = $id");
        DB::statement("DELETE from drivers_payouts where user_id = $id");
        DB::statement("DELETE from driver_markets where user_id = $id");
        DB::statement("DELETE from drivers where user_id = $id");
        DB::statement("DELETE from favorites where user_id = $id");
        DB::statement("DELETE from interactions where user_id = $id");
        DB::statement("DELETE from market_reviews where user_id = $id");
        DB::statement("DELETE from moments where user_id = $id");
        DB::statement("DELETE from payments where user_id = $id");
        DB::statement("DELETE from payment_users where user_id = $id");
        DB::statement("DELETE from orders where user_id = $id");
        DB::statement("DELETE from posts where user_id = $id");
        DB::statement("DELETE from post_saves where user_id = $id");
        DB::statement("DELETE from post_hides where user_id = $id");
        DB::statement("DELETE from product_reviews where user_id = $id");
        DB::statement("DELETE from sms_logs where user_id = $id");
        DB::statement("DELETE from user_markets where user_id = $id");
        DB::statement("DELETE from votes where user_id = $id");
        DB::statement("DELETE from vnp_shops where user_id = $id");
        DB::statement("DELETE from media where model_id = $id and model_type ='App\\\\Models\\\\User'");
        return $this->response()->success('Xoá thành công!')->refresh();
    }
    public function dialog()
    {
        $this->confirm('Dữ liệu không thể phục hồi, bạn có chắc chắn cài lại?');
    }
}
