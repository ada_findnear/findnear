<?php

namespace App\Admin\Actions\Post;

use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class DenyCreateShop extends RowAction
{
    public $name = 'Chặn chức năng tạo shop';

    public function handle(Model $model)
    {
        $model->can_create_shop = false;
        $model->user_allow_create_shop_id = null;
        $model->allow_create_shop_at = null;
        $model->save();

        return $this->response()->success('Chặn thành công!')->refresh();
    }
}
