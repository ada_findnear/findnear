<?php

namespace App\Admin\Actions\Post;

use App\Models\Manager;
use DateTime;
use Encore\Admin\Actions\RowAction;
use Encore\Admin\Facades\Admin;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AllowCreateShop extends RowAction
{
    public $name = 'Cho phép tạo shop';

    public function name(){
        if(Admin::user()->isAdministrator() && $this->row->waiting_for_admin_approve){
            return 'Duyệt';
        }
        else{
            return $this->name;
        }
    }
    public function handle(Model $model)
    {
        $model->can_create_shop = true;
        $authUser = Manager::find(Auth::id());
        // dd(($authUser->codesUpgraded->count() < $authUser->upgradeQuantities()->sum('quantity')));
        if(Admin::user()->isAdministrator() || ($authUser->codesUpgraded->count() < $authUser->upgradeQuantities()->sum('quantity'))) {
            $model->waiting_for_admin_approve = false;
            if(!Admin::user()->isAdministrator()){
                $model->user_allow_create_shop_id = Auth::id();
                $model->allow_create_shop_at = new DateTime();
            }
        }
        else{
            $model->user_allow_create_shop_id = Auth::id();
            $model->allow_create_shop_at = new DateTime();
            $model->waiting_for_admin_approve = true;
        }
        $model->save();

        return $this->response()->success((Admin::user()->isAdministrator() ? 'Duyệt' : 'Cho phép') . ' thành công!')->refresh();
    }
}
