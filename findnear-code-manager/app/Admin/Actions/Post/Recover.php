<?php

namespace App\Admin\Actions\Post;

use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Recover extends RowAction
{
    public $name = 'Phục hồi';

    public function handle(Model $model)
    {
        $model->deleted_at = null;
        $model->save();

        return $this->response()->success('Phục hồi thành công!')->refresh();
    }
}
