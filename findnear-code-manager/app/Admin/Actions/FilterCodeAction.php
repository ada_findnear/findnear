<?php

namespace App\Admin\Actions;

use App\Imports\ImportCodeNice;
use App\Models\Manager;
use App\Models\User;
use Encore\Admin\Actions\Action;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class FilterCodeAction extends Action
{
    public $name = 'Lọc excel mã đẹp';

    protected $selector = '.filter-user-action';

    public function handle(Request $request)
    {
        try {
            $file = $request->file('file_filter');
            $array = Excel::toArray(new User(), $file);
            $valid_data = array_filter($array[0], function ($row) {
                return count($row) > 0 && $row[0] != '';
            });
            $res = array_map(function ($ele) {
                return ['filter_code' => $ele[0]];
            }, $valid_data);
            DB::table('filter_codes')->truncate();
            foreach (array_chunk($res,1000) as $t)  
            {
                DB::table('filter_codes')->insert($t); 
            }
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            return $this->response()->error(__('admin.filter_failed'))->refresh();
        }
        return $this->response()->success(__('admin.filter_succeeded'))->redirect('/code/nice/filtered');
    }

    public function form()
    {
        $this->file('file_filter')->rules(['mimes:xlsx,xls']);
    }

    public function html()
    {
        return '<a class="btn btn-sm btn-success filter-user-action"><i class="fa fa-upload"></i> ' . __('admin.filter_by_excel') . '</a>';
    }
}
