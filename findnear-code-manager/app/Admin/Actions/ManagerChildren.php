<?php

namespace App\Admin\Actions;

use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;

class ManagerChildren extends RowAction
{
    public $name = 'Đại lí cấp dưới';

    public function href()
    {
        return '/managers?q=' . $this->getKey();
    }

}
