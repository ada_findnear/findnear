<?php

use Encore\Admin\Facades\Admin;
use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', function () {
        if(Admin::user()->isAdministrator() || Admin::user()->can(PERMISSION_CUSTOMER_SERVICE) || Admin::user()->can(PERMISSION_ACCOUNTANT)){
            return redirect('/code');
        }
        return redirect('/dealer-code');
    });
    $router->get('/accountant', 'AccountantController@index')->name('accountant');
    $router->get('/charts', 'ManagerController@charts');
    $router->get('/code/nice', 'CodeController@nice');
    $router->get('/code/nice/filtered', 'CodeController@filteredNice');
    $router->get('/pending-codes', DealerCodeController::class.'@pending');
    $router->get('/my-code', DealerCodeController::class.'@mine');
    $router->get('/report-dealer', ManagerController::class.'@report');
    $router->get('/activities', 'ActivityController@index');

    $router->resource('code', CodeController::class);
    $router->resource('dealer-code', DealerCodeController::class);
    $router->resource('managers', ManagerController::class)->except(['']);
    $router->resource('payment-managers', PaymentManagerController::class)->except(['index', 'edit', 'delete']);
});
