<?php

use Carbon\Carbon;

if (!function_exists('randomCode')) {
    function randomCode()
    {
        while (true) {
            $code = '';
            for ($i = 0; $i < CODE_LENGTH; $i++) {
                $code .= rand(0, 9);
            }
            if (!App\Models\User::where('code', $code)->exists()) {
                break;
            }
        }

        return $code;
    }
}

if (!function_exists('randomPassword')) {
    function randomPassword()
    {
        return str_pad(mt_rand(0, 999999), 6, '0', STR_PAD_LEFT);
    }
}

if (!function_exists('randomRefCode')) {
    function randomRefCode()
    {
        return str_pad(mt_rand(0, 9999999999), 10, '0', STR_PAD_LEFT);
    }
}

if (!function_exists('getIp')) {
    function getIp(){
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
        return request()->ip(); // it will return server ip when no client ip found
    }
}
if (!function_exists('formatDateTime')) {
    function formatDateTime($date, $addition = 7): string
    {
        return Carbon::createFromFormat('U', strtotime($date))->addHours($addition)->format('d/m/Y (H:i)');
    }
}