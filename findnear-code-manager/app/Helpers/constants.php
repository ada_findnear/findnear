<?php

if (!defined('CODE_LENGTH')) {
    define('CODE_LENGTH', 8);
}

if (!defined('PERMISSION_ACCOUNTANT')) {
    define('PERMISSION_ACCOUNTANT', 'accountant');
}

if (!defined('PERMISSION_CUSTOMER_SERVICE')) {
    define('PERMISSION_CUSTOMER_SERVICE', 'customer_service');
}

if (!defined('ADMIN_ID')) {
    define('ADMIN_ID', 1);
}
