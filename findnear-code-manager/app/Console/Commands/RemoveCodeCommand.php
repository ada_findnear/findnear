<?php

namespace App\Console\Commands;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RemoveCodeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'code:remove';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command is remove code expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        User::where('active', User::NOT_ACTIVATED_STATUS)
            ->whereDate('created_at', '<=', date('Y-m-d', strtotime("-2 days")))
            ->where('is_nice', 0)
            ->delete();
    }
}
