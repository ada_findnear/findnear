###HOW TO USE:###
# chmod +x ./release_firebase.sh
# ./release_firebase.sh android dev true
#################
##Platform default (both)
##Scheme default (dev)

findConfigureValue(){
  local keyword="$1 $2 $3"
  platform="both"
  scheme="dev"
  if [[ $keyword == *"android"* ]]; then
    platform="android"
  fi
  if [[ $keyword == *"ios"* ]]; then
    platform="ios"
  fi

  if [[ $keyword == *"prod"* ]]; then
    scheme="prod"
  fi

}

findConfigureValue $1 $2 $3

#########################################################
#flutter clean
flutter pub get

buildAndroid(){
  flutter build apk --flavor $scheme --release -t lib/main_$scheme.dart
  cd android || exit 1
  bundle config set --local path 'vendor/bundle'
  bundle install
  bundle exec fastlane deploy_firebase --env $scheme
  cd ..
}

buildIOS(){
  flutter build ios --flavor $scheme --release --no-codesign lib/main_$scheme.dart
  cd ios  || exit 1
  bundle config set --local path 'vendor/bundle'
  bundle install
  bundle exec fastlane build --env $scheme
  bundle exec fastlane deploy_firebase --env $scheme
  cd ..
}
if [[ "$platform" == "android" ]]; then
  echo "Android has been selected for distributing"
  buildAndroid
elif [[ "$platform" == "ios" ]]; then
  echo "iOS has been selected for distributing"
  buildIOS
elif [[ "$platform" == "both" ]]; then
  echo "Android and iOS has been selected for distributing"
  buildAndroid
  buildIOS
fi










