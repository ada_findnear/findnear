import UIKit
import Flutter
import GoogleMaps
import PushKit
import flutter_callkit_incoming

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    
    let methodChannelName = "vn.tada.findnear/iosNative"
    var methodChannel: FlutterMethodChannel? = nil
    
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        setupMethodChannel()
        //Add your Google Maps API Key here
        GMSServices.provideAPIKey("AIzaSyBtaQfYohHQE01th0fmXvKLG-DvIvcBhrI")
        GeneratedPluginRegistrant.register(with: self)
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        }
        self.voipRegistration()
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    private func setupMethodChannel() {
        if let rootViewController = window?.rootViewController as? FlutterViewController {
            methodChannel = FlutterMethodChannel(name: methodChannelName, binaryMessenger: rootViewController.binaryMessenger)
            
            methodChannel?.setMethodCallHandler({ call, result in
                if call.method == "somethink" {
                } else {
                    print("Method not implemented: \(call.method)");
                }
            })
        }
    }
    
    // Register for VoIP notifications
    private func voipRegistration() {
        // Create a push registry object
        let mainQueue = DispatchQueue.main
        let voipRegistry: PKPushRegistry = PKPushRegistry(queue: mainQueue)
        voipRegistry.delegate = self
        voipRegistry.desiredPushTypes = [.voIP]
    }
}

//MARK: - PKPushRegistryDelegate
extension AppDelegate : PKPushRegistryDelegate {
    func pushRegistry(
        _ registry: PKPushRegistry,
        didReceiveIncomingPushWith payload: PKPushPayload,
        for type: PKPushType,
        completion: @escaping () -> Void
    ) {
        print("AppDelegate - didReceiveIncomingPushWith - payload: \(payload.dictionaryPayload)")
        
        if type == .voIP {
            guard let type = payload.dictionaryPayload["type"] as? String, type == "income" else {
                return
            }
            let state = UIApplication.shared.applicationState
            guard state != .active else { return }
            
//            let action = (payload.dictionaryPayload["action"] as? String) ?? "" // VIDEO_CALL
            let typeCall = (payload.dictionaryPayload["typeCall"] as? String) ?? "" // VIDEO_CALL/VOICE_CALL/END_CALL
            let messageId = (payload.dictionaryPayload["messageId"] as? String) ?? ""
            
            let userInfoMakeCall = (((payload.dictionaryPayload["userInfoMakeCall"] as? String) ?? "").toJSON() as? [String: Any?]) ?? [:]
            let userId = (userInfoMakeCall["userId"] as? String) ?? ""
            let name = (userInfoMakeCall["name"] as? String) ?? ""
            
            if typeCall == "END_CALL" {
                SwiftFlutterCallkitIncomingPlugin.sharedInstance?.saveEndCall(messageId, 2)
            } else {
                var info = [String: Any?]()
                info["id"] = messageId
                info["nameCaller"] = name
                info["number"] = userId
                info["handle"] = userId
                if typeCall == "VOICE_CALL" {
                    info["type"] = 0
                } else if typeCall == "VIDEO_CALL" {
                    info["type"] = 1
                }
                info["duration"] = 60000

                print("AppDelegate - didReceiveIncomingPushWith - info: \(info)")
                SwiftFlutterCallkitIncomingPlugin.sharedInstance?.showCallkitIncoming(flutter_callkit_incoming.Data(args: info), fromPushKit: true)
            }
        }
        completion()
    }

    func pushRegistry(
        _ registry: PKPushRegistry,
        didUpdate pushCredentials: PKPushCredentials,
        for type: PKPushType
    ) {
      let deviceToken = pushCredentials.token.map { String(format: "%02x", $0) }.joined()
      print("AppDelegate - pushRegistry - pushkit device token: \(deviceToken)")
      SwiftFlutterCallkitIncomingPlugin.sharedInstance?.setDevicePushTokenVoIP(deviceToken)
    }

    func pushRegistry(
        _ registry: PKPushRegistry,
        didInvalidatePushTokenFor type: PKPushType
    ) {
      print("AppDelegate - didInvalidatePushTokenFor")
      SwiftFlutterCallkitIncomingPlugin.sharedInstance?.setDevicePushTokenVoIP("")
    }
}

extension String {
    func toJSON() -> Any? {
        guard let data = self.data(using: .utf8, allowLossyConversion: false) else { return nil }
        return try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
    }
}
