part of tenor;

Future _serverRequest(
  String url, {
  String? domainRedirect,
}) async {
  if (domainRedirect != null && domainRedirect.isNotEmpty) {
    var request = http.MultipartRequest(
      'POST',
      Uri.parse('${domainRedirect}v2/tool/getDataFromUrl'),
    );
    request.fields['url'] = url.replaceAll(' ', '%20');
    request.fields['method'] = 'GET';
    var response = await http.Response.fromStream(await request.send());

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      return data;
    } else {
      // something went wrong :(
      return [];
    }
  } else {
    var httpClient = HttpClient();
    var request = await httpClient.getUrl(Uri.parse(url));
    var response = await request.close();
    if (response.statusCode == 200) {
      var json = await utf8.decoder.bind(response).join();
      var data = jsonDecode(json);
      return data;
    } else {
      // something went wrong :(
      return [];
    }
  }
}
