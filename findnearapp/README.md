# Introduction
Find Near là 1 ứng dụng cung cấp các giải pháp kết nối người mua và bán một cách dễ dàng tiện lợi. Find Near xây dựng một cộng đồng những người yêu thích sử dụng và trải nghiệm công nghệ nhằm phục vụ nhu cầu cơ bản của người sử dụng với nơi các thông tin đã được kiểm chứng, giàu tính tin cậy và các trải nghiệm phong phú được trao đổi.

# Project Structure

- Mục tiêu: Để team mình hiểu nhau hơn, dễ bảo trì, mở rộng sau này.
- Rất mong trong quá trình làm việc nếu có những kỹ thuật nào hay hoặc cần thống nhất, mọi người hãy chia sẻ cho team và đưa vào README.md này nhé

## State management
- [Get implementation](https://github.com/jonataslaw/getx/blob/master/documentation/en_US/state_management.md)
- [Base](./lib/src/base)
# Base implementation

## Create a folder includes three files:
### state.dart extends [BaseState](./lib/src/base/base_state.dart)
- Common properties: isLoading, isSubmitting, error _(Please use these)_
### controller.dart extends [BaseController](./lib/src/base/base_controller.dart)
- Common functions
### view.dart `_*PageState` extends [BaseWidgetState](./lib/src/base/base_widget_state.dart)
- To handle show error, message, other notification, lifecycle
- Auto dispose controller
- Common functions
    
### state.dart
```dart
import '../../src/base/base_state.dart';
import 'package:get/get.dart';

class ExampleState extends BaseState {
  final message = "Initial message".obs;
}

```

### controller.dart
```dart
import '../../src/base/base_controller.dart';
import '../../src/base/base_state.dart';

import 'audience_livestream_state.dart';

class ExampleController extends BaseController {
  @override
  BaseState createState() => ExampleState();
  
  void getData(){
    try {
      state.isLoading = true;
      state.message.value = 'new message';
      state.isLoading = false;
    } catch(e) {
      state.error = e;
      state.isLoading = false;
    }
  }
}

```

### view.dart
```dart
import '../../src/base/base_widget_state.dart';
import '../../src/pages/example/audience_livestream_state.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'controller.dart';

class ExamplePage extends StatefulWidget {
 
  @override
  _ExamplePageState createState() => _ExamplePageState();
}

class _ExamplePageState extends BaseWidgetState<ExamplePage, ExampleController, ExampleState> {

  @override
  void initState() {
    controller.getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(child: Obx(() => Text(state.message.value)));
  }

  @override
  ExampleController createController() => ExampleController(); // or Get.find();
}
```

## Declare your controller as a dependency if you want to share with other places
In [di.dart](./lib/src/commons/di.dart)

```dart
void _injectControllers() {
  //...
  Get.lazyPut(ExampleController());
}

```

## References (Please check for full implementation)
- [List Shop](./lib/src/pages/list_shop)
- [Lottery Game](./lib/src/pages/game/lottery)

# Pull to refresh and load more a list

- Mix a `BaseWidgetState with RefreshLoadMoreWidgetMixin`
- Mix a `BaseController with RefreshLoadMoreControllerMixin`
- Wrap `buildList(child: listWidget)` function on your list widget
- Override `loadList()` function in `BaseController` to implement logic of getting data
- Reference: [List Shop](./lib/src/pages/list_shop)

# Network
- [Dio](https://pub.dev/packages/dio)
- Implement Repository pattern  
- References: [GameRepository](./lib/src/repository/game_repository.dart)
- Should be group by **Call & Chat**, **Market**, **Friend**, **Game**, **Newsfeed**, **User**, **Others**

# Route
- [Route management of Get](https://github.com/jonataslaw/getx/blob/master/documentation/en_US/route_management.md)
- All navigation actions must be called through functions in [navigator_utils.dart](./lib/src/utils/navigator_utils.dart)

# Rules

- [Dart code style](https://dart.dev/guides/language/effective-dart/style)
- Define colors in [app_colors.dart](./lib/src/commons/app_colors.dart)
- Define images in [app_images.dart](./lib/src/commons/app_images.dart)
- To display images from assets folder, use [AppImage](./lib/src/widgets/common.dart)
- To show toast messages or notification, use [AppSnackbar](./lib/src/commons/app_snackbar.dart)
- To show loading, use [LoadingView](./lib/src/widgets/common.dart)
- For cache data, use [LocalDataManager](./lib/src/utils/local_data_manager.dart)
- All works relate to **currentUser**, use **localDataManager.currentUser** 


# Beginer install
## Môi trường
```
Flutter 2.5.3
https://docs.flutter.dev/get-started/install/macos
```

```
flutter pub global activate intl_utils 1.9.0
flutter pub global run intl_utils:generate

// flutter pub run intl_utils:generate
```