
import 'main.dart';

import 'src/commons/app_environment.dart';

void main(){
  currentEnvironment = Environment.prod;
  loadApp();
}