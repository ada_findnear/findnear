import 'dart:async';
import 'dart:io';
import 'dart:isolate';

import 'package:findnear/src/commons/app_environment.dart';
import 'package:findnear/src/commons/app_themes.dart';
import 'package:findnear/src/commons/di.dart';
import 'package:findnear/src/global/global_data.dart';
import 'package:findnear/src/pages/splash_screen.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/listener_event_callkit.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_mentions/flutter_mentions.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:hive/hive.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'generated/l10n.dart';
import 'route_generator.dart';
import 'src/helpers/custom_trace.dart';
import 'src/repository/settings_repository.dart' as settingRepo;

export 'package:findnear/generated/l10n.dart';

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

Future<void> main() async {
  currentEnvironment = Environment.dev;
  loadApp();
}

Future<void> loadApp() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  HttpOverrides.global = new MyHttpOverrides();

  if (currentEnvironment == Environment.dev) {
    await GlobalConfiguration().loadFromAsset("configurations");
  } else {
    await GlobalConfiguration().loadFromAsset("configurations_prod");
  }

  // init Hive before injectDependencies
  final appDocumentDirectory = await getApplicationDocumentsDirectory();
  logger.d("Hive path: ${appDocumentDirectory.path}");
  Hive.init(appDocumentDirectory.path + "/hive_databases");

  getAppdocumentApp(appDocumentDirectory);

  await injectDependencies();

  await GetStorage.init();
  await print(CustomTrace(StackTrace.current,
      message: "base_url: ${GlobalConfiguration().getValue('base_url')}"));
  print(CustomTrace(StackTrace.current,
      message:
          "api_base_url: ${GlobalConfiguration().getValue('api_base_url')}"));
  print(CustomTrace(StackTrace.current,
      message:
          "api_base_url: ${GlobalConfiguration().getValue('api_base_url')}"));

  if (kDebugMode) {
    runApp(MyApp());
  } else {
    runZonedGuarded<Future<void>>(() async {
      WidgetsFlutterBinding.ensureInitialized();
      await Firebase.initializeApp();
      FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
      runApp(MyApp());
    },
        (error, stack) =>
            FirebaseCrashlytics.instance.recordError(error, stack));
    Isolate.current.addErrorListener(RawReceivePort((pair) async {
      final List<dynamic> errorAndStacktrace = pair;
      await FirebaseCrashlytics.instance.recordError(
        errorAndStacktrace.first,
        errorAndStacktrace.last,
      );
    }).sendPort);
  }
}

void getAppdocumentApp(Directory appDocumentDirectory) {
  String appDocPath = '${appDocumentDirectory.path}/FindNear';
  if (!Directory(appDocPath).existsSync()) Directory(appDocPath).createSync();
  GlobalData.instance.documentPath = appDocPath;
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  final eventCallKit = ListenerEventCallKit();
  AppLifecycleState state;
  int start = 0;
  var overlayEntry;

  @override
  void initState() {
    // settingRepo.initSettings();
    // settingRepo.getCurrentLocation();
    checkExpiredApiToken();
    WidgetsBinding.instance.addObserver(this);
    eventCallKit.listenerEventCallKit();
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    Hive.close();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState appLifecycleState) {
    // TODO: implement didChangeAppLifecycleState
    super.didChangeAppLifecycleState(appLifecycleState);
    state = appLifecycleState;
    eventCallKit.updateState(state);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Locale>(
      future: settingRepo.getSettingsFromLocal(),
      builder: (BuildContext context, AsyncSnapshot<Locale> snapshot) {
        Locale locale = snapshot.data ?? Locale("vi");
        return ScreenUtilInit(
          builder: () => RefreshConfiguration(
            headerBuilder: () => MaterialClassicHeader(
              color: AppThemes.lightTheme.colorScheme.secondary,
            ),
            footerBuilder: () => ClassicFooter(
              loadingIcon: SizedBox(
                width: 25.0,
                height: 25.0,
                child: defaultTargetPlatform == TargetPlatform.iOS
                    ? CupertinoActivityIndicator()
                    : CircularProgressIndicator(
                        strokeWidth: 2.0,
                        color: AppThemes.lightTheme.colorScheme.secondary,
                      ),
              ),
            ),
            child: GetMaterialApp(
              enableLog: true,
              navigatorKey: Get.key,
              title: "_setting.appName",
              initialRoute: SplashScreen.ROUTE_NAME,
              onGenerateRoute: RouteGenerator.generateRoute,
              debugShowCheckedModeBanner: false,
              locale: locale,
              localizationsDelegates: [
                S.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
              supportedLocales: S.delegate.supportedLocales,
              theme: AppThemes.lightTheme,
              darkTheme: AppThemes.darkTheme,
              themeMode: ThemeMode.system,
              navigatorObservers: [GetObserver()],
              builder: (_, child) => Portal(child: child),
            ),
          ),
        );
      },
    );
  }
}
