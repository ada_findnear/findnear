import 'package:findnear/src/pages/create_post_v2/create_post_screenv2.dart';
import 'package:findnear/src/pages/crop_image/crop_image_view.dart';
import 'package:findnear/src/pages/control_panel/control_panel_view.dart';
import 'package:findnear/src/pages/draw_image/draw_image_view.dart';
import 'package:findnear/src/pages/forgot_password/forgot_password_screen.dart';
import 'package:findnear/src/pages/game/get_coin/view.dart';
import 'package:findnear/src/pages/game/wheel-character-collect/view.dart';
import 'package:findnear/src/pages/game/wheel/view.dart';
import 'package:findnear/src/pages/game/lottery/view.dart';
import 'package:findnear/src/pages/list_friend.dart';
import 'package:findnear/src/pages/list_livestream/list_livestream_view.dart';
import 'package:findnear/src/pages/list_shop/view.dart';
import 'package:findnear/src/pages/login_register.dart';
import 'package:findnear/src/pages/list_user_react/list_user_react_view.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/chat_group_setting_view.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/members/chat_group_members_view.dart';
import 'package:findnear/src/pages/messages/conversation_detail/conversation_detail_view.dart';
import 'package:findnear/src/pages/messages/create_conversation/create_conversation_view.dart';
import 'package:findnear/src/pages/messages/image_file_summary/image_file_summary_view.dart';
import 'package:findnear/src/pages/messages/list_search_conversation/list_search_conversation_view.dart';
import 'package:findnear/src/pages/messages/pin_message/list_message_pinned/list_message_pinned_view.dart';
import 'package:findnear/src/pages/messages/poll/create_poll/create_poll_view.dart';
import 'package:findnear/src/pages/messages/send_name_card/send_name_card_view.dart';
import 'package:findnear/src/pages/pass_code/setting_pin_code/setting_pin_code_view.dart';
import 'package:findnear/src/pages/messages/pin_message/pin_message_view.dart';
import 'package:findnear/src/pages/preview_document.dart';
import 'package:findnear/src/pages/product/presentation/create_product/view.dart';
import 'package:findnear/src/pages/product_detail/view.dart';
import 'package:findnear/src/pages/profile_user/profile_user_view.dart';
import 'package:findnear/src/pages/rating/rating_page.dart';
import 'package:findnear/src/pages/register_shop_page/register_shop_page.dart';
import 'package:findnear/src/pages/search/auto_complete/search_auto_complete.dart';
import 'package:findnear/src/pages/shop_detail/product_by_category/view.dart';
import 'package:findnear/src/pages/shop_detail/view.dart';
import 'package:findnear/src/pages/videocall/callcoming/view.dart';
import 'package:findnear/src/pages/videocall/view.dart';
import 'package:findnear/src/pages/voice_call_v2/voice_call_screenv2.dart';
import 'package:findnear/src/pages/voice_call_v2/voicecomming/voice_comming_screen.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:flutter/material.dart';

import 'src/models/route_argument.dart';
import 'src/pages/contact_us.dart';
import 'src/pages/friends_request.dart';
import 'src/pages/help.dart';
import 'src/pages/languages.dart';
import 'src/pages/livestream/audience/audience_livestream_view.dart';
import 'src/pages/livestream/broadcast_livestream_view.dart';
import 'src/pages/login.dart';
import 'src/pages/messages/edit_conversation/edit_conversation_view.dart';
import 'src/pages/pages.dart';
import 'src/pages/position_picker.dart';
import 'src/pages/settings.dart';
import 'src/pages/sign_up/signup.dart';
import 'src/pages/splash_screen.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // Getting arguments passed in while calling Navigator.pushNamed
    final args = settings.arguments;
    switch (settings.name) {
      case SplashScreen.ROUTE_NAME:
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case SignUpWidget.ROUTE_NAME:
        return MaterialPageRoute(builder: (_) => SignUpWidget());
      case LoginWidget.ROUTE_NAME:
        return MaterialPageRoute(
            settings: RouteSettings(name: LoginWidget.ROUTE_NAME),
            builder: (_) => LoginWidget());
      case ContactUsWidget.routeName:
        return MaterialPageRoute(builder: (_) => ContactUsWidget());
      case ForgotPasswordScreen.ROUTE_NAME:
        return MaterialPageRoute(builder: (_) => ForgotPasswordScreen());
      case PagesWidget.ROUTE_NAME:
        return PageRouteBuilder(
            settings: RouteSettings(name: PagesWidget.ROUTE_NAME),
            pageBuilder: (_, __, ___) {
              prettyLog.d('/Pages with args = $args');
              return PagesWidget(currentTab: args);
            },
            transitionsBuilder: (_, animation, __, child) {
              return FadeTransition(
                opacity: animation,
                child: child,
              );
            });
      case LanguagesWidget.ROUTE_NAME:
        return MaterialPageRoute(builder: (_) => LanguagesWidget());
      case '/Help':
        return MaterialPageRoute(builder: (_) => HelpWidget());
      case '/Settings':
        return MaterialPageRoute(builder: (_) => SettingsWidget());
      case '/ListFriend':
        return MaterialPageRoute(builder: (_) => ListFriendWidget());
      case ListShopPage.ROUTE_NAME:
        return MaterialPageRoute(
            builder: (_) => ListShopPage(routeArgument: args as RouteArgument));
      case ProductDetailPage.ROUTE_NAME:
        return MaterialPageRoute(
            builder: (_) =>
                ProductDetailPage(routeArgument: args as RouteArgument));
      case ShopDetailPage.ROUTE_NAME:
        return MaterialPageRoute(
            settings: RouteSettings(name: ShopDetailPage.ROUTE_NAME),
            builder: (_) =>
                ShopDetailPage(routeArgument: args as RouteArgument));
      case CreateProductPage.ROUTE_NAME:
        return MaterialPageRoute(
            builder: (_) =>
                CreateProductPage(routeArgument: args as RouteArgument));
      case '/FriendsRequest':
        return MaterialPageRoute(builder: (_) => FriendsRequestWidget());
      case GetCoinPage.ROUTE_NAME:
        return MaterialPageRoute(builder: (_) => GetCoinPage());
      case LotteryPage.ROUTE_NAME:
        return MaterialPageRoute(
            builder: (_) => LotteryPage(routeArgument: args as RouteArgument));
      case WheelPage.ROUTE_NAME:
        return MaterialPageRoute(builder: (_) => WheelPage());
      // case VoiceCallPage.ROUTE_NAME:
      //   return MaterialPageRoute(
      //       builder: (_) =>
      //           VoiceCallPage(routeArgument: args as RouteArgument));
      // case VoiceCallComingPage.ROUTE_NAME:
      //   return MaterialPageRoute(
      //       builder: (_) =>
      //           VoiceCallComingPage(routeArgument: args as RouteArgument));
      case ChatGroupSettingPage.ROUTE_NAME:
        return MaterialPageRoute(
            builder: (_) =>
                ChatGroupSettingPage(routeArgument: args as RouteArgument));
      case LoginRegisterPage.ROUTE_NAME:
        return MaterialPageRoute(builder: (_) => LoginRegisterPage());
      case ListUserReactPage.ROUTE_NAME:
        return MaterialPageRoute(
            builder: (_) =>
                ListUserReactPage(routeArgument: args as RouteArgument));
      case PositionPickerWidget.ROUTE_NAME:
        return MaterialPageRoute(
            builder: (_) =>
                PositionPickerWidget(routeArgument: args as RouteArgument));
      case RegisterShopPage.ROUTE_NAME:
        return MaterialPageRoute(builder: (_) => RegisterShopPage());
      case WheelCharacterCollectPage.ROUTE_NAME:
        return MaterialPageRoute(builder: (_) => WheelCharacterCollectPage());
      case BroadcastLivestreamPage.ROUTE_NAME:
        return MaterialPageRoute(
            builder: (_) =>
                BroadcastLivestreamPage(routeArgument: args as RouteArgument));
      case AudienceLivestreamPage.ROUTE_NAME:
        return MaterialPageRoute(
            builder: (_) =>
                AudienceLivestreamPage(routeArgument: args as RouteArgument));
      case PinMessagePage.ROUTE_NAME:
        return MaterialPageRoute(builder: (_) => PinMessagePage());
      case ControlPanelPage.ROUTE_NAME:
        return MaterialPageRoute(builder: (_) => ControlPanelPage());
      case SettingPinCodePage.ROUTE_NAME:
        return MaterialPageRoute(
            builder: (_) =>
                SettingPinCodePage(routeArgument: args as RouteArgument));
      case ListMessagePinnedPage.ROUTE_NAME:
        return MaterialPageRoute(builder: (_) => ListMessagePinnedPage());
      case CropImagePage.ROUTE_NAME:
        return MaterialPageRoute(
          builder: (_) => CropImagePage(routeArgument: args as RouteArgument),
          fullscreenDialog: true,
        );
      case DrawImagePage.ROUTE_NAME:
        return MaterialPageRoute(
          builder: (_) => DrawImagePage(),
          fullscreenDialog: true,
        );
      case ListSearchConversationPage.ROUTE_NAME:
        return MaterialPageRoute(builder: (_) => ListSearchConversationPage());
      case ConversationDetailPage.ROUTE_NAME:
        return MaterialPageRoute(
            builder: (_) =>
                ConversationDetailPage(routeArgument: args as RouteArgument));
      case AutoCompletePlacePage.ROUTE_NAME:
        return MaterialPageRoute(
            builder: (_) =>
                AutoCompletePlacePage(routeArgument: args as RouteArgument));
      case SendNameCardPage.ROUTE_NAME:
        return MaterialPageRoute(
          builder: (_) => SendNameCardPage(
            routeArgument: args as RouteArgument,
          ),
        );
        return MaterialPageRoute(
            builder: (_) =>
                AutoCompletePlacePage(routeArgument: args as RouteArgument));
      case CreateConversationPage.ROUTE_NAME:
        return MaterialPageRoute(
            builder: (_) =>
                CreateConversationPage(routeArgument: args as RouteArgument));
      case ImageFileSummaryPage.ROUTE_NAME:
        return MaterialPageRoute(
            builder: (_) =>
                ImageFileSummaryPage(routeArgument: args as RouteArgument));

      // case VideoCallPage.ROUTE_NAME:
      //   return MaterialPageRoute(
      //       builder: (_) =>
      //           VideoCallPage(routeArgument: args as RouteArgument));
      // case VideoCallComingPage.ROUTE_NAME:
      //   return MaterialPageRoute(
      //       builder: (_) =>
      //           VideoCallComingPage(routeArgument: args as RouteArgument));
      case CreateConversationPage.ROUTE_NAME:
        return MaterialPageRoute(
            builder: (_) =>
                CreateConversationPage(routeArgument: args as RouteArgument));
      case ImageFileSummaryPage.ROUTE_NAME:
        return MaterialPageRoute(builder: (_) => ImageFileSummaryPage());
      case ChatGroupMembersPage.ROUTE_NAME:
        return MaterialPageRoute(
            builder: (_) =>
                ChatGroupMembersPage(routeArgument: args as RouteArgument));
      case EditConversationPage.ROUTE_NAME:
        return MaterialPageRoute(
            builder: (_) =>
                EditConversationPage(routeArgument: args as RouteArgument));
      case CreatePollPage.ROUTE_NAME:
        return MaterialPageRoute(
            builder: (_) =>
                CreatePollPage(routeArgument: args as RouteArgument));
      case ListLivestreamPage.ROUTE_NAME:
        return MaterialPageRoute(builder: (_) => ListLivestreamPage());
      case RatingPage.ROUTE_NAME:
        return MaterialPageRoute(
            builder: (_) => RatingPage(argument: args as RouteArgument));
      case ProductByCategoryView.ROUTE_NAME:
        return MaterialPageRoute(
            builder: (_) =>
                ProductByCategoryView(routeArgument: args as RouteArgument));
      case PreviewDocumentPage.ROUTE_NAME:
        return MaterialPageRoute(
            builder: (_) =>
                PreviewDocumentPage(routeArgument: args as RouteArgument));

        return MaterialPageRoute(
            builder: (_) =>
                EditConversationPage(routeArgument: args as RouteArgument));
      case ProfileUserPage.ROUTE_NAME:
        return MaterialPageRoute(
            builder: (_) =>
                ProfileUserPage(routeArgument: args as RouteArgument));
      case CreatePostPageV2.ROUTE_NAME:
        return MaterialPageRoute(
            builder: (_) =>
                CreatePostPageV2(routeArgument: args as RouteArgument));
      default:
        // If there is no such named route in the switch statement, e.g. /third
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                body: SafeArea(
                    child: Text('Route Error: ${settings.name} not found'))));
    }
  }
}
