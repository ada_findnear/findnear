import 'package:flutter/material.dart';

class TouchableOpacity extends StatefulWidget {
  final Widget child;
  final Function onTap;
  final Duration duration = const Duration(milliseconds: 100);
  final double opacity = 0.5;
  final bool isEnable;

  TouchableOpacity({
    @required this.child,
    this.onTap,
    this.isEnable = true,
  });

  @override
  _TouchableOpacityState createState() => _TouchableOpacityState();
}

class _TouchableOpacityState extends State<TouchableOpacity> {
  bool isDown;

  @override
  void initState() {
    super.initState();
    setState(() => isDown = false);
  }

  @override
  Widget build(BuildContext context) {
    return AbsorbPointer(
      absorbing: !widget.isEnable,
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTapDown: (_) => setState(() => isDown = true),
        onTapUp: (_) => setState(() => isDown = false),
        onTapCancel: () => setState(() => isDown = false),
        onTap: widget.onTap,
        child: AnimatedOpacity(
          child: Opacity(
            opacity: widget.isEnable ? 1 : 0.5,
            child: widget.child,
          ),
          duration: widget.duration,
          opacity: isDown ? widget.opacity : 1,
        ),
      ),
    );
  }
}
