import 'dart:io';

import 'package:findnear/src/commons/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LoadingView extends StatelessWidget {
  const LoadingView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (Platform.isIOS || Platform.isMacOS) {
      return CupertinoActivityIndicator();
    }
    return Container(
      color: Colors.transparent,
      width: 20,
      height: 20,
      child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(AppColors.red), strokeWidth: 2,),
    );
  }
}
class AppImage extends StatelessWidget {
  final String path;
  final Color color;
  final BoxFit fit;
  final double width;
  final double height;

  AppImage(this.path, {this.color, this.fit, this.height, this.width});

  @override
  Widget build(BuildContext context) {
    if (path.endsWith('.svg')) {
      return SvgPicture.asset(
        path,
        color: color,
        width: width,
        height: height,
        fit: fit ?? BoxFit.contain,
      );
    }
    return Image.asset(
      path,
      color: color,
      height: height,
      width: width,
      fit: fit ?? BoxFit.fitHeight,
    );
  }
}