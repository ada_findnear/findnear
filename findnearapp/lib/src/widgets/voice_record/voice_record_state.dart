import 'dart:async';

import 'package:get/state_manager.dart';
import 'package:record/record.dart';

import '../../base/base_state.dart';

class VoiceRecordState extends BaseState {
  static final double defaultMaxRippleSize = 160;
  static final double defaultMinRippleSize = 100;

  final audioRecorder = Record();

  Timer timer;
  Timer ampTimer;

  final isRecording = RxBool(false);
  final recordDuration = 0.obs;
  final sizeContainerRipple = RxDouble(defaultMinRippleSize);

  final isDragging = RxBool(false);
  final isLock = RxBool(false);
  final isDraggingIntoDelete = RxBool(false);
  final isDraggingIntoLock = RxBool(false);
}
