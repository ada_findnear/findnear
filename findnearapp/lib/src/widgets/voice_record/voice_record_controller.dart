import 'dart:async';
import 'dart:io';

import 'package:extended_image/extended_image.dart';
import 'package:get_storage/get_storage.dart';

import '../../base/base_controller.dart';
import '../../utils/logger.dart';
import 'voice_record_state.dart';

class VoiceRecordController extends BaseController<VoiceRecordState> {
  @override
  VoiceRecordState createState() => VoiceRecordState();

  Future<void> start(Function() callback) async {
    try {
      if (await state.audioRecorder.hasPermission()) {
        final directory = await getTemporaryDirectory();
        final filePath = directory.path + "/findnear/voice.m4a";
        final file = File(filePath);
        if (await file.existsSync()) await file.deleteSync();
        if (await !file.parent.existsSync()) await file.parent.createSync();

        await state.audioRecorder.start(path: filePath);

        bool isRecording = await state.audioRecorder.isRecording();
        state.isRecording.value = isRecording;

        state.recordDuration.value = 0;
        state.isDragging.value = true;
        state.isLock.value = false;
        state.isDraggingIntoDelete.value = false;
        state.isDraggingIntoLock.value = false;

        _startTimer();
        callback?.call();
      }
    } catch (e) {
      logger.e("VoiceRecordController - start - error: $e");
    }
  }

  Future<String> stop() async {
    state.timer?.cancel();
    state.ampTimer?.cancel();
    final path = await state.audioRecorder.stop();

    state.isRecording.value = false;
    state.isDragging.value = false;
    state.isLock.value = false;
    state.isDraggingIntoDelete.value = false;
    state.isDraggingIntoLock.value = false;
    return path;
  }

  void lock() {
    state.isDragging.value = false;
    state.isLock.value = true;
    state.isDraggingIntoDelete.value = false;
    state.isDraggingIntoLock.value = false;
  }

  void draggingIntoDelete() {
    state.isDraggingIntoDelete.value = true;
    state.isDraggingIntoLock.value = false;
  }

  void draggingIntoLock() {
    state.isDraggingIntoDelete.value = false;
    state.isDraggingIntoLock.value = true;
  }

  void draggedOutTarget() {
    state.isDraggingIntoDelete.value = false;
    state.isDraggingIntoLock.value = false;
  }

  void _startTimer() {
    state.timer?.cancel();
    state.ampTimer?.cancel();

    state.timer = Timer.periodic(const Duration(seconds: 1), (Timer t) {
      state.recordDuration.value += 1;
    });

    state.ampTimer =
        Timer.periodic(const Duration(milliseconds: 100), (Timer t) async {
      final amplitude = await state.audioRecorder.getAmplitude();
      final currentAmplitude = Platform.isIOS ? amplitude.current * 1.5 : amplitude.current;
      final sizeContainerRipple =
          (VoiceRecordState.defaultMaxRippleSize + currentAmplitude).clamp(
        VoiceRecordState.defaultMinRippleSize,
        VoiceRecordState.defaultMaxRippleSize,
      );
      state.sizeContainerRipple.value = sizeContainerRipple;
    });
  }

  @override
  void dispose() async {
    await stop();
    state.audioRecorder.dispose();
    super.dispose();
  }
}
