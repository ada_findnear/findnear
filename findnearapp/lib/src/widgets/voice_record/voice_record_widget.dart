import '../../../generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../base/base_widget_state.dart';
import '../../commons/app_colors.dart';
import '../../extensions/duration_ext.dart';
import '../../pages/voice_filter/widgets/ripple_animation_widget.dart';
import 'voice_record_controller.dart';
import 'voice_record_state.dart';

class VoiceRecordWidget extends StatefulWidget {
  final Function() onStart;
  final Function() onStop;
  final Function(String filePath) onSend;

  const VoiceRecordWidget({
    Key key,
    this.onStart,
    this.onStop,
    this.onSend,
  }) : super(key: key);

  @override
  _VoiceRecordWidgetState createState() => _VoiceRecordWidgetState();
}

class _VoiceRecordWidgetState extends BaseWidgetState<VoiceRecordWidget,
    VoiceRecordController, VoiceRecordState> {
  final double sizeContainerButton = 80;
  final double sizeRecordIcon = 36;
  final double sizeRecordingIcon = 44;
  final double sizeOtherIcon = 36;

  @override
  void dispose() {
    Get.delete<VoiceRecordController>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Obx(() => Stack(
            alignment: Alignment.center,
            children: [
              // Record Widget
              Container(
                margin: EdgeInsets.only(top: 48),
                child: _buildRecordWidget(),
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  // Top Info Widget
                  Container(
                    alignment: Alignment.bottomCenter,
                    height: 48,
                    child: _buildTopWidget(),
                  ),
                  SizedBox(
                    height: 18,
                  ),
                  // Delete and Lock Widget
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 28),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        _buildDragTarget(
                          builderWidget: _buildDeleteIconWidget(),
                          onWillAccept: controller.draggingIntoDelete,
                          onAccept: controller.stop,
                          onLeave: controller.draggedOutTarget,
                        ),
                        _buildDragTarget(
                          builderWidget: _buildLockIconWidget(),
                          onWillAccept: controller.draggingIntoLock,
                          onAccept: controller.lock,
                          onLeave: controller.draggedOutTarget,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 18,
                  ),
                ],
              ),
            ],
          )),
    );
  }

  Widget _buildRecordWidget() {
    // build normal record widget
    final iconRecordWidget = Container(
      width: sizeContainerButton,
      height: sizeContainerButton,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(100),
        ),
        border: Border.all(
          color: state.isDraggingIntoDelete.value ? Colors.red : Colors.blue,
        ),
        color: AppColors.grayBg,
      ),
      child: _buildIconRecord(),
    );

    // build animation record widget if recording
    Widget recordWidget = iconRecordWidget;
    if (state.isRecording.value) {
      recordWidget = AnimatedContainer(
        duration: Duration(milliseconds: 100),
        width: state.sizeContainerRipple.value,
        height: state.sizeContainerRipple.value,
        child: Center(
          child: RipplesAnimation(
            color: state.isDraggingIntoDelete.value ? Colors.red : Colors.blue,
            child: iconRecordWidget,
          ),
        ),
      );
    }

    if (state.isLock.value) {
      return GestureDetector(
        onTap: () async {
          final filePath = await controller.stop();
          widget.onStop?.call();
          widget.onSend?.call(filePath);
        },
        child: recordWidget,
      );
    } else {
      return LongPressDraggable(
        data: true,
        child: recordWidget,
        feedback: Container(),
        onDragStarted: () async {
          controller.start(widget.onStart);
        },
        onDragEnd: (details) async {
          if (!state.isLock.value && !state.isDraggingIntoDelete.value) {
            final filePath = await controller.stop();
            widget.onStop?.call();
            widget.onSend?.call(filePath);
          }
        },
      );
    }
  }

  Widget _buildIconRecord() {
    if (state.isDraggingIntoDelete.value) {
      return Icon(
        Icons.delete,
        color: Colors.red,
        size: sizeRecordingIcon,
      );
    }

    if (state.isDraggingIntoLock.value) {
      return Icon(
        Icons.lock,
        color: Colors.blue,
        size: sizeRecordingIcon,
      );
    }

    if (state.isRecording.value) {
      return Icon(
        state.isLock.value ? Icons.send : Icons.mic,
        color: Colors.blue,
        size: sizeRecordingIcon,
      );
    } else {
      return Icon(
        Icons.mic,
        color: Colors.blue,
        size: sizeRecordIcon,
      );
    }
  }

  Widget _buildTopWidget() {
    final context = Get.context;
    if (!state.isRecording.value) {
      return Text(S.of(context).hold_to_record);
    } else {
      if (state.isLock.value) {
        return _buildTopTimeWidget(S.of(context).hand_free_recording_mode);
      } else {
        if (state.isDragging.value &&
            !state.isDraggingIntoDelete.value &&
            !state.isDraggingIntoLock.value) {
          return _buildTopTimeWidget(S.of(context).release_to_send_voice);
        } else if (state.isDragging.value && state.isDraggingIntoDelete.value) {
          return Text(
            S.of(context).release_to_cancel,
            style: TextStyle(
              color: Theme.of(context).colorScheme.secondary,
              fontSize: 18,
            ),
          );
        } else {
          return Text(
            S.of(context).release_to_hand_free_mode,
            style: TextStyle(
              color: Colors.blue,
              fontSize: 18,
            ),
          );
        }
      }
    }
  }

  Widget _buildTopTimeWidget(String text) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Text(
          "${Duration(seconds: state.recordDuration.value).toFormatString()}",
          style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: 4,
        ),
        Text(text),
      ],
    );
  }

  Widget _buildDragTarget({
    Widget builderWidget,
    Function onWillAccept,
    Function onAccept,
    Function onLeave,
  }) {
    return DragTarget(
      builder: (context, candidateData, rejectedData) {
        return builderWidget;
      },
      onWillAccept: (data) {
        onWillAccept();
        return true;
      },
      onAccept: (data) {
        onAccept();
        widget.onStop?.call();
      },
      onLeave: (data) {
        onLeave();
      },
    );
  }

  Widget _buildDeleteIconWidget() {
    if (!state.isRecording.value) {
      return Container(
        width: sizeContainerButton,
        height: sizeContainerButton,
      );
    } else {
      if (state.isLock.value) {
        return GestureDetector(
          onTap: () {
            controller.stop();
            widget.onStop?.call();
          },
          child: Container(
            width: sizeContainerButton,
            height: sizeContainerButton,
            child: Icon(
              Icons.delete,
              color: Colors.red,
              size: sizeOtherIcon,
            ),
          ),
        );
      } else {
        return Container(
          width: sizeContainerButton,
          height: sizeContainerButton,
          child: !state.isDraggingIntoDelete.value
              ? Icon(
                  Icons.delete_outline,
                  color: Colors.grey,
                  size: sizeOtherIcon,
                )
              : Icon(
                  Icons.delete,
                  color: Colors.red,
                  size: sizeOtherIcon,
                ),
        );
      }
    }
  }

  Widget _buildLockIconWidget() {
    final emptyContainer = Container(
      width: sizeContainerButton,
      height: sizeContainerButton,
    );
    if (!state.isRecording.value) {
      return emptyContainer;
    } else {
      if (state.isLock.value) {
        return emptyContainer;
      } else {
        return Container(
          width: sizeContainerButton,
          height: sizeContainerButton,
          child: !state.isDraggingIntoLock.value
              ? Icon(
                  Icons.lock_outline,
                  color: Colors.grey,
                  size: sizeOtherIcon,
                )
              : Icon(
                  Icons.lock,
                  color: Colors.blue,
                  size: sizeOtherIcon,
                ),
        );
      }
    }
  }

  @override
  VoiceRecordController createController() => VoiceRecordController();
}
