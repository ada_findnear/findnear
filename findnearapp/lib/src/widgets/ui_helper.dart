import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';

class UIHelper {
  static Widget showImage(String imgUrl) {
    return imgUrl.contains('http')
        ? Container(
            decoration: BoxDecoration(
              // borderRadius: BorderRadius.circular(10.0),
              image: DecorationImage(
                image: CachedNetworkImageProvider(imgUrl),
                fit: BoxFit.cover,
              ),
            ),
          )
        : Image.asset(
            imgUrl,
            fit: BoxFit.contain,
          );
  }
}
