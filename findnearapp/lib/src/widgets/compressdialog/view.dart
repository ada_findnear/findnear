import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/widgets/compressdialog/controller.dart';
import 'package:findnear/src/widgets/compressdialog/state.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:video_compress/video_compress.dart';

class CompressVideoDialog extends StatefulWidget {

  final String path;
  final Function onCompressDone;

  CompressVideoDialog({Key key, this.path, this.onCompressDone}):super(key:key);

  @override
  _CompressVideoDialogState createState() => _CompressVideoDialogState();
}

class _CompressVideoDialogState extends BaseWidgetState<CompressVideoDialog,
    CompressVideoDialogController, CompressVideoDialogState> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(),
    );
  }

  Widget contentBox() {
    return Obx((){
      return Stack(
        children: [
          Container(
            padding: EdgeInsets.only(left: 20,top: 50, right: 20,bottom: 20),
            margin: EdgeInsets.only(top: 30),
            decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(color: Colors.black,offset: Offset(0,10),
                      blurRadius: 10
                  ),
                ]
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(S.of(context).video_should_be_less_than_10MB ,style: TextStyle(fontSize: 14,fontWeight: FontWeight.w600),),
                SizedBox(height: 15,),
                Visibility(
                  visible: state.isCompressing.value,
                  child: Container(
                    height: 6,
                    child: LinearProgressIndicator(
                      value: state.percent.value,
                      backgroundColor: Colors.cyan[100],
                      valueColor: new AlwaysStoppedAnimation<Color>(Colors.green),
                    ),
                  ),
                ),
                SizedBox(height: 22,),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    FlatButton(
                        onPressed: (){
                          if(state.isCompressing.value == true){
                            controller.cancelCompressingVideo();
                          }
                          Navigator.of(context).pop();
                        },
                        child: Text(S.of(context).cancel,style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),)),
                    Visibility(
                      visible: !state.isCompressing.value,
                      child: FlatButton(
                          onPressed: (){
                            state.isCompressing.value = true;
                            controller.compressingVideo(widget.path, (){
                              widget.onCompressDone(state.compressedVideoInfo.value);
                              Navigator.of(context).pop();
                            });
                          },
                          child: Text(S.of(context).ok,style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),)),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Positioned(
            left: 20,
            right: 20,
            child: CircleAvatar(
              backgroundColor: Colors.white,
              radius: 30,
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(30)),
                child: Image.asset(AppImages.icLogo),
              ),
            ),
          ),
        ],
      );
    });
  }

  @override
  CompressVideoDialogController createController() =>
      CompressVideoDialogController();
}
