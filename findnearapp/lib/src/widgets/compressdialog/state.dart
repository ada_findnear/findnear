import 'package:findnear/src/base/base_state.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:video_compress/video_compress.dart';

class CompressVideoDialogState extends BaseState{
  final isCompressing  = false.obs;
  final RxDouble percent = 0.0.obs;

  Rx<MediaInfo> compressedVideoInfo = MediaInfo(path: '').obs;
}