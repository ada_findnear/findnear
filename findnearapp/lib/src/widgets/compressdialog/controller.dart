import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/widgets/compressdialog/state.dart';
import 'package:video_compress/video_compress.dart';

class CompressVideoDialogController
    extends BaseController<CompressVideoDialogState> {
  Subscription _subscription;

  @override
  CompressVideoDialogState createState() => CompressVideoDialogState();

  void compressingVideo(String path,Function onDone) async {
    _subscription = VideoCompress.compressProgress$.subscribe((progress) {
      state.percent.value =  progress/100;
    });

    await VideoCompress.setLogLevel(0);

    final MediaInfo info = await VideoCompress.compressVideo(
      path,
      quality: VideoQuality.HighestQuality,
      deleteOrigin: false,
      includeAudio: true,
      frameRate: 60
    );
    _subscription.unsubscribe?.call();
    state.compressedVideoInfo.value = info;
    onDone();

  }

  void cancelCompressingVideo() {
    _subscription.unsubscribe?.call();
    VideoCompress.cancelCompression();
  }
}
