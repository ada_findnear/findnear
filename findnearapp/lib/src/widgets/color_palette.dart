import 'package:flutter/material.dart';

class ColorPaletteWidget extends StatelessWidget {
  final List<Color> colors;
  final Color selectedColor;
  final ValueSetter<Color> onColorSelected;

  const ColorPaletteWidget({
    Key key,
    this.colors,
    this.selectedColor,
    this.onColorSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: colors.length,
        itemBuilder: (context, index) => GestureDetector(
          onTap: () => onColorSelected?.call(colors[index]),
          child: Container(
            width: 40,
            height: double.infinity,
            alignment: Alignment.center,
            color: colors[index],
            child: Container(
              width: 10,
              height: 10,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
                color: _getSelectedItemColor(index),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Color _getSelectedItemColor(int index) {
    final currentColor = colors[index];
    if (selectedColor == currentColor) {
      return selectedColor == Colors.white ? Colors.black : Colors.white;
    } else {
      return Colors.transparent;
    }
  }
}
