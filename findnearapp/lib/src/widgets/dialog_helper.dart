import 'dart:io';

import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/elements/DashLineWidget.dart';
import 'package:findnear/src/models/bottom_sheet_option.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/pages/dialog_report/dialog_report_logic.dart';
import 'package:findnear/src/pages/dialog_report/dialog_report_view.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:package_info/package_info.dart';
import 'package:url_launcher/url_launcher.dart';

import 'common.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DialogHelper {
  static final fToast = FToast();

  static void initFToast({BuildContext context}) {
    fToast.init(context ?? Get.context);
  }

  static void showToast({String icon, String message}) {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 22.0, vertical: 8.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5.0),
        color: AppColors.pink,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          if (icon!=null) AppImage(icon, color: AppColors.red),
          if (icon!=null) const SizedBox(width: 23.5),
          Text(
            message,
            style: Theme.of(fToast.context)
                .textTheme
                .bodyText2
                .copyWith(color: AppColors.red),
          ),
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.BOTTOM,
      toastDuration: Duration(seconds: 2),
    );
  }

  static showAlertDialog(
      {BuildContext context,
      String title,
      String description,
      String okText,
      Function okFunction}) {
    Widget okButton = TextButton(
      child: Text(okText),
      onPressed: () {
        Navigator.pop(context);
        okFunction();
      },
    );
    AlertDialog alert = AlertDialog(
      title: Text(title),
      content: Text(description),
      actions: [
        okButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  static void showLoaderDialog() {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          SizedBox(width: 10),
          Text('Vui lòng đợi...')
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: Get.context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  static Future<T> showLogoutPopup<T>() {
    return showDialog(
        context: Get.context,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.symmetric(horizontal: 16),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(15))),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 22),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 16),
                  Text(
                    'Đăng xuất tài khoản',
                    style: TextStyle(
                      fontFamily: 'Quicksand',
                      fontSize: 18,
                      color: const Color(0xff333333),
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  SizedBox(height: 16),
                  Text(
                    'Bạn chắn chắn muốn thoát khỏi ứng dụng?',
                    style: TextStyle(
                      fontFamily: 'Quicksand',
                      fontSize: 14,
                      color: const Color(0xff333333),
                    ),
                  ),
                  SizedBox(height: 5),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: OutlinedButton(
                          style: OutlinedButton.styleFrom(
                            primary: const Color(0xff333333),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18)),
                          ),
                          onPressed: () {
                            Get.back();
                          },
                          child: const Text(
                            'Hủy',
                            style: TextStyle(
                              fontFamily: 'Quicksand',
                              fontSize: 14,
                              color: const Color(0xff333333),
                              //fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 16),
                      Expanded(
                        child: ElevatedButton(
                          onPressed: () => Get.back(result: true),
                          child: Text(
                            'Đăng xuất',
                            style: TextStyle(
                              fontSize: 14,
                              color: const Color(0xffffffff),
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          style: ElevatedButton.styleFrom(
                            shape: StadiumBorder(),
                            primary: Color(0xffFF0000),
                            elevation: 0,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              width: 314,
              height: 161,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: const Color(0xffffffff),
                boxShadow: [
                  BoxShadow(
                    color: const Color(0x29000000),
                    offset: Offset(0, 0),
                    blurRadius: 6,
                  ),
                ],
              ),
            ),
          );
        });
  }

  static Future<T> showConfirmPopup<T>({
    String title = "",
    String description = "",
    String okBtnText = "OK",
    Function okBtnPressed,
    String cancelText,
    Function cancelBtnPressed,
  }) {
    final finalCancelText = cancelText != null ? cancelText : S.current.cancel;
    return showDialog(
        context: Get.context,
        builder: (context) => Dialog(
              insetPadding: EdgeInsets.symmetric(horizontal: 16),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15))),
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 22),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: 16),
                    Text(
                      title,
                      style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 18,
                        color: const Color(0xff333333),
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    SizedBox(height: 16),
                    Text(
                      description,
                      style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 14,
                        color: const Color(0xff333333),
                      ),
                    ),
                    SizedBox(height: 5),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          child: OutlinedButton(
                            style: OutlinedButton.styleFrom(
                              primary: const Color(0xff333333),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18)),
                            ),
                            onPressed: () {
                              cancelBtnPressed?.call();
                              Get.back();
                            },
                            child: Text(
                              finalCancelText,
                              style: TextStyle(
                                fontFamily: 'Quicksand',
                                fontSize: 14,
                                color: const Color(0xff333333),
                                //fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(width: 16),
                        Expanded(
                          child: ElevatedButton(
                            onPressed: () {
                              okBtnPressed?.call();
                              Get.back();
                            },
                            child: Text(
                              okBtnText,
                              style: TextStyle(
                                fontSize: 14,
                                color: const Color(0xffffffff),
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            style: ElevatedButton.styleFrom(
                              shape: StadiumBorder(),
                              primary: Color(0xffFF0000),
                              elevation: 0,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                width: 314,
                height: 161,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20.0),
                  color: const Color(0xffffffff),
                  boxShadow: [
                    BoxShadow(
                      color: const Color(0x29000000),
                      offset: Offset(0, 0),
                      blurRadius: 6,
                    ),
                  ],
                ),
              ),
            ));
  }

  static Future<T> showLocationSharingPopup<T>({
    String title = "",
    String okBtnText = "OK",
  }) {
    final finalCancelText = S.current.cancel;
    final whiteBtnStyle = OutlinedButton.styleFrom(
      primary: const Color(0xff333333),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18)),
    );
    final whiteBtnTextStyle = TextStyle(
      fontFamily: 'Quicksand',
      fontSize: 14,
      color: const Color(0xff333333),
    );
    final redBtnStyle = ElevatedButton.styleFrom(
      shape: StadiumBorder(),
      primary: Color(0xffFF0000),
      elevation: 0,
    );
    final redBtnTextStyle = TextStyle(
      fontSize: 14,
      color: const Color(0xffffffff),
      fontWeight: FontWeight.w700,
    );
    return showDialog(
        context: Get.context,
        builder: (context) => Dialog(
          insetPadding: EdgeInsets.symmetric(horizontal: 16),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15))),
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 22,vertical: 24),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 16),
                Text(
                  title,
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 18,
                    color: const Color(0xff333333),
                    fontWeight: FontWeight.w700,
                  ),
                ),
                SizedBox(height: 16),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                          child: OutlinedButton(
                            style: whiteBtnStyle,
                            onPressed: () => Get.back(result: false),
                            child: Text(finalCancelText, style: whiteBtnTextStyle),
                          ),
                        ),
                        SizedBox(width: 16),
                        Expanded(
                          child: ElevatedButton(
                            style: redBtnStyle,
                            onPressed: () => Get.back(result: true),
                            child: Text(okBtnText, style: redBtnTextStyle),
                          ),
                        ),
                  ],
                ),
              ],
            ),
            width: 314,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
              color: const Color(0xffffffff),
              boxShadow: [
                BoxShadow(
                  color: const Color(0x29000000),
                  offset: Offset(0, 0),
                  blurRadius: 6,
                ),
              ],
            ),
          ),
        ));
  }

  static Future<dynamic> showForceUpdatePopup() async {
    String title = S.current.caution;
    String description = S.current.forceUpdateDescription;
    String visitStore = S.current.updateNow;
    return showDialog(
        context: Get.context,
        barrierDismissible: false,
        builder: (context) => WillPopScope(
              onWillPop: () async {
                return false;
              },
              child: Dialog(
                insetPadding: EdgeInsets.symmetric(horizontal: 16),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(15))),
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 22),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const SizedBox(height: 16),
                      Text(
                        title,
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          fontSize: 18,
                          color: const Color(0xff333333),
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      const SizedBox(height: 16),
                      Text(
                        description,
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          fontSize: 14,
                          color: const Color(0xff333333),
                        ),
                      ),
                      const SizedBox(height: 5),
                      ElevatedButton(
                        onPressed: () async {
                          final PackageInfo packageInfo =
                              await PackageInfo.fromPlatform();
                          final appPackageName = packageInfo.packageName;
                          try {
                            final url = Platform.isIOS
                                ? "market://details?id=$appPackageName"
                                : "https://play.google.com/store/apps/details?id=$appPackageName";
                            logger.d("launch url = $url");
                            if (await canLaunch(url)) {
                              await launch(url);
                            }
                          } catch (e) {
                            logger.e(e);
                          }
                        },
                        child: Text(
                          visitStore,
                          style: const TextStyle(
                            fontSize: 14,
                            color: const Color(0xffffffff),
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                          shape: StadiumBorder(),
                          primary: Color(0xffFF0000),
                          elevation: 0,
                        ),
                      ),
                      const SizedBox(height: 12),
                    ],
                  ),
                  width: 314,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0),
                    color: const Color(0xffffffff),
                    boxShadow: [
                      const BoxShadow(
                        color: Color(0x29000000),
                        offset: Offset(0, 0),
                        blurRadius: 6,
                      ),
                    ],
                  ),
                ),
              ),
            ));
  }

  static Future<T> showBottomSheetOptions<T>({
    @required BuildContext context,
    List<BottomSheetOption> options,
    Function(int) onTapAtIndex,
  }) async {
    return await showModalBottomSheet(
      context: context,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
      builder: (BuildContext context) => Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Align(
            alignment: Alignment.centerRight,
            child: GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Container(
                padding: const EdgeInsets.only(left: 10.0, top: 10, right: 10),
                margin: const EdgeInsets.only(right: 3),
                child: AppImage(AppImages.icCloseSvg),
              ),
            ),
          ),
          ListView.separated(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) => InkWell(
              onTap: () {
                if(!options[index].isReported) {
                  onTapAtIndex?.call(index);
                  Navigator.of(context).pop(index);
                } else {
                  Get.snackbar(
                    "Thông báo",
                    "Bạn đã báo cáo vi phạm bài viết rồi",
                    backgroundColor: Colors.red,
                    colorText: Colors.white,
                  );
                }
              },
              child: Container(
                color: (!options[index].isReported)? AppColors.white: AppColors.red.withOpacity(0.1),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 15, horizontal: 28),
                  child: Row(
                    children: [
                      if (options[index].icon != null)
                        Icon(options[index].icon, size: 24.w),
                      if (options[index].iconPath.isNotEmpty)
                        AppImage(options[index].iconPath, width: 24.w),
                      const SizedBox(width: 12),
                      Expanded(
                        child: Text(
                          options[index].text ?? "",
                          style: Theme.of(context).textTheme.bodyText1.copyWith(
                                color: AppColors.textBlack,),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            separatorBuilder: (context, index) => Padding(
              padding: const EdgeInsets.symmetric(horizontal: 17.5),
              child: const DashLineWidget(),
            ),
            itemCount: options.length,
          ),
        ],
      ),
    );
  }

  static void showError(String msg) {
    AppSnackbar.showError(message: msg);
  }

  static void showLoadingDialog({String message, bool dismissible = false}) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          SizedBox(width: 10.w),
          Text(message ?? S.of(Get.context).loading_dialog_message)
        ],
      ),
    );
    showDialog(
      barrierDismissible: dismissible,
      context: Get.context,
      builder: (BuildContext context) {
        return WillPopScope(
            child: alert,
            onWillPop: () {
              return Future.value(dismissible);
            });
      },
    );
  }

  static void showFindLoveLoadingDialog({String message, bool dismissible = false}) {
    showDialog(
      useSafeArea: false,
      barrierDismissible: dismissible,
      context: Get.context,
      builder: (BuildContext context) => AppImage(
        AppImages.loadingFindLove,
        fit: BoxFit.cover,
      ),
    );
  }

  static void showFindJobLoadingDialog({String message, bool dismissible = false}) {
    showDialog(
      useSafeArea: false,
      barrierDismissible: dismissible,
      context: Get.context,
      builder: (BuildContext context) => AppImage(
        AppImages.icLoadingFindJob,
        fit: BoxFit.cover,
      ),
    );
  }

  static Future<String> showReportPopup(Post post) async{
    return await showDialog(
        context: Get.context,
        builder: (context) => DialogReportWidget(post: post),
    );
  }
}
