import 'package:flutter/material.dart';

class ThumbCircleSliderComponentShape extends SliderComponentShape {
  final double thumbRadius;
  final Color backgroundColor;
  final Color foregroundColor;

  const ThumbCircleSliderComponentShape({
    @required this.thumbRadius,
    @required this.backgroundColor,
    @required this.foregroundColor,
  });

  @override
  Size getPreferredSize(bool isEnabled, bool isDiscrete) {
    return Size.fromRadius(thumbRadius);
  }

  @override
  void paint(
    PaintingContext context,
    Offset center, {
    Animation<double> activationAnimation,
    Animation<double> enableAnimation,
    bool isDiscrete,
    TextPainter labelPainter,
    RenderBox parentBox,
    SliderThemeData sliderTheme,
    TextDirection textDirection,
    double value,
    double textScaleFactor,
    Size sizeWithOverflow,
  }) {
    final Canvas canvas = context.canvas;

    // draw shadow
    final shadowPaint = Paint()
      ..color = Colors.black.withOpacity(0.3)
      ..maskFilter = MaskFilter.blur(BlurStyle.normal, 4);
    Path oval = Path()
      ..addOval(Rect.fromCircle(center: center, radius: thumbRadius));
    canvas.drawPath(oval, shadowPaint);

    // draw background circle
    final backgroundPaint = Paint()..color = backgroundColor;
    canvas.drawCircle(center, thumbRadius, backgroundPaint);

    // draw foreground circle
    final foregroundPaint = Paint()..color = foregroundColor;
    canvas.drawCircle(center, thumbRadius - (thumbRadius / 4), foregroundPaint);
  }
}
