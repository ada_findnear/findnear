import 'package:flutter/material.dart';

import 'background_slider_painter.dart';
import 'thumb_circle_slider_component_shape.dart';

class BrushSliderWidget extends StatefulWidget {
  final double value;
  final double min;
  final double max;
  final Color backgroundThumbColor;
  final Color foregroundThumbColor;
  final Color backgroundColor;
  final ValueChanged<double> onChanged;

  BrushSliderWidget({
    this.value = 5,
    this.max = 10,
    this.min = 0,
    this.backgroundThumbColor,
    this.foregroundThumbColor,
    this.backgroundColor,
    this.onChanged,
  });

  @override
  _BrushSliderWidgetState createState() => _BrushSliderWidgetState();
}

class _BrushSliderWidgetState extends State<BrushSliderWidget> {
  final double sliderHeight = 48;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          width: double.infinity,
          height: sliderHeight,
          child: Stack(
            children: <Widget>[
              // background
              Container(
                width: double.infinity,
                height: double.infinity,
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: CustomPaint(
                  painter: BackgroundSliderPainter(
                    color: widget.backgroundColor,
                  ),
                ),
              ),

              // slider
              Center(
                child: RotationTransition(
                  turns: AlwaysStoppedAnimation(-0.003),
                  child: SliderTheme(
                    data: SliderTheme.of(context).copyWith(
                      activeTrackColor: Colors.transparent,
                      inactiveTrackColor: Colors.transparent,
                      trackHeight: 4.0,
                      thumbShape: ThumbCircleSliderComponentShape(
                        thumbRadius: 16,
                        backgroundColor: widget.backgroundThumbColor,
                        foregroundColor: widget.foregroundThumbColor,
                      ),
                      overlayColor: Colors.transparent,
                      activeTickMarkColor: Colors.transparent,
                      inactiveTickMarkColor: Colors.transparent,
                    ),
                    child: Slider(
                      min: widget.min,
                      max: widget.max,
                      value: widget.value,
                      onChanged: widget.onChanged,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
