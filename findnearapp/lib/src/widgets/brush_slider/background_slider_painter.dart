import 'package:flutter/material.dart';

class BackgroundSliderPainter extends CustomPainter {
  Color color;
  BackgroundSliderPainter({
    Listenable repaint,
    this.color,
  }) : super(repaint: repaint);

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()..color = this.color ?? Colors.white;

    final path = Path();
    path.moveTo(0, size.height / 3 * 2);
    path.lineTo(size.width, size.height / 3 * 2);
    path.lineTo(size.width, size.height / 4);
    path.lineTo(0, size.height / 3 * 2 - 2);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(BackgroundSliderPainter oldDelegate) => false;
}
