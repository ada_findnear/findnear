import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../common.dart';

class GameRuleDialog extends StatelessWidget {
  final String htmlContent;

  GameRuleDialog(this.htmlContent);

  static Future<bool> show(String htmlContent, BuildContext context) {
    return showDialog(
        barrierDismissible: true,
        context: context,
        builder: (context) => GameRuleDialog(htmlContent));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
        child: Container(
          height: 460.w,
          child: _buildRuleBoard(context),
        ),
      ),
    );
  }

  Widget _buildRuleBoard(BuildContext context) {
    return _buildGoldContainer(context,
        title: AppImage('assets/svg/img_rule.svg'),
        child: SingleChildScrollView(
          child: Html(data: htmlContent),
        ));
  }

  Widget _buildGoldContainer(BuildContext context,
      {@required Widget child, @required Widget title}) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          margin: EdgeInsets.only(left: 20.w, right: 20.w, top: 20.w),
          child: Container(
            alignment: Alignment.topCenter,
            padding: EdgeInsets.only(
                top: 30.w, bottom: 70.w, left: 10.w, right: 10.w),
            child: child,
            margin: EdgeInsets.all(3),
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              color: const Color(0xffffffff),
            ),
          ),
          width: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            gradient: LinearGradient(
              begin: Alignment(0.0, -1.0),
              end: Alignment(0.0, 1.0),
              colors: [
                const Color(0xfff7aa00),
                const Color(0xfff9db00),
                const Color(0xffed9700),
                const Color(0xfff7aa00)
              ],
              stops: [0.0, 0.352, 0.727, 1.0],
            ),
          ),
        ),
        Align(
          child: title,
          alignment: Alignment.topCenter,
        ),
        Positioned(
          bottom: 16.w,
          child: Row(
            children: [
              ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
                child: Text(
                  'Không chơi',
                  style: TextStyle(
                    fontSize: 14.sp,
                    color: Colors.red,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                style: ElevatedButton.styleFrom(
                    shape: StadiumBorder(), primary: Colors.white),
              ),
              SizedBox(width: 24.w),
              ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
                child: Text(
                  'Chơi luôn',
                  style: TextStyle(
                    fontSize: 14.sp,
                    color: const Color(0xffffffff),
                    fontWeight: FontWeight.w700,
                  ),
                ),
                style: ElevatedButton.styleFrom(
                    shape: StadiumBorder(), primary: Colors.red),
              )
            ],
          ),
        )
      ],
    );
  }
}
