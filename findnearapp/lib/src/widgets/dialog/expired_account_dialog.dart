import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:findnear/src/repository/settings_repository.dart' as settingRepo;

Dialog expiredAccountDialog({String expiredDate}) {
  return Dialog(
    backgroundColor: Colors.transparent,
    insetPadding: EdgeInsets.zero,
    child: Column(
      children: [
        Spacer(),
        Stack(
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(20, 40, 20, 20),
              margin: EdgeInsets.fromLTRB(30, 25, 30, 0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Column(
                children: <Widget>[
                  Text(
                    'Tài khoản sắp hết hạn',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      color: AppColors.black,
                    ),
                  ),
                  SizedBox(height: 10),
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: 'Tài khoản của bạn sẽ hết hạn vào ngày',
                          style: TextStyle(fontSize: 14, color: AppColors.black, fontWeight: FontWeight.w300),
                        ),
                        TextSpan(
                          text: ' $expiredDate',
                          style:
                              TextStyle(fontSize: 14, color: AppColors.black, fontWeight: FontWeight.bold, height: 1.4),
                        ),
                        TextSpan(
                          text: '. Vui lòng liên hệ hotline ',
                          style:
                              TextStyle(fontSize: 14, color: AppColors.black, fontWeight: FontWeight.w300, height: 1.4),
                        ),
                        TextSpan(
                          text: '${settingRepo.setting.value.hotLine1 ?? '0963.125.582'}',
                          style:
                              TextStyle(fontSize: 14, color: AppColors.red, fontWeight: FontWeight.bold, height: 1.4),
                        ),
                        TextSpan(
                          text: ' hoặc ',
                          style:
                              TextStyle(fontSize: 14, color: AppColors.black, fontWeight: FontWeight.w300, height: 1.4),
                        ),
                        TextSpan(
                          text: '${settingRepo.setting.value.hotLine2 ?? '0983.421.693'} \n',
                          style: TextStyle(fontSize: 14, color: AppColors.red, fontWeight: FontWeight.bold),
                        ),
                        TextSpan(
                          text: 'để gia hạn tài khoản',
                          style:
                              TextStyle(fontSize: 14, color: AppColors.black, fontWeight: FontWeight.w300, height: 1.4),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Positioned.fill(
              top: 0,
              child: Align(
                alignment: Alignment.topCenter,
                child: AppImage(AppImages.icMail),
              ),
            ),
            Positioned(
              top: 30,
              right: 30,
              child: InkWell(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: AppImage('assets/img/ic_close.png'),
                ),
                onTap: () => Get.back(),
              ),
            )
          ],
        ),
        Spacer(),
      ],
    ),
  );
}

Dialog contactUsDialog() {
  return Dialog(
    backgroundColor: Colors.transparent,
    insetPadding: EdgeInsets.zero,
    child: Column(
      children: [
        Spacer(),
        Stack(
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(20, 40, 20, 20),
              margin: EdgeInsets.fromLTRB(30, 25, 30, 0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Column(
                children: <Widget>[
                  Text(
                    'Cửa hàng không khả dụng',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      color: AppColors.black,
                    ),
                  ),
                  SizedBox(height: 10),
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: 'Vui lòng liên hệ bộ phận hỗ trợ khách hàng của FindNear tại Hotline: ',
                          style:
                          TextStyle(fontSize: 14, color: AppColors.black, fontWeight: FontWeight.w300, height: 1.4),
                        ),
                        TextSpan(
                          text: '${settingRepo.setting.value.hotLine1 ?? '0985725511'}',
                          style:
                          TextStyle(fontSize: 14, color: AppColors.red, fontWeight: FontWeight.bold, height: 1.4),
                        ),
                        TextSpan(
                          text: ' để được hỗ trợ đăng ký gian hàng.',
                          style:
                          TextStyle(fontSize: 14, color: AppColors.black, fontWeight: FontWeight.w300, height: 1.4),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Positioned.fill(
              top: 0,
              child: Align(
                alignment: Alignment.topCenter,
                child: AppImage(AppImages.icMail),
              ),
            ),
            Positioned(
              top: 30,
              right: 30,
              child: InkWell(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: AppImage('assets/img/ic_close.png'),
                ),
                onTap: () => Get.back(),
              ),
            )
          ],
        ),
        Spacer(),
      ],
    ),
  );
}
