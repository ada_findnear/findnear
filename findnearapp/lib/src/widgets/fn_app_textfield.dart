import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class FnAppTextField extends StatelessWidget {
  final Function(String) onChange;
  final Function(String) validator;
  final String labelText;
  final String hintText;
  final Icon prefixIcon;
  final Widget suffixIcon;
  final TextInputType inputType;
  final bool obscureText;
  final int maxLength;

  const FnAppTextField({
    Key key,
    this.onChange,
    this.validator,
    this.labelText,
    this.hintText,
    this.prefixIcon,
    this.obscureText = false,
    this.inputType,
    this.suffixIcon,
    this.maxLength,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: inputType ?? TextInputType.text,
      onChanged: onChange,
      validator: validator,
      obscureText: obscureText,
      maxLength: maxLength,
      inputFormatters: [new LengthLimitingTextInputFormatter(maxLength)],
      maxLengthEnforced: true,
      decoration: InputDecoration(
        labelText: labelText,
        labelStyle: TextStyle(color: Theme.of(context).accentColor),
        contentPadding: EdgeInsets.all(12),
        hintText: hintText,
        hintStyle:
            TextStyle(color: Theme.of(context).focusColor.withOpacity(0.7)),
        prefixIcon: prefixIcon,
        suffixIcon: suffixIcon,
        border: OutlineInputBorder(
            borderSide: BorderSide(
                color: Theme.of(context).focusColor.withOpacity(0.2))),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
                color: Theme.of(context).focusColor.withOpacity(0.5))),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
                color: Theme.of(context).focusColor.withOpacity(0.2))),
      ),
    );
  }
}
