import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/widgets/shimmer/app_shimmer.dart';
import 'package:flutter/material.dart';

class ListLoadingShimmer extends StatelessWidget {
  const ListLoadingShimmer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        return Container(
          color: Colors.white, // #
          padding: EdgeInsets.fromLTRB(12, 10, 12, 0),
          // height: 70,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  AppShimmer(
                    width: 38,
                    height: 38,
                    cornerRadius: 19,
                  ),
                  SizedBox(width: 12),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        AppShimmer(
                          width: double.infinity,
                          height: 18,
                          cornerRadius: 9,
                        ),
                        SizedBox(height: 4),
                        AppShimmer(
                          width: 250,
                          height: 18,
                          cornerRadius: 9,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10),
              Container(
                margin: EdgeInsets.only(left: 50),
                height: 0.5,
                color: AppColors.grayLight.withOpacity(0.5),
              ),
            ],
          ),
        );
      },
      itemCount: 20,
    );
  }
}
