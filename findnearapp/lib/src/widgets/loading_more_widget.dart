import 'package:findnear/src/commons/app_colors.dart';
import 'package:flutter/material.dart';

class LoadingMoreWidget extends StatelessWidget {
  final double height;
  final Color color;
  final double loadingSize;
  final double strokeWidth;

  LoadingMoreWidget({
    this.height = 80,
    this.color = Colors.white,
    this.loadingSize = 24,
    this.strokeWidth = 4,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      child: Container(
        alignment: Alignment.center,
        child: Container(
          width: loadingSize,
          height: loadingSize,
          child: CircularProgressIndicator(
            backgroundColor: color,
            valueColor: AlwaysStoppedAnimation<Color>(AppColors.red),
            strokeWidth: strokeWidth ?? 4,
          ),
        ),
      ),
    );
  }
}
