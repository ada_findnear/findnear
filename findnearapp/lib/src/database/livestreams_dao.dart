import 'dart:io' as io;

import 'package:findnear/src/models/user.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';

import '../utils/logger.dart';

class LivestreamsDAO extends GetxService {
  final String _tableName = "livestreams";
  LazyBox _box;

  Future<LivestreamsDAO> init() async {
    _box = await Hive.openLazyBox(_tableName);
    return this;
  }

  Future<LazyBox> _getBox() async {
    if (_tableName == null || _tableName.isBlank) {
      return null;
    }

    if (Hive.isBoxOpen(_tableName)) {
      return Hive.lazyBox(_tableName);
    }

    return await Hive.openLazyBox(_tableName);
  }

  Future<User> get(String code) async {
    if (code == null || code.isBlank) return null;

    final value = await _box.get(code);
    if (value == null) {
      return null;
    } else {
      try {
        final user =
          User.fromJSON(Map<String, dynamic>.from(value));
        if (user.code?.isNotEmpty == true) {
          return user;
        } else {
          return null;
        }
      } catch (e) {
        logger.e("ConversationsDAO - get - error: $e");
        return null;
      }
    }
  }

  Future<void> put(User user) async {
    if (user != null && user.code?.isNotEmpty == true) {
      await _box.put(user.code, user.toMap());
    }
  }

  Future<void> putAll(List<User> users) async {
    if (users.isEmpty) return;

    final mapUsers = new Map<dynamic, dynamic>();
    for (final user in users) {
      if (user != null && user.code?.isNotEmpty == true) {
        mapUsers[user.code] = user.toMap();
      }
    }
    await _box.putAll(mapUsers);
  }

  Future<void> delete(String code) async {
    await deleteById(code);
  }

  Future<void> deleteFromList(List<User> users) async {
    if (users.isEmpty) return;

    final uuids = users.map((e) => e?.code);
    await deleteByIds(uuids);
  }

  Future<void> deleteById(String code) async {
    if (code != null && code.isNotEmpty) {
      final box = await _getBox();
      await box?.delete(code);
    }
  }

  Future<void> deleteByIds(List<String> codes) async {
    if (codes.isEmpty) return;
    final box = await _getBox();

    final idsNotNull = codes.where((element) => element != null);
    await box?.deleteAll(idsNotNull);
  }

  Future<int> clear() async {
    return await _box.clear();
  }

  @override
  void onClose() {
    _box.close();
    super.onClose();
  }
}
