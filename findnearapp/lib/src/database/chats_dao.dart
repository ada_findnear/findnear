import '../models/entities/message_chat/enums/message_chat_type.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';

import '../models/entities/message_chat/entities/message_chat.dart';
import '../utils/logger.dart';
import 'package:collection/collection.dart';

class ChatsDAO {
  final String tableName;

  ChatsDAO({@required this.tableName});

  Future<LazyBox> _getBox() async {
    if (tableName == null || tableName.isBlank) {
      return null;
    }

    if (Hive.isBoxOpen(tableName)) {
      return Hive.lazyBox(tableName);
    }

    return await Hive.openLazyBox(tableName);
  }

  Future<void> closeBox() async {
    if (tableName == null || tableName.isBlank) {
      return;
    }

    if (Hive.isBoxOpen(tableName)) {
      final box = await _getBox();
      await box?.close();
    }
  }

  Future<void> deleteFromDisk() async {
    final box = await _getBox();
    await box?.deleteFromDisk();
  }

  Future<MessageChat> get(String uuid) async {
    if (uuid == null || uuid.isBlank) return null;
    final box = await _getBox();

    final value = await box?.get(uuid);
    if (value == null) {
      return null;
    } else {
      try {
        final chat = MessageChat.fromJson(Map<String, dynamic>.from(value),
            forLocalDB: true);
        if (chat.uuid?.isNotEmpty == true) {
          return chat;
        } else {
          return null;
        }
      } catch (e) {
        logger.e("ChatsDAO - get - error: $e");
        return null;
      }
    }
  }

  Future<List<MessageChat>> getAll() async {
    final box = await _getBox();

    List<MessageChat> chats = [];
    for (final key in (box?.keys?.toList() ?? [])) {
      final chat = await get(key);
      if (chat != null) {
        chats.add(chat);
      }
    }
    return chats;
  }

  Future<List<MessageChat>> getImageOnly() async {
    final box = await _getBox();

    List<MessageChat> chats = [];
    for (final key in (box?.keys?.toList() ?? [])) {
      final chat = await get(key);
      if (chat != null && chat.type == MessageChatType.image) {
        chats.add(chat);
      }
    }
    return chats;
  }

  Future<List<MessageChat>> getLinkOnly() async {
    final box = await _getBox();

    List<MessageChat> chats = [];
    for (final key in (box?.keys?.toList() ?? [])) {
      final chat = await get(key);
      if (chat != null && chat.type == MessageChatType.link) {
        chats.add(chat);
      }
    }
    return chats;
  }

  Future<List<MessageChat>> getFileOnly() async {
    final box = await _getBox();

    List<MessageChat> chats = [];
    for (final key in (box?.keys?.toList() ?? [])) {
      final chat = await get(key);
      if (chat != null && chat.type == MessageChatType.file) {
        chats.add(chat);
      }
    }
    return chats;
  }

  Future<void> put(MessageChat chat) async {
    if (chat != null && chat.uuid?.isNotEmpty == true) {
      final box = await _getBox();
      await box?.put(chat.uuid, chat.toJson(forLocalDB: true));
    }
  }

  Future<void> putAll(List<MessageChat> chats) async {
    if (chats.isEmpty) return;
    final box = await _getBox();

    final mapChats = new Map<dynamic, dynamic>();
    for (final chat in chats) {
      if (chat != null && chat.uuid?.isNotEmpty == true) {
        mapChats[chat.uuid] = chat.toJson(forLocalDB: true);
      }
    }
    await box?.putAll(mapChats);
  }

  Future<void> delete(MessageChat chat) async {
    await deleteById(chat?.uuid);
  }

  Future<void> deleteFromList(List<MessageChat> chats) async {
    if (chats.isEmpty) return;
    final ids = chats.map((e) => e?.uuid);
    await deleteByIds(ids);
  }

  Future<void> deleteById(String uuid) async {
    if (uuid != null && uuid.isNotEmpty) {
      final box = await _getBox();
      await box?.delete(uuid);
    }
  }

  Future<void> deleteByIds(List<String> uuids) async {
    if (uuids.isEmpty) return;
    final box = await _getBox();

    final idsNotNull = uuids.where((element) => element != null);
    await box?.deleteAll(idsNotNull);
  }

  Future<void> findAnddeleteChatsRemoved(List<MessageChat> chats) async {
    final sortedChats = chats
      ..sort((a, b) => b.createDate.compareTo(a.createDate));
    final allChats = (await getAll())
      ..sort((a, b) => b.createDate.compareTo(a.createDate));

    final firstSortedChats = sortedChats.firstOrNull;
    final lastSortedChats = sortedChats.lastOrNull;

    if (firstSortedChats == null || lastSortedChats == null) return;

    var firstIndex = -1;
    var lastIndex = -1;
    for (var i = 0; i < allChats.length; i++) {
      final chat = allChats[i];
      if (chat.uuid == firstSortedChats.uuid) {
        firstIndex = i;
      }
      if (chat.uuid == lastSortedChats.uuid) {
        lastIndex = i;
      }
      if (firstIndex != -1 && lastIndex != -1) {
        break;
      }
    }

    if (firstIndex == -1 || lastIndex == -1) return;

    final rangeChatsUuid =
        allChats.sublist(firstIndex, lastIndex + 1).map((e) => e.uuid);
    final sortedChatsUuid = sortedChats.map((e) => e.uuid);
    final listUuidRemove =
        rangeChatsUuid.where((e) => !sortedChatsUuid.contains(e)).toList();
    await deleteByIds(listUuidRemove);
  }

  Future<int> clear() async {
    final box = await _getBox();
    return await box?.clear();
  }
}
