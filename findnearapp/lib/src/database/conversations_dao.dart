import 'dart:io' as io;

import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:hive/hive.dart';

import '../models/conversation.dart';
import '../utils/logger.dart';
import 'chats_dao.dart';

class ConversationsDAO extends GetxService {
  final String _tableName = "conversations";
  LazyBox _box;

  Future<ConversationsDAO> init() async {
    _box = await Hive.openLazyBox(_tableName);
    return this;
  }

  Future<Conversation> get(String uuid) async {
    if (uuid == null || uuid.isBlank) return null;

    final value = await _box.get(uuid);
    if (value == null) {
      return null;
    } else {
      try {
        final conversation =
            Conversation.fromJSON(Map<String, dynamic>.from(value));
        if (conversation.uuid?.isNotEmpty == true) {
          return conversation;
        } else {
          return null;
        }
      } catch (e) {
        logger.e("ConversationsDAO - get - error: $e");
        return null;
      }
    }
  }

  Stream<Conversation> watch(String uuid) {
    if (uuid == null || uuid.isBlank) return null;

    return _box.watch(key: uuid).map((event) =>
        Conversation.fromJSON(Map<String, dynamic>.from(event.value)));
  }

  Future<List<Conversation>> getAll() async {
    List<Conversation> conversations = [];
    for (final key in _box.keys.toList()) {
      final conversation = await get(key);
      if (conversation != null) {
        conversations.add(conversation);
      }
    }
    return conversations;
  }

  Future<Conversation> getAllWithUserId(String userId) async {
    User _user = Get.find<LocalDataManager>().currentUser;
    Conversation result = null;
    for (final key in _box.keys.toList()) {
      final conversation = await get(key);
      if (conversation != null &&
          conversation.userIds.length == 2 &&
          conversation.userIds.contains(userId) &&
          conversation.userIds.contains(_user.id)) {
        result = conversation;
      }
    }
    return result;
  }

  Future<List<Conversation>> getAllByOriginalId(String id) async {
    final conversations = await getAll();
    return conversations.where((element) => element.originId == id).toList();
  }

  Future<void> put(Conversation conversation) async {
    if (conversation != null && conversation.uuid?.isNotEmpty == true) {
      await _box.put(conversation.uuid, conversation.toMap());
    }
  }

  Future<void> putAll(List<Conversation> conversations) async {
    if (conversations.isEmpty) return;

    final mapConversations = new Map<dynamic, dynamic>();
    for (final conversation in conversations) {
      if (conversation != null && conversation.uuid?.isNotEmpty == true) {
        mapConversations[conversation.uuid] = conversation.toMap();
      }
    }
    await _box.putAll(mapConversations);
  }

  Future<void> delete(String uuid) async {
    await deleteById(uuid);
  }

  Future<void> deleteFromList(List<Conversation> conversations) async {
    if (conversations.isEmpty) return;

    final uuids = conversations.map((e) => e?.uuid);
    await deleteByIds(uuids);
  }

  Future<void> deleteById(String uuid) async {
    if (uuid != null && uuid.isNotEmpty) {
      await _box.delete(uuid);

      final chatsDAO = ChatsDAO(tableName: uuid);
      chatsDAO.deleteFromDisk();
    }
  }

  Future<void> deleteByIds(List<String> uuids) async {
    if (uuids.isEmpty) return;

    final idsNotNull = uuids.where((element) => element != null);
    await _box.deleteAll(idsNotNull);

    for (final uuid in idsNotNull) {
      final chatsDAO = ChatsDAO(tableName: uuid);
      chatsDAO.deleteFromDisk();
    }
  }

  Future<int> clear() async {
    return await _box.clear();
  }

  Future<int> clearIncludeChats() async {
    final appDocumentDirectory = await getApplicationDocumentsDirectory();
    final directory = appDocumentDirectory.path + "/hive_databases/";
    final files = io.Directory(directory).listSync();
    for (var file in files) {
      if (!file.path.contains("$_tableName.hive") &&
          !file.path.contains("$_tableName.lock")) {
        file.delete();
      }
    }

    return await _box.clear();
  }

  @override
  void onClose() {
    _box.close();
    super.onClose();
  }
}
