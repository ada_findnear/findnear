import 'package:findnear/src/models/media.dart';

import '../models/post.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../models/order.dart';
import '../repository/post_repository.dart' as postRepo;
import '../repository/friends_repository.dart' as friendRepo;
import '../models/user.dart' as userModel;
import '../repository/user_repository.dart' as repository;

class ProfileController extends ControllerMVC {
  List<Order> recentOrders = [];
  List<Post> Posts = [];
  int currentOffset = 0;
  int postCount = 0;
  bool canLoadMore = true;
  bool isLoading = false;
  GlobalKey<ScaffoldState> scaffoldKey;
  userModel.User userData = new userModel.User.fromJSON({});
  Post post;
  bool isLoadingPost = true;
  List<Media> listImage = [];
  List<Media> listVideo = [];

  ProfileController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();

  }

  void listenForPosts(user_id, {String message}) async {
    final Stream<Post> stream = await postRepo.getPost(offset: currentOffset, userId: user_id, limit: 10);
    setState(() {
      isLoadingPost = false;
    });

    stream.listen((Post _post) {
      setState(() {
        Posts.add(_post);
      });
    }, onError: (a) {
      ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
        content: Text(S.of(state.context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (currentOffset == Posts.length) {
        setState(() {
          canLoadMore = false;
        });
      }
      setState(() {
        currentOffset = Posts.length;
        isLoading = false;
      });
      if (message != null) {
        ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  void listenForPost(postId, index, {String message}) async {
    final Stream<Post> stream = await postRepo.getPostById(postId);

    stream.listen((Post _post) {
      setState(() {
        Posts[index] = _post;
      });
    }, onError: (a) {
      ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
        content: Text(S.of(state.context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (message != null) {
        ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  void getListMedia(String userId, {String message}) async {
    final Stream<Post> stream = await postRepo.getPost(userId: userId);
    stream.listen((Post _post) {
      if(_post.medias.length > 0) {
        _post.medias.forEach((element) {
          if (element.mime_type.indexOf("image") != -1) {
            setState(() {
              listImage.add(element);
            });
          } else if(element.mime_type.indexOf("video") != -1) {
            setState(() {
              listVideo.add(element);
            });
          }
        });
      }
    }, onError: (a) {
      ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
        content: Text(S.of(state.context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (message != null) {
        ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  void createPost(Post post, pathImages) async {
    postRepo.addPost(post, pathImages).then((value) {
      if (value != Post.fromJSON({})) {
        setState(() {
          Posts.insert(0, value);
        });
        ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
          content: Text(S.of(state.context).createPostSuccessful),
        ));
      } else {
        ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
          content: Text(S.of(state.context).verify_your_internet_connection),
        ));
      }
    });
  }

  void updatePost(Post post) async {
    postRepo.updatePost(post).then((value) {
      var dataPosts = Posts;
      var indexOfPost = Posts.indexWhere((element) => element.id == post.id);
      if (indexOfPost != -1) {
        dataPosts[indexOfPost] = value;
        setState(() {
          Posts = dataPosts;
        });
      }
      ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
        content: Text(S.of(state.context).updatePostSuccessful),
      ));
    });
  }
  void update({userModel.User user, String pathImage = null, String imageType = null}) async {
    user.deviceToken = null;
    repository.update(user: user, pathImage: pathImage, imageType: imageType).then((value) {
      setState(() {});
      ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
        content: Text(S.of(state.context).profile_settings_updated_successfully),
      ));
    });
  }

  void listenForUser(userID, bool isMyProfile) async {
    if (userID != null) {
      final userModel.User data = await repository.getUser(userID);
      if (!isMyProfile) {
        setState(() {
          userData = data;
        });
      }
      setState(() {});
    }
  }

  void toggleLikePost(postID, {String message}) async {
    postRepo.toggleLikePost(postID).then((value) {
      var dataPosts = Posts;
      var indexOfPost = Posts.indexWhere((element) => element.id == postID);
      if (value) {
        dataPosts[indexOfPost].likers_count = (int.parse(dataPosts[indexOfPost].likers_count) + 1).toString();
        dataPosts[indexOfPost].isLike = true;
      } else {
        dataPosts[indexOfPost].likers_count = (int.parse(dataPosts[indexOfPost].likers_count) - 1).toString();
        dataPosts[indexOfPost].isLike = false;
      }
      setState(() {
        Posts = dataPosts;
      });
    });
  }

  void removePost(Post post) async {
    postRepo.removePost(post).then((value) {
      if (value) {
        var dataPosts = Posts;
        var indexOfPost = Posts.indexWhere((element) => element.id == post.id);
        if (indexOfPost != -1) {
          dataPosts.removeAt(indexOfPost);
          setState(() {
            Posts = dataPosts;
          });
        }
        ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
          content: Text(S.of(state.context).removePostSuccessful),
        ));
      }
    });
  }

  void addFriend(String idUserTarget, String type) async {
    friendRepo.addFriend(idUserTarget, type).then((value) {
      this.listenForUser(int.parse(userData.id), false);
      switch (type) {
        case "add":
          scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(S.of(state.context).friendRequestSentSuccessfully)));
          break;
        case "confirm":
          scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(S.of(state.context).confirmationOfSuccessfulFriendRequest)));
          break;
        case "deny":
          scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(S.of(state.context).friendRequestDenied)));
          break;
        case "cancel":
          scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(S.of(state.context).friendRequestHasBeenCanceled)));
          break;
        case "delete":
          scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(S.of(state.context).successfulUnfriend)));
          break;
      }
    });
  }
}
