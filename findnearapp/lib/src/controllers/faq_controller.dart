import 'dart:math';

import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../models/faq_category.dart';
import '../repository/faq_repository.dart';

class FaqController extends ControllerMVC {
  List<FaqCategory> faqs = <FaqCategory>[];
  GlobalKey<ScaffoldState> scaffoldKey;
  List<dynamic> contacts = <dynamic>[
    {
    "name": "0963125582",
    "description": "Hỗ trợ khách hàng",
    "phone": "0963125582",
    "avatar": "https://datxanhkhudong.vn/wp-content/uploads/2020/12/dao-tao-dxmd-2.png",
    "zalo": "",
    },
    {
      "name": "0973421693",
        "description": "Hỗ trợ kĩ thuật",
      "phone": "0973421693",
      "avatar": "https://datxanhkhudong.vn/wp-content/uploads/2020/12/dao-tao-dxmd-2.png",
      "zalo": "",
    },
  ];

  FaqController() {
    scaffoldKey = new GlobalKey<ScaffoldState>();
    listenForFaqs();
  }

  void listenForFaqs({String message}) async {
    final Stream<FaqCategory> stream = await getFaqCategories();
    stream.listen((FaqCategory _faq) {
      setState(() {
        faqs.add(_faq);
      });
    }, onError: (a) {
      print(a);
      ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
        content: Text(S.of(state.context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (message != null) {
        ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
      faqs.add(FaqCategory.fromJSON({"id": "", "name": S.of(state.context).contact, "faqs": []}));
    });
  }

  Future<void> refreshFaqs() async {
    faqs.clear();
    listenForFaqs(message: S.of(state.context).faqsRefreshedSuccessfuly);
  }
}
