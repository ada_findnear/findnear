import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../repository/friends_repository.dart' as repository;
import '../models/user.dart' as userModel;

class FriendsRequestController extends ControllerMVC {
  List<userModel.User> friendsRequest = [];
  int friendsRequestCount = 0;
  int currentOffset = 0;
  GlobalKey<ScaffoldState> scaffoldKey;
  bool canLoadMore = true;

  void listenForFriendsRequest() async {
    final Stream<userModel.User> stream = await repository.getFriendsRequest(offset: currentOffset, limit: 10);
    stream.listen((userModel.User data) {
      print(data.id);
      setState(() {
        friendsRequest.add(data);
      });
    }, onError: (a) {
      ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
        content: Text(S.of(state.context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (currentOffset == friendsRequest.length) {
        setState(() {
          canLoadMore = false;
        });
      }
      setState(() {
        currentOffset = friendsRequest.length;
      });
    });
  }
}
