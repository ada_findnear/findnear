import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../models/address.dart';
import '../models/shop.dart';
import '../models/response/product_response.dart';
import '../repository/shop_repository.dart';
import '../repository/vnp_product_repository.dart';
import '../repository/search_repository.dart';
import '../repository/settings_repository.dart';

class SearchController extends ControllerMVC {
  List<Shop> shops = <Shop>[];
  List<Product> products = <Product>[];

  TextEditingController textController;
  SearchController() {
    textController = TextEditingController();
    listenForShops();
    listenForProducts();
  }

  void listenForShops({String search}) async {
    if (search == null) {
      search = textController.text = await getRecentSearch();
    }
    Address _address = deliveryAddress.value;
    final Stream<Shop> stream = await searchShops(search, _address);
    stream.listen((Shop _shop) {
      if(_shop.title != null){
        setState(() => shops.add(_shop));
      }
    }, onError: (a) {
      print(a);
    }, onDone: () {});
  }

  void listenForProducts({String search}) async {
    if (search == null) {
      search = await getRecentSearch();
    }
    Address _address = deliveryAddress.value;
    final Stream<Product> stream = await searchProducts(search, _address);
    stream.listen((Product _product) {
      if(_product.name != null){
        setState(() => products.add(_product));
      }
    }, onError: (a) {
      print(a);
    }, onDone: () {});
  }

  Future<void> refreshSearch(search) async {
    setState(() {
      shops = <Shop>[];
      products = <Product>[];
    });
    listenForShops(search: search);
    listenForProducts(search: search);
  }

  void saveSearch(String search) {
    setRecentSearch(search);
  }
}
