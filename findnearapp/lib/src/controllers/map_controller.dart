// import 'dart:async';
//
// import '../elements/CardUserBottomSheetWidget.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:flutter_polyline_points/flutter_polyline_points.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:mvc_pattern/mvc_pattern.dart';
//
// import '../repository/settings_repository.dart' as settingRepo;
// import '../helpers/helper.dart';
// import '../helpers/maps_util.dart';
// import '../models/address.dart';
// import '../models/market.dart';
// import '../repository/market_repository.dart';
// import '../repository/settings_repository.dart' as sett;
// import 'package:google_maps_webservice/places.dart';
//
// class MapController extends ControllerMVC {
//   Market currentMarket;
//   List<Market> topMarkets = <Market>[];
//   List<Marker> allMarkers = <Marker>[];
//   Address currentAddress;
//   CameraPosition cameraPosition;
//   MapsUtil mapsUtil = communication MapsUtil();
//   Completer<GoogleMapController> mapController = Completer();
//
//   GlobalKey<ScaffoldState> scaffoldKey;
//   var bottomSheetController;
//
//   PolylinePoints polylinePoints;
//   List<LatLng> polylineCoordinates = [];
//   Map<PolylineId, Polyline> polyLines = {};
//
//   Prediction searchPosition;
//
//   MapController() {
//     this.scaffoldKey = communication GlobalKey<ScaffoldState>();
//   }
//
//   void onClickMarker(Market market) {
//     this.bottomSheetController = this.scaffoldKey.currentState.showBottomSheet(
//       (context) => CardUserBottomSheetWidget(scaffoldKey: this.scaffoldKey, market: market),
//       shape: RoundedRectangleBorder(
//         borderRadius: communication BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
//       ),
//     );
//   }
//
//   void listenForNearMarkets(Address myLocation, Address areaLocation) async {
//     final Stream<Market> stream = await getNearMarkets(myLocation, areaLocation);
//     stream.listen((Market _market) {
//       setState(() {
//         topMarkets.add(_market);
//       });
//       Helper.getMarker(_market, this).then((marker) {
//         marker.forEach((element) {
//           setState(() {
//             allMarkers.add(element);
//           });
//         });
//       });
//     }, onError: (a) {}, onDone: () {
//       //
//     });
//   }
//
//   void getCurrentLocation() async {
//     try {
//       currentAddress = sett.deliveryAddress.value;
//       setState(() {
//         if (currentAddress.isUnknown()) {
//           cameraPosition = CameraPosition(
//             target: LatLng(21.029832, 105.852605),
//             zoom: 14.4746,
//           );
//         } else {
//           cameraPosition = CameraPosition(
//             target: LatLng(currentAddress.latitude, currentAddress.longitude),
//             zoom: 14.4746,
//           );
//         }
//       });
//       if (!currentAddress.isUnknown()) {
//         Helper.getMyPositionMarker(currentAddress.latitude, currentAddress.longitude).then((marker) {
//           setState(() {
//             allMarkers.add(marker);
//           });
//         });
//       }
//     } on PlatformException catch (e) {
//       if (e.code == 'PERMISSION_DENIED') {
//         print('Permission denied');
//       }
//     }
//   }
//
//   void getMarketLocation() async {
//     try {
//       currentAddress = await sett.getCurrentLocation();
//       setState(() {
//         cameraPosition = CameraPosition(
//           target: LatLng(double.parse(currentMarket.latitude), double.parse(currentMarket.longitude)),
//           zoom: 14.4746,
//         );
//       });
//       Helper.getMyPositionMarker(currentAddress.latitude, currentAddress.longitude).then((marker) {
//         setState(() {
//           allMarkers.add(marker);
//         });
//       });
//     } on PlatformException catch (e) {
//       if (e.code == 'PERMISSION_DENIED') {
//         print('Permission denied');
//       }
//     }
//   }
//
//   Future<void> goCurrentLocation() async {
//     final GoogleMapController controller = await mapController.future;
//
//     sett.setCurrentLocation().then((_currentAddress) {
//       setState(() {
//         sett.deliveryAddress.value = _currentAddress;
//         currentAddress = _currentAddress;
//       });
//       controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
//         target: LatLng(_currentAddress.latitude, _currentAddress.longitude),
//         zoom: 14.4746,
//       )));
//     });
//   }
//
//   Future<Null> displayPrediction(Prediction p) async {
//     final GoogleMapController controller = await mapController.future;
//
//     if (p != null) {
//       setState(() {
//         searchPosition = p;
//       });
//       PlacesDetailsResponse detail = await GoogleMapsPlaces(apiKey: settingRepo.setting.value.googleMapsKey).getDetailsByPlaceId(p.placeId);
//
//       double lat = detail.result.geometry.location.lat;
//       double lng = detail.result.geometry.location.lng;
//
//       controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
//         target: LatLng(lat, lng),
//         zoom: 14.4746,
//       )));
//     }
//   }
//
//   void getMarketsOfArea() async {
//     setState(() {
//       topMarkets = <Market>[];
//       Address areaAddress = Address.fromJSON({"latitude": cameraPosition.target.latitude, "longitude": cameraPosition.target.longitude});
//       if (cameraPosition != null) {
//         listenForNearMarkets(currentAddress, areaAddress);
//       } else {
//         listenForNearMarkets(currentAddress, currentAddress);
//       }
//     });
//   }
//
//   void getDirectionSteps() async {
//     polylinePoints = PolylinePoints();
//
//     var _currentAddress = await sett.setCurrentLocation();
//     setState(() {
//       sett.deliveryAddress.value = _currentAddress;
//       currentAddress = _currentAddress;
//     });
//
//     PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
//       settingRepo.setting.value.googleMapsKey,
//       PointLatLng(_currentAddress.latitude, _currentAddress.longitude),
//       PointLatLng(double.parse(currentMarket.latitude), double.parse(currentMarket.longitude)),
//     );
//
//     polylineCoordinates = [];
//     if (result.points.isNotEmpty) {
//       result.points.forEach((PointLatLng point) {
//         polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//       });
//     }
//
//     PolylineId id = PolylineId('poly');
//
//     Polyline polyline = Polyline(
//       polylineId: id,
//       color: Colors.blueAccent,
//       points: polylineCoordinates,
//       width: 5,
//     );
//
//     setState(() {
//       polyLines[id] = polyline;
//     });
//   }
//
//   Future refreshMap() async {
//     setState(() {
//       topMarkets = <Market>[];
//     });
//     listenForNearMarkets(currentAddress, currentAddress);
//   }
// }
