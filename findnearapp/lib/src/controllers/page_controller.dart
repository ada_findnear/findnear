import 'dart:async';
import 'dart:io';

import 'package:findnear/src/global/global_data.dart';
import 'package:findnear/src/global/global_event.dart';
import 'package:findnear/src/helpers/warning/need_location_access_warning_widget.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/entities/communication/entities/unread_message_count_entity.dart';
import 'package:findnear/src/models/entities/offer_call_entity.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/pages/videocall/callsignaling.dart';
import 'package:findnear/src/pages/videocall/view.dart';
import 'package:findnear/src/pages/voice_call_v2/voice_call_screenv2.dart';
import 'package:findnear/src/repository/communication_repository.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/repository/user_v2_repository.dart';
import 'package:findnear/src/utils/listener_event_callkit.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/dialog/expired_account_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';

class PagesController extends GetxController {
  CommunicationRepository _conversationRepository =
      CommunicationApi(isSocketApi: true);
  UserV2Repository userV2Repository = Get.find<UserV2Repository>();
  final eventCallKit = ListenerEventCallKit();

  PagesController({int initialTab}) {
    currentTab = (initialTab is int ? initialTab : 2).obs;
  }

  RxBool shouldRefreshNewsFeed = false.obs;

  RxInt currentTab;
  RxBool enableCheckboxModeRx = false.obs;
  RxBool selectAllConversationRx = false.obs;
  final unReadCountRx = Rx<List<UnreadMessageCountEntity>>([]);
  RxBool isAppOnBackground = false.obs;
  StreamSubscription _listenCallSubscription;
  StreamSubscription _listenCallSubscriptionIOS;
  StreamSubscription _listenEndCallSubscription;
  StreamSubscription _acceptCallFromCallKitSubscription;
  String callKitID = "";

  @override
  void onInit() {
    super.onInit();
  }

  void checkExpiredAccount(DateTime expiredAt, BuildContext context) {
    if (expiredAt != null && expiredAt.isAfter(DateTime.now())) {
      int differentDate = expiredAt.difference(DateTime.now()).inDays;
      switch (differentDate) {
        case 1:
        case 2:
        case 3:
        case 7:
        case 15:
        case 30:
          showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) => expiredAccountDialog(
              expiredDate: DateFormat('dd/MM/yyyy').format(expiredAt),
            ),
          );
          break;
      }
    }
  }

  void forceRefreshNewsFeed() {
    shouldRefreshNewsFeed.value = true;
    Future.delayed(Duration(milliseconds: 1))
        .then((value) => shouldRefreshNewsFeed.value = false);
  }

  @override
  void onClose() async {
    // _pageController.dispose();
    _listenCallSubscription?.cancel();
    _listenCallSubscriptionIOS?.cancel();
    _listenEndCallSubscription?.cancel();
    _acceptCallFromCallKitSubscription?.cancel();
    super.onClose();
  }

  void checkLocationGrant() async {
    // You can can also directly ask the permission about its status.
    if (await Permission.location.isGranted) {
      Fluttertoast.showToast(
        msg: "Bạn đã có quyền truy cập vào vị trí",
        toastLength: Toast.LENGTH_LONG,
        fontSize: 18.0,
      );
    } else
      showDialog(
        context: Get.context,
        barrierDismissible: true,
        builder: (BuildContext context) => Dialog(
          backgroundColor: Colors.transparent,
          insetPadding: EdgeInsets.zero,
          child: NeedLocationAccessWarningWidget(),
        ),
      );
  }

  Future<List<UnreadMessageCountEntity>> getUnreadMessageCount(
      {String sender}) async {
    List<UnreadMessageCountEntity> result =
        await _conversationRepository.getUnreadMessageCount(sender: sender);
    unReadCountRx.value = []..addAll(result);
    return result;
  }

  void listenCallComingIOS(BuildContext context) {
    _listenCallSubscriptionIOS?.cancel();
    _listenCallSubscriptionIOS =
        GlobalEvent.instance.onReceiveOfferCallIOS.stream.listen((data) {
          if(data.typeCall == CallData.video.value){
            eventCallKit.showCallVideoOverlay(context, data);
          }else{
            eventCallKit.showCallVoiceOverlay(context, data);
          }
    });
  }

  void listenCallComing(BuildContext context) {
    _listenCallSubscription?.cancel();
    // nhận sự kiện có cuộc gọi đến
    _listenCallSubscription =
        GlobalEvent.instance.onReceiveOfferCall.stream.listen((data) async {
      if (isAppOnBackground.value == true) {
        try {
          // tạo param to push callkit
          if (Platform.isAndroid) {
            var callNotiResponse = data.extendData;
            callKitID = callNotiResponse.id;
            var params = <String, dynamic>{
              'id': callKitID,
              'nameCaller': callNotiResponse.nameCaller,
              'appName': callNotiResponse.appName,
              'avatar': callNotiResponse.avatar,
              'number': callNotiResponse.codeCaller,
              'handle': callNotiResponse.codeCaller,
              'type': callNotiResponse.messageType == "call_video" ? 1 : 0,
              'extra': data.offerEntity.toJson(),
              'duration': 60000,
              'android': <String, dynamic>{
                'isCustomNotification': false,
                'isShowLogo': false,
                'ringtonePath': 'ringtone_default',
                'backgroundColor': '#0955fa',
                'actionColor': '#4CAF50'
              }
            };

            if (callNotiResponse.messageType == "call_video" ||
                callNotiResponse.messageType == "call_voice") {
              if (callNotiResponse.action == 'call_coming') {
                // user online không
                var checkOnline = false;
                final response = await userV2Repository
                    .getUserStatus(callNotiResponse.codeCaller);

                if (response.status == true) {
                  // user online
                  checkOnline = true;
                } else {
                  checkOnline = false;
                }
                // time out
                var checkTimeout = false;
                var timeNow = DateTime.now().millisecondsSinceEpoch;
                if ((callNotiResponse.timeSend + 60000) > timeNow) {
                  // chưa hết time out
                  checkTimeout = false;
                } else {
                  checkTimeout = true;
                }

                if (checkOnline == true && checkTimeout == false) {
                  await FlutterCallkitIncoming.showCallkitIncoming(params);
                }
              } else if (callNotiResponse.action == 'end_call') {
                try {
                  var params = <String, dynamic>{'id': callKitID};
                  await FlutterCallkitIncoming.endCall(params);
                } catch (e) {
                  print("TuanLA - end call error: $e");
                }
              }
            }
          }
        } catch (e) {
          print('TuanLA - $e');
        }
      } else {
        if (Platform.isIOS && GlobalData.instance.isAcceptCallKitIos) {
          if(data.typeCall == CallData.video.value){
            eventCallKit.showCallVideoOverlay(context, data);
          }else{
            eventCallKit.showCallVoiceOverlay(context, data);
          }
          return;
        }

        if (data.typeCall == CallData.video.value) {
          pushToCallVideoScreen(context, data);
        } else {
          pushToCallScreen(context, data);
        }
      }
    });
  }

  void listenEndCallFromCallKit() {
    _listenEndCallSubscription?.cancel();
    _listenEndCallSubscription =
        GlobalEvent.instance.onEndVideoCall.stream.listen((data) async {
      if (isAppOnBackground.value == true) {
        try {
          var params = <String, dynamic>{'id': callKitID};
          await FlutterCallkitIncoming.endCall(params);
        } catch (e) {}
      }
    });
  }

  void listenAcceptCallFromCallkit(BuildContext context) {
    _acceptCallFromCallKitSubscription?.cancel();
    _acceptCallFromCallKitSubscription =
        GlobalEvent.instance.onPushCallEvent.stream.listen((event) async {
      print("TuanLA - event call: $event");
      if (event.typeCall == "video") {
        NavigatorUtils.navigateToVideoCallWidget(
            context,
            ShortUserEntity(code: event.userMakeCall, id: "", avatar: ""),
            false,
            true,
            fromCallkit: true,
            offerCallEntity: event);
      } else if (event.typeCall == "audio") {
        NavigatorUtils.navigateToVoiceCallWidget(
            context,
            ShortUserEntity(code: event.userMakeCall, id: "", avatar: ""),
            false,
            true,
            fromCallkit: true,
            offerCallEntity: event);
      }
    });
  }

  void pushToCallVideoScreen(BuildContext context, OfferCallEntity user) async {
    // bắn socket update trạng thái user đang trong cuộc gọi
    eventCallKit.updateUserStatus(1, currentUser.value.code, true);
    NavigatorUtils.navigateToVideoCallComingWidget(context, user);
  }

  void pushToCallScreen(BuildContext context, OfferCallEntity user) async {
    eventCallKit.updateUserStatus(1, currentUser.value.code, true);
    NavigatorUtils.navigateToVoiceCallComingWidget(context, user);
  }
}
