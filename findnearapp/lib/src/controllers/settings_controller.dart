import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/pages/position_picker.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';

import '../helpers/maps_util.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import '../repository/user_repository.dart';
import '../models/market.dart';
import '../models/field.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../../generated/l10n.dart';
import '../models/credit_card.dart';
import '../models/user.dart' as userModel;
import '../repository/user_repository.dart' as repository;
import '../repository/field_repository.dart' as fieldRepository;
import '../repository/market_repository.dart' as marketRepository;
import '../repository/settings_repository.dart' as settingRepository;

class SettingsController extends ControllerMVC {
  GlobalKey<FormState> loginFormKey;
  GlobalKey<ScaffoldState> scaffoldKey;
  List<Map<String, dynamic>> fields = [];
  bool mapVisible;
  bool positionFixed;
  String address;
  MapsUtil mapsUtil = new MapsUtil();

  SettingsController() {
    loginFormKey = new GlobalKey<FormState>();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    this.listenForFields();
  }

  void update(userModel.User user) async {
    user.deviceToken = null;
    repository.update(user: user).then((value) {
      setState(() {});
      ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
        content:
            Text(S.of(state.context).profile_settings_updated_successfully),
      ));
    });
  }

  void listenForFields({String message}) async {
    final Stream<Field> stream = await fieldRepository.getFields();
    stream.listen((Field _field) {
      setState(() {
        fields.add({
          'value': _field.id,
          'label': _field.name,
          'icon': Container(
            padding: const EdgeInsets.all(10),
            child: ClipOval(
              child: Image.network(
                _field.image.url,
                width: 30,
                height: 30,
                fit: BoxFit.cover,
              ),
            ),
          ),
          'selected': repository.currentUser.value?.field == _field,
          'is_locked': _field.isLocked == 1 && currentUser.value?.canCreateShop == false ? 1 : 0,
        });
      });
    }, onError: (a) {
      print(a);
      ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
        content: Text(S.of(state.context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (message != null) {
        ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  void updateMarket(Market market) async {
    marketRepository.updateMarkets(market).then((value) {
      getUser(int.parse(currentUser.value?.id));
      ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
        content:
            Text(S.of(state.context).profile_settings_updated_successfully),
      ));
    });
  }

  // to do
  void toggleMapVisible(bool value) {
    Market market = currentUser.value?.market;
    market.visible = value;
    Get.find<LocalDataManager>().setIsVisibleOnMap(value);
    updateMarket(market);
    setState(() {
      mapVisible = value;
    });
  }

  void togglePositionFixed(bool value) async {
    setState(() {
      positionFixed = value;
    });

    Market market = currentUser.value?.market;
    if (value) {
      CameraPosition cameraPosition = await Navigator.of(state.context)
          .pushNamed(PositionPickerWidget.ROUTE_NAME) as CameraPosition;
      if (cameraPosition != null) {
        String addressResult = await mapsUtil.getAddressName(
            LatLng(cameraPosition.target.latitude,
                cameraPosition.target.longitude),
            settingRepository.setting.value.googleMapsKey);

        market.latitude = cameraPosition.target.latitude.toString();
        market.longitude = cameraPosition.target.longitude.toString();
        market.fixed_position = true;
        market.address = addressResult;
        updateMarket(market);
        setState(() {
          address = addressResult;
        });
      } else {
        setState(() {
          positionFixed = false;
        });
      }
    } else {
      market.fixed_position = false;
      updateMarket(market);
    }
  }

  void updateFixedAddress() async {
    CameraPosition cameraPosition = await Navigator.of(state.context).pushNamed(
            PositionPickerWidget.ROUTE_NAME,
            arguments: RouteArgument(
                param: LatLng(double.parse(currentUser.value?.market.latitude),
                    double.parse(currentUser.value?.market.longitude))))
        as CameraPosition;
    if (cameraPosition != null) {
      String addressResult = await mapsUtil.getAddressName(
          LatLng(
              cameraPosition.target.latitude, cameraPosition.target.longitude),
          settingRepository.setting.value.googleMapsKey);

      Market market = currentUser.value?.market;
      market.latitude = cameraPosition.target.latitude.toString();
      market.longitude = cameraPosition.target.longitude.toString();
      market.fixed_position = true;
      market.address = addressResult;

      updateMarket(market);
      setState(() {
        address = addressResult;
      });
    }
  }
}
