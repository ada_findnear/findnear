import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:findnear/src/global/global_data.dart';
import 'package:findnear/src/global/global_event.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/city.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/field.dart';
import 'package:findnear/src/models/twilio_user.dart';
import 'package:findnear/src/pages/change_password/change_password_view.dart';
import 'package:findnear/src/repository/chat_repository.dart';
import 'package:findnear/src/repository/field_repository.dart';
import 'package:findnear/src/services/cache_service.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:package_info/package_info.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:version/version.dart';

import '../../generated/l10n.dart';
import '../helpers/custom_trace.dart';
import '../repository/settings_repository.dart' as settingRepo;
import '../repository/user_repository.dart' as userRepo;

class SplashScreenController extends ControllerMVC {
  GlobalKey<ScaffoldState> scaffoldKey;

  ValueNotifier<Map<String, double>> progress = new ValueNotifier(new Map());

  ValueNotifier<bool> shouldShowForceUpdatePopup = new ValueNotifier(false);

  ValueNotifier<bool> onLoadDataDone = new ValueNotifier(null);

  final fieldRepository = Get.find<FieldRepository>();

  SplashScreenController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    // Should define these variables before the app loaded
    progress.value = {"Setting": 0, "User": 0};
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    Get.put<ChatRepository>(ChatRepository.init());
    settingRepo.setting.addListener(() async {
      // ---------------------->
      // start of force update
      final packageInfo = await PackageInfo.fromPlatform();
      String currentVersionString = packageInfo.version;
      final currentVersion = Version.parse(currentVersionString);
      // chek version get from api
      var versionString = null;
      // if android
      if (Platform.isAndroid) {
        versionString = settingRepo.setting.value?.versionAndroid;
      } else if (Platform.isIOS) {
        versionString = settingRepo.setting.value?.versionIos;
      }
      if (versionString != null && versionString.isNotEmpty) {
        try {
          final latestVersion = Version.parse(versionString);
          if (latestVersion > currentVersion) {
            shouldShowForceUpdatePopup.value = true;
            return;
          }
        } catch (e) {
          logger.e(e);
        }
      }
      // end of force update
      // ---------------------->
      if (settingRepo.setting.value.appName != null &&
          settingRepo.setting.value.appName != '' &&
          settingRepo.setting.value.mainColor != null) {
        progress.value["Setting"] = 41;
        // ignore: invalid_use_of_protected_member, invalid_use_of_visible_for_testing_member
        progress?.notifyListeners();
      }
    });
    userRepo.currentUser.addListener(() {
      if (userRepo.currentUser.value?.auth != null) {
        progress.value["User"] = 59;
        progress?.notifyListeners();
      }
    });
    //Fix: code ngáo
    if (userRepo.currentUser.value?.auth != null) {
      progress.value["User"] = 59;
      progress?.notifyListeners();
    }
    Timer(Duration(seconds: 20), () {
      if (scaffoldKey != null && scaffoldKey.currentContext != null) {
        ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
          content: Text(S.of(state.context).verify_your_internet_connection),
        ));
      }
    });
  }

  Future<void> loadMarketCategory() async {
    // int start = DateTime.now().millisecondsSinceEpoch;
    // try {
    //   final cacheService = Get.find<CacheService>();
    //
    //   String content =
    //   await rootBundle.loadString("assets/offline/fields.json");
    //
    //   var my_data = json.decode(content);
    //   var fields = my_data.map<Field>(
    //           (jsonElement) => Field.fromJSON(jsonElement)
    //   ).toList();
    //
    //   cacheService.fields.clear();
    //   cacheService.fields.addAll(fields);
    //   int end = DateTime.now().millisecondsSinceEpoch;
    //   print('quanth: timer splash - loadMarketCategory = ${end-start} milis');
    //   // loadMarketCategory();
    // } catch (e) {
    //   print("quanth");
    // }


    int start = DateTime.now().millisecondsSinceEpoch;
    final cacheService = Get.find<CacheService>();
    final fields = <Field>[];
    final Stream<Field> stream = await getFields();
    stream.listen(
      (Field _field) {
        fields.add(_field);
      },
      onError: (a) {
        ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
          content: Text(S.current.verify_your_internet_connection),
        ));
      },
      onDone: () {
        int end = DateTime.now().millisecondsSinceEpoch;
        print('quanth: timer1 splash - loadMarketCategory = ${end-start} milis');
        cacheService.fields.clear();
        cacheService.fields.addAll(fields);
        // loadLocalShopField();
      },
    );
  }

  Future<void> navigateToPage(){
    Navigator.of(Get.context).pushReplacementNamed('/Pages', arguments: 2);
  }

  Future<void> loadLocalAddress() async {
    int start = DateTime.now().millisecondsSinceEpoch;
    try {
      final cacheService = Get.find<CacheService>();

      String content =
          await rootBundle.loadString("assets/offline/cities.json");

      var my_data = json.decode(content);
      print("quanth ");
      var cities = my_data.map<City>(
              (jsonElement) => City.fromJSON(jsonElement)
      ).toList();

      cacheService.cities.clear();
      cacheService.cities.addAll(cities);
      int end = DateTime.now().millisecondsSinceEpoch;
      print('quanth: timer splash - loadLocalAddress = ${end-start} milis');
      // loadMarketCategory();
    } catch (e) {
      print("quanth");
    }
  }

  Future<void> loadLocalShopField() async {
    // int start = DateTime.now().millisecondsSinceEpoch;
    // try {
    //   final cacheService = Get.find<CacheService>();
    //
    //   String content =
    //   await rootBundle.loadString("assets/offline/shop_fields.json");
    //
    //   var my_data = json.decode(content);
    //   var fields = my_data.map<Field>(
    //           (jsonElement) => Field.fromJSON(jsonElement)
    //   ).toList();
    //
    //   cacheService.fields.clear();
    //   cacheService.fields.addAll(fields);
    //   int end = DateTime.now().millisecondsSinceEpoch;
    //   print('quanth: timer splash - loadLocalShopField = ${end-start} milis');
    //   // loadMarketCategory();
    // } catch (e) {
    //   print("quanth");
    // }

    int start = DateTime.now().millisecondsSinceEpoch;
    final cacheService = Get.find<CacheService>();
    final fields = <Field>[];
    final Stream<Field> stream = await getShopFields();
    stream.listen(
          (Field _field) {
        fields.add(_field);
      },
      onError: (a) {
        ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
          content: Text(S.current.verify_your_internet_connection),
        ));
      },
      onDone: () {
        int end = DateTime.now().millisecondsSinceEpoch;
        print('quanth: timer1 splash - loadLocalShopField = ${end-start} milis');
        cacheService.shopFields.clear();
        cacheService.shopFields.addAll(fields);
        // Navigator.of(Get.context).pushReplacementNamed('/Pages', arguments: 2);
      },
    );
  }

  Future notificationOnMessage(Map<String, dynamic> message) async {
    Fluttertoast.showToast(
      msg: message['notification']['title'],
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.TOP,
      timeInSecForIosWeb: 5,
    );
  }

  Future<void> checkSpecialPermissionForCall() async {
    int start = DateTime.now().millisecondsSinceEpoch;
    // TODO xin cấp quyền
    Map<Permission, PermissionStatus> statuses = await [
      Permission.microphone,
      Permission.camera,
    ].request();
    int end = DateTime.now().millisecondsSinceEpoch;
    print('quanth: timer1 splash - checkSpecialPermissionForCall = ${end-start} milis');
  }

  Future<void> saveCurrentLocation() async {
    int start = DateTime.now().millisecondsSinceEpoch;
    userRepo.saveCurrentLocation();
    int end = DateTime.now().millisecondsSinceEpoch;
    print('quanth: timer1 splash - saveCurrentLocation = ${end-start} milis');
  }

  Future<void> loadData() async {
    int start = DateTime.now().millisecondsSinceEpoch;
    shouldShowForceUpdatePopup.addListener(() {
      if (shouldShowForceUpdatePopup.value)
        DialogHelper.showForceUpdatePopup();
    });
    progress.addListener(() {
      double progress1 = 0;
      progress.value.values.forEach((_progress) {
        progress1 += _progress;
      });
      if (progress == 100 && !shouldShowForceUpdatePopup.value) {
        // loadLocalAddress();
      }
    });
    int end = DateTime.now().millisecondsSinceEpoch;
    print('quanth: timer1 splash - loadData = ${end-start} milis');
  }

  Future<void> getCurrentLocation() async {
    int start = DateTime.now().millisecondsSinceEpoch;
    settingRepo.getCurrentLocation();
    int end = DateTime.now().millisecondsSinceEpoch;
    print('quanth: timer1 splash - getCurrentLocation = ${end-start} milis');
  }

  Future<void> getCurrentUser() async {
    int start = DateTime.now().millisecondsSinceEpoch;
    userRepo.getCurrentUser().then((currentUser) async {
      // when open app, if logged in, update Device Token
      if (currentUser?.id?.isNotEmpty == true) {
        try {
          // try to get user information to check apiToken valid or not
          final user = await userRepo.getUser(int.parse(currentUser.id));
          if (user?.id?.isNotEmpty == true) {
            // when apiToken is valid, try to update Device Token
            ChatRepository().updateDeviceToken(user?.id);
          }
        } catch (e) {}
      }
    });
    int end = DateTime.now().millisecondsSinceEpoch;
    print('quanth: timer1 splash - getCurrentUser = ${end-start} milis');
  }

  Future<void> initSettings() async {
    settingRepo.initSettings();
  }
}
