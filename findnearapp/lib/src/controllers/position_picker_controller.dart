import 'dart:async';
import 'package:findnear/src/utils/utils.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:google_maps_webservice/places.dart' as place;
import '../helpers/maps_util.dart';
import '../repository/settings_repository.dart' as settingRepo;

class PositionPickerController extends ControllerMVC {
  CameraPosition cameraPosition;
  MapsUtil mapsUtil = new MapsUtil();
  Completer<GoogleMapController> mapController = Completer();
  Set<Marker> markers = new Set();
  place.Prediction searchPosition;

  void setInitPosition(LatLng initPosition) async {
    if (initPosition != null) {
      setState(() {
        cameraPosition = CameraPosition(
          target: LatLng(initPosition.latitude, initPosition.longitude),
          zoom: 14.4746,
        );
        markers.add(
          Marker(
            markerId: MarkerId('my-position'),
            position: LatLng(initPosition.latitude, initPosition.longitude),
          ),
        );
      });
      return;
    }

    Utils.determinePosition().then((locationData) {
      setState(() {
        cameraPosition = CameraPosition(
          target: LatLng(locationData.latitude, locationData.longitude),
          zoom: 14.4746,
        );
        markers.add(
          Marker(
            markerId: MarkerId('my-position'),
            position: LatLng(locationData.latitude, locationData.longitude),
          ),
        );
      });
    }).catchError((e){
      cameraPosition = CameraPosition(
        target: LatLng(21.029832, 105.852605),
        zoom: 14.4746,
      );
      markers.add(
        Marker(
          markerId: MarkerId('my-position'),
          position: LatLng(21.029832, 105.852605),
        ),
      );
    });
  }

  Future<Null> displayPrediction(place.Prediction p) async {
    final GoogleMapController controller = await mapController.future;

    if (p != null) {
      setState(() {
        searchPosition = p;
      });
      place.PlacesDetailsResponse detail = await place.GoogleMapsPlaces(apiKey: settingRepo.setting.value.googleMapsKey).getDetailsByPlaceId(p.placeId);

      double lat = detail.result.geometry.location.lat;
      double lng = detail.result.geometry.location.lng;

      controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(lat, lng),
        zoom: 14.4746,
      )));
    }
  }

  void onCameraIdle() async {
    setState(() {
      markers.clear();
      markers.add(
        Marker(
          markerId: MarkerId('my-position'),
          position: LatLng(cameraPosition.target.latitude, cameraPosition.target.longitude),
        ),
      );
    });
  }
}
