import 'package:findnear/src/controllers/profile_controller.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../../generated/l10n.dart';
import '../models/comment.dart';
import '../repository/comment_repository.dart';

class CommentController extends ControllerMVC {
  CommentRepository _commentRepository;
  GlobalKey<ScaffoldState> scaffoldKey;
  ProfileController profileController = new ProfileController();
  int currentOffsetComment = 0;
  bool canLoadMoreComment = true;
  List<Comment> comments = [];

  CommentController(postId) {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    _commentRepository = new CommentRepository();
    listenForComments(postId);
  }

  void listenForComments(postId, {String message}) async {
    final Stream<Comment> stream = await _commentRepository.getListComment(postId, currentOffsetComment);
    await stream.listen((Comment _comment) {
      setState(() {
        comments.add(_comment);
      });
    }, onError: (a) {
      ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
        content: Text(S.of(state.context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (currentOffsetComment == comments.length) {
        setState(() {
          canLoadMoreComment = false;
        });
      }
      setState(() {
        currentOffsetComment = comments.length;
      });
      if (message != null) {
        ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  void addComment({comment, postId, pathImage = null}) async {
    _commentRepository.addComment(comment: comment, pathImage: pathImage).then((value) {
      this.listenForComments(postId);
    });
  }

  void deleteComment(String commentId) async {
    try {
      final result = await _commentRepository.deleteComment(commentId: commentId);
      if (result) {
        ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
          content: Text(S.of(Get.context).success),
        ));
        setState(() {
          comments.removeWhere((element) => element.id == commentId);
        });
      } else {
        ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
          content: Text(S.of(Get.context).failure),
        ));
      }
    } catch (e) {
      logger.e(e);
      ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
        content: Text(S.of(Get.context).failure),
      ));
    }
  }
}
