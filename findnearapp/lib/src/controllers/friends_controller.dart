import 'package:findnear/src/repository/group_repository.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../../generated/l10n.dart';
import '../models/user.dart';
import '../repository/friends_repository.dart' as repository;

class FriendsController extends ControllerMVC {
  GlobalKey<ScaffoldState> scaffoldKey;
  int currentOffset = 0;
  bool canLoadMore = true;
  List<User> friends = [];

  FriendsController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    listenForFriend();
  }

  void listenForFriend({String message, String keyword = '', bool isClearData: false}) async {
    final stream = await repository.getListFriend(keyword: keyword, currentOffset: currentOffset);
    if (isClearData == true) {
      friends.clear();
    }
    stream.listen((User friend) {
      setState(() {
        friends.add(friend);
      });
    }, onError: (a) {
      ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
        content: Text(S.of(state.context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (currentOffset == friends.length) {
        setState(() {
          canLoadMore = false;
        });
      }
      setState(() {
        currentOffset = friends.length;
      });
      if (message != null) {
        ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  void unfriend(String idUserTarget, String type) async {
    repository.addFriend(idUserTarget, 'delete').then((value) {
        var dataFriends = friends;
        var indexOfPost = friends.indexWhere((element) => element.id == idUserTarget);
        if (indexOfPost != -1) {
          dataFriends.removeAt(indexOfPost);
          setState(() {
            friends = dataFriends;
          });
        }
        ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
          content: Text(S.of(state.context).removePostSuccessful),
        ));
      this.listenForFriend();
      scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(S.of(state.context).successfulUnfriend)));
    });
  }
}
