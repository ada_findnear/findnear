import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:findnear/src/controllers/chat_controller.dart';
import 'package:findnear/src/models/chat.dart';
import 'package:findnear/src/models/entities/communication/entities/group_config_entity.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/entities/communication/response/conversation_entity.dart';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../models/conversation.dart';
import '../models/media.dart';
import '../models/user.dart';
import '../repository/chat_repository.dart';
import '../repository/group_repository.dart';
import '../repository/user_repository.dart';

class GroupController extends ControllerMVC {
  GroupRepository _groupRepository;
  GlobalKey<ScaffoldState> scaffoldKey;
  int currentOffset = 0;
  bool canLoadMore = true;
  List<User> friends = [];
  Conversation conversation;
  ChatRepository _chatRepository;
  Stream<QuerySnapshot> chats;
  ChatController _chatController;

  GroupController({ignoreId = ''}) {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    _groupRepository = new GroupRepository();
    _chatRepository = new ChatRepository();
    _chatController = new ChatController();
    listenForFriend(ignoreId: ignoreId);
  }

  void listenForFriend(
      {String message, String keyword = '', String ignoreId = '', bool isClearData: false}) async {
    final stream = await _groupRepository.getListFriend(
        keyword: keyword, ignoreId: ignoreId, currentOffset: currentOffset);
    if (isClearData == true) {
      friends.clear();
    }
    stream.listen((User friend) {
      setState(() {
        friends.add(friend);
      });
    }, onError: (a) {
      ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
        content: Text(S.of(state.context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (currentOffset == friends.length) {
        setState(() {
          canLoadMore = false;
        });
      }
      setState(() {
        currentOffset = friends.length;
      });
      if (message != null) {
        ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  Future<ConversationEntity> createGroup({String nameGroup = '', listFriend, String pathImage = ''}) async {
    List<User> users = [];
    List<ShortUserEntity> shortUsers = [];

    users.add(currentUser.value);
    shortUsers.add(ShortUserEntity.fromUser(currentUser.value));
    Conversation _conversation = new Conversation(users);
    Media image = new Media();

    _conversation.id = UniqueKey().toString();
    print('GroupConversationId ${_conversation.id}');
    _conversation.lastMessageTime = DateTime.now().toUtc().millisecondsSinceEpoch;
    _conversation.name = nameGroup;
    _conversation.readByUsers = [currentUser.value?.id ?? ""];
    _conversation.idAdmin = currentUser.value?.id ?? "";
    if (_conversation.visibleToUsers == null) {
      _conversation.visibleToUsers = [];
    }
    listFriend.forEach((element) {
      _conversation.users.add(element);
      _conversation.visibleToUsers.add(element.id);
      shortUsers.add(ShortUserEntity.fromUser(element));
    });
    _conversation.visibleToUsers.sort((a, b) => a.compareTo(b));
    _conversation.type = _conversation.users.length > 2 ? "GROUP": "PEEK";
    if(_conversation.type == "GROUP"){
      if (pathImage != '' && pathImage != null) {
        image = await _chatRepository.uploadFile(pathImage, "group_avatar");
      }
      _conversation.groupAvatar = image.url;
      _conversation.config = GroupConfigEntity(groupAvatar: image.url, shortUsers: shortUsers);
    } else {
      String currentUserId = currentUser.value.id;
      User user = users.firstWhere((element) => element.id != currentUserId);
      String imgUrl = user.image.thumb ?? "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
      _conversation.groupAvatar = imgUrl;
      _conversation.config = GroupConfigEntity(groupAvatar: imgUrl, shortUsers: shortUsers);
    }
    setState(() {
      conversation = _conversation;
    });

    ConversationEntity result = await _chatRepository.createConversations(_conversation);
    print('quan ${result.toJson().toString()}');
    return result;
    // _chatRepository.createConversation(_conversation).then((value) {
    //   listenForChats(_conversation);
    //   _chatController.addMessage(
    //       _conversation,
    //       jsonEncode({"text": currentUser.value?.name ?? "", "typeSystem": TypeSystem.createGroup.index}),
    //       ChatType.system.index);
    // });
  }

  listenForChats(Conversation _conversation) async {
    _conversation.readByUsers.add(currentUser.value?.id ?? "");
    _chatRepository.getChats(_conversation).then((snapshots) {
      setState(() {
        chats = snapshots;
        //chats.
      });
    });
  }
}
