import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:findnear/src/pages/profile_user/profile_user_view.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:get/get.dart';
import 'package:flutter/cupertino.dart';
import '../models/user.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../models/chat.dart';
import '../models/conversation.dart';
import '../repository/chat_repository.dart';
import '../repository/notification_repository.dart';
import '../repository/user_repository.dart';
import '../models/media.dart';

class ChatController extends ControllerMVC {
  Conversation conversation;
  ChatRepository _chatRepository;
  Stream<QuerySnapshot> conversations;
  Stream<QuerySnapshot> chats;
  GlobalKey<ScaffoldState> scaffoldKey;
  Media currentMedia = null;
  List newUser = [];
  User otherUser;

  ChatController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    _chatRepository = new ChatRepository();
//    _createConversation();
  }

  signIn() {
    //_chatRepository.signUpWithEmailAndPassword(currentUser.value.email, currentUser.value?.apiToken);
//    _chatRepository.signInWithToken(currentUser.value?.apiToken);
  }

  createConversation(Conversation _conversation) async {
    var listUser = [_conversation.users[0], currentUser.value];
    listUser.sort((a, b) => a.id.compareTo(b.id));
    _conversation.users = listUser;
    _conversation.lastMessageTime = DateTime.now().toUtc().millisecondsSinceEpoch;
    _conversation.readByUsers = [currentUser.value?.id ?? ""];
    setState(() {
      conversation = _conversation;
    });
    _chatRepository.createConversation(_conversation).then((value) {
      listenForChats(_conversation);
    });
  }

  listenForConversations() async {
    _chatRepository.getUserConversations(currentUser.value?.id ?? "").then((snapshots) {
      setState(() {
        conversations = snapshots;
      });
    });
  }

  listenForChats(Conversation _conversation) async {
    _conversation.readByUsers.add(currentUser.value?.id ?? "");
    _chatRepository.getChats(_conversation).then((snapshots) {
      setState(() {
        chats = snapshots;
      });
    });
  }

  addMessage(Conversation _conversation, String text, int type, {senderId = null}) {
    Chat _chat = new Chat(
      DateTime.now().toUtc().millisecondsSinceEpoch.toString(),
      text,
      DateTime.now().toUtc().millisecondsSinceEpoch,
      senderId != null ? senderId : currentUser.value?.id ?? "",
      currentMedia?.url,
      currentMedia?.thumb,
      type,
    );
    if (_conversation.id == null) {
      _conversation.id = UniqueKey().toString();
      createConversation(_conversation);
    }
    if (text != '') {
      _conversation.lastMessage = text;
    } else {
      if (type == ChatType.image.index) {
        _conversation.lastMessage = jsonEncode({"text": currentUser.value?.name ?? "", "typeSystem": TypeSystem.imageChat.index});
      } else if (type == ChatType.video.index) {
        _conversation.lastMessage = jsonEncode({"text": currentUser.value?.name ?? "", "typeSystem": TypeSystem.videoChat.index});
      }
    }
    _conversation.lastMessageTime = _chat.time;
    _conversation.readByUsers = [currentUser.value?.id ?? ""];
    _chatRepository.addMessage(_conversation, _chat).then((value) {
      _conversation.users.forEach((_user) {
        if (_user.id != currentUser.value?.id ?? "") {
          sendNotification(text, S.of(Get.context).newMessageFrom + " " + currentUser.value?.name ?? "", _user);
        }
      });
      setState(() {
        currentMedia = null;
        conversation = _conversation;
      });
    });
  }

  uploadMedia (String pathMedia, int type) {
    _chatRepository.uploadFile(pathMedia, "media_message").then((Media _media) {
      setState(() {
        currentMedia = _media;
      });
      addMessage(conversation, "", type);
    });
  }

  orderSnapshotByTime(AsyncSnapshot snapshot) {
    final docs = snapshot.data.docs;
    docs.sort((QueryDocumentSnapshot a, QueryDocumentSnapshot b) {
      var time1 = a.get('time');
      var time2 = b.get('time');
      return time2.compareTo(time1) as int;
    });
    return docs;
  }

  updateGroupName({Conversation conversation, String nameGroup}) {
    conversation.name = nameGroup;
    _chatRepository.updateConversation(conversation.id, conversation.toUpdatedMap()).then((value) {
      listenForConversations();
      ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
        content: Text(S.of(state.context).updatePostSuccessful),
      ));
    });
  }

  updateAvatar({Conversation conversation, String pathImage}) async {
    Media image = new Media();
    if (pathImage != '' && pathImage != null) {
      image = await _chatRepository.uploadFile(pathImage, "group_avatar");
    }
    conversation.groupAvatar = image.url;
    _chatRepository.updateConversation(conversation.id, conversation.toUpdatedMap()).then((value) {
      listenForConversations();
      ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
        content: Text(S.of(state.context).updatePostSuccessful),
      ));
    });
  }

  addUser({Conversation conversation, List<User> listUser}) async {
    List userNames = [];
    listUser.forEach((element) {
      conversation.users.add(element);
      conversation.visibleToUsers.add(element.id);
      userNames.add(element.name);
    });
    _chatRepository.updateConversation(conversation.id, conversation.toUpdatedMap()).then((value) {
      listenForConversations();
      this.addMessage(conversation, jsonEncode({"text": userNames.join(', '), "typeSystem": TypeSystem.addMember.index}), ChatType.system.index);
      ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
        content: Text(S.of(state.context).removeUserSuccessful),
      ));
    });
  }

  removeUser({Conversation conversation, String userID}) async {
    var indexUser = conversation.users.indexWhere((element) => element.id == userID);
    if (currentUser.value?.id == userID) {
      conversation.idAdmin = conversation.visibleToUsers.firstWhere((element) => element != currentUser.value?.id);
    }
    conversation.visibleToUsers.remove(userID);
    _chatRepository.removeUserConversation(conversation.id, conversation.toUpdatedMap(), indexUser).then((value) {
      listenForConversations();
      this.addMessage(conversation, jsonEncode({
        "text": conversation.toUpdatedMap()['users'][indexUser]['name'],
        "typeSystem": currentUser.value?.id != userID ? TypeSystem.removeMember.index : TypeSystem.outGroup.index
      }), ChatType.system.index);
      ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
        content: Text(S.of(state.context).removeUserSuccessful),
      ));
    });
  }

  removeConversation({String conversationId}) async {
    _chatRepository.removeConversation(conversationId).then((value) {
      listenForConversations();
      ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
        content: Text(S.of(state.context).removeUserSuccessful),
      ));
    });
  }

  navigateToProfile() async {
    if (conversation.idAdmin == null) {
      var targetUser = conversation.users.firstWhere((element) => element.id != (currentUser.value?.id ?? ""));
      User targetUserModel = await getUser(int.parse(targetUser.id));
      NavigatorUtils.navigateToProfilePage(targetUserModel.id);
    }
  }

  void listenForUser(String userId) async {
    final User data = await getUser(int.tryParse(userId) ?? 0);
    if (data.isBlocker) {
      DialogHelper.showAlertDialog(
        context: Get.context,
        title: S.of(Get.context).error,
        description: "Không thể nhắn tin do bạn đang bị chặn",
        okText: S.of(Get.context).ok,
        okFunction: () {
          Get.back();
        },
      );
    } else if (data.isBlocking) {
      DialogHelper.showAlertDialog(
        context: Get.context,
        title: S.of(Get.context).error,
        description: "Không thể nhắn tin do bạn đang chặn người này",
        okText: S.of(Get.context).ok,
        okFunction: () {
          Get.back();
        },
      );
    }
  }

  Future<bool> doBlockUser(String userId) async {
    try {
      final User data = await blockUser(int.tryParse(userId) ?? 0);
      if (data != null) {
        ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
          content: Text(S.of(Get.context).blockUserSuccessfully),
        ));
        return true;
      } else {
        ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
          content: Text(S.of(Get.context).blockUserFailure),
        ));
        return false;
      }
    } catch (e) {
      ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
        content: Text(S.of(Get.context).blockUserFailure),
      ));
      return false;
    }
  }
}
