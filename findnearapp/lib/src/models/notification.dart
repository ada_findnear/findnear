import 'dart:convert';

import 'package:findnear/src/models/user.dart';

enum NotificationType {
  friendRequest,
  friendRequestAccepted,
  likePost,
  likeComment,
  commentPost,
  statusChangedOrder,
  userCreateMoment,
  newOrder,
  friendPost,
  winGame,
}

extension NotificationTypeExtension on NotificationType {
  static const String statusChangedOrder = "App\\Notifications\\StatusChangedOrder";
  static const String commentPost = "App\\Notifications\\CommentPost";
  static const String likePost = "App\\Notifications\\LikePost";
  static const String likeComment = "App\\Notifications\\LikeComment";
  static const String friendRequest = "App\\Notifications\\RequestAddFriend";
  static const String friendRequestAccepted = "App\\Notifications\\AgreeAddFriend";
  static const String userCreateMoment = "App\\Notifications\\UserCreateMoment";
  static const String newOrder = "App\\Notifications\\NewOrder";
  static const String friendPost = "App\\Notifications\\FriendPost";
  static const String winGame = "App\\Notifications\\WinGame";

  String get rawString {
    switch (this) {
      case NotificationType.friendRequest:
        return friendRequest;
      case NotificationType.likePost:
        return likePost;
      case NotificationType.likeComment:
        return likeComment;
      case NotificationType.commentPost:
        return commentPost;
      case NotificationType.statusChangedOrder:
        return statusChangedOrder;
      case NotificationType.friendRequestAccepted:
        return friendRequestAccepted;
      case NotificationType.userCreateMoment:
        return userCreateMoment;
      case NotificationType.newOrder:
        return newOrder;
      case NotificationType.friendPost:
        return friendPost;
      case NotificationType.winGame:
        return winGame;
      default:
        return "";
    }
  }
}

extension NotificationTypeStringExtension on String {
  NotificationType get toNotificationType {
    if (this == NotificationType.friendRequest.rawString) {
      return NotificationType.friendRequest;
    } else if (this == NotificationType.likePost.rawString) {
      return NotificationType.likePost;
    } else if (this == NotificationType.likeComment.rawString) {
      return NotificationType.likeComment;
    } else if (this == NotificationType.commentPost.rawString) {
      return NotificationType.commentPost;
    } else if (this == NotificationType.statusChangedOrder.rawString) {
      return NotificationType.statusChangedOrder;
    } else if (this == NotificationType.friendRequestAccepted.rawString) {
      return NotificationType.friendRequestAccepted;
    } else if (this == NotificationType.userCreateMoment.rawString) {
      return NotificationType.userCreateMoment;
    } else if (this == NotificationType.newOrder.rawString) {
      return NotificationType.newOrder;
    } else if (this == NotificationType.friendPost.rawString) {
      return NotificationType.friendPost;
    } else if (this == NotificationType.winGame.rawString) {
      return NotificationType.winGame;
    }
    return null;
  }
}

class Notification {
  String id;
  String type;
  String avatarPath;
  Map<String, dynamic> data;
  bool read;
  DateTime createdAt;
  User user;

  Notification({
    this.id,
    this.type,
    this.avatarPath,
    this.data,
    this.read,
    this.createdAt,
    this.user,
  });

  Notification.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      type = jsonMap['type'] != null ? jsonMap['type'].toString() : '';
      data = jsonMap['data'] != null ? jsonDecode(jsonMap['data']) : {};
      avatarPath = data['image'] != null ? data['image'] : "";
      read = jsonMap['read_at'] != null ? true : false;
      createdAt = DateTime.parse(jsonMap['created_at']);
      user = jsonMap['user'] != null ? User.fromJSON(jsonMap['user']) : null;
    } catch (e) {
      id = '';
      type = '';
      avatarPath = '';
      data = {};
      read = false;
      createdAt = new DateTime(0);
      print(e);
    }
  }

  Map markReadMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["read_at"] = !read;
    return map;
  }

  NotificationType get getNotificationType => this.type.toNotificationType;
}
