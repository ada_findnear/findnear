import 'package:findnear/src/models/media.dart';
import 'package:intl/intl.dart';

class Game {
  String code;
  Object created_at;
  int current_phase;
  List<Object> custom_fields;
  String description;
  bool has_media;
  int id;
  List<Media> media;
  String name;
  int spent_points;
  int total_player;
  Object updated_at;

  Game(
      {this.code,
      this.created_at,
      this.current_phase,
      this.custom_fields,
      this.description,
      this.has_media,
      this.id,
      this.media,
      this.name,
      this.spent_points,
      this.total_player,
      this.updated_at});

  static Game fromJson(Map<String, dynamic> json) {
    return Game(
      code: json['code'],
      // created_at: json['created_at'] != null ? Object.fromJson(json['created_at']) : null,
      current_phase: json['current_phase'],
      description: json['description'],
      has_media: json['has_media'],
      id: json['id'],
      media: json['media'] != null
          ? (json['media'] as List).map((i) => Media.fromJSON(i)).toList()
          : null,
      name: json['name'],
      spent_points: json['spent_points'],
      total_player: json['total_player'],
      //updated_at: json['updated_at'] != null ? Object.fromJson(json['updated_at']) : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['current_phase'] = this.current_phase;
    data['description'] = this.description;
    data['has_media'] = this.has_media;
    data['id'] = this.id;
    data['name'] = this.name;
    data['spent_points'] = this.spent_points;
    data['total_player'] = this.total_player;
    // if (this.created_at != null) {
    //     data['created_at'] = this.created_at.toJson();
    // }
    // if (this.custom_fields != null) {
    //     data['custom_fields'] = this.custom_fields.map((v) => v.toJson()).toList();
    // }
    if (this.media != null) {
      data['media'] = this.media.map((v) => v.toMap()).toList();
    }
    // if (this.updated_at != null) {
    //     data['updated_at'] = this.updated_at.toJson();
    // }
    return data;
  }

  String get firstIcon => media?.isNotEmpty == true ? media[0].icon : '';

  String get firstThumb => media?.isNotEmpty == true ? media[0].thumb : '';
}

class GameHistory {
  String data;
  String created_at;
  int game_id;
  int id;
  int phase;
  int point;
  int winPoint;
  int wonNumber;
  String updated_at;
  int user_id;

  DateTime get createAtDate => DateTime.tryParse(created_at);

  DateTime get createAtDay =>
      DateTime(createAtDate.year, createAtDate.month, createAtDate.day);

  String get lotteryDataString => lotteryData.join(' ');
  List<String> get lotteryData => data.replaceAll(' ', '')
      .replaceAll('[', '')
      .replaceAll(']', '')
      .split(',')
      .map((e) => NumberFormat('00').format(int.parse(e)))
      .toList();

  String get fndData => data.replaceAll(' ', '')
      .replaceAll('[', '')
      .replaceAll(']', '')
      .replaceAll('\"', '')
      .replaceAll('\\', '');

  GameHistory(
      {this.data,
      this.created_at,
      this.game_id,
      this.id,
      this.phase,
      this.point,
      this.winPoint,
      this.wonNumber,
      this.updated_at,
      this.user_id});

  factory GameHistory.fromJson(Map<String, dynamic> json) {
    return GameHistory(
      data: json['data'],
      created_at: json['created_at'],
      game_id: json['game_id'],
      id: json['id'],
      phase: json['phase'],
      point: json['point'],
      winPoint: json['win_point'],
      wonNumber: json['won_number'],
      updated_at: json['updated_at'],
      user_id: json['user_id'],
    );
  }

}
