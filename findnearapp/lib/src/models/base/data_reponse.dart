class ArrayDataResponse<T> {
  List<T> data = List.empty();
  String message = '';
  bool success = false;
  PageAble pageAble = PageAble();

  ArrayDataResponse({this.data, this.message, this.success, this.pageAble});

  factory ArrayDataResponse.fromJson(
    Map<String, dynamic> json,
    T Function(Map<String, dynamic> item) itemParser,
  ) {
    return ArrayDataResponse(
      data: json['data'] != null
          ? (json['data'] as List).map((e) => itemParser(e)).toList()
          : List.empty(),
      message: json['message'] ?? '',
      success: json['success'] ?? false,
      pageAble: PageAble.fromJson(json['pageAble']),
    );
  }
}

class DataResponse<T> {
  T data;
  String message;
  bool success;

  DataResponse({this.data, this.message, this.success});

  factory DataResponse.fromJson(Map<String, dynamic> json,
      T Function(Map<String, dynamic> item) dataParser) {
    return DataResponse(
      data: json['data'] != null ? dataParser(json['data']) : null,
      message: json['message'],
      success: json['success'],
    );
  }
}

class PageAble {
  int total = 0;
  int pageSize = 0;
  int pageIndex = 0;

  PageAble({this.total, this.pageSize, this.pageIndex});

  factory PageAble.fromJson(Map<String, dynamic> json) {
    if (json == null) return PageAble();

    return PageAble(
      total: json['total'] ?? 0,
      pageSize: json['pageSize'] ?? 0,
      pageIndex: json['pageIndex'] ?? 0,
    );
  }
}
