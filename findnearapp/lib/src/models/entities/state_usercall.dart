class StateUserCall {
  StateUserCall({
    this.status,
    this.currentServerId,
    this.isCalling,
    this.devicePlatform
  });

  bool status;
  String currentServerId;
  bool isCalling;
  String devicePlatform;

  factory StateUserCall.fromJson(Map<String, dynamic> json) => StateUserCall(
    status: json["status"] ?? false,
    currentServerId: json["currentServerId"],
    isCalling: json["isCalling"] ?? false,
    devicePlatform: json["devicePlatform"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "currentServerId": currentServerId,
    "isCalling": isCalling,
    "devicePlatform":devicePlatform,
  };
}
