class PageAbleEntity {
  int total;
  int pageSize;
  int pageIndex;

  PageAbleEntity({
    this.total,
    this.pageSize,
    this.pageIndex,
  });

  factory PageAbleEntity.fromJson(Map<String, dynamic> json) {
    return PageAbleEntity(
      total: json['total'] ?? 0,
      pageSize: json['pageSize'] ?? 0,
      pageIndex: json['pageIndex'] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    data['pageSize'] = this.pageSize;
    data['pageIndex'] = this.pageIndex;
    return data;
  }
}
