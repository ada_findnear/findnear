class CreateLiveEntity {
  int user_id;
  String description;
  String status;
  int id;

  CreateLiveEntity({this.user_id, this.description, this.status, this.id});

  factory CreateLiveEntity.fromJson(Map<String, dynamic> json) =>
      CreateLiveEntity(
        user_id: json["user_id"],
        description: json["description"],
        status: json["status"],
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "user_id": user_id,
        "description": description,
        "status": status,
        "id": id
      };
}
