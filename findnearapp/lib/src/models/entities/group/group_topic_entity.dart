class GroupTopicEntity {
  int id;
  String title;
  bool active;

  GroupTopicEntity({
    this.id,
    this.title,
    this.active,
  });

  factory GroupTopicEntity.fromJson(Map<String, dynamic> json) {
    return GroupTopicEntity(
      id: json["id"],
      title: json["title"],
      active: json["active"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "title": title,
      "active": active,
    };
  }
}
