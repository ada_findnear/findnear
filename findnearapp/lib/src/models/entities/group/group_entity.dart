import 'dart:convert';

import 'package:findnear/src/models/media.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/enum_utils.dart';
import 'package:flutter/material.dart';

class GroupEntity {
  int id;
  String title;
  int group_type_id;
  int verified;
  String status;
  String viewer;
  int owner_id;
  String updated_at;
  String created_at;
  bool has_media;
  List<Media> media;
  String content;
  User owner;
  bool joined;
  int members_count;
  int posts_count;

  String get viewerDisplay {
    if (viewerEnum == null) {
      return "";
    }
    return viewerEnum.display;
  }

  IconData get viewerIcon {
    if (viewer == null || viewer.isEmpty) {
      return Icons.lock_outline;
    }
    switch (viewer) {
      case "0":
        return Icons.lock_outline;
      case "1":
        return Icons.people;
      case "2":
        return Icons.public_sharp;
      default:
        return Icons.lock_outline;
    }
  }

  GroupViewerStatus get viewerEnum {
    return (viewer ?? "").toGroupViewerStatus;
  }

  bool get isMyGroup {
    final currentUserId = currentUser.value.id;
    if (currentUserId == null) {
      return false;
    }

    return currentUserId == owner_id.toString();
  }

  String get groupThumbnail {
    if (media != null && media.length > 0) {
      return media.first.thumb;
    }
    return "";
  }

  // TODO: - không phải trường status
  String get requestJoinGroupStatusDisplay {
    if (status == null || status.isEmpty) {
      return "";
    }
    switch (status) {
      case "0":
        return "Bị từ chối";
      case "1":
        return "Chờ phê duyệt";
      case "2":
        return "Đã phê duyệt";
      default:
        return "";
    }
  }

  GroupEntity({
    this.id,
    this.title,
    this.group_type_id,
    this.verified,
    this.status,
    this.viewer,
    this.owner_id,
    this.updated_at,
    this.created_at,
    this.has_media,
    this.media,
    this.content,
    this.owner,
    this.joined,
    this.members_count,
    this.posts_count,
  });

  factory GroupEntity.fromJson(Map<String, dynamic> json) {
    List<Media> media = json['media'] != null
        ? List.from(json['media'])
            .map((element) => () {
                  var media = Media.fromJSON(element);
                  return media;
                }())
            .toList()
        : [];
    return GroupEntity(
      id: json["id"],
      title: json["title"],
      group_type_id: json["group_type_id"],
      verified: json["verified"],
      status: json["status"],
      viewer: json["viewer"]?.toString(),
      owner_id: json["owner_id"],
      updated_at: json["updated_at"],
      created_at: json["created_at"],
      has_media: json["has_media"],
      media: media,
      content: json["content"],
      owner: json["owner"] != null ? User.fromJSON(json["owner"]) : null,
      joined: json["joined"],
      members_count: json["members_count"],
      posts_count: json["posts_count"],
    );
  }

  Map<String, dynamic> toJson() {
    var media = "";
    if (this.media?.isNotEmpty == true)
      media = jsonDecode(
        this.media?.map((e) => e?.toMap())?.toList()?.toString(),
      );
    return {
      "id": id,
      "title": title,
      "group_type_id": group_type_id,
      "verified": verified,
      "status": status,
      "viewer": viewer,
      "owner_id": owner_id,
      "updated_at": updated_at,
      "created_at": created_at,
      "has_media": has_media,
      "media": media,
      "content": content,
      "owner": this.owner?.toMap()?.toString(),
      "joined": this.joined,
      "members_count": members_count,
      "posts_count": posts_count,
    };
  }

  GroupEntity copyWith({
    int id,
    String title,
    int group_type_id,
    int verified,
    String status,
    String viewer,
    int owner_id,
    String updated_at,
    String created_at,
    bool has_media,
    List<Media> media,
    String content,
    User owner,
    bool joined,
    int members_count,
    int posts_count,
  }) {
    return GroupEntity(
      id: id ?? this.id,
      title: title ?? this.title,
      group_type_id: group_type_id ?? this.group_type_id,
      verified: verified ?? this.verified,
      status: status ?? this.status,
      viewer: viewer ?? this.viewer,
      owner_id: owner_id ?? this.owner_id,
      updated_at: updated_at ?? this.updated_at,
      created_at: created_at ?? this.created_at,
      has_media: has_media ?? this.has_media,
      media: media ?? this.media,
      content: content ?? this.content,
      owner: owner ?? this.owner,
      joined: joined ?? this.joined,
      members_count: members_count ?? this.members_count,
      posts_count: posts_count ?? this.posts_count,
    );
  }
}
