class LiveTokenEntity {
  String token;
  int uid;
  String channel_name;

  LiveTokenEntity({this.token, this.uid, this.channel_name});

  factory LiveTokenEntity.fromJson(Map<String, dynamic> json) =>
      LiveTokenEntity(
        token: json["token"],
        uid: json["uid"],
        channel_name: json["channel_name"],
      );

  Map<String, dynamic> toJson() =>
      {"token": token, "uid": uid, "channel_name": channel_name};
}
