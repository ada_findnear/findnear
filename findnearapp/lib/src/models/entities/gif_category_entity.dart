import 'package:tenor/tenor.dart';

class GifCategoryEntity {
  final TenorCategories tenorCategories;
  final TenorResponse tenorResponse;

  GifCategoryEntity({
    this.tenorCategories,
    this.tenorResponse,
  });

  GifCategoryEntity copyWith({
    TenorCategories tenorCategories,
    TenorResponse tenorResponse,
  }) {
    return GifCategoryEntity(
      tenorCategories: tenorCategories ?? this.tenorCategories,
      tenorResponse: tenorResponse ?? this.tenorResponse,
    );
  }
}
