class UnreadMessageCountEntity {
  SenderId id;
  int totalMessageUnRead;

  UnreadMessageCountEntity({this.id, this.totalMessageUnRead});

  factory UnreadMessageCountEntity.fromJson(Map<String, dynamic> json) =>
      UnreadMessageCountEntity(
        id: json["id"],
        totalMessageUnRead: json["totalMessageUnRead"],
      );

  Map<String, dynamic> toJson() => {
    "id": id,
    "totalMessageUnRead": totalMessageUnRead,
  };

}

class SenderId {
  String createBy;

  SenderId({this.createBy});

  factory SenderId.fromJson(Map<String, dynamic> json) =>
      SenderId(
        createBy: json["createBy"],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createBy'] = this.createBy;
    return data;
  }

}