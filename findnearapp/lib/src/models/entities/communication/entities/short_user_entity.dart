import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/bottom_sheet/change_bg_logic.dart';

class ShortUserEntity {
  String id;
  String code;
  String avatar;
  String name;
  String deceiveToken;
  // String groupBackground;

  ShortUserEntity({this.id, this.code, this.avatar, this.name,this.deceiveToken /*this.groupBackground*/});

  factory ShortUserEntity.fromJson(Map<String, dynamic> json) =>
      ShortUserEntity(
        id: json["id"],
        code: json["code"],
        avatar: json["avatar"],
        name: json["name"],
        deceiveToken: json["deceiveToken"],
        /*groupBackground: json["group_background"],*/
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "code": code,
        "avatar": avatar,
        "name": name,
        "deceiveToken": deceiveToken,
        /*"group_background": groupBackground,*/
      };

  static ShortUserEntity fromUser(User user) => ShortUserEntity(
        id: user.id,
        code: user.code,
        avatar: user.image.thumb,
        name: user.name,
        deceiveToken: user.deviceToken,
        /*groupBackground: ChangeBgController.DEFAULT_BACKGROUND,*/
      );
}