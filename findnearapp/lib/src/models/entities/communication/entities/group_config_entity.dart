import 'dart:convert';

import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';

class GroupConfigEntity {
  String groupAvatar;
  List<ShortUserEntity> shortUsers;
  String groupBackground;
  String privateBackground;

  GroupConfigEntity({this.groupAvatar, this.shortUsers, this.groupBackground, this.privateBackground});

  factory GroupConfigEntity.fromJson(Map<String, dynamic> json) {
    String groupAvatar = json["group_avatar"];
    String groupBackground = json["group_background"];
    String privateBackground = json["private_background"];
    List<ShortUserEntity> list = [];
    try {
      list = List<ShortUserEntity>.from(jsonDecode(json["short_users"])
          .map((x) => ShortUserEntity.fromJson(x)));
    } catch (e){
      list = [];
    }

    return GroupConfigEntity(
      groupAvatar: groupAvatar,
      shortUsers: list,
      groupBackground: groupBackground,
      privateBackground: privateBackground
    );
  }


  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['group_avatar'] = groupAvatar;
    data['group_background'] = groupBackground;
    data['private_background'] = privateBackground;
    data['short_users'] = shortUsers != null
        ? jsonEncode(shortUsers.map((e) => e.toJson()).toList()).toString()
        : [];
    return data;
  }
}
