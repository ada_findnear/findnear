class BaseSocketResponse<T> {
  T data;
  String messageType;
  String action;

  BaseSocketResponse({this.data, this.messageType, this.action});

  factory BaseSocketResponse.fromJson(
      Map<String, dynamic> json,
      T Function(dynamic) dataFromJson,
      ) {
    return BaseSocketResponse(
      data: dataFromJson(json['data']),
      action: json['action'],
      messageType: json['messageType'],
    );
  }
}