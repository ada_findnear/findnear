import 'package:findnear/src/models/entities/communication/entities/group_config_entity.dart';
import 'package:findnear/src/models/entities/message_chat/entities/message_chat.dart';
import 'package:global_configuration/global_configuration.dart';

import '../../../conversation.dart';
import '../../../user.dart';

class ConversationEntity {
  String id;
  String uuid;
  String name;
  String createBy;
  String type;
  bool isDisplay;
  GroupConfigEntity config;
  String pin;
  String originId;
  List<String> users;
  int v;
  bool pinTop;
  String createDate;
  int totalMessageUnRead;
  MessageChat lastMessage;

  ConversationEntity({
    this.id,
    this.uuid,
    this.name,
    this.createBy,
    this.type,
    this.isDisplay,
    this.config,
    this.pin,
    this.originId,
    this.users,
    this.v,
    this.pinTop = false,
    this.createDate,
    this.totalMessageUnRead,
    this.lastMessage
  });

  factory ConversationEntity.fromJson(Map<String, dynamic> json) =>
      ConversationEntity(
          id: json["_id"],
          uuid: json["uuid"],
          name: json["name"],
          type: json["type"],
          createBy: json["createBy"],
          isDisplay: json["isDisplay"],
          config: json["config"] != null ? GroupConfigEntity.fromJson(json["config"]) : GroupConfigEntity(groupAvatar: "${GlobalConfiguration().getValue('base_url')}images/image_default.png}"),
          pin: json["pin"],
          originId: json["originId"],
          users: List<String>.from(json["users"].map((x) => x)),
          v: json["__v"],
          pinTop: json["pinTop"],
          createDate: json["createDate"],
          totalMessageUnRead: json["totalMessageUnRead"],
          lastMessage: json["lastMessage"] != null ? MessageChat.fromJson(json["lastMessage"]) : MessageChat(),
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "uuid": uuid,
        "name": name,
        "createBy": createBy,
        "isDisplay": isDisplay,
        "config": config,
        "pin": pin,
        "type": type,
        "originId": originId,
        "users": List<String>.from(users.map((x) => x)),
        "__v": v,
        "pinTop": pinTop,
        "createDate": createDate,
        "totalMessageUnRead": totalMessageUnRead,
        "lastMessage": lastMessage
      };

  Conversation get toConversation => Conversation.copy(
      id: id,
      uuid: uuid,
      name: name,
      readByUsers: [],
      users: [],
      userIds: users,
      lastMessageTime: 1638334856000,
      type: type,
      config: config,
      pinTop: pinTop,
      pin: pin,
      originId: originId,
      createDate: createDate,
      isDisplay: isDisplay,
      createBy: createBy,
      totalMessageUnRead: totalMessageUnRead,
      lastMessageEntity: lastMessage
  );

  static ConversationEntity fromConversation(Conversation conversation) => ConversationEntity(
    id: conversation.id,
    uuid: conversation.uuid,
    name: conversation.name,
    users: conversation.userIds,
    type: conversation.type,
    config: conversation.config,
    pinTop: conversation.pinTop,
    pin: conversation.pin,
    originId: conversation.originId,
    createDate: conversation.createDate,
    isDisplay: conversation.isDisplay,
    createBy: conversation.createBy,
    totalMessageUnRead: conversation.totalMessageUnRead,
    lastMessage: conversation.lastMessageEntity
  );

  Map<String, dynamic> toBodyRequest() => {
    "uuid": uuid,
    "name": name,
    "createBy": createBy,
    "type": type,
    "isDisplay": isDisplay,
    "config": config.toJson(),
    "pin": pin,
    "users": List<String>.from(users.map((x) => x)),
    "pinTop": pinTop,
  };

}
