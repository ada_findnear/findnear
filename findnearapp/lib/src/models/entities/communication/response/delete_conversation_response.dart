import 'package:findnear/src/pages/messages/conversation_list/widgets/conversation_widget.dart';
import 'package:flutter/cupertino.dart';

import '../../../conversation.dart';

class DeleteConversationResponse {
  int deletedCount;

  DeleteConversationResponse({this.deletedCount});

  factory DeleteConversationResponse.fromJson(Map<String, dynamic> json) =>
      DeleteConversationResponse(deletedCount: json["deletedCount"]);

  Map<String, dynamic> toJson() => {"deletedCount": deletedCount};
}
