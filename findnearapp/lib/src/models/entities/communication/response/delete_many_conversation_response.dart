import 'package:findnear/src/models/entities/communication/entities/group_config_entity.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';

import '../../../conversation.dart';
import '../../../media.dart';

class DeleteManyConversationResponse {
  bool status;

  DeleteManyConversationResponse({
    this.status,
  });

  factory DeleteManyConversationResponse.fromJson(Map<String, dynamic> json) =>
      DeleteManyConversationResponse(
          status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
  };
}
