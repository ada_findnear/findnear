import 'package:findnear/src/models/entities/communication/entities/group_config_entity.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';

import '../../../conversation.dart';
import '../../../media.dart';

class UnreadMessageCountParam {
  String createBy;

  UnreadMessageCountParam({
    this.createBy,
  });

  factory UnreadMessageCountParam.fromJson(Map<String, dynamic> json) =>
      UnreadMessageCountParam(
        createBy: json["createBy"],
      );

  Map<String, dynamic> toJson() => {
        "createBy": createBy,
  };
}
