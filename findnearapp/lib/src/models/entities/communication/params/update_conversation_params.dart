import 'dart:convert';

import 'package:findnear/src/models/entities/communication/entities/group_config_entity.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';

import '../../../conversation.dart';
import '../../../media.dart';

class UpdateConversationParam {
  String conversationId;
  List<String> username;
  String shortUsers;

  UpdateConversationParam({
    this.conversationId,
    this.username,
    this.shortUsers,
  });

  factory UpdateConversationParam.fromJson(Map<String, dynamic> json) =>
      UpdateConversationParam(
        conversationId: json["conversationId"],
        username: json["userNames"],
        shortUsers: json["shortUsers"],
      );

  Map<String, dynamic> toJson() => {
        "conversationId": conversationId,
        "userNames": username,
        "shortUsers": shortUsers,
  };

  factory UpdateConversationParam.fromConversation(
      {Conversation conversation, List<ShortUserEntity> members}){
    List<String> memberIds = [];
    if(members?.isNotEmpty ?? false){
      members.forEach((element) {
        memberIds.add(element.code);
      });
    }

    String shortUsers = jsonEncode(conversation.config.shortUsers.map((e) => e.toJson()).toList()).toString();

    return UpdateConversationParam(
        conversationId: conversation.uuid,
        username: memberIds,
        shortUsers: shortUsers,
    );
  }

}
