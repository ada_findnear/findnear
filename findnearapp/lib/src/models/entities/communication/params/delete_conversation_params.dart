import 'package:findnear/src/models/entities/communication/entities/group_config_entity.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';

import '../../../conversation.dart';
import '../../../media.dart';

class DeleteConversationParam {
  List<String> ids;

  DeleteConversationParam({
    this.ids,
  });

  factory DeleteConversationParam.fromJson(Map<String, dynamic> json) =>
      DeleteConversationParam(
        ids: List<String>.from(json["ids"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "ids": ids,
  };
}
