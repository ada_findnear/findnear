import 'package:findnear/src/models/entities/communication/entities/group_config_entity.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';

import '../../../conversation.dart';
import '../../../media.dart';

class PinnedConversationParam {
  String uuid;
  bool pinTop;

  PinnedConversationParam({
    this.uuid,
    this.pinTop
  });

  factory PinnedConversationParam.fromJson(Map<String, dynamic> json) =>
      PinnedConversationParam(
        uuid: json["uuid"],
        pinTop: json["pinTop"],
      );

  Map<String, dynamic> toJson() => {
        "uuid": uuid,
        "pinTop": pinTop,
  };

  factory PinnedConversationParam.fromConversation(Conversation conversation){
    return PinnedConversationParam(
        uuid: conversation.uuid,
        pinTop: !conversation.pinTop,
    );
  }

}
