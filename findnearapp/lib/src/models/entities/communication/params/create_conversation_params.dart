import 'package:findnear/src/models/entities/communication/entities/group_config_entity.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';

import '../../../conversation.dart';
import '../../../media.dart';

class CreateConversationParam {
  String name;
  String createBy;
  String type;
  bool isDisplay;
  String pin;
  List<String> users;
  GroupConfigEntity config;

  CreateConversationParam({
    this.name,
    this.createBy,
    this.type,
    this.isDisplay,
    this.pin,
    this.users,
    this.config,
  });

  factory CreateConversationParam.fromJson(Map<String, dynamic> json) =>
      CreateConversationParam(
        name: json["name"],
        type: json["type"],
        createBy: json["createBy"],
        isDisplay: json["isDisplay"],
        pin: json["pin"],
        users: List<String>.from(json["users"].map((x) => x)),
        config: json["config"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "createBy": createBy,
        "isDisplay": isDisplay,
        "pin": pin,
        "type": type,
        "users": users,
        "config": config.toJson()
  };

  factory CreateConversationParam.fromConversation(Conversation conversation){
    String createBy = Get.find<LocalDataManager>().currentUser.code;
    List<String> users = conversation.users.map((e) => e.code).toList() ?? [];
    String type = users.length > 2 ? "GROUP" : "PEER";
    return CreateConversationParam(
        name: conversation.name,
        createBy: createBy,
        type: type,
        pin: "",
        isDisplay: true,
        users: users,
        config: conversation.config,
    );
  }

}
