import 'package:findnear/src/models/entities/communication/entities/group_config_entity.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';

import '../../../conversation.dart';
import '../../../media.dart';

class ConversationParam {
  String uuid;
  String name;
  String createBy;
  String type;
  bool isDisplay;
  GroupConfigEntity config;
  String pin;
  List<String> users;
  bool pinTop;

  ConversationParam({
    this.uuid,
    this.name,
    this.createBy,
    this.type,
    this.isDisplay,
    this.pin,
    this.users,
    this.config,
    this.pinTop,
  });

  factory ConversationParam.fromJson(Map<String, dynamic> json) =>
      ConversationParam(
        uuid: json["uuid"],
        name: json["name"],
        type: json["type"],
        createBy: json["createBy"],
        isDisplay: json["isDisplay"],
        pin: json["pin"],
        users: List<String>.from(json["users"].map((x) => x)),
        config: json["config"],
        pinTop: json["pinTop"],
      );

  Map<String, dynamic> toJson() => {
        "uuid": uuid,
        "name": name,
        "createBy": createBy,
        "isDisplay": isDisplay,
        "pin": pin,
        "type": type,
        "users": users,
        "config": config,
        "pinTop": pinTop,
  };

  factory ConversationParam.fromConversation(Conversation conversation){
    return ConversationParam(
        uuid: conversation.uuid,
        name: conversation.name,
        createBy: conversation.createBy,
        type: conversation.type,
        isDisplay: conversation.isDisplay,
        config: conversation.config,
        pin: conversation.pin,
        users: conversation.userIds,
        pinTop: conversation.pinTop,
    );
  }

}
