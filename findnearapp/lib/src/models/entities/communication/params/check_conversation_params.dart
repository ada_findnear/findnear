import 'package:findnear/src/models/entities/communication/entities/group_config_entity.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';

import '../../../conversation.dart';
import '../../../media.dart';

class CheckConversationParam {
  String receiver;
  String sender;

  CheckConversationParam({
    this.receiver,
    this.sender,
  });

  factory CheckConversationParam.fromJson(Map<String, dynamic> json) =>
      CheckConversationParam(
        receiver: json["receiver"],
        sender: json["sender"],
      );

  Map<String, dynamic> toJson() => {
        "receiver": receiver,
        "sender": sender,
  };

}
