import 'package:findnear/src/models/entities/communication/entities/group_config_entity.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';

import '../../../conversation.dart';
import '../../../media.dart';

class CreatePinCodeParam {
  String uuid;
  String pinCode;

  CreatePinCodeParam({
    this.uuid,
    this.pinCode
  });

  factory CreatePinCodeParam.fromJson(Map<String, dynamic> json) =>
      CreatePinCodeParam(
        uuid: json["uuid"],
        pinCode: json["pin"],
      );

  Map<String, dynamic> toJson() => {
        "uuid": uuid,
        "pin": pinCode,
  };
}
