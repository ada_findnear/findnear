class AnswerCallEntity{
  String action;
  String type;
  String receiver;
  AnswerEntity answerEntity;

  AnswerCallEntity({
    this.action,
    this.type,
    this.receiver,
    this.answerEntity,
  });

  factory AnswerCallEntity.fromJson(Map<String, dynamic> json) =>
      AnswerCallEntity(
        action: json["action"],
        type: json["type"],
        receiver: json["receiver"],
        answerEntity: AnswerEntity.fromJson(json["answer"]),
      );

  Map<String, dynamic> toJson() => {
    "action": action,
    "type": type,
    "receiver": receiver,
    "answer": answerEntity,
  };
}

class AnswerEntity{
  String sdp;
  String type;

  AnswerEntity({
    this.sdp,
    this.type,
  });

  factory AnswerEntity.fromJson(Map<String, dynamic> json) =>
      AnswerEntity(
        sdp: json["sdp"],
        type: json["type"],
      );

  Map<String, dynamic> toJson() => {
    "sdp": sdp,
    "type": type,
  };
}