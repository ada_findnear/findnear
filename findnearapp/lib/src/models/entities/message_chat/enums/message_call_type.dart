import 'package:flutter/foundation.dart';

enum MessageCallType {
  unknown,
  audio,
  video,
}

extension MessageCallTypeParser on String {
  MessageCallType toMessageCallType() {
    return MessageCallType.values.firstWhere(
          (e) => describeEnum(e).toLowerCase() == this.toLowerCase(),
      orElse: () => MessageCallType.unknown,
    );
  }
}

enum MessageCallStateType {
  unknown,
  cancel,
  reject,
  accept,
}

extension MessageCallStateTypeParser on String {
  MessageCallStateType toMessageCallStateType() {
    return MessageCallStateType.values.firstWhere(
          (e) => describeEnum(e).toLowerCase() == this.toLowerCase(),
      orElse: () => MessageCallStateType.unknown,
    );
  }
}