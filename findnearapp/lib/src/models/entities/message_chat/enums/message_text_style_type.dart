import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

enum MessageTextStyleType {
  unknown,
  raanana,
  appleChancery,
  raleway,
  satisfy,
  svnAndrogyne,
  svnAppleberry,
  svnCinnamoncake,
  svnLoveOfThunder,
  svnAgencyFb,
  svnCapricaScript,
  svnFranko,
  svnHoleHearted,
  svnParisPro,
  svnManus,
}

extension MessageTextStyleTypeParser on String {
  MessageTextStyleType toMessageTextStyleType() {
    return MessageTextStyleType.values.firstWhere(
      (e) => describeEnum(e).toLowerCase() == this.toLowerCase(),
      orElse: () => MessageTextStyleType.unknown,
    );
  }
}

class MessageTextStyleEntity {
  final String id;
  final String font;
  final FontWeight weight;
  final Color color;

  MessageTextStyleEntity._(
    this.id,
    this.font,
    this.weight,
    this.color,
  );

  factory MessageTextStyleEntity.fromMessageTextStyleType(
    MessageTextStyleType messageTextStyleType,
  ) {
    return MessageTextStyleEntity.fromId(
      describeEnum(
        messageTextStyleType,
      ),
    );
  }

  factory MessageTextStyleEntity.fromId(
    String id,
  ) {
    final messageTextStyleType = id?.toMessageTextStyleType() ?? MessageTextStyleType.unknown;
    switch (messageTextStyleType) {
      case MessageTextStyleType.raanana:
        return new MessageTextStyleEntity._(
          id,
          "Raanana",
          FontWeight.w500,
          Color(0xFF0089FF),
        );
        break;
      case MessageTextStyleType.appleChancery:
        return new MessageTextStyleEntity._(
          id,
          "AppleChancery",
          FontWeight.w500,
          Color(0xFFFF0000),
        );
        break;
      case MessageTextStyleType.raleway:
        return new MessageTextStyleEntity._(
          id,
          "Raleway",
          FontWeight.w500,
          Color(0xFFFFA900),
        );
        break;
      case MessageTextStyleType.satisfy:
        return new MessageTextStyleEntity._(
          id,
          "Quicksand",
          FontWeight.w500,
          Color(0xFF00CBB3),
        );
        break;
      case MessageTextStyleType.svnAndrogyne:
        return new MessageTextStyleEntity._(
          id,
          "SvnAndrogyne",
          FontWeight.w500,
          Color(0xFF7D3D1D),
        );
        break;
      case MessageTextStyleType.svnAppleberry:
        return new MessageTextStyleEntity._(
          id,
          "SvnAppleberry",
          FontWeight.w500,
          Color(0xFFCE00FF),
        );
        break;
      case MessageTextStyleType.svnCinnamoncake:
        return new MessageTextStyleEntity._(
          id,
          "SvnCinnamoncake",
          FontWeight.w500,
          Color(0xFFFF6CCC),
        );
        break;
      case MessageTextStyleType.svnLoveOfThunder:
        return new MessageTextStyleEntity._(
          id,
          "SvnLoveOfThunder",
          FontWeight.w500,
          Color(0xFF0024BE),
        );
        break;
      case MessageTextStyleType.svnAgencyFb:
        return new MessageTextStyleEntity._(
          id,
          "SvnAgencyFb",
          FontWeight.w500,
          Color(0xFF97C400),
        );
        break;
      case MessageTextStyleType.svnCapricaScript:
        return new MessageTextStyleEntity._(
          id,
          "SvnCapricaScript",
          FontWeight.w500,
          Color(0xFF166010),
        );
        break;
      case MessageTextStyleType.svnFranko:
        return new MessageTextStyleEntity._(
          id,
          "SvnFranko",
          FontWeight.w500,
          Color(0xFFBC863D),
        );
        break;
      case MessageTextStyleType.svnHoleHearted:
        return new MessageTextStyleEntity._(
          id,
          "SvnHoleHearted",
          FontWeight.w500,
          Color(0xFF57EEFA),
        );
        break;
      case MessageTextStyleType.svnParisPro:
        return new MessageTextStyleEntity._(
          id,
          "SvnParisPro",
          FontWeight.w500,
          Color(0xFFFF0058),
        );
        break;
      case MessageTextStyleType.svnManus:
        return new MessageTextStyleEntity._(
          id,
          "SvnManus",
          FontWeight.w500,
          Color(0xFFFF7600),
        );
        break;
      default:
        return new MessageTextStyleEntity._(
          id,
          "Quicksand",
          FontWeight.w500,
          Color(0xFF333333),
        );
    }
  }
}
