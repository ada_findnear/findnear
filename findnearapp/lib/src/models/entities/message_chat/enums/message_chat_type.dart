import 'package:flutter/foundation.dart';

enum MessageChatType {
  unknown,
  text,
  reply,
  image,
  video,
  sticker,
  gif,
  voice,
  music,
  location,
  document,
  reminder,
  nameCard,
  sendMoney,
  call,
  changeBackground,
  changeAvatarGroup,
  changeNameGroup,
  inviteMember,
  file,
  link,
  initialization,
  leave,
  poll,
}

extension MessageChatTypeParser on String {
  MessageChatType toMessageChatType() {
    return MessageChatType.values.firstWhere(
      (e) => describeEnum(e).toLowerCase() == this.toLowerCase(),
      orElse: () => MessageChatType.unknown,
    );
  }
}
