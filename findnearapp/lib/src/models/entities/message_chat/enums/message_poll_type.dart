import 'package:flutter/foundation.dart';

enum MessagePollType {
  unknown,
  create,
  vote,
  changeVote,
  addOption,
  close,
  share,
}

extension MessagePollTypeParser on String {
  MessagePollType toMessagePollType() {
    return MessagePollType.values.firstWhere(
          (e) => describeEnum(e).toLowerCase() == this.toLowerCase(),
      orElse: () => MessagePollType.unknown,
    );
  }
}