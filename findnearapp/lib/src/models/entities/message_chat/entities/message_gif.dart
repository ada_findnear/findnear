import '../../../../utils/logger.dart';

class MessageGif {
  String url = "";
  int width = 0;
  int height = 0;

  MessageGif({
    this.url = "",
    this.width = 0,
    this.height = 0,
  });

  MessageGif.fromJson(Map<String, dynamic> jsonData) {
    try {
      url = jsonData["url"] ?? url;
      width = jsonData["width"] ?? width;
      height = jsonData["height"] ?? height;
    } catch (e) {
      logger.e("MessageGif - fromJson - error: $e");
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["url"] = this.url;
    data["width"] = this.width;
    data["height"] = this.height;
    return data;
  }
}