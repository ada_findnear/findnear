import 'dart:convert';

import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import '../../../../utils/logger.dart';

class MessageInitialization {
  List<ShortUserEntity> shortUsers;
  String groupName = "";
  String createBy;

  MessageInitialization({
    this.shortUsers,
    this.groupName,
    this.createBy,
  });

  MessageInitialization.fromJson(Map<String, dynamic> jsonData) {
    try {
      groupName = jsonData["group_name"] ?? groupName;
      createBy = jsonData["create_by"] ?? createBy;
      List<ShortUserEntity> list = [];
      try {
        list = List<ShortUserEntity>.from(jsonDecode(jsonData["short_users"])
            .map((x) => ShortUserEntity.fromJson(x)));
      } catch (e){
        list = [];
      }
      shortUsers = list;
    } catch (e) {
      logger.e("MessageLocation - fromJson - error: $e");
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['short_users'] = shortUsers != null
        ? jsonEncode(shortUsers.map((e) => e.toJson()).toList()).toString()
        : [];
    data["group_name"] = this.groupName;
    data["create_by"] = this.createBy;
    return data;
  }

  static MessageInitialization fromConversation(Conversation conversation){
    return MessageInitialization(
      shortUsers: conversation.config.shortUsers,
      groupName: conversation.name,
      createBy: conversation.createBy,
    );
  }
}