import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';

import '../../../../utils/date_utils.dart';
import '../../../../utils/logger.dart';
import '../enums/message_poll_type.dart';

class MessagePoll {
  MessagePollType type = MessagePollType.unknown;
  String id = "";
  String uuid = "";
  String conversationId = "";
  String topic = "";
  String sender = "";
  DateTime startTime = null;
  DateTime endTime = null;
  List<MessagePollOption> options = [];
  bool allowMultipleVoting = false;
  bool allowAdditionOption = false;

  MessagePoll({
    this.type = MessagePollType.unknown,
    this.id = "",
    this.uuid = "",
    this.conversationId = "",
    this.topic = "",
    this.sender = "",
    this.startTime = null,
    this.endTime = null,
    this.options = const [],
    this.allowMultipleVoting = false,
    this.allowAdditionOption = false,
  });

  MessagePoll copyWith({
    MessagePollType type,
    String id,
    String uuid,
    String conversationId,
    String topic,
    String sender,
    DateTime startTime,
    DateTime endTime,
    List<MessagePollOption> options,
    bool allowMultipleVoting,
    bool allowAdditionOption,
  }) {
    return MessagePoll(
      type: type ?? this.type,
      id: id ?? this.id,
      uuid: uuid ?? this.uuid,
      conversationId: conversationId ?? this.conversationId,
      topic: topic ?? this.topic,
      sender: sender ?? this.sender,
      startTime: startTime ?? this.startTime,
      endTime: endTime ?? this.endTime,
      options: options ?? this.options,
      allowMultipleVoting: allowMultipleVoting ?? this.allowMultipleVoting,
      allowAdditionOption: allowAdditionOption ?? this.allowAdditionOption,
    );
  }

  MessagePoll.fromJson(Map<String, dynamic> jsonData) {
    try {
      type = (jsonData["type"] ?? "").toString().toMessagePollType();
      id = jsonData["_id"] ?? id;
      uuid = jsonData["uuid"] ?? uuid;
      conversationId = jsonData["conversationId"] ?? conversationId;
      topic = jsonData["topic"] ?? topic;
      sender = jsonData["sender"] ?? sender;
      try {
        if (jsonData["startTime"] != null) {
          startTime =
              DateUtils.changeIso8601StringToDatetime(jsonData["startTime"]);
        } else {
          startTime = null;
        }
        if (jsonData["endTime"] != null) {
          endTime =
              DateUtils.changeIso8601StringToDatetime(jsonData["endTime"]);
        } else {
          endTime = null;
        }
      } catch (_) {
        startTime = null;
        endTime = null;
      }
      try {
        options = List<MessagePollOption>.from(
          List.from(jsonData["options"]).map(
            (x) => MessagePollOption.fromJson(x),
          ),
        );
      } catch (e) {
        logger.e("MessagePoll - jsonData[options] - error: $e");
        options = [];
      }
      allowMultipleVoting =
          jsonData["allowMultipleVoting"] ?? allowMultipleVoting;
      allowAdditionOption =
          jsonData["allowAdditionOption"] ?? allowAdditionOption;
    } catch (e) {
      logger.e("MessagePoll - fromJson - error: $e");
    }
  }

  MessagePoll.createRequest({
    this.topic = "",
    this.endTime = null,
    this.options = const [],
    this.allowMultipleVoting = false,
    this.allowAdditionOption = false,
  }) {
    startTime = DateTime.now();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["type"] = describeEnum(this.type);
    data["_id"] = this.id;
    data["uuid"] = this.uuid;
    data["conversationId"] = this.conversationId;
    data["topic"] = this.topic;
    data["sender"] = this.sender;

    if (startTime != null) {
      data["startTime"] =
          DateFormat(DateUtils.iso8601Format).format(startTime.toUtc());
    } else {
      data["startTime"] = null;
    }

    if (endTime != null) {
      data["endTime"] =
          DateFormat(DateUtils.iso8601Format).format(endTime.toUtc());
    } else {
      data["endTime"] = null;
    }

    data["options"] = this.options.map((e) => e.toJson()).toList();
    data["allowMultipleVoting"] = this.allowMultipleVoting;
    data["allowAdditionOption"] = this.allowAdditionOption;
    return data;
  }

  Map<String, dynamic> toCreateRequest() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["topic"] = this.topic;
    if (startTime != null) {
      data["startTime"] =
          DateFormat(DateUtils.iso8601Format).format(startTime.toUtc());
    } else {
      data["startTime"] =
          DateFormat(DateUtils.iso8601Format).format(DateTime.now().toUtc());
    }
    if (endTime != null) {
      data["endTime"] =
          DateFormat(DateUtils.iso8601Format).format(endTime.toUtc());
    } else {
      data["endTime"] = null;
    }
    data["options"] = this.options.map((e) => e.toCreateRequest()).toList();
    data["allowMultipleVoting"] = this.allowMultipleVoting;
    data["allowAdditionOption"] = this.allowAdditionOption;
    return data;
  }

  String getMessageContent(String userName) {
    switch (type) {
      case MessagePollType.create:
        return "$userName tạo cuộc bình chọn mới";
      case MessagePollType.vote:
        return "$userName lựa chọn trong cuộc bình chọn";
      case MessagePollType.changeVote:
        return "$userName đổi lựa chọn trong cuộc bình chọn";
      case MessagePollType.addOption:
        return "$userName thêm lựa chọn mới trong cuộc bình chọn";
      case MessagePollType.close:
        return "$userName khóa cuộc bình chọn";
      case MessagePollType.share:
        return "$userName chia sẻ cuộc bình chọn";
      default:
        return "";
    }
  }

  String getDeadlineFormated() {
    if (endTime == null) {
      return "";
    } else {
      final dateFormat = DateFormat("HH:mm, dd/MM/yyyy");
      return "Kết thúc lúc ${dateFormat.format(endTime)}";
    }
  }

  bool isLocked() {
    if (endTime == null) {
      return false;
    } else {
      return DateTime.now().compareTo(endTime) >= 0;
    }
  }
}

class MessagePollOption {
  String id = "";
  String name = "";
  List<String> pollters = [];

  MessagePollOption({
    this.id = "",
    this.name = "",
    this.pollters = const [],
  });

  MessagePollOption.fromJson(Map<String, dynamic> jsonData) {
    try {
      id = jsonData["_id"] ?? id;
      name = jsonData["name"] ?? name;
      pollters = (List<String>.from(jsonData["pollters"])) ?? pollters;
    } catch (e) {
      logger.e("MessagePollOption - fromJson - error: $e");
    }
  }

  MessagePollOption.createRequest({
    this.name = "",
  });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["_id"] = this.id;
    data["name"] = this.name;
    data["pollters"] = this.pollters;
    return data;
  }

  Map<String, dynamic> toCreateRequest() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["name"] = this.name;
    return data;
  }
}
