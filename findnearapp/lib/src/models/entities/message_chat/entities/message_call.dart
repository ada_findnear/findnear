import 'package:findnear/src/models/entities/message_chat/enums/message_call_type.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:flutter/foundation.dart';

class MessageCall {
  MessageCallType messageCallType = MessageCallType.unknown;
  MessageCallStateType messageCallStateType = MessageCallStateType.unknown;
  Duration duration = Duration(seconds: 0);

  MessageCall({
    this.messageCallType,
    this.messageCallStateType,
    this.duration
  });

  MessageCall.fromJson(Map<String, dynamic> jsonData) {
    try {
      messageCallType = jsonData["call_type"].toString().toMessageCallType() ?? MessageCallType.unknown;
      messageCallStateType = jsonData["call_state"].toString().toMessageCallStateType() ?? MessageCallStateType.unknown;
      duration = Duration(seconds: (jsonData["duration"] ?? 0));
    } catch (e) {
      logger.e("MessageCall - fromJson - error: $e");
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["call_type"] = describeEnum(this.messageCallType);
    data["call_state"] = describeEnum(this.messageCallStateType);
    data["duration"] = this.duration.inSeconds;
    return data;
  }
}