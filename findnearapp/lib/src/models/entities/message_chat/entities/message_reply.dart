import '../../../../utils/logger.dart';
import 'message_chat.dart';

class MessageReply {
  String owner = "";
  MessageChat message = null; // ATTENTION: Avoid to set = new MessageChat()

  MessageReply({
    this.owner = "",
    this.message,
  });

  MessageReply.fromJson(Map<String, dynamic> jsonData) {
    try {
      owner = jsonData["owner"] ?? owner;

      // ATTENTION: When use MessageChat in MessageChatConfigData will make loop forever
      // must to check empty to avoid create MessageChat
      if ((jsonData["message"] ?? "") != "") {
        message = MessageChat.fromJson(
          Map<String, dynamic>.from(jsonData["message"] ?? {}),
        );
      }
    } catch (e) {
      logger.e("MessageReply - fromJson - error: $e");
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["owner"] = this.owner;
    if (this.message != null) {
      try {
        data["message"] = this.message.toJson();
        data["message"]["config"] = {};
      } catch (e) {
        logger.e("MessageReply - toJson - error: $e");
        data["message"] = this.message.toJson();
      }
    }
    return data;
  }
}
