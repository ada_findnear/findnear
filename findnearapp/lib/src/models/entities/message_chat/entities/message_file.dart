
import 'dart:io';

import 'package:findnear/src/global/global_data.dart';
import 'package:findnear/src/models/media.dart';
import 'package:findnear/src/pages/messages/conversation_detail/widgets/file_type/file_type.dart';

import '../../../../utils/logger.dart';

class MessageFile {
  String downloadUrl = "";
  String fileName = "";
  String formatedSize = "0 KB";
  String sender = "";
  String mimeType = "";

  MessageFile({
    this.downloadUrl,
    this.fileName,
    this.formatedSize,
    this.sender,
    this.mimeType,
  });

  MessageFile.fromJson(Map<String, dynamic> jsonData) {
    try {
      downloadUrl = jsonData["downloadUrl"] ?? downloadUrl;
      fileName = jsonData["fileName"] ?? fileName;
      formatedSize = jsonData["formatedSize"] ?? formatedSize;
      sender = jsonData["sender"] ?? sender;
      mimeType = jsonData["mimeType"] ?? mimeType;
    } catch (e) {
      logger.e("MessageLocation - fromJson - error: $e");
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["downloadUrl"] = this.downloadUrl;
    data["fileName"] = this.fileName;
    data["formatedSize"] = this.formatedSize;
    data["sender"] = this.sender;
    data["mimeType"] = this.mimeType;
    return data;
  }

  static MessageFile fromMedia(Media media){
    return MessageFile(
      downloadUrl: media.url,
      fileName: media.fileName,
      formatedSize: media.formated_size,
      sender: "Unknown",
      mimeType: media.mime_type,
    );
  }
  bool get isDocFile => downloadUrl != null && docFileType.any((element) => downloadUrl.endsWith('.$element'));
  bool  isDownloadedOn(String savedDir) => File('$savedDir/${downloadUrl.split('/').last}').existsSync();
  bool get isDownloaded => isDownloadedOn(GlobalData.instance.documentPath);
}