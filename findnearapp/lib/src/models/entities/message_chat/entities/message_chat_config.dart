import 'dart:convert';

import '../../../../utils/logger.dart';
import '../enums/message_chat_type.dart';
import 'message_chat_config_data.dart';

class MessageChatConfig {
  MessageChatConfigData data = MessageChatConfigData();

  MessageChatConfig({
    MessageChatConfigData data,
  }) {
    this.data = data ?? MessageChatConfigData();
  }

  MessageChatConfig copyWith({
    MessageChatConfigData data,
  }) {
    return MessageChatConfig(
      data: data ?? this.data,
    );
  }

  MessageChatConfig.fromJson(Map<String, dynamic> jsonData) {
    try {
      final jsonString = jsonData["data"] ?? "";
      if (jsonString != "") {
        data = MessageChatConfigData.fromJson(jsonDecode(jsonString));
      }
    } catch (e) {
      logger.e("MessageChatConfig - fromJson - error: $e");
    }
  }

  Map<String, dynamic> toJson(MessageChatType messageChatType) {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    try {
      data["data"] = jsonEncode(this.data.toJson(messageChatType));
    } catch (e) {
      logger.e("MessageChat - toJson - error: $e");
    }
    return data;
  }
}
