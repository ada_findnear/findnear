import '../../../../utils/logger.dart';
import '../../../media.dart';
import '../enums/message_chat_type.dart';
import '../enums/message_text_style_type.dart';
import 'message_background.dart';
import 'message_call.dart';
import 'message_file.dart';
import 'message_gif.dart';
import 'message_image.dart';
import 'message_initialization.dart';
import 'message_invite_member.dart';
import 'message_leave_conversation.dart';
import 'message_location.dart';
import 'message_name_card.dart';
import 'message_poll.dart';
import 'message_reply.dart';
import 'message_sticker.dart';
import 'message_text_style.dart';
import 'message_voice.dart';

class MessageChatConfigData {
  MessageReply reply = MessageReply();
  MessageImage image = MessageImage();
  Media video = Media();
  MessageSticker sticker = MessageSticker();
  MessageGif gif = MessageGif();
  MessageVoice voice = MessageVoice();
  MessageLocation location = MessageLocation();
  MessageNameCard nameCard = MessageNameCard();
  MessageBackground background = MessageBackground();
  MessageCall messageCall = MessageCall();
  MessageFile messageFile = MessageFile();
  MessageInitialization messageInit = MessageInitialization();
  MessageInviteMember messageInvite = MessageInviteMember();
  MessageLeaveConversation messageLeave = MessageLeaveConversation();
  MessageTextStyle messageTextStyle = MessageTextStyle();
  MessagePoll poll = MessagePoll();

  MessageChatConfigData({
    MessageReply reply,
    MessageImage image,
    Media video,
    MessageSticker sticker,
    MessageGif gif,
    MessageVoice voice,
    MessageLocation location,
    MessageNameCard nameCard,
    MessageCall messageCall,
    MessageBackground background,
    MessageFile messageFile,
    MessageInitialization messageInit,
    MessageInviteMember messageInvite,
    MessageLeaveConversation messageLeave,
    MessageTextStyle messageTextStyle,
    MessagePoll poll,
  }) {
    this.reply = reply ?? MessageReply();
    this.image = image ?? MessageImage();
    this.video = video ?? Media();
    this.sticker = sticker ?? MessageSticker();
    this.gif = gif ?? MessageGif();
    this.voice = voice ?? MessageVoice();
    this.location = location ?? MessageLocation();
    this.nameCard = nameCard ?? MessageNameCard();
    this.messageCall = messageCall ?? MessageCall();
    this.background = background ?? MessageBackground();
    this.messageFile = messageFile ?? MessageFile();
    this.messageInit = messageInit ?? MessageInitialization();
    this.messageInvite = messageInvite ?? MessageInviteMember();
    this.messageLeave = messageLeave ?? MessageLeaveConversation();
    this.messageTextStyle = messageTextStyle ?? MessageTextStyle();
    this.poll = poll ?? MessagePoll();
  }

  MessageChatConfigData copyWith({
    MessageReply reply,
    MessageImage image,
    Media video,
    MessageSticker sticker,
    MessageGif gif,
    MessageVoice voice,
    MessageLocation location,
    MessageNameCard nameCard,
    MessageCall messageCall,
    MessageBackground background,
    MessageFile file,
    MessageInitialization messageInit,
    MessageInviteMember messageInvite,
    MessageLeaveConversation messageLeave,
    MessageTextStyle messageTextStyle,
    MessagePoll poll,
  }) {
    return MessageChatConfigData(
      reply: reply ?? this.reply,
      image: image ?? this.image,
      video: video ?? this.video,
      sticker: sticker ?? this.sticker,
      gif: gif ?? this.gif,
      voice: voice ?? this.voice,
      location: location ?? this.location,
      nameCard: nameCard ?? this.nameCard,
      messageCall: messageCall ?? this.messageCall,
      background: background ?? this.background,
      messageFile: file ?? this.messageFile,
      messageInit: messageInit ?? this.messageInit,
      messageInvite: messageInvite ?? this.messageInvite,
      messageLeave: messageLeave ?? this.messageLeave,
      messageTextStyle: messageTextStyle ?? this.messageTextStyle,
      poll: poll ?? this.poll,
    );
  }

  MessageChatConfigData.fromJson(Map<String, dynamic> jsonData) {
    try {
      reply = MessageReply.fromJson(
        Map<String, dynamic>.from(jsonData["reply"] ?? {}),
      );
      image = MessageImage.fromJson(
        Map<String, dynamic>.from(jsonData["image"] ?? {}),
      );
      video = Media.fromJSON(
        Map<String, dynamic>.from(jsonData["video"] ?? {}),
      );
      sticker = MessageSticker.fromJson(
        Map<String, dynamic>.from(jsonData["sticker"] ?? {}),
      );
      gif = MessageGif.fromJson(
        Map<String, dynamic>.from(jsonData["gif"] ?? {}),
      );
      voice = MessageVoice.fromJson(
        Map<String, dynamic>.from(jsonData["voice"] ?? {}),
      );
      location = MessageLocation.fromJson(
        Map<String, dynamic>.from(jsonData["location"] ?? {}),
      );
      nameCard = MessageNameCard.fromJson(
        Map<String, dynamic>.from(jsonData["name_card"] ?? {}),
      );
      messageCall = MessageCall.fromJson(
        Map<String, dynamic>.from(jsonData["message_call"] ?? {}),
      );
      background = MessageBackground.fromJson(
        Map<String, dynamic>.from(jsonData["background"] ?? {}),
      );
      messageFile = MessageFile.fromJson(
        Map<String, dynamic>.from(jsonData["message_file"] ?? {}),
      );
      messageInit = MessageInitialization.fromJson(
        Map<String, dynamic>.from(jsonData["message_init"] ?? {}),
      );
      messageInvite = MessageInviteMember.fromJson(
        Map<String, dynamic>.from(jsonData["message_invite"] ?? {}),
      );
      messageLeave = MessageLeaveConversation.fromJson(
        Map<String, dynamic>.from(jsonData["message_leave"] ?? {}),
      );
      messageTextStyle = MessageTextStyle.fromJson(
        Map<String, dynamic>.from(jsonData["message_text_style"] ?? {}),
      );
      poll = MessagePoll.fromJson(
        Map<String, dynamic>.from(jsonData["poll"] ?? {}),
      );
    } catch (e) {
      logger.e("MessageChatConfigData - fromJson - error: $e");
    }
  }

  Map<String, dynamic> toJson(MessageChatType messageChatType) {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    try {
      switch (messageChatType) {
        case MessageChatType.reply:
          data["reply"] = this.reply.toJson();
          break;
        case MessageChatType.image:
          data["image"] = this.image.toJson();
          break;
        case MessageChatType.video:
          data["video"] = this.video.toMap();
          break;
        case MessageChatType.sticker:
          data["sticker"] = this.sticker.toJson();
          break;
        case MessageChatType.gif:
          data["gif"] = this.gif.toJson();
          break;
        case MessageChatType.voice:
          data["voice"] = this.voice.toJson();
          break;
        case MessageChatType.location:
          data["location"] = this.location.toJson();
          break;
        case MessageChatType.nameCard:
          data["name_card"] = this.nameCard.toJson();
          break;
        case MessageChatType.changeBackground:
          data["background"] = this.background.toJson();
          break;
        case MessageChatType.file:
          data["message_file"] = this.messageFile.toJson();
          break;
        case MessageChatType.call:
          data["message_call"] = this.messageCall.toJson();
          break;
        case MessageChatType.initialization:
          data["message_init"] = this.messageInit.toJson();
          break;
        case MessageChatType.inviteMember:
          data["message_invite"] = this.messageInvite.toJson();
          break;
        case MessageChatType.leave:
          data["message_leave"] = this.messageLeave.toJson();
          break;
        case MessageChatType.poll:
          data["poll"] = this.poll.toJson();
          break;
        default:
          break;
      }
      if (this.messageTextStyle.style != MessageTextStyleType.unknown) {
        data["message_text_style"] = this.messageTextStyle.toJson();
      }
    } catch (e) {
      logger.e("MessageChatConfigData - toJson - error: $e");
    }
    return data;
  }
}
