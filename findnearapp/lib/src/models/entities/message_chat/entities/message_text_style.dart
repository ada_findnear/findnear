import 'package:flutter/foundation.dart';

import '../../../../utils/logger.dart';
import '../enums/message_text_style_type.dart';

class MessageTextStyle {
  MessageTextStyleType style = MessageTextStyleType.unknown;

  MessageTextStyle({
    this.style = MessageTextStyleType.unknown,
  });

  MessageTextStyle.fromJson(Map<String, dynamic> jsonData) {
    try {
      style = jsonData["style"]?.toString()?.toMessageTextStyleType() ?? style;
    } catch (e) {
      logger.e("MessageTextStyle - fromJson - error: $e");
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["style"] = describeEnum(this.style);
    return data;
  }
}
