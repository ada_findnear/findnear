import '../../../../utils/logger.dart';

class MessageSticker {
  String stickerId = "";

  MessageSticker({
    this.stickerId,
  });

  MessageSticker.fromJson(Map<String, dynamic> jsonData) {
    try {
      stickerId = jsonData["stickerId"] ?? stickerId;
    } catch (e) {
      logger.e("MessageSticker - fromJson - error: $e");
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["stickerId"] = this.stickerId;
    return data;
  }
}
