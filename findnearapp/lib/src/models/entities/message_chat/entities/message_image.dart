import '../../../../utils/logger.dart';
import '../../../media.dart';

class MessageImage {
  Media media = Media();
  int width = 0;
  int height = 0;

  MessageImage({
    Media media,
    this.width = 0,
    this.height = 0,
  }) {
    this.media = media ?? Media();
  }

  MessageImage.fromJson(Map<String, dynamic> jsonData) {
    try {
      media = Media.fromJSON(
        Map<String, dynamic>.from(jsonData["media"] ?? {}),
      );
      width = jsonData["width"] ?? width;
      height = jsonData["height"] ?? height;
    } catch (e) {
      logger.e("MessageImage - fromJson - error: $e");
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["media"] = this.media.toMap();
    data["width"] = this.width;
    data["height"] = this.height;
    return data;
  }
}
