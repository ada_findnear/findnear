import '../../../../utils/logger.dart';

class MessageBackground {
  String groupBackground = "";
  String privateBackground = "";

  MessageBackground({
    this.groupBackground,
    this.privateBackground,
  });

  MessageBackground.fromJson(Map<String, dynamic> jsonData) {
    try {
      groupBackground = jsonData["group_background"] ?? groupBackground;
      privateBackground = jsonData["private_background"] ?? privateBackground;
    } catch (e) {
      logger.e("MessageLocation - fromJson - error: $e");
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["group_background"] = this.groupBackground;
    data["private_background"] = this.privateBackground;
    return data;
  }
}