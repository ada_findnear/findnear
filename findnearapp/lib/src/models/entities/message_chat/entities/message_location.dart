import '../../../../utils/logger.dart';

class MessageLocation {
  String url = "";
  double lat = 0;
  double lng = 0;

  MessageLocation({
    this.url = "",
    this.lat = 0,
    this.lng = 0,
  });

  MessageLocation.fromJson(Map<String, dynamic> jsonData) {
    try {
      url = jsonData["url"] ?? url;
      lat = jsonData["lat"] ?? lat;
      lng = jsonData["lng"] ?? lng;
    } catch (e) {
      logger.e("MessageLocation - fromJson - error: $e");
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["url"] = this.url;
    data["lat"] = this.lat;
    data["lng"] = this.lng;
    return data;
  }
}