import 'package:flutter/foundation.dart';
import "package:intl/intl.dart";

import '../../../../utils/date_utils.dart';
import '../../../../utils/logger.dart';
import '../../../response/user_v2_response.dart';
import '../../../user.dart';
import '../enums/message_chat_type.dart';
import 'message_chat_config.dart';

class MessageChat {
  String id = "";
  String uuid = "";
  MessageChatType type = MessageChatType.unknown;
  String content = "";
  String sender = "";
  DateTime createDate = DateTime.fromMillisecondsSinceEpoch(0);
  String conversationId = "";
  int v = 0;
  MessageChatConfig config = MessageChatConfig();

  UserV2Response user = UserV2Response(); // local variable

  MessageChat({
    this.id = "",
    this.uuid = "",
    this.type = MessageChatType.unknown,
    this.content = "",
    this.sender = "",
    this.conversationId = "",
    this.v = 0,
    DateTime createDate,
    MessageChatConfig config,
    UserV2Response user,
  }) {
    this.createDate = createDate ?? DateTime.fromMillisecondsSinceEpoch(0);
    this.config = config ?? MessageChatConfig();
    this.user = user ?? UserV2Response();
  }

  MessageChat copyWith({
    String id,
    String uuid,
    MessageChatType type,
    String content,
    String sender,
    DateTime createDate,
    String conversationId,
    int v,
    MessageChatConfig config,
    User user,
  }) {
    return MessageChat(
      id: id ?? this.id,
      uuid: uuid ?? this.uuid,
      type: type ?? this.type,
      content: content ?? this.content,
      sender: sender ?? this.sender,
      createDate: createDate ?? this.createDate,
      conversationId: conversationId ?? this.conversationId,
      v: v ?? this.v,
      config: config ?? this.config,
      user: user ?? this.user,
    );
  }

  MessageChat.fromJson(Map<String, dynamic> jsonData,
      {bool forLocalDB = false}) {
    try {
      id = jsonData["_id"] ?? id;
      uuid = jsonData["uuid"] ?? uuid;
      type = (jsonData["type"] ?? "").toString().toMessageChatType();
      content = jsonData["content"] ?? content;
      sender = jsonData["sender"] ?? sender;
      try {
        createDate =
            DateUtils.changeIso8601StringToDatetime(jsonData["createDate"]);
      } catch (_) {
        createDate = DateTime.fromMillisecondsSinceEpoch(0);
      }
      conversationId = jsonData["conversationId"] ?? conversationId;
      v = jsonData["__v"] ?? v;
      config = MessageChatConfig.fromJson(
          Map<String, dynamic>.from(jsonData["config"] ?? {}));

      if (forLocalDB) {
        user = UserV2Response.fromJson(
            Map<String, dynamic>.from(jsonData["user"] ?? {}));
      }
    } catch (e) {
      logger.e("MessageChat - fromJson - error: $e");
    }
  }

  Map<String, dynamic> toJson({bool forLocalDB = false}) {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    try {
      data["_id"] = this.id;
      data["uuid"] = this.uuid;
      data["type"] = describeEnum(this.type);
      data["content"] = this.content;
      data["sender"] = this.sender;
      if (createDate.millisecondsSinceEpoch != 0) {
        data["createDate"] =
            DateFormat(DateUtils.iso8601Format).format(createDate.toUtc());
      }
      data["conversationId"] = this.conversationId;
      data["__v"] = this.v;
      data["config"] = this.config.toJson(this.type);

      if (forLocalDB) {
        data["user"] = this.user?.toJson();
      }
    } catch (e) {
      logger.e("MessageChat - toJson - error: $e");
    }
    return data;
  }
}
