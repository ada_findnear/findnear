import 'dart:convert';

import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import '../../../../utils/logger.dart';

class MessageLeaveConversation {
  ShortUserEntity shortUser;
  String createBy;
  Conversation conversation;

  MessageLeaveConversation({
    this.shortUser,
    this.conversation,
    this.createBy,
  });

  MessageLeaveConversation.fromJson(Map<String, dynamic> jsonData) {
    try {
      conversation = jsonData["conversation"] != null ? Conversation.fromJSON(jsonDecode(jsonData["conversation"])) : null;
      createBy = jsonData["create_by"] ?? "";
      shortUser = jsonData["short_user"]!= null ? ShortUserEntity.fromJson(jsonDecode(jsonData["short_user"])) : null;
    } catch (e) {
      logger.e("MessageLocation - fromJson - error: $e");
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['short_user'] = jsonEncode(shortUser.toJson());
    data["create_by"] = this.createBy;
    data["conversation"] = jsonEncode(this.conversation.toJson());
    return data;
  }

  static MessageLeaveConversation fromConversation(Conversation conversation, ShortUserEntity newMember){
    return MessageLeaveConversation(
      conversation: conversation,
      shortUser: newMember,
      createBy: conversation.createBy,
    );
  }
}