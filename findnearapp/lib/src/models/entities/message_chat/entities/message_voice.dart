import '../../../../utils/logger.dart';

class MessageVoice {
  String url = "";
  Duration duration = Duration(seconds: 0);

  MessageVoice({
    this.url = "",
    this.duration = const Duration(seconds: 0),
  });

  MessageVoice.fromJson(Map<String, dynamic> jsonData) {
    try {
      url = jsonData["url"] ?? url;
      duration = Duration(seconds: jsonData["duration"] ?? 0);
    } catch (e) {
      logger.e("MessageVoice - fromJson - error: $e");
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["url"] = this.url;
    data["duration"] = this.duration.inSeconds;
    return data;
  }
}
