import '../../../../utils/logger.dart';

class MessageNameCard {
  String userId = "";
  String userCode = "";
  String avatarUrl = "";
  String name = "";

  MessageNameCard({
    this.userId = "",
    this.userCode = "",
    this.avatarUrl = "",
    this.name = "",
  });

  MessageNameCard.fromJson(Map<String, dynamic> jsonData) {
    try {
      userId = jsonData["user_id"] ?? userId;
      userCode = jsonData["user_code"] ?? userCode;
      avatarUrl = jsonData["avatar_url"] ?? avatarUrl;
      name = jsonData["name"] ?? name;
    } catch (e) {
      logger.e("MessageNameCard - fromJson - error: $e");
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["user_id"] = this.userId;
    data["user_code"] = this.userCode;
    data["avatar_url"] = this.avatarUrl;
    data["name"] = this.name;
    return data;
  }
}
