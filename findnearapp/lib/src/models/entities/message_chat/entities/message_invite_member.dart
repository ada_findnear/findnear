import 'dart:convert';

import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import '../../../../utils/logger.dart';

class MessageInviteMember {
  List<ShortUserEntity> shortUsers;
  Conversation conversation;
  String createBy;

  MessageInviteMember({
    this.shortUsers,
    this.conversation,
    this.createBy,
  });

  MessageInviteMember.fromJson(Map<String, dynamic> jsonData) {
    try {
      conversation = jsonData["conversation"] != null ? Conversation.fromJSON(jsonDecode(jsonData["conversation"])) : null;
      createBy = jsonData["create_by"] ?? "";
      List<ShortUserEntity> list = [];
      if(jsonData["short_users"]!= null)
        try {
          list = List<ShortUserEntity>.from(jsonDecode(jsonData["short_users"])
              .map((x) => ShortUserEntity.fromJson(x)));
        } catch (e){
          list = [];
        }
      shortUsers = list;
    } catch (e) {
      logger.e("MessageLocation - fromJson - error: $e");
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['short_users'] = shortUsers != null
        ? jsonEncode(shortUsers.map((e) => e.toJson()).toList()).toString()
        : [];
    data["conversation"] = jsonEncode(this.conversation.toJson());
    data["create_by"] = this.createBy;
    return data;
  }

  static MessageInviteMember fromConversation(Conversation conversation, List<ShortUserEntity> newMember, String sender){
    return MessageInviteMember(
      shortUsers: newMember,
      conversation: conversation,
      createBy: sender,
    );
  }
}