import 'package:findnear/src/models/response/comment_response.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/livestream/presenter/comment_presenter.dart';
import 'package:global_configuration/global_configuration.dart';

class MetaEntity {
  String replyId;
  String replySender;
  String replyContent;
  String senderAvatar;
  String senderName;

  MetaEntity({
    this.replyId,
    this.replySender,
    this.replyContent,
    this.senderAvatar,
    this.senderName,
  });

  factory MetaEntity.fromJson(Map<String, dynamic> json) {
    return MetaEntity(
      replyId: json['replyId'] ?? "",
      replySender: json['replySender'] ?? "",
      replyContent: json['replyContent'] ?? "",
      senderAvatar: json['senderAvatar'] ?? "${GlobalConfiguration().getValue('base_url')}images/image_default.png",
      senderName: json['senderName'] ?? "",
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['replyId'] = this.replyId;
    data['replySender'] = this.replySender;
    data['replyContent'] = this.replyContent;
    data['senderAvatar'] = this.senderAvatar;
    data['senderName'] = this.senderName;
    return data;
  }

  static MetaEntity fromCommentPresenter({CommentPresenter reply, User sender}){
    return MetaEntity(
      replyId: reply?.id ?? "",
      replySender: reply?.name ?? "",
      replyContent: reply?.text ?? "",
      senderAvatar: sender?.image?.thumb ?? "${GlobalConfiguration().getValue('base_url')}images/image_default.png",
      senderName: sender.name ?? "",
    );
  }
}
