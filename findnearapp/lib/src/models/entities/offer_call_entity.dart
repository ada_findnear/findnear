import 'dart:convert';

import 'package:findnear/src/models/response/call_notification_response.dart';

class OfferCallEntity{
  String action;
  String type;
  String userMakeCall;
  OfferEntity offerEntity;
  String typeCall;
  CallNotiResponse extendData;

  OfferCallEntity({
    this.action,
    this.type,
    this.userMakeCall,
    this.offerEntity,
    this.typeCall,
    this.extendData,
  });

  OfferCallEntity copyWith({
    String action,
    String type,
    String userMakeCall,
    OfferEntity offerEntity,
    String typeCall,
    CallNotiResponse extendData,
}) {
    return OfferCallEntity(
        action: action ?? this.action,
        type: type ?? this.type,
        userMakeCall: userMakeCall ?? this.userMakeCall,
        offerEntity: offerEntity ?? this.offerEntity,
        typeCall: typeCall ?? this.typeCall,
        extendData: extendData ?? this.extendData,
    );
  }

  factory OfferCallEntity.fromJson(Map<String, dynamic> json) {
    OfferEntity _offerEntity;
    if(json["offer"] is String){
      _offerEntity = OfferEntity.fromJson(jsonDecode(json["offer"]));
    }else if(json["offer"] is Map){
      _offerEntity = OfferEntity.fromJson(Map<String, dynamic>.from(json["offer"]));
    }

    CallNotiResponse _callNotiResponse;
    if(json["offer"] is String){
      _callNotiResponse = CallNotiResponse.fromJson(jsonDecode(json["extendData"]));
    }else if(json["offer"] is Map){
      _callNotiResponse = CallNotiResponse.fromJson(Map<String, dynamic>.from(json["extendData"]));
    }
   return OfferCallEntity(
      action: json["action"],
      type: json["type"],
      userMakeCall: json["userMakeCall"],
      offerEntity: _offerEntity,
      typeCall: json["typeCall"],
      extendData: _callNotiResponse,
    );
  }


  Map<String, dynamic> toJson() => {
    "action": action,
    "type": type,
    "userMakeCall": userMakeCall,
    "offer": offerEntity.toJson(),
    "typeCall": typeCall,
  };
}

class OfferEntity{
  String sdp;
  String type;

  OfferEntity({
    this.sdp,
    this.type,
  });

  factory OfferEntity.fromJson(Map<String, dynamic> json) =>
      OfferEntity(
        sdp: json["sdp"],
        type: json["type"],
      );

  Map<String, dynamic> toJson() => {
    "sdp": sdp,
    "type": type,
  };
}