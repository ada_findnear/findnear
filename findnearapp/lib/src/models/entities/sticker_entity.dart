class StickerEntity {
  String id; //Ex: "123" => 1: type, 23: index

  StickerEntity({this.id});

  String get path {
    return "assets/stickers/ic_sticker_${id}.png";
  }
}

class StickerList {
  List<StickerEntity> stickers;

  StickerList() {
    stickers = [
      StickerEntity(id: "101"),
      StickerEntity(id: "102"),
      StickerEntity(id: "103"),
      StickerEntity(id: "104"),
      StickerEntity(id: "105"),
      StickerEntity(id: "106"),
      StickerEntity(id: "107"),
      StickerEntity(id: "108"),
      StickerEntity(id: "109"),
      StickerEntity(id: "110"),
      StickerEntity(id: "111"),
      StickerEntity(id: "112"),
      StickerEntity(id: "113"),
      StickerEntity(id: "114"),
      StickerEntity(id: "115"),
    ];
  }
}
