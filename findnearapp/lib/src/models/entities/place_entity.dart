// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

import 'package:findnear/src/pages/search/search_logic.dart';

class PlaceList {
  PlaceList({
    this.htmlAttributions,
    this.results,
    this.status,
  });

  List<dynamic> htmlAttributions;
  List<PlaceEntity> results;
  String status;

  factory PlaceList.fromJson(Map<String, dynamic> json) => PlaceList(
        htmlAttributions: List<dynamic>.from(json["html_attributions"].map((x) => x)),
        results: List<PlaceEntity>.from(json["results"].map((x) => PlaceEntity.fromJson(x))),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "html_attributions": List<dynamic>.from(htmlAttributions.map((x) => x)),
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
        "status": status,
      };
}

class PlaceAutoCompleteEntity {
  PlaceAutoCompleteEntity({
    this.htmlAttributions,
    this.result,
    this.status,
  });

  List<dynamic> htmlAttributions;
  PlaceEntity result;
  String status;

  factory PlaceAutoCompleteEntity.fromJson(Map<String, dynamic> json) => PlaceAutoCompleteEntity(
    htmlAttributions: List<dynamic>.from(json["html_attributions"].map((x) => x)),
    result: PlaceEntity.fromJson(json["result"]),
    status: json["status"],
  );

  Map<String, dynamic> toJson() => {
    "html_attributions": List<dynamic>.from(htmlAttributions.map((x) => x)),
    "result": result.toJson(),
    "status": status,
  };
}

class PlaceEntity {
  PlaceEntity({
    this.formattedAddress,
    this.geometry,
    this.icon,
    this.iconBackgroundColor,
    this.iconMaskBaseUri,
    this.name,
    this.placeId,
    this.reference,
  });

  String formattedAddress;
  GeometryEntity geometry;
  String icon;
  String iconBackgroundColor;
  String iconMaskBaseUri;
  String name;
  String placeId;
  String reference;

  factory PlaceEntity.fromJson(Map<String, dynamic> json) => PlaceEntity(
        formattedAddress: json["formatted_address"],
        geometry: GeometryEntity.fromJson(json["geometry"]),
        icon: json["icon"],
        iconBackgroundColor: json["icon_background_color"],
        iconMaskBaseUri: json["icon_mask_base_uri"],
        name: json["name"],
        placeId: json["place_id"],
        reference: json["reference"],
      );

  Map<String, dynamic> toJson() => {
        "formatted_address": formattedAddress,
        "geometry": geometry.toJson(),
        "icon": icon,
        "icon_background_color": iconBackgroundColor,
        "icon_mask_base_uri": iconMaskBaseUri,
        "name": name,
        "place_id": placeId,
        "reference": reference,
      };
}

class AddressList {
  AddressList({
    this.results,
    this.status,
  });

  List<AddressEntity> results;
  String status;

  factory AddressList.fromJson(Map<String, dynamic> json) => AddressList(
    results: List<AddressEntity>.from(json["results"].map((x) => AddressEntity.fromJson(x))),
    status: json["status"],
  );

  Map<String, dynamic> toJson() => {
    "results": List<dynamic>.from(results.map((x) => x.toJson())),
    "status": status,
  };
}

class AddressEntity {
  AddressEntity({
    this.formattedAddress,
    this.placeId,
  });

  String formattedAddress;
  String placeId;

  factory AddressEntity.fromJson(Map<String, dynamic> json) => AddressEntity(
    formattedAddress: json["formatted_address"],
    placeId: json["place_id"],
  );

  Map<String, dynamic> toJson() => {
    "formatted_address": formattedAddress,
    "place_id": placeId,
  };
}

class AddressAutoList {
  AddressAutoList({
    this.predictions,
    this.status,
  });

  List<AddressAutoEntity> predictions;
  String status;

  factory AddressAutoList.fromJson(Map<String, dynamic> json) => AddressAutoList(
    predictions: List<AddressAutoEntity>.from(json["predictions"].map((x) => AddressAutoEntity.fromJson(x))),
    status: json["status"],
  );

  Map<String, dynamic> toJson() => {
    "predictions": List<dynamic>.from(predictions.map((x) => x.toJson())),
    "status": status,
  };
}

class AddressAutoEntity {
  AddressAutoEntity({
    this.description,
    this.placeId,
  });

  String description;
  String placeId;

  factory AddressAutoEntity.fromJson(Map<String, dynamic> json) => AddressAutoEntity(
    description: json["description"],
    placeId: json["place_id"],
  );

  Map<String, dynamic> toJson() => {
    "description": description,
    "place_id": placeId,
  };
}

class GeometryEntity {
  GeometryEntity({
    this.location,
    this.viewport,
  });

  LocationEntity location;
  ViewportEntity viewport;

  factory GeometryEntity.fromJson(Map<String, dynamic> json) => GeometryEntity(
        location: LocationEntity.fromJson(json["location"]),
        viewport: ViewportEntity.fromJson(json["viewport"]),
      );

  Map<String, dynamic> toJson() => {
        "location": location.toJson(),
        "viewport": viewport.toJson(),
      };
}

class LocationEntity {
  LocationEntity({
    this.lat,
    this.lng,
  });

  double lat;
  double lng;

  factory LocationEntity.fromJson(Map<String, dynamic> json) => LocationEntity(
        lat: json["lat"].toDouble(),
        lng: json["lng"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "lat": lat,
        "lng": lng,
      };
}

class ViewportEntity {
  ViewportEntity({
    this.northeast,
    this.southwest,
  });

  LocationEntity northeast;
  LocationEntity southwest;

  factory ViewportEntity.fromJson(Map<String, dynamic> json) => ViewportEntity(
        northeast: LocationEntity.fromJson(json["northeast"]),
        southwest: LocationEntity.fromJson(json["southwest"]),
      );

  Map<String, dynamic> toJson() => {
        "northeast": northeast.toJson(),
        "southwest": southwest.toJson(),
      };
}

class SearchResultEntity{
  SearchResultEntity({
    this.type,
    this.data,
  });

  ObjectType type;
  dynamic data;
}