class CandidateCallEntity{
  String action;
  String type;
  String receiver;
  CandidateEntity candidateEntity;

  CandidateCallEntity({
    this.action,
    this.type,
    this.receiver,
    this.candidateEntity,
  });

  factory CandidateCallEntity.fromJson(Map<String, dynamic> json) =>
      CandidateCallEntity(
        action: json["action"],
        type: json["type"],
        receiver: json["receiver"],
        candidateEntity: CandidateEntity.fromJson(json["candidate"]),
      );

  Map<String, dynamic> toJson() => {
    "action": action,
    "type": type,
    "receiver": receiver,
    "candidate": candidateEntity,
  };
}

class CandidateEntity{
  String candidate;
  int sdpMLineIndex;
  String sdpMid;

  CandidateEntity({
    this.candidate,
    this.sdpMLineIndex,
    this.sdpMid,
  });

  factory CandidateEntity.fromJson(Map<String, dynamic> json) =>
      CandidateEntity(
        candidate: json["candidate"],
        sdpMLineIndex: json["sdpMLineIndex"],
        sdpMid: json["sdpMid"],
      );

  Map<String, dynamic> toJson() => {
    "candidate": candidate,
    "sdpMLineIndex": sdpMLineIndex,
    "sdpMid": sdpMid,
  };
}