import '../user.dart';

class LiveStreamInfoEntity {
  int id;
  int user_id;
  String description;
  String status;
  String created_at;
  int number_viewer;
  User user;

  LiveStreamInfoEntity(
      {this.id,
      this.user_id,
      this.description,
      this.status,
      this.created_at,
      this.number_viewer,
      this.user});

  factory LiveStreamInfoEntity.fromJson(Map<String, dynamic> json) =>
      LiveStreamInfoEntity(
        id: json["id"],
        user_id: json["user_id"],
        description: json["description"],
        status: json["status"],
        created_at: json["created_at"],
        number_viewer: json["number_viewer"],
        user: User.fromJSON(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "id": user_id,
        "user_id": user_id,
        "description": description,
        "status": status,
        "created_at": created_at,
        "number_viewer": number_viewer,
        "user": user,
      };
}
