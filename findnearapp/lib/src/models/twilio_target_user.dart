import 'package:global_configuration/global_configuration.dart';

class TwilioTargetUser {
  String name;
  String avatar;

  TwilioTargetUser({this.name, this.avatar}) {
    avatar ??= "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
  }

  TwilioTargetUser.fromJson(Map<String, dynamic> json) {
    name = json['sender_name'];
    avatar = json['sender_avatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['sender_name'] = this.name;
    data['sender_avatar'] = this.avatar;
    return data;
  }
}
