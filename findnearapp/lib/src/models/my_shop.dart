import 'package:findnear/src/models/shop.dart';

import 'media.dart';

class MyShop {
  int id;
  int user_id;
  String vote;
  int city;
  int district;
  int ward;
  int field_id;
  String title;
  String address;
  List<Media> media;
  String street;
  String description;
  String status;
  bool recommended;
  String latitude;
  String longitude;

  MyShop({
    this.id,
    this.user_id,
    this.vote,
    this.city,
    this.district,
    this.ward,
    this.field_id,
    this.title,
    this.address,
    this.street,
    this.description,
    this.status,
    this.recommended,
    this.media,
    this.latitude,
    this.longitude,
  });

  factory MyShop.fromJson(Map<String, dynamic> json) {
    return MyShop(
      id: json['id'],
      user_id: json['user_id'],
      vote: json['vote'].toString(),
      city: json['city'],
      district: json['district'],
      ward: json['ward'],
      field_id: json['field_id'],
      title: json['title'],
      media: json['media'] != null
          ? (json['media'] as List).map((i) => Media.fromJSON(i)).toList()
          : null,
      address: json['address'],
      street: json['street'],
      description: json['description'],
      status: json['status'],
      recommended: json['recommended'],
      latitude: json['latitude'],
      longitude: json['longitude'],

    );
  }

  Map<String, dynamic> toJson() {
    // ignore: unnecessary_cast
    return {
      'id': this.id,
      'user_id': this.user_id,
      'vote': this.vote,
      'city': this.city,
      'district': this.district,
      'ward': this.ward,
      'field_id': this.field_id,
      'title': this.title,
      'address': this.address,
      'street': this.street,
      'description': this.description,
      'status': this.status,
      'recommended': this.recommended,
      'latitude':this.latitude,
      'longitude':this.longitude,
    if (this.media != null) 'media' : this.media.map((v) => v.toMap()).toList(),
    } as Map<String, dynamic>;
  }

  Shop toShop() {
    
    Shop shop = Shop();
    shop.id = this.id;
    shop.title = this.title;
    shop.street = this.street;
    shop.media = this.media;
    shop.field_id = this.field_id;
    shop.district = this.district;
    shop.ward = this.ward;
    shop.city = this.city;
    shop.description = this.description;
    shop.media = this.media;
    shop.vote = double.parse(this.vote);
    return shop;
  }
}
