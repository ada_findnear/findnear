
import 'package:findnear/src/models/user.dart';

class LotteGameResult {
  int id;
  int user_id;
  int game_id;
  int phase;
  int point;
  String created_at;
  String updated_at;
  bool won;
  int win_point;
  int won_number;
  User user;

  LotteGameResult(
      {this.id,
      this.user_id,
      this.game_id,
      this.phase,
      this.point,
      this.created_at,
      this.updated_at,
      this.won,
      this.win_point,
      this.won_number,
      this.user});

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'user_id': this.user_id,
      'game_id': this.game_id,
      'phase': this.phase,
      'point': this.point,
      'created_at': this.created_at,
      'updated_at': this.updated_at,
      'won': this.won,
      'win_point': this.win_point,
      'won_number': this.won_number,
      'user': this.user.toMap(),
    };
  }

  factory LotteGameResult.fromMap(Map<String, dynamic> map) {
    return LotteGameResult(
      id: map['id'] as int,
      user_id: map['user_id'] as int,
      game_id: map['game_id'] as int,
      phase: map['phase'] as int,
      point: map['point'] as int,
      created_at: map['created_at'] as String,
      updated_at: map['updated_at'] as String,
      won: map['won'] as bool,
      win_point: map['win_point'] as int,
      won_number: map['won_number'] as int,
      user: User.fromJSON(map['user']),
    );
  }
}
