import 'package:findnear/src/models/entities/meta_entity.dart';

class CommentParam {
  String commentType;
  String content;
  MetaEntity meta;

  CommentParam({
    this.commentType,
    this.content,
    this.meta,
  });

  factory CommentParam.fromJson(Map<String, dynamic> json) {
    return CommentParam(
      commentType: json['commentType'] ?? "MSG",
      content: json['content'] ?? "",
      meta: json['meta'] != null ? MetaEntity.fromJson(json['meta']) : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['commentType'] = this.commentType;
    data['content'] = this.content;
    if(meta!= null) data['meta'] = this.meta.toJson();
    return data;
  }
}
