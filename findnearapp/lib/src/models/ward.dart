import '../helpers/custom_trace.dart';
import '../models/media.dart';
import '../models/market.dart';
import 'product.dart';

class Ward {
  int idx;
  int province_id;
  int district_id;
  String vnname;
  String vnprefix;
  int index;

  int indexUI;
  Ward();

  Ward.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      idx = jsonMap['idx'];
      province_id = jsonMap['province_id'];
      district_id = jsonMap['district_id'];
      vnname = jsonMap['vnname'] != null ? jsonMap['vnname'].toString() : '';
      vnprefix = jsonMap['vnprefix'] != null ? jsonMap['vnprefix'].toString() : '';
    } catch (e) {
      print(CustomTrace(StackTrace.current, message: e));
    }
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["idx"] = idx;
    map["province_id"] = province_id;
    map["vnname"] = vnname;
    map["vnprefix"] = vnprefix;
    map["province_id"] = province_id;
    return map;
  }

  @override
  bool operator ==(dynamic other) {
    return other.idx == this.idx;
  }

  @override
  int get hashCode => this.idx.hashCode;

  void createIndex(int indexUI) {
    this.indexUI = indexUI;
  }
}
