import 'package:findnear/src/models/media.dart';

import 'rating.dart';

class OrderHistory {
  int id;
  String code;
  int userId;
  int shopId;
  int customerId;
  String status;
  int total;
  int numberItem;
  String createdAt;
  String deletedAt;
  String updatedAt;
  List<Rating> rating;
  List<ProductOrder> products;

  OrderHistory();

  OrderHistory.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'];
      code = jsonMap['code'];
      userId = jsonMap['user_id'];
      shopId = jsonMap['shop_id'];
      customerId = jsonMap['customer_id'];
      status = jsonMap['status'];
      total = jsonMap['total'];
      numberItem = jsonMap['number_item'];
      createdAt = jsonMap['createdAt'];
      deletedAt = jsonMap['deleted_at'];
      updatedAt = jsonMap['updated_at'];
      rating = jsonMap['rating'] != null
          ? List.from(jsonMap['products'])
              .map((element) => Rating.fromJSON(element))
              .toSet()
              .toList()
          : [];
      ;
      products = jsonMap['products'] != null
          ? List.from(jsonMap['products'])
              .map((element) => ProductOrder.fromJSON(element))
              .toSet()
              .toList()
          : [];
    } catch (e) {}
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['code'] = this.code;
    data['user_id'] = this.userId;
    data['shop_id'] = this.shopId;
    data['customer_id'] = this.customerId;
    data['total'] = this.total;
    data['status'] = this.status;
    data['number_item'] = this.numberItem;
    data['createdAt'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    data['rating'] = this.rating;
    data['products'] = this.products;
    return data;
  }
}

class ProductOrder {
  String id;
  String name;
  double price;
  double discountPrice;
  Media image;
  List<Media> images;
  ProductOrder.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      name = jsonMap['name'];
      price = jsonMap['price'] != null
          ? double.parse(jsonMap['price'] ?? 0.0)
          : 0.0;
      discountPrice = jsonMap['discount_price'] != null
          ? jsonMap['discount_price'].toDouble()
          : 0.0;
      price = discountPrice != 0 ? discountPrice : price;
      discountPrice = discountPrice == 0
          ? discountPrice
          : jsonMap['price'] != null
              ? double.parse(jsonMap['price'] ?? 0.0)
              : 0.0;
      image = jsonMap['media'] != null && (jsonMap['media'] as List).length > 0
          ? Media.fromJSON(jsonMap['media'][0])
          : new Media();
      images = jsonMap['media'] != null && (jsonMap['media'] as List).length > 0
          ? List.from(jsonMap['media'])
              .map((element) => Media.fromJSON(element))
              .toSet()
              .toList()
          : [];
    } catch (e) {
      // id = '';
      // name = '';
      // price = 0.0;
      // discountPrice = 0.0;
      // image = new Media();
      // images = [];
      print(e);
    }
  }
}
