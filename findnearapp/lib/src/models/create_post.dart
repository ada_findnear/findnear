import 'package:images_picker/images_picker.dart';

class CreatePost {
  String parentId;
  String content;
  String viewer;
  List<Media> medias;
  String videoThumbnailPath;
  CreatePost({this.content, this.viewer, this.medias, this.parentId, this.videoThumbnailPath});

  Map<String, String> toMap() {
    var map = new Map<String, String>();
    map["content"] = content ?? "";
    map["viewer"] = viewer;
    if (parentId != null) map["parent_id"] = parentId;
    return map;
  }
}
