import 'package:findnear/src/utils/enum_utils.dart';

class UpdatePostGroupStatusInput {
  int postId;
  int groupId;
  PostGroupStatus status;
  String reason;
  bool is_banned;

  UpdatePostGroupStatusInput({
    this.postId,
    this.groupId,
    this.status,
    this.reason,
    this.is_banned,
  });

  Map<String, dynamic> toJson() {
    Map<String, dynamic> parameter = {};

    if (postId != null) {
      parameter["post_id"] = postId;
    }
    parameter["reason"] = reason ?? "";

    if (status != null) {
      parameter["status"] = status.toParam;
    }

    parameter["is_banned"] = is_banned ?? 0;

    return parameter;
  }

  static UpdatePostGroupStatusInput approvalPosts({
    int groupId,
    int postId,
  }) {
    return UpdatePostGroupStatusInput(
      groupId: groupId,
      postId: postId,
      status: PostGroupStatus.approved,
    );
  }

  static UpdatePostGroupStatusInput rejectPosts({
    int groupId,
    int postId,
  }) {
    return UpdatePostGroupStatusInput(
      groupId: groupId,
      postId: postId,
      status: PostGroupStatus.reject,
    );
  }

  static UpdatePostGroupStatusInput banPosts({
    int groupId,
    int postId,
    String reason,
  }) {
    return UpdatePostGroupStatusInput(
      groupId: groupId,
      postId: postId,
      status: PostGroupStatus.banned,
      reason: reason,
    );
  }
}
