class GroupInput {
  int groupId;
  String title;
  int group_type_id;
  String viewer;
  String content;

  GroupInput({
    this.groupId,
    this.title,
    this.group_type_id,
    this.viewer,
    this.content,
  });

  Map<String, dynamic> toJson() {
    Map<String, dynamic> parameter = {};

    if (title != null) {
      parameter["title"] = title;
    }
    if (group_type_id != null) {
      parameter["group_type_id"] = group_type_id;
    }
    if (viewer != null) {
      parameter["viewer"] = viewer;
    }
    if (content != null) {
      parameter["content"] = content;
    }

    return parameter;
  }
}
