import 'package:findnear/src/utils/enum_utils.dart';

class GetPostsOfGroupInput {
  int groupId;
  String content;
  PostGroupStatus status;

  GetPostsOfGroupInput({
    this.groupId,
    this.content,
    this.status,
  });

  Map<String, dynamic> toJson() {
    Map<String, dynamic> parameter = {};

    if (content != null) {
      parameter["content"] = content;
    }
    if (status != null) {
      parameter["status"] = status.toParam;
    }
    return parameter;
  }

  /**
   * Danh sach post request mới
   */
  static GetPostsOfGroupInput pendingPosts({int groupId}) {
    return GetPostsOfGroupInput(
      groupId: groupId,
      status: PostGroupStatus.pending,
    );
  }

  /**
   * Danh sach post đã từ chối
   */
  static GetPostsOfGroupInput approvalPost({int groupId}) {
    return GetPostsOfGroupInput(
      groupId: groupId,
      status: PostGroupStatus.approved,
    );
  }

  /**
   * Danh sach post đã mời
   */
  static GetPostsOfGroupInput rejectedPosts({int groupId}) {
    return GetPostsOfGroupInput(
      groupId: groupId,
      status: PostGroupStatus.reject,
    );
  }
}
