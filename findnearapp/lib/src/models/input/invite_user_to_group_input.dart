import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/enum_utils.dart';

class InviteUserToGroupInput {
  int group_id;
  int user_id;
  InviteUserToGroupStatus status;
  String content;

  InviteUserToGroupInput({
    this.group_id,
    this.user_id,
    this.status,
    this.content,
  });

  /**
   * Current user join group.
   */
  static InviteUserToGroupInput joinGroup(int groupId) {
    final userId = currentUser.value.id;

    return InviteUserToGroupInput(
      group_id: groupId,
      user_id: int.parse(userId),
      status: InviteUserToGroupStatus.pending,
    );
  }

  /**
   * Mời user vào group.
   */
  static InviteUserToGroupInput inviteToGroup({
    int groupId,
    int userId,
  }) {
    return InviteUserToGroupInput(
      group_id: groupId,
      user_id: userId,
      status: InviteUserToGroupStatus.invited,
    );
  }

  /**
   * Approval user vào group.
   */
  static InviteUserToGroupInput approvalToGroup({
    int groupId,
    int userId,
  }) {
    return InviteUserToGroupInput(
      group_id: groupId,
      user_id: userId,
      status: InviteUserToGroupStatus.approved,
    );
  }

  static InviteUserToGroupInput rejectToGroup({
    int groupId,
    int userId,
  }) {
    return InviteUserToGroupInput(
      group_id: groupId,
      user_id: userId,
      status: InviteUserToGroupStatus.rejected,
    );
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> parameter = {};

    if (user_id != null) {
      parameter["user_id"] = user_id;
    }
    parameter["content"] = content ?? "";
    if (status != null) {
      parameter["status"] = status.toParam;
    }

    return parameter;
  }
}
