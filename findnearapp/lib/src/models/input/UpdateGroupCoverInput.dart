class UpdateGroupCoverInput {
  int groupId;
  String coverFilePath;

  UpdateGroupCoverInput({
    this.groupId,
    this.coverFilePath,
  });
}
