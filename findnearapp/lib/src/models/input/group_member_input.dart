import 'package:findnear/src/utils/enum_utils.dart';

class GroupMemberInput {
  int groupId;
  int userId;
  String content;
  InviteUserToGroupStatus status;

  GroupMemberInput({
    this.groupId,
    this.userId,
    this.content,
    this.status,
  });

  Map<String, dynamic> toJson() {
    Map<String, dynamic> parameter = {};

    if (content != null) {
      parameter["content"] = content;
    }
    if (status != null) {
      parameter["status"] = status.toParam;
    }
    return parameter;
  }

  /**
   * Danh sach member request mới
   */
  static GroupMemberInput newMember({int groupId}) {
    return GroupMemberInput(
      groupId: groupId,
      status: InviteUserToGroupStatus.pending,
    );
  }

  /**
   * Danh sach member đã từ chối
   */
  static GroupMemberInput memberRejected({int groupId}) {
    return GroupMemberInput(
      groupId: groupId,
      status: InviteUserToGroupStatus.rejected,
    );
  }

  /**
   * Danh sach member đã mời
   */
  static GroupMemberInput memberInvited({int groupId}) {
    return GroupMemberInput(
      groupId: groupId,
      status: InviteUserToGroupStatus.invited,
    );
  }
}
