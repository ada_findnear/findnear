import 'package:findnear/src/models/address.dart';

import 'media.dart';
import 'user.dart';

class Shop {
  String address;
  int category_id;
  int city;
  String created_at;
  List<Object> custom_fields;
  String description;
  int district;
  bool has_media;
  int id;
  String latitude;
  String longitude;
  List<Media> media;
  bool recommended;
  String status;
  String street;
  String title;
  String updated_at;
  int user_id;
  User owner;
  double vote;
  double distance;
  int ward;
  int field_id;
  String img_url;
  String user_code;
  double range;
  double rating;
  bool isMonopoly;

  Shop(
      {this.address,
      this.category_id,
      this.city,
      this.created_at,
      this.custom_fields,
      this.description,
      this.district,
      this.has_media,
      this.id,
      this.latitude,
      this.longitude,
      this.media,
      this.recommended,
      this.status,
      this.street,
      this.title,
      this.updated_at,
      this.user_id,
      this.owner,
      this.vote,
      this.distance,
      this.ward,
      this.field_id,
      this.img_url,
      this.user_code,
      this.range,
      this.rating,
      this.isMonopoly});

  factory Shop.fromJson(Map<String, dynamic> json) {
    return Shop(
      address: json['address'] != null ? json['address'] : '',
      category_id: json['category_id'],
      city: json['city'],
      created_at: json['created_at'],
      //custom_fields: json['custom_fields'] != null ? (json['custom_fields'] as List).map((i) => Object.fromJson(i)).toList() : null,
      description: json['description'],
      district: json['district'],
      has_media: json['has_media'],
      id: json['id'],
      latitude: json['latitude'] != null ? json['latitude'] : null,
      longitude: json['longitude'] != null ? json['longitude'] : null,
      media: json['media'] != null
          ? (json['media'] as List).map((i) => Media.fromJSON(i)).toList()
          : null,
      recommended: json['recommended'],
      status: json['status'],
      street: json['street'],
      title: json['title'],
      updated_at: json['updated_at'],
      user_id: json['user_id'],
      owner: json['owner'] != null ? User.fromJSON(json['owner']) : new User(),
      vote: double.tryParse(json['vote'].toString()),
      distance: json['distance'] != null
          ? double.tryParse(json['distance']?.toString() ?? "")
          : 0,
      ward: json['ward'],
      field_id: json['field_id'],
      img_url: json['img_url'] != null ? json['img_url'] : '',
      user_code: json['user_code'] != null ? json['user_code'] : '',
      range: json['range'] != null
          ? double.tryParse(json['range']?.toString() ?? "")
          : 0,
      rating: json['rating'] != null
          ? double.tryParse(json['rating']?.toString() ?? "")
          : 0,
      isMonopoly: json['is_monopoly'] != null
          ? json['is_monopoly'] == 0
              ? false
              : true
          : false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['category_id'] = this.category_id;
    data['city'] = this.city;
    data['created_at'] = this.created_at;
    data['description'] = this.description;
    data['district'] = this.district;
    data['has_media'] = this.has_media;
    data['id'] = this.id;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['recommended'] = this.recommended;
    data['status'] = this.status;
    data['street'] = this.street;
    data['title'] = this.title;
    data['updated_at'] = this.updated_at;
    data['user_id'] = this.user_id;
    data['vote'] = this.vote;
    data['ward'] = this.ward;
    data['address'] = this.address;
    data['img_url'] = this.img_url;
    data['user_code'] = this.user_code;
    if (this.media != null) {
      data['media'] = this.media.map((v) => v.toMap()).toList();
    }
    data['rating'] = this.rating;
    return data;
  }

  String get firstThumb => media?.isNotEmpty == true ? media[0].thumb : "";
  String get image => media?.isNotEmpty == true ? media[0].url : "";
}
