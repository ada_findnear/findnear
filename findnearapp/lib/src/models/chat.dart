import 'package:findnear/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'user.dart';

enum ChatType { text, image, video, system, sticker, gif, location, voice, file }
enum TypeSystem {
  createGroup,
  addMember,
  removeMember,
  outGroup,
  imageChat,
  videoChat
}
enum TypeEndCall { normal, deny, miss }

class Chat {
  // String id = UniqueKey().toString();
  String id; //millisecondsSinceEpoch
  // message text
  String text;

  // time of the message
  int time; //millisecondsSinceEpoch
  // user id who send the message
  String userId;
  String url;
  String urlThumb;
  int type;

  User user;

  String documentId;
  Chat parentMessage;
  String stickerId;

  String gifUrl;
  String locationUrl;

  Duration durationVoice;

  //Quoted
  bool isQuotedMessage;
  String quotedInfo;

  Chat(
    this.id,
    this.text,
    this.time,
    this.userId,
    this.url,
    this.urlThumb,
    this.type, {
    this.parentMessage,
    this.stickerId,
    this.gifUrl,
    this.durationVoice,
    this.isQuotedMessage,
    this.quotedInfo,
    this.locationUrl,
  });

  Chat.from({
    this.id,
    this.text,
    this.time,
    this.userId,
    this.url,
    this.urlThumb,
    this.type,
    this.user,
    this.documentId,
    this.parentMessage,
    this.stickerId,
    this.gifUrl,
    this.durationVoice,
    this.isQuotedMessage,
    this.quotedInfo,
    this.locationUrl,
  });

  Chat copyWith({
    String id,
    String text,
    int time,
    String userId,
    String url,
    String urlThumb,
    int type,
    User user,
    String documentId,
    Chat parentMessage,
    String stickerId,
    String gifUrl,
    String durationVoice,
    bool isQuotedMessage,
    String quotedInfo,
    String locationUrl
  }) {
    return Chat.from(
      id: id ?? this.id,
      text: text ?? this.text,
      time: time ?? this.time,
      userId: userId ?? this.userId,
      url: url ?? this.url,
      urlThumb: urlThumb ?? this.urlThumb,
      type: type ?? this.type,
      user: user ?? this.user,
      documentId: documentId ?? this.documentId,
      parentMessage: parentMessage ?? this.parentMessage,
      stickerId: stickerId ?? this.stickerId,
      gifUrl: gifUrl ?? this.gifUrl,
      durationVoice: durationVoice ?? this.durationVoice,
      isQuotedMessage: isQuotedMessage ?? this.isQuotedMessage,
      quotedInfo: quotedInfo ?? this.quotedInfo,
      locationUrl: locationUrl ?? this.locationUrl,
    );
  }

  Chat.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'] != null ? jsonMap['id'].toString() : null;
      text = jsonMap['text'] != null ? jsonMap['text'].toString() : '';
      time = jsonMap['time'] != null ? jsonMap['time'] : 0;
      userId = jsonMap['user'] != null ? jsonMap['user'].toString() : null;
      url = jsonMap['url'] != null ? jsonMap['url'].toString() : null;
      urlThumb =
          jsonMap['url_thumb'] != null ? jsonMap['url_thumb'].toString() : null;
      type = jsonMap['type'] != null ? jsonMap['type'] : 0;
      parentMessage = jsonMap['parent_message'] != null
          ? Chat.fromJSON(Map<String, dynamic>.from(jsonMap['parent_message']))
          : null;
      stickerId = jsonMap['sticker_id'];
      gifUrl = jsonMap['gif_url'];
      durationVoice = Duration(seconds: jsonMap['duration_voice'] ?? 0);
      isQuotedMessage = jsonMap['is_quoted_message'];
      quotedInfo = jsonMap['quoted_info'];
      locationUrl = jsonMap['location_url'];
    } catch (e) {
      id = null;
      text = '';
      time = 0;
      user = null;
      userId = null;
      url = null;
      urlThumb = null;
      type = 0;
      gifUrl = '';
      locationUrl = '';
      durationVoice = Duration(seconds: 0);
      print(e);
    }
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["text"] = text;
    map["time"] = time;
    map["user"] = userId;
    map["url"] = url;
    map["url_thumb"] = urlThumb;
    map["type"] = type;
    map["parent_message"] = parentMessage?.toMap();
    map["sticker_id"] = stickerId;
    map["gif_url"] = gifUrl;
    map["duration_voice"] = durationVoice?.inSeconds;
    map['is_quoted_message'] = isQuotedMessage;
    map['quoted_info'] = quotedInfo;
    map['location_url'] = locationUrl;
    return map;
  }

  String get messageText {
    if (type == ChatType.text.index) {
      return text ?? "";
    } else if (type == ChatType.video.index) {
      return S.of(Get.context).video;
    } else if (type == ChatType.image.index) {
      return S.of(Get.context).photo;
    } else if (type == ChatType.sticker.index) {
      return "Sticker";
    } else if (type == ChatType.gif.index) {
      return "GIF";
    } else if (type == ChatType.location.index) {
      return "Location";
    } else if (type == ChatType.voice.index) {
      return "Voice";
    } else {
      return "";
    }
  }
}
