import 'user.dart';
import 'media.dart';

class NewsfeedComment {
  final String id;
  final String postId;
  final String userId;
  final String content;
  final String createdAt;
  final String updatedAt;
  final String deletedAt;
  final String parentId;
  final User creator;
  final List<Media> media;
  final List<NewsfeedComment> children;
  final bool isLike;

  NewsfeedComment({
    this.id,
    this.postId,
    this.userId,
    this.content,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
    this.parentId,
    this.creator,
    this.media,
    this.children,
    this.isLike,
  });

  factory NewsfeedComment.fromMap(Map<String, dynamic> map) {
    return NewsfeedComment(
      id: map['id'] != null ? map['id'].toString() : '',
      postId: map['post_id'] != null ? map['post_id'].toString() : '',
      userId: map['user_id'] != null ? map['user_id'].toString() : '',
      content: map['content'] != null ? map['content'].toString() : '',
      createdAt: map['created_at'] != null ? map['created_at'].toString() : '',
      updatedAt: map['updated_at'] != null ? map['updated_at'].toString() : '',
      deletedAt: map['deleted_at'] != null ? map['deleted_at'].toString() : '',
      parentId: map['parent_id'] != null ? map['parent_id'].toString() : '',
      creator: map['creator'] != null ? User.fromJSON(map['creator']) : null,
      media: map['media'] is List
          ? (map['media'] as List)
              .map((e) => e is Map<String, dynamic> ? Media.fromJSON(e) : null)
              .where((element) => element != null)
              .toList(growable: false)
          : [],
      children: map['children'] is List
          ? (map['children'] as List)
              .map((e) =>
                  e is Map<String, dynamic> ? NewsfeedComment.fromMap(e) : null)
              .where((element) => element != null)
              .toList(growable: false)
          : [],
      isLike: map['isLike'] != null ? map['isLike'] : false,
    );
  }

  Map<String, dynamic> toMap({withoutMedia = false}) {
    return {
      'id': this.id,
      'post_id': this.postId,
      'user_id': this.userId,
      'content': this.content,
      'created_at': this.createdAt,
      'updated_at': this.updatedAt,
      'deleted_at': this.deletedAt,
      'parent_id': this.parentId,
      'creator': this.creator,
      if (!withoutMedia) 'media': this.media,
      'children': this.children,
      'isLike': this.isLike,
    };
  }

  NewsfeedComment copyWith({
    String id,
    String postId,
    String userId,
    String content,
    String createdAt,
    String updatedAt,
    String deletedAt,
    String parentId,
    User creator,
    List<Media> media,
    List<NewsfeedComment> children,
    bool isLike,
  }) {
    return NewsfeedComment(
      id: id ?? this.id,
      postId: postId ?? this.postId,
      userId: userId ?? this.userId,
      content: content ?? this.content,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
      deletedAt: deletedAt ?? this.deletedAt,
      parentId: parentId ?? this.parentId,
      creator: creator ?? this.creator,
      media: media ?? this.media,
      children: children ?? this.children,
      isLike: isLike ?? this.isLike,
    );
  }

  bool get isReply => parentId != "0" && parentId != "";
}
