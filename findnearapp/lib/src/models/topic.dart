import 'media.dart';

class Topic {
  String id;
  String title;
  String desc;
  String date;
  Media image;
  int likeCount;
  int commentCount;

  Topic();
  Topic.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap["id"];
    } catch (e) {
      print(e);
    }
  }
}
