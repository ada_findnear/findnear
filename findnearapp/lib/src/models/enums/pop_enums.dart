enum PopTarget { newsfeed, market, livestreamList }

extension PopTargetExtension on PopTarget {
  int get index {
    switch (this) {
      case PopTarget.newsfeed:
        return 0;
      case PopTarget.market:
        return 1;
      case PopTarget.livestreamList:
        return 5;
      default:
        return -1;
    }
  }
}
