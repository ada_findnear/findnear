/// socket event
enum WebsocketEvent {
  UPDATE_USERNAME,
  MESSAGE_SERVER,
  MESSAGE,
}

extension WebsocketEventExtension on WebsocketEvent {
  static WebsocketEvent fromEventName(String eventName) {
    if (eventName == "updateUserName") {
      return WebsocketEvent.UPDATE_USERNAME;
    } else if (eventName == "message-server") {
      return WebsocketEvent.MESSAGE_SERVER;
    } else if(eventName == "message"){
      return WebsocketEvent.MESSAGE;
    }
    return null;
  }

  String get value {
    switch (this) {
      case WebsocketEvent.UPDATE_USERNAME:
        return "updateUserName";
      case WebsocketEvent.MESSAGE_SERVER:
        return "message-server";
      case WebsocketEvent.MESSAGE:
        return "message";
      default:
        return "";
    }
  }
}

/// socket action
enum WebsocketAction {
  CHAT,
  VIDEO_CALL,
  COMMENT_LIVE_STREAM,
}

extension WebsocketActionExtension on WebsocketAction {
  static WebsocketAction fromActionName(String eventName) {
    if (eventName == "CHAT") {
      return WebsocketAction.CHAT;
    } else if(eventName == "VIDEO_CALL"){
      return WebsocketAction.VIDEO_CALL;
    } else if(eventName == "COMMENT_LIVE_STREAM"){
      return WebsocketAction.COMMENT_LIVE_STREAM;
    }
    return null;
  }

  String get value {
    switch (this) {
      case WebsocketAction.CHAT:
        return "CHAT";
      case WebsocketAction.VIDEO_CALL:
        return "VIDEO_CALL";
      case WebsocketAction.COMMENT_LIVE_STREAM:
        return "COMMENT_LIVE_STREAM";
      default:
        return "";
    }
  }
}

/// socket MessageType
enum WebsocketMessageType {
  CREATE_CONVERSATION,
  LEFT_GROUP,
  DELETE_CONVERSATION,
  RECEIVE_MESSAGE,
  DELETE_MESSAGE,
  ADD_USER_TO_GROUP,
}

extension WebsocketMessageTypeExtension on WebsocketMessageType {
  static WebsocketMessageType fromMessageTypeName(String eventName) {
    if (eventName == "CREATE_CONVERSATION") {
      return WebsocketMessageType.CREATE_CONVERSATION;
    } else if (eventName == "LEFT_GROUP") {
      return WebsocketMessageType.LEFT_GROUP;
    } else if (eventName == "DELETE_CONVERSATION") {
      return WebsocketMessageType.DELETE_CONVERSATION;
    } else if (eventName == "RECEIVE_MESSAGE") {
      return WebsocketMessageType.RECEIVE_MESSAGE;
    } else if (eventName == "DELETE_MESSAGE") {
      return WebsocketMessageType.DELETE_MESSAGE;
    } else if (eventName == "ADD_USER_TO_GROUP") {
      return WebsocketMessageType.ADD_USER_TO_GROUP;
    }
    return null;
  }

  String get value {
    switch (this) {
      case WebsocketMessageType.CREATE_CONVERSATION:
        return "CREATE_CONVERSATION";
      case WebsocketMessageType.LEFT_GROUP:
        return "LEFT_GROUP";
      case WebsocketMessageType.DELETE_CONVERSATION:
        return "DELETE_CONVERSATION";
      case WebsocketMessageType.RECEIVE_MESSAGE:
        return "RECEIVE_MESSAGE";
      case WebsocketMessageType.DELETE_MESSAGE:
        return "DELETE_MESSAGE";
      case WebsocketMessageType.ADD_USER_TO_GROUP:
        return "ADD_USER_TO_GROUP";
      default:
        return "";
    }
  }
}

enum WebsocketVideoCallType {
  LOGIN,
  OFFER,
  ANSWER,
  CANDIDATE,
  LEAVE,
  UPDATESTATUS,
  READY,
}

extension WebsocketVideoCallTypeExtension on WebsocketVideoCallType {
  static WebsocketVideoCallType fromMessageTypeName(String eventName) {
    if (eventName == "login") {
      return WebsocketVideoCallType.LOGIN;
    } else if (eventName == "offer") {
      return WebsocketVideoCallType.OFFER;
    } else if (eventName == "answer") {
      return WebsocketVideoCallType.ANSWER;
    } else if (eventName == "candidate") {
      return WebsocketVideoCallType.CANDIDATE;
    }else if (eventName == "leave") {
      return WebsocketVideoCallType.LEAVE;
    }else if (eventName == "updateStatus") {
      return WebsocketVideoCallType.UPDATESTATUS;
    }else if (eventName == "ready") {
      return WebsocketVideoCallType.READY;
    }
    return null;
  }

  String get value {
    switch (this) {
      case WebsocketVideoCallType.LOGIN:
        return "login";
      case WebsocketVideoCallType.OFFER:
        return "offer";
      case WebsocketVideoCallType.ANSWER:
        return "answer";
      case WebsocketVideoCallType.CANDIDATE:
        return "candidate";
      case WebsocketVideoCallType.LEAVE:
        return "leave";
      case WebsocketVideoCallType.UPDATESTATUS:
        return "updateStatus";
      default:
        return "";
    }
  }
}

/// socket commentType
enum WebSocketCommentType {
  NEW_COMMENT,
  LIKE_COMMENT,
  UPDATE_COUNT_JOIN_LIVESTREAM,
  KICK_MEMBER
}

extension WebSocketCommentTypeExtension on WebSocketCommentType {
  static WebSocketCommentType fromMessageTypeName(String eventName) {
    if (eventName == "NEW_COMMENT") {
      return WebSocketCommentType.NEW_COMMENT;
    } else if (eventName == "LIKE_COMMENT") {
      return WebSocketCommentType.LIKE_COMMENT;
    } else if (eventName == "UPDATE_COUNT_JOIN_LIVESTREAM") {
      return WebSocketCommentType.UPDATE_COUNT_JOIN_LIVESTREAM;
    } else if (eventName == "KICK_MEMBER") {
      return WebSocketCommentType.KICK_MEMBER;
    }
    return null;
  }

  String get value {
    switch (this) {
      case WebSocketCommentType.NEW_COMMENT:
        return "NEW_COMMENT";
      case WebSocketCommentType.LIKE_COMMENT:
        return "LIKE_COMMENT";
      case WebSocketCommentType.UPDATE_COUNT_JOIN_LIVESTREAM:
        return "UPDATE_COUNT_JOIN_LIVESTREAM";
      case WebSocketCommentType.KICK_MEMBER:
        return "KICK_MEMBER";
      default:
        return "";
    }
  }
}

