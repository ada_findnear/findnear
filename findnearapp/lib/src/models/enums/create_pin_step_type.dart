import 'package:findnear/main.dart';
import 'package:get/get.dart';

enum CreatePINStepType {
  INPUT_NEW_PIN_CODE,
  REINPUT_NEW_PIN_CODE
}

extension CreatePINStepTypeExtension on CreatePINStepType {
  String get title {
    switch (this) {
      case CreatePINStepType.INPUT_NEW_PIN_CODE:
        return S
            .of(Get.context)
            .input_pin_code;
      case CreatePINStepType.REINPUT_NEW_PIN_CODE:
        return S
            .of(Get.context)
            .confirm_input_pin_code;
      default:
        return "";
    }
  }

  String get content {
    switch (this) {
      case CreatePINStepType.INPUT_NEW_PIN_CODE:
        return S
            .of(Get.context)
            .input_pin_code_content;
      case CreatePINStepType.REINPUT_NEW_PIN_CODE:
        return S
            .of(Get.context)
            .confirm_input_pin_code_content;
      default:
        return "";
    }
  }
}
