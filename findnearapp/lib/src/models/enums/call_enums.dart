enum HistoryCallStatus { missed, completed, busy, noAnswer }

HistoryCallStatus callTypeFromString(String status) {
  switch (status) {
    case 'missed':
      return HistoryCallStatus.missed;
    case 'busy':
      return HistoryCallStatus.busy;
    case 'no-answer':
      return HistoryCallStatus.noAnswer;
    case 'completed':
      return HistoryCallStatus.completed;
    default:
      return HistoryCallStatus.missed;
  }
}
