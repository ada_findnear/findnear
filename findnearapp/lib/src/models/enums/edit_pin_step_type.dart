import 'package:findnear/main.dart';
import 'package:get/get.dart';

enum EditPINStepType {
  NONE,
  INPUT_OLD_PIN_CODE,
  INPUT_NEW_PIN_CODE,
  REINPUT_NEW_PIN_CODE
}

extension EditPINStepTypeExtension on EditPINStepType {
  String get title {
    switch (this) {
      case EditPINStepType.INPUT_OLD_PIN_CODE:
        return S
            .of(Get.context)
            .input_old_pin_code;
      case EditPINStepType.INPUT_NEW_PIN_CODE:
        return S
            .of(Get.context)
            .input_new_pin_code;
      case EditPINStepType.REINPUT_NEW_PIN_CODE:
        return S
            .of(Get.context)
            .reinput_new_pin_code;
      default:
        return "";
    }
  }
}
