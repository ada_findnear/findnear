enum FindnearWheelPrice { F, I, N, D, E, A, R, BANNER, ID, XU, UNLUCKY }

extension FindnearWheelPriceExtension on FindnearWheelPrice {
  int get number {
    switch (this) {
      case FindnearWheelPrice.UNLUCKY:
        return 0;
      case FindnearWheelPrice.A:
        return 1;
      case FindnearWheelPrice.R:
        return 2;
      case FindnearWheelPrice.XU:
        return 3;
      case FindnearWheelPrice.F:
        return 4;
      case FindnearWheelPrice.I:
        return 5;
      case FindnearWheelPrice.BANNER:
        return 6;
      case FindnearWheelPrice.N:
        return 7;
      case FindnearWheelPrice.D:
        return 8;
      case FindnearWheelPrice.ID:
        return 9;
      case FindnearWheelPrice.E:
        return 10;
      default:
        return 0;
    }
  }

  String get text {
    switch (this) {
      case FindnearWheelPrice.UNLUCKY:
        return "0";
      case FindnearWheelPrice.A:
        return "A";
      case FindnearWheelPrice.R:
        return "R";
      case FindnearWheelPrice.XU:
        return "2000";
      case FindnearWheelPrice.F:
        return "F";
      case FindnearWheelPrice.I:
        return "I";
      case FindnearWheelPrice.BANNER:
        return "BANNER";
      case FindnearWheelPrice.N:
        return "N";
      case FindnearWheelPrice.D:
        return "D";
      case FindnearWheelPrice.ID:
        return "ID";
      case FindnearWheelPrice.E:
        return "E";
      default:
        return "0";
    }
  }

  String get description {
    switch (this) {
      case FindnearWheelPrice.UNLUCKY:
        return "Chúc bạn may mắn lần sau";
      case FindnearWheelPrice.A:
        return "Bạn đã nhận được chữ A";
      case FindnearWheelPrice.R:
        return "Bạn đã nhận được chữ R";
      case FindnearWheelPrice.XU:
        return "Bạn đã nhận được 2000 xu";
      case FindnearWheelPrice.F:
        return "Bạn đã nhận được chữ F";
      case FindnearWheelPrice.I:
        return "Bạn đã nhận được chữ I";
      case FindnearWheelPrice.BANNER:
        return "Bạn đã nhận được BANNER";
      case FindnearWheelPrice.N:
        return "Bạn đã nhận được chữ N";
      case FindnearWheelPrice.D:
        return "Bạn đã nhận được chữ D";
      case FindnearWheelPrice.ID:
        return "Bạn đã nhận được ID Gian hàng";
      case FindnearWheelPrice.E:
        return "Bạn đã nhận được chữ E";
      default:
        return "Chúc bạn may mắn lần sau";
    }
  }

  static FindnearWheelPrice fromText(String text) {
    if (text == "0")
      return FindnearWheelPrice.UNLUCKY;
    else if (text == "A")
      return FindnearWheelPrice.A;
    else if (text == "R")
      return FindnearWheelPrice.R;
    else if (text == "2000")
      return FindnearWheelPrice.XU;
    else if (text == "F")
      return FindnearWheelPrice.F;
    else if (text == "I")
      return FindnearWheelPrice.I;
    else if (text == "BANNER")
      return FindnearWheelPrice.BANNER;
    else if (text == "N")
      return FindnearWheelPrice.N;
    else if (text == "D")
      return FindnearWheelPrice.D;
    else if (text == "ID")
      return FindnearWheelPrice.ID;
    else if (text == "E")
      return FindnearWheelPrice.E;
    return FindnearWheelPrice.UNLUCKY;
  }


}
