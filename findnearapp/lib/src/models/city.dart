import '../helpers/custom_trace.dart';
import 'district.dart';

class City {
  int idx;
  String vnname;
  String code;
  List<District> districts;

  int indexUI;
  City();

  City.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      idx = jsonMap['idx'];
      vnname = jsonMap['vnname'] != null ? jsonMap['vnname'].toString() : '';
      code = jsonMap['code'] != null ? jsonMap['code'].toString() : '';
      districts =  (jsonMap['districts'] as List<dynamic>)
          .map((e) => District.fromJSON(e as Map<String, dynamic>))
          .toList();

    } catch (e) {
      print(CustomTrace(StackTrace.current, message: e));
    }
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["idx"] = idx;
    map["vnname"] = vnname;
    map["code"] = code;
    map["districts"] = districts;
    return map;
  }

  @override
  bool operator ==(dynamic other) {
    return other.idx == this.idx;
  }

  @override
  int get hashCode => this.idx.hashCode;

  void createIndex(int indexUI) {
    this.indexUI = indexUI;
    int index = 0;
    for (final item in districts){
      item.createIndex(index);
      index++;
    }
  }
}
