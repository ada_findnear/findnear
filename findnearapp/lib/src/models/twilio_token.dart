class TwilioToken {
  bool success;
  String accessToken;
  String message;

  TwilioToken({this.success, this.accessToken, this.message});

  TwilioToken.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    accessToken = json['data'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['data'] = this.accessToken;
    data['message'] = this.message;
    return data;
  }
}
