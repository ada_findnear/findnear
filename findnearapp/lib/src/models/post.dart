import 'dart:convert';

import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/models/media.dart';
import 'package:findnear/src/models/newsfeed_comment.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/utils/logger.dart';

import '../helpers/custom_trace.dart';

class Post {
  String id;
  String content;
  List<Media> medias;
  String userId;
  String created_at;
  String likers_count;
  String comments_count;
  bool isLike;
  String viewer;
  User creator;
  Media thumbnail;
  List<User> likers;
  List<NewsfeedComment> comments;
  Post originalPost;
  bool isReported;
  GroupEntity group;

  Post({
    this.id,
    this.content,
    this.medias,
    this.userId,
    this.created_at,
    this.likers_count,
    this.comments_count,
    this.isLike,
    this.viewer,
    this.creator,
    this.likers,
    this.comments,
    this.originalPost,
    this.isReported = false,
    this.group,
  });

  Post.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      userId = jsonMap['user_id'].toString();
      content = jsonMap['content'] != null ? jsonMap['content'] : '';
      created_at = jsonMap['created_at'].toString();
      if (jsonMap['thumbnail'] != null &&
          jsonMap['thumbnail'] is Map<String, dynamic>)
        thumbnail = Media.fromJSON(jsonMap['thumbnail']);
      likers_count = jsonMap['likers_count'] != null
          ? jsonMap['likers_count'].toString()
          : '';
      comments_count = jsonMap['comments_count'] != null
          ? jsonMap['comments_count'].toString()
          : '';
      medias = jsonMap['media'] != null
          ? List.from(jsonMap['media'])
              .map((element) => () {
                    var media = Media.fromJSON(element);
                    if (media.isVideo && thumbnail != null) {
                      media.thumb = thumbnail.url;
                      media.thumbId = thumbnail.id;
                    }
                    return media;
                  }())
              .where((element) => element.isImagePostCollection)
              .toList()
          : [];
      isLike = jsonMap['isLike'];
      viewer = jsonMap['viewer'].toString();
      creator =
          jsonMap['creator'] != null ? User.fromJSON(jsonMap['creator']) : null;
      likers = jsonMap['likers'] != null
          ? List.from(jsonMap['likers'])
              .map((e) => User.fromJSON(e))
              .toList(growable: false)
          : [];
      comments = jsonMap['comments'] != null
          ? List.from(jsonMap['comments'])
              .map((e) => NewsfeedComment.fromMap(e))
              .toList(growable: false)
          : [];
      originalPost =
          jsonMap['parent'] != null ? Post.fromJSON(jsonMap['parent']) : null;
      isReported = false;

      if (jsonMap['group'] != null && jsonMap['group'] is Map<String, dynamic>)
        group = GroupEntity.fromJson(jsonMap['group']);
    } catch (e) {
      id = '';
      userId = '';
      content = '';
      created_at = '';
      likers_count = '';
      comments_count = '';
      medias = [];
      isLike = false;
      viewer = '';
      creator = User();
      likers = [];
      comments = [];
      originalPost = null;
      isReported = false;
      logger.e(e);
      print(CustomTrace(StackTrace.current, message: e));
    }
  }

  factory Post.fromMap(Map<String, dynamic> map) {
    Media thumbnail = null;
    if (map['thumbnail'] != null && map['thumbnail'] is Map<String, dynamic>)
      thumbnail = Media.fromJSON(map['thumbnail']);
    return Post(
      id: map['id'].toString(),
      content: map['content'] ?? "",
      medias: map['media'] != null
          ? List.from(map['media'])
              .map((element) => () {
                    var media = Media.fromJSON(element);
                    if (media.isVideo && thumbnail != null) {
                      media.thumb = thumbnail.url;
                      media.thumbId = thumbnail.id;
                    }
                    return media;
                  }())
              .where((element) => element.isImagePostCollection)
              .toList()
          : [],
      userId: map['userId'].toString(),
      created_at: map['created_at'].toString(),
      likers_count: map['likers_count'].toString(),
      comments_count: map['comments_count'].toString(),
      isLike: map['isLike'],
      viewer: map['viewer'].toString(),
      creator: map['creator'] != null ? User.fromJSON(map['creator']) : null,
      likers: map['likers'] != null
          ? List.from(map['likers'])
              .map((e) => User.fromJSON(e))
              .toList(growable: false)
          : [],
      comments: map['comments'] != null
          ? List.from(map['comments'])
              .map((e) => NewsfeedComment.fromMap(e))
              .toList(growable: false)
          : [],
      originalPost: map['parent'] != null ? Post.fromMap(map['parent']) : null,
      group: map['group'] != null && map['group'] is Map<String, dynamic>
          ? GroupEntity.fromJson(map['group'])
          : null,
    );
  }

  Map<String, String> toMapCreateRequest() {
    var media = "";
    if (this.medias?.isNotEmpty == true)
      media =
          jsonDecode(this.medias?.map((e) => e?.toMap())?.toList()?.toString());
    return {
      'id': this.id,
      'content': this.content,
      'medias': media,
      'userId': this.userId,
      'created_at': this.created_at,
      'likers_count': this.likers_count,
      'comments_count': this.comments_count,
      'isLike': this.isLike?.toString(),
      'viewer': this.viewer,
      'creator': this.creator?.toMap()?.toString()
    };
  }

  Map toMap() {
    var map = new Map<String, String>();
    map["id"] = id;
    map["content"] = content;
    map["viewer"] = viewer;
    return map;
  }

  Map<String, dynamic> toMapComplex() {
    return {
      'id': this.id,
      'content': this.content,
      'medias': this.medias,
      'userId': this.userId,
      'created_at': this.created_at,
      'likers_count': this.likers_count,
      'comments_count': this.comments_count,
      'isLike': this.isLike,
      'viewer': this.viewer,
      'creator': this.creator,
      'likers': this.likers,
      'comments': this.comments,
    };
  }

  @override
  bool operator ==(Object other) =>
      other is Post &&
      runtimeType == other.runtimeType &&
      id == other.id &&
      content == other.content &&
      medias == other.medias &&
      userId == other.userId &&
      created_at == other.created_at &&
      likers_count == other.likers_count &&
      comments_count == other.comments_count &&
      isLike == other.isLike &&
      viewer == other.viewer &&
      creator == other.creator &&
      likers == other.likers &&
      comments == other.comments;

  @override
  int get hashCode => this.id.hashCode;

  Post copyWith({
    String id,
    String content,
    List<Media> medias,
    String userId,
    String created_at,
    String likers_count,
    String comments_count,
    bool isLike,
    String viewer,
    User creator,
    List<User> likers,
    List<NewsfeedComment> comments,
    Post originalPost,
  }) {
    return Post(
      id: id ?? this.id,
      content: content ?? this.content,
      medias: medias ?? this.medias,
      userId: userId ?? this.userId,
      created_at: created_at ?? this.created_at,
      likers_count: likers_count ?? this.likers_count,
      comments_count: comments_count ?? this.comments_count,
      isLike: isLike ?? this.isLike,
      viewer: viewer ?? this.viewer,
      creator: creator ?? this.creator,
      likers: likers ?? this.likers,
      comments: comments ?? this.comments,
      originalPost: originalPost ?? this.originalPost,
    );
  }

  bool get isSharedPost => originalPost != null;

  @override
  String toString() {
    return 'Post{id: $id, content: $content, originalPost: $originalPost}';
  }
}
