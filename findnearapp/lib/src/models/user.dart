import 'dart:io';

import 'package:findnear/src/models/shop.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:flutter/material.dart';

import '../models/media.dart';
import 'entities/communication/entities/short_user_entity.dart';
import 'field.dart';
import 'market.dart';

enum UserState { available, away, busy }

class User {
  String id;
  String name;
  String dealerCode;
  String email;
  String password;
  String apiToken;
  String deviceToken;
  String phone;
  bool verifiedPhone;
  String verificationId;
  String address;
  String bio;
  Media image;
  Media imageCover;
  bool isFriend;
  String gender;
  String age;
  String code;
  String auth_code;
  bool has_friend_request_from;
  bool has_sent_friend_request_to;
  Market market;
  Shop shop;
  Field field;
  String fieldId;
  bool isBlocking;
  bool isBlocker;
  int totalPoint;
  int currentPoint;
  bool isLiveStreaming;
  bool isOnline;
  bool gameCanNotification;
  bool isActive;
  bool canCreateShop;
  String avatar;
  String expiredAt;
  // used for indicate if client logged in or not
  bool auth;
  String last_updated;

//  String role;

  User();

  User.name({
    this.id,
    this.name,
    this.dealerCode,
    this.email,
    this.password,
    this.apiToken,
    this.deviceToken,
    this.phone,
    this.verifiedPhone,
    this.verificationId,
    this.address,
    this.bio,
    this.image,
    this.imageCover,
    this.isFriend,
    this.gender,
    this.age,
    this.code,
    this.auth_code,
    this.has_friend_request_from,
    this.has_sent_friend_request_to,
    this.market,
    this.shop,
    this.field,
    this.fieldId,
    this.isBlocking,
    this.isBlocker,
    this.totalPoint,
    this.currentPoint,
    this.isLiveStreaming,
    this.isOnline,
    this.auth,
    this.gameCanNotification,
    this.isActive,
    this.canCreateShop,
    this.expiredAt,
    this.avatar,
    this.last_updated,
  });

  DateTime get expiredDate {
    try {
      return DateTime.parse(expiredAt);
    } catch (e) {
      return null;
    }
  }

  DateTime get lastUpdatedDate {
    try {
      return DateTime.parse(last_updated);
    } catch (e) {
      return null;
    }
  }

  User.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      name = jsonMap['name'] != null ? jsonMap['name'] : '';
      email = jsonMap['email'] != null ? jsonMap['email'] : '';
      apiToken = jsonMap['api_token'];
      deviceToken = jsonMap['device_token'];
      isFriend = jsonMap['is_friend'];
      code = jsonMap['code'];
      auth_code = jsonMap['auth_code'];
      isBlocking = jsonMap['is_blocking'] ?? false;
      isBlocker = jsonMap['is_blocker'];
      totalPoint = int.tryParse(jsonMap['total_point']?.toString() ?? "0") ?? 0;
      currentPoint =
          int.tryParse(jsonMap['current_point']?.toString() ?? "0") ?? 0;
      isLiveStreaming = jsonMap['isLiveStreaming'];
      gameCanNotification =
          (jsonMap['game_can_notification']?.toString() ?? 'false').toBool;
      isOnline = jsonMap['is_online'];
      isActive = jsonMap['active'];
      canCreateShop =
          jsonMap['can_create_shop'].toString().toLowerCase().toBool;
      expiredAt = jsonMap['expired_at'];
      avatar = jsonMap['avatar'].toString() ?? "";
      last_updated = jsonMap['last_updated'];

      try {
        age = jsonMap['age']?.toString() ?? "";
      } catch (e) {
        prettyLog.e(e);
        age = "";
      }
      try {
        gender = jsonMap['custom_fields']['gender']['view'];
      } catch (e) {
        // prettyLog.e(e);
        gender = "";
      }
      try {
        phone = jsonMap['custom_fields']['phone']['view'];
      } catch (e) {
        //prettyLog.e(e);
        phone = "";
      }
      try {
        verifiedPhone = jsonMap['custom_fields']['verifiedPhone']['view'] == '1'
            ? true
            : false;
      } catch (e) {
        //prettyLog.e(e);
        verifiedPhone = false;
      }
      try {
        field = Field.fromJSON(jsonMap['market']['fields'][0]);
      } catch (e) {
        //prettyLog.e(e);
        field = new Field();
      }
      try {
        address = jsonMap['custom_fields']['address']['view'];
      } catch (e) {
        // prettyLog.e(e);
        address = "";
      }
      try {
        bio = jsonMap['custom_fields']['bio']['view'];
      } catch (e) {
        //prettyLog.e(e);
        bio = "";
      }
      try {
        image = Media.fromJSON((jsonMap['media'] as List)
            .lastWhere((element) => element['collection_name'] == 'avatar'));
      } catch (_) {
        try {
          if ((jsonMap["media"] ?? {})["collection_name"] == "avatar") {
            image = Media.fromJSON(
                Map<String, dynamic>.from(jsonMap["media"] ?? {}));
          }
        } catch (e) {
          prettyLog.e(e);
          image = new Media();
        }
      }
      try {
        if ((jsonMap['media'] as List)?.isNotEmpty == true) {
          imageCover = Media.fromJSON((jsonMap['media'] as List)
              .where((element) => element['collection_name'] == 'cover')
              .toList()[0]);
        }
      } catch (e) {
        // prettyLog.e(e);
        imageCover = new Media();
      }
      try {
        has_friend_request_from =
            (jsonMap['has_friend_request_from']?.toString() ?? 'false').toBool;
      } catch (e) {
        prettyLog.e(e);
        has_friend_request_from = false;
      }
      try {
        has_sent_friend_request_to =
            (jsonMap['has_sent_friend_request_to']?.toString() ?? 'false')
                .toBool;
      } catch (e) {
        has_sent_friend_request_to = false;
        prettyLog.e(e);
      }
      try {
        market = jsonMap['market'] != null
            ? Market.fromJSON(jsonMap['market'])
            : new Market();
      } catch (e) {
        market = new Market();
        prettyLog.e(e);
      }
      try {
        shop = jsonMap['shop'] != null
            ? Shop.fromJson(jsonMap['shop'])
            : new Shop();
      } catch (e) {
        shop = new Shop();
        prettyLog.e(e);
      }
    } catch (e) {
      prettyLog.e(e);
    }
  }

  //to save local
  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["email"] = email;
    map["name"] = name;
    map["expired_at"] = expiredAt;
    if (password != null) {
      map["password"] = password;
    }
    map["api_token"] = apiToken;
    if (deviceToken != null) {
      map["device_token"] = deviceToken;
    }
    if (phone != null) {
      map["phone"] = phone;
    }
    map["verifiedPhone"] = verifiedPhone;
    if (address != null) {
      map["address"] = address;
    }
    if (bio != null) {
      map["bio"] = bio;
    }
    //todo need to save media
    // map["media"] = image?.toMap();
    // map["media_cover"] = imageCover?.toMap();
    // map["isFriend"] = isFriend;
    // map["gender"] = gender;
    map["age"] = age;
    map["code"] = code;
    if (totalPoint != null) {
      map["total_point"] = totalPoint;
    }
    if (currentPoint != null) {
      map["current_point"] = currentPoint;
    }
    map["has_friend_request_from"] = has_friend_request_from;
    map["has_sent_friend_request_to"] = has_sent_friend_request_to;
    if (fieldId != null) {
      map["fieldId"] = fieldId;
    }
    map['game_can_notification'] = gameCanNotification;
    return map;
  }

  //create a body request for api
  Map<String, String> toRequest() {
    return {
      if (email != null) 'email': email,
      if (name != null) 'name': name,
      if (phone != null) 'phone': phone,
      if (bio != null) 'bio': bio,
      if (age != null) 'age': age,
    };
  }

  //create a body request for update user api
  Map<String, String> toUpdateUserModel() {
    return {
      if (email != null) 'email': email,
      if (name != null) 'name': name,
      if (phone != null) 'phone': phone,
      if (bio != null) 'bio': bio,
      if (age != null) 'age': age,
      if (address != null) 'address': address,
      if (fieldId != null) 'fieldId': fieldId
    };
  }

  Map toRestrictMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["email"] = email;
    map["name"] = name;
    map["thumb"] = image?.thumb;
    map["device_token"] = deviceToken;
    map["market_id"] = market.id;
    return map;
  }

  // to save local DB
  Map<String, dynamic> toLocalDBMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["email"] = email;
    map["name"] = name;
    if (deviceToken != null) {
      map["device_token"] = deviceToken;
    }
    if (phone != null) {
      map["phone"] = phone;
    }
    map["verifiedPhone"] = verifiedPhone;
    if (address != null) {
      map["address"] = address;
    }
    if (bio != null) {
      map["bio"] = bio;
    }
    map["media"] = image?.toMap();
    map["age"] = age;
    map["code"] = code;
    return map;
  }

  @override
  String toString() {
    var map = this.toMap();
    map["auth"] = this.auth;
    return map.toString();
  }

  bool profileCompleted() {
    return address != null &&
        address != '' &&
        phone != null &&
        phone != '' &&
        verifiedPhone != null &&
        verifiedPhone;
  }

  ImageProvider getAvatarImage() {
    if (image.icon.contains("http")) {
      return NetworkImage(image.icon);
    } else {
      return FileImage(File(image.url));
    }
  }

  ImageProvider getCoverImage() {
    if (image.url.contains("http")) {
      return NetworkImage(imageCover.thumb);
    } else {
      return FileImage(File(imageCover.url));
    }
  }

  bool isMe() {
    return currentUser.value?.id == id;
  }

  User copyWith({
    String id,
    String name,
    String email,
    String password,
    String apiToken,
    String deviceToken,
    String phone,
    bool verifiedPhone,
    String verificationId,
    String address,
    String bio,
    Media image,
    Media imageCover,
    bool isFriend,
    String gender,
    String age,
    String code,
    bool has_friend_request_from,
    bool has_sent_friend_request_to,
    Market market,
    Shop shop,
    Field field,
    String fieldId,
    bool isBlocking,
    bool isBlocker,
    int totalPoint,
    int currentPoint,
    bool isLiveStreaming,
    bool gameCanNotification,
    bool auth,
    String expiredAt,
    bool isActive,
    bool canCreateShop,
    String last_updated,
  }) {
    return User.name(
      id: id ?? this.id,
      name: name ?? this.name,
      email: email ?? this.email,
      password: password ?? this.password,
      apiToken: apiToken ?? this.apiToken,
      deviceToken: deviceToken ?? this.deviceToken,
      phone: phone ?? this.phone,
      verifiedPhone: verifiedPhone ?? this.verifiedPhone,
      verificationId: verificationId ?? this.verificationId,
      address: address ?? this.address,
      bio: bio ?? this.bio,
      image: image ?? this.image,
      imageCover: imageCover ?? this.imageCover,
      isFriend: isFriend ?? this.isFriend,
      gender: gender ?? this.gender,
      age: age ?? this.age,
      code: code ?? this.code,
      has_friend_request_from:
          has_friend_request_from ?? this.has_friend_request_from,
      has_sent_friend_request_to:
          has_sent_friend_request_to ?? this.has_sent_friend_request_to,
      market: market ?? this.market,
      shop: shop ?? this.shop,
      field: field ?? this.field,
      fieldId: fieldId ?? this.fieldId,
      isBlocking: isBlocking ?? this.isBlocking,
      isBlocker: isBlocker ?? this.isBlocker,
      totalPoint: totalPoint ?? this.totalPoint,
      currentPoint: currentPoint ?? this.currentPoint,
      isLiveStreaming: isLiveStreaming ?? this.isLiveStreaming,
      auth: auth ?? this.auth,
      gameCanNotification: gameCanNotification ?? this.gameCanNotification,
      expiredAt: expiredAt ?? this.expiredAt,
      isActive: isActive ?? this.isActive,
      canCreateShop: canCreateShop ?? this.canCreateShop,
      last_updated: last_updated ?? this.last_updated,
    );
  }

  static User fromShortUserEntity(ShortUserEntity shortUserEntity) {
    return User.name(
      id: shortUserEntity.id,
      code: shortUserEntity.code,
      name: shortUserEntity.name,
    );
  }
}

extension UserExtension on User {
  bool get isMe => currentUser.value?.id == id;
}

extension StringExtension on String {
  bool get toBool {
    if (this == 'true' || this == '1') return true;
    return false;
  }
}
