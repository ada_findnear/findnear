import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import 'enums/call_enums.dart';

class HistoryCall {
  int id;
  User caller;
  User receiver;
  int duration;
  String status;
  String callId;
  String createdAt;
  String updatedAt;

  HistoryCall({
    this.id,
    this.caller,
    this.receiver,
    this.duration,
    this.status,
    this.callId,
    this.createdAt,
    this.updatedAt,
  });

  HistoryCall.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    caller = json['caller'] != null ? User.fromJSON(json['caller']) : User();
    receiver = json['receiver'] != null ? User.fromJSON(json['receiver']) : User();
    duration = json['duration'];
    status = json['status'];
    callId = json['call_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    data['caller'] = this.caller;
    data['receiver'] = this.receiver;
    data['duration'] = this.duration;
    data['status'] = this.status;
    data['call_id'] = this.callId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }

  User currentUser = Get.find<LocalDataManager>().currentUser;

  String get callTime {
    if (duration != null && duration > 0) {
      Duration time = Duration(seconds: duration);
      String twoDigits(int n) => n.toString().padLeft(2, "0");
      String twoDigitMinutes = twoDigits(time.inMinutes.remainder(60));
      String twoDigitSeconds = twoDigits(time.inSeconds.remainder(60));
      if (twoDigits(time.inHours) != '00') {
        return "${twoDigits(time.inHours)} ${S.current.hour} $twoDigitMinutes ${S.current.minute}";
      } else if (twoDigitMinutes != '00') {
        return '$twoDigitMinutes ${S.current.minute} $twoDigitSeconds ${S.current.second}';
      } else {
        return '$twoDigitSeconds ${S.current.second}';
      }
    }
    return '';
  }

  DateTime get callDate {
    try {
      DateTime date = DateTime.parse(createdAt);
      return date;
    } catch (e) {
      return null;
    }
  }

  bool get isComingCall {
    return currentUser.id == receiver.id;
  }

  User get targetUser {
    if (isComingCall) {
      return caller;
    } else {
      return receiver;
    }
  }

  HistoryCallStatus get callStatus {
    return callTypeFromString(status);
  }

  String get callStatusToString {
    switch (callStatus) {
      case HistoryCallStatus.missed:
        return S.current.missed_call;
        break;
      case HistoryCallStatus.noAnswer:
        return S.current.missed_call;
        break;
      case HistoryCallStatus.busy:
        return S.current.busy_call;
        break;
      case HistoryCallStatus.noAnswer:
        return S.current.noAnswer;
        break;
      case HistoryCallStatus.completed:
        if (currentUser.id == caller.id) {
          return S.current.outgoingCall;
        } else {
          return S.current.incomingCall;
        }
        break;
      default:
        return S.current.unknown;
    }
  }

  String get icon {
    switch (callStatus) {
      case HistoryCallStatus.missed:
        return AppImages.icMissedCall;
        break;
      case HistoryCallStatus.noAnswer:
        return AppImages.icMissedCall;
        break;
      case HistoryCallStatus.busy:
      case HistoryCallStatus.completed:
        if (currentUser.id == caller.id) {
          return AppImages.icOutGoingCall;
        } else {
          return AppImages.icInComingCall;
        }
        break;
      default:
        return AppImages.icMissedCall;
    }
  }

  Color get color {
    switch (callStatus) {
      case HistoryCallStatus.missed:
      case HistoryCallStatus.noAnswer:
      case HistoryCallStatus.busy:
        return Colors.red;
        break;
      case HistoryCallStatus.completed:
        if (currentUser.id == caller.id) {
          return Colors.black;
        } else {
          return Colors.green;
        }
        break;
      default:
        return Colors.red;
    }
  }
}
