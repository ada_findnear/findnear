import '../helpers/custom_trace.dart';
import '../models/media.dart';
import '../models/market.dart';
import 'product.dart';

class BusinessItem {
  int id;
  int parent_id;
  String title;
  String description;
  String status;
  String created_at;
  String updated_at;

  BusinessItem();

  BusinessItem.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'];
      parent_id = jsonMap['parent_id'];
      title = jsonMap['title'].toString();
      description = jsonMap['description'].toString();
      status = jsonMap['status'].toString();
      created_at = jsonMap['created_at'].toString();
      created_at = jsonMap['updated_at'].toString();
    } catch (e) {}
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["parent_id"] = parent_id;
    map["title"] = title;
    map["description"] = description;
    map["status"] = status;
    map["created_at"] = created_at;
    map["updated_at"] = updated_at;
    return map;
  }

  @override
  bool operator ==(dynamic other) {
    return other.id == this.id;
  }

  @override
  int get hashCode => this.id.hashCode;
}
