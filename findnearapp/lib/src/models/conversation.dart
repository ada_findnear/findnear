import 'dart:convert';

import 'package:findnear/src/utils/date_utils.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:global_configuration/global_configuration.dart';

import '../repository/user_repository.dart';

import 'entities/communication/entities/group_config_entity.dart';
import 'entities/communication/entities/short_user_entity.dart';
import 'entities/message_chat/entities/message_chat.dart';
import 'user.dart';

class Conversation {
  String id;

  // conversation name for example chat with market name
  String name;

  // Id user admin
  String idAdmin;

  String groupAvatar;

  // Chats messages
  String lastMessage;

  int lastMessageTime;

  // Ids of users that read the chat message
  List<String> readByUsers;

  // Ids of users that muted this conversation
  List<String> mutedUsers;

  // Ids of users in this conversation
  List<String> visibleToUsers;

  // users in the conversation
  List<User> users;

  // uuid
  String uuid;

  // originId
  String originId;

  // type
  String type;

  // config
  GroupConfigEntity config;

  // ghim hội thoại
  bool pinTop = false;

  // danh sách id của member
  List<String> userIds;

  // mã PIN của hội thoại
  String pin;

  // thời gian tạo cuộc trò chuyện
  String createDate;

  // Ẩn/ hiện cuộc hội thoại
  bool isDisplay;

  // người tạo
  String createBy;

  // số tin nhắn chưa đọc
  int totalMessageUnRead;

  // last messages
  MessageChat lastMessageEntity;

  String get beautyCreateDate => realUpdateTime != null
      ? DateUtils.getVerboseDateTimeRepresentation(realUpdateTime)
      : "";

  bool get isHadLastMessageTime => lastMessageEntity != null && lastMessageEntity.createDate != null;

  DateTime get realUpdateTime => !isHadLastMessageTime ? DateUtils.changeIso8601StringToDatetime(createDate) : lastMessageEntity.createDate;

  int get createDateInMilis => realUpdateTime.millisecondsSinceEpoch;//DateUtils.timeInMilisFromDateTime(createDate);

  Conversation.copy({
    this.id,
    this.name,
    this.idAdmin,
    this.groupAvatar,
    this.lastMessage,
    this.lastMessageTime,
    this.readByUsers,
    this.mutedUsers,
    this.visibleToUsers,
    this.users,
    this.uuid,
    this.originId,
    this.type,
    this.config,
    this.pinTop,
    this.userIds,
    this.pin,
    this.createDate,
    this.isDisplay,
    this.createBy,
    this.totalMessageUnRead,
    this.lastMessageEntity
  });

  Conversation(this.users, {this.id = null, this.name = ''}) {
    visibleToUsers = this.users.map((user) => user.id).toList();
    readByUsers = [];
  }

  Conversation.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'] != null ? jsonMap['id'].toString() : null;
      name = jsonMap['name'] != null ? jsonMap['name'].toString() : '';
      idAdmin =
          jsonMap['id_admin'] != null ? jsonMap['id_admin'].toString() : null;
      groupAvatar = jsonMap['group_avatar'] != null
          ? jsonMap['group_avatar'].toString()
          : null;
      readByUsers = jsonMap['read_by_users'] != null
          ? List.from(jsonMap['read_by_users'])
          : [];
      mutedUsers = jsonMap['muted_users'] != null
          ? List.from(jsonMap['muted_users'])
          : [];
      visibleToUsers = jsonMap['visible_to_users'] != null
          ? List.from(jsonMap['visible_to_users'])
          : [];
      lastMessage =
          jsonMap['message'] != null ? jsonMap['message'].toString() : '';
      lastMessageTime = jsonMap['time'] != null ? jsonMap['time'] : 0;
      users = jsonMap['users'] != null
          ? List.from(jsonMap['users']).map((element) {
              element['media'] = [
                {'thumb': element['thumb'], 'collection_name': 'avatar'}
              ];
              element['market'] = {'id': element['market_id']};
              return User.fromJSON(Map<String, dynamic>.from(element));
            }).toList()
          : [];
      if (idAdmin == null && users.isNotEmpty) {
        name = users
            .firstWhere((element) => element.id != currentUser.value?.id)
            .name;
      }
      uuid = jsonMap['uuid'] != null ? jsonMap['uuid'].toString() : null;
      originId = jsonMap['originId'] != null ? jsonMap['originId'].toString() : null;
      config = jsonMap['config'] != null
          ? GroupConfigEntity.fromJson(
              Map<String, dynamic>.from(jsonMap['config']))
          : GroupConfigEntity(
              groupAvatar:
                  "${GlobalConfiguration().getValue('base_url')}images/image_default.png}",
              shortUsers: []);
      userIds =
          jsonMap['user_ids'] != null ? List.from(jsonMap['user_ids']) : [];
      type = jsonMap['type'] != null ? jsonMap['type'].toString() : null;
      pinTop = jsonMap['pinTop'];
      pin = jsonMap['pin'] != null ? jsonMap['pin'].toString() : null;
      createDate = jsonMap['createDate'] != null
          ? jsonMap['createDate'].toString()
          : null;
      isDisplay = jsonMap['isDisplay'];
      createBy =
          jsonMap['createBy'] != null ? jsonMap['createBy'].toString() : null;
      totalMessageUnRead = jsonMap['totalMessageUnRead'] ?? 0;
      lastMessageEntity = jsonMap['lastMessage'] != null
          ? MessageChat.fromJson(Map<String, dynamic>.from(jsonMap['lastMessage']))
          : MessageChat();
    } catch (e) {
      id = '';
      name = '';
      idAdmin = '';
      groupAvatar = '';
      readByUsers = [];
      users = [];
      lastMessage = '';
      lastMessageTime = 0;
      config = GroupConfigEntity(
          groupAvatar:
              "${GlobalConfiguration().getValue('base_url')}images/image_default.png}");
      userIds = [];
      type = '';
      pinTop = false;
      pin = '';
      createDate = '';
      isDisplay = true;
      createBy = '';
      totalMessageUnRead = 0;
      lastMessageEntity = MessageChat();
    }
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["name"] = name;
    map["id_admin"] = idAdmin;
    map["group_avatar"] = groupAvatar;
    map["users"] =
        users.map((element) => element.toRestrictMap()).toSet().toList();
    map["visible_to_users"] =
        users.map((element) => element.id).toSet().toList();
    map["read_by_users"] = readByUsers;
    map["message"] = lastMessage;
    map["time"] = lastMessageTime;
    map["config"] = config.toJson();
    map["uuid"] = uuid;
    map["originId"] = originId;
    map["user_ids"] = userIds;
    map["type"] = type;
    map["pinTop"] = pinTop;
    map["pin"] = pin;
    map["isDisplay"] = isDisplay;
    map["createDate"] = createDate;
    map["createBy"] = createBy;
    map["totalMessageUnRead"] = totalMessageUnRead;
    if(lastMessageEntity.uuid != null)
      map["lastMessage"] = lastMessageEntity.toJson();
    return map;
  }

  Map toUpdatedMap() {
    var map = new Map<String, dynamic>();
    map["name"] = name;
    map["group_avatar"] = groupAvatar;
    map["message"] = lastMessage;
    map["time"] = lastMessageTime;
    map["read_by_users"] = readByUsers;

    final usersMap = users
        .map(
          (element) => element.toRestrictMap(),
        )
        .toSet()
        .toList();
    // avoid clear users in Firebase when update
    if (usersMap.isNotEmpty) map["users"] = usersMap;

    //
    visibleToUsers.sort((a, b) => a.compareTo(b));
    map["visible_to_users"] = visibleToUsers;
    map["id_admin"] = idAdmin;
    return map;
  }

  Map toUsersMap() {
    var map = new Map<String, dynamic>();

    final usersMap = users
        .map(
          (element) => element.toRestrictMap(),
        )
        .toSet()
        .toList();
    // avoid clear users in Firebase when update
    if (usersMap.isNotEmpty) map["users"] = usersMap;

    return map;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    try {
      data["_id"] = id;
      data["uuid"] = uuid;
      data["name"] = name;
      data["createBy"] = createBy;
      data["type"] = type;
      data["isDisplay"] = isDisplay;
      data["config"] = config?.toJson();
      data["pin"] = pin;
      data["originId"] = originId;
      data["createDate"] = createDate;

      if (users.isNotEmpty) {
        data["user"] = users.map((e) => e.code);
      }
    } catch (e) {
      logger.e("Conversation - toJson - error: $e");
    }
    return data;
  }

  DateTime get lastMessageDateTime {
    return DateTime.fromMillisecondsSinceEpoch(lastMessageTime, isUtc: true);
  }

  Conversation copyWith({
    String id,
    String name,
    String idAdmin,
    String groupAvatar,
    String lastMessage,
    int lastMessageTime,
    List<String> readByUsers,
    List<String> mutedUsers,
    List<String> visibleToUsers,
    List<User> users,
    String uuid,
    String originId,
    List<String> userIds,
    String type,
    String pin,
    String createDate,
    bool isDisplay,
    String createBy,
    int totalMessageUnRead,
    MessageChat lastMessageEntity,
    GroupConfigEntity config,
  }) {
    return new Conversation.copy(
      id: id ?? this.id,
      name: name ?? this.name,
      idAdmin: idAdmin ?? this.idAdmin,
      groupAvatar: groupAvatar ?? this.groupAvatar,
      lastMessage: lastMessage ?? this.lastMessage,
      lastMessageTime: lastMessageTime ?? this.lastMessageTime,
      readByUsers: readByUsers ?? this.readByUsers,
      mutedUsers: mutedUsers ?? this.mutedUsers,
      visibleToUsers: visibleToUsers ?? this.visibleToUsers,
      users: users ?? this.users,
      pinTop: pinTop ?? this.pinTop,
      uuid: uuid ?? this.uuid,
      originId: originId ?? this.originId,
      userIds: userIds ?? this.userIds,
      type: type ?? this.type,
      pin: pin ?? this.pin,
      createDate: createDate ?? this.createDate,
      isDisplay: isDisplay ?? this.isDisplay,
      createBy: createBy ?? this.createBy,
      totalMessageUnRead: totalMessageUnRead ?? this.totalMessageUnRead,
      lastMessageEntity: lastMessageEntity ?? this.lastMessageEntity,
      config: config ?? this.config,
    );
  }
}
