import 'package:findnear/src/models/ward.dart';

import '../helpers/custom_trace.dart';

class District {
  int idx;
  int province_id;
  String vnname;
  String vnprefix;
  List<Ward> wards;

  int indexUI;
  District();

  District.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      idx = jsonMap['idx'];
      province_id = jsonMap['province_id'];
      vnname = jsonMap['vnname'] != null ? jsonMap['vnname'].toString() : '';
      vnprefix = jsonMap['vnprefix'] != null ? jsonMap['vnprefix'].toString() : '';
      wards =  (jsonMap['wards'] as List<dynamic>)
          .map((e) => Ward.fromJSON(e as Map<String, dynamic>))
          .toList();
    } catch (e) {
      print(CustomTrace(StackTrace.current, message: e));
    }
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["idx"] = idx;
    map["province_id"] = province_id;
    map["vnname"] = vnname;
    map["vnprefix"] = vnprefix;
    map["wards"] = wards;
    return map;
  }

  @override
  bool operator ==(dynamic other) {
    return other.idx == this.idx;
  }

  @override
  int get hashCode => this.idx.hashCode;

  void createIndex(int indexUI) {
    this.indexUI = indexUI;
    int index = 0;
    for (final ward in wards){
      ward.createIndex(index);
      index++;
    }
  }
}
