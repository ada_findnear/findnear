import 'dart:convert';
import 'package:findnear/src/utils/logger.dart';
import '../models/media.dart';

class SearchUserEntity {
  String id;
  String name;
  List<Media> media;
  double distance;
  String code;

  SearchUserEntity();

  SearchUserEntity.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      name = jsonMap['name'];
      code = jsonMap['code'];
      distance = jsonMap['distance'] != null ? double.parse(jsonMap['distance'].toString()) : 0.0;
      media = jsonMap['media'] != null
          ? List.from(jsonMap['media'])
          .map((element) => () {
        var media = Media.fromJSON(element);
        return media;
      }())
          .toList()
          : [];
    } catch (e) {
      id = '';
      name = '';
      code = '';
      distance = 0.0;
      media = [];
      prettyLog.e(e);
    }
  }

  Map<String, dynamic> toMap() {
    var medias = "";
    if (this.media?.isNotEmpty == true)
      medias =
          jsonDecode(this.media?.map((e) => e?.toMap())?.toList()?.toString());
    return {
      'id': id,
      'name': name,
      'distance': distance,
      'medias': medias,
      'code': code,
    };
  }
}
