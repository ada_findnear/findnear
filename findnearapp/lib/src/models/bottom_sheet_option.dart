import 'package:flutter/material.dart';

class BottomSheetOption {
  final String iconPath;
  final IconData icon;
  final String text;
  final bool isReported;

  const BottomSheetOption({this.iconPath = "", this.icon, this.text = "", this.isReported = false});
}
