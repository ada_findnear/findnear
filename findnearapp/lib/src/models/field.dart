import '../models/media.dart';

class Field {
  String id;
  String name;
  String description;
  Media image;
  bool selected;
  bool showIcon = true;
  int indexUI;
  int isLocked;

  Field({this.id, this.name, this.description, this.image, this.selected, this.indexUI});

  Field.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      name = jsonMap['name'];
      description = jsonMap['description'];
      image = jsonMap['media'] != null && (jsonMap['media'] as List).length > 0 ? Media.fromJSON(jsonMap['media'][0]) : new Media();
      selected = jsonMap['selected'] ?? false;
      showIcon = jsonMap['showIcon'] ?? true;
      isLocked = jsonMap['is_locked'] ?? 0;
    } catch (e) {
      id = '';
      name = '';
      description = '';
      image = new Media();
      selected = false;
      showIcon = true;
      isLocked = 0;
      print(e);
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map['id'] = id;
    map['name'] = name;
    return map;
  }

  @override
  bool operator ==(dynamic other) {
    return other.id == this.id;
  }

  @override
  int get hashCode => super.hashCode;

  void createIndex(int index) {
    this.indexUI = index;
  }
}
