import 'package:flutter/material.dart';
import 'dart:io';
import 'media.dart';

class Comment {
  String id;
  String post_id;
  String content;
  String createdAt;
  String creatorName;
  String creatorId;
  Media image;
  Media avatar;

  Comment();

  Comment.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'] != '' ? jsonMap['id'].toString() : '';
      content = jsonMap['content'] != null ? jsonMap['content'].toString() : '';
      post_id = jsonMap['post_id'] != '' ? jsonMap['post_id'].toString() : '';
      try {
        createdAt = jsonMap['created_at'].toString();
      } catch (e) {
        createdAt = '';
      }
      try {
        creatorName = jsonMap['creator']['name'] != null ? jsonMap['creator']['name'].toString() : null;
        creatorId = jsonMap['creator']['id'] != null ? jsonMap['creator']['id'].toString() : null;
      } catch (e) {
        creatorName = '';
      }
      try {
        image = Media.fromJSON((jsonMap['media'] as List).where((element) => element['collection_name'] == 'image_post_comment').toList()[0]);
      } catch (e) {
        image = new Media();
      }

      try {
        avatar = Media.fromJSON((jsonMap['creator']['media'] as List).where((element) => element['collection_name'] == 'avatar').toList()[0]);
      } catch (e) {
        avatar = new Media();
      }
    } catch (e) {
      print(e);
    }
  }

  Map toMap({withoutMedia = false}) {
    var map = withoutMedia ? new Map<String, String>() : new Map<String, dynamic>();
    map["id"] = id;
    map["post_id"] = post_id;
    map["content"] = content;
    map["createdAt"] = createdAt;
    map["creatorName"] = creatorName;
    map["creatorId"] = creatorId ?? "";
    if (!withoutMedia) {
      map["media"] = image?.toMap();
    }
    return map;
  }

  ImageProvider getAvatarImage() {
    if (avatar.icon.contains("http")) {
      return NetworkImage(avatar.url);
    } else {
      return FileImage(File(avatar.url));
    }
  }
}
