import 'package:findnear/src/models/media.dart';

class RecommendType {
  int id;
  String name;
  Media image;
  RecommendType();
  RecommendType.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'] ?? 0;
      name = jsonMap['name'] ?? 0;
      image = jsonMap['image'] ?? '';
    } catch (e) {
      print(e);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['media'] = this.image;
    return data;
  }
}
