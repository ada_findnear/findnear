import 'package:global_configuration/global_configuration.dart';
import 'package:images_picker/images_picker.dart' as mediaPicker;

class Media {
  String id;
  String name;
  String fileName;
  String url;
  String thumb;
  String thumbId;
  String icon;
  int size;
  String collection;
  String mime_type;
  String formated_size;

  bool get isImagePostCollection {
    return collection == "image_post";
  }

  bool get isThumbnailPostCollection {
    return collection == "thumbnail_post";
  }

  Media({this.thumb, this.url}) {
    url ??=
        "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
    thumb ??=
        "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
    icon ??=
        "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
  }

  Media.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      if (jsonMap != null) {
        id = jsonMap['id'].toString();
        name = jsonMap['name'];
        fileName = jsonMap['file_name'];
        url = jsonMap['url'];
        thumb = jsonMap['thumb'];
        icon = jsonMap['icon'];
        size = jsonMap['size'];
        collection = jsonMap['collection_name'];
        mime_type = jsonMap['mime_type'];
        formated_size = jsonMap['formated_size'];
      }
    } catch (e) {
      url =
          "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
      thumb =
          "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
      icon =
          "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
      print(e);
    }
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["name"] = name;
    map["file_name"] = fileName;
    map["url"] = url;
    map["thumb"] = thumb;
    map["icon"] = icon;
    map["size"] = size;
    map["collection_name"] = collection;
    map["mime_type"] = mime_type;
    map["formated_size"] = formated_size;
    return map;
  }

  mediaPicker.Media toMediaPicker() {
    return mediaPicker.Media(
      path: url,
      thumbPath: thumb,
      size: size.toDouble()
    );
  }

  @override
  bool operator ==(dynamic other) {
    return other.url == this.url;
  }

  @override
  int get hashCode => this.url.hashCode;

  bool get isVideo => this.mime_type.indexOf("video") != -1;

  bool get isPhoto => this.mime_type.indexOf("image") != -1;

  @override
  String toString() {
    return this.toMap().toString();
  }
}
