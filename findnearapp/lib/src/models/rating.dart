import 'package:findnear/src/models/media.dart';
import 'package:findnear/src/models/user.dart';

class Rating {
  int id;
  int rating;
  String comment;
  User user;
  List<Media> images;
  Rating();
  Rating.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'] ?? 0;
      rating = jsonMap['rating'] != null
          ? int.parse(jsonMap['rating'].toString())
          : 0;
      comment = jsonMap['comment'] ?? '';
      images = jsonMap['media'] != null && (jsonMap['media'] as List).length > 0
          ? List.from(jsonMap['media'])
              .map((element) => Media.fromJSON(element))
              .toSet()
              .toList()
          : [];
      user = jsonMap['user'] != null ? User.fromJSON(jsonMap['user']) : null;
    } catch (e) {
      print(e);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['rating'] = this.rating;
    data['user'] = this.user;
    data['media'] = this.images;
    return data;
  }
}
