import 'package:findnear/src/utils/logger.dart';

import '../helpers/custom_trace.dart';
import '../models/media.dart';
import '../models/market.dart';
import 'BusinessItem.dart';
import 'product.dart';

class Business {
  String success;
  List<BusinessItem> data = [];

  Business();

  Business.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      success = jsonMap['success'].toString();
      data =    (jsonMap['data'] as List<dynamic>)
          .map((e) => BusinessItem.fromJSON(e as Map<String, dynamic>))
          .toList();

    } catch (e) {
      logger.log("✅ fsdf" + e.toString());
    }
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["success"] = success;
    return map;
  }

  @override
  bool operator ==(dynamic other) {
    return other.data == this.data;
  }

  @override
  int get hashCode => this.data.hashCode;
}
