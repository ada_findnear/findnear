import 'package:findnear/src/models/shop.dart';
import 'package:findnear/src/utils/logger.dart';

import '../helpers/custom_trace.dart';
import '../models/media.dart';
import '../models/market.dart';
import 'product.dart';

class Slide {
  String id;
  String refId; //ID of the shop which shown in this banner
  String refType;
  String imageUrl;
  bool isMonopoly;
  Slide();

  Slide.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      refType = jsonMap['ref_type'].toString();
      imageUrl = jsonMap['img_url'].toString();
      refId = jsonMap['ref_id'].toString();
      refId = jsonMap['ref_id'].toString();
      isMonopoly = jsonMap['is_monopoly'] != null
          ? jsonMap['is_monopoly'] == 0
              ? false
              : true
          : false;
    } catch (e) {
      id = '';
      refType = '';
      imageUrl = '';
      refId = '';
      isMonopoly = false;
      print(CustomTrace(StackTrace.current, message: e));
    }
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["ref_id"] = refId;
    map["ref_type"] = refType;
    map["img_url"] = imageUrl;
    map['is_monopoly'] = isMonopoly;
    return map;
  }

  @override
  bool operator ==(dynamic other) {
    return other.id == this.id;
  }

  @override
  int get hashCode => this.id.hashCode;
}
