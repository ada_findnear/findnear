import '../models/field.dart';

class Filter {
  List<Field> fields;

  Filter();

  Filter.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      fields = jsonMap['fields'] != null && (jsonMap['fields'] as List).length > 0
          ? List.from(jsonMap['fields']).map((element) => Field.fromJSON(element)).toList()
          : [];
    } catch (e) {
      print(e);
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map['fields'] = fields.map((element) => element.toMap()).toList();
    return map;
  }

  Map<String, dynamic> toQuery({Map<String, dynamic> oldQuery}) {
    final filters = (fields ?? []).where((element) => element.name != null).toList();
    Map<String, dynamic> query = {};
    String relation = '';
    if (oldQuery != null) {
      relation = oldQuery['with'] != null ? oldQuery['with'] + '.' : '';
      query['with'] = oldQuery['with'] != null ? oldQuery['with'] : null;
    }
    if (filters != null && filters.isNotEmpty) {
      query['fields[]'] = filters.map((element) => element.id).toList();
    }
    if (oldQuery != null) {
      if (query['search'] != null) {
        query['search'] += ';' + oldQuery['search'];
      } else {
        query['search'] = oldQuery['search'];
      }

      if (query['searchFields'] != null) {
        query['searchFields'] += ';' + oldQuery['searchFields'];
      } else {
        query['searchFields'] = oldQuery['searchFields'];
      }

//      query['search'] =
//          oldQuery['search'] != null ? (query['search']) ?? '' + ';' + oldQuery['search'] : query['search'];
//      query['searchFields'] = oldQuery['searchFields'] != null
//          ? query['searchFields'] ?? '' + ';' + oldQuery['searchFields']
//          : query['searchFields'];
    }
    query['searchJoin'] = 'and';
    return query;
  }
}
