import 'package:findnear/src/models/category.dart';

import '../media.dart';

class ProductsResponse {
  Data data;
  String message;
  bool success;

  ProductsResponse({this.data, this.message, this.success});

  factory ProductsResponse.fromJson(Map<String, dynamic> json) {
    return ProductsResponse(
      data: json['data'] != null ? Data.fromJson(json['data']) : null,
      message: json['message'],
      success: json['success'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<Product> data;
  int current_page;
  String first_page_url;
  int from;
  int last_page;
  String last_page_url;
  Object next_page_url;
  String path;
  String per_page;
  Object prev_page_url;
  int to;
  int total;

  Data(
      {this.data,
      this.current_page,
      this.first_page_url,
      this.from,
      this.last_page,
      this.last_page_url,
      this.next_page_url,
      this.path,
      this.per_page,
      this.prev_page_url,
      this.to,
      this.total});

  factory Data.fromJson(Map<String, dynamic> json) {
    return Data(
      data: json['data'] != null
          ? (json['data'] as List).map((i) => Product.fromJson(i)).toList()
          : null,
      current_page: json['current_page'],
      first_page_url: json['first_page_url'],
      from: json['from'],
      last_page: json['last_page'],
      last_page_url: json['last_page_url'],
      //next_page_url: json['next_page_url'] != null ? Object.fromJson(json['next_page_url']) : null,
      path: json['path'],
      per_page: json['per_page'],
      // prev_page_url: json['prev_page_url'] != null ? Object.fromJson(json['prev_page_url']) : null,
      to: json['to'],
      total: json['total'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['current_page'] = this.current_page;
    data['first_page_url'] = this.first_page_url;
    data['from'] = this.from;
    data['last_page'] = this.last_page;
    data['last_page_url'] = this.last_page_url;
    data['path'] = this.path;
    data['per_page'] = this.per_page;
    data['to'] = this.to;
    data['total'] = this.total;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    // if (this.next_page_url != null) {
    //     data['next_page_url'] = this.next_page_url.toJson();
    // }
    // if (this.prev_page_url != null) {
    //     data['prev_page_url'] = this.prev_page_url.toJson();
    // }
    return data;
  }
}

class Product {
  List<Category> categories;
  String created_at;
  List<Object> custom_fields;
  String description;
  bool hasMedia;
  int id;
  List<Media> media;
  String name;
  double price;
  int shop_id;
  String status;
  String updated_at;
  int userId;

  Product(
      {this.categories,
      this.created_at,
      this.custom_fields,
      this.description,
      this.hasMedia,
      this.id,
      this.media,
      this.name,
      this.price,
      this.shop_id,
      this.status,
      this.updated_at,
      this.userId});

  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
      categories: json['categories'] != null
          ? (json['categories'] as List)
              .map((i) => Category.fromJSON(i))
              .toList()
          : null,
      created_at: json['created_at'],
      //  custom_fields: json['custom_fields'] != null ? (json['custom_fields'] as List).map((i) => Object.fr(i)).toList() : null,
      description: json['description'],
      hasMedia: json['has_media'],
      id: json['id'],
      media: json['media'] != null
          ? (json['media'] as List).map((i) => Media.fromJSON(i)).toList()
          : null,
      name: json['name'],
      price: double.parse(json['price'].toString()),
      shop_id: json['shop_id'],
      status: json['status'],
      updated_at: json['updated_at'],
      userId: json['user_id'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.created_at;
    data['description'] = this.description;
    data['has_media'] = this.hasMedia;
    data['id'] = this.id;
    data['name'] = this.name;
    data['price'] = this.price;
    data['shop_id'] = this.shop_id;
    data['status'] = this.status;
    data['updated_at'] = this.updated_at;
    data['user_id'] = this.userId;
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    // if (this.custom_fields != null) {
    //     data['custom_fields'] = this.custom_fields.map((v) => v.toJson()).toList();
    // }
    if (this.media != null) {
      data['media'] = this.media.map((v) => v.toMap()).toList();
    }
    return data;
  }

  String get firstThumb => media?.isNotEmpty == true ? media.first.thumb : "";
  String get firstImage => media?.isNotEmpty == true ? media.first.url : "";
}
