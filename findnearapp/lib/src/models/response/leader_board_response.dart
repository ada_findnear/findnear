import '../user.dart';

///Sample response
///        {
//             success: true,
//             data: [
//                 {
//                     id: 800,
//                     user_id: 35903,
//                     game_id: 2,
//                     phase: 4,
//                     point: -100,
//                     created_at: "2021-09-16 13:29:57",
//                     updated_at: "2021-09-16 14:00:01",
//                     data: "[57, 2, 38]",
//                     won: true,
//                     win_point: 500,
//                     won_number: 38,
//                     custom_fields: [ ],
//                     user: {
//                         id: 35903,
//                         name: "Customer",
//                         email: null,
//                         code: "20202020",
//                         is_nice: 1,
//                         password_default: "cUs3Qs",
//                         owner_id: 1,
//                         active: true,
//                         birth_date: null,
//                         api_token: "hkSQXofifdkhxf90AQ3g57scEDzI6URLOvMmB6RC42gqkQnxXAMmamNqgu6e",
//                         device_token: "c9q7Kiy_wkupo0C_bzNv04:APA91bHJtgUkRTrQ1GXEXm9Ptb3V1kVUfOI9hmSxNPHOlshTZ8oTIDFJRnkNGBUUKOyn_gu5FVfGxTpGv1lspsYkBLP_ozluUquVN_CkvO8W9vMq_1OUSq5uwiqUBGnGcB5VHrViws7p",
//                         stripe_id: null,
//                         card_brand: null,
//                         card_last_four: null,
//                         trial_ends_at: null,
//                         braintree_id: null,
//                         paypal_email: null,
//                         user_detail_id: 60,
//                         auth_code: "151192",
//                         expired_at: "2022-09-09 13:37:02",
//                         created_at: "2021-09-09 20:31:22",
//                         updated_at: "2021-09-16 15:35:54",
//                         total_point: 200,
//                         current_point: 100,
//                         spent_point: -100,
//                         game_can_notification: true,
//                         report: null,
//                         locked_at: null,
//                         deleted_at: null,
//                         custom_fields: {
//                             phone: {
//                                 value: null,
//                                 view: null,
//                                 name: "phone"
//                             },
//                             bio: {
//                                 value: null,
//                                 view: "",
//                                 name: "bio"
//                             },
//                             address: {
//                                 value: null,
//                                 view: null,
//                                 name: "address"
//                             },
//                             verifiedPhone: {
//                                 value: null,
//                                 view: null,
//                                 name: "verifiedPhone"
//                             }
//                         },
//                         has_media: true,
//                         is_friend: false,
//                         age: 0,
//                         has_friend_request_from: false,
//                         has_sent_friend_request_to: false,
//                         is_online: true,
//                         media: [
//                             {
//                                 id: 2569,
//                                 model_type: "App\Models\User",
//                                 model_id: 35903,
//                                 collection_name: "avatar",
//                                 name: "image_picker_2106F4BF-E403-4192-AD51-EDC3836BACED-2025-000001B53032DC2E",
//                                 file_name: "image_picker_2106F4BF-E403-4192-AD51-EDC3836BACED-2025-000001B53032DC2E.jpg",
//                                 mime_type: "image/jpeg",
//                                 disk: "public",
//                                 size: 69938,
//                                 manipulations: [ ],
//                                 custom_properties: {
//                                     generated_conversions: {
//                                         thumb: true,
//                                         icon: true
//                                     }
//                                 },
//                                 responsive_images: [ ],
//                                 order_column: 2412,
//                                 created_at: "2021-09-09 13:37:23",
//                                 updated_at: "2021-09-09 13:37:23",
//                                 url: "https://app.findnear.vn/storage/app/public/2569/image_picker_2106F4BF-E403-4192-AD51-EDC3836BACED-2025-000001B53032DC2E.jpg",
//                                 thumb: "https://app.findnear.vn/storage/app/public/2569/conversions/image_picker_2106F4BF-E403-4192-AD51-EDC3836BACED-2025-000001B53032DC2E-thumb.jpg",
//                                 icon: "https://app.findnear.vn/storage/app/public/2569/conversions/image_picker_2106F4BF-E403-4192-AD51-EDC3836BACED-2025-000001B53032DC2E-icon.jpg",
//                                 formated_size: "68.3 KB"
//                             },
//                             {
//                                 id: 2570,
//                                 model_type: "App\Models\User",
//                                 model_id: 35903,
//                                 collection_name: "cover",
//                                 name: "image_picker_A3561F29-2F1A-4DE7-AD51-5CF00BE5FEE7-2025-000001B543610965",
//                                 file_name: "image_picker_A3561F29-2F1A-4DE7-AD51-5CF00BE5FEE7-2025-000001B543610965.jpg",
//                                 mime_type: "image/jpeg",
//                                 disk: "public",
//                                 size: 105197,
//                                 manipulations: [ ],
//                                 custom_properties: {
//                                     generated_conversions: {
//                                         thumb: true,
//                                         icon: true
//                                     }
//                                 },
//                                 responsive_images: [ ],
//                                 order_column: 2413,
//                                 created_at: "2021-09-09 13:37:36",
//                                 updated_at: "2021-09-09 13:37:36",
//                                 url: "https://app.findnear.vn/storage/app/public/2570/image_picker_A3561F29-2F1A-4DE7-AD51-5CF00BE5FEE7-2025-000001B543610965.jpg",
//                                 thumb: "https://app.findnear.vn/storage/app/public/2570/conversions/image_picker_A3561F29-2F1A-4DE7-AD51-5CF00BE5FEE7-2025-000001B543610965-thumb.jpg",
//                                 icon: "https://app.findnear.vn/storage/app/public/2570/conversions/image_picker_A3561F29-2F1A-4DE7-AD51-5CF00BE5FEE7-2025-000001B543610965-icon.jpg",
//                                 formated_size: "102.7 KB"
//                             }
//                         ]
//                     }
//                 }
//             ],
//             message: "Game Histories retrieved successfully"
//         };
class LeaderBoardResponse {
    List<User> data;
    String message;
    bool success;

    LeaderBoardResponse({this.data, this.message, this.success});

    factory LeaderBoardResponse.fromJson(Map<String, dynamic> json) {
        return LeaderBoardResponse(
            data: json['data'] != null ? (json['data'] as List).map((i) => User.fromJSON(i)).toList() : null,
            message: json['message'], 
            success: json['success'], 
        );
    }

    factory LeaderBoardResponse.fromJsonToLotteryLeaderBoard(Map<String, dynamic> json) {
        return LeaderBoardResponse(
            data: json['data'] != null ? (json['data'] as List).map((i) => User.fromJSON((i as Map<String, dynamic>)['user'])).toList() : null,
            message: json['message'],
            success: json['success'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['message'] = this.message;
        data['success'] = this.success;
        if (this.data != null) {
            data['data'] = this.data.map((v) => v.toMap()).toList();
        }
        return data;
    }
}
