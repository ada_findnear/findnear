import 'package:findnear/src/models/rating.dart';
import 'package:findnear/src/models/recommend_type.dart';
import 'package:findnear/src/models/response/base_response.dart';

class RecommendTypeResponse extends BaseResponse<List<RecommendType>> {
  RecommendTypeResponse(
      {List<RecommendType> data, String message, bool success})
      : super(message: message, success: success, data: data);

  factory RecommendTypeResponse.fromJson(Map<String, dynamic> json) {
    final baseResponse = BaseResponse<List<RecommendType>>.fromJson(
      json,
      (data) => data is List
          ? data.map((e) => RecommendType.fromJSON(e)).toList(growable: false)
          : <RecommendType>[],
    );
    return RecommendTypeResponse(
      message: baseResponse.message,
      data: baseResponse.data,
      success: baseResponse.success,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = (this.data.map((e) => e.toJson()).toList());
    }
    return data;
  }
}
