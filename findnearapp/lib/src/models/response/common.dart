class CustomProperties {
  GeneratedConversions generated_conversions;

  CustomProperties({this.generated_conversions});

  factory CustomProperties.fromJson(Map<String, dynamic> json) {
    return CustomProperties(
      generated_conversions: json['generated_conversions'] != null
          ? GeneratedConversions.fromJson(json['generated_conversions'])
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.generated_conversions != null) {
      data['generated_conversions'] = this.generated_conversions.toJson();
    }
    return data;
  }
}

class GeneratedConversions {
  bool icon;
  bool thumb;

  GeneratedConversions({this.icon, this.thumb});

  factory GeneratedConversions.fromJson(Map<String, dynamic> json) {
    return GeneratedConversions(
      icon: json['icon'],
      thumb: json['thumb'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['icon'] = this.icon;
    data['thumb'] = this.thumb;
    return data;
  }
}