import 'package:findnear/src/models/category.dart';
import 'package:findnear/src/models/shop.dart';

class ShopsResponse {
  Data data;
  String message;
  bool success;

  ShopsResponse({this.data, this.message, this.success});

  factory ShopsResponse.fromJson(Map<String, dynamic> json) {
    return ShopsResponse(
      data: json['data'] != null ? Data.fromJson(json['data']) : null,
      message: json['message'],
      success: json['success'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<Shop> data;
  List<Category> categories;
  int current_page;
  String first_page_url;
  int from;
  int last_page;
  String last_page_url;
  Object next_page_url;
  String path;
  int per_page;
  Object prev_page_url;
  int to;
  int total;

  Data(
      {this.data,
      this.current_page,
      this.categories,
      this.first_page_url,
      this.from,
      this.last_page,
      this.last_page_url,
      this.next_page_url,
      this.path,
      this.per_page,
      this.prev_page_url,
      this.to,
      this.total});

  factory Data.fromJson(Map<String, dynamic> json) {
    return Data(
      data: json['data'] != null
          ? (json['data'] as List).map((i) => Shop.fromJson(i)).toList()
          : null,
      categories: json['categories'] != null
          ? (json['categories'] as List)
              .map((i) => Category.fromJSON(i))
              .toList()
          : null,
      current_page: json['current_page'],
      first_page_url: json['first_page_url'],
      from: json['from'],
      last_page: json['last_page'],
      //last_page_url: json['last_page_url'],
      // next_page_url: json['next_page_url'] != null ? Object.fromJson(json['next_page_url']) : null,
      path: json['path'],
      per_page: int.tryParse(json['per_page'].toString()),
      //prev_page_url: json['prev_page_url'] != null ? Object.fromJson(json['prev_page_url']) : null,
      to: json['to'],
      total: json['total'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['current_page'] = this.current_page;
    data['first_page_url'] = this.first_page_url;
    data['from'] = this.from;
    data['last_page'] = this.last_page;
    data['last_page_url'] = this.last_page_url;
    data['path'] = this.path;
    data['per_page'] = this.per_page;
    data['to'] = this.to;
    data['total'] = this.total;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    // if (this.next_page_url != null) {
    //     data['next_page_url'] = this.next_page_url.toJson();
    // }
    // if (this.prev_page_url != null) {
    //     data['prev_page_url'] = this.prev_page_url.toJson();
    // }
    return data;
  }
}
