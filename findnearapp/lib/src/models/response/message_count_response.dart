import '../field.dart';

class MessageCountResponse {
  String message;
  bool success;
  int unReadCount;

  MessageCountResponse({this.unReadCount, this.message, this.success});

  factory MessageCountResponse.fromJson(Map<String, dynamic> json) {
    return MessageCountResponse(
      unReadCount: json['data'] ?? 0,
      message: json['message'],
      success: json['success'],
    );
  }

  MessageCountResponse.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      message = jsonMap['message'].toString();
      success = jsonMap['success'] != null ? jsonMap['success'] : false;
      unReadCount = jsonMap['data'] ?? 0;
    } catch (e) {
      message = '';
      success = false;
      unReadCount = 0;
      print(e);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['success'] = this.success;
    data['data'] = this.unReadCount;
    return data;
  }

}
