import 'package:findnear/src/pages/livestream/model/ls_join_entity.dart';

class LeaveLiveResponse {
  String message;
  bool success;

  LeaveLiveResponse({this.message, this.success});

  factory LeaveLiveResponse.fromJson(Map<String, dynamic> json) {
    return LeaveLiveResponse(
      message: json['message'],
      success: json['success'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['success'] = this.success;
    return data;
  }
}