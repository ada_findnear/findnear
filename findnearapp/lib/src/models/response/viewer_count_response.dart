
class ViewerCountResponse {
  String liveStreamId;
  int count;

  ViewerCountResponse({
    this.liveStreamId,
    this.count,
  });

  factory ViewerCountResponse.fromJson(Map<String, dynamic> json) {
    return ViewerCountResponse(
      liveStreamId: json['liveStreamId'] ?? "",
      count: json['count'] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['liveStreamId'] = this.liveStreamId;
    data['count'] = this.count;
    return data;
  }
}
