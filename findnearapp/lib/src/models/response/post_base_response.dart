import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/utils/logger.dart';

import 'base_response.dart';

class PostBaseResponse extends BaseResponse<Post> {
  PostBaseResponse({Post data, String message, bool success})
      : super(message: message, success: success, data: data);

  factory PostBaseResponse.fromJson(Map<String, dynamic> json) {
    final baseResponse = BaseResponse<Post>.fromJson(
      json,
      (data) => Post.fromMap(data),
    );

    return PostBaseResponse(
      message: baseResponse.message,
      data: baseResponse.data,
      success: baseResponse.success,
    );
  }

  bool get isLike => this?.data?.isLike ?? false;
}
