import '../../utils/logger.dart';

class UserV2Response {
  int id = -1;
  String code = "";
  String name = "";
  String deviceToken = "";
  bool isBlocker = false;
  bool isBlocking = false;
  List<MediaUserV2Response> media = [];
  Market market = Market();

  UserV2Response({
    this.id,
    this.code,
    this.name,
    this.deviceToken,
    this.isBlocker,
    this.isBlocking,
    this.media,
    this.market,
  });

  UserV2Response copyWith({
    int id,
    String code,
    String name,
    String deviceToken,
    bool isBlocker,
    bool isBlocking,
    String media,
    Market market,
  }) {
    return UserV2Response(
      id: id ?? this.id,
      code: code ?? this.code,
      name: name ?? this.name,
      deviceToken: deviceToken ?? this.deviceToken,
      isBlocker: isBlocker ?? this.isBlocker,
      isBlocking: isBlocking ?? this.isBlocking,
      media: media ?? this.media,
      market: market ?? this.market,
    );
  }

  UserV2Response.fromJson(Map<String, dynamic> jsonData) {
    try {
      id = jsonData["id"] ?? id;
      code = jsonData["code"] ?? code;
      name = jsonData["name"] ?? name;
      deviceToken = jsonData["device_token"] ?? deviceToken;
      isBlocker = jsonData["is_blocker"] ?? isBlocker;
      isBlocking = jsonData["is_blocking"] ?? isBlocking;
      media = (jsonData['media'] as List)
          .map(
            (e) => MediaUserV2Response.fromJson(
              Map<String, dynamic>.from(e),
            ),
          )
          .toList();
      market =
          Market.fromJson(Map<String, dynamic>.from(jsonData["market"] ?? {}));
    } catch (e) {
      logger.e("UserV2Response - fromJson - error: $e");
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    try {
      data["id"] = this.id;
      data["code"] = this.code;
      data["name"] = this.name;
      data["device_token"] = this.deviceToken;
      data["is_blocker"] = this.isBlocker;
      data["is_blocking"] = this.isBlocking;
      data["media"] = List<Map<String, dynamic>>.from(
        media.map(
          (x) => x.toJson(),
        ),
      );
      data["market"] = market.toJson();
    } catch (e) {
      logger.e("UserV2Response - toJson - error: $e");
    }
    return data;
  }
}

class MediaUserV2Response {
  int id = -1;
  String collectionName = "";
  String url = "";
  String thumb = "";
  String icon = "";

  MediaUserV2Response({
    this.id,
    this.collectionName,
    this.url,
    this.thumb,
    this.icon,
  });

  MediaUserV2Response copyWith({
    String id,
    String collectionName,
    String url,
    String thumb,
    String icon,
  }) {
    return MediaUserV2Response(
      id: id ?? this.id,
      collectionName: collectionName ?? this.collectionName,
      url: url ?? this.url,
      thumb: thumb ?? this.thumb,
      icon: icon ?? this.icon,
    );
  }

  MediaUserV2Response.fromJson(Map<String, dynamic> jsonData) {
    try {
      id = jsonData["id"] ?? id;
      collectionName = jsonData["collection_name"] ?? collectionName;
      url = jsonData["url"] ?? url;
      thumb = jsonData["thumb"] ?? thumb;
      icon = jsonData["icon"] ?? icon;
    } catch (e) {
      logger.e("MediaUserV2Response - fromJson - error: $e");
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    try {
      data["id"] = this.id;
      data["collection_name"] = this.collectionName;
      data["url"] = this.url;
      data["thumb"] = this.thumb;
      data["icon"] = this.icon;
    } catch (e) {
      logger.e("MediaUserV2Response - toJson - error: $e");
    }
    return data;
  }
}

class Market {
  int id = -1;

  Market({
    this.id,
  });

  Market copyWith({
    String id,
  }) {
    return Market(
      id: id ?? this.id,
    );
  }

  Market.fromJson(Map<String, dynamic> jsonData) {
    try {
      id = jsonData["id"] ?? id;
    } catch (e) {
      logger.e("Market - fromJson - error: $e");
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    try {
      data["id"] = this.id;
    } catch (e) {
      logger.e("Market - toJson - error: $e");
    }
    return data;
  }
}
