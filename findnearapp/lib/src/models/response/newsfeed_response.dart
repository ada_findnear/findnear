import 'package:findnear/src/models/post.dart';

import 'base_response.dart';

class NewsfeedResponse extends BaseResponse<List<Post>> {
  NewsfeedResponse({List<Post> data, String message, bool success})
      : super(message: message, success: success, data: data);

  factory NewsfeedResponse.fromJson(Map<String, dynamic> json) {
    final baseResponse = BaseResponse<List<Post>>.fromJson(
      json,
      (data) => data is List
          ? data.map((e) => Post.fromMap(e)).toList(growable: false)
          : <Post>[],
    );
    return NewsfeedResponse(
      message: baseResponse.message,
      data: baseResponse.data,
      success: baseResponse.success,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = (this.data.map((e) => e.toMap()).toList());
    }
    return data;
  }
}
