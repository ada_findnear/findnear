import 'package:findnear/src/models/entities/page_able_entity.dart';
import 'package:findnear/src/models/response/comment_response.dart';

class FetchCommentResponse {
  List<CommentResponse> data;
  PageAbleEntity pageAble;

  FetchCommentResponse({
    this.data,
    this.pageAble,
  });

  factory FetchCommentResponse.fromJson(Map<String, dynamic> json) {
    return FetchCommentResponse(
      data: json['data'] != null ? List<CommentResponse>.from(json["data"]
          .map((x) => CommentResponse.fromJson(x))) : [],
      pageAble: json['pageAble'] != null ? PageAbleEntity.fromJson(json['pageAble']) : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['data'] = this.data;
    data['pageAble'] = this.pageAble.toJson();
    return data;
  }
}
