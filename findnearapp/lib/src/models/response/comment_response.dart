import 'package:findnear/src/models/entities/meta_entity.dart';
import 'package:findnear/src/utils/date_utils.dart';

class CommentResponse {
  String id;
  String livestreamId;
  String sender;
  String commentType;
  String content;
  String time;
  MetaEntity meta;
  List<String> userLikes;
  // "userLikes": []
  int totalLike;
  String commentId;
  int v;

  DateTime get realUpdateTime => DateUtils.changeIso8601StringToDatetime(time);
  int get createDateInMilis => realUpdateTime.millisecondsSinceEpoch;

  CommentResponse({
    this.id,
    this.livestreamId,
    this.sender,
    this.commentType,
    this.content,
    this.time,
    this.meta,
    this.userLikes,
    this.totalLike,
    this.commentId,
    this.v,
  });

  factory CommentResponse.fromJson(Map<String, dynamic> json) {
    return CommentResponse(
      id: json['_id'] ?? "",
      livestreamId: json['livestreamId'] ?? "",
      sender: json['sender'] ?? "",
      commentType: json['commentType'] ?? "MSG",
      content: json['content'] ?? "",
      time: json['time'] ?? "",
      meta: json['meta'] != null ? MetaEntity.fromJson(json['meta']) : null,
      userLikes: json['userLikes'] != null ? List<String>.from(json["userLikes"].map((x) => x)): [],
      totalLike: json['totalLike'] ?? 0,
      commentId: json['_id'] ?? "",
      v: json['__v'] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['livestreamId'] = this.livestreamId;
    data['commentType'] = this.commentType;
    data['content'] = this.content;
    data['time'] = this.time;
    if(meta!= null) data['meta'] = this.meta.toJson();
    data['userLikes'] = this.userLikes ?? [];
    data['totalLike'] = this.totalLike;
    data['_id'] = this.commentId;
    data['v'] = this.v;
    return data;
  }
}
