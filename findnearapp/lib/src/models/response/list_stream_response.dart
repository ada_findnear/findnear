import '../entities/livestream_info_entity.dart';
import 'base_response.dart';

class ListStreamResponse extends BaseResponse<List<LiveStreamInfoEntity>> {
  ListStreamResponse({List<LiveStreamInfoEntity> data, String message, bool success})
      : super(message: message, success: success, data: data);

  factory ListStreamResponse.fromJson(Map<String, dynamic> json) {
    final baseResponse = BaseResponse<List<LiveStreamInfoEntity>>.fromJson(
      json,
          (data) => data is List
          ? data.map((e) => LiveStreamInfoEntity.fromJson(e)).toList(growable: false)
          : <LiveStreamInfoEntity>[],
    );
    return ListStreamResponse(
      message: baseResponse.message,
      data: baseResponse.data,
      success: baseResponse.success,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = (this.data.map((e) => e.toJson()).toList());
    }
    return data;
  }
}