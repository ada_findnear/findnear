import 'package:findnear/src/models/order_history.dart';

class OrderHistoryResponse {
  List<OrderHistory> data;
  String message;
  bool success;

  OrderHistoryResponse({this.data, this.message, this.success});

  factory OrderHistoryResponse.fromJson(Map<String, dynamic> json) {
    return OrderHistoryResponse(
      data: json['data'] != null
          ? (json['data'] as List).map((i) => OrderHistory.fromJSON(i)).toList()
          : null,
      // data: json['data'] != null ? Data.fromJson(json['data']) : null,
      message: json['message'],
      success: json['success'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['success'] = this.success;
    if (this?.data?.isNotEmpty ?? false) {
      data['data'] = this.data.toList();
    }
    return data;
  }
}

class Data {
  List<OrderHistory> data;
  int current_page;
  String first_page_url;
  int from;
  int last_page;
  String last_page_url;
  Object next_page_url;
  String path;
  int per_page;
  Object prev_page_url;
  int to;
  int total;

  Data(
      {this.data,
      this.current_page,
      this.first_page_url,
      this.from,
      this.last_page,
      this.last_page_url,
      this.next_page_url,
      this.path,
      this.per_page,
      this.prev_page_url,
      this.to,
      this.total});

  factory Data.fromJson(Map<String, dynamic> json) {
    return Data(
      data: json['data'] != null
          ? (json['data'] as List).map((i) => OrderHistory.fromJSON(i)).toList()
          : null,
      current_page: json['current_page'],
      first_page_url: json['first_page_url'],
      from: json['from'],
      last_page: json['last_page'],
      //last_page_url: json['last_page_url'],
      // next_page_url: json['next_page_url'] != null ? Object.fromJson(json['next_page_url']) : null,
      path: json['path'],
      per_page: int.tryParse(json['per_page'].toString()),
      //prev_page_url: json['prev_page_url'] != null ? Object.fromJson(json['prev_page_url']) : null,
      to: json['to'],
      total: json['total'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['current_page'] = this.current_page;
    data['first_page_url'] = this.first_page_url;
    data['from'] = this.from;
    data['last_page'] = this.last_page;
    data['last_page_url'] = this.last_page_url;
    data['path'] = this.path;
    data['per_page'] = this.per_page;
    data['to'] = this.to;
    data['total'] = this.total;
    if (this.data != null) {
      data['data'] = this.data.toList();
    }
    return data;
  }
}
