class CreateCommentChannelResponse {
  String livestreamId;
  String ownerId;
  int count;
  String id;
  int v;

  CreateCommentChannelResponse({this.livestreamId, this.ownerId, this.count, this.id, this.v});

  factory CreateCommentChannelResponse.fromJson(Map<String, dynamic> json) {
    return CreateCommentChannelResponse(
      livestreamId: json['livestreamId'] ?? "",
      ownerId: json['ownerId'] ?? "",
      count: json['count'] ?? 0,
      id: json['id'] ?? "0",
      v: json['__v'] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['livestreamId'] = this.livestreamId;
    data['ownerId'] = this.ownerId;
    data['count'] = this.count;
    data['id'] = this.id;
    data['v'] = this.v;
    return data;
  }
}