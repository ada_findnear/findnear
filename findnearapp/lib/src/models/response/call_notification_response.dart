
class CallNotiResponse {
  String id;
  String nameCaller;
  String appName;
  String avatar;
  String action;
  String messageType;
  int timeSend;
  String codeCaller;
  String number;
  String handle;

  CallNotiResponse(
      {this.id,
      this.nameCaller,
      this.appName,
      this.avatar,
      this.action,
      this.messageType,
      this.timeSend,
      this.codeCaller,
      this.number,
      this.handle});

  factory CallNotiResponse.fromJson(Map<String, dynamic> json) {
    // OfferCallEntity extra = OfferCallEntity();
    // if (json['extra'] is String) {
    //   extra = OfferCallEntity.fromJson(jsonDecode(json['extra']));
    // } else if (json['extra'] is Map) {
    //   extra = OfferCallEntity.fromJson(Map<String, dynamic>.from(json['extra']));
    // }
    return CallNotiResponse(
      id: json['id'],
      nameCaller: json['nameCaller'],
      appName: json['appName'],
      avatar: json['avatar'],
      action: json['action'],
      messageType: json['messageType'],
      timeSend: json['timeSend'] is String ? int.parse(json['timeSend']?? "0"):json['timeSend'],
      codeCaller: json['codeCaller'],
      number: json['number'],
      handle: json['handle'],
    //  extra: extra,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nameCaller'] = this.nameCaller;
    data['appName'] = this.appName;
    data['avatar'] = this.avatar;
    data['action'] = this.action;
    data['messageType'] = this.messageType;
    data['timeSend'] = this.timeSend;
    data['codeCaller'] = this.codeCaller;
    data['number'] = this.number;
    data['handle'] = this.handle;
   // data['extra'] = this.extra;
    return data;
  }
}
