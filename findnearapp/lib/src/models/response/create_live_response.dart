import '../entities/create_livestream_entity.dart';

class CreateLiveResponse {
  CreateLiveEntity data;
  String message;
  bool success;

  CreateLiveResponse({this.data, this.message, this.success});

  factory CreateLiveResponse.fromJson(Map<String, dynamic> json) {
    return CreateLiveResponse(
      data: json['data'] != null ? CreateLiveEntity.fromJson(json['data']) : null,
      message: json['message'],
      success: json['success'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['success'] = this.success;
    return data;
  }
}