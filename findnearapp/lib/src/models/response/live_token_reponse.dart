
import '../entities/live_token_entity.dart';

class LiveTokenResponse {
  LiveTokenEntity data;
  String message;
  bool success;

  LiveTokenResponse({this.data, this.message, this.success});

  factory LiveTokenResponse.fromJson(Map<String, dynamic> json) {
    return LiveTokenResponse(
      data: json['data'] != null ? LiveTokenEntity.fromJson(json['data']) : null,
      message: json['message'],
      success: json['success'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['success'] = this.success;
    return data;
  }
}