import 'package:findnear/src/models/rating.dart';
import 'package:findnear/src/models/recommend_type.dart';
import 'package:findnear/src/models/response/base_response.dart';

class RatingOrderResponse extends BaseResponse<Rating> {
  RatingOrderResponse({Rating data, String message, bool success})
      : super(message: message, success: success, data: data);

  factory RatingOrderResponse.fromJson(Map<String, dynamic> json) {
    final baseResponse = BaseResponse<Rating>.fromJson(
      json,
      (data) => data != null ? Rating.fromJSON(data) : null,
    );
    return RatingOrderResponse(
      message: baseResponse.message,
      data: baseResponse.data,
      success: baseResponse.success,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data;
    }
    return data;
  }
}
