import 'package:findnear/src/pages/livestream/model/ls_join_entity.dart';

class JoinLiveResponse {
  String message;
  bool success;
  LSJoinEntity data;

  JoinLiveResponse({this.message, this.success, this.data});

  factory JoinLiveResponse.fromJson(Map<String, dynamic> json) {
    return JoinLiveResponse(
      message: json['message'],
      success: json['success'],
      data: LSJoinEntity.fromJSON(json['data']),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['success'] = this.success;
    data['data'] = this.data.toJSON();
    return data;
  }
}