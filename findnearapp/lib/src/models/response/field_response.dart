import '../field.dart';

class FieldResponse {
  List<Field> fieldList;
  String message;
  bool success;

  FieldResponse({this.fieldList, this.message, this.success});

  factory FieldResponse.fromJson(Map<String, dynamic> json) {
    return FieldResponse(
      fieldList: json['data'] != null
          ? (json['data'] as List).map((i) => Field.fromJSON(i)).toList()
          : null,
      message: json['message'],
      success: json['success'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['success'] = this.success;
    // if (this.data != null) {
    //   data['data'] = this.data.toJson();
    // }
    return data;
  }

  void createIndex() {
    int index = 0;
    for (final item in fieldList){
      item.createIndex(index);
      index++;
    }
  }
}
