import '../user.dart';

class UserLiveResponse {
  String message;
  bool success;
  User data;

  UserLiveResponse({this.message, this.success, this.data});

  factory UserLiveResponse.fromJson(Map<String, dynamic> json) {
    return UserLiveResponse(
      message: json['message'],
      success: json['success'],
      data: User.fromJSON(json["data"]),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['success'] = this.success;
    data['data'] = data;
    return data;
  }
}