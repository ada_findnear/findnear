import '../shop.dart';

class ShopsDetailResponse {
  Shop data;
  String message;
  bool success;

  ShopsDetailResponse({this.data, this.message, this.success});

  factory ShopsDetailResponse.fromJson(Map<String, dynamic> json) {
    return ShopsDetailResponse(
      data: json['data'] != null ? Shop.fromJson(json['data']) : null,
      message: json['message'],
      success: json['success'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}