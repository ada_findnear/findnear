class BaseResponse<T> {
  T data;
  String message;
  bool success;

  BaseResponse({this.data, this.message, this.success});

  factory BaseResponse.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic) dataFromJson,
  ) {
    return BaseResponse(
      data: dataFromJson(json['data']),
      message: json['message'],
      success: json['success'],
    );
  }
}
