import 'dart:convert';

import '../field.dart';
import '../media.dart';
import '../my_shop.dart';

class MyShopResponse {
  MyShop data;
  String message;
  bool success;

  MyShopResponse({this.data, this.message, this.success});

  factory MyShopResponse.fromJson(Map<String, dynamic> json) {
    return MyShopResponse(
      data: json['data'] != null ? MyShop.fromJson(json['data']) : null,
      message: json['message'],
      success: json['success'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['success'] = this.success;
    // if (this.data != null) {
    //   data['data'] = this.data.toJson();
    // }
    return data;
  }
}
