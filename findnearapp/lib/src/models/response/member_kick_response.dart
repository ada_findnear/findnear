
class MemberKickResponse {
  String liveStreamId;
  String sender;

  MemberKickResponse({
    this.liveStreamId,
    this.sender,
  });

  factory MemberKickResponse.fromJson(Map<String, dynamic> json) {
    return MemberKickResponse(
      liveStreamId: json['liveStreamId'] ?? "",
      sender: json['sender'] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['liveStreamId'] = this.liveStreamId;
    data['sender'] = this.sender;
    return data;
  }
}
