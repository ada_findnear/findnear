import 'package:dio/dio.dart';
import 'package:findnear/src/models/base/data_reponse.dart';
import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/models/entities/group/group_topic_entity.dart';
import 'package:findnear/src/models/input/GetPostsOfGroupInput.dart';
import 'package:findnear/src/models/input/UpdateGroupCoverInput.dart';
import 'package:findnear/src/models/input/UpdatePostGroupStatusInput.dart';
import 'package:findnear/src/models/input/group_input.dart';
import 'package:findnear/src/models/input/group_member_input.dart';
import 'package:findnear/src/models/input/invite_user_to_group_input.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/models/response/base_response.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/repository/base/base_repository.dart';

abstract class GroupApi {
  Future<BaseResponse<GroupEntity>> createGroup(GroupInput input);
  Future<BaseResponse<GroupEntity>> updateGroup(GroupInput input);
  Future<BaseResponse<GroupEntity>> updateGroupCover(
      UpdateGroupCoverInput input);

  Future<ArrayDataResponse<GroupTopicEntity>> getGroupTopic();
  Future inviteUserToGroup(InviteUserToGroupInput input);
  Future<ArrayDataResponse<GroupEntity>> getGroupsJoined();
  Future<ArrayDataResponse<Post>> getGroupPostsJoined({
    offset = 0,
    limit = 10,
  });

  Future<ArrayDataResponse<GroupEntity>> getMyGroups({
    offset = 0,
    limit = 10,
  });

  Future<ArrayDataResponse<Post>> getMyGroupPosts({offset = 0, limit = 10});
  Future<ArrayDataResponse<Post>> getPostsOfGroup(
    GetPostsOfGroupInput input, {
    offset = 0,
    limit = 10,
  });
  Future<void> updatePostGroupStatus(UpdatePostGroupStatusInput input);

  Future<ArrayDataResponse<GroupEntity>> getDiscoverGroup({
    offset = 0,
    limit = 10,
  });

  Future<BaseResponse<GroupEntity>> getGroupDetail(int id);
  Future<ArrayDataResponse<User>> getGroupMembers(
    GroupMemberInput input, {
    offset = 0,
    limit = 10,
  });

  Future<ArrayDataResponse<GroupEntity>> getSuggestGroups({
    offset = 0,
    limit = 10,
  });

  Future<BaseResponse<GroupEntity>> outGroup(int groupId);
}

class GroupApiImpl extends BaseApi implements GroupApi {
  @override
  Future<BaseResponse<GroupEntity>> createGroup(GroupInput input) async {
    final response = await sendRequest(
      POST('group'),
      body: input.toJson(),
      authenticate: true,
    );
    return BaseResponse<GroupEntity>.fromJson(
      response,
      (data) => GroupEntity.fromJson(data),
    );
  }

  @override
  Future<ArrayDataResponse<GroupTopicEntity>> getGroupTopic() async {
    final response = await sendRequest(
      GET('group-type'),
      authenticate: true,
    );
    return ArrayDataResponse<GroupTopicEntity>.fromJson(
      response,
      (data) => GroupTopicEntity.fromJson(data),
    );
  }

  @override
  Future inviteUserToGroup(InviteUserToGroupInput input) async {
    print("inviteUserToGroup body ${input.toJson()}");

    await sendRequest(
      POST('group/${input.group_id}/member'),
      body: input.toJson(),
      authenticate: true,
    );
    return;
  }

  @override
  Future<ArrayDataResponse<GroupEntity>> getGroupsJoined() async {
    final response = await sendRequest(
      GET('my-group'),
      authenticate: true,
    );
    return ArrayDataResponse<GroupEntity>.fromJson(
      response,
      (data) => GroupEntity.fromJson(data),
    );
  }

  @override
  Future<ArrayDataResponse<GroupEntity>> getMyGroups({
    offset = 0,
    limit = 10,
  }) async {
    final response = await sendRequest(
      GET('group'),
      authenticate: true,
      queryParameters: {
        'limit': limit,
        'offset': offset,
        'owned': 1,
      },
    );
    return ArrayDataResponse<GroupEntity>.fromJson(
      response,
      (data) => GroupEntity.fromJson(data),
    );
  }

  @override
  Future<ArrayDataResponse<Post>> getGroupPostsJoined(
      {offset = 0, limit = 10}) async {
    final response = await sendRequest(
      GET('my-group-post'),
      authenticate: true,
      queryParameters: {
        'limit': limit,
        'offset': offset,
      },
    );
    return ArrayDataResponse<Post>.fromJson(
      response,
      (data) => Post.fromMap(data),
    );
  }

  @override
  Future<ArrayDataResponse<Post>> getMyGroupPosts(
      {offset = 0, limit = 10}) async {
    final response = await sendRequest(
      GET('my-owned-group-post'),
      authenticate: true,
      queryParameters: {
        'limit': limit,
        'offset': offset,
      },
    );
    return ArrayDataResponse<Post>.fromJson(
      response,
      (data) => Post.fromMap(data),
    );
  }

  @override
  Future<ArrayDataResponse<GroupEntity>> getDiscoverGroup(
      {offset = 0, limit = 10}) async {
    final response = await sendRequest(
      GET('discover-group'),
      authenticate: true,
      queryParameters: {
        'limit': limit,
        'offset': offset,
      },
    );
    return ArrayDataResponse<GroupEntity>.fromJson(
      response,
      (data) => GroupEntity.fromJson(data),
    );
  }

  @override
  Future<BaseResponse<GroupEntity>> updateGroupCover(
      UpdateGroupCoverInput input) async {
    var formData = FormData();
    var entry = MapEntry(
      'image',
      MultipartFile.fromFileSync(
        input.coverFilePath,
        filename: input.coverFilePath.split("/").last,
      ),
    );
    formData.files.add(entry);

    final response = await sendRequest(
      POST('group/${input.groupId}/media'),
      body: formData,
      authenticate: true,
    );

    return BaseResponse<GroupEntity>.fromJson(
      response,
      (data) => GroupEntity.fromJson(data),
    );
  }

  @override
  Future<BaseResponse<GroupEntity>> getGroupDetail(int id) async {
    final response = await sendRequest(
      GET('group/${id}'),
      authenticate: true,
    );
    return BaseResponse<GroupEntity>.fromJson(
      response,
      (data) => GroupEntity.fromJson(data),
    );
  }

  @override
  Future<ArrayDataResponse<User>> getGroupMembers(GroupMemberInput input,
      {offset = 0, limit = 10}) async {
    var parameter = input.toJson();
    parameter['limit'] = limit;
    parameter['offset'] = offset;

    final response = await sendRequest(
      GET('group/${input.groupId}/member'),
      authenticate: true,
      queryParameters: parameter,
    );
    return ArrayDataResponse<User>.fromJson(
      response,
      (data) => User.fromJSON(data),
    );
  }

  @override
  Future<ArrayDataResponse<GroupEntity>> getSuggestGroups(
      {offset = 0, limit = 10}) async {
    final response = await sendRequest(
      GET('group'),
      authenticate: true,
      queryParameters: {
        'limit': limit,
        'offset': offset,
        'to_join': 1,
      },
    );
    return ArrayDataResponse<GroupEntity>.fromJson(
      response,
      (data) => GroupEntity.fromJson(data),
    );
  }

  @override
  Future<BaseResponse<GroupEntity>> updateGroup(GroupInput input) async {
    print("update group body ${input.toJson()}");
    final response = await sendRequest(
      PUT('group/${input.groupId}'),
      body: input.toJson(),
      authenticate: true,
    );
    return BaseResponse<GroupEntity>.fromJson(
      response,
      (data) => GroupEntity.fromJson(data),
    );
  }

  @override
  Future<ArrayDataResponse<Post>> getPostsOfGroup(
    GetPostsOfGroupInput input, {
    offset = 0,
    limit = 10,
  }) async {
    var parameter = input.toJson();
    parameter['limit'] = limit;
    parameter['offset'] = offset;

    final response = await sendRequest(
      GET('group/${input.groupId}/post'),
      authenticate: true,
      queryParameters: parameter,
    );
    return ArrayDataResponse<Post>.fromJson(
      response,
      (data) => Post.fromJSON(data),
    );
  }

  @override
  Future<void> updatePostGroupStatus(UpdatePostGroupStatusInput input) async {
    print("updatePostGroupStatusp body ${input.toJson()}");

    await sendRequest(
      POST('group/${input.groupId}/post'),
      body: input.toJson(),
      authenticate: true,
    );
    return;
  }

  @override
  Future<BaseResponse<GroupEntity>> outGroup(int groupId) async {
    final response = await sendRequest(
      DELETE('group/${groupId}/member'),
      authenticate: true,
    );
    return BaseResponse<GroupEntity>.fromJson(
      response,
      (data) => GroupEntity.fromJson(data),
    );
  }
}
