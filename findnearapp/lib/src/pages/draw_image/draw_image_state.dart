import 'package:flutter/material.dart';
import 'package:flutter_painter/flutter_painter.dart';
import 'package:get/state_manager.dart';

import '../../base/base_state.dart';

enum ToolType {
  colorPalette,
  brush,
  eraser,
}

class DrawImageState extends BaseState {
  static final double _defaultStrokeWidth = 6;
  static final _defaultColorPalette = [
    Color(0xFFFF0000),
    Color(0xFFFFA900),
    Color(0xFFFFD72E),
    Color(0xFF00AA41),
    Color(0xFF0089FF),
    Color(0xFF0036AF),
    Color(0xFF8900FF),
    Color(0xFF80FFC4),
    Color(0xFFFF8383),
    Color(0xFF7D3D1D),
    Color(0xFF00B29D),
    Color(0xFF00EBFF),
    Color(0xFFCC0000),
    Color(0xFFFFFFFF),
    Color(0xFFF2F2F2),
    Color(0xFF979797),
    Color(0xFF333333),
  ];

  GlobalKey repaintBoundaryKey = GlobalKey();
  PainterController painterController;

  final colorPalette = _defaultColorPalette;
  final double strokeWidthMin = 1;
  final double strokeWidthMax = 12;

  var currentToolType = Rx<ToolType>(ToolType.colorPalette);
  var backgroundImagePath = RxString(null);

  var selectedColor = Rx<Color>(_defaultColorPalette.first);

  var currentWidthBrush = RxDouble(_defaultStrokeWidth);
  var currentWidthEraser = RxDouble(_defaultStrokeWidth);
}
