import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_painter/flutter_painter.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:images_picker/images_picker.dart';

import '../../../generated/l10n.dart';
import '../../base/base_controller.dart';
import '../../elements/MediaActionSheetWidget.dart';
import '../../utils/logger.dart';
import '../../utils/navigator_utils.dart';
import '../../utils/utils.dart';
import '../../widgets/dialog_helper.dart';
import 'draw_image_state.dart';

class DrawImageController extends BaseController<DrawImageState> {
  @override
  DrawImageState createState() => DrawImageState();

  void undo() {
    state.painterController.removeLastDrawable();
  }

  void toggleColorPalette() {
    state.currentToolType.value = ToolType.colorPalette;
  }

  void changeBrushColor(Color color) {
    state.selectedColor.value = color;
    state.painterController.freeStyleSettings =
        state.painterController.freeStyleSettings.copyWith(
      color: color,
    );
  }

  void toggleBrush() {
    state.currentToolType.value = ToolType.brush;
    state.painterController.freeStyleSettings =
        state.painterController.freeStyleSettings.copyWith(
      color: state.selectedColor.value,
      strokeWidth: state.currentWidthBrush.value,
    );
  }

  void changeWidthBrush(double value) {
    state.currentWidthBrush.value = value;
    state.painterController.freeStyleSettings =
        state.painterController.freeStyleSettings.copyWith(
      strokeWidth: value,
    );
  }

  void toggleEraser() {
    state.currentToolType.value = ToolType.eraser;
    state.painterController.freeStyleSettings =
        state.painterController.freeStyleSettings.copyWith(
      color: Colors.white,
      strokeWidth: state.currentWidthEraser.value,
    );
  }

  void changeWidthEraser(double value) {
    state.currentWidthEraser.value = value;
    state.painterController.freeStyleSettings =
        state.painterController.freeStyleSettings.copyWith(
      strokeWidth: value,
    );
  }

  void selectImage() async {
    final context = Get.context;
    final mediaActionSheetWidget = MediaActionSheetWidget(context: context);
    mediaActionSheetWidget.showActionSheet(
      S.of(context).select_background_image,
      [
        S.of(context).takePhoto,
        S.of(context).chooseFromGrallery,
      ],
      (int index) async {
        if (index == 0) {
          mediaActionSheetWidget.pickImageDialog(
            ImageSource.camera,
            (PickedFile pickedFile) =>
                state.backgroundImagePath.value = pickedFile.path,
          );
        }
        if (index == 1) {
          List<Media> res = await ImagesPicker.pick(
            pickType: PickType.image,
            language: Language.System,
            quality: 0.8,
          );
          state.backgroundImagePath.value = res[0].path;
        }
      },
    );
  }

  void resetPainter() {
    if (state.painterController.drawables.isEmpty &&
        state.backgroundImagePath.value == null) return;

    final context = Get.context;
    DialogHelper.showConfirmPopup(
      title: S.of(context).notifications,
      description: S.of(context).reset_draw_image_dialog_message,
      okBtnText: S.of(context).delete,
      cancelText: S.of(context).no_dialog_action,
      okBtnPressed: () {
        state.painterController.clearDrawables();
        state.backgroundImagePath.value = null;
      },
    );
  }

  void navigateBack() {
    if (state.painterController.drawables.isEmpty &&
        state.backgroundImagePath.value == null) {
      goBack();
      return;
    }

    final context = Get.context;
    DialogHelper.showConfirmPopup(
      title: S.of(context).confirmation,
      description: S.of(context).discard_draw_image_dialog_message,
      okBtnText: S.of(context).yes_dialog_action,
      cancelText: S.of(context).no_dialog_action,
      okBtnPressed: () {
        goBack();
      },
    );
  }

  void send() async {
    final file = await _saveDrawFile();

    if (file != null) {
      goBack(argument: file.path);
    }
  }

  Future<File> _saveDrawFile() async {
    try {
      final boundary = state.repaintBoundaryKey.currentContext
          .findRenderObject() as RenderRepaintBoundary;
      final directory = await getTemporaryDirectory();
      final file = File(directory.path + "/findnear/painter.png");

      if (!file.parent.existsSync()) {
        file.parent.createSync();
      }

      final pngBytes = await Utils.captureWidget(boundary);
      var painterFile = await file.writeAsBytes(pngBytes);

      logger.d("DrawImageController - save draw file: ${painterFile.path}");
      return painterFile;
    } catch (e) {
      logger.e("DrawImageController - save draw file failed: $e");
      return null;
    }
  }
}
