import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_painter/flutter_painter.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

import '../../../generated/l10n.dart';
import '../../base/base_widget_state.dart';
import '../../commons/app_colors.dart';
import '../../commons/app_images.dart';
import '../../widgets/brush_slider/brush_slider_widget.dart';
import '../../widgets/color_palette.dart';
import '../../widgets/common.dart';
import 'draw_image_controller.dart';
import 'draw_image_state.dart';

class DrawImagePage extends StatefulWidget {
  static const String ROUTE_NAME = '/DrawImage';

  const DrawImagePage({Key key}) : super(key: key);

  @override
  _DrawImagePageState createState() => _DrawImagePageState();
}

class _DrawImagePageState extends BaseWidgetState<DrawImagePage,
    DrawImageController, DrawImageState> {
  @override
  void initState() {
    super.initState();
    state.painterController = PainterController(
      settings: PainterSettings(
        freeStyle: FreeStyleSettings(
          enabled: true,
          color: state.selectedColor.value,
          strokeWidth: state.currentWidthBrush.value,
        ),
      ),
    );
  }

  @override
  void dispose() {
    Get.delete<DrawImageController>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        backgroundColor: AppColors.grayBg,
        appBar: _buildAppBar(),
        body: _buildBody(),
      ),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      centerTitle: false,
      leadingWidth: 52,
      leading: IconButton(
        icon: Image.asset(
          AppImages.icBackDark,
          color: AppColors.textBlack,
        ),
        onPressed: controller.navigateBack,
      ),
      titleSpacing: 0,
      title: Text(
        "Hình vẽ",
        overflow: TextOverflow.fade,
        maxLines: 1,
        style: Theme.of(context).textTheme.headline6.merge(
              TextStyle(
                letterSpacing: 1.3,
                color: AppColors.textBlack,
              ),
            ),
      ),
      actions: [
        Container(
          margin: EdgeInsets.symmetric(vertical: 10, horizontal: 12),
          child: ElevatedButton(
            onPressed: controller.send,
            child: Text(S.of(context).send),
            style: ElevatedButton.styleFrom(
                shape: StadiumBorder(),
                primary: Theme.of(context).colorScheme.secondary),
          ),
        ),
      ],
    );
  }

  Widget _buildBody() {
    return SafeArea(
      child: Container(
        width: double.infinity,
        height: double.infinity,
        child: Column(
          children: [
            Expanded(
              child: _buildPainterWidget(),
            ),
            Container(
              color: AppColors.white,
              width: double.infinity,
              height: 56,
              child: Obx(_buildToolConfigWidget),
            ),
            Obx(
              () => _buildToolsWidget(context),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildPainterWidget() {
    return RepaintBoundary(
      key: state.repaintBoundaryKey,
      child: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            color: Colors.white,
            child: Obx(_buildBackgroundWidget),
          ),
          FlutterPainter(
            controller: state.painterController,
          ),
        ],
      ),
    );
  }

  Widget _buildBackgroundWidget() {
    if (state.backgroundImagePath.value == null) {
      return Container();
    } else {
      return Image.file(
        File(state.backgroundImagePath.value),
        fit: BoxFit.cover,
      );
    }
  }

  Widget _buildToolConfigWidget() {
    switch (state.currentToolType.value) {
      case ToolType.colorPalette:
        return Container(
          alignment: Alignment.bottomLeft,
          child: Container(
            height: 30,
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: Theme.of(context).hintColor.withOpacity(0.15),
                    offset: Offset(0, 0),
                    blurRadius: 6)
              ],
            ),
            child: Obx(
              () => ColorPaletteWidget(
                colors: state.colorPalette,
                selectedColor: state.selectedColor.value,
                onColorSelected: controller.changeBrushColor,
              ),
            ),
          ),
        );
      case ToolType.brush:
        return Container(
          child: Obx(
            () => _buildSliderWidget(
              value: state.currentWidthBrush.value,
              color: state.selectedColor.value,
              onChanged: controller.changeWidthBrush,
            ),
          ),
        );
      case ToolType.eraser:
        return Container(
          child: Obx(
            () => _buildSliderWidget(
              value: state.currentWidthEraser.value,
              color: Colors.white,
              onChanged: controller.changeWidthEraser,
            ),
          ),
        );
    }
    return Container();
  }

  Widget _buildSliderWidget({
    double value,
    Color color,
    Function onChanged,
  }) {
    Color sliderColor = color;
    if (sliderColor == Colors.white) sliderColor = AppColors.dividerColor;

    return BrushSliderWidget(
      value: value,
      min: state.strokeWidthMin,
      max: state.strokeWidthMax,
      backgroundThumbColor: Colors.white,
      foregroundThumbColor: sliderColor,
      backgroundColor: AppColors.divider,
      onChanged: onChanged,
    );
  }

  Widget _buildToolsWidget(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.grayBg,
        boxShadow: [
          BoxShadow(
            color: Theme.of(context).hintColor.withOpacity(0.08),
            offset: Offset(0, -6.0),
            blurRadius: 6,
          )
        ],
      ),
      width: double.infinity,
      height: 52,
      child: Material(
        child: Row(
          children: [
            _buildTool(
              icon: AppImages.icUndo,
              onPressed: controller.undo,
            ),
            _buildTool(
              icon: AppImages.icColorPalette,
              isSelected: state.currentToolType == ToolType.colorPalette,
              onPressed: controller.toggleColorPalette,
            ),
            _buildTool(
              icon: AppImages.icBrush,
              isSelected: state.currentToolType == ToolType.brush,
              onPressed: controller.toggleBrush,
            ),
            _buildTool(
              icon: AppImages.icEraser,
              isSelected: state.currentToolType == ToolType.eraser,
              onPressed: controller.toggleEraser,
            ),
            _buildTool(
              icon: AppImages.icImage,
              onPressed: controller.selectImage,
            ),
            _buildTool(
              icon: AppImages.icDelete,
              onPressed: controller.resetPainter,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTool({
    String icon,
    Function onPressed,
    bool isSelected = false,
  }) {
    return Expanded(
      child: IconButton(
        onPressed: onPressed,
        splashColor: Colors.red,
        icon: AppImage(
          icon,
          color: isSelected
              ? Theme.of(context).colorScheme.secondary
              : Colors.grey,
          width: 24,
          height: 24,
        ),
      ),
    );
  }

  Future<bool> _onWillPop() async {
    controller.navigateBack();
    return false;
  }

  @override
  DrawImageController createController() => DrawImageController();
}
