import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/pages/preview_video.dart';
import 'package:findnear/src/pages/video.dart';
import '../models/route_argument.dart';
import 'package:findnear/src/pages/preview_media.dart';
import '../repository/user_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../controllers/profile_controller.dart';

class ProfileListVideoWidget extends StatefulWidget {
  final String userId;

  ProfileListVideoWidget({Key key, this.userId}) : super(key: key);

  @override
  _ProfileListVideoWidgetState createState() => _ProfileListVideoWidgetState();
}

class _ProfileListVideoWidgetState extends StateMVC<ProfileListVideoWidget> {
  ProfileController _profileController;

  _ProfileListVideoWidgetState() : super(ProfileController()) {
    _profileController = controller;
  }

  initState() {
    if (widget.userId != null) {
      _profileController.getListMedia(widget.userId);
    } else {
      _profileController.getListMedia(currentUser.value?.id ?? "");
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final isDark = Theme.of(context).brightness == Brightness.dark;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Image.asset(isDark ? AppImages.icBackDark : AppImages.icBackLight),
          onPressed: () {
            Navigator.pop(context, false);
          },
        ),
        leadingWidth: 40,
        titleSpacing: 0,
        centerTitle: false,
        title: Text(
          S.of(context).video,
          style: Theme.of(context).textTheme.bodyText1.copyWith(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
        ),
      ),
      body: listVideo(),
    );
  }

  Widget listVideo() {
    return StaggeredGridView.countBuilder(
      crossAxisCount: 6,
      itemCount: _profileController.listVideo.length,
      itemBuilder: (BuildContext context, int index) => GestureDetector(
        onTap: () {
          _previewTopicVideo(_profileController.listVideo.elementAt(index).url);
        },
        child: VideoWidget(path: _profileController.listVideo.elementAt(index).url),
      ),
      staggeredTileBuilder: (int index) => new StaggeredTile.count(2, 2),
      mainAxisSpacing: 4.0,
      crossAxisSpacing: 4.0,
    );
  }

  Future<bool> _loadMore() async {
    await Future.delayed(Duration(seconds: 2, milliseconds: 1000));
    if (widget.userId != null) {
      await _profileController.getListMedia(widget.userId);
    } else {
      await _profileController.getListMedia(currentUser.value?.id ?? "");
    }
    return true;
  }

  void _previewTopicVideo(url) {
    Navigator.of(context).push(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) => PreviewVideoWidget(path: url),
      ),
    );
  }
}
