import 'package:get/get.dart';

import 'control_panel_state.dart';

class ControlPanelLogic extends GetxController {
  final ControlPanelState state = ControlPanelState();
}
