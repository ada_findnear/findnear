import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/chat.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UnpinPanel extends StatelessWidget {
  Function onTapUnpin;
  Function onTapPinMessageList;

  UnpinPanel({this.onTapUnpin, this.onTapPinMessageList});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisSize: MainAxisSize.min,
      children: [
        Flexible(
          child: Container(
            width: Get.mediaQuery.size.width / 1.8,
            margin: EdgeInsets.symmetric(horizontal: 14, vertical: 123),
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
              color: AppColors.white,
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Flexible(
                    child: GestureDetector(
                      onTap: (){
                        onTapPinMessageList();
                      },
                      child: _buildItem(
                          image: AppImages.icGroupListUnpin,
                          text: "Danh sách tin ghim"),
                    )),
                Flexible(
                    child: Container(
                  color: AppColors.grayLight,
                  height: 0.5,
                  margin: EdgeInsets.symmetric(vertical: 11),
                )),
                Flexible(
                    child: GestureDetector(
                      onTap: (){
                        onTapUnpin();
                      },
                      child: _buildItem(
                          image: AppImages.icGroupUnpin, text: "Bỏ ghim"),
                    )),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildItem({String image, String text}) {
    return Container(
      color: AppColors.white,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(
            image,
            width: 14,
            height: 14,
          ),
          SizedBox(width: 20),
          Expanded(
            child: Material(
              child: Text(
                text,
                style: TextStyle(
                    fontFamily: "Quicksand",
                    fontSize: 14,
                    color: AppColors.black,
                    fontWeight: FontWeight.normal),
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
