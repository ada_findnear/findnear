import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../commons/app_colors.dart';
import '../../commons/app_images.dart';
import '../../models/entities/message_chat/entities/message_chat.dart';
import '../../models/entities/message_chat/enums/message_chat_type.dart';
import '../../widgets/common.dart';
import '../../widgets/touchable_opacity.dart';
import 'control_panel_logic.dart';

class ControlPanelPage extends StatelessWidget {
  static const String ROUTE_NAME = 'ControlPanelPage';
  final logic = Get.put(ControlPanelLogic());
  final state = Get.find<ControlPanelLogic>().state;

  final MessageChat chat;
  final Function onTapReply;
  final Function onTapCopy;
  final Function onTapPinMessage;
  final Function onTapForward;
  final Function onTapDelete;
  final Function onTapEditImage;
  final Function onDownload;

  ControlPanelPage({
    this.chat,
    this.onTapReply,
    this.onTapCopy,
    this.onTapPinMessage,
    this.onTapForward,
    this.onTapDelete,
    this.onTapEditImage,
    this.onDownload,
  });

  @override
  Widget build(BuildContext context) {
    final datas = _createDatas()..removeWhere((e) => !e.visible);
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        // Container(
        //   margin: EdgeInsets.symmetric(horizontal: 12, vertical: 9),
        //   padding: EdgeInsets.symmetric(horizontal: 18, vertical: 9),
        //   decoration: BoxDecoration(
        //     color: AppColors.white,
        //     borderRadius: BorderRadius.all(Radius.circular(20)),
        //   ),
        //   child: Row(
        //     children: [
        //       Expanded(
        //           flex: 1,
        //           child: Image.asset(AppImages.icBoxHeart,
        //               width: 22.48, height: 20.54)),
        //       Expanded(
        //           flex: 1,
        //           child: Image.asset(AppImages.icBoxLike,
        //               width: 21.17, height: 23.21)),
        //       Expanded(
        //           flex: 1,
        //           child: Image.asset(AppImages.icBoxLaughing,
        //               width: 26, height: 26)),
        //       Expanded(
        //           flex: 1,
        //           child: Image.asset(AppImages.icBoxCrying,
        //               width: 26.1, height: 25.27)),
        //       Expanded(
        //           flex: 1,
        //           child: Image.asset(AppImages.icBoxLove,
        //               width: 26.39, height: 25.71)),
        //       Expanded(
        //           flex: 1,
        //           child: Image.asset(AppImages.icBoxShocked,
        //               width: 25.98, height: 25.71)),
        //       Expanded(
        //           flex: 1,
        //           child: Image.asset(AppImages.icBoxVampire,
        //               width: 26.39, height: 25.71)),
        //     ],
        //   ),
        // ),
        Container(
          margin: EdgeInsets.only(
            left: 20,
            right: 20,
            bottom: 45,
          ),
          padding: EdgeInsets.only(
            left: 8,
            right: 8,
            top: 16,
            bottom: 4,
          ),
          decoration: BoxDecoration(
            color: AppColors.white,
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
          ),
          child: GridView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 5,
              crossAxisSpacing: 2,
              mainAxisSpacing: 4,
              childAspectRatio: 68 / 54,
            ),
            itemCount: datas.length,
            itemBuilder: (context, index) {
              final data = datas[index];
              return TouchableOpacity(
                onTap: () {
                  Get.back();
                  data.onTap?.call();
                },
                child: _buildItem(
                  icon: data.iconPath,
                  text: data.text,
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  Widget _buildItem({String icon, String text}) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          icon.contains(".svg")
              ? AppImage(icon, width: 17, height: 17)
              : Image.asset(icon, width: 17, height: 17),
          SizedBox(height: 8),
          Text(
            text,
            maxLines: 2,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: "Quicksand",
              fontSize: 12,
              color: AppColors.black,
              fontWeight: FontWeight.normal,
            ),
          ),
        ],
      ),
    );
  }

  List<_ControlPanelEntity> _createDatas() {
    return [
      _ControlPanelEntity(
        text: "Trả lời",
        iconPath: AppImages.icBoxQuote,
        visible: true,
        onTap: () => onTapReply(chat),
      ),
      _ControlPanelEntity(
        text: "Sao chép",
        iconPath: AppImages.icBoxCopy,
        visible: true,
        onTap: () => onTapCopy(chat),
      ),
      _ControlPanelEntity(
        text: "Ghim",
        iconPath: AppImages.icBoxPin,
        visible: true,
        onTap: () => onTapPinMessage(chat),
      ),
      _ControlPanelEntity(
        text: "Chuyển tiếp",
        iconPath: AppImages.icBoxDirect,
        visible: true,
        onTap: () => onTapForward(chat),
      ),
      _ControlPanelEntity(
        text: "Xoá",
        iconPath: AppImages.icBoxTrash,
        visible: true,
        onTap: () => onTapDelete(chat),
      ),
      _ControlPanelEntity(
        text: "Chỉnh sửa ảnh",
        iconPath: AppImages.icEditImage,
        visible: chat.type == MessageChatType.image,
        onTap: () => onTapEditImage(chat),
      ),
      _ControlPanelEntity(
        text: "Tải về",
        iconPath: AppImages.icDownload,
        visible: chat.type == MessageChatType.image ||
            chat.type == MessageChatType.video,
        onTap: () => onDownload(chat),
      ),
    ];
  }
}

class _ControlPanelEntity {
  String text;
  String iconPath;
  bool visible;
  Function onTap;

  _ControlPanelEntity({
    @required this.text,
    @required this.iconPath,
    @required this.visible,
    @required this.onTap,
  });
}
