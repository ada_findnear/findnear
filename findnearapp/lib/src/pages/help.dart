import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/ContactWidget.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../controllers/faq_controller.dart';
import '../elements/CircularLoadingWidget.dart';
import '../elements/FaqItemWidget.dart';

class HelpWidget extends StatefulWidget {
  @override
  _HelpWidgetState createState() => _HelpWidgetState();
}

class _HelpWidgetState extends StateMVC<HelpWidget>
    with TickerProviderStateMixin {
  FaqController _con;
  TabController _tabController;

  _HelpWidgetState() : super(FaqController()) {
    _con = controller;
    _tabController = TabController(length: 3, vsync: this);
    _tabController.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return _con.faqs.isEmpty
        ? CircularLoadingWidget(height: 500)
        : DefaultTabController(
            length: _con.faqs.length,
            child: Scaffold(
              key: _con.scaffoldKey,
              appBar: AppBar(
                backgroundColor: Colors.white,
                elevation: 10,
                iconTheme: IconThemeData(color: Colors.black),
                title: Text(
                  S.of(context).help__support,
                  style: Theme.of(context).textTheme.headline6.merge(
                      TextStyle(letterSpacing: 1.3, color: Colors.black)),
                ),
                // actions: <Widget>[
                //   communication ShoppingCartButtonWidget(iconColor: Theme.of(context).primaryColor, labelColor: Theme.of(context).accentColor),
                // ],
              ),
              body: Container(
                color: Colors.white,
                child: Column(
                  children: [
                    SizedBox(height: 16),
                    Stack(
                      children: [
                        AppImage(AppImages.imgHelpHeader),
                        Positioned(
                          top: 20,
                          left: 20,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Chào ${currentUser?.value?.name ?? 'bạn'},',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    color: Color.fromARGB(255, 255, 0, 0)),
                              ),
                              SizedBox(height: 6),
                              Text('Chúng tôi giúp gì được cho bạn?',
                                  style: TextStyle(
                                      fontSize: 14,
                                      color: Color.fromARGB(255, 255, 0, 0))),
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 10),
                    TabBar(
                      labelPadding: EdgeInsets.only(left: 5, right: 5),
                      indicatorColor: Colors.transparent,
                      controller: _tabController,
                      isScrollable: true,
                      tabs: List.generate(_con.faqs.length, (index) {
                        return Tab(
                            child: Container(
                          padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                          decoration: BoxDecoration(
                              color: _tabController?.index == index
                                  ? Color.fromARGB(255, 255, 0, 0)
                                  : Color.fromARGB(255, 237, 237, 237),
                              borderRadius: BorderRadius.circular(20)),
                          child: Text(
                            _con.faqs.elementAt(index).name ?? '',
                            style: TextStyle(
                                color: _tabController?.index == index
                                    ? Colors.white
                                    : Colors.black),
                          ),
                        ));
                      }),
                      labelColor: Theme.of(context).primaryColor,
                    ),
                    Expanded(
                      child: RefreshIndicator(
                        onRefresh: _con.refreshFaqs,
                        child: TabBarView(
                          controller: _tabController,
                          children: List.generate(_con.faqs.length, (index) {
                            return SingleChildScrollView(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  () {
                                    if (index == 0) {
                                      return _buildFaqTabPage(index);
                                    } else if (index == 1) {
                                      return _buildBuyIDTabPage();
                                    } else {
                                      return _buildContactTabPage();
                                    }
                                  }()
                                ],
                              ),
                            );
                          }),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
  }

  Widget _buildBuyIDTabPage() {
    return Container(
      alignment: Alignment.topCenter,
      padding: EdgeInsets.symmetric(vertical: 5),
      child: Column(
        children: [
          SizedBox(height: 12),
          Text('Để mua mã ID, vui lòng liên hệ hotline:',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
          SizedBox(height: 12),
          Container(
            padding: EdgeInsets.fromLTRB(15, 8, 15, 8),
            child: Text(
              '0963 125 582',
              style: TextStyle(
                  color: Color.fromARGB(255, 255, 0, 0),
                  fontSize: 16,
                  fontWeight: FontWeight.bold),
            ),
            decoration: BoxDecoration(
                border: Border.all(
                  color: Color.fromARGB(255, 255, 0, 0),
                ),
                color: Color.fromARGB(255, 255, 234, 234),
                borderRadius: BorderRadius.circular(20)),
          ),
          SizedBox(height: 22),
          Text(
            'Bộ phận CSKH của Findnear sẽ hỗ trợ bạn mua mã ID nhanh nhất',
            style: TextStyle(fontSize: 14),
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }

  Widget _buildContactTabPage() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 18,
        ),
        Text(
          'Hỗ trợ trực tuyến',
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: 11,
        ),
        ListView.separated(
          padding: EdgeInsets.symmetric(vertical: 5),
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          primary: false,
          itemCount: _con.contacts.length,
          separatorBuilder: (context, index) {
            return SizedBox(height: 13);
          },
          itemBuilder: (context, indexContact) {
            return ContactWidget(
                contact: _con.contacts.elementAt(indexContact));
          },
        ),
      ],
    );
  }

  Widget _buildFaqTabPage(int index) {
    return ListView.separated(
      padding: EdgeInsets.symmetric(vertical: 5),
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      primary: false,
      itemCount: _con.faqs.elementAt(index).faqs.length,
      separatorBuilder: (context, index) {
        return Divider(height: 1, color: Colors.black38);
      },
      itemBuilder: (context, indexFaq) {
        return FaqItemWidget(
            faq: _con.faqs.elementAt(index).faqs.elementAt(indexFaq));
      },
    );
  }
}
