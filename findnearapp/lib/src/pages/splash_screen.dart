import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:findnear/main.dart';
import 'package:findnear/src/global/global_event.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/pages/videocall/callsignaling.dart';
import 'package:findnear/src/pages/videocall/view.dart';
import 'package:findnear/src/pages/voice_call_v2/voice_call_screenv2.dart';
import 'package:findnear/src/services/websocket_service.dart';
import 'package:findnear/src/utils/listener_event_callkit.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';
import 'package:get/get.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:rxdart/rxdart.dart' as RxDart;

import '../controllers/splash_screen_controller.dart';

class SplashScreen extends StatefulWidget {
  static const String ROUTE_NAME = '/Splash';

  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends StateMVC<SplashScreen> {
  SplashScreenController _con;
  final eventCallKit = ListenerEventCallKit();

  StreamSubscription receiveOfferCallSub;

  SplashScreenState() : super(SplashScreenController()) {
    _con = controller;
  }

  int start = 0;

  @override
  void initState() {
    start = DateTime.now().millisecondsSinceEpoch;
    super.initState();
    DialogHelper.initFToast();
    RxDart.Rx.zip9(
            Stream.fromFuture(_con.initSettings()),
            Stream.fromFuture(_con.getCurrentLocation()),
            Stream.fromFuture(_con.getCurrentUser()),
            Stream.fromFuture(_con.loadData()),
            Stream.fromFuture(_con.loadLocalAddress()),
            Stream.fromFuture(_con.loadMarketCategory()),
            Stream.fromFuture(_con.loadLocalShopField()),
            Stream.fromFuture(_con.checkSpecialPermissionForCall()),
            Stream.fromFuture(_con.saveCurrentLocation()),
            (a, b, c, d, e, f, g, h, i) => [a, b, c, d, e, f, g, h, i])
        .first
        .then((value) => _con.navigateToPage());

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      receiveOfferCallSub?.cancel();
      receiveOfferCallSub = GlobalEvent.instance.onReceiveOfferCall.stream.listen((data) async {
        if(data.typeCall == CallData.video.value){
          eventCallKit.showCallVideoOverlay(context, data);
        }else{
          eventCallKit.showCallVoiceOverlay(context, data);
        }
      });

      if (Platform.isAndroid) {
        try {
          final id = (await LocalDataManager.init()).getIncomingCallIdFcm();
          final activeCallsStr = await FlutterCallkitIncoming.activeCalls();
          final activeCalls = (jsonDecode(activeCallsStr)) as List;
          var isAcceptCall = false;
          var userMakeCall = "";
          var typeCall = "";

          for (final call in activeCalls) {
            if (call["id"] == id) {
              isAcceptCall = true;
              userMakeCall = call["handle"];
              typeCall = call["type"] == 1 ? "VIDEO_CALL" : "VOICE_CALL";
              break;
            }
          }

          if (isAcceptCall) {
            FlutterCallkitIncoming.endAllCalls();
            eventCallKit.pushNotiReady(userMakeCall, typeCall);
          }
        } catch (e) {
          print("NamNH - main - error: $e");
        }
      }
    });
  }

  @override
  void dispose() {
    receiveOfferCallSub?.cancel();
    super.dispose();
  }

  // void loadData() {
  //   _con.shouldShowForceUpdatePopup.addListener(() {
  //     if (_con.shouldShowForceUpdatePopup.value)
  //       DialogHelper.showForceUpdatePopup();
  //   });
  //   _con.progress.addListener(() {
  //     double progress = 0;
  //     _con.progress.value.values.forEach((_progress) {
  //       progress += _progress;
  //     });
  //     if (progress == 100 && !_con.shouldShowForceUpdatePopup.value) {
  //       _con.loadLocalAddress();
  //     }
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      body: Container(
        decoration: BoxDecoration(
          color: Color.fromRGBO(255, 181, 181, 1),
        ),
        child: Stack(
          children: [
            Positioned.fill(
              child: Image.asset(
                "assets/img/bg.png",
                fit: BoxFit.fill,
              ),
            ),
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              child: Container(
                width: double.infinity,
                height: Get.width,
                child: Image.asset(
                  "assets/img/bg_top.png",
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Positioned(
              top: 208,
              left: 0,
              right: 0,
              child: Center(
                child: Image.asset(
                  "assets/img/bg_center.png",
                  fit: BoxFit.fill,
                  width: 328,
                  height: 188,
                ),
              ),
            ),
            Positioned(
              top: 208,
              left: 0,
              right: 0,
              child: Container(
                alignment: Alignment.center,
                child: Image.asset(
                  'assets/img/logo.png',
                  width: 140,
                  height: 140,
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Positioned(
              top: 420,
              left: 0,
              right: 0,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    S.of(context).app_name,
                    style: TextStyle(
                      fontSize: 24,
                      color: Color.fromRGBO(51, 51, 51, 1),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(S.of(context).app_slogan,
                      style: TextStyle(
                        fontSize: 16,
                        color: Color.fromRGBO(51, 51, 51, 1),
                      )),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
