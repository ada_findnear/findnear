import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/enums/pop_enums.dart';
import 'package:findnear/src/models/response/live_token_reponse.dart';
import 'package:findnear/src/repository/livestream_repository.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:get/get.dart';
import 'package:rxdart/rxdart.dart' as RxDart;
import 'list_livestream_state.dart';

class ListLivestreamLogic extends GetxController {

  final ListLivestreamState state = ListLivestreamState();
  LiveStreamRepository livestreamRepository = Get.find<LiveStreamRepository>();

  @override
  ListLivestreamState createState() => ListLivestreamState();

  Future<void> getTopLivestreamShop() async {
    state.fetchTopLSStatus.value = LoadStatus.loading;
    try {
      final response = await livestreamRepository.getListLiveStream();
      state.topLivestreams.value = []..addAll(response?.data ?? []);
      state.fetchTopLSStatus.value = LoadStatus.success;
    } catch (e){
      state.fetchTopLSStatus.value = LoadStatus.failure;
    }
  }

  Future<void> getLivestreamShop() async {
    state.fetchLSStatus.value = LoadStatus.loading;
    try {
      final response = await livestreamRepository.getListLiveStream();
      state.livestreams.value = []..addAll(response?.data ?? []);
      state.fetchLSStatus.value = LoadStatus.success;
    } catch (e){
      state.fetchLSStatus.value = LoadStatus.failure;
    }
  }

  Future<void> refreshData() async {
    RxDart.Rx.zip2(
        Stream.fromFuture(getTopLivestreamShop()),
        Stream.fromFuture(getLivestreamShop()),
            (a, b) =>
        [a, b]).first.then((value) {
      state.refreshController.refreshCompleted();
    });
  }

  void getTokenAudience(int broadcasterId) async{
    DialogHelper.showLoaderDialog();
    RxDart.Rx.zip2(
        Stream.fromFuture(livestreamRepository.getTokenAudience(broadcasterId)),
        Stream.fromFuture(livestreamRepository.getRTMToken()),
            (broadcastToken, rtmToken) =>
        [broadcastToken, rtmToken]).first.then((value) {
      LiveTokenResponse token = value[0];
      LiveTokenResponse rtmToken = value[1];
      if(token.success && rtmToken.success){
        NavigatorUtils.navigateToAudienceLivestreamWidget(token.data, broadcasterId, rtmToken.data, PopTarget.livestreamList.index);
      } else{
        AppSnackbar.showInfo(message: "Vào xem không thành công!");
      }
    },
    );
    Get.back();
  }

}
