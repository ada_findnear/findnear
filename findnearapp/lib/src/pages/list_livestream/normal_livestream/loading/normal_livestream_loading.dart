import 'package:findnear/src/widgets/shimmer/app_shimmer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NormalLivestreamLoading extends StatelessWidget {
  NormalLivestreamLoading();

  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Flexible(child: _getLiveListWidget()),
        ],
      ),
    );
  }

  Widget _getLiveListWidget() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: GridView.count(
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        crossAxisCount: 2,
        childAspectRatio: 2/3,
        children: List.generate(4, (index) {
          return AppShimmer(cornerRadius: 10);
        }),
      ),
    );
  }

}
