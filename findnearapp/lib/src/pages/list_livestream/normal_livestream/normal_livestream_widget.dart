import 'dart:ui';

import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/pages/list_livestream/item/normal_livestream_item_widget.dart';
import 'package:findnear/src/pages/list_livestream/item/top_livestream_item_widget.dart';
import 'package:findnear/src/pages/list_livestream/list_livestream_logic.dart';
import 'package:findnear/src/pages/list_livestream/list_livestream_state.dart';
import 'package:findnear/src/pages/list_livestream/normal_livestream/loading/normal_livestream_loading.dart';
import 'package:findnear/src/pages/list_livestream/presenter/livestream_item_presenter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NormalLivestreamWidget extends StatefulWidget {
  Function onItemClick;

  NormalLivestreamWidget(this.onItemClick);

  @override
  _NormalLivestreamWidgetState createState() => _NormalLivestreamWidgetState();
}

class _NormalLivestreamWidgetState extends State<NormalLivestreamWidget> {
  final ListLivestreamLogic logic = Get.find<ListLivestreamLogic>();
  final ListLivestreamState state = Get.find<ListLivestreamLogic>().state;

  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Obx(() {
        if(state.fetchLSStatus.value == LoadStatus.loading)
          return NormalLivestreamLoading();
        return Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (state.livestreams.value.length > 0)
              Flexible(child: _getLiveListWidget()),
          ],
        );
      }),
    );
  }

  Widget _getLiveListWidget() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: GridView.count(
        // shrinkWrap: true,
        // primary: false,
        // padding: EdgeInsets.zero,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        crossAxisCount: 2,
        childAspectRatio: 2/3,
        children: List.generate(state.livestreams.length, (index) {
          return NormalLivestreamItemWidget(
              LivestreamItemPresenter.fromLSInfo(
                  state.livestreams.value[index]),
              widget.onItemClick
          );
        }),
      ),
    );
  }

  @override
  ListLivestreamLogic createController() => ListLivestreamLogic();
}
