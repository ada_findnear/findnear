import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/entities/livestream_info_entity.dart';
import 'package:findnear/src/pages/list_livestream/presenter/livestream_item_presenter.dart';
import 'package:findnear/src/pages/livestream/component/ls_circle_avatar.dart';
import 'package:findnear/src/pages/livestream/component/ls_gradient_header.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:findnear/src/helpers/app_config.dart' as config;

class NormalLivestreamItemWidget extends StatefulWidget {
  LivestreamItemPresenter liveInfo;
  Function onClick;

  NormalLivestreamItemWidget(this.liveInfo, this.onClick);

  @override
  _NormalLivestreamItemWidgetState createState() => _NormalLivestreamItemWidgetState();
}

class _NormalLivestreamItemWidgetState extends State<NormalLivestreamItemWidget> {
  Widget build(BuildContext context) {
    return _getLivestreamItem(widget.liveInfo);
  }

  Widget _getLivestreamItem(LivestreamItemPresenter liveInfo) {
    return Stack(
      children: [
        InkWell(
          onTap: () {
            widget.onClick(liveInfo);
          },
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              image: DecorationImage(
                image: CachedNetworkImageProvider(liveInfo.liverBanner),
                fit: BoxFit.cover,
              ),
            ),
            child: Stack(
              children: [
                _buildFooterGradient(),
                Positioned.fill(
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: _buildFooter(),
                  ),
                ),
                Positioned.fill(
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: _buildViewers(),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildFooter() {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Flexible(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(width: 4),
                LSCircleAvatar(
                  width: 19,
                  height: 19,
                  url: widget.liveInfo.liverAvatar,
                ),
                SizedBox(width: 4),
                Expanded(
                  child: Stack(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: widget.liveInfo.hasCoinClaim ? 35 : 0),
                        child: Text(
                          widget.liveInfo.liverName,
                          style: TextStyle(
                              fontFamily: "Quicksand",
                              fontSize: 12,
                              color: AppColors.white,
                              fontWeight: FontWeight.bold),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                        ),
                      ),
                      Positioned.fill(
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: Visibility(
                              visible: widget.liveInfo.hasCoinClaim,
                              child: _buildClaimCoin()),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 4),
          Flexible(
            child: Container(
              padding: EdgeInsets.only(left: 4, right: 4, bottom: 4),
              child: Text(
                widget.liveInfo.caption.toUpperCase(),
                style: TextStyle(
                    fontFamily: "Quicksand",
                    fontSize: 10,
                    color: AppColors.white,
                    fontWeight: FontWeight.normal),
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildViewers() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 4, vertical: 4),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            width: 33,
            height: 15,
            decoration: BoxDecoration(
              color: config.CustomColors().mainColor(1),
              borderRadius: BorderRadius.circular(3),
            ),
            padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 2),
            child: Text(
              "LIVE",
              style: Theme.of(context).textTheme.headline6.merge(TextStyle(
                color: AppColors.white,
                fontSize: 8,
              )),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(width: 2),
          Flexible(
            child: Container(
              height: 15,
              padding: EdgeInsets.symmetric(horizontal: 2, vertical: 1),
              decoration: BoxDecoration(
                color: AppColors.black.withOpacity(0.5),
                borderRadius: BorderRadius.circular(5),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(width: 2),
                  Image.asset(AppImages.icEye, width: 10.99, height: 6.42, color: Colors.white,),
                  SizedBox(width: 2),
                  Flexible(
                    child: Text(
                      "${widget.liveInfo.viewerCount}",
                      style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: Colors.white,
                          fontSize: 10,
                          fontWeight: FontWeight.normal),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildFooterGradient() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Flexible(
            child: Container(
              height: 65,
              child: LSGradientHeader(
                height: 65,
                start: Colors.transparent,
                end: Colors.black,
                hasRadius: true,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildClaimCoin(){
    return Container(
      height: 15,
      width: 35,
      padding: EdgeInsets.symmetric(horizontal: 2, vertical: 1),
      decoration: BoxDecoration(
        color: AppColors.red.withOpacity(0.5),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(60),
            bottomLeft: Radius.circular(60),
          ),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(width: 2),
          Image.asset(AppImages.icLsCoin, width: 8.74, height: 8.74),
          SizedBox(width: 2),
          Flexible(
            child: Text(
              "+100",
              style: TextStyle(
                  fontFamily: 'Quicksand',
                  color: Colors.white,
                  fontSize: 6,
                  fontWeight: FontWeight.bold),
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
            ),
          ),
        ],
      ),
    );
  }
}