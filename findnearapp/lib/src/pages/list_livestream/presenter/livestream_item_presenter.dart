import 'package:findnear/src/models/entities/livestream_info_entity.dart';
import 'package:global_configuration/global_configuration.dart';

class LivestreamItemPresenter {
  int id;
  int userId;
  String liverBanner;
  String liverDescription;
  String liverName;
  String liverAvatar;
  String liverCode;
  String caption;
  bool hasCoinClaim;
  String viewerCount;

  LivestreamItemPresenter({
    this.id,
    this.userId,
    this.liverBanner,
    this.liverDescription,
    this.liverName,
    this.liverAvatar,
    this.liverCode,
    this.caption,
    this.hasCoinClaim,
    this.viewerCount,
  });

  static LivestreamItemPresenter fromLSInfo(LiveStreamInfoEntity info) {
    String liverBanner = "https://t3.ftcdn.net/jpg/03/32/16/46/360_F_332164697_q0b6DnjSfpSvMwoyJMrBzUoMxgjQEkIu.jpg";
    String viewerCount = info.number_viewer.toString();
    String name = info.user.name ?? "";
    String code = info.user.code ?? "";
    int id = info.id;
    int userId = info.user_id;
    String avatar = info.user.image.thumb ??
        "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
    String description = info.description;

    return LivestreamItemPresenter(
      id: id,
      userId: userId,
      liverBanner: liverBanner,
      liverDescription: description,
      liverName: name,
      liverAvatar: avatar,
      liverCode: code,
      caption: description,
      hasCoinClaim: true,
      viewerCount: viewerCount,
    );
  }
}