import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/pages/list_livestream/list_livestream_state.dart';
import 'package:findnear/src/pages/list_livestream/presenter/livestream_item_presenter.dart';
import 'package:findnear/src/pages/list_livestream/top_livestream/top_livestream_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart' as refresh;
import 'list_livestream_logic.dart';
import 'normal_livestream/normal_livestream_widget.dart';

class ListLivestreamPage extends StatefulWidget {
  static const String ROUTE_NAME = '/ListLivestreamPage';

  @override
  State<ListLivestreamPage> createState() => _ListLivestreamPageState();
}

class _ListLivestreamPageState extends State<ListLivestreamPage> {
  final ListLivestreamLogic logic = Get.put(ListLivestreamLogic());
  final ListLivestreamState state = Get
      .find<ListLivestreamLogic>()
      .state;

  @override
  void initState() {
    super.initState();
    logic.refreshData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0,
        centerTitle: false,
        title: _buildHeader(),
      ),
      floatingActionButton: Builder(
        builder: (context) =>
            FloatingActionButton(
              onPressed: () {
                /// todo
              },
              backgroundColor: Color(0xffFF0000),
              child: Image.asset(AppImages.icLsCreateLivestream),
            ),
      ),
      body: _buildBodyWidget(),
    );
  }

  Widget _buildBodyWidget() {
    state.refreshController = refresh.RefreshController();
    return refresh.SmartRefresher(
      controller: state.refreshController,
      // onLoading: () => logic.refreshData(),
      onRefresh: () => logic.refreshData(),
      enablePullUp: false,
      enablePullDown: true,
      child: Column(
        children: [
          TopLivestreamWidget(
            "Top Livestream",
            (LivestreamItemPresenter liveInfo) {
              logic.getTokenAudience(liveInfo.userId);
            },
          ),
          Expanded(
            child: NormalLivestreamWidget(
               (LivestreamItemPresenter liveInfo) {
                 logic.getTokenAudience(liveInfo.userId);
               },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildHeader() {
    return Container(
      child: Stack(
        children: [
          Container(
            padding: EdgeInsets.only(top: 4, right: 8),
            child: Text(
              "Findnear Live",
              style: TextStyle(
                color: Color(0xff000000),
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Positioned.fill(
            child: Align(
              alignment: Alignment.topRight,
              child: Image.asset(
                AppImages.icLsBroadcast,
                fit: BoxFit.none,
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  ListLivestreamLogic createController() => ListLivestreamLogic();
}
