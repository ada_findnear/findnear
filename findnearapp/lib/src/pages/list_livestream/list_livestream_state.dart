import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/entities/live_token_entity.dart';
import 'package:findnear/src/models/entities/livestream_info_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart' as refresh;

class ListLivestreamState{
  ListLivestreamState() {
    ///Initialize variables
  }
  refresh.RefreshController refreshController = refresh.RefreshController();
  final topLivestreams = List<LiveStreamInfoEntity>.empty().obs;
  final livestreams = List<LiveStreamInfoEntity>.empty().obs;
  Rx<LoadStatus> fetchTopLSStatus = LoadStatus.initial.obs;
  Rx<LoadStatus> fetchLSStatus = LoadStatus.initial.obs;
}
