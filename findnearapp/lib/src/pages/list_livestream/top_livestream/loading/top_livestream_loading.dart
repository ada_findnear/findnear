import 'dart:ui';

import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/pages/list_livestream/item/top_livestream_item_widget.dart';
import 'package:findnear/src/pages/list_livestream/list_livestream_logic.dart';
import 'package:findnear/src/pages/list_livestream/list_livestream_state.dart';
import 'package:findnear/src/pages/list_livestream/presenter/livestream_item_presenter.dart';
import 'package:findnear/src/widgets/shimmer/app_shimmer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TopLivestreamLoading extends StatelessWidget {
  TopLivestreamLoading();

  Widget build(BuildContext context) {
    return Container(
      color: Color(0xffFFEAEA),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Flexible(child: _getHeaderWidget()),
          Flexible(child: _getLiveListWidget()),
        ],
      ),
    );
  }

  Widget _getHeaderWidget() {
    return Container(
      padding: EdgeInsets.all(10),
      child: Row(
        children: [
          Image.asset(
            AppImages.icLsFlame,
            fit: BoxFit.none,
          ),
          SizedBox(width: 4),
          Text(
            "Top Livestream",
            style: TextStyle(
              color: Color(0xff000000),
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }

  Widget _getLiveListWidget() {
    return Container(
      width: double.infinity,
      height: 100,
      padding: EdgeInsets.only(left: 10, bottom: 10),
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: 6,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: EdgeInsets.only(right: 10),
            child: AppShimmer(width: 87, height: 87, cornerRadius: 10),
          );
        },
      ),
    );
  }
}
