import 'dart:ui';

import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/pages/list_livestream/item/top_livestream_item_widget.dart';
import 'package:findnear/src/pages/list_livestream/list_livestream_logic.dart';
import 'package:findnear/src/pages/list_livestream/list_livestream_state.dart';
import 'package:findnear/src/pages/list_livestream/presenter/livestream_item_presenter.dart';
import 'package:findnear/src/pages/list_livestream/top_livestream/loading/top_livestream_loading.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TopLivestreamWidget extends StatefulWidget {
  String title;
  Function onItemClick;

  TopLivestreamWidget(this.title, this.onItemClick);

  @override
  _TopLivestreamWidgetState createState() => _TopLivestreamWidgetState();
}

class _TopLivestreamWidgetState extends State<TopLivestreamWidget> {
  final ListLivestreamLogic logic = Get.find<ListLivestreamLogic>();
  final ListLivestreamState state = Get.find<ListLivestreamLogic>().state;

  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    return Container(
      color: Color(0xffFFEAEA),
      child: Obx(() {
        if(state.fetchTopLSStatus.value == LoadStatus.loading)
          return TopLivestreamLoading();
        return Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Flexible(child: _getHeaderWidget()),
            if (state.topLivestreams.value.length > 0)
              Flexible(child: _getLiveListWidget()),
          ],
        );
      }),
    );
  }

  Widget _getHeaderWidget() {
    return Container(
      padding: EdgeInsets.all(10),
      child: Row(
        children: [
          Image.asset(
            AppImages.icLsFlame,
            fit: BoxFit.none,
          ),
          SizedBox(width: 4),
          Text(
            widget.title,
            style: TextStyle(
              color: Color(0xff000000),
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }

  Widget _getLiveListWidget() {
    return Container(
      width: double.infinity,
      height: 100,
      padding: EdgeInsets.only(left: 10),
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: state.topLivestreams.value.length,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: EdgeInsets.only(right: 10),
            child:
                // Text(index.toString()),
                TopLivestreamItemWidget(
                    LivestreamItemPresenter.fromLSInfo(
                        state.topLivestreams.value[index]),
                    widget.onItemClick),
          );
        },
      ),
    );
  }

  @override
  ListLivestreamLogic createController() => ListLivestreamLogic();
}
