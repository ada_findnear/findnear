import 'dart:async';

import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/city.dart';
import 'package:findnear/src/models/district.dart';
import 'package:findnear/src/models/entities/place_entity.dart';
import 'package:findnear/src/models/field.dart';
import 'package:findnear/src/models/shop.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/models/ward.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class RegisterShopSState extends BaseState {
  RegisterShopSState();

  TextEditingController shopNameController = TextEditingController(text: "");
  // TextEditingController streetController = TextEditingController(text: "");
  TextEditingController descriptionController = TextEditingController(
    text: "",
  );
  TextEditingController addressController = TextEditingController(text: "");

  ValueNotifier<User> get user => currentUser;

  TextEditingController dropdownController = TextEditingController(text: "");

  List<Field> business;
  Rx<Field> selectedBusiness = Field().obs;

  String get currentUserProfessId => user.value.field.id;

  // RxList<City> cityListRx = <City>[].obs;
  // Rx<City> selectedCity = City().obs;

  // RxList<District> districtListRx = <District>[].obs;
  // Rx<District> selectedDistrict = District().obs;

  // RxList<Ward> wardListRx = <Ward>[].obs;
  // Rx<Ward> selectedWard = Ward().obs;

  Rx<Shop> shop = Shop().obs;
  RxString selectedJobType = "".obs;

  RxString shopAvatar = "".obs;
  String title = "Đăng ký gian hàng";
  String btnText = "Lưu";
  final formKey = GlobalKey<FormState>();
  bool isCreateNew = true; // this screen can use for create and update

  Completer<GoogleMapController> mapController;
  final isLoadAddress = false.obs;

  Rxn<CameraPosition> cameraPosition = Rxn<CameraPosition>();

  RxDouble latPost = 0.0.obs;
  RxDouble lngPost = 0.0.obs;

  RxList<AddressEntity> placeResult = <AddressEntity>[].obs;

  PlaceEntity placeEntity = PlaceEntity();

  List<Marker> allMarkers = <Marker>[];

}
