import 'dart:convert';

import 'package:dio/dio.dart' as dio;
import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/commons/app_configs.dart';
import 'package:findnear/src/models/district.dart';
import 'package:findnear/src/models/entities/place_entity.dart';
import 'package:findnear/src/models/field.dart';
import 'package:findnear/src/models/my_shop.dart';
import 'package:findnear/src/models/response/my_shop_response.dart';
import 'package:findnear/src/models/shop.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/models/ward.dart';
import 'package:findnear/src/pages/register_shop_page/register_shop_state.dart';
import 'package:findnear/src/pages/shop_detail/logic.dart';
import 'package:findnear/src/repository/find_near_repository.dart';
import 'package:findnear/src/repository/register_shop_repository.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/services/cache_service.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;

import '../../repository/settings_repository.dart' as sett;
import '../../settings/global_variable.dart' as GlobalVar;
import '../../utils/utils.dart';

class RegisterShopSController extends BaseController<RegisterShopSState> {
  RegisterShopRepository registerShopRepository;

  FindNearRepository findNearRepository;

  RegisterShopSController(
      {this.registerShopRepository, this.findNearRepository});

  final _shopDetailLogic = Get.find<ShopDetailLogic>();

  Shop getMyShop(MyShop myshop) {
    if (myshop == null) {
      return null;
    }
    Shop shop = Shop();
    shop.id = myshop.id;
    shop.title = myshop.title;
    shop.street = myshop.street;
    shop.media = myshop.media;
    shop.field_id = myshop.field_id;
    shop.district = myshop.district;
    shop.ward = myshop.ward;
    shop.city = myshop.city;
    shop.description = myshop.description;
    shop.media = myshop.media;
    shop.latitude = myshop.latitude;
    shop.longitude = myshop.longitude;
    if (myshop.vote != null && myshop.vote != '' && myshop.vote != 'null') {
      shop.vote = double.parse(myshop.vote);
    } else {
      shop.vote = 0.0;
    }
    return shop;
  }

  void init() async {
    if (GlobalVar.current_location == null) {
      GlobalVar.current_location = await Utils.determinePosition();
    }
    state.shop.value = getMyShop(Get.find<LocalDataManager>().getMyShop());
    state.isCreateNew = state.shop.value == null;
    if (state.shop.value != null) {
      state.shopNameController =
          TextEditingController(text: state.shop.value.title);
      // state.streetController =
      //     TextEditingController(text: state.shop.value.street);
      state.descriptionController =
          TextEditingController(text: state.shop.value.description);
      if (state.shop.value.media?.isNotEmpty == true)
        state.shopAvatar.value = state.shop.value.media[0].url;
      else
        state.shopAvatar.value =
            "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
      state.title = "Chỉnh sửa gian hàng";

      if (state.shop.value.latitude == null ||
          state.shop.value.longitude == null) {
        try {
          var currentAddress = sett.deliveryAddress.value;
          if (currentAddress.isUnknown()) {
            state.cameraPosition.value = CameraPosition(
              target: LatLng(21.029832, 105.852605),
              zoom: 18,
            );

            state.latPost.value = 21.029832;
            state.lngPost.value = 105.852605;
          } else {
            state.latPost.value = currentAddress.latitude;
            state.lngPost.value = currentAddress.longitude;
            state.cameraPosition.value = CameraPosition(
              target: LatLng(currentAddress.latitude, currentAddress.longitude),
              zoom: 18,
            );
          }
          if (!currentAddress.isUnknown()) {
            state.latPost.value = currentAddress.latitude;
            state.lngPost.value = currentAddress.longitude;
            state.cameraPosition.value = CameraPosition(
              target: LatLng(currentAddress.latitude, currentAddress.longitude),
              zoom: 18,
            );
          }
        } on PlatformException catch (e) {
          if (e.code == 'PERMISSION_DENIED') {
            print('Permission denied');
          }
        }
      } else {
        await searchAddressFromLatLng(LatLng(
            double.parse(state.shop.value.latitude),
            double.parse(state.shop.value.longitude)));
        state.addressController.text =
            state.placeResult.value[0].formattedAddress;

        state.latPost.value = double.parse(state.shop.value.latitude);
        state.lngPost.value = double.parse(state.shop.value.longitude);

        state.cameraPosition.value = CameraPosition(
          target: LatLng(double.parse(state.shop.value.latitude),
              double.parse(state.shop.value.longitude)),
          zoom: 18,
        );
        addMarker(LatLng(double.parse(state.shop.value.latitude),
            double.parse(state.shop.value.longitude)));
      }
    } else {
      var currentAddress = sett.deliveryAddress.value;
      if (currentAddress.isUnknown()) {
        state.latPost.value = 21.029832;
        state.lngPost.value = 105.852605;
        state.cameraPosition.value = CameraPosition(
          target: LatLng(21.029832, 105.852605),
          zoom: 18,
        );
        addMarker(LatLng(21.029832, 105.852605));
      } else {
        state.latPost.value = currentAddress.latitude;
        state.lngPost.value = currentAddress.longitude;
        state.cameraPosition.value = CameraPosition(
          target: LatLng(currentAddress.latitude, currentAddress.longitude),
          zoom: 18,
        );
        addMarker(LatLng(currentAddress.latitude, currentAddress.longitude));
      }
    }
    getData();
  }

  void addMarker(LatLng latlng) {
    state.allMarkers.add(Marker(
      markerId: MarkerId('place_name'),
      position: latlng,
    ));
  }

  Future<void> getData() async {
    final cacheService = Get.find<CacheService>();
    List<Field> response = await cacheService.fields.value;
    state.business = response;
    if (state.shop.value != null &&
        state.shop.value.field_id != null &&
        state.business != null &&
        state.business.isNotEmpty) {
      var fieldId = state.business
          .where((i) => i.id == state.shop.value.field_id.toString());
      if (fieldId != null && fieldId.isNotEmpty)
        state.selectedBusiness.value = fieldId.first;
    }
    state.isLoading = false;
  }

  Future<bool> registerShop(Function showMessage) async {
    DialogHelper.showLoaderDialog();
    User _currentUserLogin = currentUser.value;
    http.Response response = await registerShopRepository.registerShop(
        state.shopNameController.text,
        _currentUserLogin.id,
      "","","","",
        state.descriptionController.text,
        state.shopAvatar.value,
        state.selectedJobType.value,
        state.latPost.value,
        state.lngPost.value,
        //    pathMedia
        );
    Get.back();
    if (response.statusCode == 200) {
      state.shop.value = Shop.fromJson(jsonDecode(response.body));
      MyShopResponse myShopResponse =
          MyShopResponse.fromJson(jsonDecode(response.body));
      await Get.find<LocalDataManager>().saveMyShop(myShopResponse.data);
      _shopDetailLogic.getShopDetail(shopId: myShopResponse.data.id);
      return true;
    }
    return false;
  }

  void uploadAvatar(String path) async {
    if (path != null && path.isNotEmpty) {
      state.shopAvatar.value = path;
    }
  }

  bool validate(Function showMessage) {
    bool result = state.formKey.currentState?.validate();
    if (!result) {
      return result;
    }

    if (state.selectedJobType.value == null) {
      showMessage("Xin chọn loại hình kinh doanh");
      return false;
    }

    if (state.shopAvatar.value == null || state.shopAvatar.value.isEmpty) {
      showMessage("Xin chọn hình ảnh cho shop của bạn");
      return false;
    }

    return true;
  }

  String validatorDescription(String value) {
    return '';
  }

  String getTitle() {
    return state.title;
  }

  String getBtnText() {
    return state.btnText;
  }

  String getSelectBusinessText() {
    if (state.selectedBusiness.value.name != null) {
      return state.selectedBusiness.value.name;
    }
    return 'Loại hình kinh doanh';
  }

  void getUserLocation() async {
    DialogHelper.showLoaderDialog();
    final GoogleMapController controller = await state.mapController.future;

    var position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    await searchAddressFromLatLng(
        LatLng(position.latitude, position.longitude));
    state.addressController.text = state.placeResult.value[0].formattedAddress;

    state.latPost.value = position.latitude;
    state.lngPost.value = position.longitude;

    state.allMarkers.add(Marker(
      markerId: MarkerId('place_name'),
      position: LatLng(
        position.latitude,
        position.longitude,
      ),
    ));

    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
      target: LatLng(position.latitude, position.longitude),
      zoom: 18,
    )));

    Get.back();
  }

  void searchAddressFromLatLng(LatLng latlng) async {
    dio.Response response;
    var dioClient = dio.Dio(); // with default Options
    dioClient.options.baseUrl = 'https://maps.googleapis.com';
    dioClient.options.connectTimeout = 30000; //30s
    dioClient.options.receiveTimeout = 60000; //60s
    try {
      response =
          await dioClient.get('/maps/api/geocode/json', queryParameters: {
        "latlng": "${latlng.latitude},${latlng.longitude}",
        "key": "AIzaSyBtaQfYohHQE01th0fmXvKLG-DvIvcBhrI",
      });
      final data = response.data as Map<String, dynamic>;
      final placeList = AddressList.fromJson(data);
      state.placeResult.value = placeList?.results ?? [];
      state.placeResult.refresh();
    } catch (e, s) {
      logger.e(e);
    }
  }

  void getAddressInTapMap(LatLng position) async {
    addMarker(position);
    await searchAddressFromLatLng(position);
    state.addressController.text = state.placeResult.value[0].formattedAddress;
  }

  void getLatLngFromPlaceId(String placeId) async {
    dio.Response response;
    var dioClient = dio.Dio(); // with default Options
    dioClient.options.baseUrl = 'https://maps.googleapis.com';
    dioClient.options.connectTimeout = 30000; //30s
    dioClient.options.receiveTimeout = 60000; //60s
    try {
      response =
          await dioClient.get('/maps/api/place/details/json', queryParameters: {
        "placeid": placeId,
        "key": AppConfigs.googleAPI,
      });
      final data = response.data as Map<String, dynamic>;
      final place = PlaceAutoCompleteEntity.fromJson(data);
      state.placeEntity = place.result ?? PlaceEntity;

      state.latPost.value = state.placeEntity.geometry.location.lat;
      state.lngPost.value = state.placeEntity.geometry.location.lng;

      addMarker(LatLng(state.placeEntity.geometry.location.lat,
          state.placeEntity.geometry.location.lng));
      final GoogleMapController controller = await state.mapController.future;
      controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(state.placeEntity.geometry.location.lat,
            state.placeEntity.geometry.location.lng),
        zoom: 18,
      )));
    } catch (e, s) {
      logger.e(e);
    }
  }

  @override
  RegisterShopSState createState() => RegisterShopSState();
}
