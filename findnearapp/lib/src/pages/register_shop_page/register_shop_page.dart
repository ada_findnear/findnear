import 'dart:async';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_configs.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/commons/app_shadows.dart';
import 'package:findnear/src/elements/CircularLoadingWidget.dart';
import 'package:findnear/src/elements/MediaActionSheetWidget.dart';
import 'package:findnear/src/models/city.dart';
import 'package:findnear/src/models/district.dart';
import 'package:findnear/src/models/entities/place_entity.dart';
import 'package:findnear/src/models/field.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/models/ward.dart';
import 'package:findnear/src/pages/register_shop_page/register_shop_controller.dart';
import 'package:findnear/src/pages/register_shop_page/register_shop_state.dart';
import 'package:findnear/src/pages/search/auto_complete/search_auto_complete.dart';
import 'package:findnear/src/repository/find_near_repository.dart';
import 'package:findnear/src/repository/register_shop_repository.dart';
import 'package:findnear/src/services/cache_service.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:google_api_headers/google_api_headers.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:image_picker/image_picker.dart';
import 'package:select_form_field/select_form_field.dart';

import '../settings.dart';
import 'loading/register_shop_loading_widget.dart';

class RegisterShopPage extends StatefulWidget {
  static const String ROUTE_NAME = '/RegisterShopPage';

  RegisterShopPage();

  @override
  _RegisterShopState createState() => _RegisterShopState();
}

class _RegisterShopState extends BaseWidgetState<
    RegisterShopPage,
    RegisterShopSController,
    RegisterShopSState> /*with AutomaticKeepAliveClientMixin*/ {
  double topMargirn = 32;
  MediaActionSheetWidget _mediaActionSheetWidget;

  Completer<GoogleMapController> mapController = Completer();
  GoogleMapController mGoogleMapController;

  @override
  void initState() {
    super.initState();
    controller.init();
    state.mapController = mapController;
    _mediaActionSheetWidget = MediaActionSheetWidget(context: context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _getAppBar(),
      body: _getBody(),
    );
  }

  AppBar _getAppBar() {
    return AppBar(title: _getTitleWidget());
  }

  Widget _getTitleWidget() {
    return Row(
      children: [
        Text(
          controller.getTitle(),
          style: TextStyle(
            fontFamily: 'Quicksand',
            fontSize: 16,
            color: const Color(0xff000000),
            fontWeight: FontWeight.w700,
          ),
        ),
        Spacer(),
        ElevatedButton(
          onPressed: () {
            if (!state.isLoading) registerShop();
          },
          child: Text(
            controller.getBtnText(),
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 14,
              color: const Color(0xffffffff),
              fontWeight: FontWeight.w700,
            ),
          ),
          style: ElevatedButton.styleFrom(
              shape: StadiumBorder(), primary: Colors.red),
        ),
      ],
    );
  }

  registerShop() async {
    if (controller.validate(registerShowMessage)) {
      bool result = await controller.registerShop(registerShowMessage);
      if (result) {
        DialogHelper.showAlertDialog(
          context: context,
          title: '',
          description: () {
            if (state.isCreateNew)
              return 'Đăng ký gian hàng thành công';
            else
              return 'Chỉnh sửa gian hàng thành công';
          }(),
          okText: 'OK',
          okFunction: registerShopSuccessful,
        );
      } else {
        DialogHelper.showAlertDialog(
          context: context,
          title: '',
          description: 'Chưa lưu được, vui lòng thử lại',
          okText: 'OK',
          okFunction: () {},
        );
      }
    }
  }

  registerShopSuccessful() async {
    Get.back(
        result: controller.getMyShop(Get.find<LocalDataManager>().getMyShop()));
  }

  registerShowMessage(String message) {
    DialogHelper.showAlertDialog(
      context: context,
      title: '',
      description: message,
      okText: 'OK',
      okFunction: () {},
    );
  }

  Widget _getBody() {
    return Obx(() => state.isLoading
        ? RegisterShopLoadingWidget()
        : Container(
            padding: EdgeInsets.only(left: 20, top: topMargirn, right: 20),
            child: Form(
                key: state.formKey,
                child: ListView(
                  children: [
                    _buildTitle(S.of(context).shopTypeName),
                    _textFieldValue(
                        state.shopNameController,
                        S.of(context).shopTypeName,
                        S.of(context).shopTypeNameValidate,
                        maxLine: 1),
                    SizedBox(height: 16),
                    ValueListenableBuilder(
                      valueListenable: state.user,
                      builder: (context, User value, child) => () {
                        return _buildJobType(value);
                      }(),
                    ),
                    _getAddress(),
                    SizedBox(height: 16),
                    _textFieldValue(
                      state.addressController,
                      S.of(context).address,
                      '',
                      maxLine: 2,
                      onTap: () {
                        FocusScope.of(context).unfocus();
                        NavigatorUtils.toPlaceCompletePage(callback: (AddressAutoEntity entity){
                          state.addressController.text = entity.description;
                          controller.getLatLngFromPlaceId(entity.placeId);
                        });
                      },
                    ),
                    SizedBox(height: 16),
                    // Row(
                    //   // crossAxisAlignment : CrossAxisAlignment.start,
                    //   children: [
                    //     Expanded(
                    //       child: Column(
                    //         crossAxisAlignment: CrossAxisAlignment.start,
                    //         children: [
                    //           _buildTitle(S.of(context).cityNameTitle),
                    //           _dropDownCity(),
                    //           Container(
                    //             margin: EdgeInsets.only(top: 9),
                    //             height: 1,
                    //             color: Colors.black26,
                    //           )
                    //         ],
                    //       ),
                    //     ),
                    //     SizedBox(width: 16),
                    //     Expanded(
                    //       child: Column(
                    //         crossAxisAlignment: CrossAxisAlignment.start,
                    //         children: [
                    //           _buildTitle(S.of(context).districtNameTitle),
                    //           _dropDownDistrict(),
                    //           Container(
                    //             margin: EdgeInsets.only(top: 9),
                    //             height: 1,
                    //             color: Colors.black26,
                    //           )
                    //         ],
                    //       ),
                    //     ),
                    //   ],
                    // ),
                    // SizedBox(height: 16),
                    // Row(
                    //   mainAxisSize: MainAxisSize.min,
                    //   children: [
                    //     Flexible(
                    //       child: Column(
                    //         crossAxisAlignment: CrossAxisAlignment.start,
                    //         children: [
                    //           _buildTitle(S.of(context).wardNameTitle),
                    //           _dropDownWard(),
                    //           Container(
                    //             margin: EdgeInsets.only(top: 9),
                    //             height: 1,
                    //             color: Colors.black26,
                    //           )
                    //         ],
                    //       ),
                    //     ),
                    //     SizedBox(width: 16),
                    //     Flexible(
                    //       child: Container(
                    //         child: Column(
                    //           crossAxisAlignment: CrossAxisAlignment.start,
                    //           children: [
                    //             _buildTitle(S.of(context).alleyNameTitle),
                    //             _textFieldValue(state.streetController,
                    //                 S.of(context).alleyNameTitle, '',
                    //                 maxLine: 1),
                    //           ],
                    //         ),
                    //       ),
                    //     ),
                    //   ],
                    // ),
                    // SizedBox(height: 32),
                    Container(
                      height: 300,
                      child: _buildMapWidget(),
                      decoration:
                          BoxDecoration(border: Border.all(color: Colors.grey)),
                    ),
                    SizedBox(height: 32),
                    _buildTitle(S.of(context).desNameTitle),
                    _textFieldValue(state.descriptionController,
                        S.of(context).desNameTitle, ''),
                    SizedBox(height: 16),
                    _addShowImage(),
                    SizedBox(height: 16),
                  ],
                ))));
  }

  Widget _buildMapWidget() {
    return Stack(
      children: [
        GoogleMap(
          mapToolbarEnabled: false,
          mapType: MapType.normal,
          buildingsEnabled: false,
          initialCameraPosition: state.cameraPosition.value,
          markers: Set<Marker>.of(state.allMarkers),
          myLocationButtonEnabled: false,
          myLocationEnabled: false,
          zoomControlsEnabled: true,
          onMapCreated: (GoogleMapController controller) {
            mapController.complete(controller);
            this.mGoogleMapController = controller;
          },
          onCameraMove: (CameraPosition cameraPosition) {
            state.cameraPosition.value = cameraPosition;
          },
          onTap: (LatLng position) {
            controller.getAddressInTapMap(position);
            state.latPost.value  = position.latitude;
            state.lngPost.value  = position.longitude;
            mGoogleMapController
                .animateCamera(CameraUpdate.newLatLngZoom(position, 18));
          },
          onCameraIdle: () async {
            print("initial position is : ${state.cameraPosition.value.target}");
          },
          gestureRecognizers: Set()
            ..add(Factory<PanGestureRecognizer>(() => PanGestureRecognizer()))
            ..add(
              Factory<VerticalDragGestureRecognizer>(
                  () => VerticalDragGestureRecognizer()),
            )
            ..add(
              Factory<HorizontalDragGestureRecognizer>(
                  () => HorizontalDragGestureRecognizer()),
            )
            ..add(
              Factory<ScaleGestureRecognizer>(() => ScaleGestureRecognizer()),
            ),
        ),
      ],
    );
  }

  Widget _getAddress() {
    return Container(
      padding: EdgeInsets.only(
        top: topMargirn,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            S.of(context).address,
            style: TextStyle(
              color: Color(0xff333333),
              fontSize: 14,
              fontWeight: FontWeight.bold,
            ),
          ),
          GestureDetector(
            onTap: controller.getUserLocation,
            child: Container(
              width: 30,
              height: 30,
              child: Image.asset(AppImages.icMyLocation, fit: BoxFit.fill),
              decoration: BoxDecoration(
                  boxShadow: AppShadow.boxShadow,
                  borderRadius: BorderRadius.circular(15)),
            ),
          ),
        ],
      ),
    );
  }

  Widget _textFieldValue(
      TextEditingController controller, String hintText, String error,
      {int maxLine, Function onTap}) {
    return Container(
      child: TextFormField(
        style: TextStyle(
          fontFamily: "Quicksand",
          color: AppColors.black,
          fontWeight: FontWeight.normal,
          fontSize: 14,
        ),
        maxLines: maxLine ?? null,
        controller: controller,
        decoration: InputDecoration(
          hintText: hintText,
          hintStyle: TextStyle(color: Color(0xff333333), fontSize: 12),
          // border: UnderlineInputBorder(),
          border: InputBorder.none,
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.black26),
          ),
          contentPadding: EdgeInsets.only(bottom: 12, top: 3),
          isDense: true,
        ),
        onChanged: (String value) {
          state.formKey.currentState?.validate();
          state.formKey.currentState?.save();
        },
        validator: (String value) {
          if ((value == null || value.isEmpty) && error.isNotEmpty) {
            return error;
          }
          return null;
        },
        onTap: onTap,
      ),
    );
  }

  Widget _dropdownIcon() => SizedBox(
        child: Image.asset("assets/img/ic_expand_more_24px.png"),
        width: 24,
        height: 24,
      );

  Widget _dropDownBusiness() {
    return Container(
        padding: EdgeInsets.only(left: 0, top: topMargirn, right: 0),
        child: Obx(
          () => PopupMenuButton<String>(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(children: [
                    _labelValue(controller.getSelectBusinessText()),
                    Spacer(),
                    _dropdownIcon()
                  ]),
                ]),
            itemBuilder: (context) => [
              for (Field item in state.business)
                PopupMenuItem(
                  value: item.indexUI.toString(),
                  child: _labelTitle(item.name),
                ),
            ],
            onSelected: (selected) {
              int index = int.parse(selected);
              state.selectedBusiness.value = state.business[index];
            },
          ),
        ));
  }

  Widget _addShowImage() {
    return Container(
        margin: EdgeInsets.only(top: 20),
        child: DottedBorder(
            color: Colors.red,
            //color of dotted/dash line
            strokeWidth: 2,
            //thickness of dash/dots
            dashPattern: [10, 10],
            borderType: BorderType.RRect,
            radius: Radius.circular(16),
            //dash patterns, 10 is dash width, 6 is space width
            child: Container(
                height: 180,
                //height of inner container
                width: double.infinity,
                //width to 100% match to parent container.
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _getImageWidget(),
                    _labelTitle(S.of(context).desImageTitle),
                    ElevatedButton(
                      onPressed: () {
                        _pickImage();
                      },
                      child: Text(
                        S.of(context).chooseImage,
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          fontSize: 14,
                          color: Colors.red,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                        shape: StadiumBorder(),
                        primary: Color(0xFFFFDBDB),
                      ),
                    ),
                  ],
                ))));
  }

  Widget _getImageWidget() {
    return Obx(() =>
        (state.shopAvatar.value != null && state.shopAvatar.value.isNotEmpty)
            ? _getImageShop(state.shopAvatar.value)
            : _getDefalutImageWidget());
  }

  Widget _getImageShop(String url) {
    if (url.startsWith('http')) {
      return Container(
        width: 100.0,
        height: 100.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          image: DecorationImage(
            image: CachedNetworkImageProvider(url),
            fit: BoxFit.cover,
          ),
        ),
      );
    }
    return Image.file(
      File(url),
      width: 100,
      height: 100,
      fit: BoxFit.cover,
    );
  }

  Widget _getDefalutImageWidget() {
    return MaterialButton(
      onPressed: () {},
      color: Color(0xFFFFDBDB),
      child: SvgPicture.asset(
        'assets/svg/ic_image.svg',
        color: Colors.red,
      ),
      padding: EdgeInsets.all(16),
      shape: CircleBorder(),
    );
  }

  _pickImage() {
    _editAvatar(context, state.shopAvatar.value);
  }

  void _editAvatar(BuildContext context, String url) async {
    _mediaActionSheetWidget.showActionSheet(
        '', [S.of(context).takePhoto, S.of(context).chooseFromGrallery],
        (int index) {
      if (index == 0) {
        _openCamera((pickedFile) {
          state.shopAvatar.value = pickedFile.path;
        });
      }
      if (index == 1) {
        _openGrallery((pickedFile) {
          state.shopAvatar.value = pickedFile.path;
        });
      }
    });
  }

  void _openCamera(OnPickImageCallback onPickImageCallback) {
    _mediaActionSheetWidget.pickImageDialog(ImageSource.camera,
        (PickedFile pickedFile) {
      // setState(() {
      onPickImageCallback(pickedFile);
      // });
    });
  }

  void _openGrallery(OnPickImageCallback onPickImageCallback) {
    _mediaActionSheetWidget.pickImageDialog(ImageSource.gallery,
        (PickedFile pickedFile) {
      // setState(() {
      onPickImageCallback(pickedFile);
      // });
    });
  }

  Widget _labelTitle(String name) {
    return Text(name,
        textAlign: TextAlign.left,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          fontSize: 14,
          color: const Color(0xff333333),
          fontWeight: FontWeight.normal,
        ));
  }

  Widget _labelValue(String value) {
    return Text(value,
        style: TextStyle(
          fontSize: 14,
          color: const Color(0xff333333),
          fontWeight: FontWeight.normal,
        ));
  }

  @override
  bool get wantKeepAlive => true;

  @override
  RegisterShopSController createController() {
    final registerShopRepository = RegisterShopApi();
    final findNearRepository = FindNearApi();
    return RegisterShopSController(
        registerShopRepository: registerShopRepository,
        findNearRepository: findNearRepository);
  }

  Widget _buildJobType(User value) {
    String initId = (state.shop.value?.field_id ?? "Nhà hàng").toString();
    final cacheService = Get.find<CacheService>();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _buildTitle(S.of(context).job_type),
        SelectFormField(
          style: TextStyle(
            fontFamily: "Quicksand",
            color: AppColors.black,
            fontWeight: FontWeight.normal,
            fontSize: 16,
          ),
          labelText: 'Shape',
          initialValue: initId,
          decoration: getInputDecoration(
            hintText: S.of(context).chooseProfession,
            labelText: S.of(context).job_type,
          ),
          changeIcon: true,
          enableSearch: true,
          items: cacheService.fieldsAsMap,
          validator: (input) => null,
          onChanged: (input) => {state.selectedJobType.value = input},
          itemIsLocked: () {
            showDialog(
              context: Get.context,
              barrierDismissible: true,
              builder: (BuildContext context) => contactUsDialog(),
            );
          },
        ),
      ],
    );
  }

  Dialog contactUsDialog() {
    return Dialog(
      backgroundColor: Colors.transparent,
      insetPadding: EdgeInsets.zero,
      child: Column(
        children: [
          Spacer(),
          Stack(
            children: [
              Container(
                padding: EdgeInsets.fromLTRB(20, 40, 20, 20),
                margin: EdgeInsets.fromLTRB(30, 25, 30, 0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: Column(
                  children: <Widget>[
                    Text(
                      S.of(context).job_is_not_available,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: AppColors.black,
                      ),
                    ),
                    SizedBox(height: 10),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: S.of(context).job_is_not_available_content_1,
                            style: TextStyle(
                                fontSize: 14,
                                color: AppColors.black,
                                fontWeight: FontWeight.w300,
                                height: 1.4),
                          ),
                          TextSpan(
                            text: '0985725511',
                            style: TextStyle(
                                fontSize: 14,
                                color: AppColors.red,
                                fontWeight: FontWeight.bold,
                                height: 1.4),
                          ),
                          TextSpan(
                            text: S.of(context).job_is_not_available_content_2,
                            style: TextStyle(
                                fontSize: 14,
                                color: AppColors.black,
                                fontWeight: FontWeight.w300,
                                height: 1.4),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Positioned.fill(
                top: 0,
                child: Align(
                  alignment: Alignment.topCenter,
                  child: AppImage(AppImages.icMail),
                ),
              ),
              Positioned(
                top: 30,
                right: 30,
                child: InkWell(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AppImage('assets/img/ic_close.png'),
                  ),
                  onTap: () => Get.back(),
                ),
              )
            ],
          ),
          Spacer(),
        ],
      ),
    );
  }

  InputDecoration getInputDecoration({String hintText, String labelText}) {
    return new InputDecoration(
      hintText: hintText,
      // labelText: labelText,
      hintStyle: TextStyle(
        fontFamily: "Quicksand",
        color: AppColors.black,
        fontWeight: FontWeight.normal,
        fontSize: 14,
      ),
      enabledBorder:
          UnderlineInputBorder(borderSide: BorderSide(color: Colors.black26)),
      focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Theme.of(context).hintColor)),
      floatingLabelBehavior: FloatingLabelBehavior.auto,
      contentPadding: EdgeInsets.only(bottom: 12, top: 3),
      isDense: true,
    );
  }

  Widget _buildTitle(String text) {
    return Text(
      text,
      style: TextStyle(
        fontFamily: "Quicksand",
        color: AppColors.black,
        fontWeight: FontWeight.bold,
        fontSize: 14,
      ),
    );
  }

  void hideKeyboard() {
    FocusManager.instance.primaryFocus?.unfocus();
  }
}
