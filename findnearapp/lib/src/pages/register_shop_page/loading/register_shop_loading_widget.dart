import 'package:findnear/src/widgets/shimmer/app_shimmer.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RegisterShopLoadingWidget extends StatelessWidget {
  const RegisterShopLoadingWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _getBody(),
    );
  }

  Widget _getTitleWidget() {
    return Row(
      children: [
        AppShimmer(width: 100, height: 20, cornerRadius: 10),
        Spacer(),
        ElevatedButton(
          onPressed: () {},
          child: AppShimmer(width: 60, height: 20, cornerRadius: 10),
          style: ElevatedButton.styleFrom(
              shape: StadiumBorder(), primary: Colors.red),
        ),
      ],
    );
  }

  Widget _getBody() {
    return Container(
        padding: EdgeInsets.only(left: 20, top: 32, right: 20),
        child: Form(
            child: ListView(
              children: [
                AppShimmer(width: MediaQuery.of(Get.context).size.width, height: 50, cornerRadius: 10),
                SizedBox(height: 20),
                AppShimmer(width: 80, height: 30, cornerRadius: 10),
                SizedBox(height: 20),
                AppShimmer(width: MediaQuery.of(Get.context).size.width / 3, height: 50, cornerRadius: 10),
                SizedBox(height: 20),
                Row(
                  children: [
                    Expanded(child: AppShimmer( height: 50, cornerRadius: 10)),
                    SizedBox(width: 16),
                    Expanded(child: AppShimmer( height: 50, cornerRadius: 10)),
                  ],
                ),
                SizedBox(height: 20),
                Row(
                  children: [
                    Expanded(child: AppShimmer( height: 50, cornerRadius: 10)),
                    SizedBox(width: 16),
                    Expanded(child: AppShimmer( height: 50, cornerRadius: 10)),
                  ],
                ),
                SizedBox(height: 20),
                AppShimmer(width: MediaQuery.of(Get.context).size.width, height: 150, cornerRadius: 10),
                SizedBox(height: 20),
                AppShimmer(width: MediaQuery.of(Get.context).size.width, height: 150, cornerRadius: 20),
              ],
            )));
  }
}
