import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/database/conversations_dao.dart';
import 'package:findnear/src/global/global_event.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/repository/communication_repository.dart';
import 'package:get/get.dart';
import 'setting_pin_code_state.dart';

class SettingPinCodeLogic extends BaseController<SettingPinCodeState> {
  final _conversationsDAO = Get.find<ConversationsDAO>();
  CommunicationRepository _repository = CommunicationApi();

  @override
  SettingPinCodeState createState() => SettingPinCodeState();

  void changePINCode(ResetPINCodeType type, String uuid) async {
    await updatePINCode("", uuid);
    GlobalEvent.instance.onPinCodeUpdated.add(state.conversation);
    state.resetPINCodeType= type;
  }

  void fetchConversationFromLocal(String uuid) async {
    state.fetchConversationStatus.value = LoadStatus.loading;
    try {
      state.conversation = await _conversationsDAO.get(uuid);
      state.fetchConversationStatus.value = LoadStatus.success;
    } catch (e){
      state.fetchConversationStatus.value = LoadStatus.failure;
    }
  }

  Future<bool> updatePINCode(String pinCode, String uuid) async{
    state.fetchConversationStatus.value = LoadStatus.loading;
    try {
      var response = await _repository.createPinCode(uuid: uuid, pinCode: pinCode);
      if(response != null){
        state.conversation.pin = pinCode;
        await _conversationsDAO.put(state.conversation);
        GlobalEvent.instance.onPinCodeUpdated.add(state.conversation); // báo cho các màn hình khác cập nhật lại
        Conversation result = await _conversationsDAO.get(state.conversation.uuid);
        state.fetchConversationStatus.value = LoadStatus.success;
        return true;
      }
      return false;
    } catch (e){
      state.fetchConversationStatus.value = LoadStatus.failure;
      return false;
    }
  }

}
