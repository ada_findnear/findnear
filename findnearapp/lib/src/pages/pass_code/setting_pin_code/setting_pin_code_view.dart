import 'dart:async';

import 'package:findnear/main.dart';
import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/enums/edit_pin_step_type.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/pages/messages/conversation_detail/conversation_detail_logic.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:passcode_screen/circle.dart';
import 'package:passcode_screen/keyboard.dart';
import 'package:passcode_screen/passcode_screen.dart';

import 'setting_pin_code_logic.dart';
import 'setting_pin_code_state.dart';

class SettingPinCodePage extends StatefulWidget {
  static const String ROUTE_NAME = '/SettingPinCodePage';
  final RouteArgument routeArgument;

  SettingPinCodePage({this.routeArgument});

  @override
  State<SettingPinCodePage> createState() => _SettingPinCodePageState();
}

class _SettingPinCodePageState extends BaseWidgetState<SettingPinCodePage,
    SettingPinCodeLogic,
    SettingPinCodeState> {

  final StreamController<bool> _verificationNotifier =
  StreamController<bool>.broadcast();
  EditPINStepType _stepType = EditPINStepType.NONE;

  String get uuid => widget.routeArgument.param;

  @override
  SettingPinCodeLogic createController() => SettingPinCodeLogic();

  @override
  void initState() {
    super.initState();
    controller.fetchConversationFromLocal(uuid);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.red,
        title: Text(
          'Ẩn trò chuyện',
        ),
        iconTheme: IconThemeData(
          color: Colors.white, //change your color here
        ),
      ),
      body: Obx(() {
        return Container(
          color: AppColors.greyBg,
          child: Column(
            children: [
              _buildItem(
                  title: "Đổi mã PIN",
                  content: "Đổi mã PIN cho trò chuyện ẩn",
                  click: () {
                    _goToPinCodeScreen(oldPinCode: state.conversation.pin, type: EditPINStepType.INPUT_OLD_PIN_CODE);
                  },
                  type: state.resetPINCodeType,
              ),
              _buildSeperate(),
              _buildItem(
                  title: "Đặt lại mã PIN",
                  content:
                  "Đặt lại mã PIN để huỷ mã PIN cũ và toàn bộ thiết lập. Khi đó, tất cả các cuộc trò chuyện đang ẩn cũng sẽ bị xoá",
                  click: () {
                    _resetPinCode(state.conversation.uuid);
                  },
                type: state.resetPINCodeType,
              ),
              _buildSeperate(),
              _buildItem(
                  title: "",
                  content: state.resetPINCodeType.title,
                  click: () {}),
            ],
          ),
        );
      }),
    );
  }

  @override
  void dispose() {
    _verificationNotifier.close();
    super.dispose();
  }

  Widget _buildItem({String title, String content, Function click, ResetPINCodeType type}) {
    return Flexible(
      flex: 1,
      child: GestureDetector(
        onTap: () {
          if(type == ResetPINCodeType.NONE)
            click();
        },
        child: Container(
          color: AppColors.white,
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Visibility(
                      visible: title.isNotEmpty,
                      child: Text(
                        title ?? '',
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          fontSize: 16,
                          color: type == ResetPINCodeType.NONE ? Colors.black : AppColors.gray,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    SizedBox(height: 5),
                    Flexible(
                      child: Text(
                        content,
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          fontSize: 14,
                          color: AppColors.gray,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(width: 10),
              Visibility(
                visible: title.isNotEmpty,
                child: Icon(
                  Icons.arrow_forward_ios_outlined,
                  color: Colors.grey,
                  size: 20,
                ),
              ),
              Visibility(
                visible: title.isEmpty,
                child: SizedBox(
                  width: 20,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildSeperate() {
    return Container(
      color: AppColors.white,
      width: Get.mediaQuery.size.width,
      child: Container(
        width: Get.mediaQuery.size.width,
        height: 1,
        color: AppColors.greyBg,
        margin: EdgeInsets.symmetric(vertical: 5),
      ),
    );
  }

  void showResetPINCodeDialog(String uuid) {
    showCupertinoDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text(
            "Đặt lại mã PIN?",
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 18,
              color: AppColors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          content: Text(
            "Tất cả trò chuyện đang ẩn sẽ bị xoá sau khi đặt lại mã PIN",
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 14,
              color: AppColors.black,
              fontWeight: FontWeight.normal,
            ),
          ),
          actions: [
            CupertinoDialogAction(
              child: Text(
                S.of(context).cancel_input,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 16,
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            CupertinoDialogAction(
                child: Text(
                  "Đặt lại",
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 16,
                    color: AppColors.red,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                onPressed: () {
                  controller.changePINCode(ResetPINCodeType.RESET, uuid);
                  Navigator.of(context).pop();
                }),
          ],
        );
      },
    );
  }

  void _resetPinCode(String uuid) {
    showResetPINCodeDialog(uuid);
  }

  void _onPasscodeCancelled() {
    Navigator.maybePop(Get.context);
  }

  void _goToPinCodeScreen({String oldPinCode, EditPINStepType type}) {
    Navigator.push(
        Get.context,
        PageRouteBuilder(
            opaque: false,
            pageBuilder: (context, animation, secondaryAnimation) {
              return PasscodeScreen(
                title: Text(
                  type.title,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: 'Quicksand',
                      color: Colors.black,
                      fontSize: 14),
                ),
                isValidCallback: (String enteredPasscode) async {
                  switch (type) {
                    case EditPINStepType.INPUT_OLD_PIN_CODE:
                      // Navigator.pop(context);
                      _goToPinCodeScreen(oldPinCode: enteredPasscode, type: EditPINStepType.INPUT_NEW_PIN_CODE);
                      break;
                    case EditPINStepType.INPUT_NEW_PIN_CODE:
                      // Navigator.pop(context);
                      _goToPinCodeScreen(oldPinCode: enteredPasscode, type: EditPINStepType.REINPUT_NEW_PIN_CODE);
                      break;
                    case EditPINStepType.REINPUT_NEW_PIN_CODE:
                      bool success = await controller.updatePINCode(enteredPasscode, state.conversation.uuid);
                      if(success)
                        Fluttertoast.showToast(
                            msg: "Đổi mã PIN thành công",
                            toastLength: Toast.LENGTH_SHORT,
                            backgroundColor: AppColors.black,
                            textColor: AppColors.white,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIosWeb: 1);
                      else
                        Fluttertoast.showToast(
                            msg: "Đổi mã PIN thất bại",
                            toastLength: Toast.LENGTH_SHORT,
                            backgroundColor: AppColors.black,
                            textColor: AppColors.white,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIosWeb: 1);
                      break;
                  }
                },
                passwordEnteredCallback: (String enteredPasscode) async {
                  bool isValid = true;
                  if(type == EditPINStepType.INPUT_OLD_PIN_CODE || type == EditPINStepType.REINPUT_NEW_PIN_CODE)
                    isValid = enteredPasscode == oldPinCode;
                  _verificationNotifier.add(isValid);
                  if (!isValid)
                    Fluttertoast.showToast(
                        msg: "Mã PIN không đúng, vui lòng thử lại.",
                        toastLength: Toast.LENGTH_SHORT,
                        backgroundColor: AppColors.black,
                        textColor: AppColors.white,
                        gravity: ToastGravity.CENTER,
                        timeInSecForIosWeb: 1);
                },
                cancelButton: Text(
                  S
                      .of(context)
                      .cancel_input,
                  style: const TextStyle(fontSize: 16, color: Colors.black),
                  semanticsLabel: S
                      .of(context)
                      .cancel_input,
                ),
                deleteButton: Image.asset(AppImages.icDeletePinDigit, width: 40, height: 30,),
                circleUIConfig: CircleUIConfig(
                    borderColor: AppColors.red,
                    fillColor: AppColors.red,
                    borderWidth: 2,
                    circleSize: 15),
                keyboardUIConfig: KeyboardUIConfig(
                  digitBorderWidth: 2,
                  primaryColor: Colors.white,
                  digitFillColor: Colors.white,
                  digitTextStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 28,
                      fontWeight: FontWeight.normal),
                ),
                shouldTriggerVerification: _verificationNotifier.stream,
                backgroundColor: AppColors.greyBg,
                cancelCallback: _onPasscodeCancelled,
                passwordDigits: 4,
              );
            }));
  }
}
