import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:get/get.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';

import '../../../../main.dart';

enum ResetPINCodeType {
  NONE,
  RESET,
}

extension ResetPINCodeTypeExtension on ResetPINCodeType {
  String get title {
    switch (this) {
      case ResetPINCodeType.NONE:
        return S.of(Get.context).reset_pin_code_description;
      case ResetPINCodeType.RESET:
        return S.of(Get.context).hide_conversation_description;
      default:
        return "";
    }
  }
}
class SettingPinCodeState extends BaseState{
  Rx<ResetPINCodeType> resetPINCodeTypeRx = ResetPINCodeType.NONE.obs;
  Conversation conversation;
  Rx<LoadStatus> fetchConversationStatus = LoadStatus.initial.obs;

  ResetPINCodeType get resetPINCodeType => resetPINCodeTypeRx.value;
  set resetPINCodeType(ResetPINCodeType type) => resetPINCodeTypeRx.value = type;

}
