import 'dart:io';

import 'package:findnear/src/models/entities/message_chat/entities/message_file.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:get/get.dart';

class PreviewDocumentPage extends StatelessWidget {
  static const String ROUTE_NAME = '/PreviewDocumentPage';

  final RouteArgument routeArgument;
  var isLoading = true.obs;

  String get fileUrl => routeArgument.param as String;
  String get fileName => fileUrl.split('/').last;

  PreviewDocumentPage({Key key, @required this.routeArgument})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          fileName,
          style: TextStyle(color: Colors.black),
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
        ),
        leading: IconButton(
          icon: Icon(
            Icons.close,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context, true);
          },
        ),
        automaticallyImplyLeading: false,
        // backgroundColor: Colors.black,
        elevation: 1,
      ),
      body: Obx(() {
        return Stack(
          fit: StackFit.expand,
          children: [
            WebView(
              javascriptMode: JavascriptMode.unrestricted ,
              initialUrl: Platform.isIOS ? fileUrl : 'http://docs.google.com/viewer?url=$fileUrl',
              onPageFinished: (url) {
                print('onPageFinished: $url');
                isLoading.value = false;
              },
            ),
            if (isLoading.value == true) Center(child: LoadingView())
          ],
        );
      }),
    );
  }
}
