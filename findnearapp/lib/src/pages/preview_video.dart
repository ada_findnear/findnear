import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class PreviewVideoWidget extends StatefulWidget {
  final String path;
  final String thumbUrl;
  final bool closable;
  final num width;
  final num height;

  PreviewVideoWidget(
      {Key key,
      @required this.path,
      this.closable = true,
      this.thumbUrl,
      this.width,
      this.height})
      : super(key: key);

  @override
  _PreviewVideoWidgetState createState() => _PreviewVideoWidgetState();
}

class _PreviewVideoWidgetState extends State<PreviewVideoWidget> {
  VideoPlayerController _videoPlayerController;

  @override
  void initState() {
    _videoPlayerController = VideoPlayerController.network(widget.path)
      ..initialize().then((_) {
        // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
        setState(() {
          _videoPlayerController.play();
        });
      });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var ratio = (widget.width ?? 9.0) / (widget.height ?? 16.0);
    return Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          leading: Visibility(
            visible: widget.closable,
            child: IconButton(
              icon: Icon(
                Icons.close,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.pop(context, true);
              },
            ),
          ),
          automaticallyImplyLeading: false,
          backgroundColor: Colors.black,
          elevation: 0,
        ),
        body: Center(
          child: InkWell(
            onTap: () {
              setState(() {
                _videoPlayerController.value.isPlaying
                    ? _videoPlayerController.pause()
                    : _videoPlayerController.play();
              });
            },
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                _videoPlayerController.value.isInitialized
                    ? AspectRatio(
                        aspectRatio: _videoPlayerController.value.aspectRatio,
                        child: VideoPlayer(_videoPlayerController),
                      )
                    : Container(
                        width: double.infinity,
                        height: double.infinity,
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            CachedNetworkImage(
                              errorWidget: (c, s, i) =>
                                  SizedBox(width: 0, height: 0),
                              imageUrl: widget.thumbUrl ?? '',
                              fit: BoxFit.cover,
                            ),
                            CircularProgressIndicator()
                          ],
                        )),
                Visibility(
                  visible: _videoPlayerController.value.isInitialized && !_videoPlayerController.value.isPlaying,
                  child: IconButton(
                      iconSize: 55,
                      icon: Icon(_videoPlayerController.value.isPlaying
                          ? Icons.pause
                          : Icons.play_arrow)),
                )
              ],
            ),
          ),
        ));
  }

  @override
  void dispose() {
    super.dispose();
    _videoPlayerController.dispose();
  }
}
