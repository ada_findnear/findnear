
import 'dart:convert';

import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/models/newsfeed_comment.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/repository/newsfeed_repository.dart';
import 'package:findnear/src/repository/post_repository.dart' as postRepo;
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:findnear/src/utils/media/media_picker_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:images_picker/images_picker.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'newsfeed_post_detail_state.dart';

class NewsfeedPostDetailsLogic extends GetxController {
  final Post post;

  final NewsfeedPostDetailsState state;
  final NewsfeedRepository newsfeedRepository = Get.find();
  final RefreshController refreshController;

  String get postId => post.id;

  NewsfeedPostDetailsLogic({this.post, this.refreshController})
      : state = NewsfeedPostDetailsState(initialPost: post);

  void initialize({Post post}) {
    _resetData();
    getComments();
  }

  void _resetData() {
    state.comments.clear();
  }

  void getComments({bool isRefresh = false}) async {
    state.isLoading.value = true;
    try {
      final post = await newsfeedRepository.getPostDetails(postId);
      updatePost(post: post);
      final comments = post.comments;
      if (isRefresh) {
        _resetData();
        refreshController.refreshCompleted();
      } else {
        refreshController.loadComplete();
      }
      if (comments.length > 0) {
        state.comments.addAll(comments);
      }
      state.isLoading.value = false;
    } catch (e) {
      print(e);
      state.isLoading.value = false;
      if (isRefresh) {
        refreshController.refreshFailed();
      } else {
        refreshController.loadFailed();
      }
      String s = jsonDecode(e.response.toString())["message"];
      if( s == "Post not found"){
        ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
          content: Text(S.of(Get.context).post_is_delete),
        ));
        Get.back();
      }
    }
  }

  void updatePost({Post post}) {
    if (post != null) state.post.value = post;
  }

  void toggleLikeComment(
    NewsfeedComment comment, {
    @required int parentIndex,
    int childIndex = -1,
  }) async {
    NewsfeedComment comment;
    if (childIndex == -1) {
      comment = state.comments.elementAt(parentIndex);
    } else {
      comment =
          state.comments.elementAt(parentIndex).children.elementAt(childIndex);
    }
    NewsfeedComment result;
    result = await newsfeedRepository.toggleLikeComment(commentId: comment.id);

    comment = comment.copyWith(isLike: result.isLike);
    if (childIndex == -1) {
      state.comments[parentIndex] = comment.copyWith(isLike: result.isLike);
    } else {
      final newReplyList = [...state.comments[parentIndex].children];
      newReplyList[childIndex] = comment.copyWith(isLike: result.isLike);
      final updatedComment =
          state.comments[parentIndex].copyWith(children: newReplyList);
      state.comments[parentIndex] = updatedComment;
    }
  }

  void deleteComment(
    NewsfeedComment comment, {
    @required int parentIndex,
    int childIndex = -1,
  }) async {
    NewsfeedComment comment;
    if (childIndex == -1) {
      comment = state.comments.elementAt(parentIndex);
    } else {
      comment =
          state.comments.elementAt(parentIndex).children.elementAt(childIndex);
    }

    final result =
        await newsfeedRepository.deleteComment(commentId: comment.id);

    if (result.success ?? false) {
      unfocusReply();
      if (childIndex == -1) {
        state.comments.removeAt(parentIndex);
      } else {
        final newReplyList = [...state.comments[parentIndex].children];
        newReplyList.removeAt(childIndex);
        final updatedComment =
            state.comments[parentIndex].copyWith(children: newReplyList);
        state.comments[parentIndex] = updatedComment;
      }
    }
  }

  void sendComment({String content = ""}) async {
    if ((content?.isEmpty ?? true) &&
        (state?.media?.value?.path?.isEmpty ?? true)) {
      return;
    }

    final imagePath = state?.media?.value?.path;
    removeImageFromCommentBar();
    state.isLoading.value = true;
    state.isLoadingCommentBar.value = true;
    try {
      final result = (await newsfeedRepository.createComment(
              content: content,
              postId: state.post.value.id,
              imagePath: imagePath))
          .copyWith(creator: currentUser.value);

      state.comments.add(result);
      final updatedPost = state.post.value.copyWith(
          comments_count:
              "${(int.tryParse(state?.post?.value?.comments_count) ?? 0) + 1}");
      updatePost(post: updatedPost);
      unfocusReply();
      state.isLoading.value = false;
      state.isLoadingCommentBar.value = false;
    } catch (e) {
      logger.e(e);
      state.isLoading.value = false;
      state.isLoadingCommentBar.value = false;
    }
  }

  void sendReply({String content, String replyingToId}) async {
    if ((content?.isEmpty ?? true) &&
        (state?.media?.value?.path?.isEmpty ?? true)) {
      return;
    }
    final imagePath = state?.media?.value?.path;
    removeImageFromCommentBar();
    state.isLoading.value = true;
    try {
      final result = (await newsfeedRepository.createComment(
        content: content,
        postId: state.post.value.id,
        parentCommentId: replyingToId,
        imagePath: imagePath,
      ))
          .copyWith(creator: currentUser.value);
      final indexOfReplyingComment =
          state.comments.indexWhere((element) => element.id == replyingToId);
      if (indexOfReplyingComment != -1) {
        final parentComment = state.comments.elementAt(indexOfReplyingComment);
        final updatedReplies = [...parentComment.children, result];
        state.comments[indexOfReplyingComment] = parentComment.copyWith(
          children: updatedReplies,
        );
      }
      unfocusReply();
      state.isLoading.value = false;
    } catch (e) {
      logger.e(e);
      state.isLoading.value = false;
    }
  }

  Future onClickImage() async {
    await MediaPickerUtils.pickPhotoForStory(0).then((media) {
      if (media != null && media.isNotEmpty) {
        state.media.value = media.first;
      }
    });
  }

  void removeImageFromCommentBar() {
    state.media.value = Media(path: "", size: 0);
  }

  void unfocusReply() {
    state.parentCommentId.value = "";
    removeImageFromCommentBar();
  }

  void hidePost() async {
    final Post post = state.post.value;
    try {
      final result = await newsfeedRepository.hidePost(post.id);
      if (result.success) {
        Get.back();
      }
    } catch (e) {
      print(e);
    }
  }

  void deletePost() async {
    final post = state.post.value;
    try {
      final result = await postRepo.removePost(post);
      if (result) {
        Get.back();
      }
    } catch (e) {
      logger.e(e);
    }
  }
}
