import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/CommentBarWidget.dart';
import 'package:findnear/src/elements/CommentReplyWidget.dart';
import 'package:findnear/src/elements/NewsfeedItemWidget.dart';
import 'package:findnear/src/helpers/app_config.dart';
import 'package:findnear/src/models/bottom_sheet_option.dart';
import 'package:findnear/src/models/newsfeed_comment.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/pages/create_post/create_post.controller.dart';
import 'package:findnear/src/pages/create_post/create_post.screen.dart';
import 'package:findnear/src/pages/preview_media.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'newsfeed_post_detail_logic.dart';
import 'newsfeed_post_detail_state.dart';

class NewsfeedPostDetailsPage extends StatefulWidget {
  final Post post;
  final bool isPostContainsOnlyId;

  const NewsfeedPostDetailsPage({
    Key key,
    this.post,
    this.isPostContainsOnlyId = false,
  }) : super(key: key);

  @override
  _NewsfeedPostDetailsPageState createState() =>
      _NewsfeedPostDetailsPageState();
}

class _NewsfeedPostDetailsPageState extends State<NewsfeedPostDetailsPage> {
  NewsfeedPostDetailsLogic logic;
  NewsfeedPostDetailsState state;
  RefreshController _refreshController = RefreshController();
  TextEditingController textEditingController = TextEditingController();
  FocusNode textFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    logic = Get.put(
      NewsfeedPostDetailsLogic(
        post: widget.post,
        refreshController: _refreshController,
      ),
      tag: widget.post.id,
    );
    state = Get.find<NewsfeedPostDetailsLogic>(tag: widget.post.id).state;
    logic.initialize();
    textFocusNode.addListener(onFocusChangeListener);
  }

  AppBar _getAppBar() {
    return AppBar(
      backgroundColor: AppColors.white,
      centerTitle: false,
      title: Text(
        S.current.comment,
        style: Theme.of(context).textTheme.headline2.merge(TextStyle(
              color: AppColors.textBlack,
              fontSize: 16,
            )),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context);
        return false;
      },
      child: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Container(
          color: AppColors.white,
          child: SafeArea(
            child: Scaffold(
              backgroundColor: AppColors.white,
              appBar: _getAppBar(),
              body: Column(
                children: [
                  Expanded(
                    child: Obx(() {
                      if (widget.isPostContainsOnlyId &&
                          (state.post?.value?.userId?.isEmpty ?? true)) {
                        return Center(child: LoadingView());
                      }
                      return SmartRefresher(
                        enablePullUp: false,
                        onRefresh: () async =>
                            await logic.getComments(isRefresh: true),
                        controller: _refreshController,
                        child: SingleChildScrollView(
                          padding: const EdgeInsets.only(
                              top: 16, bottom: 40),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Container(
                                padding: EdgeInsets.only(left: 15, right: 15),
                                child: NewsfeedItemWidget(
                                  post: logic.state.post.value,
                                  isMyProfile: logic.state.post.value.creator.id ==
                                      currentUser.value.id,
                                  hasActionBars: false,
                                  onMyPostMenuPressed: () async {
                                    final indexTapped =
                                        await DialogHelper.showBottomSheetOptions(
                                      context: context,
                                      options: [
                                        BottomSheetOption(
                                          iconPath: AppImages.icWrite,
                                          text: S.of(context).edit_post,
                                        ),
                                        BottomSheetOption(
                                          iconPath: AppImages.icTrash,
                                          text: S.of(context).delete_post,
                                        ),
                                        BottomSheetOption(
                                          icon: Icons.share,
                                          text: S.of(context).shareThisPost,
                                        ),
                                      ],
                                    );
                                    switch (indexTapped) {
                                      case 0:
                                        _onClickEditPost();
                                        break;
                                      case 1:
                                        final isDeleting =
                                            await DialogHelper.showConfirmPopup(
                                          title: S.current.deletePost,
                                          description:
                                              S.current.confirmDeleteDescription,
                                          okBtnText: S.current.deletePostShort,
                                        );

                                        if (isDeleting ?? false) {
                                          logic.deletePost();
                                        }
                                        break;
                                      case 2:
                                        _onClickSharePost();
                                        break;
                                    }
                                  },
                                  onOtherUserPostMenuPressed: () async {
                                    final indexTapped = await DialogHelper.showBottomSheetOptions(
                                      context: context,
                                      options: [
                                        // temporarily disable
                                        // BottomSheetOption(
                                        //   iconPath: AppImages.icBookmarkOutlined,
                                        //   text: S.of(context).save_post,
                                        // ),
                                        BottomSheetOption(
                                          iconPath: AppImages.icHide,
                                          text: S.of(context).hide_post,
                                        ),
                                        BottomSheetOption(
                                          icon: Icons.share,
                                          text: S.of(context).shareThisPost,
                                        ),
                                      ],
                                    );
                                    switch (indexTapped) {
                                    // case 0:
                                    //   logic.savePost(index);
                                    //   break;
                                      case 0:
                                        logic.hidePost();
                                        break;
                                      case 2:
                                        _onClickSharePost();
                                        break;
                                    }
                                  },
                                  // toggleLikePost: (id) => logic.toggleLikePost(index),
                                ),
                              ),
                              _buildComments(context),
                            ],
                          ),
                        ),
                      );
                    }),
                  ),
                  Obx(() {
                    return CommentBarWidget(
                      canInput: true,
                      focusNode: textFocusNode,
                      controller: textEditingController,
                      onTapSticker: () {
                        textFocusNode.unfocus();
                        logic.onClickImage();
                      },
                      media: state.media.value.path.isNotEmpty
                          ? state.media.value
                          : null,
                      hideMedia: textFocusNode.hasFocus,
                      isLoading: state.isLoadingCommentBar.value,
                      onTapRemoveMedia: () => logic.removeImageFromCommentBar(),
                      onSend: (text) async {
                        textEditingController.text = "";
                        textFocusNode.unfocus();
                        if (state.parentCommentId.value == "") {
                          await logic.sendComment(content: text);
                        } else {
                          await logic.sendReply(
                              content: text,
                              replyingToId:
                                  state.parentCommentId.value.toString());
                        }
                      },
                    );
                  }),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildComments(BuildContext context) {
    return ListView.separated(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      padding: const EdgeInsets.symmetric(horizontal: 13),
      itemBuilder: (context, index) {
        final comment = state.comments.elementAt(index);
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            CommentReplyWidget(
              comment: comment,
              onReplyCallback: (reply) {
                state.parentCommentId.value = reply.id.toString();
                textFocusNode.requestFocus();
              },
              onLongPress: () async{
                await _handleLongPress(parentIndex: index);
              },
              onLikePress: () {
                logic.toggleLikeComment(
                  comment,
                  parentIndex: index,
                );
              },
            ),
            if (state.comments.elementAt(index).children.length > 0)
              Container(
                margin: const EdgeInsets.only(left: 50),
                child: const Divider(
                    height: 1, thickness: 1, color: AppColors.divider),
              ),
            ListView.separated(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (context, replyIndex) {
                final reply = state.comments
                    .elementAt(index)
                    .children
                    .elementAt(replyIndex);
                return CommentReplyWidget(
                  comment: reply,
                  onReplyCallback: (reply) {
                    state.parentCommentId.value = reply.parentId.toString();
                    textFocusNode.requestFocus();
                  },
                  onLikePress: () => logic.toggleLikeComment(
                    reply,
                    parentIndex: index,
                    childIndex: replyIndex,
                  ),
                  onLongPress: () async {
                    await _handleLongPress(
                        parentIndex: index, childIndex: replyIndex);
                  },
                );
              },
              separatorBuilder: (context, index) => Container(
                margin: const EdgeInsets.only(left: 50),
                child: const Divider(
                    height: 1, thickness: 1, color: AppColors.divider),
              ),
              itemCount: state.comments.elementAt(index).children.length,
            )
          ],
        );
      },
      separatorBuilder: (context, index) =>
          const Divider(height: 1, thickness: 1, color: AppColors.divider),
      itemCount: state.comments.length,
    );
  }

  void onFocusChangeListener() {
    if (textFocusNode.hasFocus) {
      setState(() {});
    }
  }

  void _onClickEditPost() async {
    Get.put(CreatePostController(post: state.post.value));
    final result =
        await Get.to(() => CreatePostScreen(user: currentUser.value));
    Get.delete<CreatePostController>();
    if (result != null) logic.initialize();
  }

  void _handleLongPress({
    @required int parentIndex,
    int childIndex = -1,
  }) async {
    NewsfeedComment comment;
    if (childIndex == -1) {
      comment = state.comments.elementAt(parentIndex);
    } else {
      comment =
          state.comments.elementAt(parentIndex).children.elementAt(childIndex);
    }
    if (!comment.creator.isMe()) return;
    final indexTapped = await DialogHelper.showBottomSheetOptions(
      context: context,
      options: [
        BottomSheetOption(
          iconPath: AppImages.icTrash,
          text: S.of(context).deleteComment,
        ),
      ],
    );
    switch (indexTapped) {
      case 0:
        final isDeleting =
        await DialogHelper.showConfirmPopup(
          title: S.current.deleteComment,
          description: S.current.areYouSureToDeleteThisComment,
          okBtnText: S.current.deleteComment,
        );

        if (isDeleting ?? false) {
          logic.deleteComment(
            comment,
            parentIndex: parentIndex,
            childIndex: childIndex,
          );
        }
        break;
    }
  }

  void _onClickSharePost() async {
    final originalPost = state.post.value.isSharedPost ? state.post.value
        .originalPost : state.post.value;
    Get.put(CreatePostController(originalPost: originalPost));
    await Get.to(() => CreatePostScreen(
      user: currentUser.value,
      originalPost: originalPost,
    ));
    Get.delete<CreatePostController>();
  }

  @override
  void dispose() {
    textFocusNode.removeListener(onFocusChangeListener);
    Get.delete<NewsfeedPostDetailsLogic>();
    super.dispose();
  }
}
