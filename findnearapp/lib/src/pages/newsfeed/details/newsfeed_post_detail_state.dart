import 'package:findnear/src/models/newsfeed_comment.dart';
import 'package:findnear/src/models/post.dart';
import 'package:get/get.dart';
import 'package:images_picker/images_picker.dart';

class NewsfeedPostDetailsState {
  RxList<NewsfeedComment> comments = List<NewsfeedComment>.empty().obs;
  Rx<Post> post;
  Rx<bool> isLoading = false.obs;
  Rx<bool> isLoadingCommentBar = false.obs;
  Rx<String> parentCommentId = "".obs;
  Rx<Media> media = Media(path: "", size: 0).obs;

  NewsfeedPostDetailsState({Post initialPost}) : post = initialPost.obs;
}
