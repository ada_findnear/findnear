import 'package:dio/dio.dart';
import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/enums/pop_enums.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/models/response/live_token_reponse.dart';
import 'package:findnear/src/pages/newsfeed/newsfeed_state.dart';
import 'package:findnear/src/repository/livestream_repository.dart';
import 'package:findnear/src/repository/post_repository.dart' as postRepo;
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/services/notification_service.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../repository/newsfeed_repository.dart';
import 'package:get/get.dart';
import 'package:rxdart/rxdart.dart' as RxDart;
import 'package:findnear/src/models/enums/load_status.dart' as loadStatus;

class NewsfeedLogic extends GetxController {
  final NotificationService notificationService =
      Get.find<NotificationService>();
  final NewsfeedRepository newsfeedRepository = Get.find();
  final NewsfeedState state;
  final RefreshController refreshController;
  final LiveStreamRepository liveStreamRepository =
      Get.find<LiveStreamRepository>();

  NewsfeedLogic(
      {GlobalKey<ScaffoldState> scaffoldKey, @required this.refreshController})
      : state = NewsfeedState();

  void initialize() {
    _resetData();
    RxDart.Rx.zip2(
        Stream.fromFuture(listenForPosts()),
        Stream.fromFuture(getListLiveStream()),
            (broadcastToken, rtmToken) =>
        [broadcastToken, rtmToken]).first;
  }

  Future<void> listenForPosts({bool isRefresh = false}) async {
    if (isRefresh) {
      state.isLoading.value = true;
    }
    if (state.currentOffset.value != 0 && !isRefresh) {
      state.isLoadingMore.value = true;
    }

    try {
      final newPosts = await newsfeedRepository.getNewsfeed(
          offset: isRefresh ? 0 : state.currentOffset, limit: 10);
      if (isRefresh) {
        state.isLoadingMore.value = false;
        _resetData();
        refreshController.refreshCompleted();
      } else {
        refreshController.loadComplete();
      }
      if (newPosts.length > 0) {
        state.posts.addAll(newPosts);
      } else {
        state.canLoadMore.value = false;
      }
      state.currentOffset.value = state.posts.length;
    } catch (e) {
      logger.e(e);
      AppSnackbar.showError(
          message: (e is DioError)
              ? e.message
              : S.of(Get.context).verify_your_internet_connection);
      if (isRefresh) {
        refreshController.refreshFailed();
      } else {
        refreshController.loadFailed();
      }
    } finally {
      if (isRefresh) {
        state.isLoading.value = false;
      } else {
        state.isLoadingMore.value = false;
      }
    }
  }

  void _resetData() {
    state.posts.clear();
    state.canLoadMore.value == true;
    state.currentOffset.value = 0;
  }

  void toggleLikePost(int index) async {
    final Post post = state.posts.elementAt(index);
    final updatePost = post.copyWith(
      isLike: !post.isLike,
      likers_count:
          "${(int.tryParse(post.likers_count) ?? 0) + (post.isLike ? -1 : 1)}",
    );
    state.posts[index] = updatePost;
    try {
      final result = await newsfeedRepository.toggleLikePost(post.id);
      post.isLike = result.isLike;
    } catch (e) {
      logger.e(e);
      state.posts[index] = post;
    }
  }

  void hidePost(int index) async {
    final Post post = state.posts.elementAt(index);
    try {
      final result = await newsfeedRepository.hidePost(post.id);
      if (result.success) {
        state.posts.removeAt(index);
        state.message.value = MessageStates.hidePostSuccessfully;
      }
    } catch (e) {
      print(e);
    }
  }

  void savePost(int index) async {
    final Post post = state.posts.elementAt(index);
    try {
      final result = await newsfeedRepository.savePost(post.id);
      if (result.success) {
        state.message.value = MessageStates.savePostSuccessfully;
      }
    } catch (e) {
      print(e);
    }
  }

  void updatePost({int atIndex}) async {
    final postId = state.posts.elementAt(atIndex).id;
    try {
      final post = await newsfeedRepository.getPostDetails(postId);
      state.posts[atIndex] = post;
    } catch (e) {
      logger.e(e);
    }
  }

  void deletePost({int index}) async {
    final post = state.posts.elementAt(index);
    try {
      final result = await postRepo.removePost(post);
      if (result) {
        state.posts.removeAt(index);
        initialize();
      }
    } catch (e) {
      logger.e(e);
    }
  }

  void getTokenBroadcaster() async{
    DialogHelper.showLoaderDialog();
    RxDart.Rx.zip2(
        Stream.fromFuture(liveStreamRepository.getTokenBroadcaster()),
        Stream.fromFuture(liveStreamRepository.getRTMToken()),
            (broadcastToken, rtmToken) =>
        [broadcastToken, rtmToken]).first.then((value) {
          try{
            LiveTokenResponse token = value[0];
            LiveTokenResponse rtmToken = value[1];
            if(token.success && rtmToken.success){
              state.liveToken.value = token.data;
              NavigatorUtils.navigateToBroadcastLivestreamWidget(token.data, rtmToken.data, PopTarget.newsfeed.index);
            } else{
              AppSnackbar.showInfo(message: "Tạo livestream không thành công!");
            }
          } catch(e){
            Fluttertoast.showToast(
                msg: "Tạo livestream thất bại!",
                toastLength: Toast.LENGTH_SHORT,
                backgroundColor: AppColors.black,
                textColor: AppColors.white,
                gravity: ToastGravity.CENTER,
                timeInSecForIosWeb: 1);
          }

        },
    );
    Get.back();
  }

  void getTokenAudience(int broadcasterId) async{
    DialogHelper.showLoaderDialog();
    RxDart.Rx.zip2(
        Stream.fromFuture(liveStreamRepository.getTokenAudience(broadcasterId)),
        Stream.fromFuture(liveStreamRepository.getRTMToken()),
            (broadcastToken, rtmToken) =>
        [broadcastToken, rtmToken]).first.then((value) {
            LiveTokenResponse token = value[0];
            LiveTokenResponse rtmToken = value[1];
            if(token.success && rtmToken.success){
                state.audienceToken.value = token.data;
                NavigatorUtils.navigateToAudienceLivestreamWidget(token.data, broadcasterId, rtmToken.data, PopTarget.newsfeed.index);
            } else{
                AppSnackbar.showInfo(message: "Vào xem không thành công!");
            }
        },
    );
    Get.back();
  }

  Future<void> getListLiveStream() async{
    state.fetchListLiveStatus.value = loadStatus.LoadStatus.loading;
    try {
      state.listLive.value = [];
      final result = await liveStreamRepository.getListLiveStream();
      if(result.success){
        result.data.forEach((element) {
          if(element.user_id != int.parse(currentUser.value.id)){
            state.listLive.value.add(element);
          }
        });
      }else{
        state.listLive.value = [];
      }
      state.posts.refresh();
      state.fetchListLiveStatus.value = loadStatus.LoadStatus.success;
    } catch (e){
      state.fetchListLiveStatus.value = loadStatus.LoadStatus.failure;
    }

  }
}
