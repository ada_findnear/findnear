import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/CircularAvatarWidget.dart';
import 'package:findnear/src/elements/NewsfeedItemWidget.dart';
import 'package:findnear/src/elements/SearchBarNewsfeedWidget.dart';
import 'package:findnear/src/models/bottom_sheet_option.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/pages/create_post/create_post.controller.dart';
import 'package:findnear/src/pages/create_post/create_post.screen.dart';
import 'package:findnear/src/pages/game/get_coin/view.dart';
import 'package:findnear/src/pages/groups/group_newsfeed_pageview/group_newsfeed_pageview.dart';
import 'package:findnear/src/pages/notification/notification_list/notification_list_view.dart';
import 'package:findnear/src/pages/story/presentation/view_list/stories_controller.dart';
import 'package:findnear/src/pages/story/presentation/view_list/stories_controller_impl.dart';
import 'package:findnear/src/pages/story/presentation/view_list/stories_widget.dart';
import 'package:findnear/src/pages/story/presentation/view_livestream/loading/view_livestream_loading.dart';
import 'package:findnear/src/pages/story/presentation/view_livestream/view_livestream_view.dart';
import 'package:findnear/src/pages/tab_map/widgets/notification_button.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/media/media_picker_utils.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:images_picker/images_picker.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:findnear/src/models/enums/load_status.dart' as loadStatus;
import 'newsfeed_logic.dart';
import 'newsfeed_state.dart';
import 'package:rxdart/rxdart.dart' as RxDart;

class NewsfeedPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;
  final RefreshController refreshController;

  const NewsfeedPage({Key key, this.parentScaffoldKey, this.refreshController})
      : super(key: key);

  @override
  _NewsfeedPageState createState() => _NewsfeedPageState();
}

class _NewsfeedPageState extends State<NewsfeedPage>
    with AutomaticKeepAliveClientMixin, TickerProviderStateMixin {
  NewsfeedLogic logic;
  NewsfeedState state;
  FToast fToast;
  AnimationController _controller;
  Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (isLoggedIn) {
        Get.put<StoriesController>(StoriesControllerImpl(Get.find()));
      }
      logic = Get.put(NewsfeedLogic(
        scaffoldKey: widget.parentScaffoldKey,
        refreshController: widget.refreshController,
      ));
      state = Get
          .find<NewsfeedLogic>()
          .state;
      logic.initialize();
      DialogHelper.initFToast(context: context);
      state.message.listen(onMessage);
      setState(() {});
    });

    _controller = AnimationController(
      duration: const Duration(milliseconds: 2000),
      vsync: this,
    )
      ..repeat(reverse: true);
    _animation = CurvedAnimation(
      parent: _controller,
      curve: Curves.fastOutSlowIn,
    );
  }

  void onMessage(MessageStates message) {
    switch (message) {
      case MessageStates.savePostSuccessfully:
        DialogHelper.showToast(
          icon: AppImages.icBookmark,
          message: S.current.save_post_successfully,
        );
        break;
      case MessageStates.unsavePostSuccessfully:
        DialogHelper.showToast(
          icon: AppImages.icRemoveBookmark,
          message: S
              .of(context)
              .unsave_post_successfully,
        );
        break;
      case MessageStates.hidePostSuccessfully:
        DialogHelper.showToast(
          icon: AppImages.icHide,
          message: S
              .of(context)
              .hidePostSuccessfully,
        );
        break;
      case MessageStates.unhidePostSuccessfully:
        DialogHelper.showToast(
          icon: AppImages.icHide,
          message: S
              .of(context)
              .unhidePostSuccessfully,
        );
        break;
      case MessageStates.idle:
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    if (logic == null) return Center(child: LoadingView());
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: _getAppBar(),
      body: _buildBodyWidget(),
      floatingActionButton: ScaleTransition(
        scale: _animation,
        child: IconButton(
          iconSize: 80,
          icon: AppImage('assets/img/btn_gift.png'),
          onPressed: () {
            Get.toNamed(GetCoinPage.ROUTE_NAME);
          },
        ),
      ),
    );
  }

  Widget _getAppBar() {
    if (logic == null) return Container();
    return AppBar(
      backgroundColor: Theme
          .of(context)
          .scaffoldBackgroundColor,
      centerTitle: true,
      leadingWidth: 52,
      leading: IconButton(
        icon: AppImage(
          AppImages.icHamburger,
          color: Theme
              .of(context)
              .accentColor,
        ),
        onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
      ),
      titleSpacing: 0,
      title: SearchBarNewsfeedWidget(),
      actions: [
        GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            if (isLoggedIn) {
              Get.to(() => GroupNewsFeedPageView());
            } else {
              NavigatorUtils.toLogin();
            }
          },
          child: Container(
            padding: const EdgeInsets.all(12),
            child: AppImage(
              AppImages.ic_group_appbar,
              fit: BoxFit.contain,
              width: 20,
              height: 20,
            ),
          ),
        ),
        GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            _onClickCreatePost();
          },
          child: Container(
            padding: const EdgeInsets.all(12),
            child: AppImage(AppImages.icWrite, fit: BoxFit.contain),
          ),
        ),
        Obx(() {
          return NotificationButton(
            onPressed: () {
              if (isLoggedIn) {
                Get.to(() => NotificationListPage());
              } else {
                NavigatorUtils.toLogin();
              }
            },
            badgeCounter:
            logic.notificationService.unreadNotificationCounter.value,
          );
        }),
      ],
    );
  }

  Widget _buildPostCreateWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        GestureDetector(
          onTap: _onClickCreatePost,
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 9, horizontal: 12),
            child: Row(
              children: [
                Container(
                  margin: const EdgeInsets.only(right: 12),
                  child: CircularAvatarWidget(
                    imageUrl: currentUser?.value?.image?.url,
                  ),
                ),
                Expanded(
                  child: Text(
                    S.of(context)
                        .what_is_on_your_mind(currentUser?.value?.name ?? ""),
                    style: Theme
                        .of(context)
                        .textTheme
                        .bodyText2
                        .merge(
                        TextStyle(color: AppColors.textBlack, fontSize: 14)),
                  ),
                ),
              ],
            ),
          ),
        ),
        const Divider(height: 1, thickness: 1, color: AppColors.divider),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 20),
          child: Row(
            // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                flex: 3,
                child: InkWell(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12),
                    child: Center(
                      child: _buildActionItemWidget(
                          text: S
                              .of(context)
                              .postImageVideo,
                          icon: AppImage(
                            AppImages.icImageUpload,
                          )),
                    ),
                  ),
                  onTap: () {
                    _onClickPostVideoImage();
                  },
                ),
              ),
              Flexible(
                flex: 2,
                child: InkWell(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12),
                    child: Center(
                      child: _buildActionItemWidget(
                        text: S
                            .of(context)
                            .goLive,
                        icon: AppImage(
                          AppImages.icLive,
                        ),
                      ),
                    ),
                  ),
                  onTap: () {
                    _onClickLivestream();
                  },
                ),
              ),
              Flexible(
                flex: 2,
                child: InkWell(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12),
                    child: Center(
                      child: _buildActionItemWidget(
                        text: S
                            .of(context)
                            .createAlbum,
                        icon: AppImage(
                          AppImages.icNewsfeedAlbum,
                        ),
                      ),
                    ),
                  ),
                  onTap: () {
                    _onClickPostAlbum();
                  },
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildActionItemWidget({
    @required String text,
    @required Widget icon,
    VoidCallback onTap,
  }) {
    return GestureDetector(
      onTap: onTap,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          icon,
          SizedBox(width: 7),
          Text(
            text,
            style: Theme
                .of(context)
                .textTheme
                .bodyText1
                .merge(TextStyle(fontSize: 13, color: AppColors.gray)),
          )
        ],
      ),
    );
  }

  Widget _buildBodyWidget() {
    return Obx(
          () =>
          RefreshConfiguration.copyAncestor(
            context: Get.context,
            footerTriggerDistance: Get.height * 4,
            child: SmartRefresher(
              enablePullUp: state.canLoadMore.value,
              onRefresh: () async {
                if (isLoggedIn) {
                  Get.find<StoriesController>().refreshData();
                }
                RxDart.Rx.zip2(
                    Stream.fromFuture(logic.listenForPosts(isRefresh: true)),
                    Stream.fromFuture(logic.getListLiveStream()),
                        (broadcastToken, rtmToken) =>
                    [broadcastToken, rtmToken]).first.then((value) => widget.refreshController?.refreshCompleted());
              },
              onLoading: () async {
                await logic.listenForPosts();
                widget.refreshController?.loadComplete();
              },
              controller: widget.refreshController,
              child: ListView.separated(
                itemCount: isLoggedIn
                    ? (state?.posts?.length ?? 0) + 2
                    : (state?.posts?.length ?? 0) + 1,
                itemBuilder: (context, index) {
                  // create PostCreateWidget item
                  final postCreateWidget = Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Divider(
                          height: 5, thickness: 5, color: AppColors.divider),
                      _buildPostCreateWidget(),
                      const Divider(
                          height: 5, thickness: 5, color: AppColors.divider),
                    ],
                  );

                  // create NewsfeedItemWidget item
                  final postIndex = isLoggedIn ? index - 2 : index - 1;
                  final postWidget = Container(
                    padding: const EdgeInsets.only(top: 3, bottom: 3),
                    child: GetBuilder<NewsfeedLogic>(
                      init: logic,
                      builder: (GetxController controller) {
                        final post = state.posts.elementAt(postIndex);
                        return Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 15, vertical: 13),
                          margin: const EdgeInsets.symmetric(horizontal: 0),
                          // decoration: BoxDecoration(
                          //   color: Colors.transparent,
                          //   border: Border.all(
                          //     color: Color(0xFFEDEDED),
                          //   ),
                          //   borderRadius: BorderRadius.circular(20),
                          // ),
                          child: NewsfeedItemWidget(
                            post: post,
                            isMyProfile: post?.creator?.id ==
                                currentUser.value?.id,
                            onMyPostMenuPressed: () async {
                              if (!isLoggedIn) {
                                NavigatorUtils.toLogin();
                                return;
                              }
                              final indexTapped =
                              await DialogHelper.showBottomSheetOptions(
                                context: context,
                                options: [
                                  BottomSheetOption(
                                    iconPath: AppImages.icWrite,
                                    text: S
                                        .of(context)
                                        .edit_post,
                                  ),
                                  BottomSheetOption(
                                    iconPath: AppImages.icTrash,
                                    text: S
                                        .of(context)
                                        .delete_post,
                                  ),
                                  // temporarily disable
                                  // BottomSheetOption(
                                  //   iconPath: AppImages.icBookmarkOutlined,
                                  //   text: S.of(context).save_post,
                                  // ),
                                  BottomSheetOption(
                                    icon: Icons.share,
                                    text: S
                                        .of(context)
                                        .shareThisPost,
                                  ),
                                ],
                              );
                              switch (indexTapped) {
                                case 0:
                                  _onClickEditPost(
                                      state.posts.elementAt(postIndex));
                                  break;
                                case 1:
                                  final isDeleting =
                                  await DialogHelper.showConfirmPopup(
                                    title: S.current.deletePost,
                                    description: S.current
                                        .confirmDeleteDescription,
                                    okBtnText: S.current.deletePostShort,
                                  );

                                  if (isDeleting ?? false)
                                    logic.deletePost(index: postIndex);

                                  break;
                                case 2:
                                // share other app
                                  onClickSharePostOtherApp();
                                  break;
                              }
                            },
                            onSharePost: () {
                              // share in app
                              _onClickSharePost(post);
                            },
                            onOtherUserPostMenuPressed: () async {
                              if (!isLoggedIn) {
                                NavigatorUtils.toLogin();
                                return;
                              }
                              final indexTapped =
                              await DialogHelper.showBottomSheetOptions(
                                context: context,
                                options: [
                                  // temporarily disable
                                  // BottomSheetOption(
                                  //   iconPath: AppImages.icBookmarkOutlined,
                                  //   text: S.of(context).save_post,
                                  // ),
                                  BottomSheetOption(
                                    iconPath: AppImages.icHide,
                                    text: S
                                        .of(context)
                                        .hide_post,
                                  ),
                                  BottomSheetOption(
                                    icon: Icons.share,
                                    text: S
                                        .of(context)
                                        .shareThisPost,
                                  ),
                                  BottomSheetOption(
                                    icon: Icons.report,
                                    text: S
                                        .of(context)
                                        .reportThisPost,
                                    isReported: post.isReported,
                                  ),
                                ],
                              );
                              switch (indexTapped) {
                              // case 0:
                              //   logic.savePost(postIndex);
                              //   break;
                                case 0:
                                  logic.hidePost(postIndex);
                                  break;
                                case 1:
                                // _onClickSharePost(post);
                                  onClickSharePostOtherApp();
                                  break;
                                case 2:
                                  state.posts[postIndex].isReported = true;
                                  _onClickReport(post);
                                  break;
                              }
                            },
                            toggleLikePost: (id) {
                              if (!isLoggedIn) {
                                NavigatorUtils.toLogin();
                                return;
                              }
                              logic.toggleLikePost(postIndex);
                            },
                            didUpdateCallback: () {
                              logic.updatePost(atIndex: postIndex);
                            },
                          ),
                        );
                      },
                    ),
                  );

                  // Check case and return item widget
                  if (isLoggedIn) {
                    if (index == 0) {
                      return Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            height: 36,
                            width: MediaQuery
                                .of(context)
                                .size
                                .width,
                            padding: EdgeInsets.only(left: 12),
                            color: Color(0xFFFFEDED),
                            child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  S
                                      .of(context)
                                      .label_moment,
                                  style: TextStyle(
                                      fontSize: 14,
                                      color: Color(0xFF333333),
                                      fontFamily: 'Quicksand',
                                      fontWeight: FontWeight.w600),
                                )),
                          ),
                          StoriesWidget(),
                          state.listLive.value.isEmpty ?
                          Container() :
                          Container(
                            height: 2,
                            width: MediaQuery
                                .of(context)
                                .size
                                .width,
                            margin: EdgeInsets.symmetric(vertical: 5),
                            color: Color(0xFFEDEDED),
                          ),
                          Obx(() {
                            if(state.fetchListLiveStatus.value == loadStatus.LoadStatus.loading)
                              return ViewLivestreamLoading();
                            else if(state.fetchListLiveStatus.value == loadStatus.LoadStatus.success){
                              if(state.listLive.value.isEmpty)
                                return Container();
                              else
                                return ViewLivestreamPage(
                                  listLive: state.listLive.value,
                                  onItemPressed: (broadcasterId) async {
                                    await logic.getTokenAudience(broadcasterId);
                                  },
                                );
                            } else
                              return Container();
                          }),
                        ],
                      );
                    } else if (index == 1) {
                      return postCreateWidget;
                    } else {
                      return postWidget;
                    }
                  } else {
                    if (index == 0) {
                      return postCreateWidget;
                    } else {
                      return postWidget;
                    }
                  }
                },
                separatorBuilder: (context, index) =>
                const Divider(height: 8, thickness: 8, color: AppColors.divider),
              ),
            ),
          ),
    );
  }

  void _onClickCreatePost([List<Media> list]) async {
    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    }
    Get.put(CreatePostController());
    var res = await goTo(
        screen: CreatePostScreen(
          user: currentUser.value,
          medias: list,
        ));
    Get.delete<CreatePostController>();
    if (res != null) logic.initialize();
  }

  void _onClickEditPost(Post post) async {
    Get.put(CreatePostController(post: post));
    final result = await Get.to(() =>
        CreatePostScreen(
          user: currentUser.value,
          originalPost: post.originalPost,
        ));
    Get.delete<CreatePostController>();
    if (result != null) logic.initialize();
  }

  void _onClickSharePost(Post post) async {
    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    }
    final originalPost = post.isSharedPost ? post.originalPost : post;
    Get.put(CreatePostController(originalPost: originalPost));
    var res = await Get.to(() =>
        CreatePostScreen(
          user: currentUser.value,
          originalPost: originalPost,
        ));
    Get.delete<CreatePostController>();
    if (res != null) logic.initialize();
  }

  void onClickSharePostOtherApp() async {
    DialogHelper.showAlertDialog(
      context: context,
      description: "Chức năng đang trong quá trình hoàn thiện",
      title: S.current.notifications,
      okText: 'OK',
      okFunction: () {

      },
    );
  }

  void _onClickReport(Post post) {
    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    }
    DialogHelper.showReportPopup(post);
  }

  void _onClickPostVideoImage() async {
    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    }
    var photo = await MediaPickerUtils.pickPhotoAndVideoForFeed(0);
    if (photo != null) _onClickCreatePost(photo);
  }

  void _onClickPostAlbum() async {
    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    }
    var video = await MediaPickerUtils.pickVideoForFeed(0);
    if (video != null && video.isNotEmpty) _onClickCreatePost(video);
  }

  void _onClickLivestream() async {
    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    }
    // NavigatorUtils.navigateToBroadcastPrepareLivestream();
    await logic.getTokenBroadcaster();
  }

  @override
  void dispose() {
    Get.delete<NewsfeedLogic>();
    Get.delete<StoriesController>();
    _controller.dispose();
    super.dispose();
  }

  @override
  bool get wantKeepAlive => true;
}
