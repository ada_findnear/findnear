import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:velocity_x/velocity_x.dart';

import '../../generated/l10n.dart';
import '../controllers/user_controller.dart';

class ContactUsWidget extends StatefulWidget {
  static const String routeName = '/ContactUs';

  @override
  _ContactUsWidgetState createState() => _ContactUsWidgetState();
}

class _ContactUsWidgetState extends StateMVC<ContactUsWidget> {
  _ContactUsWidgetState() : super(UserController());

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        resizeToAvoidBottomInset: false,
        body: VxBox(
          child: VStack(
            [_itemTopLogin(), _itemBody()],
          ),
        )
            .padding(EdgeInsets.only(top: Get.mediaQuery.viewPadding.top))
            .color(AppColors.red)
            .height(Get.height)
            .make(),
      );

  Widget _itemTopLogin() => ZStack([
        AppImage(
          AppImages.icMarkLogin,
          fit: BoxFit.cover,
          width: Get.width,
        ),
        Align(
            child: VStack([
              AppImage(AppImages.icLogin, width: Get.width * 0.4),
            ]),
            alignment: Alignment.center),
        Icon(
          Icons.arrow_back_ios,
          color: AppColors.white,
        ).paddingAll(16).onTap(goBack),
      ]).box.height(Get.height * 0.25).make();

  Widget _itemBody() => VxBox(
              child: VStack([
        _itemDescription(),
        _itemBack().paddingOnly(bottom: Get.mediaQuery.viewPadding.bottom),
      ]))
          .withDecoration(BoxDecoration(
              color: Vx.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(24), topRight: Radius.circular(24))))
          .make()
          .marginOnly(top: 8)
          .expand();

  Widget _itemDescription() => VxRichText(S.of(Get.context).forgot_1)
      .withTextSpanChildren([
        VelocityXTextSpan(S.of(Get.context).hot_line)
            .color(AppColors.red)
            .bold
            .size(17)
            .make(),
        VelocityXTextSpan(S.of(Get.context).forgot_2)
            .color(AppColors.textBlack)
            .size(17)
            .make()
      ])
      .textStyle(TextStyle(color: AppColors.textBlack, fontSize: 17))
      .make()
      .paddingAll(24)
      .expand();

  Widget _itemBack() => VxBox(
          child: S
              .of(Get.context)
              .go_back
              .toUpperCase()
              .text
              .size(18)
              .medium
              .color(AppColors.white)
              .make())
      .roundedLg
      .alignCenter
      .color(AppColors.red)
      .width(Get.width)
      .padding(EdgeInsets.symmetric(vertical: 8))
      .margin(EdgeInsets.symmetric(horizontal: 24, vertical: 16))
      .make()
      .onTap(() => goBack());
}
