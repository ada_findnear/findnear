import 'dart:async';

import 'package:findnear/src/helpers/app_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class FnStopWatch extends StatefulWidget {
  const FnStopWatch({Key key}) : super(key: key);

  @override
  _FnStopWatchState createState() => _FnStopWatchState();
}

class _FnStopWatchState extends State<FnStopWatch> {
  bool flag = true;
  Stream<int> timerStream;
  StreamSubscription<int> timerSubscription;
  String hourStr = '00';
  String minutesStr = '00';
  String secondsStr = '00';

  @override
  void initState() {
    super.initState();

    timerStream = stopWatchStream();
    timerSubscription = timerStream.listen((int newTick) {
      setState(() {
        hourStr = (((newTick / 60) / 60) % 60).floor().toString().padLeft(2, '0');
        minutesStr = ((newTick / 60) % 60).floor().toString().padLeft(2, '0');
        secondsStr = (newTick % 60).floor().toString().padLeft(2, '0');
      });
    });
  }

  Stream<int> stopWatchStream() {
    StreamController<int> streamController;
    Timer timer;
    Duration timerInterval = Duration(seconds: 1);
    int counter = 0;

    void stopTimer() {
      if (timer != null) {
        timer.cancel();
        timer = null;
        counter = 0;
        streamController.close();
      }
    }

    void tick(_) {
      counter++;
      streamController.add(counter);
      if (!flag) {
        stopTimer();
      }
    }

    void startTimer() {
      timer = Timer.periodic(timerInterval, tick);
    }

    streamController = StreamController<int>(
      onListen: startTimer,
      onCancel: stopTimer,
      onResume: startTimer,
      onPause: stopTimer,
    );

    return streamController.stream;
  }

  @override
  void dispose() {
    timerSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String time = '';
    if (hourStr != '0') {
      time = "$hourStr:$minutesStr:$secondsStr";
    } else {
      time = "$minutesStr:$secondsStr";
    }
    return Center(
      child: Text(
        time,
        style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w500,
          color: Color(0xffFF0000),
        ),
        textAlign: TextAlign.center,
      ),
    );
  }
}
