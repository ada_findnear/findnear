import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/shop.dart';
import 'package:get/get.dart';

class ListShopState extends BaseState {
  final _shops = List<Shop>.empty().obs;

  List<Shop> get shops => _shops.toList();

  set shops(value) {
    _shops.value = value;
  }
}
