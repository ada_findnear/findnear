import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/base/base_refresh_load_more.dart';
import 'package:findnear/src/models/response/shop_response.dart';
import 'package:findnear/src/models/shop.dart';
import 'package:findnear/src/pages/list_shop/view.dart';
import 'package:findnear/src/repository/shop_repository.dart';

import 'state.dart';

class ListShopController extends BaseController<ListShopState>
    with RefreshLoadMoreControllerMixin {
  ShopsRepository shopRepository;

  ListShopController(this.shopRepository);

  ListShopType type;
  double long;
  double lat;
  String fieldID;

  @override
  Future<bool> loadList() async {
    if (!canLoadMore) return true;
    try {
      if (currentPage == 1) state.isLoading = true;
      ShopsResponse response = await getList();
      if (response?.data?.data?.isNotEmpty == true) {
        canLoadMore = true;
        state.shops = List<Shop>.from(currentPage == 1
            ? List<Shop>.empty()
            : (state.shops ?? List<Shop>.empty()))
          ..addAll(response?.data?.data ?? List.empty());
      } else {
        canLoadMore = false;
      }
      state.isLoading = false;
      return true;
    } catch (e) {
      state.error = e;
      state.isLoading = false;
      return false;
    }
  }

  @override
  ListShopState createState() => ListShopState();

  Future<ShopsResponse> getList() async {
    if (type == ListShopType.recommended) {
      return await shopRepository.getRecommendedShops(page: currentPage++);
    } else {
      return await shopRepository.getNearShops(
          long: long, lat: lat, fieldID: fieldID, page: currentPage++);
    }
  }
}
