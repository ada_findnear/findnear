
import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/base/base_refresh_load_more.dart';
import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/pages/shop_detail/view.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'controller.dart';
import 'state.dart';

enum ListShopType { recommended, nearMe, hot }

class ListShopPage extends StatefulWidget {
  static const String ROUTE_NAME = '/ListShop';
  final RouteArgument routeArgument;

  ListShopPage({this.routeArgument});

  @override
  _ListShopPageState createState() => _ListShopPageState();
}

class _ListShopPageState
    extends BaseWidgetState<ListShopPage, ListShopController, ListShopState>
    with RefreshLoadMoreWidgetMixin {

  @override
  void onReady() {
    controller.type = widget.routeArgument.param['type'];
    controller.long = widget.routeArgument.param['long'];
    controller.lat = widget.routeArgument.param['lat'];
    controller.fieldID = widget.routeArgument.param['fieldID'] ?? '';
    controller.loadList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0,
        centerTitle: false,
        title: Text(
          _getTitle(),
          style: TextStyle(
            fontFamily: 'Quicksand',
            fontSize: 16,
            color: const Color(0xff333333),
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      body: _buildBody(),
    );
  }

  String _getTitle() {
    if (widget.routeArgument.param['type'] == ListShopType.nearMe) {
      return 'Dịch vụ gần tôi';
    }
    if (widget.routeArgument.param['type'] == ListShopType.recommended) {
      return 'Đề xuất cho bạn';
    }
    return widget.routeArgument.param['title'] ?? "";
  }

  Widget _buildShopItem(int index) {
    final shop = state.shops[index];
    return InkWell(
      onTap: () => NavigatorUtils.toShopDetailPage(shopId: shop.id),
          // Get.toNamed(ShopDetailPage.ROUTE_NAME,
          // arguments: RouteArgument(param: shop)),
      child: Container(
        child: Row(
          children: [
            Container(
              width: 80.0,
              height: 80.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                image: DecorationImage(
                  image: CachedNetworkImageProvider(shop.img_url),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(width: 10),
            Expanded(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  shop.title,
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 14,
                    color: const Color(0xff333333),
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Text(
                  '${shop.distance.toStringAsFixed(1)}km',
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 14,
                    color: const Color(0xff707070),
                    height: 1.4285714285714286,
                  ),
                ),
                /*RatingBar(
                  initialRating: shop.vote ?? 0,
                  direction: Axis.horizontal,
                  itemSize: 12,
                  ignoreGestures: true,
                  //allowHalfRating: true,
                  itemCount: 5,
                  ratingWidget: RatingWidget(
                    full: SvgPicture.asset('assets/svg/ic_star_filled.svg'),
                    half: SvgPicture.asset('assets/svg/ic_star_filled.svg'),
                    empty: SvgPicture.asset('assets/svg/ic_star_empty.svg'),
                  ),
                  itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                  onRatingUpdate: (rating) {
                    print(rating);
                  },
                ),*/
              ],
            )),
          ],
        ),
      ),
    );
  }

  Widget _buildBody() {
    return Obx(() {
      if (state.isLoading && state.shops.isBlank) {
        return Center(child: LoadingView());
      }
      if (state.shops.isBlank) {
        return Center(child: Text('Chưa có cửa hàng nào'));
      }
      return buildList(
          child: ListView.separated(
        itemBuilder: (context, index) {
          return _buildShopItem(index);
        },
        itemCount: state.shops.length,
        separatorBuilder: (_, __) => Divider(
          height: 30,
          color: AppColors.dividerColor,
          thickness: 1,
        ),
        padding: EdgeInsets.symmetric(horizontal: 12, vertical: 20),
      ));
    });
  }

  @override
  ListShopController createController() => ListShopController(Get.find());
}
