import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/media.dart';
import 'package:findnear/src/models/order.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/repository/chat_repository.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../models/user.dart' as userModel;
import '../../repository/friends_repository.dart' as friendRepo;
import '../../repository/post_repository.dart' as postRepo;
import '../../repository/user_repository.dart' as repository;

class MyProfileState {
  RxList<Order> recentOrders = List<Order>.empty().obs;
  RxList<Post> posts = List<Post>.empty().obs;
  RxInt currentOffset = 0.obs;
  RxInt postCount = 0.obs;
  RxBool canLoadMore = true.obs;

  // RxBool isLoading = false.obs;
  Rx<LoadStatus> fetchPostsStatus = LoadStatus.initial.obs;

  GlobalKey<ScaffoldState> scaffoldKey;
  Rx<Post> post = Post.fromJSON({}).obs;

  // RxBool isLoadingPost = true.obs;
  RxList<Media> listImage = List<Media>.empty().obs;
  RxList<Media> listVideo = List<Media>.empty().obs;
  Rx<userModel.User> myProfile = userModel.User.fromJSON({}).obs;

  String get firstImageUrl {
    if (posts.isEmpty) return null;
    var firstResult = posts.firstWhere(
            (post) =>
        post.medias != null &&
            post.medias.isNotEmpty &&
            post.medias.firstWhere(
                    (media) => media.mime_type.indexOf('image') != -1,
                orElse: () => null) !=
                null,
        orElse: () => null);
    if (firstResult != null)
      return firstResult.medias
          .firstWhere((element) => element.thumb != null, orElse: () => null)
          .thumb;
    return null;
  }

  String get firstVideoThumbUrl {
    if (posts.isEmpty) return null;
    var firstResult = posts.firstWhere(
        (post) =>
            post.medias != null &&
            post.medias.isNotEmpty &&
            post.medias.firstWhere(
                    (media) => media.mime_type.indexOf('video') != -1,
                    orElse: () => null) !=
                null,
        orElse: () => null);
    if (firstResult != null)
      return firstResult.medias
          .firstWhere((element) => element.thumb != null, orElse: () => null)
          .thumb;
    return null;
  }

  RxInt userId = 0.obs;
  RxBool showBackgroundHeader = false.obs;

  Conversation conversation;

  RouteArgument _routeArgument;

  ChatRepository _chatRepository = ChatRepository();

  MyProfileState() {
    ///Initialize variables
  }
}
