import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/models/user.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MyProfileBanner extends StatelessWidget {
  final User user;
  final VoidCallback onEditBioSelected;
  final VoidCallback onEditAvatarSelected;
  final VoidCallback onEditCoverSelected;

  MyProfileBanner({
    Key key,
    this.user,
    this.onEditBioSelected,
    this.onEditAvatarSelected,
    this.onEditCoverSelected,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.transparent.withAlpha(0),
      child: Column(
        children: [
          Container(
            height: 250,
            child: Stack(
              children: [
                _coverWidget(context),
                Positioned.fill(
                  bottom: -170,
                  child: Center(
                    child: _avatarWidget(context),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 6),
          (user?.bio ?? "").isEmpty ? _bioEmpty(context) : _bioText(context),
          SizedBox(height: 6),
        ],
      ),
    );
  }

  Widget _bioText(BuildContext context) {
    return new GestureDetector(
      onTap: onEditBioSelected,
      child: Padding(
        padding: const EdgeInsets.only(left: 20.0, right: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              fit: FlexFit.loose,
              child: Padding(
                padding: EdgeInsets.only(right: onEditBioSelected != null ? 5 : 0),
                child: Text(
                  user?.bio ?? "",
                  style: TextStyle(
                    fontSize: 12.0,
                    color: Theme.of(context).accentColor,
                  ),
                ),
              ),
            ),
            SvgPicture.asset(
              "assets/svg/ic_pencil.svg",
              color: Theme.of(context).accentColor,
              width: 10,
              height: 10,
              fit: BoxFit.contain,
            )
          ],
        ),
      ),
    );
  }

  Widget _bioEmpty(BuildContext context) {
    return new GestureDetector(
      onTap: () {
        if (onEditBioSelected != null) {
          onEditBioSelected();
        }
      },
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset(
              "assets/svg/ic_pencil.svg",
              color: Theme.of(context).accentColor,
              width: 10,
              height: 10,
              fit: BoxFit.contain,
            ),
            SizedBox(
              width: 4,
            ),
            Text(
              S.of(context).add_your_bio,
              style: TextStyle(
                fontSize: 12.0,
                color: Theme.of(context).accentColor,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _coverWidget(BuildContext context) {
    return GestureDetector(
      onTap: onEditCoverSelected,
      child: Stack(
        children: [
          Container(
            height: 220,
            width: double.infinity,
            child: CachedNetworkImage(
              imageUrl: user?.imageCover?.url ?? "",
              fit: BoxFit.cover,
              height: 220,
              // width: double.infinity,
              errorWidget: (context, url, error) =>
                  (user?.imageCover?.thumb?.isNotEmpty ?? false)
                      ? Image.network(
                          user?.imageCover?.thumb,
                          fit: BoxFit.cover,
                          height: 220,
                        )
                      : Container(
                          width: double.infinity,
                          height: 220,
                          color: AppColors.gray,
                        ),
            ),
          ),
          Positioned(
            bottom: 12,
            right: 12,
            child: _iconCameraWidget(
              context,
              iconColor: Colors.red,
              bg: Colors.white.withOpacity(0.797),
              onPressed: onEditCoverSelected,
            ),
          )
        ],
      ),
    );
  }

  Widget _avatarWidget(BuildContext context) {
    return Container(
      width: 80,
      height: 80,
      child: Stack(
        children: [
          Container(
            width: 80,
            height: 80,
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.white,
                width: 4,
              ),
              shape: BoxShape.circle,
            ),
            child: new GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: onEditAvatarSelected,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: CachedNetworkImage(
                  imageUrl: user?.image?.icon ?? "",
                  fit: BoxFit.cover,
                  width: 80,
                  height: 80,
                  errorWidget: (context, url, error) => ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: Image.network(
                      url,
                      fit: BoxFit.cover,
                      width: 80,
                      height: 80,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            right: 5,
            child: _iconCameraWidget(context, onPressed: onEditAvatarSelected),
          ),
        ],
      ),
    );
  }

  Widget _iconCameraWidget(BuildContext context, {Color bg, Color iconColor, VoidCallback onPressed}) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        width: 26,
        height: 26,
        padding: EdgeInsets.all(5),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: (bg ?? Colors.red).withOpacity(0.767),
        ),
        child: SvgPicture.asset(
          "assets/svg/ic_photo_camera.svg",
          color: iconColor ?? Colors.white,
          fit: BoxFit.contain,
        ),
      ),
    );
  }
}
