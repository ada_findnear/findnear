import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../models/user.dart' as userModel;
import '../../repository/friends_repository.dart' as friendRepo;
import '../../repository/post_repository.dart' as postRepo;
import '../../repository/user_repository.dart' as repository;

import 'my_profile_state.dart';

class MyProfileLogic extends GetxController {
  final state = MyProfileState();

  void fetchInitialData() {
    //Clear data
    state.posts.clear();
    state.recentOrders.clear();
    state.canLoadMore.value == true;
    state.currentOffset.value = 0;

    listenForUser(int.parse(repository.currentUser?.value?.id), true);
    listenForPosts(repository.currentUser?.value?.id);
    state.userId.value = int.parse(repository.currentUser?.value?.id);
    state.myProfile.value = repository.currentUser?.value;
  }

  void fetchNextPageData() {
    if (state.canLoadMore.value == false) {
      return;
    }
    listenForPosts(repository.currentUser?.value?.id);
  }

  void listenForPosts(String user_id, {String message}) async {
    if (state.fetchPostsStatus.value == LoadStatus.loading) {
      return;
    }
    state.fetchPostsStatus.value = LoadStatus.loading;
    final Stream<Post> stream = await postRepo.getPost(offset: state.currentOffset, userId: user_id, limit: 10);
    stream.listen(
      (Post _post) {
        state.posts.add(_post.copyWith(creator: repository.currentUser.value));
      },
      onError: (a) {
        state.fetchPostsStatus.value = LoadStatus.failure;
        ScaffoldMessenger.of(state.scaffoldKey.currentContext).showSnackBar(SnackBar(
          content: Text(S.of(Get.context).verify_your_internet_connection),
        ));
      },
      onDone: () {
        if (state.currentOffset.value == state.posts.length) {
          state.canLoadMore.value = false;
        }
        state.currentOffset.value = state.posts.length;
        state.fetchPostsStatus.value = LoadStatus.success;

        if (message != null) {
          ScaffoldMessenger.of(state.scaffoldKey.currentContext).showSnackBar(SnackBar(
            content: Text(message),
          ));
        }
      },
    );
  }

  void listenForPost(BuildContext context, postId, index, {String message}) async {
    final Stream<Post> stream = await postRepo.getPostById(postId);

    stream.listen((Post _post) {
      state.posts[index] = _post;
    }, onError: (a) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(S.of(context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (message != null) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  void getListMedia(BuildContext context, user_id, {String message}) async {
    final Stream<Post> stream = await postRepo.getPost(userId: user_id);
    stream.listen((Post _post) {
      if (_post.medias.length > 0) {
        _post.medias.forEach((element) {
          if (element.mime_type.indexOf("image") != -1) {
            state.listImage.add(element);
          } else if (element.mime_type.indexOf("video") != -1) {
            state.listVideo.add(element);
          }
        });
      }
    }, onError: (a) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(S.of(context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (message != null) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  void createPost(BuildContext context, Post post, pathImages) async {
    postRepo.addPost(post, pathImages).then((value) {
      if (value != Post.fromJSON({})) {
        state.posts.insert(0, value);
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(S.of(context).createPostSuccessful),
        ));
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(S.of(context).verify_your_internet_connection),
        ));
      }
    });
  }

  void updatePost(BuildContext context, Post post) async {
    postRepo.updatePost(post).then((value) {
      var dataPosts = state.posts;
      var indexOfPost = state.posts.indexWhere((element) => element.id == post.id);
      if (indexOfPost != -1) {
        dataPosts[indexOfPost] = value;
        state.posts = dataPosts;
      }
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(S.of(context).updatePostSuccessful),
      ));
    });
  }

  void updateUser({String pathImage = null, String imageType = null}) async {
    state.myProfile.value.deviceToken = null;
    repository.update(user: state.myProfile.value, pathImage: pathImage, imageType: imageType).then((value) {
      state.myProfile.value = value;
      ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
        content: Text(S.of(Get.context).profile_settings_updated_successfully),
      ));
    });
  }

  void listenForUser(userID, bool isMyProfile) async {
    if (userID != null) {
      final userModel.User data = await repository.getUser(userID);
      if (!isMyProfile) {
        state.myProfile.value = data;
      }
    }
  }

  void toggleLikePost(postID, {String message}) async {
    postRepo.toggleLikePost(postID).then((value) {
      var indexOfPost = state.posts.indexWhere((element) => element.id == postID);
      if (value) {
        state.posts[indexOfPost].likers_count = (int.parse(state.posts[indexOfPost].likers_count) + 1).toString();
        state.posts[indexOfPost].isLike = true;
      } else {
        state.posts[indexOfPost].likers_count = (int.parse(state.posts[indexOfPost].likers_count) - 1).toString();
        state.posts[indexOfPost].isLike = false;
      }
      state.posts.refresh();
    });
  }

  void removePost(BuildContext context, Post post) async {
    postRepo.removePost(post).then((value) {
      if (value) {
        var dataPosts = state.posts;
        var indexOfPost = state.posts.indexWhere((element) => element.id == post.id);
        if (indexOfPost != -1) {
          dataPosts.removeAt(indexOfPost);

          state.posts = dataPosts;
        }
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(S.of(context).removePostSuccessful),
        ));
      }
    });
  }

  void addFriend(BuildContext context, String idUserTarget, String type) async {
    friendRepo.addFriend(idUserTarget, type).then((value) {
      this.listenForUser(int.parse(state.myProfile.value.id), false);
      switch (type) {
        case "add":
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(S.of(context).friendRequestSentSuccessfully)));
          break;
        case "confirm":
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(S.of(context).confirmationOfSuccessfulFriendRequest)));
          break;
        case "deny":
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(S.of(context).friendRequestDenied)));
          break;
        case "cancel":
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(S.of(context).friendRequestHasBeenCanceled)));
          break;
        case "delete":
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(S.of(context).successfulUnfriend)));
          break;
      }
    });
  }

  void updateShowBGHeader(double pixels) {
    state.showBackgroundHeader.value = pixels > 200;
  }

  void deletePost({int index}) async {
    final post = state.posts.elementAt(index);
    try {
      final result = await postRepo.removePost(post);
      if (result) {
        state.posts.removeAt(index);
        fetchInitialData();
      }
    } catch (e) {
      logger.e(e);
    }

  }
}
