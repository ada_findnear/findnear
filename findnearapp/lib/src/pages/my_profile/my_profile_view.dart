import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/AlbumItemWidget.dart';
import 'package:findnear/src/elements/MediaActionSheetWidget.dart';
import 'package:findnear/src/elements/NewsfeedItemWidget.dart';
import 'package:findnear/src/elements/PermissionDeniedWidget.dart';
import 'package:findnear/src/elements/ProfileWriteTopicWidget.dart';
import 'package:findnear/src/models/bottom_sheet_option.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/models/shop.dart';
import 'package:findnear/src/pages/create_post/create_post.controller.dart';
import 'package:findnear/src/pages/create_post/create_post.screen.dart';
import 'package:findnear/src/pages/detail_topic.dart';
import 'package:findnear/src/pages/preview_media.dart';
import 'package:findnear/src/pages/profile_list_photo.dart';
import 'package:findnear/src/pages/profile_list_video.dart';
import 'package:findnear/src/pages/profile_user/widgets/profile_action_button.dart';
import 'package:findnear/src/pages/register_shop_page/register_shop_page.dart';
import 'package:findnear/src/pages/shop_detail/view.dart';
import 'package:findnear/src/pages/write_topic.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:findnear/src/widgets/dialog/expired_account_dialog.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import 'my_profile_logic.dart';
import 'my_profile_state.dart';
import 'widgets/my_profile_banner.dart';

class MyProfilePage extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  MyProfilePage({
    this.parentScaffoldKey,
  });

  @override
  _MyProfilePageState createState() => _MyProfilePageState();
}

class _MyProfilePageState extends State<MyProfilePage>
    with AutomaticKeepAliveClientMixin {
  MyProfileLogic logic;
  MyProfileState state;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  MediaActionSheetWidget _mediaActionSheetWidget;

  final _scrollController = ScrollController();
  final _scrollThreshold = 200.0;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      logic = Get.put(MyProfileLogic());
      state = Get
          .find<MyProfileLogic>()
          .state;
      logic.fetchInitialData();
      setState(() {});
    });
    DialogHelper.initFToast(context: context);
  }

  @override
  void dispose() {
    Get.delete<MyProfileLogic>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    if (state != null && _scaffoldKey != null) {
      state.scaffoldKey = _scaffoldKey;
    }
    _mediaActionSheetWidget = MediaActionSheetWidget(context: context);
    final isDark = Theme
        .of(context)
        .brightness == Brightness.dark;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: isDark
          ? Theme
          .of(context)
          .scaffoldBackgroundColor
          : Color(0xFFF8F8F8),
      appBar: _buildAppBar(),
      extendBodyBehindAppBar: true,
      body: _buildBodyWidget(),
      floatingActionButton: _buildProfileShopWidget(),
    );
  }

  PreferredSize _buildAppBar() {
    return PreferredSize(
      preferredSize: Size(Get.width, 40),
      child: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                // 10% of the width, so there are ten blinds.
                colors: <Color>[
                  Color(0xFF333333),
                  Color(0x00000000)
                ], // red to yellow
              ),
            ),
          ),
          AppBar(
            backgroundColor: Colors.transparent,
            centerTitle: true,
            elevation: 0,
            leadingWidth: 52,
            leading: IconButton(
              icon: SvgPicture.asset(
                AppImages.icHamburger,
                color: Theme
                    .of(context)
                    .accentColor,
              ),
              onPressed: () =>
                  widget.parentScaffoldKey.currentState.openDrawer(),
            ),
            actions: [
              (currentUser.value?.apiToken != null)
                  ? IconButton(
                onPressed: () {
                  Navigator.of(context).pushNamed('/Settings');
                },
                icon: SvgPicture.asset(
                  "assets/svg/ic_menu.svg",
                  color: Colors.white,
                ),
              )
                  : SizedBox()
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildBodyWidget() {
    if (logic == null) return Center(child: LoadingView());
    return currentUser.value?.apiToken == null
        ? PermissionDeniedWidget()
        : _timelineContainer();
    // : SingleChildScrollView(
    //     child: Column(
    //       crossAxisAlignment: CrossAxisAlignment.center,
    //       children: [
    //         _buildBannerWidget(),
    //         Padding(
    //           padding: const EdgeInsets.only(left: 22, right: 13),
    //           child: Column(
    //             children: [
    //               _mediaContainer(),
    //               _addTopicContainer(),
    //               _timelineContainer(),
    //               _loadMoreWidget(),
    //             ],
    //           ),
    //         )
    //       ],
    //     ),
    //   );
  }

  Widget _buildTopWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        _buildBannerWidget(),
        _mediaContainer(),
        _addTopicContainer(),
      ],
    );
  }

  Widget _mediaContainer() {
    return Container(
      height: 80.0,
      margin: EdgeInsets.only(top: 11),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            AlbumItemWidget(
              icon: Icons.photo,
              title: S
                  .of(context)
                  .myPhoto,
              desc: S
                  .of(context)
                  .seeAllPhoto,
              onSelected: () {
                Get.to(() => ProfileListPhotoWidget());
              },
              grStartColor: Color.fromRGBO(252, 36, 36, 1),
              grMiddleColor: Color.fromRGBO(252, 82, 82, 0.8),
              grEndColor: Color.fromRGBO(255, 181, 181, 0.3),
              imgAsset: 'assets/svg/ic_photo.svg',
              backgroundUrl: state.firstImageUrl,
              // imgAsset: "assets/svg/ic_photo.svg",
            ),
            SizedBox(
              width: 10,
            ),
            AlbumItemWidget(
              icon: Icons.videocam,
              title: S
                  .of(context)
                  .myVideo,
              desc: S
                  .of(context)
                  .seeAllVideo,
              onSelected: () {
                Get.to(() => ProfileListVideoWidget());
              },
              grStartColor: Color.fromRGBO(0, 141, 76, 1),
              grMiddleColor: Color.fromRGBO(119, 180, 123, 0.8),
              grEndColor: Color.fromRGBO(255, 181, 181, 0.3),
              imgAsset: "assets/svg/ic_video_camera.svg",
              backgroundUrl: state.firstVideoThumbUrl,
            ),
          ],
        ),
      ),
    );
  }

  Widget _addTopicContainer() {
    return Container(
      margin: EdgeInsets.only(top: 20),
      child: GestureDetector(
        onTap: () {
          _writeTopic();
        },
        child: ProfileWriteTopicWidget(),
      ),
    );
  }

  Widget _timelineContainer() {
    return Obx(() {
      return RefreshIndicator(
        onRefresh: _onRefreshData,
        child: ListView.separated(
          controller: _scrollController,
          padding: EdgeInsets.zero,
          itemCount: state.posts.length + 1,
          itemBuilder: (BuildContext context, int index) {
            if (index == 0) {
              return _buildTopWidget();
            }
            final postIndex = index - 1;
            final post = state.posts.elementAt(postIndex);
            if(post.creator == null && post.userId == currentUser.value.id){
              post.creator = currentUser.value;
            }
            prettyLog.d("index: $postIndex\n${post.toString()}");
            return Container(
              color: Colors.white,
              padding: const EdgeInsets.only(top: 13, bottom: 17, left: 15, right: 15),
              child: Hero(
                tag: 'preview_${postIndex ?? 0}',
                child: NewsfeedItemWidget(
                  onPreviewPhoto: (url) {
                    print("onPreviewPhoto");
                    print(url);
                    _previewTopicPhoto(
                      url,
                    );
                  },
                  onMyPostMenuPressed: () async {
                    final indexTapped =
                    await DialogHelper.showBottomSheetOptions(
                      context: context,
                      options: [
                        BottomSheetOption(
                          iconPath: AppImages.icWrite,
                          text: S
                              .of(context)
                              .edit_post,
                        ),
                        BottomSheetOption(
                          iconPath: AppImages.icTrash,
                          text: S
                              .of(context)
                              .delete_post,
                        ),
                        BottomSheetOption(
                          icon: Icons.share,
                          text: S
                              .of(context)
                              .shareThisPost,
                        ),
                        // temporarily disable
                        // BottomSheetOption(
                        //   iconPath: AppImages.icBookmarkOutlined,
                        //   text: S.of(context).save_post,
                        // ),
                      ],
                    );
                    switch (indexTapped) {
                      case 0:
                        _onClickEditPost(state.posts.elementAt(index - 1));
                        break;
                      case 1:
                        final isDeleting = await DialogHelper.showConfirmPopup(
                          title: S.current.deletePost,
                          description: S.current.confirmDeleteDescription,
                          okBtnText: S.current.deletePostShort,
                        );

                        if (isDeleting ?? false) logic.deletePost(
                            index: index - 1);

                        break;
                      case 2:
                        onClickSharePostOtherApp();
                        break;
                    }
                  },
                  post: post,
                  toggleLikePost: (postID) {
                    logic.toggleLikePost(postID);
                  },
                  onSharePost:(){
                    _onClickSharePost(post);
                  } ,
                  isMyProfile: true,
                  didUpdateCallback: () async {
                    await _onRefreshData();
                  },
                ),
              ),
            );
          },
          separatorBuilder: (context, index) =>
          index == 0
              ? SizedBox(height: 16)
              : const Divider(
              height: 8, thickness: 8, color: AppColors.divider),
          // shrinkWrap: true,
          scrollDirection: Axis.vertical,
        ),
      );
      // return state.isLoadingPost.value
      //     ? PostsLoaderWidget(type: "post")
      //     : (state.posts.length == 0)
      //         ? NoDataPostsWidget(isMyProfile: true)
      //         : GetBuilder<ProfileV2Controller>(
      //             builder: (logic) {
      //               return ;
      //             },
      //           );
    });
  }

  void _editAvatar(BuildContext context, String url) async {
    _mediaActionSheetWidget.showActionSheet(S
        .of(context)
        .avatar, [
      S
          .of(context)
          .viewProfilePicture,
      S
          .of(context)
          .takePhoto,
      S
          .of(context)
          .chooseFromGrallery
    ], (int index) {
      if (index == 0) {
        _previewAvatar(url);
      }
      if (index == 1) {
        _openCamera((pickedFile) {
          logic.updateUser(pathImage: pickedFile.path, imageType: 'avatar');
        });
      }
      if (index == 2) {
        _openGrallery((pickedFile) {
          logic.updateUser(pathImage: pickedFile.path, imageType: 'avatar');
        });
      }
    });
  }

  void _editBio(BuildContext context) {
    Navigator.of(context)
        .push(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) =>
            WriteTopicWidget(
              isWriteTopic: false,
              updateUser: () {
                state.myProfile.value?.bio = currentUser.value?.bio ?? "";
                logic.updateUser();
              },
            ),
      ),
    )
        .then((value) {
      // _con.update(user: dataUser);
    });
  }

  void _writeTopic() {
    Navigator.of(context)
        .push(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) =>
            WriteTopicWidget(
              isWriteTopic: true,
              handleSubmit: (post, pathImages) {
                logic.createPost(context, post, pathImages);
              },
            ),
      ),
    )
        .then((value) {});
  }

  void _editTopic(BuildContext context, Post post) {
    Navigator.of(context).push(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) =>
            WriteTopicWidget(
                isWriteTopic: true,
                handleSubmit: (post, pathImages) {
                  logic.updatePost(context, post);
                },
                currentPost: post),
      ),
    );
  }

  void _editCover(BuildContext context, String url) async {
    await _mediaActionSheetWidget.showActionSheet(S
        .of(context)
        .cover, [
      S
          .of(context)
          .viewFullCover,
      S
          .of(context)
          .takePhoto,
      S
          .of(context)
          .chooseFromGrallery
    ], (int index) {
      if (index == 0) {
        _previewCover(url);
      }
      if (index == 1) {
        _openCamera((pickedFile) {
          logic.updateUser(pathImage: pickedFile.path, imageType: 'cover');
        });
      }
      if (index == 2) {
        _openGrallery((pickedFile) {
          logic.updateUser(pathImage: pickedFile.path, imageType: 'cover');
        });
      }
    });
  }

  void _openTopicSetting(BuildContext context, Post post) async {
    await _mediaActionSheetWidget.showActionSheet("Options", [
      S
          .of(context)
          .privacy,
      S
          .of(context)
          .editCaption,
      S
          .of(context)
          .deleteActivity
    ], (int index) {
      if (index == 0) {
        _mediaActionSheetWidget.showActionSheet(S
            .of(context)
            .privacy, [
          S
              .of(context)
              .viewerPrivate,
          S
              .of(context)
              .viewerOnlyFriend,
          S
              .of(context)
              .viewerPublic
        ], (int index) {
          logic.updatePost(
              context,
              new Post.fromJSON(
                  {'id': post.id, 'content': post.content, 'viewer': index}));
        });
      }
      if (index == 1) {
        _editTopic(context, post);
      }
      if (index == 2) {
        showDialog<String>(
            context: context,
            builder: (BuildContext context) =>
                AlertDialog(
                  title: Text(S
                      .of(context)
                      .confirm),
                  content: Text(S
                      .of(context)
                      .confirmRemovePost),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () => Navigator.pop(context, 'Cancel'),
                      child: Text(S
                          .of(context)
                          .cancel),
                    ),
                    TextButton(
                      onPressed: () => _submitRemovePost(context, post),
                      child: Text(S
                          .of(context)
                          .ok),
                    ),
                  ],
                ));
      }
    });
  }

  void _submitRemovePost(BuildContext context, post) {
    Navigator.pop(context, 'OK');
    logic.removePost(context, post);
  }

  void _openGrallery(OnPickImageCallback onPickImageCallback) {
    _mediaActionSheetWidget.pickImageDialog(ImageSource.gallery,
            (PickedFile pickedFile) {
          // setState(() {
          onPickImageCallback(pickedFile);
          // });
        });
  }

  void _openCamera(OnPickImageCallback onPickImageCallback) {
    _mediaActionSheetWidget.pickImageDialog(ImageSource.camera,
            (PickedFile pickedFile) {
          // setState(() {
          onPickImageCallback(pickedFile);
          // });
        });
  }

  void _previewTopicPhoto(String url, {String thumbUrl}) {
    print("_previewTopicPhoto");
    String path = url;
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) =>
            PreviewMediaWidget(
              photoPath: path,
              thumbPath: thumbUrl,
            ),
      ),
    );
  }

  void _previewAvatar(String url) {
    Navigator.of(context).push(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) => PreviewMediaWidget(photoPath: url),
      ),
    );
  }

  void _previewCover(String url) {
    Navigator.of(context).push(CupertinoPageRoute(
      fullscreenDialog: true,
      builder: (context) => PreviewMediaWidget(photoPath: url),
    ));
  }

  void _openDetailTopic(BuildContext context,
      {post, profileController, userId, index}) async {
    print("_openDetailTopic");
    final result = await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext context) =>
            DetailTopicWidget(
              post: post,
            ),
      ),
    );

    if (result != null) {
      await logic.listenForPost(context, post.id, index);
    }
  }

  void _onClickSharePost(Post post) async {
    final originalPost = post.isSharedPost ? post.originalPost : post;
    Get.put(CreatePostController(originalPost: originalPost));
    var res = await Get.to(() =>
        CreatePostScreen(
          user: currentUser.value,
          originalPost: originalPost,
        ));
    Get.delete<CreatePostController>();
    if (res != null) logic.fetchInitialData();
  }

  void onClickSharePostOtherApp() async {
    DialogHelper.showAlertDialog(
      context: context,
      description: "Chức năng đang trong quá trình hoàn thiện",
      title: S.current.notifications,
      okText: 'OK',
      okFunction: () {

      },
    );
  }

  @override
  bool get wantKeepAlive => true;

  Widget _buildBannerWidget() {
    return Obx(() {
      return MyProfileBanner(
        user: state.myProfile.value,
        onEditBioSelected: () {
          _editBio(context);
        },
        onEditAvatarSelected: () {
          _editAvatar(context, state.myProfile?.value?.image?.url);
        },
        onEditCoverSelected: () {
          _editCover(context, state.myProfile?.value?.imageCover?.url);
        },
      );
    });
  }

  Widget _buildProfileShopWidget() {
    if (state?.myProfile?.value?.shop?.id != null && state.myProfile.value.canCreateShop == true) {
      return IconButton(
        iconSize: 72.w,
        icon: SvgPicture.asset(AppImages.icMyStore),
        onPressed: () {
          Shop myShop = state.myProfile.value.shop;
          // Get.toNamed(ShopDetailPage.ROUTE_NAME,
          //     arguments: RouteArgument(param: myShop));
          NavigatorUtils.toShopDetailPage(shopId: myShop.id);
        },
      );
    }/* TODO: wait for BE's logic for disabling/enabling shop
    else if (state?.myProfile?.value?.canCreateShop ?? false) {
      return IconButton(
        iconSize: 72.w,
        icon: SvgPicture.asset(AppImages.icAddStore),
        onPressed: () {
          if (currentUser.value.canCreateShop) {
            Get.to(RegisterShopPage());
          } else {
            showDialog(
              context: context,
              barrierDismissible: true,
              builder: (BuildContext context) => contactUsDialog(),
            );
          }
        },
      );
    }*/
    return Container();
  }

  Widget _loadMoreWidget() {
    return !state.canLoadMore.value
        ? SizedBox()
        : Container(
      padding: EdgeInsets.only(top: 20, bottom: 10),
      child: Image.asset(
        'assets/img/loading_load_more.gif',
        height: 30,
        width: 30,
      ),
    );
  }

  Future<void> _onRefreshData() async {
    logic.fetchInitialData();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      logic.fetchNextPageData();
    }
  }

  void _onClickEditPost(Post post) async {
    Get.put(CreatePostController(post: post));
    final result = await Get.to(
            () => CreatePostScreen(user: currentUser.value));
    Get.delete<CreatePostController>();
    if (result != null) logic.fetchInitialData();
  }
}
