import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/entities/place_entity.dart';
import 'package:findnear/src/models/market.dart';
import 'package:findnear/src/repository/settings_repository.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import '/../../../generated/l10n.dart';

class PlaceResultWidget extends StatelessWidget {
  final PlaceEntity place;
  final VoidCallback onPressed;
  final VoidCallback onPressedDirection;

  PlaceResultWidget({
    Key key,
    this.place,
    this.onPressed,
    this.onPressedDirection,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              SizedBox(width: 27),
              AppImage(AppImages.icLocationGray, width: 38, height: 38),
              SizedBox(width: 10),
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      place.name ?? "",
                      overflow: TextOverflow.ellipsis,
                      softWrap: true,
                      maxLines: 2,
                      style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 14, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 4),
                    Text(
                      place.formattedAddress ?? "",
                      maxLines: 2,
                      softWrap: true,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 12),
                    ),
                  ],
                ),
              ),
              SizedBox(width: 10),
              GestureDetector(
                onTap: onPressedDirection,
                child: AppImage(AppImages.icDirectionSearch
                    , width: 22, height: 22),
              ),
              SizedBox(width: 27),
            ],
          ),
          SizedBox(height: 8),
          Divider(
            height: 1,
            color: Colors.grey,
            indent: 27,
          ),
        ],
      ),
    );
  }
}
