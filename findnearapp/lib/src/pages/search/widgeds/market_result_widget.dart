import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/market.dart';
import 'package:findnear/src/repository/settings_repository.dart';
import 'package:flutter/material.dart';
import '/../../../generated/l10n.dart';

class MarketResultWidget extends StatelessWidget {
  final Market market;
  final VoidCallback onPressed;

  MarketResultWidget({
    Key key,
    this.market,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              SizedBox(width: 27),
              Container(
                width: 40,
                height: 40,
                child: CircleAvatar(
                  backgroundImage: NetworkImage(market.owner?.image?.thumb ?? ""),
                  radius: 45 / 2,
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(45 / 2),
                ),
              ),
              SizedBox(width: 10),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      market.owner?.name ?? "",
                      overflow: TextOverflow.fade,
                      softWrap: false,
                      style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 14, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 2),
                    Text(
                      "ID: ${market.owner?.code ?? ""}",
                      style: Theme.of(context).textTheme.headline6.copyWith(fontSize: 14, fontWeight: FontWeight.normal),
                    ),
                    (market.address ?? "").isNotEmpty ? Row(
                      children: [
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(top: 7),
                            child: Text(
                              market.address ?? "",
                              maxLines: 1,
                              style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 12),
                            ),
                          ),
                        ),
                        SizedBox(width: 4),
                      ],
                    ) : Container(),
                  ],
                ),
              ),
              SizedBox(width: 8),
              Image.asset(AppImages.icCallTiny),
              SizedBox(width: 6),
              Image.asset(AppImages.icMessageTiny),
              SizedBox(width: 26),
            ],
          ),
          SizedBox(height: 5),
          Container(
            margin: EdgeInsets.only(left: (27.0 + 40 + 10)),
            child: Text(
              Helper.getDistance(market.distance, Helper.of(context).trans(text: setting.value.distanceUnit)),
              style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 12, color: Theme.of(context).textTheme.bodyText1.color.withOpacity(0.6)),
            ),
          ),
          SizedBox(height: 8),
          Divider(
            height: 1,
            color: Colors.grey,
            indent: 27,
          ),
        ],
      ),
    );
  }
}
