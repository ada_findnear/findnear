import 'package:findnear/main.dart';
import 'package:findnear/src/widgets/shimmer/app_shimmer.dart';
import 'package:flutter/material.dart';

class SearchPlaceLoadingWidget extends StatelessWidget {
  const SearchPlaceLoadingWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        return Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(height: 10),
              Container(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    SizedBox(width: 27),
                    AppShimmer(width: 40, height: 40, cornerRadius: 4),
                    SizedBox(width: 12),
                    Expanded(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          AppShimmer(width: 100, height: 18, cornerRadius: 9),
                          SizedBox(height: 10),
                          AppShimmer(width: 250, height: 14, cornerRadius: 8),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 8),
              Divider(
                height: 1,
                color: Colors.grey,
                indent: 27,
              ),
            ],
          ),
        );
      },
      itemCount: 20,
    );
  }
}
