import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/market.dart';
import 'package:findnear/src/models/search_user_entity.dart';
import 'package:findnear/src/repository/settings_repository.dart';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import '/../../../generated/l10n.dart';

class FriendResultWidget extends StatelessWidget {
  final SearchUserEntity user;
  final VoidCallback onPressed;

  FriendResultWidget({
    Key key,
    this.user,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              SizedBox(width: 27),
              Container(
                width: 40,
                height: 40,
                child: CircleAvatar(
                  backgroundImage: NetworkImage(
                      (user.media.isEmpty) ?
                      '${GlobalConfiguration().getValue('base_url')}images/image_default.png':
                      user.media[0].thumb),
                  radius: 45 / 2,
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(45 / 2),
                ),
              ),
              SizedBox(width: 10),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      user?.name ?? "",
                      overflow: TextOverflow.fade,
                      softWrap: false,
                      style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 14, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 2),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "ID: ${user?.code ?? ""}",
                          style: Theme.of(context).textTheme.headline6.copyWith(fontSize: 14, fontWeight: FontWeight.normal),
                        ),
                        SizedBox(width: 8),
                        Row(
                          children: [
                            Image.asset(AppImages.icCallTiny),
                            SizedBox(width: 8),
                            Image.asset(AppImages.icMessageTiny),
                          ],
                        ),
                      ],
                    ),

                    SizedBox(height: 5),
                    Container(
                      child: Text(
                        Helper.getDistance(user.distance ?? 69, Helper.of(context).trans(text: setting.value.distanceUnit)),
                        style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 12, color: Theme.of(context).textTheme.bodyText1.color.withOpacity(0.6)),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(width: 26),
            ],
          ),
          SizedBox(height: 8),
          Divider(
            height: 1,
            color: Colors.grey,
            indent: 27,
          ),
        ],
      ),
    );
  }
}
