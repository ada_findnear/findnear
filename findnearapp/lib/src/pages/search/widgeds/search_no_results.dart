import 'package:findnear/generated/l10n.dart';
import 'package:flutter/material.dart';

class SearchNoResultsWidget extends StatelessWidget {
  const SearchNoResultsWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        S.of(context).no_result,
        style: TextStyle(
          fontSize: 18,
          color: Colors.grey,
        ),
      ),
    );
  }
}
