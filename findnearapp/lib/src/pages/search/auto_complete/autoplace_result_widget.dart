import 'package:findnear/src/models/entities/place_entity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AutoPlaceResultWidget extends StatelessWidget {
  final AddressAutoEntity place;
  final VoidCallback onPressed;

  AutoPlaceResultWidget({
    Key key,
    this.place,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 8),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: [
              SizedBox(width: 27,),
              Expanded(
                child: Text(
                  place.description ?? "",
                  overflow: TextOverflow.ellipsis,
                  softWrap: true,
                  maxLines: 2,
                  style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 14, fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(width: 27,),
            ],
          ),


          SizedBox(height: 8),
          Divider(
            height: 1,
            color: Colors.grey,
            indent: 27,
          ),
        ],
      ),
    );
  }

}