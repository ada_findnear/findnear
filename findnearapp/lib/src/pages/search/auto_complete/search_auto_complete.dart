import 'dart:math';

import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/entities/place_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/pages/search/auto_complete/autoplace_result_widget.dart';
import 'package:findnear/src/pages/search/auto_complete/controller.dart';
import 'package:findnear/src/pages/search/auto_complete/state.dart';
import 'package:findnear/src/pages/search/widgeds/search_no_results.dart';
import 'package:findnear/src/pages/search/widgeds/search_place_loading_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AutoCompletePlacePage extends StatefulWidget {
  static const String ROUTE_NAME = '/auto_complete';
  final RouteArgument routeArgument;

  AutoCompletePlacePage({this.routeArgument}) ;

  @override
  _AutoCompletePlaceState createState() => _AutoCompletePlaceState();
}

class _AutoCompletePlaceState extends BaseWidgetState<AutoCompletePlacePage,
    AutoCompletePlaceController, AutoCompletePlaceState> {

  final _keySearch = "".obs;
  var delaySearch;
  Function callback ;

  @override
  void initState() {
    super.initState();
    callback = widget.routeArgument.param;
    delaySearch = debounce(_keySearch, (value){
        controller.searchPlace(allowEmptyKeyword: false);
    }, time: Duration(milliseconds: 1000));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: SafeArea(child: _buildBody(),),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Theme.of(context).brightness == Brightness.dark
              ? Image.asset(
            AppImages.icBackDark,
          )
              : Image.asset(
            AppImages.icBackLight,
          )),
      leadingWidth: 40,
      titleSpacing: 0,
      elevation: 0,
      title: Container(
        height: 40,
        width: double.infinity,
        margin: EdgeInsets.only(right: 15),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: TextField(
                controller: state.textEditingController,
                textInputAction: TextInputAction.search,
                decoration: new InputDecoration(
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  contentPadding: EdgeInsets.symmetric(vertical: 4),
                  hintText: S.of(context).search_place,
                  prefixIcon: Icon(
                    Icons.search_rounded,
                    color: Theme.of(context).iconTheme.color,
                    size: 20,
                  ),
                ),
                onChanged: (s) {
                  _keySearch.value = s;
                },
                onSubmitted: (text) {
                },
              ),
            ),
            Obx(() {
              return Visibility(
                visible: (state.keyword.value ?? "").isNotEmpty,
                child: GestureDetector(
                  onTap: controller.clearKeyword,
                  child: Container(
                    width: 48,
                    child: Center(
                      child: Icon(
                        Icons.close_rounded,
                        color: Theme.of(context).iconTheme.color,
                        size: 20,
                      ),
                    ),
                  ),
                ),
              );
            }),
          ],
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          border: Border.all(color: Colors.grey, width: 1),
        ),
      ),
    );
  }

  Widget _buildBody(){
    return Obx(() {
      if (state.searchPlaceStatus.value == LoadStatus.loading) {
        return SearchPlaceLoadingWidget();
      } else if (state.searchPlaceStatus.value == LoadStatus.success) {
        final placeList = state.placeResult;
        return  placeList.length > 0 ? ListView.builder(
          itemBuilder: (context, index) {
            final item = placeList[index];
            return AutoPlaceResultWidget(
              place: item,
              onPressed: () {
                FocusScope.of(context).unfocus();
                callback(item);
                Get.back();
              },
            );
          },
          itemCount: placeList.length,
        ): SearchNoResultsWidget();
      } else {
        return Container();
      }
    });
  }

  @override
  AutoCompletePlaceController createController() {
    return AutoCompletePlaceController();
  }

}

class Uuid {
  final Random _random = Random();

  String generateV4() {
    final int special = 8 + _random.nextInt(4);
    return '${_bitsDigits(16, 4)}${_bitsDigits(16, 4)}-'
        '${_bitsDigits(16, 4)}-'
        '4${_bitsDigits(12, 3)}-'
        '${_printDigits(special, 1)}${_bitsDigits(12, 3)}-'
        '${_bitsDigits(16, 4)}${_bitsDigits(16, 4)}${_bitsDigits(16, 4)}';
  }

  String _bitsDigits(int bitCount, int digitCount) =>
      _printDigits(_generateBits(bitCount), digitCount);

  int _generateBits(int bitCount) => _random.nextInt(1 << bitCount);

  String _printDigits(int value, int count) =>
      value.toRadixString(16).padLeft(count, '0');
}
