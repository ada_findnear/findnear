import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/entities/place_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class AutoCompletePlaceState extends BaseState{
  TextEditingController textEditingController = TextEditingController();

  RxString keyword = "".obs;
  Rx<LoadStatus> searchPlaceStatus = LoadStatus.initial.obs;
  RxList<AddressAutoEntity> placeResult = <AddressAutoEntity>[].obs;

}