import 'dart:convert';
import 'dart:math';

import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/commons/app_configs.dart';
import 'package:findnear/src/models/entities/place_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/pages/search/auto_complete/state.dart';
import 'package:dio/dio.dart' as dio;
import 'package:findnear/src/utils/logger.dart';

class AutoCompletePlaceController extends BaseController<AutoCompletePlaceState>{

  @override
  AutoCompletePlaceState createState() => AutoCompletePlaceState();

  void clearKeyword() {
    state.textEditingController.text = "";
  }


  void searchPlace({bool allowEmptyKeyword = false}) async {
    final keyword = state.textEditingController.text?.trim() ?? "";
    if (keyword.isEmpty && !allowEmptyKeyword) {
      return;
    }
    state.placeResult.clear();
    state.searchPlaceStatus.value = LoadStatus.loading;

    dio.Response response;
    var dioClient = dio.Dio(); // with default Options
    dioClient.options.baseUrl = 'https://maps.googleapis.com';
    dioClient.options.connectTimeout = 30000; //30s
    dioClient.options.receiveTimeout = 60000; //60s
    try {
      response = await dioClient.get(
        '/maps/api/place/autocomplete/json',
        queryParameters: {
          "input": keyword,
          "key": AppConfigs.googleAPI,
          "sessiontoken":Uuid().generateV4(),
          "region":"vn",
        }
      );
      _logResponse(response);
      final data = response.data as Map<String, dynamic>;
      final placeList = AddressAutoList.fromJson(data);
      state.placeResult.value = placeList?.predictions ?? [];
      state.placeResult.refresh();
      state.searchPlaceStatus.value = LoadStatus.success;
    } catch (e, s) {
      logger.e(e);
      logger.e(s);
      state.searchPlaceStatus.value = LoadStatus.failure;
    }
  }

  void _logResponse(dio.Response response) {
    final statusCode = response.statusCode;
    final uri = Uri.parse("https://maps.googleapis.com/maps/api/place/autocomplete");
    final data = jsonEncode(response.data);
    logger.log("✅ RESPONSE[$statusCode] => PATH: $uri\n DATA: $data");
  }

}

class Uuid {
  final Random _random = Random();
  String generateV4() {
    final int special = 8 + _random.nextInt(4);

    return '${_bitsDigits(16, 4)}${_bitsDigits(16, 4)}-'
        '${_bitsDigits(16, 4)}-'
        '4${_bitsDigits(12, 3)}-'
        '${_printDigits(special, 1)}${_bitsDigits(12, 3)}-'
        '${_bitsDigits(16, 4)}${_bitsDigits(16, 4)}${_bitsDigits(16, 4)}';
  }

  String _bitsDigits(int bitCount, int digitCount) =>
      _printDigits(_generateBits(bitCount), digitCount);

  int _generateBits(int bitCount) => _random.nextInt(1 << bitCount);

  String _printDigits(int value, int count) =>
      value.toRadixString(16).padLeft(count, '0');
}