import 'package:findnear/src/models/entities/place_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SearchState {
  RxString keyword = "".obs;
  TextEditingController textEditingController = TextEditingController();

  Rx<LoadStatus> searchStatus = LoadStatus.initial.obs;
  RxList<SearchResultEntity> searchResults = <SearchResultEntity>[].obs;

  SearchState() {
    textEditingController.addListener(() {
      keyword.value = textEditingController.text;
    });
  }
}
