import 'dart:convert';

import 'package:dio/dio.dart' as dio;
import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_configs.dart';
import 'package:findnear/src/models/entities/place_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/market.dart';
import 'package:findnear/src/models/search_user_entity.dart';
import 'package:findnear/src/repository/market_repository.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';

import 'search_state.dart';

enum ObjectType { header, friend, location }

class SearchLogic extends GetxController {
  final state = SearchState();
  List<SearchResultEntity> _searchResults = <SearchResultEntity>[];

  void clearKeyword() {
    state.textEditingController.text = "";
  }

  void search(BuildContext context,
      {bool allowEmptyKeywordFriend = false,
      bool allowEmptyKeywordPlace = false}) async {

    _searchResults.clear();
    state.searchStatus.value = LoadStatus.loading;

    await searchFriend(allowEmptyKeyword:allowEmptyKeywordFriend);
    await searchPlace(allowEmptyKeyword:allowEmptyKeywordPlace);

    state.searchResults.value = _searchResults;
    state.searchStatus.value = LoadStatus.success;
  }

  void searchFriend({bool allowEmptyKeyword = false}) async {
    List<SearchUserEntity> listUser = <SearchUserEntity>[];
    final keyword = state.textEditingController.text?.trim() ?? "";

    if (keyword.isEmpty && !allowEmptyKeyword) {
      return;
    }

    final Stream<SearchUserEntity> stream =
        await getSearchFriends(keyword: keyword);
    stream.listen(
      (SearchUserEntity _market) {
        listUser.add(_market);
      },
      onError: (a) {
        state.searchStatus.value = LoadStatus.failure;
      },
      onDone: () {
        _searchResults.add(SearchResultEntity(
            type: ObjectType.header, data: S.of(Get.context).friend));
        listUser.forEach((element) {
          _searchResults
              .add(SearchResultEntity(type: ObjectType.friend, data: element));
        });
      },
    );
  }

  void searchPlace({bool allowEmptyKeyword = false}) async {
    List<PlaceEntity> listPlace = <PlaceEntity>[];
    final keyword = state.textEditingController.text?.trim() ?? "";

    if (keyword.isEmpty && !allowEmptyKeyword) {
      return;
    }

    Position position = null;
    try {
      position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
      _logRequest(
          method: "GET",
          url: Uri.parse(
              "https://maps.googleapis.com/maps/api/place/textsearch/json"),
          headers: {
            "query": keyword,
            "key": AppConfigs.googleAPI,
          }..addIf(
              position != null,
              "location",
              "${position.latitude},${position.longitude}",
            ));
    } catch (e, s) {
      logger.e(e);
    }

    dio.Response response;
    var dioClient = dio.Dio(); // with default Options
    dioClient.options.baseUrl = 'https://maps.googleapis.com';
    dioClient.options.connectTimeout = 30000; //30s
    dioClient.options.receiveTimeout = 60000; //60s
    try {
      response = await dioClient.get(
        '/maps/api/place/textsearch/json',
        queryParameters: {
          "query": keyword,
          "key": "AIzaSyBtaQfYohHQE01th0fmXvKLG-DvIvcBhrI",
        }..addIf(
            position != null,
            "location",
            "${position.latitude},${position.longitude}",
          ),
      );
      _logResponse(response);
      final data = response.data as Map<String, dynamic>;
      final placeList = PlaceList.fromJson(data);
      listPlace = placeList?.results ?? [];
      if (listPlace.length > 0) {
        _searchResults.add(SearchResultEntity(
            type: ObjectType.header, data: S.of(Get.context).place));
        listPlace.forEach((element) {
          _searchResults.add(
              SearchResultEntity(type: ObjectType.location, data: element));
        });
      }
    } catch (e, s) {
      logger.e(e);
      logger.e(s);
      state.searchStatus.value = LoadStatus.failure;
    }
  }

  void _logRequest({String method, Uri url, Map<String, String> headers}) {
    logger.log("✈️ REQUEST[$method] => PATH: $url \n headers: ${headers}",
        printFullText: true);
  }

  void _logResponse(dio.Response response) {
    final statusCode = response.statusCode;
    final uri =
        Uri.parse("https://maps.googleapis.com/maps/api/place/textsearch/json");
    final data = jsonEncode(response.data);
    logger.log("✅ RESPONSE[$statusCode] => PATH: $uri\n DATA: $data");
  }
}
