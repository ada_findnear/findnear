import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/entities/place_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/search_user_entity.dart';
import 'package:findnear/src/pages/map_direction/map_direction_view.dart';
import 'package:findnear/src/pages/profile_user/profile_user_view.dart';
import 'package:findnear/src/pages/search/widgeds/friend_result_widget.dart';
import 'package:findnear/src/pages/search/widgeds/place_result_widget.dart';
import 'package:findnear/src/pages/search/widgeds/search_no_results.dart';
import 'package:findnear/src/pages/search/widgeds/search_place_loading_widget.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'search_logic.dart';
import 'search_state.dart';

class SearchPage extends StatefulWidget {
  final String keyword;
  SearchPage({this.keyword});

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage>
    with SingleTickerProviderStateMixin {
  final SearchLogic logic = Get.put(SearchLogic());
  final SearchState state = Get
      .find<SearchLogic>()
      .state;
  var delaySearch;
  final _keySearch = "".obs;

  @override
  void initState() {
    super.initState();
    state.textEditingController.text = widget.keyword;
    state.keyword.value = widget.keyword;

    WidgetsBinding.instance.addPostFrameCallback((_) {
      logic.search(context,
          allowEmptyKeywordFriend: true, allowEmptyKeywordPlace: false);
    });

    delaySearch = debounce(_keySearch, (value) {
      logic.search(context);
    }, time: Duration(milliseconds: 1000));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: SafeArea(child: _buildBody()),
    );
  }

  @override
  void dispose() {
    Get.delete<SearchLogic>();
    delaySearch.dispose();
    super.dispose();
  }

  AppBar _buildAppBar() {
    return AppBar(
      leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Theme
              .of(context)
              .brightness == Brightness.dark
              ? Image.asset(
            AppImages.icBackDark,
          )
              : Image.asset(
            AppImages.icBackLight,
          )),
      leadingWidth: 40,
      titleSpacing: 0,
      elevation: 0,
      title: Container(
        height: 40,
        width: double.infinity,
        margin: EdgeInsets.only(right: 15),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: TextField(
                controller: state.textEditingController,
                textInputAction: TextInputAction.search,
                decoration: new InputDecoration(
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  contentPadding: EdgeInsets.symmetric(vertical: 4),
                  hintText: S
                      .of(context)
                      .search,
                  prefixIcon: Icon(
                    Icons.search_rounded,
                    color: Theme
                        .of(context)
                        .iconTheme
                        .color,
                    size: 20,
                  ),
                  // suffixIcon: IconButton(
                  //   // onPressed: _controller.clear,
                  //   icon: Icon(Icons.clear),
                  // ),
                ),
                onChanged: (s) {
                  _keySearch.value = s;
                },
                onSubmitted: (text) {},
              ),
            ),
            Obx(() {
              return Visibility(
                visible: (state.keyword.value ?? "").isNotEmpty,
                child: GestureDetector(
                  onTap: logic.clearKeyword,
                  child: Container(
                    width: 48,
                    child: Center(
                      child: Icon(
                        Icons.close_rounded,
                        color: Theme
                            .of(context)
                            .iconTheme
                            .color,
                        size: 20,
                      ),
                    ),
                  ),
                ),
              );
            }),
          ],
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          border: Border.all(color: Colors.grey, width: 1),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return Obx(() {
      if (state.searchStatus.value == LoadStatus.loading) {
        return SearchPlaceLoadingWidget();
      } else if (state.searchStatus.value == LoadStatus.success) {
        final results = state.searchResults;
        return results.length > 1
            ? ListView.builder(
          itemCount: results.length,
          itemBuilder: (context, index) {
            final item = results[index];
            switch (item.type) {
              case ObjectType.header:
                return Column(
                  children: [
                    Container(
                      height: 36,
                      color: Color(0xFFFFEDED),
                      padding: EdgeInsets.only(left: 27),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          item.data as String,
                          style: TextStyle(
                            fontSize: 14,
                            fontFamily: 'Quicksand',
                            color: Color(0xFF333333),
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 5,),
                  ],
                );
                break;
              case ObjectType.location:
                return PlaceResultWidget(
                  place: item.data as PlaceEntity,
                  onPressed: () {
                    _openMap(item.data);
                  },
                  onPressedDirection: (){
                    _openMapDirection(item.data);
                  },
                );
                break;
              case ObjectType.friend:
                return FriendResultWidget(
                    user: item.data as SearchUserEntity,
                    onPressed: () {
                      _openProfile(userId: item.data.id);
                    });
                break;
              default:
                return Container();
                break;
            }
          },
        )
            : SearchNoResultsWidget();
      } else {
        return Container();
      }
    });
  }

  void _openMap(PlaceEntity place) {
    Get.back(result: place);
  }

  void _openMapDirection(PlaceEntity place) {
    Get.to(() =>
        MapDirectionPage(place: place,));
  }

  void _openProfile({String userId}) {
    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    }
    NavigatorUtils.navigateToProfilePage(userId);
  }
}
