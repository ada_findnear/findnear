import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:get/get.dart';

import '../elements/FriendItemWidget.dart';
import '../helpers/loadmore_text_builder.dart';
import 'package:loadmore/loadmore.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../elements/DrawerWidget.dart';
import '../controllers/friends_request_controller.dart';
import '../../generated/l10n.dart';
import 'profile_user/profile_user_view.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

// ignore: must_be_immutable
class FriendsRequestWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  FriendsRequestWidget({Key key});

  @override
  _FriendsRequestWidgetState createState() {
    return _FriendsRequestWidgetState();
  }
}

class _FriendsRequestWidgetState extends StateMVC<FriendsRequestWidget> {
  FriendsRequestController _con;

  _FriendsRequestWidgetState() : super(FriendsRequestController()) {
    _con = controller;
  }

  initState() {
    _con.listenForFriendsRequest();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: widget.scaffoldKey,
      drawer: DrawerWidget(),
      appBar: AppBar(
        leading: new IconButton(
            icon:
                new Icon(Icons.arrow_back, color: Theme.of(context).hintColor),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        centerTitle: true,
        title: Text(S.of(context).friendRequest),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: LoadMore(
              isFinish: !_con.canLoadMore,
              child: ListView.separated(
                padding: EdgeInsets.fromLTRB(0, 10.w, 0, 0),
                itemCount: _con.friendsRequest.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                      onTap: () {
                        NavigatorUtils.navigateToProfilePage(_con.friendsRequest.elementAt(index).id);
                      },
                      child: FriendItemWidget(
                        user: _con.friendsRequest.elementAt(index),
                        isListFriend: false,
                      ));
                },
                physics: const NeverScrollableScrollPhysics(),
                separatorBuilder: (BuildContext context, int index) =>
                    const Divider(),
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
              ),
              onLoadMore: _loadMore,
              whenEmptyLoad: false,
              delegate: DefaultLoadMoreDelegate(),
              textBuilder: CustomLoadMoreTextBuilder.getText,
            ),
          ),
          Visibility(
            visible: !(_con.friendsRequest.length > 0),
              child: Center(
                  child: Text(
            S.of(context).no_friends_request,
            style: TextStyle(
              color: Color(0xFF333333),
              fontSize: 16,
              fontWeight: FontWeight.w700,
              fontFamily: 'Quicksand',
            ),
          ))),
        ],
      ),
    );
  }

  Future<bool> _loadMore() async {
    await Future.delayed(Duration(seconds: 2, milliseconds: 1000));

    await _con.listenForFriendsRequest();
    return true;
  }
}
