import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/models/input/invite_user_to_group_input.dart';
import 'package:findnear/src/repository/findnear_group_repository.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:get/get.dart';

import 'join_group_state.dart';

class JoinGroupLogic extends GetxController {
  final JoinGroupState state;

  final FindNearGroupRepository _groupRepository = Get.find();

  JoinGroupLogic() : state = JoinGroupState();

  void requestJoinGroup(GroupEntity group) async {
    print("Click");
    state.requestJoinGroupState.value = LoadingState.loading;
    try {
      final input = InviteUserToGroupInput.joinGroup(group.id);
      await _groupRepository.inviteUserToGroup(input);

      state.requestJoinGroupState.value = LoadingState.success;
    } catch (e) {
      state.requestJoinGroupState.value = LoadingState.failure;
    }
  }
}
