import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/pages/groups/group_content_management_pageview/group_content_management_pageview.dart';
import 'package:findnear/src/pages/groups/group_member_administration_pageview/group_member_administration_pageview.dart';
import 'package:findnear/src/pages/groups/group_permission_censorship_page/group_permission_censorship_page.dart';
import 'package:findnear/src/pages/groups/update_group_viewer_status_page/view.dart';
import 'package:findnear/src/utils/enum_utils.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'logic.dart';
import 'state.dart';

class GroupDetailSettingPage extends StatefulWidget {
  GroupEntity group;
  GroupDetailSettingPage({
    Key key,
    this.group,
  }) : super(key: key);

  @override
  _GroupDetailSettingPageState createState() => _GroupDetailSettingPageState();
}

class _GroupDetailSettingPageState extends State<GroupDetailSettingPage> {

  GroupDetailSettingLogic logic = Get.put(GroupDetailSettingLogic());
  GroupDetailSettingState state = Get.find<GroupDetailSettingLogic>().state;

  List<GroupDetailSettingFeature> features = [
    GroupDetailSettingFeature.member_administration,
    GroupDetailSettingFeature.content_management,
    GroupDetailSettingFeature.privacy,
    GroupDetailSettingFeature.out_group,
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: _getAppBar(),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            GridView.count(
              crossAxisCount: 2,
              crossAxisSpacing: 0,
              mainAxisSpacing: 0,
              shrinkWrap: true,
              children: features.map((e) {
                return _settingItem(
                  e.title,
                  e.icon,
                  () async {
                    switch (e) {
                      case GroupDetailSettingFeature.member_administration:
                        Get.to(() => GroupMemberAdministrationPageView(
                              group: widget.group,
                            ));
                        break;
                      case GroupDetailSettingFeature.content_management:
                        Get.to(() => GroupContentManagementPageView(
                              group: widget.group,
                            ));
                        break;
                      case GroupDetailSettingFeature.permission_censorship:
                        Get.to(() => GroupPermissionCensorshipPage(
                              group: widget.group,
                            ));
                        break;
                      case GroupDetailSettingFeature.privacy:
                        await Get.to(UpdateGroupViewerStatus(
                          group: widget.group,
                        ));
                        // TODO: - Reload list update status
                        break;
                      case GroupDetailSettingFeature.out_group:
                        _showOutGroupConfirmDialog();
                        break;
                      default:
                        break;
                    }
                  },
                );
              }).toList(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _settingItem(String title, String appIcon, VoidCallback onTap) {
    return GestureDetector(
      onTap: () {
        onTap.call();
      },
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 80,
              width: 80,
              margin: EdgeInsets.only(bottom: 5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(5),
                ),
              ),
              child: Center(
                child: AppImage(
                  appIcon,
                ),
              ),
            ),
            Text(
              title,
              style: Theme.of(context).textTheme.bodyText1.merge(
                    TextStyle(
                      color: AppColors.textBlack,
                      fontSize: 14,
                      fontFamily: 'Quicksand',
                    ),
                  ),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
      ),
    );
  }

  void _showOutGroupConfirmDialog() {
    DialogHelper.showConfirmPopup(
      title: "Rời khỏi nhóm",
      description: "Bạn có chắc chắn muốn rời khỏi nhóm?",
      okBtnText: "Rời khỏi",
      cancelText: S.of(context).no_dialog_action,
      okBtnPressed: () async {
        await logic.outGroup(context, widget.group.id);
        Get.back();
        Get.back();
      },
    );
  }

  Widget _getAppBar() {
    return AppBar(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      leadingWidth: 52,
      centerTitle: false,
      title: Text(
        "Quản trị nhóm",
        style: Theme.of(context).textTheme.headline2.merge(
              TextStyle(
                color: AppColors.textBlack,
                fontSize: 16,
              ),
            ),
      ),
      actions: [],
    );
  }
}
