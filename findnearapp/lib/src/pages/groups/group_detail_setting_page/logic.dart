import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/repository/findnear_group_repository.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'state.dart';

class GroupDetailSettingLogic extends GetxController {
  final FindNearGroupRepository _groupRepository = Get.find();

  final GroupDetailSettingState state;
  OverlayEntry loader;

  GroupDetailSettingLogic({GlobalKey<ScaffoldState> scaffoldKey})
      : state = GroupDetailSettingState();

  Future<void> outGroup(BuildContext context, int groupId) async {
    try {
      loader = Helper.overlayLoader(context);
      Overlay.of(context).insert(loader);

      await _groupRepository.outGroup(groupId);

      Helper.hideLoader(loader);
    } catch (e) {
      Helper.hideLoader(loader);
      Helper.showErrorMessage(e);
    }
  }
}
