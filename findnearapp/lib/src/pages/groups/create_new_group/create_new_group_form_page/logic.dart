import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/models/input/group_input.dart';
import 'package:findnear/src/repository/findnear_group_repository.dart';
import 'package:findnear/src/utils/enum_utils.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'state.dart';

class CreateNewGroupFormLogic extends GetxController {
  final CreateNewGroupFormState state;

  CreateNewGroupFormLogic() : state = CreateNewGroupFormState();

  final FindNearGroupRepository _repository = Get.find();

  OverlayEntry loader;
  GroupEntity group;

  bool validateBeforeCreateGroup() {
    final isTitleValid = state.groupName.isNotEmpty;
    final isGroupTopicValid = state.groupTopic != -1;
    final isPublishStatusValid = state.publishStatus != -1;
    final isGroupDescValid = state.groupDesc.isNotEmpty;
    final isValid = isTitleValid &&
        isGroupTopicValid &&
        isPublishStatusValid &&
        isGroupDescValid;

    if (!isTitleValid) {
      AppSnackbar.showError(message: 'Vui lòng nhập tên nhóm');
    } else if (!isGroupTopicValid) {
      AppSnackbar.showError(message: 'Vui lòng chọn chủ đề nhóm ');
    } else if (!isPublishStatusValid) {
      AppSnackbar.showError(message: 'Vui lòng chọn quyền riêng tư');
    } else if (!isGroupDescValid) {
      AppSnackbar.showError(message: 'Vui lòng nhập giới thiệu');
    }
    return isValid;
  }

  void initGroupData(GroupEntity group) {
    this.group = group;
    state.groupName.value = group.title;
    state.groupDesc.value = group.content;
    state.publishStatusValueSelected.value = group.viewer.toString();
    state.groupTopicValueSelected.value = group.group_type_id.toString();
  }

  Future createGroup(BuildContext context) async {
    final isValid = validateBeforeCreateGroup();
    if (!isValid) {
      return;
    }

    GroupInput input = new GroupInput(
      title: state.groupName.value,
      group_type_id: state.groupTopic,
      viewer: state.publishStatus.toString(),
      content: state.groupDesc.value,
    );
    print("input ${input.toJson()}");
    try {
      print("Get.key.currentContext ${context}");

      loader = Helper.overlayLoader(context);
      Overlay.of(context).insert(loader);

      state.createGroupLoadingState.value = LoadingState.loading;
      var res = await _repository.createGroup(input);
      state.group.value = res.data;
      state.createGroupLoadingState.value = LoadingState.success;

      Helper.hideLoader(loader);
    } catch (e) {
      state.createGroupLoadingState.value = LoadingState.failure;
      Helper.hideLoader(loader);
      Helper.showErrorMessage(e);
    }
  }

  Future editGroup(BuildContext context) async {
    final isValid = validateBeforeCreateGroup();
    if (!isValid) {
      return;
    }

    GroupInput input = new GroupInput(
      groupId: group.id,
      title: state.groupName.value,
      group_type_id: state.groupTopic,
      viewer: state.publishStatus.toString(),
      content: state.groupDesc.value,
    );
    print("input ${input.toJson()}");
    try {
      print("Get.key.currentContext ${context}");

      loader = Helper.overlayLoader(context);
      Overlay.of(context).insert(loader);

      state.updateGroupLoadingState.value = LoadingState.loading;
      var res = await _repository.updateGroup(input);
      state.group.value = res.data;
      Helper.hideLoader(loader);
      state.updateGroupLoadingState.value = LoadingState.success;
    } catch (e) {
      state.updateGroupLoadingState.value = LoadingState.failure;
      Helper.hideLoader(loader);
      Helper.showErrorMessage(e);
    }
  }

  Future getInitialized() async {
    state.getDataLoadingState.value = LoadingState.loading;
    await getGroupTopics();
    await getPublishStatus();
    state.getDataLoadingState.value = LoadingState.success;
  }

  Future getGroupTopics() async {
    try {
      var res = await _repository.getGroupTopic();
      final topics = res.data;
      final topicDataSource = topics.map((e) {
        return {"value": e.id, "label": e.title};
      }).toList();
      state.groupTopicDataSource.assignAll(topicDataSource);
    } catch (e) {
      print("getPublishStatus");
      print(e);
    }
  }

  Future getPublishStatus() async {
    try {
      final dataSource = GroupViewerStatus.values
          .map((e) => {"value": e.toParam, "label": e.display})
          .toList();

      state.publishStatusDataSource.assignAll(dataSource);
    } catch (e) {
      print("getPublishStatus");
      print(e);
    }
  }
}
