import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:findnear/src/widgets/shimmer/group/loading_shimmer.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:select_form_field/select_form_field.dart';

import 'logic.dart';
import 'state.dart';

class CreateNewGroupFormPage extends StatefulWidget {
  GroupEntity group;
  Function completedCallback;

  CreateNewGroupFormPage({
    Key key,
    this.completedCallback,
    this.group,
  }) : super(key: key);

  @override
  _CreateGroupFormPageState createState() => _CreateGroupFormPageState();
}

class _CreateGroupFormPageState extends State<CreateNewGroupFormPage>
    with AutomaticKeepAliveClientMixin {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final CreateNewGroupFormLogic logic = Get.put(CreateNewGroupFormLogic());
  final CreateNewGroupFormState state =
      Get.find<CreateNewGroupFormLogic>().state;

  var isEdit = false;

  @override
  void initState() {
    super.initState();

    isEdit = widget.group != null;
    if (widget.group != null) {
      final group = widget.group;
      logic.initGroupData(group);
    }

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      state.createGroupLoadingState.listen((p0) {
        print("p0 ${p0}");
        if (p0 == LoadingState.success) {
          try {
            widget.completedCallback?.call(state.group.value);
          } catch (e) {}
        }
      });

      state.updateGroupLoadingState.listen((p0) {
        print("p0 ${p0}");
        if (p0 == LoadingState.success) {
          try {
            Get.back();
          } catch (e) {}
        }
      });
    });

    logic.getInitialized();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _getAppBar(),
      body: Obx(() {
        if (state.getDataLoadingState == LoadingState.loading) {
          return LoadingShimmer();
        }
        return Container(
          margin: EdgeInsets.symmetric(horizontal: 15),
          child: Form(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Obx(() {
                  return TextFormField(
                    style: TextStyle(color: Theme.of(context).hintColor),
                    keyboardType: TextInputType.emailAddress,
                    decoration: getInputDecoration(
                      labelText: S.of(context).group_form_group_name_label,
                      hintText: S.of(context).group_form_group_name_placeholder,
                    ),
                    initialValue: state.groupName.value,
                    validator: null,
                    onChanged: (value) {
                      state.groupName.value = value;
                    },
                  );
                }),
                SizedBox(height: 15),
                Obx(() {
                  return SelectFormField(
                    style: TextStyle(color: Theme.of(context).hintColor),
                    labelText: 'Shape',
                    initialValue: state.groupTopicValueSelected.value,
                    decoration: getInputDecoration(
                      labelText: S.of(context).group_form_group_topic_label,
                      hintText: S.of(context).group_form_group_topic_label,
                    ),
                    changeIcon: true,
                    enableSearch: true,
                    items: state.groupTopicDataSource,
                    validator: (input) => null,
                    onSaved: (input) {
                      print("onSaved");
                      print(input);
                    },
                    itemIsLocked: () {
                      print("itemIsLocked");
                    },
                    onChanged: (value) {
                      print("onChanged ${value}");
                      state.groupTopicValueSelected.value = value;
                    },
                  );
                }),
                SizedBox(height: 15),
                Obx(() {
                  return SelectFormField(
                    style: TextStyle(color: Theme.of(context).hintColor),
                    labelText: 'Shape',
                    initialValue: state.publishStatusValueSelected.value,
                    decoration: getInputDecoration(
                      labelText: S.of(context).privacy,
                      hintText: S.of(context).privacy,
                    ),
                    changeIcon: true,
                    enableSearch: true,
                    items: state.publishStatusDataSource,
                    validator: (input) => null,
                    onSaved: (input) {
                      print("onSaved");
                      print(input);
                    },
                    itemIsLocked: () {
                      print("itemIsLocked");
                    },
                    onChanged: (value) {
                      print("onChanged ${value}");
                      state.publishStatusValueSelected.value = value;
                    },
                  );
                }),
                SizedBox(height: 15),
                TextFormField(
                  style: TextStyle(color: Theme.of(context).hintColor),
                  decoration: getInputDecoration(
                    labelText: S.of(context).group_form_group_desc_label,
                    hintText: S.of(context).group_form_group_desc_placeholder,
                  ),
                  initialValue: state.groupDesc.value,
                  validator: null,
                  minLines: 1,
                  maxLines: 7,
                  keyboardType: TextInputType.multiline,
                  onChanged: (value) {
                    state.groupDesc.value = value;
                  },
                ),
              ],
            ),
          ),
        );
      }),
    );
  }

  Widget _getAppBar() {
    return AppBar(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      leadingWidth: 52,
      centerTitle: false,
      title: Text(
        isEdit ? "Chỉnh sửa" : S.current.group_form_group_title,
        style: Theme.of(context).textTheme.headline2.merge(
              TextStyle(
                color: AppColors.textBlack,
                fontSize: 16,
              ),
            ),
      ),
      actions: [
        Container(
          margin: EdgeInsets.only(right: 10),
          child: Center(
            child: ElevatedButton(
              onPressed: () {
                if (isEdit) {
                  logic.editGroup(context);
                } else {
                  logic.createGroup(context);
                }
              },
              style: ElevatedButton.styleFrom(
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(20.0),
                ),
                primary: AppColors.red,
                elevation: 0,
              ),
              child: Text(
                isEdit ? "Lưu" : S.of(context).common_button_continue,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 14,
                  color: AppColors.white,
                ),
                textAlign: TextAlign.left,
              ),
            ),
          ),
        ),
      ],
    );
  }

  InputDecoration getInputDecoration({String hintText, String labelText}) {
    return new InputDecoration(
      hintText: hintText,
      labelText: labelText,
      hintStyle: Theme.of(context).textTheme.bodyText2.merge(
            TextStyle(color: Theme.of(context).focusColor),
          ),
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(
          color: Theme.of(context).hintColor.withOpacity(0.2),
        ),
      ),
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Theme.of(context).hintColor),
      ),
      floatingLabelBehavior: FloatingLabelBehavior.auto,
      labelStyle: Theme.of(context).textTheme.bodyText2.merge(
            TextStyle(color: Theme.of(context).hintColor),
          ),
    );
  }

  /// Handle AutomaticKeepAliveClientMixin
  @override
  bool get wantKeepAlive => true;
}
