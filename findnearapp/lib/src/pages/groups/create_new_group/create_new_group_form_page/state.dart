import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:get/get.dart';

class CreateNewGroupFormState {
  RxString groupName = "".obs;
  RxString groupDesc = "".obs;
  RxString publishStatusValueSelected = "".obs;
  RxList<Map<String, dynamic>> publishStatusDataSource =
      List<Map<String, dynamic>>.empty().obs;
  RxList<Map<String, dynamic>> groupTopicDataSource =
      List<Map<String, dynamic>>.empty().obs;
  RxString groupTopicValueSelected = "".obs;
  Rx<LoadingState> createGroupLoadingState = LoadingState.idle.obs;
  Rx<LoadingState> updateGroupLoadingState = LoadingState.idle.obs;
  Rx<LoadingState> getDataLoadingState = LoadingState.idle.obs;

  Rx<GroupEntity> group = (new GroupEntity()).obs;

  int get publishStatus {
    final value = publishStatusDataSource.firstWhere((element) {
      print(" element[value] ${element["value"]}");
      print(
          " publishStatusValueSelected.value ${publishStatusValueSelected.value}");
      return element["value"].toString() == publishStatusValueSelected.value;
    }, orElse: () {
      return {};
    });
    if (value.isEmpty) {
      return -1;
    }
    return value["value"];
  }

  int get groupTopic {
    final value = groupTopicDataSource.firstWhere((element) {
      print(" element[value] ${element["value"]}");
      print(" groupTopicValueSelected.value ${groupTopicValueSelected.value}");
      return element["value"].toString() == groupTopicValueSelected.value;
    }, orElse: () {
      return {};
    });

    if (value.isEmpty) {
      return -1;
    }
    return value["value"];
  }

  CreateNewGroupFormState();
}
