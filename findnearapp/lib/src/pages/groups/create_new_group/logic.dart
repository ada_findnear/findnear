import 'package:dio/dio.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/models/input/UpdateGroupCoverInput.dart';
import 'package:findnear/src/models/input/invite_user_to_group_input.dart';
import 'package:findnear/src/repository/findnear_group_repository.dart';
import 'package:findnear/src/repository/user_v2_repository.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'state.dart';

class CreateNewGroupPageViewLogic extends GetxController {
  final CreateNewGroupPageViewState state;

  final FindNearGroupRepository _repository = Get.find();
  final _userV2Repository = Get.find<UserV2Repository>();

  CreateNewGroupPageViewLogic() : state = CreateNewGroupPageViewState();

  int currentOffset = 0;
  bool canLoadMore = true;

  OverlayEntry loader;

  void updateGroup(GroupEntity group) {
    state.group.value = group;
  }

  void inviteMemberToGroup(int user_id) {
    final group_id = state.group.value.id;
    if (group_id == null) {
      return;
    }
    try {
      InviteUserToGroupInput input =
          InviteUserToGroupInput(group_id: group_id, user_id: user_id);
      _repository.inviteUserToGroup(input);
    } catch (e) {
      print("inviteMemberToGroup");
      print(e);
      if (e is DioError) {
        print(e.response);
      }
    }
  }

  void updateGroupCover(BuildContext context, String coverFilePath) async {
    final group_id = state.group.value.id;
    if (group_id == null) {
      return;
    }
    try {
      loader = Helper.overlayLoader(context);
      Overlay.of(context).insert(loader);

      state.updateGroupCoverLoadingState.value = LoadingState.loading;
      final input = UpdateGroupCoverInput(
          groupId: group_id, coverFilePath: coverFilePath);
      await _repository.updateGroupCover(input);
      state.updateGroupCoverLoadingState.value = LoadingState.success;

      Helper.hideLoader(loader);
    } catch (e) {
      Helper.hideLoader(loader);
      state.updateGroupCoverLoadingState.value = LoadingState.failure;
      Helper.showErrorMessage(e);
    }
  }
}
