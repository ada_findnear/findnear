import 'dart:io';

import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/bottom_sheet_option.dart';
import 'package:findnear/src/pages/groups/create_new_group/logic.dart';
import 'package:findnear/src/pages/groups/create_new_group/state.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/media/media_picker_utils.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class AddGroupCoverImagePage extends StatefulWidget {
  Function completedCallback;

  AddGroupCoverImagePage({
    Key key,
    this.completedCallback,
  }) : super(key: key);

  @override
  _AddGroupCoverImagePageState createState() => _AddGroupCoverImagePageState();
}

class _AddGroupCoverImagePageState extends State<AddGroupCoverImagePage>
    with AutomaticKeepAliveClientMixin {
  CreateNewGroupPageViewLogic logic;
  CreateNewGroupPageViewState state;

  String _coverFilePath = "";

  @override
  void initState() {
    super.initState();
    logic = Get.put(CreateNewGroupPageViewLogic());
    state = Get.find<CreateNewGroupPageViewLogic>().state;

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      state.updateGroupCoverLoadingState.listen((p0) {
        print("p0 ${p0}");
        if (p0 == LoadingState.success) {
          try {
            widget.completedCallback?.call(state.group.value);
          } catch (e) {
            print("state.updateGroupCoverLoadingState.listen((p0) $e");
          }
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _getAppBar(),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return Obx(
      () {
        if (state.updateGroupCoverLoadingState.value == LoadingState.loading) {
          return Container(
            alignment: Alignment.center,
            child: LoadingView(),
          );
        }

        final isChooseImage =
            _coverFilePath != null && _coverFilePath.isNotEmpty;

        return Container(
          margin: EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                S.of(context).group_add_cover_title,
                style: Theme.of(context).textTheme.headline1.merge(
                      TextStyle(
                        color: AppColors.textBlack,
                        fontSize: 16,
                        fontFamily: 'Quicksand-bold',
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              GestureDetector(
                onTap: () {
                  _openChooseImageBottomSheet();
                },
                child: Container(
                  margin: EdgeInsets.symmetric(vertical: 15),
                  child: Stack(
                    children: [
                      Container(
                        width: double.infinity,
                        child: isChooseImage
                            ? Image.file(
                                File(_coverFilePath),
                                height: 130,
                                fit: BoxFit.cover,
                              )
                            : AppImage(
                                AppImages.addGroupCover,
                                height: 130,
                                fit: BoxFit.fill,
                              ),
                      ),
                      Positioned(
                        bottom: 5,
                        right: 0,
                        child: Container(
                          height: 26,
                          width: 26,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(13),
                            color: AppColors.black,
                          ),
                          child: Icon(
                            Icons.edit,
                            size: 20,
                            color: Colors.white,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Visibility(
                visible: !isChooseImage,
                child: Center(
                  child: Text(
                    S.of(context).group_add_cover_detail,
                    style: Theme.of(context).textTheme.bodyText1.merge(
                          TextStyle(
                            color: AppColors.gray2,
                            fontSize: 12,
                          ),
                        ),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }

  void _openChooseImageBottomSheet() async {
    DialogHelper.showBottomSheetOptions(
      context: Get.context,
      options: [
        BottomSheetOption(
            iconPath: AppImages.icCamera, text: S.of(Get.context).cameraButton),
        BottomSheetOption(
            iconPath: AppImages.icImage, text: S.of(Get.context).galleryButton)
      ],
      onTapAtIndex: (i) {
        switch (i) {
          case 0:
            _openCamera();
            break;
          case 1:
            _openGallery();
        }
      },
    );
  }

  void _openGallery() async {
    var file = await MediaPickerUtils.pickPhotoForStory(0);
    if (file != null && file.isNotEmpty) {
      setState(() {
        _coverFilePath = file.first.path;
      });
    }
  }

  void _openCamera() async {
    PickedFile file = await ImagePicker().getImage(source: ImageSource.camera);
    if (file != null) {
      setState(() {
        _coverFilePath = file.path;
      });
    }
  }

  Widget _getAppBar() {
    return AppBar(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      leadingWidth: 52,
      centerTitle: false,
      title: Text(
        S.current.group_add_cover_title,
        style: Theme.of(context).textTheme.headline2.merge(
              TextStyle(
                color: AppColors.textBlack,
                fontSize: 16,
              ),
            ),
      ),
      actions: [
        Container(
          margin: EdgeInsets.only(right: 10),
          child: Center(
            child: ElevatedButton(
              onPressed: () {
                if (!isLoggedIn) {
                  NavigatorUtils.toLogin();
                  return;
                }
                if (_coverFilePath.isNotEmpty) {
                  logic.updateGroupCover(context, _coverFilePath);
                } else {
                  print("_coverFilePath empty");
                }
              },
              style: ElevatedButton.styleFrom(
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(20.0),
                ),
                primary: AppColors.red,
                elevation: 0,
              ),
              child: Text(
                S.of(context).common_button_continue,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 14,
                  color: AppColors.white,
                ),
                textAlign: TextAlign.left,
              ),
            ),
          ),
        ),
      ],
    );
  }

  /// Handle AutomaticKeepAliveClientMixin
  @override
  bool get wantKeepAlive => true;
}
