import 'package:dio/dio.dart';
import 'package:findnear/src/models/input/invite_user_to_group_input.dart';
import 'package:findnear/src/repository/findnear_group_repository.dart';
import 'package:findnear/src/repository/user_v2_repository.dart';
import 'package:get/get.dart';

import '../logic.dart';
import '../state.dart';
import 'state.dart';

class GroupInviteMemberLogic extends GetxController {
  final GroupInviteMemberState state;
  final CreateNewGroupPageViewState pageState =
      Get.find<CreateNewGroupPageViewLogic>().state;

  final FindNearGroupRepository _repository = Get.find();
  final _userV2Repository = Get.find<UserV2Repository>();

  GroupInviteMemberLogic() : state = GroupInviteMemberState();

  void initialize() async {
    await fetchFriends();
  }

  void inviteMemberToGroup(int user_id) {
    final group_id = pageState.group.value.id;
    if (group_id == null) {
      return;
    }
    try {
      InviteUserToGroupInput input =
          InviteUserToGroupInput(group_id: group_id, user_id: user_id);
      _repository.inviteUserToGroup(input);
    } catch (e) {
      print("inviteMemberToGroup");
      print(e);
      if (e is DioError) {
        print(e.response);
      }
    }
  }

  Future<void> fetchFriends({
    String keyword = "",
    bool isReload = false,
  }) async {
    var offset = state.friends.value.length;

    if (isReload) {
      state.isLoadingRx.value = true;
      offset = 0;
      state.canLoadmore.value = true;
      state.friends.value = [];
    }

    if (state.canLoadmore.value != true) return;
    final response = await _userV2Repository.fetchFriends(
      keyword: keyword,
      currentOffset: offset,
    );

    if (response.data.isNotEmpty == true) {
      state.friends.value = state.friends.value..addAll(response.data);
      state.friends.refresh();
    } else {
      state.canLoadmore.value = false;
    }
    state.isLoadingRx.value = false;
  }
}
