import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/elements/CircularAvatarWidget.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/groups/create_new_group/group_invite_member_page/state.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'logic.dart';

class CreateNewGroupInviteMemberPage extends StatefulWidget {
  Function completedCallback;

  CreateNewGroupInviteMemberPage({Key key, this.completedCallback,})
      : super(key: key);

  @override
  _GroupInviteMemberPageState createState() => _GroupInviteMemberPageState();
}

class _GroupInviteMemberPageState extends State<CreateNewGroupInviteMemberPage>
    with AutomaticKeepAliveClientMixin {
  RefreshController refreshController =
      RefreshController(initialRefresh: false);

  final TextEditingController _textSearchEditingController =
      TextEditingController();

  GroupInviteMemberLogic logic;
  GroupInviteMemberState state;

  @override
  void initState() {
    super.initState();
    logic = Get.put(GroupInviteMemberLogic());
    state = logic.state;

    logic.initialize();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _getAppBar(),
      body: Obx(() {
        if (state.isLoadingRx.value) {
          return Container(
            alignment: Alignment.center,
            child: LoadingView(),
          );
        }
        return Container(
          child: Column(
            children: [
              SizedBox(
                height: 15,
              ),
              Container(
                height: 40,
                width: double.infinity,
                margin: EdgeInsets.symmetric(horizontal: 15),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: TextField(
                        controller: _textSearchEditingController,
                        textInputAction: TextInputAction.search,
                        decoration: new InputDecoration(
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          contentPadding: EdgeInsets.symmetric(vertical: 4),
                          hintText: S.of(context).common_search,
                          prefixIcon: Icon(
                            Icons.search_rounded,
                            color: Theme.of(context).iconTheme.color,
                            size: 20,
                          ),
                        ),
                        onChanged: (textSearch) {
                          // TODO: -
                        },
                        onSubmitted: (text) {
                          // TODO: -
                        },
                      ),
                    ),
                    Visibility(
                      visible: true,
                      child: GestureDetector(
                        onTap: null,
                        child: Container(
                          width: 48,
                          child: Center(
                            child: Icon(
                              Icons.close_rounded,
                              color: Theme.of(context).iconTheme.color,
                              size: 20,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  border: Border.all(color: Colors.grey, width: 1),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Obx(
                () {
                  final friends = state.friends.value;

                  if (friends.isEmpty) {
                    return Expanded(
                      child: Center(
                        child: Text(
                          _textSearchEditingController.text.isEmpty
                              ? "Không có danh sách bạn bè"
                              : "Không tìm thấy kết quả",
                        ),
                      ),
                    );
                  }

                  return Expanded(
                    child: Container(
                      child: RefreshConfiguration.copyAncestor(
                        context: Get.context,
                        footerTriggerDistance: Get.height * 4,
                        child: SmartRefresher(
                          enablePullUp: state.canLoadmore.value,
                          onRefresh: () async {
                            await logic.fetchFriends(
                                keyword: _textSearchEditingController.text);
                            refreshController?.refreshCompleted();
                          },
                          onLoading: () async {
                            await logic.fetchFriends(
                                keyword: _textSearchEditingController.text);
                            refreshController?.loadComplete();
                          },
                          controller: refreshController,
                          child: ListView.separated(
                            separatorBuilder:
                                (BuildContext context, int index) {
                              return SizedBox(
                                height: 10,
                              );
                            },
                            padding: EdgeInsets.symmetric(horizontal: 15),
                            itemCount: friends.length,
                            itemBuilder: (BuildContext context, int index) {
                              final user = friends.elementAt(index);
                              return _buildGroupItem(user);
                            },
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ],
          ),
        );
      }),
    );
  }

  Container _buildGroupItem(User user) {
    String imageUrl;
    if (user.avatar != null) {
      imageUrl = user.avatar;
    }
    String userName = user.name;
    bool isInvited = state.selectedFriends.value
        .where(
          (element) => element.id == user.id,
        )
        .isNotEmpty;

    return Container(
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(5),
            child: Builder(
              builder: (context) {
                final double size = 50;
                if (imageUrl == null || imageUrl == "")
                  return AvatarPlaceholder(size: size);
                return CachedNetworkImage(
                  imageUrl: imageUrl,
                  fit: BoxFit.cover,
                  width: size,
                  height: size,
                  placeholder: (context, url) => CupertinoActivityIndicator(),
                  errorWidget: (context, url, error) =>
                      AvatarPlaceholder(size: size),
                );
              },
            ),
          ),
          SizedBox(
            width: 5,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  userName,
                  style: Theme.of(context).textTheme.headline6.merge(
                        TextStyle(
                          color: AppColors.textBlack,
                          fontSize: 14,
                          fontFamily: 'Quicksand',
                        ),
                      ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ),
          isInvited
              ? Container(
                  child: ElevatedButton(
                    onPressed: null,
                    style: ElevatedButton.styleFrom(
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(20.0),
                      ),
                      primary: Color(0xFFEDEDED),
                      elevation: 0,
                    ),
                    child: Text(
                      'Đã mời',
                      style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 14,
                        color: Color(0xFFA8A8A8),
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                )
              : Container(
                  child: ElevatedButton(
                    onPressed: () {
                      logic.inviteMemberToGroup(int.parse(user.id));
                    },
                    style: ElevatedButton.styleFrom(
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(20.0),
                      ),
                      primary: AppColors.red200,
                      side: BorderSide(color: AppColors.red),
                      elevation: 0,
                    ),
                    child: Text(
                      'Mời',
                      style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 14,
                        color: AppColors.red,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ),
        ],
      ),
    );
  }

  Widget _getAppBar() {
    return AppBar(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      leadingWidth: 52,
      centerTitle: false,
      title: Text(
        S.current.group_form_group_invite_member_title,
        style: Theme.of(context).textTheme.headline2.merge(
              TextStyle(
                color: AppColors.textBlack,
                fontSize: 16,
              ),
            ),
      ),
      actions: [
        Container(
          margin: EdgeInsets.only(right: 10),
          child: Center(
            child: ElevatedButton(
              onPressed: () {
                widget.completedCallback?.call();
              },
              style: ElevatedButton.styleFrom(
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(20.0),
                ),
                primary: AppColors.red,
                elevation: 0,
              ),
              child: Text(
                S.of(context).common_button_continue,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 14,
                  color: AppColors.white,
                ),
                textAlign: TextAlign.left,
              ),
            ),
          ),
        ),
      ],
    );
  }

  /// Handle AutomaticKeepAliveClientMixin
  @override
  bool get wantKeepAlive => true;
}
