import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/user.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class GroupInviteMemberState extends BaseState {
  final selectedFriends = Rx<List<User>>([]);
  final friends = Rx<List<User>>([]);

  // loadmore
  final canLoadmore = true.obs;

  // search
  final keywordSearch = "".obs;
}
