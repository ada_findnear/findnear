import 'package:findnear/src/commons/app_colors.dart';
import 'package:flutter/material.dart';

class CreateNewGroupStep extends StatelessWidget {
  int currentStep = 1;

  CreateNewGroupStep({Key key, this.currentStep}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 15),
        child: Row(
          children: [
            Expanded(
              child: Container(
                color: currentStep >= 1 ? AppColors.red : AppColors.gray2,
                height: 3,
              ),
            ),
            SizedBox(
              width: 5,
            ),
            Expanded(
              child: Container(
                color: currentStep >= 2 ? AppColors.red : AppColors.gray2,
                height: 3,
              ),
            ),
            SizedBox(
              width: 5,
            ),
            Expanded(
              child: Container(
                color: currentStep >= 3 ? AppColors.red : AppColors.gray2,
                height: 3,
              ),
            )
          ],
        ),
      ),
    );
  }
}
