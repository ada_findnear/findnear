import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:get/get.dart';

class CreateNewGroupPageViewState {
  Rx<GroupEntity> group = (new GroupEntity()).obs;
  RxList<User> friends = List<User>.empty().obs;

  Rx<LoadingState> updateGroupCoverLoadingState = LoadingState.idle.obs;
}
