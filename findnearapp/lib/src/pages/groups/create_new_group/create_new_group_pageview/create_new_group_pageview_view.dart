import 'dart:async';

import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/pages/groups/create_new_group/add_group_cover_image_page/view.dart';
import 'package:findnear/src/pages/groups/create_new_group/create_new_group_form_page/view.dart';
import 'package:findnear/src/pages/groups/create_new_group/group_invite_member_page/view.dart';
import 'package:findnear/src/pages/groups/create_new_group/widgets/create_new_group_step.dart';
import 'package:findnear/src/pages/groups/group_detail_page/view.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../logic.dart';
import '../state.dart';

class CreateNewGroupPageViewView extends StatefulWidget {
  const CreateNewGroupPageViewView({Key key}) : super(key: key);

  @override
  _CreateNewGroupPageViewViewState createState() =>
      _CreateNewGroupPageViewViewState();
}

class _CreateNewGroupPageViewViewState
    extends State<CreateNewGroupPageViewView> {
  PageController _pageController;
  int currentTab = 0;
  int currentStep = 1;

  CreateNewGroupPageViewLogic logic;
  CreateNewGroupPageViewState state;

  @override
  void initState() {
    super.initState();
    logic = Get.put(CreateNewGroupPageViewLogic());
    state = Get.find<CreateNewGroupPageViewLogic>().state;

    _pageController = PageController(initialPage: currentTab);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: PageView(
                controller: _pageController,
                physics: NeverScrollableScrollPhysics(),
                onPageChanged: (int page) {
                  switch (page) {
                    case 0:
                      break;
                    case 1:
                      break;
                    case 2:
                      break;
                  }
                  setState(() {});
                },
                children: [
                  CreateNewGroupFormPage(
                    completedCallback: (GroupEntity group) {
                      print("completedCallback");
                      logic.updateGroup(group);

                      setState(() {
                        currentStep = 2;
                        currentTab = 1;
                      });
                      print("_pageController.jumpToPage(currentTab);");
                      _pageController.jumpToPage(currentTab);
                    },
                  ),
                  CreateNewGroupInviteMemberPage(
                    completedCallback: () {
                      currentStep = 3;
                      currentTab = 2;
                      _pageController.jumpToPage(currentTab);
                      setState(() {});
                    },
                  ),
                  AddGroupCoverImagePage(
                    completedCallback: (group) {
                      print("Show alert success");
                      Timer(Duration(milliseconds: 500), () {
                        try {
                          DialogHelper.showAlertDialog(
                            context: context,
                            title: "Tạo nhóm thành công",
                            description:
                                "Bắt đầu chia sẻ thông tin hữu ích với bạn bè ngay thôi.",
                            okText: S.of(context).ok,
                            okFunction: () {
                              Get.back();

                              print("Get back and goto group detail");

                              Get.to(
                                GroupDetailPage(
                                  groupId: group.id,
                                ),
                              );
                            },
                          );
                        } catch (e) {
                          print("AddGroupCoverImageView timer error ${e}");
                        }
                      });
                    },
                  ),
                ],
              ),
            ),
            CreateNewGroupStep(
              currentStep: currentStep,
            ),
          ],
        ),
      ),
    );
  }

  Widget _getAppBar() {
    return AppBar(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      leadingWidth: 52,
      centerTitle: false,
      title: Text(
        S.current.group_form_group_title,
        style: Theme.of(context).textTheme.headline2.merge(
              TextStyle(
                color: AppColors.textBlack,
                fontSize: 16,
              ),
            ),
      ),
      actions: [
        Container(
          margin: EdgeInsets.only(right: 10),
          child: Center(
            child: ElevatedButton(
              onPressed: () {
                /// Go to create new group page view
                currentStep += 1;
                if (currentStep > 3) {
                  currentStep = 3;
                  _showCompletedCreateNewGroupSuccess();
                }

                currentTab += 1;
                if (currentTab > 2) {
                  currentTab = 2;
                }
                print(currentStep);
                _pageController.jumpToPage(currentTab);
                setState(() {});
              },
              style: ElevatedButton.styleFrom(
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(20.0),
                ),
                primary: AppColors.red,
                elevation: 0,
              ),
              child: Text(
                currentStep == 3
                    ? S.of(context).common_button_finished
                    : S.of(context).common_button_continue,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 14,
                  color: AppColors.white,
                ),
                textAlign: TextAlign.left,
              ),
            ),
          ),
        ),
      ],
    );
  }

  void _showCompletedCreateNewGroupSuccess() {
    DialogHelper.showAlertDialog(
      context: Get.context,
      title: S.of(context).group_create_alert_success_title,
      description: S.of(context).group_create_alert_success_content,
      okText: S.of(Get.context).ok,
      okFunction: () {
        Get.to(GroupDetailPage());
      },
    );
  }
}
