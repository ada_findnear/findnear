import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/pages/groups/create_new_group/create_new_group_pageview/create_new_group_pageview_view.dart';
import 'package:findnear/src/pages/groups/group_detail_page/view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

import '../logic.dart';
import '../state.dart';

class ListMyGroupView extends StatefulWidget {
  const ListMyGroupView({Key key}) : super(key: key);

  @override
  _ListMyGroupViewState createState() => _ListMyGroupViewState();
}

class _ListMyGroupViewState extends State<ListMyGroupView> {
  MyGroupNewsfeedLogic logic;
  MyGroupNewsfeedState state;

  @override
  void initState() {
    super.initState();
    logic = Get.find<MyGroupNewsfeedLogic>();
    state = logic.state;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      width: double.infinity,
      child: Obx(() {
        final myGroups = state.myGroups;
        return ListView.separated(
          separatorBuilder: (BuildContext context, int index) {
            return SizedBox(
              width: 10,
            );
          },
          padding: EdgeInsets.symmetric(horizontal: 15),
          itemCount: myGroups.length + 1,
          scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext context, int index) {
            if (index == 0) {
              return _buildCreateGroupButton();
            }
            final groupIndex = index - 1;
            final group = myGroups.elementAt(groupIndex);
            return _buildGroupItem(group);
          },
        );
      }),
    );
  }

  Widget _buildCreateGroupButton() {
    return GestureDetector(
      onTap: () {
        Get.to(CreateNewGroupPageViewView());
      },
      child: Container(
        height: 70,
        width: 70,
        decoration: new BoxDecoration(
          color: const Color.fromRGBO(0, 0, 0, 0.7),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              Icons.add_circle,
              size: 20,
              color: AppColors.white,
            ),
            Text(
              S.of(context).my_group_create_new_group_button,
              style: Theme.of(context).textTheme.bodyText1.merge(
                    TextStyle(
                      color: AppColors.white,
                      fontSize: 10,
                    ),
                  ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildGroupItem(GroupEntity group) {
    String groupThumbnail;
    final media = group.media;
    if (media != null && media.length > 0) {
      groupThumbnail = media.first.thumb;
    }
    return GestureDetector(
      onTap: () {
        Get.to(
          GroupDetailPage(
            groupId: group.id,
          ),
        );
      },
      child: Container(
        height: 70,
        width: 70,
        decoration: new BoxDecoration(
          color: const Color.fromRGBO(0, 0, 0, 1),
          image: new DecorationImage(
            fit: BoxFit.cover,
            colorFilter: new ColorFilter.mode(
                Colors.black.withOpacity(0.4), BlendMode.dstATop),
            image: groupThumbnail != null
                ? new NetworkImage(
                    groupThumbnail,
                  )
                : AssetImage(AppImages.icLogo),
          ),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Align(
          alignment: Alignment.bottomLeft,
          child: Padding(
            padding: EdgeInsets.all(2),
            child: Text(
              "Hội Quán Shipper Hội Quán Shipper Hội Quán Shipper",
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context).textTheme.overline.merge(
                    TextStyle(
                      color: AppColors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
            ),
          ),
        ),
      ),
    );
  }
}
