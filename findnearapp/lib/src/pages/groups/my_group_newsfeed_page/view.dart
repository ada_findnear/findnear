import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/GroupNewsFeedItemWidget.dart';
import 'package:findnear/src/models/bottom_sheet_option.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/pages/create_post/create_post.controller.dart';
import 'package:findnear/src/pages/create_post/create_post.screen.dart';
import 'package:findnear/src/pages/groups/group_detail_page/view.dart';
import 'package:findnear/src/pages/groups/group_newsfeed/group_newsfeed_empty/view.dart';
import 'package:findnear/src/pages/groups/my_group/my_group_empty/my_group_empty_view.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'list_my_group/view.dart';
import 'logic.dart';
import 'state.dart';

class MyGroupNewsFeedPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  MyGroupNewsFeedPage({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _GroupViewState createState() => _GroupViewState();
}

class _GroupViewState extends State<MyGroupNewsFeedPage>
    with AutomaticKeepAliveClientMixin {
  MyGroupNewsfeedLogic logic;
  MyGroupNewsfeedState state;

  final RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  List<Post> _posts = [];

  @override
  void initState() {
    super.initState();
    logic = Get.put(MyGroupNewsfeedLogic(
      scaffoldKey: widget.parentScaffoldKey,
    ));
    state = Get.find<MyGroupNewsfeedLogic>().state;
    logic.initialize();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      body: SafeArea(
        child: Obx(() {
          if (state.getDataLoadingState == LoadingState.loading) {
            return Container(
              alignment: Alignment.center,
              child: LoadingView(),
            );
          }
          final posts = state.posts;
          _posts = posts;

          return Column(
            children: [
              /// My groups
              Container(
                padding: EdgeInsets.only(bottom: 10),
                margin: EdgeInsets.only(bottom: 2),
                child: ListMyGroupView(),
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 0.5,
                      color: AppColors.gray2,
                    )
                  ],
                ),
              ),

              /// My groups feed
              Expanded(
                child: _buildGroupNewsfeedList(),
              ),
            ],
          );
        }),
      ),
    );
  }

  Widget _buildGroupNewsfeedList() {
    final posts = _posts;

    return Obx(() {
      final isHasMyGroup = state.myGroups.length > 0;
      final isHasPost = state.posts.length > 0;

      return RefreshConfiguration.copyAncestor(
        context: Get.context,
        footerTriggerDistance: Get.height * 4,
        child: SmartRefresher(
          enablePullUp: state.canLoadMore.value,
          onRefresh: () async {
            print("onRefresh");
            await logic.onRefreshData();
            _refreshController?.refreshCompleted();
          },
          onLoading: () async {
            print("on load more");
            await logic.onLoadMoreData();
            _refreshController?.loadComplete();
          },
          controller: _refreshController,
          child: !isHasMyGroup
              ? MyGroupEmptyView()
              : !isHasPost
                  ? GroupNewsfeedEmpty()
                  : ListView.separated(
                      padding: EdgeInsets.only(top: 10),
                      itemCount: posts.length ?? 0,
                      itemBuilder: (context, index) {
                        // create NewsfeedItemWidget item
                        return Container(
                          padding: const EdgeInsets.only(top: 3, bottom: 3),
                          child: GetBuilder<MyGroupNewsfeedLogic>(
                            init: logic,
                            builder: (GetxController controller) {
                              final post = posts.elementAt(index);

                              return Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 15, vertical: 13),
                                margin:
                                    const EdgeInsets.symmetric(horizontal: 15),
                                decoration: BoxDecoration(
                                  color: Colors.transparent,
                                  border: Border.all(
                                    color: Color(0xFFEDEDED),
                                  ),
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                child: GroupNewsFeedItemWidget(
                                  post: post,
                                  isMyProfile: post?.creator?.id ==
                                      currentUser.value?.id,
                                  onPressed: () {
                                    Get.to(GroupDetailPage(
                                      groupId: post.group?.id,
                                    ));
                                  },
                                  onMyPostMenuPressed: () {
                                    _onMyPostMenuPressed(index);
                                  },
                                  onSharePost: () {
                                    _onClickSharePost(post);
                                  },
                                  onOtherUserPostMenuPressed: () async {
                                    onOtherUserPostMenuPressed(index);
                                  },
                                  toggleLikePost: (id) {
                                    if (!isLoggedIn) {
                                      NavigatorUtils.toLogin();
                                      return;
                                    }
                                    logic.toggleLikePost(index);
                                  },
                                  didUpdateCallback: () {
                                    logic.updatePost(atIndex: index);
                                  },
                                ),
                              );
                            },
                          ),
                        );
                      },
                      separatorBuilder: (context, index) => const Divider(
                        height: 8,
                        thickness: 8,
                        color: AppColors.white,
                      ),
                    ),
        ),
      );
    });
  }

  void _onMyPostMenuPressed(int index) async {
    Post post = _posts.elementAt(index);

    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    }
    final indexTapped = await DialogHelper.showBottomSheetOptions(
      context: context,
      options: [
        BottomSheetOption(
          iconPath: AppImages.icWrite,
          text: S.of(context).edit_post,
        ),
        BottomSheetOption(
          iconPath: AppImages.icTrash,
          text: S.of(context).delete_post,
        ),
        // temporarily disable
        // BottomSheetOption(
        //   iconPath: AppImages.icBookmarkOutlined,
        //   text: S.of(context).save_post,
        // ),
        BottomSheetOption(
          icon: Icons.share,
          text: S.of(context).shareThisPost,
        ),
      ],
    );
    switch (indexTapped) {
      case 0:
        _onClickEditPost(post);
        break;
      case 1:
        final isDeleting = await DialogHelper.showConfirmPopup(
          title: S.current.deletePost,
          description: S.current.confirmDeleteDescription,
          okBtnText: S.current.deletePostShort,
        );

        if (isDeleting ?? false) {
          logic.deletePost(index: index);
        }

        break;
      case 2:
        // share other app
        onClickSharePostOtherApp();
        break;
    }
  }

  void onOtherUserPostMenuPressed(int index) async {
    Post post = _posts.elementAt(index);

    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    }
    final indexTapped = await DialogHelper.showBottomSheetOptions(
      context: context,
      options: [
        // temporarily disable
        // BottomSheetOption(
        //   iconPath: AppImages.icBookmarkOutlined,
        //   text: S.of(context).save_post,
        // ),
        BottomSheetOption(
          iconPath: AppImages.icHide,
          text: S.of(context).hide_post,
        ),
        BottomSheetOption(
          icon: Icons.share,
          text: S.of(context).shareThisPost,
        ),
        BottomSheetOption(
          icon: Icons.report,
          text: S.of(context).reportThisPost,
          isReported: post.isReported,
        ),
      ],
    );
    switch (indexTapped) {
      // case 0:
      //   logic.savePost(postIndex);
      //   break;
      case 0:
        logic.hidePost(index);
        break;
      case 1:
        // _onClickSharePost(post);
        onClickSharePostOtherApp();
        break;
      case 2:
        state.posts[index].isReported = true;
        _onClickReport(post);
        break;
    }
  }

  void _onClickSharePost(Post post) async {
    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    }
    final originalPost = post.isSharedPost ? post.originalPost : post;
    Get.put(CreatePostController(originalPost: originalPost));
    var res = await Get.to(
      () => CreatePostScreen(
        user: currentUser.value,
        originalPost: originalPost,
      ),
    );
    Get.delete<CreatePostController>();
    if (res != null) logic.initialize();
  }

  void _onClickEditPost(Post post) async {
    Get.put(CreatePostController(post: post));
    final result = await Get.to(() => CreatePostScreen(
          user: currentUser.value,
          originalPost: post.originalPost,
        ));
    Get.delete<CreatePostController>();
    if (result != null) logic.initialize();
  }

  void onClickSharePostOtherApp() async {
    await FlutterShare.share(
      title: 'Example share',
      text: 'Example share text',
      linkUrl: 'https://flutter.dev/',
      chooserTitle: 'Example Chooser Title',
    );
  }

  void _onClickReport(Post post) {
    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    }
    DialogHelper.showReportPopup(post);
  }

  /// Handle AutomaticKeepAliveClientMixin
  @override
  bool get wantKeepAlive => true;
}
