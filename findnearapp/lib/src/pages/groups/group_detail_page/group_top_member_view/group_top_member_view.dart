import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/elements/CircularAvatarWidget.dart';
import 'package:flutter/material.dart';

class GroupTopMemberView extends StatelessWidget {
  static const double radius = 20;
  static const double overlappingPixels = 5;
  final List<String> images;

  const GroupTopMemberView({Key key, this.images = const []}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final int count = images.length > 3 ? 3 : images.length;
    double firstItemOffset = 0;
    double secondItemOffset = 0;
    if (count == 2) {
      firstItemOffset = radius * 2 - overlappingPixels;
    } else if (count > 2) {
      secondItemOffset = radius * 2 - overlappingPixels;
      firstItemOffset = secondItemOffset + radius * 2 - overlappingPixels;
    }

    return Row(
      children: [
        Container(
          width: 3 * 2 * radius - (3 - 1) * overlappingPixels,
          height: radius * 2 + 1,
          alignment: Alignment.centerLeft,
          child: Stack(
            alignment: Alignment.centerLeft,
            clipBehavior: Clip.none,
            children: [
              if (images.length > 0)
                Positioned(
                  right: firstItemOffset,
                  child: CircularAvatarWidget(
                    imageUrl: images.elementAt(0),
                    size: 2 * radius,
                    hasBorder: true,
                    borderColor: AppColors.white,
                  ),
                ),
              if (images.length > 1)
                Positioned(
                  right: secondItemOffset,
                  child: CircularAvatarWidget(
                    imageUrl: images.elementAt(1),
                    size: 2 * radius,
                    hasBorder: true,
                    borderColor: AppColors.white,
                  ),
                ),
              if (images.length > 2)
                Positioned(
                  right: 0,
                  child: _buildMoreUsersWidget(context),
                ),
            ],
          ),
        ),
        Expanded(child: Container()),
      ],
    );
  }

  Widget _buildMoreUsersWidget(BuildContext context) {
    return Container(
      width: 2 * radius,
      height: 2 * radius,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(radius),
        color: AppColors.gray,
      ),
      child: Center(
        child: Icon(
          Icons.more_horiz,
          color: Colors.white,
        ),
      ),
    );
  }
}
