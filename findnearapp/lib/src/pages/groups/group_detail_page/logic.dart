import 'package:dio/dio.dart';
import 'package:findnear/src/models/input/GetPostsOfGroupInput.dart';
import 'package:findnear/src/models/input/group_member_input.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/repository/findnear_group_repository.dart';
import 'package:findnear/src/repository/newsfeed_repository.dart';
import 'package:findnear/src/repository/post_repository.dart' as postRepo;
import 'package:findnear/src/utils/logger.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'state.dart';

class GroupDetailLogic extends GetxController {
  final NewsfeedRepository newsfeedRepository = Get.find();

  final GroupDetailState state;

  final FindNearGroupRepository _repository = Get.find();

  final int limit = 10;

  int groupId;

  GroupDetailLogic({
    GlobalKey<ScaffoldState> scaffoldKey,
  }) : state = GroupDetailState();

  void initialize() async {
    if (groupId == null) {
      return;
    }
    state.loadingState.value = LoadingState.loading;
    await getGroupMembers(groupId);
    await getGroupDetail(groupId);
    await getPostsOfGroup();
    state.loadingState.value = LoadingState.success;
  }

  void onRefreshData() async {
    state.currentOffset.value = 0;
    state.isLoading.value = true;
    await getGroupMembers(groupId);
    await getGroupDetail(groupId);
    await getPostsOfGroup(
      offset: 0,
      limit: limit,
      isRefresh: true,
    );
    state.isLoading.value = false;
  }

  void onLoadMoreData() async {
    if (!state.canLoadMore.value) {
      return;
    }
    state.isLoadingMore.value = true;
    state.canLoadMore.value = false;
    await getPostsOfGroup(offset: state.currentOffset.value, limit: limit);
    state.isLoadingMore.value = false;
  }

  Future<void> getGroupDetail(int id) async {
    try {
      state.loadingState.value = LoadingState.loading;
      final res = await _repository.getGroupDetail(id);
      final group = res.data;
      state.group.value = group;
      state.loadingState.value = LoadingState.success;
    } catch (e) {
      state.loadingState.value = LoadingState.failure;
      print("getGroupDetail");
      print(e);
      if (e is DioError) {
        print(e.response);
      }
    }
  }

  Future<void> getPostsOfGroup({
    offset = 0,
    limit = 10,
    isRefresh = false,
  }) async {
    try {
      final input = GetPostsOfGroupInput(groupId: groupId);
      final res = await _repository.getPostsOfGroup(
        input,
        offset: offset,
        limit: limit,
      );
      final posts = res.data;

      if (isRefresh) {
        state.posts.clear();
      }

      state.posts.addAll(posts);

      state.canLoadMore.value = posts.length == limit;
      state.currentOffset.value = state.posts.length;
    } catch (e) {
      print("getPostsOfGroup");
      print(e);
      if (e is DioError) {
        print(e.response);
      }
    }
  }

  Future<void> getGroupMembers(
    int groupId, {
    offset = 0,
    limit = 10,
    isRefresh = false,
  }) async {
    try {
      final input = GroupMemberInput(
        groupId: groupId,
      );
      final res = await _repository.getGroupMembers(
        input,
        // offset: offset,
        // limit: limit,
      );
      final members = res.data;

      if (isRefresh) {
        state.members.clear();
      }

      state.members.addAll(members);
    } catch (e) {
      print("getGroupMembers");
      print(e);
      if (e is DioError) {
        print(e.response);
      }
    }
  }

  /// Group post
  void toggleLikePost(int index) async {
    final Post post = state.posts.elementAt(index);
    final updatePost = post.copyWith(
      isLike: !post.isLike,
      likers_count:
          "${(int.tryParse(post.likers_count) ?? 0) + (post.isLike ? -1 : 1)}",
    );
    state.posts[index] = updatePost;
    try {
      final result = await newsfeedRepository.toggleLikePost(post.id);
      post.isLike = result.isLike;
    } catch (e) {
      logger.e(e);
      state.posts[index] = post;
    }
  }

  void hidePost(int index) async {
    final Post post = state.posts.elementAt(index);
    try {
      final result = await newsfeedRepository.hidePost(post.id);
      if (result.success) {
        state.posts.removeAt(index);
        state.message.value = MessageStates.hidePostSuccessfully;
      }
    } catch (e) {
      print(e);
    }
  }

  void savePost(int index) async {
    final Post post = state.posts.elementAt(index);
    try {
      final result = await newsfeedRepository.savePost(post.id);
      if (result.success) {
        state.message.value = MessageStates.savePostSuccessfully;
      }
    } catch (e) {
      print(e);
    }
  }

  void updatePost({int atIndex}) async {
    final postId = state.posts.elementAt(atIndex).id;
    try {
      final post = await newsfeedRepository.getPostDetails(postId);
      state.posts[atIndex] = post;
    } catch (e) {
      logger.e(e);
    }
  }

  void deletePost({int index}) async {
    final post = state.posts.elementAt(index);
    try {
      final result = await postRepo.removePost(post);
      if (result) {
        state.posts.removeAt(index);
        initialize();
      }
    } catch (e) {
      logger.e(e);
    }
  }
}
