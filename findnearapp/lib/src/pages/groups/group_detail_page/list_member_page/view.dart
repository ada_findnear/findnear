import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/CircularAvatarWidget.dart';
import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/groups/group_detail_page/list_member_page/state.dart';
import 'package:findnear/src/pages/groups/group_invite_friend_page/view.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:findnear/src/widgets/shimmer/list_loading_shimmer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'logic.dart';

class GroupDetailListMemberPage extends StatefulWidget {
  GroupEntity group;
  GroupDetailListMemberPage({
    Key key,
    this.group,
  }) : super(key: key);

  @override
  _GroupDetailListMemberPageState createState() =>
      _GroupDetailListMemberPageState();
}

class _GroupDetailListMemberPageState extends State<GroupDetailListMemberPage>
    with AutomaticKeepAliveClientMixin {
  RefreshController refreshController =
      RefreshController(initialRefresh: false);

  GroupDetailListMemberLogic logic = Get.put(GroupDetailListMemberLogic());
  GroupDetailListMemberState state =
      Get.find<GroupDetailListMemberLogic>().state;

  @override
  void initState() {
    super.initState();

    if (widget.group != null) {
      logic.groupId = widget.group.id;
      logic.initialize();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _getAppBar(),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return Obx(() {
      if (state.isLoading.value) {
        return ListLoadingShimmer();
      }

      return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 10,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 15),
            child: ElevatedButton(
              onPressed: () {
                Get.to(() => GroupInviteFriendPage());
              },
              style: ElevatedButton.styleFrom(
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(20.0),
                ),
                primary: AppColors.red200,
                elevation: 0,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(
                    Icons.person_add,
                    color: AppColors.red,
                    size: 15,
                  ),
                  Text(
                    'Thêm thành viên',
                    style: TextStyle(
                      fontFamily: 'Quicksand-bold',
                      fontSize: 12,
                      color: AppColors.red,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ],
              ),
            ),
          ),
          Obx(() {
            return Container(
              margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: Text(
                'Danh sách thành viên (${state.members.length})',
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 14,
                  color: AppColors.black,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.left,
              ),
            );
          }),
          Expanded(
            child: _buildMemberList(),
          ),
        ],
      );
    });
  }

  Widget _buildMemberList() {
    return Obx(() {
      final members = state.members;
      final isHasData = members.length > 0;

      return Container(
        child: RefreshConfiguration.copyAncestor(
          context: Get.context,
          footerTriggerDistance: Get.height * 4,
          child: SmartRefresher(
            enablePullUp: state.canLoadMore.value,
            onRefresh: () async {
              await logic.onRefreshData();
              refreshController?.refreshCompleted();
            },
            onLoading: () async {
              await logic.onLoadMoreData();
              refreshController?.loadComplete();
            },
            controller: refreshController,
            child: !isHasData
                ? Center(
                    child: Text("Không có thành viên"),
                  )
                : ListView.separated(
                    separatorBuilder: (BuildContext context, int index) {
                      return SizedBox(
                        height: 10,
                      );
                    },
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    itemCount: members.length,
                    itemBuilder: (BuildContext context, int index) {
                      final member = members.elementAt(index);
                      return _buildGroupItem(context, member, index);
                    },
                  ),
          ),
        ),
      );
    });
  }

  Container _buildGroupItem(
    BuildContext context,
    User member,
    int index,
  ) {
    String imageUrl = member.avatar ?? "";
    String groupName = member.name ?? "";
    String memberDesc = "";

    bool isMe = currentUser.value.id.toString() == member.id.toString();
    bool isAdmin = widget.group.owner_id.toString() == member.id.toString();
    if (isAdmin) {
      memberDesc = "Quản trị viên";
    } else {
      if (isMe) {
        memberDesc = "Bạn";
      } else {
        DateTime lastUpdatedDate = member.lastUpdatedDate;
        String joinedAt = "";
        if (lastUpdatedDate != null) {
          joinedAt = DateFormat('dd/MM/yyyy').format(lastUpdatedDate);
        }
        memberDesc = "Thành viên từ ${joinedAt}";
      }
    }

    return Container(
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(5),
            child: Builder(
              builder: (context) {
                final double size = 50;
                if (imageUrl == null || imageUrl == "")
                  return AvatarPlaceholder(size: size);
                return CachedNetworkImage(
                  imageUrl: imageUrl,
                  fit: BoxFit.cover,
                  width: size,
                  height: size,
                  placeholder: (context, url) => CupertinoActivityIndicator(),
                  errorWidget: (context, url, error) =>
                      AvatarPlaceholder(size: size),
                );
              },
            ),
          ),
          SizedBox(
            width: 5,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  groupName,
                  style: Theme.of(context).textTheme.headline6.merge(
                        TextStyle(
                          color: AppColors.textBlack,
                          fontSize: 14,
                          fontFamily: 'Quicksand',
                        ),
                      ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  children: [
                    isAdmin
                        ? AppImage(
                            AppImages.ic_admin_medal,
                            height: 15,
                            width: 15,
                          )
                        : Container(),
                    Expanded(
                      child: Text(
                        memberDesc,
                        style: Theme.of(context).textTheme.bodyText1.merge(
                              TextStyle(
                                color: AppColors.gray2,
                                fontSize: 10,
                              ),
                            ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _getAppBar() {
    return AppBar(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      leadingWidth: 52,
      centerTitle: false,
      title: Text(
        "Thành viên",
        style: Theme.of(context).textTheme.headline2.merge(
              TextStyle(
                color: AppColors.textBlack,
                fontSize: 16,
              ),
            ),
      ),
      actions: [],
    );
  }

  /// Handle AutomaticKeepAliveClientMixin
  @override
  bool get wantKeepAlive => true;
}
