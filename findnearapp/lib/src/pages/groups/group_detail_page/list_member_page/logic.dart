import 'package:dio/dio.dart';
import 'package:findnear/src/models/input/group_member_input.dart';
import 'package:findnear/src/repository/findnear_group_repository.dart';
import 'package:findnear/src/repository/newsfeed_repository.dart';
import 'package:findnear/src/services/notification_service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'state.dart';

class GroupDetailListMemberLogic extends GetxController {
  final NotificationService notificationService =
      Get.find<NotificationService>();

  final NewsfeedRepository newsfeedRepository = Get.find();

  final GroupDetailListMemberState state;

  final FindNearGroupRepository _repository = Get.find();

  final int limit = 10;

  int groupId;

  GroupDetailListMemberLogic({
    GlobalKey<ScaffoldState> scaffoldKey,
  }) : state = GroupDetailListMemberState();

  void initialize() async {
    if (groupId == null) {
      return;
    }
    state.isLoading.value = true;
    await getGroupMembers(
      groupId,
      offset: state.currentOffset.value,
      limit: limit,
    );
    state.isLoading.value = false;
  }

  void onRefreshData() async {
    state.currentOffset.value = 0;
    state.isRefreshing.value = true;
    await getGroupMembers(
      groupId,
      offset: state.currentOffset.value,
      limit: limit,
      isRefresh: true,
    );
    state.isRefreshing.value = false;
  }

  void onLoadMoreData() async {
    if (!state.canLoadMore.value) {
      return;
    }
    state.isLoadingMore.value = true;
    state.canLoadMore.value = false;
    await getGroupMembers(
      groupId,
      offset: state.currentOffset.value,
      limit: limit,
    );
    state.isLoadingMore.value = false;
  }

  Future<void> getGroupMembers(
    int groupId, {
    offset = 0,
    limit = 10,
    isRefresh = false,
  }) async {
    try {
      final input = GroupMemberInput(
        groupId: groupId,
      );
      final res = await _repository.getGroupMembers(
        input,
        offset: offset,
        limit: limit,
      );
      final members = res.data;

      if (isRefresh) {
        state.members.clear();
      }
      state.members.addAll(members);

      state.canLoadMore.value = members.length == limit;
      state.currentOffset.value = state.members.length;
    } catch (e) {
      print("getGroupMembers");
      print(e);
      if (e is DioError) {
        print(e.response);
      }
    }
  }
}
