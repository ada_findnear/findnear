import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:get/get_rx/get_rx.dart';

class GroupDetailListMemberState {
  RxInt currentOffset = 0.obs;
  Rx<bool> isLoading = false.obs;
  Rx<bool> isRefreshing = false.obs;
  Rx<bool> isLoadingMore = false.obs;
  RxBool canLoadMore = true.obs;
  Rx<MessageStates> message = MessageStates.idle.obs;
  RxList<User> members = List<User>.empty().obs;
  GroupDetailListMemberState();
}
