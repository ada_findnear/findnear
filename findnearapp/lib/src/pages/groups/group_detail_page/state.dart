import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:get/get_rx/get_rx.dart';

class GroupDetailState {
  RxInt currentOffset = 0.obs;
  Rx<bool> isLoading = false.obs;
  Rx<bool> isLoadingMore = false.obs;
  RxBool canLoadMore = true.obs;
  Rx<MessageStates> message = MessageStates.idle.obs;

  Rx<GroupEntity> group = (new GroupEntity()).obs;

  RxList<Post> posts = List<Post>.empty().obs;

  RxList<User> members = List<User>.empty().obs;

  Rx<LoadingState> loadingState = LoadingState.idle.obs;

  GroupDetailState();
}
