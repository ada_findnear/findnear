import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/bottom_sheet_option.dart';
import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/pages/create_post/create_post.controller.dart';
import 'package:findnear/src/pages/create_post/create_post.screen.dart';
import 'package:findnear/src/pages/groups/create_new_group/create_new_group_form_page/view.dart';
import 'package:findnear/src/pages/groups/group_detail_page/widgets/group_post_ttem_widget.dart';
import 'package:findnear/src/pages/groups/group_detail_setting_page/view.dart';
import 'package:findnear/src/pages/groups/group_invite_friend_page/view.dart';
import 'package:findnear/src/pages/groups/update_group_viewer_status_page/view.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:findnear/src/widgets/shimmer/list_loading_shimmer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'group_top_member_view/group_top_member_view.dart';
import 'list_member_page/view.dart';
import 'logic.dart';
import 'state.dart';

class GroupDetailPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  int groupId;

  GroupDetailPage({
    Key key,
    this.parentScaffoldKey,
    this.groupId,
  }) : super(key: key);

  @override
  _GroupDetailPageViewState createState() => _GroupDetailPageViewState();
}

class _GroupDetailPageViewState extends State<GroupDetailPage> {
  GroupDetailLogic logic;
  GroupDetailState state;

  final _refreshController = RefreshController(initialRefresh: false);

  final TextEditingController _textEditingController = TextEditingController();

  List<Post> _posts = [];

  @override
  void initState() {
    super.initState();
    logic = Get.put(GroupDetailLogic(
      scaffoldKey: widget.parentScaffoldKey,
    ));
    state = Get.find<GroupDetailLogic>().state;

    if (widget.groupId != null) {
      logic.groupId = widget.groupId;
      logic.initialize();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      extendBodyBehindAppBar: true,
      appBar: _getAppBar(),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return Obx(() {
      if (state.loadingState.value == LoadingState.loading) {
        return ListLoadingShimmer();
      }

      final group = state.group.value;
      String groupCover = group.groupThumbnail ?? "";
      String groupOwnerName = "";

      bool isMyGroup = group?.isMyGroup ?? false;

      if (isMyGroup) {
        groupOwnerName = "Tôi";
      } else {
        groupOwnerName = group.owner?.name ?? "";
      }

      return RefreshConfiguration.copyAncestor(
        context: Get.context,
        footerTriggerDistance: Get.height * 4,
        child: SmartRefresher(
          enablePullUp: state.canLoadMore.value,
          onRefresh: () async {
            print("onRefresh");
            await logic.onRefreshData();
            _refreshController?.refreshCompleted();
          },
          onLoading: () async {
            print("on load more");
            await logic.onLoadMoreData();
            _refreshController?.loadComplete();
          },
          controller: _refreshController,
          child: Container(
            child: Column(
              children: [
                Stack(
                  children: [
                    _buildCoverGroup(groupCover),
                    _buildOwnerGroupLabel(groupOwnerName),
                    _buildBlurBackgroundCover(),
                    _buildEditGroupBtn(),
                  ],
                ),
                Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: 15,
                    vertical: 10,
                  ),
                  child: _buildGroupInformation(group),
                ),
                Container(
                  height: 40,
                  width: double.infinity,
                  margin: EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    children: [
                      Expanded(
                        child: _buildCreatePostWidget(context),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: _buildBodyWidget(),
                ),
              ],
            ),
          ),
        ),
      );
    });
  }

  Positioned _buildBlurBackgroundCover() {
    return Positioned(
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      child: Container(
        decoration: new BoxDecoration(
          gradient: new LinearGradient(
            begin: Alignment(0, -1),
            end: Alignment(0, 0),
            colors: [
              Colors.black.withOpacity(1),
              Colors.black.withOpacity(0.1),
            ],
          ),
        ),
      ),
    );
  }

  Positioned _buildEditGroupBtn() {
    return Positioned(
      bottom: 20,
      right: 10,
      child: GestureDetector(
        onTap: () async {
          await Get.to(CreateNewGroupFormPage(group: state.group.value));
          logic.getGroupDetail(state.group.value.id);
        },
        child: Container(
          height: 26,
          width: 26,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(13),
            color: AppColors.black,
          ),
          child: Icon(
            Icons.edit,
            size: 20,
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  Positioned _buildOwnerGroupLabel(String groupOwnerName) {
    return Positioned(
      bottom: 0,
      left: 0,
      right: 0,
      child: Container(
        height: 35,
        padding: EdgeInsets.symmetric(
          horizontal: 15,
        ),
        color: AppColors.red.withOpacity(0.7),
        child: Align(
          alignment: Alignment.centerLeft,
          child: RichText(
            textAlign: TextAlign.start,
            text: TextSpan(
              children: [
                TextSpan(
                  text: "Nhóm của ",
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 14,
                    color: AppColors.white,
                    fontWeight: FontWeight.w300,
                    height: 1.4,
                  ),
                ),
                TextSpan(
                  text: groupOwnerName,
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 14,
                    color: AppColors.white,
                    fontWeight: FontWeight.bold,
                    height: 1.4,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Container _buildCoverGroup(String groupCover) {
    return Container(
      height: 200,
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.green,
        boxShadow: [
          BoxShadow(
            blurRadius: 0.5,
            color: AppColors.gray2,
          )
        ],
      ),
      child: groupCover.isEmpty
          ? AppImage(
              AppImages.icLogo,
              fit: BoxFit.cover,
            )
          : Image.network(
              groupCover,
              fit: BoxFit.cover,
            ),
    );
  }

  Widget _buildGroupInformation(GroupEntity group) {
    String groupName = group.title ?? "";
    String groupDesc = group.content ?? "";
    String numberOfMember = "${group.members_count ?? 0} thành viên";
    String numberOfPost = "${group.posts_count ?? 0} bài viết";
    String groupPublishStatus = group.viewerDisplay ?? "";
    IconData viewerIcon = group?.viewerIcon ?? Icons.lock_outline;
    bool isMember = true; // TODO: -

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Expanded(
              child: Text(
                groupName,
                style: Theme.of(context).textTheme.headline6.merge(
                      TextStyle(
                        color: AppColors.textBlack,
                        fontSize: 14,
                        fontFamily: 'Quicksand',
                      ),
                    ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            isMember
                ? Row(
                    children: [
                      Icon(
                        Icons.check,
                        size: 16,
                        color: AppColors.green,
                      ),
                      Text(
                        "Đã là thành viên",
                        style: Theme.of(context).textTheme.bodyText1.merge(
                              TextStyle(
                                color: AppColors.green,
                                fontSize: 14,
                                fontFamily: 'Quicksand',
                              ),
                            ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  )
                : Container()
          ],
        ),
        SizedBox(
          height: 5,
        ),
        Row(
          children: [
            GestureDetector(
              onTap: () async {
                /// TODO: - Neeus có phân quyền cần đổi lại điều kiện ở đây
                final isCanSetting = state.group?.value?.isMyGroup ?? false;

                if (!isCanSetting) {
                  return;
                }

                await Get.to(UpdateGroupViewerStatus(
                  group: group,
                ));

                /// Reload group detail
                logic.getGroupDetail(group.id);
              },
              child: Row(
                children: [
                  Icon(
                    viewerIcon,
                    size: 10,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    groupPublishStatus,
                    style: Theme.of(context).textTheme.bodyText1.merge(
                          TextStyle(
                            color: AppColors.gray2,
                            fontSize: 10,
                          ),
                        ),
                  )
                ],
              ),
            ),
            SizedBox(
              width: 15,
            ),
            GestureDetector(
              onTap: () {
                Get.to(() => GroupDetailListMemberPage(
                      group: group,
                    ));
              },
              child: Row(
                children: [
                  Icon(
                    Icons.people_alt_rounded,
                    size: 10,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    numberOfMember,
                    style: Theme.of(context).textTheme.bodyText1.merge(
                          TextStyle(
                            color: AppColors.gray2,
                            fontSize: 10,
                          ),
                        ),
                  )
                ],
              ),
            ),
            SizedBox(
              width: 15,
            ),
            Row(
              children: [
                Icon(
                  Icons.sticky_note_2,
                  size: 10,
                ),
                SizedBox(
                  width: 5,
                ),
                Text(
                  numberOfPost,
                  style: Theme.of(context).textTheme.bodyText1.merge(
                        TextStyle(
                          color: AppColors.gray2,
                          fontSize: 10,
                        ),
                      ),
                )
              ],
            )
          ],
        ),
        SizedBox(
          height: 5,
        ),
        Container(
          child: Row(
            children: [
              Expanded(
                child: Obx(() {
                  final members = state.members;
                  final memberImages =
                      members.map((element) => element.avatar).toList();
                  return GroupTopMemberView(
                    images: memberImages,
                  );
                }),
              ),
              Container(
                child: ElevatedButton(
                  onPressed: () {
                    /// Go to create new group page view
                    Get.to(
                      () => GroupInviteFriendPage(
                        groupId: widget.groupId,
                      ),
                    );
                  },
                  style: ElevatedButton.styleFrom(
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(20.0),
                    ),
                    primary: AppColors.red,
                    elevation: 0,
                  ),
                  child: Row(
                    children: [
                      Icon(
                        Icons.add_outlined,
                        color: Colors.white,
                        size: 15,
                      ),
                      Text(
                        'Mời bạn bè',
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          fontSize: 12,
                          color: AppColors.white,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          groupDesc,
          style: Theme.of(context).textTheme.bodyText1.merge(
                TextStyle(
                  color: AppColors.gray2,
                  fontSize: 12,
                ),
              ),
        ),
      ],
    );
  }

  Widget _buildCreatePostWidget(BuildContext context) {
    return TextField(
      onTap: () async {
        /// Go to "Tạo mới bài đăng";
        Get.put(CreatePostController());
        var res = await goTo(
          screen: CreatePostScreen(
            user: currentUser.value,
            medias: [],
            group: state.group.value,
          ),
        );
        Get.delete<CreatePostController>();
        if (res != null) logic.initialize();
      },
      readOnly: true,
      controller: _textEditingController,
      textInputAction: TextInputAction.search,
      decoration: new InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          borderSide: BorderSide(color: AppColors.grayLight, width: 1),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          borderSide: BorderSide(color: AppColors.grayLight, width: 1),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          borderSide: BorderSide(color: AppColors.grayLight, width: 1),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          borderSide: BorderSide(color: AppColors.grayLight, width: 1),
        ),
        disabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          borderSide: BorderSide(color: AppColors.grayLight, width: 1),
        ),
        hintText: "Bạn muốn đăng gì nào?",
        hintStyle: Theme.of(context).textTheme.bodyText1.merge(
              TextStyle(
                color: AppColors.gray2,
                fontSize: 12,
              ),
            ),
        suffixIcon: Icon(
          Icons.close_rounded,
          color: Theme.of(context).iconTheme.color,
          size: 20,
        ),
      ),
      onChanged: (textSearch) {
        // TODO: -
      },
      onSubmitted: (text) {},
    );
  }

  Widget _buildBodyWidget() {
    final posts = state.posts;
    final isHasData = posts.length > 0;

    if (!isHasData) {
      return Container(
        child: Center(
          child: Text("Chưa có bài viết nào"),
        ),
      );
    }

    return ListView.separated(
      padding: EdgeInsets.only(top: 10),
      itemCount: posts.length ?? 0,
      separatorBuilder: (context, index) => const Divider(
        height: 8,
        thickness: 8,
        color: AppColors.white,
      ),
      itemBuilder: (context, index) {
        final post = posts.elementAt(index);
        return _buildGroupDetailPostCell(post, index);
      },
    );
  }

  Widget _buildGroupDetailPostCell(Post post, int index) {
    return Container(
      padding: const EdgeInsets.only(top: 3, bottom: 3),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 13),
        margin: const EdgeInsets.symmetric(horizontal: 15),
        decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border.all(
            color: Color(0xFFEDEDED),
          ),
          borderRadius: BorderRadius.circular(20),
        ),
        child: GroupPostItemWidget(
          post: post,
          isMyProfile: post?.creator?.id == currentUser.value?.id,
          onMyPostMenuPressed: () {
            _onMyPostMenuPressed(index);
          },
          onSharePost: () {
            _onClickSharePost(post);
          },
          onOtherUserPostMenuPressed: () async {
            onOtherUserPostMenuPressed(index);
          },
          toggleLikePost: (id) {
            if (!isLoggedIn) {
              NavigatorUtils.toLogin();
              return;
            }
            logic.toggleLikePost(index);
          },
          didUpdateCallback: () {
            logic.updatePost(atIndex: index);
          },
        ),
      ),
    );
  }

  void _onMyPostMenuPressed(int index) async {
    Post post = _posts.elementAt(index);

    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    }
    final indexTapped = await DialogHelper.showBottomSheetOptions(
      context: context,
      options: [
        BottomSheetOption(
          iconPath: AppImages.icWrite,
          text: S.of(context).edit_post,
        ),
        BottomSheetOption(
          iconPath: AppImages.icTrash,
          text: S.of(context).delete_post,
        ),
        // temporarily disable
        // BottomSheetOption(
        //   iconPath: AppImages.icBookmarkOutlined,
        //   text: S.of(context).save_post,
        // ),
        BottomSheetOption(
          icon: Icons.share,
          text: S.of(context).shareThisPost,
        ),
      ],
    );
    switch (indexTapped) {
      case 0:
        _onClickEditPost(post);
        break;
      case 1:
        final isDeleting = await DialogHelper.showConfirmPopup(
          title: S.current.deletePost,
          description: S.current.confirmDeleteDescription,
          okBtnText: S.current.deletePostShort,
        );

        if (isDeleting ?? false) {
          logic.deletePost(index: index);
        }

        break;
      case 2:
        // share other app
        onClickSharePostOtherApp();
        break;
    }
  }

  void onOtherUserPostMenuPressed(int index) async {
    Post post = _posts.elementAt(index);

    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    }
    final indexTapped = await DialogHelper.showBottomSheetOptions(
      context: context,
      options: [
        // temporarily disable
        // BottomSheetOption(
        //   iconPath: AppImages.icBookmarkOutlined,
        //   text: S.of(context).save_post,
        // ),
        BottomSheetOption(
          iconPath: AppImages.icHide,
          text: S.of(context).hide_post,
        ),
        BottomSheetOption(
          icon: Icons.share,
          text: S.of(context).shareThisPost,
        ),
        BottomSheetOption(
          icon: Icons.report,
          text: S.of(context).reportThisPost,
          isReported: post.isReported,
        ),
      ],
    );
    switch (indexTapped) {
      // case 0:
      //   logic.savePost(postIndex);
      //   break;
      case 0:
        logic.hidePost(index);
        break;
      case 1:
        // _onClickSharePost(post);
        onClickSharePostOtherApp();
        break;
      case 2:
        state.posts[index].isReported = true;
        _onClickReport(post);
        break;
    }
  }

  void _onClickSharePost(Post post) async {
    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    }
    final originalPost = post.isSharedPost ? post.originalPost : post;
    Get.put(CreatePostController(originalPost: originalPost));
    var res = await Get.to(
      () => CreatePostScreen(
        user: currentUser.value,
        originalPost: originalPost,
      ),
    );
    Get.delete<CreatePostController>();
    if (res != null) logic.initialize();
  }

  void _onClickEditPost(Post post) async {
    Get.put(CreatePostController(post: post));
    final result = await Get.to(() => CreatePostScreen(
          user: currentUser.value,
          originalPost: post.originalPost,
        ));
    Get.delete<CreatePostController>();
    if (result != null) logic.initialize();
  }

  void onClickSharePostOtherApp() async {
    await FlutterShare.share(
      title: 'Example share',
      text: 'Example share text',
      linkUrl: 'https://flutter.dev/',
      chooserTitle: 'Example Chooser Title',
    );
  }

  void _onClickReport(Post post) {
    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    }
    DialogHelper.showReportPopup(post);
  }

  Widget _getAppBar() {
    return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0,
      leadingWidth: 52,
      centerTitle: false,
      iconTheme: IconThemeData(color: AppColors.white),
      actions: [
        // IconButton(
        //   icon: AppImage(
        //     AppImages.icNotificationLight,
        //     color: Colors.white,
        //   ),
        //   onPressed: () {},
        // ),
        // IconButton(
        //   icon: Icon(
        //     Icons.search,
        //     color: Colors.white,
        //   ),
        //   onPressed: () {},
        // ),
        Obx(() {
          /// TODO: - Neeus có phân quyền cần đổi lại điều kiện ở đây
          final isCanSetting = state.group?.value?.isMyGroup ?? false;
          return Visibility(
            visible: isCanSetting,
            child: IconButton(
              icon: Icon(
                Icons.settings,
                color: Colors.white,
              ),
              onPressed: () {
                Get.to(() => GroupDetailSettingPage(group: state.group.value));
              },
            ),
          );
        })
      ],
    );
  }
}
