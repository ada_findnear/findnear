import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/elements/CircularAvatarWidget.dart';
import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class GroupPermissionCensorshipPage extends StatefulWidget {
  GroupEntity group;

  GroupPermissionCensorshipPage({
    Key key,
    this.group,
  }) : super(key: key);

  @override
  _GroupPermissionCensorshipPageState createState() =>
      _GroupPermissionCensorshipPageState();
}

class _GroupPermissionCensorshipPageState
    extends State<GroupPermissionCensorshipPage> {
  RefreshController refreshController =
      RefreshController(initialRefresh: false);

  int listCount = 100;

  final TextEditingController _textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _getAppBar(),
      body: Container(
        child: Column(
          children: [
            SizedBox(
              height: 15,
            ),
            Expanded(
              child: Container(
                child: RefreshConfiguration.copyAncestor(
                  context: Get.context,
                  footerTriggerDistance: Get.height * 4,
                  child: SmartRefresher(
                    enablePullUp: true,
                    onRefresh: () async {
                      // await logic.listenForPosts(isRefresh: true);
                      // logic.getListLiveStream();
                      refreshController?.refreshCompleted();
                    },
                    onLoading: () async {
                      // await logic.listenForPosts();
                      refreshController?.loadComplete();
                    },
                    controller: refreshController,
                    child: ListView.separated(
                      separatorBuilder: (BuildContext context, int index) {
                        return SizedBox(
                          height: 10,
                        );
                      },
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      itemCount: listCount, // TODO: -
                      itemBuilder: (BuildContext context, int index) {
                        if (index == 0) {
                          return Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Quản trị viên",
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline6
                                      .merge(
                                        TextStyle(
                                          color: AppColors.textBlack,
                                          fontSize: 14,
                                          fontFamily: 'Quicksand',
                                        ),
                                      ),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Wrap(
                                  runSpacing: 10,
                                  children: [
                                    _buildAdministratorsItem(context),
                                    _buildAdministratorsItem(context),
                                    _buildAdministratorsItem(context),
                                    _buildAdministratorsItem(context),
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Divider(
                                  height: 2,
                                  thickness: 2,
                                  color: AppColors.divider,
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  "Thêm quản trị viên",
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline6
                                      .merge(
                                        TextStyle(
                                          color: AppColors.textBlack,
                                          fontSize: 14,
                                          fontFamily: 'Quicksand',
                                        ),
                                      ),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Container(
                                  height: 40,
                                  width: double.infinity,
                                  margin: EdgeInsets.symmetric(horizontal: 15),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Expanded(
                                        child: TextField(
                                          controller: _textEditingController,
                                          textInputAction:
                                              TextInputAction.search,
                                          decoration: new InputDecoration(
                                            border: InputBorder.none,
                                            focusedBorder: InputBorder.none,
                                            enabledBorder: InputBorder.none,
                                            errorBorder: InputBorder.none,
                                            disabledBorder: InputBorder.none,
                                            contentPadding:
                                                EdgeInsets.symmetric(
                                                    vertical: 4),
                                            hintText: S.of(context).search,
                                            prefixIcon: Icon(
                                              Icons.search_rounded,
                                              color: Theme.of(context)
                                                  .iconTheme
                                                  .color,
                                              size: 20,
                                            ),
                                          ),
                                          onChanged: (textSearch) {
                                            // TODO: -
                                          },
                                          onSubmitted: (text) {},
                                        ),
                                      ),
                                      Visibility(
                                        visible: true,
                                        child: GestureDetector(
                                          onTap: null,
                                          child: Container(
                                            width: 48,
                                            child: Center(
                                              child: Icon(
                                                Icons.close_rounded,
                                                color: Theme.of(context)
                                                    .iconTheme
                                                    .color,
                                                size: 20,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    border: Border.all(
                                        color: Colors.grey, width: 1),
                                  ),
                                ),
                              ],
                            ),
                          );
                        }
                        return _buildGroupItem(context);
                      },
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Container _buildGroupItem(BuildContext context) {
    // TODO: -
    String imageUrl =
        "https://hinhnen123.com/wp-content/uploads/2021/06/anh-meo-cute-nhat-9.jpg";
    String groupName = "Hội quán Shipper";
    String numberMember = "1.235 thành viên";
    String numberPost = "502 bài viết";

    return Container(
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(5),
            child: Builder(
              builder: (context) {
                final double size = 50;
                if (imageUrl == null || imageUrl == "")
                  return AvatarPlaceholder(size: size);
                return CachedNetworkImage(
                  imageUrl: imageUrl,
                  fit: BoxFit.cover,
                  width: size,
                  height: size,
                  placeholder: (context, url) => CupertinoActivityIndicator(),
                  errorWidget: (context, url, error) =>
                      AvatarPlaceholder(size: size),
                );
              },
            ),
          ),
          SizedBox(
            width: 5,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  groupName,
                  style: Theme.of(context).textTheme.headline6.merge(
                        TextStyle(
                          color: AppColors.textBlack,
                          fontSize: 14,
                          fontFamily: 'Quicksand',
                        ),
                      ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ),
          Container(
            child: ElevatedButton(
              onPressed: () {
                /// Go to create new group page view
              },
              style: ElevatedButton.styleFrom(
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(20.0),
                ),
                primary: AppColors.red200,
                side: BorderSide(color: AppColors.red),
                elevation: 0,
              ),
              child: Text(
                'Mời',
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 14,
                  color: AppColors.red,
                ),
                textAlign: TextAlign.left,
              ),
            ),
          ),
          Container(
            child: ElevatedButton(
              onPressed: () {
                /// Go to create new group page view
              },
              style: ElevatedButton.styleFrom(
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(20.0),
                ),
                primary: Color(0xFFEDEDED),
                elevation: 0,
              ),
              child: Text(
                'Đã mời',
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 14,
                  color: Color(0xFFA8A8A8),
                ),
                textAlign: TextAlign.left,
              ),
            ),
          )
        ],
      ),
    );
  }

  Container _buildAdministratorsItem(BuildContext context) {
    // TODO: -
    String imageUrl =
        "https://hinhnen123.com/wp-content/uploads/2021/06/anh-meo-cute-nhat-9.jpg";
    String groupName = "Hội quán Shipper";
    String numberMember = "1.235 thành viên";
    String numberPost = "502 bài viết";

    return Container(
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(5),
            child: Builder(
              builder: (context) {
                final double size = 50;
                if (imageUrl == null || imageUrl == "")
                  return AvatarPlaceholder(size: size);
                return CachedNetworkImage(
                  imageUrl: imageUrl,
                  fit: BoxFit.cover,
                  width: size,
                  height: size,
                  placeholder: (context, url) => CupertinoActivityIndicator(),
                  errorWidget: (context, url, error) =>
                      AvatarPlaceholder(size: size),
                );
              },
            ),
          ),
          SizedBox(
            width: 5,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  groupName,
                  style: Theme.of(context).textTheme.headline6.merge(
                        TextStyle(
                          color: AppColors.textBlack,
                          fontSize: 14,
                          fontFamily: 'Quicksand',
                        ),
                      ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                Text(
                  'Quản trị viên',
                  style: Theme.of(context).textTheme.bodyText1.merge(
                        TextStyle(
                          color: AppColors.gray2,
                          fontSize: 12,
                        ),
                      ),
                )
              ],
            ),
          ),
          Container(
            child: ElevatedButton(
              onPressed: () {
                /// Go to create new group page view
              },
              style: ElevatedButton.styleFrom(
                elevation: 0,
                primary: Colors.transparent,
              ),
              child: Icon(
                Icons.delete,
                size: 20,
                color: Colors.black,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _getAppBar() {
    return AppBar(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      leadingWidth: 52,
      centerTitle: false,
      title: Text(
        "Quyền và người kiểm duyệt",
        style: Theme.of(context).textTheme.headline2.merge(
              TextStyle(
                color: AppColors.textBlack,
                fontSize: 16,
              ),
            ),
      ),
      actions: [],
    );
  }
}
