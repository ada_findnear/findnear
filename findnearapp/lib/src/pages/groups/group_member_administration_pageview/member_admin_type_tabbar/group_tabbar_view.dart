import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/pages/groups/group_member_administration_pageview/group_member_admin_invited/logic.dart';
import 'package:findnear/src/pages/groups/group_member_administration_pageview/group_member_admin_invited/state.dart';
import 'package:findnear/src/pages/groups/group_member_administration_pageview/group_member_admin_new/logic.dart';
import 'package:findnear/src/pages/groups/group_member_administration_pageview/group_member_admin_new/state.dart';
import 'package:findnear/src/pages/groups/group_member_administration_pageview/group_member_admin_rejected/logic.dart';
import 'package:findnear/src/pages/groups/group_member_administration_pageview/group_member_admin_rejected/state.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

enum MemberAdminStatus {
  new_members,
  rejected_members,
  invited_members,
}

class MemberAdminStatusTabBar extends StatelessWidget {
  Function itemClickCallback;
  MemberAdminStatus currentGroupTabBar;

  MemberAdminStatusTabBar({this.itemClickCallback, this.currentGroupTabBar});

  final _groupTabBarDataSource = MemberAdminStatus.values;

  final GroupMemberAdminNewState _newMemberState =
      Get.find<GroupMemberAdminNewLogic>().state;
  final GroupMemberAdminRejectedState _rejectedMemberState =
      Get.find<GroupMemberAdminRejectedLogic>().state;
  final GroupMemberAdminInvitedState _invitedMemberState =
      Get.find<GroupMemberAdminInvitedLogic>().state;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: double.infinity,
      child: ListView.separated(
        separatorBuilder: (BuildContext context, int index) {
          return SizedBox(
            width: 10,
          );
        },
        padding: EdgeInsets.symmetric(horizontal: 10),
        itemCount: _groupTabBarDataSource.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int index) {
          final tabbar = _groupTabBarDataSource.elementAt(index);
          return _buildGroupItem(context, tabbar);
        },
      ),
    );
  }

  String getGroupTabBarTitle(MemberAdminStatus tabBarType, int numberOfMember) {
    switch (tabBarType) {
      case MemberAdminStatus.new_members:
        return "Thành viên mới ($numberOfMember)";
      case MemberAdminStatus.rejected_members:
        return "Đã từ chối ($numberOfMember)";
      case MemberAdminStatus.invited_members:
        return "Đã mời ($numberOfMember)";
      default:
        return "";
    }
  }

  Widget _buildGroupItem(BuildContext context, MemberAdminStatus tabBarType) {
    return Obx(() {
      int numberOfMembers = 0;
      switch (tabBarType) {
        case MemberAdminStatus.new_members:
          numberOfMembers = _newMemberState.members.length;
          break;
        case MemberAdminStatus.rejected_members:
          numberOfMembers = _rejectedMemberState.members.length;
          break;
        case MemberAdminStatus.invited_members:
          numberOfMembers = _invitedMemberState.members.length;
          break;
      }
      final title = getGroupTabBarTitle(tabBarType, numberOfMembers);
      var isSelected = currentGroupTabBar == tabBarType;
      var titleTextColor = isSelected ? AppColors.white : AppColors.gray;
      var groupBgColor = isSelected ? AppColors.red : Color(0xFFF2F2F2);
      return GestureDetector(
        onTap: () {
          itemClickCallback.call(tabBarType);
        },
        child: Container(
          child: Align(
            alignment: Alignment.center,
            child: Container(
              height: 30,
              decoration: new BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: groupBgColor,
              ),
              child: Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  child: Text(
                    title,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context).textTheme.overline.merge(
                          TextStyle(
                            color: titleTextColor,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                  ),
                ),
              ),
            ),
          ),
        ),
      );
    });
  }
}
