import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/elements/CircularAvatarWidget.dart';
import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/groups/group_member_administration_pageview/member_admin_type_tabbar/group_tabbar_view.dart';
import 'package:findnear/src/widgets/shimmer/list_loading_shimmer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'logic.dart';
import 'state.dart';

class GroupMemberAdminRejected extends StatefulWidget {
  GroupEntity group;
  MemberAdminStatus listStatus;

  GroupMemberAdminRejected({
    Key key,
    this.listStatus,
    this.group,
  }) : super(key: key);

  @override
  _GroupMemberAdminRejectedState createState() =>
      _GroupMemberAdminRejectedState();
}

class _GroupMemberAdminRejectedState extends State<GroupMemberAdminRejected>
    with AutomaticKeepAliveClientMixin {
  RefreshController refreshController =
      RefreshController(initialRefresh: false);

  final TextEditingController _textEditingController = TextEditingController();

  final GroupMemberAdminRejectedLogic _logic =
      Get.find<GroupMemberAdminRejectedLogic>();
  final GroupMemberAdminRejectedState _state =
      Get.find<GroupMemberAdminRejectedLogic>().state;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _buildBody(),
    );
  }

  Widget _buildBody() {
    return Obx(() {
      if (_state.isLoading.value) {
        return ListLoadingShimmer();
      }

      return Column(
        children: [
          SizedBox(
            height: 15,
          ),
          Obx(() {
            final isHasData = _state.members.length != 0;
            return Expanded(
              child: Container(
                child: RefreshConfiguration.copyAncestor(
                  context: Get.context,
                  footerTriggerDistance: Get.height * 4,
                  child: SmartRefresher(
                    enablePullUp: _state.canLoadMore.value,
                    onRefresh: () async {
                      await _logic.onRefreshData();
                      refreshController?.refreshCompleted();
                    },
                    onLoading: () async {
                      await _logic.onLoadMoreData();
                      refreshController?.loadComplete();
                    },
                    controller: refreshController,
                    child: !isHasData
                        ? Center(
                            child: Text(
                              "Không có thành viên đã từ chối",
                            ),
                          )
                        : ListView.separated(
                            separatorBuilder:
                                (BuildContext context, int index) {
                              return SizedBox(
                                height: 10,
                              );
                            },
                            padding: EdgeInsets.symmetric(horizontal: 15),
                            itemCount: _state.members.length,
                            itemBuilder: (BuildContext context, int index) {
                              final member = _state.members.elementAt(index);
                              return _buildGroupItem(context, member, index);
                            },
                          ),
                  ),
                ),
              ),
            );
          }),
        ],
      );
    });
  }

  Container _buildGroupItem(
    BuildContext context,
    User user,
    int index,
  ) {
    String imageUrl = user.avatar ?? "";
    String userName = user.name ?? "";
    String rejectedBy = widget.group?.owner?.name ?? "";

    DateTime lastUpdatedDate = user.lastUpdatedDate;
    String rejectedAt = "";
    if (lastUpdatedDate != null) {
      rejectedAt = DateFormat('dd/MM/yyyy').format(lastUpdatedDate);
    }

    return Container(
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(5),
            child: Builder(
              builder: (context) {
                final double size = 50;
                if (imageUrl == null || imageUrl == "")
                  return AvatarPlaceholder(size: size);
                return CachedNetworkImage(
                  imageUrl: imageUrl,
                  fit: BoxFit.cover,
                  width: size,
                  height: size,
                  placeholder: (context, url) => CupertinoActivityIndicator(),
                  errorWidget: (context, url, error) =>
                      AvatarPlaceholder(size: size),
                );
              },
            ),
          ),
          SizedBox(
            width: 5,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        userName,
                        style: Theme.of(context).textTheme.headline6.merge(
                              TextStyle(
                                color: AppColors.textBlack,
                                fontSize: 14,
                                fontFamily: 'Quicksand',
                              ),
                            ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Text(
                      rejectedAt,
                      style: Theme.of(context).textTheme.bodyText1.merge(
                            TextStyle(
                              color: AppColors.gray2,
                              fontSize: 12,
                            ),
                          ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
                Text(
                  "Từ chối bởi ${rejectedBy}",
                  style: Theme.of(context).textTheme.bodyText1.merge(
                        TextStyle(
                          color: AppColors.gray2,
                          fontSize: 12,
                        ),
                      ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                Container(
                  child: ElevatedButton(
                    onPressed: () {
                      _logic.acceptAgainMember(context, user, index);
                    },
                    style: ElevatedButton.styleFrom(
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(20.0),
                      ),
                      primary: AppColors.red200,
                      side: BorderSide(color: AppColors.red),
                      elevation: 0,
                      minimumSize: Size(double.minPositive, 30),
                    ),
                    child: Text(
                      'Chấp nhận lại',
                      style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 14,
                        color: AppColors.red,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  /// Handle AutomaticKeepAliveClientMixin
  @override
  bool get wantKeepAlive => true;
}
