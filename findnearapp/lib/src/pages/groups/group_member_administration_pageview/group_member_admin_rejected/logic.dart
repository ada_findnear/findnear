import 'package:dio/dio.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/input/group_member_input.dart';
import 'package:findnear/src/models/input/invite_user_to_group_input.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/repository/findnear_group_repository.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'state.dart';

class GroupMemberAdminRejectedLogic extends GetxController {
  GroupMemberAdminRejectedState state;

  final FindNearGroupRepository _repository = Get.find();

  GroupMemberAdminRejectedLogic() : state = GroupMemberAdminRejectedState();

  int groupId;
  final int limit = 10;
  OverlayEntry loader;

  void initialize() async {
    if (groupId == null) {
      return;
    }
    state.currentOffset.value = 0;
    state.isLoading.value = true;
    await getGroupMembers(
      offset: state.currentOffset.value,
      limit: limit,
    );
    state.isLoading.value = false;
  }

  void onRefreshData() async {
    state.currentOffset.value = 0;
    state.isLoading.value = true;
    await getGroupMembers(
      offset: state.currentOffset.value,
      limit: limit,
      isRefresh: true,
    );
    state.isLoading.value = false;
  }

  void onLoadMoreData() async {
    print("onLoadMoreData");
    if (!state.canLoadMore.value) {
      return;
    }
    state.isLoadingMore.value = true;
    state.canLoadMore.value = false;
    await getGroupMembers(
      offset: state.currentOffset.value,
      limit: limit,
    );
    state.isLoadingMore.value = false;
  }

  Future<void> getGroupMembers({
    offset = 0,
    limit = 10,
    isRefresh = false,
  }) async {
    try {
      final input = GroupMemberInput.memberRejected(
        groupId: groupId,
      );

      final res = await _repository.getGroupMembers(
        input,
        offset: offset,
        limit: limit,
      );
      final members = res.data;

      if (isRefresh) {
        state.members.clear();
      }
      state.members.addAll(members);
      state.canLoadMore.value = members.length == limit;
      state.currentOffset.value = state.members.length;
    } catch (e) {
      print("getGroupMembers");
      print(e);
      if (e is DioError) {
        print(e.response);
      }
    }
  }

  void acceptAgainMember(BuildContext context, User user, int index) async {
    final userId = int.parse(user.id);
    if (groupId == null || userId == null) {
      return;
    }
    try {
      loader = Helper.overlayLoader(context);
      Overlay.of(context).insert(loader);

      final input = InviteUserToGroupInput.approvalToGroup(
        groupId: groupId,
        userId: userId,
      );

      final res = await _repository.inviteUserToGroup(input);
      state.members.removeAt(index);
      Helper.hideLoader(loader);

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Duyệt thành viên thành công"),
      ));
    } catch (e) {
      print("inviteMemberToGroup");
      Helper.showErrorMessage(e);
      Helper.hideLoader(loader);
    }
  }
}
