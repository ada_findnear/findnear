import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/pages/groups/group_member_administration_pageview/member_admin_type_tabbar/group_tabbar_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'group_member_admin_invited/logic.dart';
import 'group_member_admin_invited/view.dart';
import 'group_member_admin_new/logic.dart';
import 'group_member_admin_new/view.dart';
import 'group_member_admin_rejected/logic.dart';
import 'group_member_admin_rejected/view.dart';

class GroupMemberAdministrationPageView extends StatefulWidget {
  GroupEntity group;
  GroupMemberAdministrationPageView({Key key, this.group}) : super(key: key);

  @override
  _GroupViewState createState() => _GroupViewState();
}

class _GroupViewState extends State<GroupMemberAdministrationPageView> {
  PageController _pageController;
  int currentTab = 0;
  MemberAdminStatus currentGroupTabBar = MemberAdminStatus.new_members;

  final RefreshController refreshController =
      RefreshController(initialRefresh: false);

  final GroupMemberAdminNewLogic _newMembersLogic =
      Get.put(GroupMemberAdminNewLogic());
  final GroupMemberAdminRejectedLogic _rejectedMembersLogic =
      Get.put(GroupMemberAdminRejectedLogic());
  final GroupMemberAdminInvitedLogic _invitedMembersLogic =
      Get.put(GroupMemberAdminInvitedLogic());

  @override
  void initState() {
    super.initState();

    if (widget.group != null) {
      _newMembersLogic.groupId = widget.group.id;
      _rejectedMembersLogic.groupId = widget.group.id;
      _invitedMembersLogic.groupId = widget.group.id;
    }

    _newMembersLogic.initialize();
    _rejectedMembersLogic.initialize();
    _invitedMembersLogic.initialize();

    _pageController = PageController(initialPage: currentTab);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: _getAppBar(),
      body: Column(
        children: [
          MemberAdminStatusTabBar(
            itemClickCallback: (tabBarType) {
              print("Click");
              print(tabBarType);
              switch (tabBarType) {
                case MemberAdminStatus.new_members:
                  currentTab = 0;
                  break;
                case MemberAdminStatus.rejected_members:
                  currentTab = 1;
                  break;
                case MemberAdminStatus.invited_members:
                  currentTab = 2;
                  break;
              }
              _pageController.jumpToPage(currentTab);
              currentGroupTabBar = tabBarType;

              setState(() {});
            },
            currentGroupTabBar: currentGroupTabBar,
          ),
          Expanded(
            child: PageView(
              controller: _pageController,
              onPageChanged: (int page) {
                switch (page) {
                  case 0:
                    currentGroupTabBar = MemberAdminStatus.new_members;
                    break;
                  case 1:
                    currentGroupTabBar = MemberAdminStatus.rejected_members;
                    break;
                  case 2:
                    currentGroupTabBar = MemberAdminStatus.invited_members;
                    break;
                }
                setState(() {});
              },
              children: [
                GroupMemberAdminNew(
                  listStatus: MemberAdminStatus.new_members,
                ),
                GroupMemberAdminRejected(
                  listStatus: MemberAdminStatus.invited_members,
                  group: widget.group,
                ),
                GroupMemberAdminInvited(
                  listStatus: MemberAdminStatus.rejected_members,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _getAppBar() {
    return AppBar(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      leadingWidth: 52,
      centerTitle: false,
      title: Text(
        "Quản trị thành viên",
        style: Theme.of(context).textTheme.headline2.merge(
              TextStyle(
                color: AppColors.textBlack,
                fontSize: 16,
              ),
            ),
      ),
      actions: [],
    );
  }
}
