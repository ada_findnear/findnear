import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/elements/CircularAvatarWidget.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/groups/group_member_administration_pageview/group_member_admin_new/logic.dart';
import 'package:findnear/src/pages/groups/group_member_administration_pageview/group_member_admin_new/state.dart';
import 'package:findnear/src/pages/groups/group_member_administration_pageview/member_admin_type_tabbar/group_tabbar_view.dart';
import 'package:findnear/src/widgets/shimmer/list_loading_shimmer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class GroupMemberAdminNew extends StatefulWidget {
  MemberAdminStatus listStatus;

  GroupMemberAdminNew({Key key, this.listStatus}) : super(key: key);

  @override
  _GroupMemberAdminNewState createState() => _GroupMemberAdminNewState();
}

class _GroupMemberAdminNewState extends State<GroupMemberAdminNew>
    with AutomaticKeepAliveClientMixin {
  RefreshController refreshController =
      RefreshController(initialRefresh: false);

  final TextEditingController _textEditingController = TextEditingController();

  final GroupMemberAdminNewLogic _logic = Get.find<GroupMemberAdminNewLogic>();
  final GroupMemberAdminNewState _state =
      Get.find<GroupMemberAdminNewLogic>().state;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _buildBody(),
    );
  }

  Widget _buildBody() {
    return Obx(() {
      if (_state.isLoading.value) {
        return ListLoadingShimmer();
      }

      return Column(
        children: [
          SizedBox(
            height: 15,
          ),
          Obx(() {
            return Expanded(
              child: Container(
                child: RefreshConfiguration.copyAncestor(
                  context: Get.context,
                  footerTriggerDistance: Get.height * 4,
                  child: SmartRefresher(
                    enablePullUp: _state.canLoadMore.value,
                    onRefresh: () async {
                      await _logic.onRefreshData();
                      refreshController?.refreshCompleted();
                    },
                    onLoading: () async {
                      await _logic.onLoadMoreData();
                      refreshController?.loadComplete();
                    },
                    controller: refreshController,
                    child: _state.members.length == 0
                        ? Center(
                            child: Text(
                              "Không có thành viên mới",
                            ),
                          )
                        : ListView.separated(
                            separatorBuilder:
                                (BuildContext context, int index) {
                              return SizedBox(
                                height: 10,
                              );
                            },
                            padding: EdgeInsets.symmetric(horizontal: 15),
                            itemCount: _state.members.length,
                            itemBuilder: (BuildContext context, int index) {
                              final member = _state.members.elementAt(index);
                              return _buildGroupItem(context, member, index);
                            },
                          ),
                  ),
                ),
              ),
            );
          }),
        ],
      );
    });
  }

  Container _buildGroupItem(
    BuildContext context,
    User user,
    int index,
  ) {
    String imageUrl = user.avatar ?? "";
    String userName = user.name ?? "";

    DateTime lastUpdatedDate = user.lastUpdatedDate;
    String requestedAt = "";
    if (lastUpdatedDate != null) {
      requestedAt = DateFormat('dd/MM/yyyy').format(lastUpdatedDate);
    }

    return Container(
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(5),
            child: Builder(
              builder: (context) {
                final double size = 50;
                if (imageUrl == null || imageUrl == "")
                  return AvatarPlaceholder(size: size);
                return CachedNetworkImage(
                  imageUrl: imageUrl,
                  fit: BoxFit.cover,
                  width: size,
                  height: size,
                  placeholder: (context, url) => CupertinoActivityIndicator(),
                  errorWidget: (context, url, error) =>
                      AvatarPlaceholder(size: size),
                );
              },
            ),
          ),
          SizedBox(
            width: 5,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        userName,
                        style: Theme.of(context).textTheme.headline6.merge(
                              TextStyle(
                                color: AppColors.textBlack,
                                fontSize: 14,
                                fontFamily: 'Quicksand',
                              ),
                            ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Text(
                      requestedAt,
                      style: Theme.of(context).textTheme.bodyText1.merge(
                            TextStyle(
                              color: AppColors.gray2,
                              fontSize: 12,
                            ),
                          ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
                Row(
                  children: [
                    Container(
                      child: ElevatedButton(
                        onPressed: () {
                          _logic.rejectMember(context, user, index);
                        },
                        style: ElevatedButton.styleFrom(
                          shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(20.0),
                          ),
                          primary: AppColors.white,
                          side: BorderSide(color: AppColors.grayBorder),
                          elevation: 0,
                          minimumSize: Size(double.minPositive, 30),
                        ),
                        child: Text(
                          'Từ chối',
                          style: TextStyle(
                            fontFamily: 'Quicksand',
                            fontSize: 14,
                            color: AppColors.gray,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                        left: 10,
                      ),
                      child: ElevatedButton(
                        onPressed: () {
                          /// Go to create new group page view
                          _logic.acceptMember(context, user, index);
                        },
                        style: ElevatedButton.styleFrom(
                          shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(20.0),
                          ),
                          primary: AppColors.red200,
                          side: BorderSide(color: AppColors.red),
                          elevation: 0,
                          minimumSize: Size(double.minPositive, 30),
                        ),
                        child: Text(
                          'Chấp nhận',
                          style: TextStyle(
                            fontFamily: 'Quicksand',
                            fontSize: 14,
                            color: AppColors.red,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  /// Handle AutomaticKeepAliveClientMixin
  @override
  bool get wantKeepAlive => true;
}
