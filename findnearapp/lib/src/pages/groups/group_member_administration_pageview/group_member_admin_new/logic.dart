import 'package:dio/dio.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/input/group_member_input.dart';
import 'package:findnear/src/models/input/invite_user_to_group_input.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/groups/group_member_administration_pageview/group_member_admin_rejected/logic.dart';
import 'package:findnear/src/pages/groups/group_member_administration_pageview/group_member_admin_rejected/state.dart';
import 'package:findnear/src/repository/findnear_group_repository.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'state.dart';

class GroupMemberAdminNewLogic extends GetxController {
  GroupMemberAdminNewState state;

  final FindNearGroupRepository _repository = Get.find();

  GroupMemberAdminNewLogic() : state = GroupMemberAdminNewState();

  int groupId;
  final int limit = 10;
  OverlayEntry loader;

  void initialize() async {
    if (groupId == null) {
      return;
    }
    state.currentOffset.value = 0;
    state.isLoading.value = true;
    await getGroupMembers(
      offset: state.currentOffset.value,
      limit: limit,
    );
    state.isLoading.value = false;
  }

  void onRefreshData() async {
    state.currentOffset.value = 0;
    state.isLoading.value = true;
    await getGroupMembers(
      offset: state.currentOffset.value,
      limit: limit,
      isRefresh: true,
    );
    state.isLoading.value = false;
  }

  void onLoadMoreData() async {
    print("onLoadMoreData");
    if (!state.canLoadMore.value) {
      return;
    }
    state.isLoadingMore.value = true;
    state.canLoadMore.value = false;
    await getGroupMembers(
      offset: state.currentOffset.value,
      limit: limit,
    );
    state.isLoadingMore.value = false;
  }

  Future<void> getGroupMembers({
    offset = 0,
    limit = 10,
    isRefresh = false,
  }) async {
    try {
      final input = GroupMemberInput.newMember(
        groupId: groupId,
      );

      final res = await _repository.getGroupMembers(
        input,
        offset: offset,
        limit: limit,
      );
      final members = res.data;

      if (isRefresh) {
        state.members.clear();
      }
      state.members.addAll(members);
      state.canLoadMore.value = members.length == limit;
      state.currentOffset.value = state.members.length;
    } catch (e) {
      print("getGroupMembers");
      print(e);
      if (e is DioError) {
        print(e.response);
      }
    }
  }

  void acceptMember(BuildContext context, User user, int index) async {
    final userId = int.parse(user.id);
    if (groupId == null || userId == null) {
      return;
    }
    try {
      loader = Helper.overlayLoader(context);
      Overlay.of(context).insert(loader);

      final input = InviteUserToGroupInput.approvalToGroup(
        groupId: groupId,
        userId: userId,
      );
      final res = await _repository.inviteUserToGroup(input);
      state.members.removeAt(index);
      Helper.hideLoader(loader);

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Duyệt thành viên thành công"),
      ));
    } catch (e) {
      print("inviteMemberToGroup");
      Helper.showErrorMessage(e);
      Helper.hideLoader(loader);
    }
  }

  void rejectMember(BuildContext context, User user, int index) async {
    final userId = int.parse(user.id);
    if (groupId == null || userId == null) {
      return;
    }
    try {
      loader = Helper.overlayLoader(context);
      Overlay.of(context).insert(loader);

      final input = InviteUserToGroupInput.rejectToGroup(
        groupId: groupId,
        userId: userId,
      );
      final res = await _repository.inviteUserToGroup(input);
      state.members.removeAt(index);

      /// Move user to tab "Đã từ chối"
      GroupMemberAdminRejectedState _rejectedState =
          Get.find<GroupMemberAdminRejectedLogic>().state;
      _rejectedState.members.add(user);

      Helper.hideLoader(loader);

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Thành viên đã bị từ chối tham gia nhóm"),
      ));
    } catch (e) {
      print("inviteMemberToGroup");
      Helper.showErrorMessage(e);
      Helper.hideLoader(loader);
    }
  }
}
