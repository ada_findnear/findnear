import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/newsfeed/newsfeed_state.dart';
import 'package:get/get.dart';

class GroupMemberAdminInvitedState {
  RxInt currentOffset = 0.obs;
  Rx<bool> isLoading = false.obs;
  Rx<bool> isLoadingMore = false.obs;
  RxBool canLoadMore = false.obs;
  Rx<MessageStates> message = MessageStates.idle.obs;

  RxList<User> members = List<User>.empty().obs;
}
