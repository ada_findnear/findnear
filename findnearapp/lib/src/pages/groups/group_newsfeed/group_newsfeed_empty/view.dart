import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';

class GroupNewsfeedEmpty extends StatefulWidget {
  const GroupNewsfeedEmpty({Key key}) : super(key: key);

  @override
  _MyGroupEmptyViewState createState() => _MyGroupEmptyViewState();
}

class _MyGroupEmptyViewState extends State<GroupNewsfeedEmpty> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Center(
            child: AppImage(
              AppImages.newsFeedGroupEmpty,
              height: 185,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            S.of(context).group_newsfeed_empty_title,
            style: Theme.of(context).textTheme.headline6.merge(
                  TextStyle(
                    color: AppColors.textBlack,
                    fontSize: 14,
                    fontFamily: 'Quicksand',
                  ),
                ),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            S.of(context).group_newsfeed_empty_desc,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.bodyText1.merge(
                  TextStyle(
                    color: AppColors.textBlack,
                    fontSize: 12,
                    fontFamily: 'Quicksand',
                  ),
                ),
          ),
        ],
      ),
    );
  }
}
