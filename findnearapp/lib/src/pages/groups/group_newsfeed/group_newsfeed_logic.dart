import 'package:dio/dio.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/repository/findnear_group_repository.dart';
import 'package:findnear/src/repository/newsfeed_repository.dart';
import 'package:findnear/src/repository/post_repository.dart' as postRepo;
import 'package:findnear/src/services/notification_service.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'group_newsfeed_state.dart';

class GroupNewsFeedLogic extends GetxController {
  final NewsfeedRepository newsfeedRepository = Get.find();
  final FindNearGroupRepository _groupRepository = Get.find();

  final GroupNewsFeedState state;

  final int limit = 10;

  GroupNewsFeedLogic({GlobalKey<ScaffoldState> scaffoldKey})
      : state = GroupNewsFeedState();

  void initialize() async {
    state.getDataLoadingState.value = LoadingState.loading;
    await getGroupsJoined();
    await getGroupPosts(
      offset: 0,
      limit: limit,
    );
    state.getDataLoadingState.value = LoadingState.success;
  }

  void onRefreshData() async {
    state.currentOffset.value = 0;
    state.isLoading.value = true;
    await getGroupsJoined(isRefresh: true);
    await getGroupPosts(
      offset: 0,
      limit: limit,
      isRefresh: true,
    );
    state.isLoading.value = false;
  }

  void onLoadMoreData() async {
    if (!state.canLoadMore.value) {
      return;
    }
    state.isLoadingMore.value = true;
    state.canLoadMore.value = false;
    await getGroupPosts(offset: state.currentOffset.value, limit: limit);
    state.isLoadingMore.value = false;
  }

  Future<void> getGroupPosts({
    offset = 0,
    limit = 10,
    isRefresh = false,
  }) async {
    try {
      final res = await _groupRepository.getGroupPostsJoined(
        offset: offset,
        limit: limit,
      );
      final posts = res.data;

      if (isRefresh) {
        state.posts.clear();
      }
      state.posts.addAll(posts);
      state.canLoadMore.value = posts.length == limit;
      state.currentOffset.value = state.posts.length;
    } catch (e) {
      print("getGroupsJoined");
      print(e);
      if (e is DioError) {
        print(e.response);
      }
    }
  }

  void getGroupsJoined({
    offset = 0,
    limit = 10,
    isRefresh = false,
  }) async {
    try {
      final res = await _groupRepository.getGroupsJoined();
      final groups = res.data;
      if (isRefresh) {
        state.joinedGroups.clear();
      }
      state.joinedGroups.addAll(groups);
    } catch (e) {
      print("getGroupsJoined");
      print(e);
      if (e is DioError) {
        print(e.response);
      }
    }
  }

  /// Group post
  void toggleLikePost(int index) async {
    final Post post = state.posts.elementAt(index);
    final updatePost = post.copyWith(
      isLike: !post.isLike,
      likers_count:
          "${(int.tryParse(post.likers_count) ?? 0) + (post.isLike ? -1 : 1)}",
    );
    state.posts[index] = updatePost;
    try {
      final result = await newsfeedRepository.toggleLikePost(post.id);
      post.isLike = result.isLike;
    } catch (e) {
      logger.e(e);
      state.posts[index] = post;
    }
  }

  void hidePost(int index) async {
    final Post post = state.posts.elementAt(index);
    try {
      final result = await newsfeedRepository.hidePost(post.id);
      if (result.success) {
        state.posts.removeAt(index);
        state.message.value = MessageStates.hidePostSuccessfully;
      }
    } catch (e) {
      print(e);
    }
  }

  void savePost(int index) async {
    final Post post = state.posts.elementAt(index);
    try {
      final result = await newsfeedRepository.savePost(post.id);
      if (result.success) {
        state.message.value = MessageStates.savePostSuccessfully;
      }
    } catch (e) {
      print(e);
    }
  }

  void updatePost({int atIndex}) async {
    final postId = state.posts.elementAt(atIndex).id;
    try {
      final post = await newsfeedRepository.getPostDetails(postId);
      state.posts[atIndex] = post;
    } catch (e) {
      logger.e(e);
    }
  }

  void deletePost({int index}) async {
    final post = state.posts.elementAt(index);
    try {
      final result = await postRepo.removePost(post);
      if (result) {
        state.posts.removeAt(index);
        initialize();
      }
    } catch (e) {
      logger.e(e);
    }
  }
}
