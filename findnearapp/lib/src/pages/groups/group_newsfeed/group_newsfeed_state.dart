import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:get/get_rx/get_rx.dart';

class GroupNewsFeedState {
  RxInt currentOffset = 0.obs;
  Rx<bool> isLoading = false.obs;
  Rx<bool> isLoadingMore = false.obs;
  RxBool canLoadMore = false.obs;
  Rx<MessageStates> message = MessageStates.idle.obs;

  RxList<GroupEntity> joinedGroups = List<GroupEntity>.empty().obs;
  RxList<Post> posts = List<Post>.empty().obs;
  Rx<LoadingState> getDataLoadingState = LoadingState.idle.obs;
  Rx<LoadingState> getGroupPostsLoadingState = LoadingState.idle.obs;

  GroupNewsFeedState();
}
