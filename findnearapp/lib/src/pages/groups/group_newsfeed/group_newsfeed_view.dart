import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/GroupNewsFeedItemWidget.dart';
import 'package:findnear/src/models/bottom_sheet_option.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/pages/create_post/create_post.controller.dart';
import 'package:findnear/src/pages/create_post/create_post.screen.dart';
import 'package:findnear/src/pages/groups/group_suggest_for_you/logic.dart';
import 'package:findnear/src/pages/groups/group_suggest_for_you/state.dart';
import 'package:findnear/src/pages/groups/group_suggest_for_you/view.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'group_newsfeed_empty/view.dart';
import 'group_newsfeed_logic.dart';
import 'group_newsfeed_state.dart';
import 'list_group/list_group_view.dart';

class GroupNewsFeedPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  GroupNewsFeedPage({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _GroupViewState createState() => _GroupViewState();
}

class _GroupViewState extends State<GroupNewsFeedPage>
    with AutomaticKeepAliveClientMixin {
  GroupNewsFeedLogic logic;
  GroupNewsFeedState state;

  List<Post> _posts = [];

  final _refreshController = RefreshController(initialRefresh: false);

  GroupSuggestForYouLogic suggestGroupLogic =
      Get.put(GroupSuggestForYouLogic());
  GroupSuggestForYouState suggestGroupState =
      Get.find<GroupSuggestForYouLogic>().state;

  @override
  void initState() {
    super.initState();
    logic = Get.put(GroupNewsFeedLogic(
      scaffoldKey: widget.parentScaffoldKey,
    ));
    state = Get.find<GroupNewsFeedLogic>().state;
    logic.initialize();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      body: Obx(() {
        if (state.getDataLoadingState == LoadingState.loading) {
          return Container(
            alignment: Alignment.center,
            child: LoadingView(),
          );
        }
        final posts = state.posts;
        _posts = posts;
        final isHasData = _posts.length > 0;
        final joinedGroups = state.joinedGroups;

        return Stack(
          children: [
            Column(
              children: [
                /// Danh sách group đã join
                Visibility(
                  visible: joinedGroups.length > 0,
                  child: Container(
                    padding: EdgeInsets.only(bottom: 10),
                    margin: EdgeInsets.only(bottom: 2),
                    child: ListGroupView(),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 0.5,
                          color: AppColors.gray2,
                        )
                      ],
                    ),
                  ),
                ),

                /// Newsfeed post group
                Expanded(
                  child: _buildGroupNewsfeedList(),
                ),
              ],
            ),
            Visibility(
              visible: !isHasData,
              child: Container(
                child: DraggableScrollableSheet(
                  initialChildSize: 0.4,
                  minChildSize: 0.4,
                  maxChildSize: 0.9,
                  builder: (_, controller) => Container(
                    padding: EdgeInsets.only(
                      top: 10,
                    ),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: GroupSuggestForYou(
                      scrollController: controller,
                    ),
                  ),
                ),
              ),
            ),
          ],
        );
      }),
    );
  }

  Widget _buildGroupNewsfeedList() {
    final posts = _posts;
    final isHasData = posts.length > 0;

    return RefreshConfiguration.copyAncestor(
      context: Get.context,
      footerTriggerDistance: Get.height * 4,
      child: SmartRefresher(
        enablePullUp: state.canLoadMore.value,
        onRefresh: () async {
          print("onRefresh");
          await logic.onRefreshData();
          await suggestGroupLogic.onRefreshData();
          _refreshController?.refreshCompleted();
        },
        onLoading: () async {
          print("on load more");
          await logic.onLoadMoreData();
          _refreshController?.loadComplete();
        },
        controller: _refreshController,
        child: isHasData
            ? ListView.separated(
                padding: EdgeInsets.only(top: 10),
                itemCount: posts.length ?? 0,
                separatorBuilder: (context, index) => const Divider(
                  height: 8,
                  thickness: 8,
                  color: AppColors.white,
                ),
                itemBuilder: (context, index) {
                  return _buildGroupPostItem(index);
                },
              )
            : GroupNewsfeedEmpty(),
      ),
    );
  }

  Widget _buildGroupPostItem(int index) {
    Post post = _posts.elementAt(index);

    return Container(
      padding: const EdgeInsets.only(top: 3, bottom: 3),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 13),
        margin: const EdgeInsets.symmetric(horizontal: 15),
        decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border.all(
            color: Color(0xFFEDEDED),
          ),
          borderRadius: BorderRadius.circular(20),
        ),
        child: GroupNewsFeedItemWidget(
          post: post,
          isMyProfile: post?.creator?.id == currentUser.value?.id,
          onMyPostMenuPressed: () {
            _onMyPostMenuPressed(index);
          },
          onSharePost: () {
            _onClickSharePost(post);
          },
          onOtherUserPostMenuPressed: () async {
            onOtherUserPostMenuPressed(index);
          },
          toggleLikePost: (id) {
            if (!isLoggedIn) {
              NavigatorUtils.toLogin();
              return;
            }
            logic.toggleLikePost(index);
          },
          didUpdateCallback: () {
            logic.updatePost(atIndex: index);
          },
        ),
      ),
    );
  }

  void _onMyPostMenuPressed(int index) async {
    Post post = _posts.elementAt(index);

    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    }
    final indexTapped = await DialogHelper.showBottomSheetOptions(
      context: context,
      options: [
        BottomSheetOption(
          iconPath: AppImages.icWrite,
          text: S.of(context).edit_post,
        ),
        BottomSheetOption(
          iconPath: AppImages.icTrash,
          text: S.of(context).delete_post,
        ),
        // temporarily disable
        // BottomSheetOption(
        //   iconPath: AppImages.icBookmarkOutlined,
        //   text: S.of(context).save_post,
        // ),
        BottomSheetOption(
          icon: Icons.share,
          text: S.of(context).shareThisPost,
        ),
      ],
    );
    switch (indexTapped) {
      case 0:
        _onClickEditPost(post);
        break;
      case 1:
        final isDeleting = await DialogHelper.showConfirmPopup(
          title: S.current.deletePost,
          description: S.current.confirmDeleteDescription,
          okBtnText: S.current.deletePostShort,
        );

        if (isDeleting ?? false) {
          logic.deletePost(index: index);
        }

        break;
      case 2:
        // share other app
        onClickSharePostOtherApp();
        break;
    }
  }

  void onOtherUserPostMenuPressed(int index) async {
    Post post = _posts.elementAt(index);

    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    }
    final indexTapped = await DialogHelper.showBottomSheetOptions(
      context: context,
      options: [
        // temporarily disable
        // BottomSheetOption(
        //   iconPath: AppImages.icBookmarkOutlined,
        //   text: S.of(context).save_post,
        // ),
        BottomSheetOption(
          iconPath: AppImages.icHide,
          text: S.of(context).hide_post,
        ),
        BottomSheetOption(
          icon: Icons.share,
          text: S.of(context).shareThisPost,
        ),
        BottomSheetOption(
          icon: Icons.report,
          text: S.of(context).reportThisPost,
          isReported: post.isReported,
        ),
      ],
    );
    switch (indexTapped) {
      // case 0:
      //   logic.savePost(postIndex);
      //   break;
      case 0:
        logic.hidePost(index);
        break;
      case 1:
        // _onClickSharePost(post);
        onClickSharePostOtherApp();
        break;
      case 2:
        state.posts[index].isReported = true;
        _onClickReport(post);
        break;
    }
  }

  void _onClickSharePost(Post post) async {
    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    }
    final originalPost = post.isSharedPost ? post.originalPost : post;
    Get.put(CreatePostController(originalPost: originalPost));
    var res = await Get.to(
      () => CreatePostScreen(
        user: currentUser.value,
        originalPost: originalPost,
      ),
    );
    Get.delete<CreatePostController>();
    if (res != null) logic.initialize();
  }

  void _onClickEditPost(Post post) async {
    Get.put(CreatePostController(post: post));
    final result = await Get.to(() => CreatePostScreen(
          user: currentUser.value,
          originalPost: post.originalPost,
        ));
    Get.delete<CreatePostController>();
    if (result != null) logic.initialize();
  }

  void onClickSharePostOtherApp() async {
    await FlutterShare.share(
        title: 'Example share',
        text: 'Example share text',
        linkUrl: 'https://flutter.dev/',
        chooserTitle: 'Example Chooser Title');
  }

  void _onClickReport(Post post) {
    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    }
    DialogHelper.showReportPopup(post);
  }

  /// Handle AutomaticKeepAliveClientMixin
  @override
  bool get wantKeepAlive => true;
}
