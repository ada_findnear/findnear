import 'package:findnear/src/commons/app_colors.dart';
import 'package:flutter/material.dart';

enum GroupTabBarType { all_group, my_group, discover }

class GroupTabbarView extends StatelessWidget {
  Function itemClickCallback;
  GroupTabBarType currentGroupTabBar;

  GroupTabbarView({this.itemClickCallback, this.currentGroupTabBar});

  final _groupTabBarDataSource = GroupTabBarType.values;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: double.infinity,
      child: ListView.separated(
        separatorBuilder: (BuildContext context, int index) {
          return SizedBox(
            width: 10,
          );
        },
        padding: EdgeInsets.symmetric(horizontal: 10),
        itemCount: _groupTabBarDataSource.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int index) {
          final tabbar = _groupTabBarDataSource.elementAt(index);
          return _buildGroupItem(context, tabbar);
        },
      ),
    );
  }

  String getGroupTabBarTitle(GroupTabBarType tabBarType) {
    switch (tabBarType) {
      case GroupTabBarType.all_group:
        return "Nhóm tham gia";
      case GroupTabBarType.my_group:
        return "Nhóm của tôi";
      case GroupTabBarType.discover:
        return "Khám phá";
      default:
        return "";
    }
  }

  Widget _buildGroupItem(BuildContext context, GroupTabBarType tabBarType) {
    final title = getGroupTabBarTitle(tabBarType);
    var isSelected = currentGroupTabBar == tabBarType;
    var titleTextColor = isSelected ? AppColors.white : AppColors.gray;
    var groupBgColor = isSelected ? AppColors.red : Color(0xFFF2F2F2);
    return GestureDetector(
      onTap: () {
        itemClickCallback.call(tabBarType);
      },
      child: Container(
        child: Align(
          alignment: Alignment.center,
          child: Container(
            height: 30,
            decoration: new BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: groupBgColor,
            ),
            child: Align(
              alignment: Alignment.center,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 15),
                child: Text(
                  title,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.overline.merge(
                        TextStyle(
                          color: titleTextColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
