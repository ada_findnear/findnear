import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/pages/groups/group_detail_page/view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

import '../group_newsfeed_logic.dart';
import '../group_newsfeed_state.dart';

class ListGroupView extends StatefulWidget {
  const ListGroupView({Key key}) : super(key: key);

  @override
  _ListGroupViewState createState() => _ListGroupViewState();
}

class _ListGroupViewState extends State<ListGroupView> {
  GroupNewsFeedLogic logic;
  GroupNewsFeedState state;

  @override
  void initState() {
    super.initState();
    logic = Get.find<GroupNewsFeedLogic>();
    state = logic.state;
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        final joinedGroups = state.joinedGroups;
        return Container(
          height: 70,
          width: double.infinity,
          child: ListView.separated(
            separatorBuilder: (BuildContext context, int index) {
              return SizedBox(
                width: 10,
              );
            },
            padding: EdgeInsets.symmetric(horizontal: 15),
            itemCount: joinedGroups.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (BuildContext context, int index) {
              final group = joinedGroups.elementAt(index);
              return _buildGroupItem(group);
            },
          ),
        );
      },
    );
  }

  Widget _buildGroupItem(GroupEntity group) {
    String groupThumbnail;
    final media = group.media;
    if (media != null && media.length > 0) {
      groupThumbnail = media.first.thumb;
    }

    return GestureDetector(
      onTap: () {
        Get.to(
          GroupDetailPage(
            groupId: group.id,
          ),
        );
      },
      child: Container(
        height: 70,
        width: 70,
        decoration: new BoxDecoration(
          color: const Color.fromRGBO(0, 0, 0, 1),
          image: new DecorationImage(
            fit: BoxFit.cover,
            colorFilter: new ColorFilter.mode(
                Colors.black.withOpacity(0.4), BlendMode.dstATop),
            image: groupThumbnail != null
                ? new NetworkImage(
                    groupThumbnail,
                  )
                : AssetImage(AppImages.icLogo),
          ),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Align(
          alignment: Alignment.bottomLeft,
          child: Padding(
            padding: EdgeInsets.all(2),
            child: Text(
              group.title,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context).textTheme.overline.merge(
                    TextStyle(
                      color: AppColors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
            ),
          ),
        ),
      ),
    );
  }
}
