import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:get/get_rx/get_rx.dart';

class GroupDiscoverState {
  RxInt currentOffset = 0.obs;
  Rx<bool> isLoading = false.obs;
  Rx<bool> isLoadingMore = false.obs;
  RxBool canLoadMore = false.obs;
  Rx<MessageStates> message = MessageStates.idle.obs;
  Rx<LoadingState> requestJoinGroupState = LoadingState.idle.obs;

  Rx<List<GroupEntity>> discoverGroups = Rx<List<GroupEntity>>([]);
  Rx<LoadingState> getDataLoadingState = LoadingState.idle.obs;

  GroupDiscoverState();
}
