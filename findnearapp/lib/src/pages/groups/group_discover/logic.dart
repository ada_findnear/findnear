import 'package:dio/dio.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/models/input/invite_user_to_group_input.dart';
import 'package:findnear/src/repository/findnear_group_repository.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'state.dart';

class GroupDiscoverLogic extends GetxController {
  final FindNearGroupRepository _groupRepository = Get.find();

  final GroupDiscoverState state;
  final int limit = 10;
  OverlayEntry loader;

  GroupDiscoverLogic({GlobalKey<ScaffoldState> scaffoldKey})
      : state = GroupDiscoverState();

  void initialize() async {
    state.getDataLoadingState.value = LoadingState.loading;
    await getDiscoverGroup();
    state.getDataLoadingState.value = LoadingState.success;
  }

  void onRefreshData() async {
    state.currentOffset.value = 0;
    state.isLoading.value = true;
    await getDiscoverGroup(
      offset: 0,
      limit: limit,
      isRefresh: true,
    );
    state.isLoading.value = false;
  }

  void onLoadMoreData() async {
    if (!state.canLoadMore.value) {
      return;
    }
    state.isLoadingMore.value = true;
    state.canLoadMore.value = false;
    await getDiscoverGroup(offset: state.currentOffset.value, limit: limit);
    state.isLoadingMore.value = false;
  }

  void getDiscoverGroup({
    offset = 0,
    limit = 10,
    isRefresh = false,
  }) async {
    try {
      final res = await _groupRepository.getDiscoverGroup(
        offset: offset,
        limit: limit,
      );
      final groups = res.data;
      if (isRefresh) {
        state.discoverGroups.value.clear();
      }
      state.discoverGroups.value.addAll(groups);

      state.canLoadMore.value = groups.length == limit;
      state.currentOffset.value = state.discoverGroups.value.length;
    } catch (e) {
      print("getDiscoverGroup");
      print(e);
      if (e is DioError) {
        print(e.response);
      }
    }
  }

  void requestJoinGroup(
      BuildContext context, GroupEntity group, int index) async {
    state.requestJoinGroupState.value = LoadingState.loading;
    try {
      loader = Helper.overlayLoader(context);
      Overlay.of(context).insert(loader);

      final input = InviteUserToGroupInput.joinGroup(group.id);
      await _groupRepository.inviteUserToGroup(input);

      final _group = state.discoverGroups.value.elementAt(index);
      final updateGroup = _group.copyWith(joined: true);
      state.discoverGroups.value[index] = updateGroup;

      state.requestJoinGroupState.value = LoadingState.success;

      Helper.hideLoader(loader);

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Tham gia nhóm thành công"),
      ));
    } catch (e) {
      state.requestJoinGroupState.value = LoadingState.failure;
      Helper.hideLoader(loader);
      Helper.showErrorMessage(e);
    }
  }
}
