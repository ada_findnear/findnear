import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/CircularAvatarWidget.dart';
import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/pages/groups/group_discover/group_discover_empty/view.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'logic.dart';
import 'state.dart';

class GroupDiscoverView extends StatefulWidget {
  const GroupDiscoverView({Key key}) : super(key: key);

  @override
  _GroupDiscoverViewState createState() => _GroupDiscoverViewState();
}

class _GroupDiscoverViewState extends State<GroupDiscoverView>
    with AutomaticKeepAliveClientMixin {
  final _refreshController = RefreshController(initialRefresh: false);

  GroupDiscoverLogic logic;
  GroupDiscoverState state;

  @override
  void initState() {
    super.initState();
    logic = Get.put(GroupDiscoverLogic());
    state = Get.find<GroupDiscoverLogic>().state;
    logic.initialize();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      if (state.getDataLoadingState == LoadingState.loading) {
        return Container(
          alignment: Alignment.center,
          child: LoadingView(),
        );
      }

      final discoverGroups = state.discoverGroups.value;
      final isHasData = discoverGroups.length > 0;

      return Container(
        child: RefreshConfiguration.copyAncestor(
          context: Get.context,
          footerTriggerDistance: Get.height * 4,
          child: SmartRefresher(
            enablePullUp: state.canLoadMore.value,
            onRefresh: () async {
              await logic.onRefreshData();
              _refreshController?.refreshCompleted();
            },
            onLoading: () async {
              await logic.onLoadMoreData();
              _refreshController?.loadComplete();
            },
            controller: _refreshController,
            child: !isHasData
                ? GroupDiscoverEmptyView()
                : ListView.separated(
                    separatorBuilder: (BuildContext context, int index) {
                      return SizedBox(
                        height: 10,
                      );
                    },
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    itemCount: state.discoverGroups.value.length,
                    itemBuilder: (BuildContext context, int index) {
                      final group = state.discoverGroups.value.elementAt(index);
                      return _buildGroupItem(group, index);
                    },
                  ),
          ),
        ),
      );
    });
  }

  Container _buildGroupItem(GroupEntity group, int index) {
    String imageUrl = group?.groupThumbnail ?? "";
    String groupName = group?.title;
    String numberMember = "${group?.members_count ?? 0} thành viên";
    String numberPost = "${group?.posts_count ?? 0} bài viết";

    bool isJoined = group.joined ?? false;
    String joinGroupBtnTitle = "";
    if (isJoined) {
      joinGroupBtnTitle = "Chờ phê duyệt";
    } else {
      joinGroupBtnTitle = "Tham gia";
    }

    return Container(
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(5),
            child: Builder(
              builder: (context) {
                final double size = 50;
                if (imageUrl == null || imageUrl == "")
                  return AppImage(
                    AppImages.icLogo,
                    fit: BoxFit.cover,
                    width: size,
                    height: size,
                  );
                return CachedNetworkImage(
                  imageUrl: imageUrl,
                  fit: BoxFit.cover,
                  width: size,
                  height: size,
                  placeholder: (context, url) => CupertinoActivityIndicator(),
                  errorWidget: (context, url, error) =>
                      AvatarPlaceholder(size: size),
                );
              },
            ),
          ),
          SizedBox(
            width: 5,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  groupName,
                  style: Theme.of(context).textTheme.headline6.merge(
                        TextStyle(
                          color: AppColors.textBlack,
                          fontSize: 14,
                          fontFamily: 'Quicksand',
                        ),
                      ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  children: [
                    Row(
                      children: [
                        Icon(
                          Icons.people_alt_rounded,
                          size: 10,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          numberMember,
                          style: Theme.of(context).textTheme.bodyText1.merge(
                                TextStyle(
                                  color: AppColors.gray2,
                                  fontSize: 10,
                                ),
                              ),
                        )
                      ],
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.sticky_note_2,
                          size: 10,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          numberPost,
                          style: Theme.of(context).textTheme.bodyText1.merge(
                                TextStyle(
                                  color: AppColors.gray2,
                                  fontSize: 10,
                                ),
                              ),
                        )
                      ],
                    )
                  ],
                )
              ],
            ),
          ),
          Container(
            height: 26,
            child: OutlinedButton(
              onPressed: () {
                if (!isJoined) {
                  logic.requestJoinGroup(context, group, index);
                }
              },
              style: OutlinedButton.styleFrom(
                side: BorderSide(
                    color: isJoined ? AppColors.green : AppColors.red,
                    width: 1),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(13),
                ),
              ),
              child: Text(
                joinGroupBtnTitle,
                style: Theme.of(context).textTheme.caption.merge(
                      TextStyle(
                        color: isJoined ? AppColors.green : AppColors.red,
                        fontSize: 12,
                        fontFamily: 'Quicksand',
                      ),
                    ),
              ),
            ),
          )
        ],
      ),
    );
  }

  /// Handle AutomaticKeepAliveClientMixin
  @override
  bool get wantKeepAlive => true;
}
