import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';

class GroupDiscoverEmptyView extends StatefulWidget {
  const GroupDiscoverEmptyView({Key key}) : super(key: key);

  @override
  _GroupDiscoverEmptyViewState createState() => _GroupDiscoverEmptyViewState();
}

class _GroupDiscoverEmptyViewState extends State<GroupDiscoverEmptyView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Center(
            child: AppImage(
              AppImages.myGroupEmpty,
              height: 175,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            "Không có nhóm nào",
            style: Theme.of(context).textTheme.headline6.merge(
                  TextStyle(
                    color: AppColors.textBlack,
                    fontSize: 14,
                    fontFamily: 'Quicksand',
                  ),
                ),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
        ],
      ),
    );
  }
}
