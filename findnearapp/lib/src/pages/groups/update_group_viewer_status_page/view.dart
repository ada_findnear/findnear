import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/utils/enum_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'logic.dart';
import 'state.dart';

class UpdateGroupViewerStatus extends StatefulWidget {
  GroupEntity group;

  UpdateGroupViewerStatus({
    Key key,
    this.group,
  }) : super(key: key);

  @override
  _UpdateGroupViewerStatusState createState() =>
      _UpdateGroupViewerStatusState();
}

class _UpdateGroupViewerStatusState extends State<UpdateGroupViewerStatus> {
  final UpdateGroupViewerStatusLogic logic =
      Get.put(UpdateGroupViewerStatusLogic());
  final UpdateGroupViewerStatusState state =
      Get.find<UpdateGroupViewerStatusLogic>().state;

  @override
  void initState() {
    super.initState();
    if (widget.group != null) {
      logic.group = widget.group;
      state.currentStatus.value = widget.group.viewerEnum;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _getAppBar(),
      body: SafeArea(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.symmetric(
                  vertical: 10,
                  horizontal: 10,
                ),
                child: Text(
                  "Thay đổi quyền riêng tư",
                  style: Theme.of(context).textTheme.headline6.merge(
                        TextStyle(
                          color: AppColors.textBlack,
                          fontSize: 16,
                          fontFamily: 'Quicksand',
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: GroupViewerStatus.values
                      .map((e) => _buildPublicStatusWidget(e))
                      .toList(),
                ),
              ),
              Expanded(
                child: Container(),
              ),
              Container(
                child: Center(
                  child: ElevatedButton(
                    onPressed: () {
                      logic.updateGroupViewer(context);
                    },
                    style: ElevatedButton.styleFrom(
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(20.0),
                      ),
                      primary: AppColors.red200,
                      side: BorderSide(color: AppColors.red),
                      elevation: 0,
                    ),
                    child: Text(
                      'LƯU CẬP NHẬT',
                      style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 14,
                        color: AppColors.red,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildPublicStatusWidget(GroupViewerStatus status) {
    String label = status.display;
    String desc = status.desc;

    return Obx(() {
      return GestureDetector(
        onTap: () {
          print("Tap");
          state.currentStatus.value = status;
        },
        child: Container(
          margin: EdgeInsets.symmetric(
            horizontal: 10,
            vertical: 5,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Radio(
                  value: status,
                  toggleable: state.currentStatus.value == status,
                  activeColor: AppColors.red,
                  groupValue: state.currentStatus.value,
                  onChanged: (v) {
                    print("v ${v}");
                    state.currentStatus.value = v;
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(
                  horizontal: 10,
                ),
                padding: EdgeInsets.only(
                  top: 5,
                ),
                child: Icon(
                  status.icon,
                  color: AppColors.gray,
                  size: 18,
                ),
              ),
              Expanded(
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        label,
                        style: Theme.of(context).textTheme.headline6.merge(
                              TextStyle(
                                color: AppColors.textBlack,
                                fontSize: 16,
                                fontFamily: 'Quicksand',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                      ),
                      Text(
                        desc,
                        style: Theme.of(context).textTheme.headline6.merge(
                              TextStyle(
                                color: AppColors.gray,
                                fontSize: 14,
                                fontFamily: 'Quicksand',
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      );
    });
  }

  Widget _getAppBar() {
    return AppBar(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      leadingWidth: 52,
      centerTitle: false,
      title: Text(
        "Quyền riêng tư",
        style: Theme.of(context).textTheme.headline2.merge(
              TextStyle(
                color: AppColors.textBlack,
                fontSize: 16,
              ),
            ),
      ),
    );
  }
}
