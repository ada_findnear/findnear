import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/models/input/group_input.dart';
import 'package:findnear/src/repository/findnear_group_repository.dart';
import 'package:findnear/src/utils/enum_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'state.dart';

class UpdateGroupViewerStatusLogic extends GetxController {
  final UpdateGroupViewerStatusState state;

  UpdateGroupViewerStatusLogic() : state = UpdateGroupViewerStatusState();

  final FindNearGroupRepository _repository = Get.find();

  GroupEntity group;
  OverlayEntry loader;

  void updateGroupViewer(BuildContext context) async {
    if (group.id == null) {
      return;
    }
    try {
      loader = Helper.overlayLoader(context);
      Overlay.of(context).insert(loader);

      final input = GroupInput(
        groupId: group.id,
        viewer: state.currentStatus.value.toParam,
      );
      await _repository.updateGroup(input);

      Helper.hideLoader(loader);

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Cập nhật quyền riêng tư thành công"),
      ));
    } catch (e) {
      Helper.hideLoader(loader);
      Helper.showErrorMessage(e);
    }
  }
}
