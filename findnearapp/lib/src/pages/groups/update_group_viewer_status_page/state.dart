import 'package:findnear/src/utils/enum_utils.dart';
import 'package:get/get.dart';

class UpdateGroupViewerStatusState {
  Rx<GroupViewerStatus> currentStatus = GroupViewerStatus.public.obs;
}
