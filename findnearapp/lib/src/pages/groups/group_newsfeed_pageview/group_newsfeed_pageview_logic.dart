import 'package:findnear/src/repository/newsfeed_repository.dart';
import 'package:findnear/src/services/notification_service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'group_newsfeed_pageview_state.dart';

class GroupNewsFeedPageViewLogic extends GetxController {
  final NotificationService notificationService =
      Get.find<NotificationService>();

  final NewsfeedRepository newsfeedRepository = Get.find();

  final GroupNewsFeedPageViewState state;
  final RefreshController refreshController;

  GroupNewsFeedPageViewLogic(
      {GlobalKey<ScaffoldState> scaffoldKey, @required this.refreshController})
      : state = GroupNewsFeedPageViewState();

  void initialize() {}
}
