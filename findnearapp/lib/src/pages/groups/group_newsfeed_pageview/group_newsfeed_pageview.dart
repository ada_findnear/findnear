import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/SearchBarNewsfeedWidget.dart';
import 'package:findnear/src/pages/create_post/create_post.controller.dart';
import 'package:findnear/src/pages/create_post/create_post.screen.dart';
import 'package:findnear/src/pages/groups/group_discover/view.dart';
import 'package:findnear/src/pages/groups/group_newsfeed/group_newsfeed_view.dart';
import 'package:findnear/src/pages/groups/group_newsfeed/group_tabbar/group_tabbar_view.dart';
import 'package:findnear/src/pages/groups/my_group_newsfeed_page/view.dart';
import 'package:findnear/src/pages/groups/share/join_group_logic.dart';
import 'package:findnear/src/pages/groups/share/join_group_state.dart';
import 'package:findnear/src/pages/notification/notification_list/notification_list_view.dart';
import 'package:findnear/src/pages/tab_map/widgets/notification_button.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:images_picker/images_picker.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'group_newsfeed_pageview_logic.dart';
import 'group_newsfeed_pageview_state.dart';

class GroupNewsFeedPageView extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;
  final RefreshController refreshController =
      RefreshController(initialRefresh: false);

  GroupNewsFeedPageView({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _GroupViewState createState() => _GroupViewState();
}

class _GroupViewState extends State<GroupNewsFeedPageView> {
  GroupNewsFeedPageViewLogic logic;
  GroupNewsFeedPageViewState state;

  PageController _pageController;
  int currentTab = 0;
  GroupTabBarType currentGroupTabBar = GroupTabBarType.all_group;

  JoinGroupLogic _joinGroupLogic = Get.put(JoinGroupLogic());
  JoinGroupState _joinGroupState = Get.find<JoinGroupLogic>().state;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: currentTab);

    logic = Get.put(GroupNewsFeedPageViewLogic(
      scaffoldKey: widget.parentScaffoldKey,
      refreshController: widget.refreshController,
    ));
    state = Get.find<GroupNewsFeedPageViewLogic>().state;
    logic.initialize();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _joinGroupState.requestJoinGroupState.listen((p0) {
        print("p0 ${p0}");
        if (_joinGroupState.requestJoinGroupState == LoadingState.success) {
          print("_showRequestJoinGroupSuccess");
          _showRequestJoinGroupSuccess();
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: _getAppBar(),
      body: Column(
        children: [
          GroupTabbarView(
            itemClickCallback: (tabBarType) {
              print("Click");
              print(tabBarType);
              switch (tabBarType) {
                case GroupTabBarType.all_group:
                  currentTab = 0;
                  break;
                case GroupTabBarType.my_group:
                  currentTab = 1;
                  break;
                case GroupTabBarType.discover:
                  currentTab = 2;
                  break;
              }
              _pageController.jumpToPage(currentTab);
              currentGroupTabBar = tabBarType;

              setState(() {});
            },
            currentGroupTabBar: currentGroupTabBar,
          ),
          Expanded(
            child: PageView(
              controller: _pageController,
              onPageChanged: (int page) {
                switch (page) {
                  case 0:
                    currentGroupTabBar = GroupTabBarType.all_group;
                    break;
                  case 1:
                    currentGroupTabBar = GroupTabBarType.my_group;
                    break;
                  case 2:
                    currentGroupTabBar = GroupTabBarType.discover;
                    break;
                }
                setState(() {});
              },
              children: [
                GroupNewsFeedPage(),
                MyGroupNewsFeedPage(),
                GroupDiscoverView(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _showRequestJoinGroupSuccess() {
    DialogHelper.showAlertDialog(
      context: Get.context,
      title: S.of(context).group_join_group_alert_success_title,
      description: S.of(context).group_join_group_alert_success_content,
      okText: S.of(Get.context).ok,
      okFunction: () {},
    );
  }

  Widget _getAppBar() {
    if (logic == null) return Container();
    return AppBar(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      centerTitle: true,
      leadingWidth: 52,
      titleSpacing: 0,
      title: SearchBarNewsfeedWidget(),
      actions: [
        GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            Get.to(() => GroupNewsFeedPageView());
          },
          child: Container(
            padding: const EdgeInsets.all(12),
            child: AppImage(
              AppImages.ic_group_appbar,
              fit: BoxFit.contain,
              width: 20,
              height: 20,
              color: AppColors.red,
            ),
          ),
        ),
        GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            _onClickCreatePost();
          },
          child: Container(
            padding: const EdgeInsets.all(12),
            child: AppImage(AppImages.icWrite, fit: BoxFit.contain),
          ),
        ),
        Obx(() {
          return NotificationButton(
            onPressed: () {
              if (isLoggedIn) {
                Get.to(() => NotificationListPage());
              } else {
                NavigatorUtils.toLogin();
              }
            },
            badgeCounter:
                logic.notificationService.unreadNotificationCounter.value,
          );
        }),
      ],
    );
  }

  void _onClickCreatePost([List<Media> list]) async {
    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    }
    Get.put(CreatePostController());
    var res = await goTo(
        screen: CreatePostScreen(
      user: currentUser.value,
      medias: list,
    ));
    Get.delete<CreatePostController>();
    if (res != null) logic.initialize();
  }
}
