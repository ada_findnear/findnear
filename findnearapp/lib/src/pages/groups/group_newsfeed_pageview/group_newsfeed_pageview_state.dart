import 'package:findnear/src/models/entities/live_token_entity.dart';
import 'package:findnear/src/models/entities/livestream_info_entity.dart';
import 'package:findnear/src/models/post.dart';
import 'package:get/get_rx/get_rx.dart';

class GroupNewsFeedPageViewState {
  RxList<Post> posts = List<Post>.empty().obs;
  RxInt currentOffset = 0.obs;
  Rx<bool> isLoading = false.obs;
  Rx<bool> isLoadingMore = false.obs;
  RxBool canLoadMore = true.obs;
  Rx<MessageStates> message = MessageStates.idle.obs;
  Rxn<LiveTokenEntity> liveToken = Rxn<LiveTokenEntity>();
  Rxn<LiveTokenEntity> audienceToken = Rxn<LiveTokenEntity>();
  final listLive = Rx<List<LiveStreamInfoEntity>>([]);

  GroupNewsFeedPageViewState();
}

enum MessageStates {
  idle,
  savePostSuccessfully,
  unsavePostSuccessfully,
  hidePostSuccessfully,
  unhidePostSuccessfully,
}
