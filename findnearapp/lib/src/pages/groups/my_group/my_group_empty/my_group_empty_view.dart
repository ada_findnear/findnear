import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/pages/groups/create_new_group/create_new_group_pageview/create_new_group_pageview_view.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MyGroupEmptyView extends StatefulWidget {
  const MyGroupEmptyView({Key key}) : super(key: key);

  @override
  _MyGroupEmptyViewState createState() => _MyGroupEmptyViewState();
}

class _MyGroupEmptyViewState extends State<MyGroupEmptyView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Center(
            child: AppImage(
              AppImages.myGroupEmpty,
              height: 175,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            "Bạn chưa có nhóm của mình",
            style: Theme.of(context).textTheme.headline6.merge(
                  TextStyle(
                    color: AppColors.textBlack,
                    fontSize: 14,
                    fontFamily: 'Quicksand',
                  ),
                ),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            "Tạo nhóm ngay để chia sẻ với bạn bè nhiều hơn",
            style: Theme.of(context).textTheme.bodyText1.merge(
                  TextStyle(
                    color: AppColors.textBlack,
                    fontSize: 12,
                    fontFamily: 'Quicksand',
                  ),
                ),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          SizedBox(
            height: 10,
          ),
          ElevatedButton(
            onPressed: () {
              /// Go to create new group page view
              Get.to(() => CreateNewGroupPageViewView());
            },
            style: ElevatedButton.styleFrom(
              shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(20.0),
              ),
              primary: AppColors.red400,
              elevation: 0,
            ),
            child: Text(
              'Tạo nhóm mới',
              style: TextStyle(
                fontFamily: 'Quicksand',
                fontSize: 14,
                color: AppColors.red,
              ),
              textAlign: TextAlign.left,
            ),
          )
        ],
      ),
    );
  }
}
