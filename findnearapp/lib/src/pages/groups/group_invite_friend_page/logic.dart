import 'package:dio/dio.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/input/group_member_input.dart';
import 'package:findnear/src/models/input/invite_user_to_group_input.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/repository/findnear_group_repository.dart';
import 'package:findnear/src/repository/user_v2_repository.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'state.dart';

class GroupInviteFriendLogic extends GetxController {
  final GroupInviteFriendState state;

  final FindNearGroupRepository _repository = Get.find();
  final _userV2Repository = Get.find<UserV2Repository>();

  GroupInviteFriendLogic() : state = GroupInviteFriendState();

  int groupId;
  final int limit = 10;
  OverlayEntry loader;

  void initialize() async {
    if (groupId == null) {
      return;
    }
    state.currentOffset.value = 0;
    state.isLoading.value = true;
    await fetchFriends(offset: state.currentOffset.value, limit: limit);
    await getGroupMembers(groupId);
    state.isLoading.value = false;
  }

  void onRefreshData() async {
    state.currentOffset.value = 0;
    state.isRefreshing.value = true;
    await fetchFriends(
      offset: state.currentOffset.value,
      limit: limit,
      isRefresh: true,
    );
    await getGroupMembers(groupId);
    state.isRefreshing.value = false;
  }

  void onLoadMoreData() async {
    print("onLoadMoreData");
    if (!state.canLoadMore.value) {
      return;
    }
    state.isLoadingMore.value = true;
    state.canLoadMore.value = false;
    await fetchFriends(offset: state.currentOffset.value, limit: limit);
    state.isLoadingMore.value = false;
  }

  void inviteMemberToGroup(BuildContext context, User user) async {
    final userId = int.parse(user.id);
    if (groupId == null || userId == null) {
      return;
    }
    try {
      loader = Helper.overlayLoader(context);
      Overlay.of(context).insert(loader);

      final input = InviteUserToGroupInput.inviteToGroup(
        groupId: groupId,
        userId: userId,
      );
      final res = await _repository.inviteUserToGroup(input);
      state.members.add(user);
      Helper.hideLoader(loader);
    } catch (e) {
      print("inviteMemberToGroup");
      Helper.showErrorMessage(e);
      Helper.hideLoader(loader);
    }
  }

  Future<void> fetchFriends({
    offset = 0,
    limit = 10,
    isRefresh = false,
  }) async {
    try {
      final keyword = state.keywordSearch.value;
      final response = await _userV2Repository.fetchFriends(
        keyword: keyword,
        currentOffset: offset,
        limit: limit,
      );
      final friends = response.data;

      if (isRefresh) {
        state.friends.value.clear();
      }
      state.friends.value.addAll(friends);

      state.canLoadMore.value = friends.length == limit;
      state.currentOffset.value = state.friends.value.length;
    } catch (e) {
      print("fetchFriends");
      print(e);
      if (e is DioError) {
        print(e.response);
      }
    }
  }

  Future<void> getGroupMembers(
    int groupId, {
    offset = 0,
    limit = 10,
    isRefresh = false,
  }) async {
    try {
      final input = GroupMemberInput(
        groupId: groupId,
      );

      final res = await _repository.getGroupMembers(
        input,
        // offset: offset,
        // limit: limit,
      );
      final members = res.data;

      if (isRefresh) {
        state.members.clear();
      }
      state.members.addAll(members);
    } catch (e) {
      print("getGroupMembers");
      print(e);
      if (e is DioError) {
        print(e.response);
      }
    }
  }
}
