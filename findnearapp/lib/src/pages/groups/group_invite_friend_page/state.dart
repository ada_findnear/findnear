import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:get/get.dart';

class GroupInviteFriendState {
  RxInt currentOffset = 0.obs;
  Rx<bool> isLoading = false.obs;
  Rx<bool> isRefreshing = false.obs;
  Rx<bool> isLoadingMore = false.obs;
  RxBool canLoadMore = false.obs;
  Rx<MessageStates> message = MessageStates.idle.obs;

  RxList<User> allMembers = List<User>.empty().obs;
  RxList<User> members = List<User>.empty().obs;
  final friends = Rx<List<User>>([]);

  // loadmore
  final canLoadmore = true.obs;

  // search
  final keywordSearch = "".obs;
}
