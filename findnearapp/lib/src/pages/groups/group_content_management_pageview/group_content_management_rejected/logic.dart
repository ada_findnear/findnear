import 'package:dio/dio.dart';
import 'package:findnear/src/models/input/GetPostsOfGroupInput.dart';
import 'package:findnear/src/repository/findnear_group_repository.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'state.dart';

class GroupContentManagementRejectedLogic extends GetxController {
  GroupContentManagementRejectedState state;

  final FindNearGroupRepository _repository = Get.find();

  GroupContentManagementRejectedLogic()
      : state = GroupContentManagementRejectedState();

  int groupId;
  final int limit = 10;
  OverlayEntry loader;

  void initialize() async {
    if (groupId == null) {
      return;
    }
    state.currentOffset.value = 0;
    state.isLoading.value = true;
    await getPosts(
      offset: state.currentOffset.value,
      limit: limit,
    );
    state.isLoading.value = false;
  }

  void onRefreshData() async {
    state.currentOffset.value = 0;
    state.isLoading.value = true;
    await getPosts(
      offset: state.currentOffset.value,
      limit: limit,
      isRefresh: true,
    );
    state.isLoading.value = false;
  }

  void onLoadMoreData() async {
    print("onLoadMoreData");
    if (!state.canLoadMore.value) {
      return;
    }
    state.isLoadingMore.value = true;
    state.canLoadMore.value = false;
    await getPosts(
      offset: state.currentOffset.value,
      limit: limit,
    );
    state.isLoadingMore.value = false;
  }

  Future<void> getPosts({
    offset = 0,
    limit = 10,
    isRefresh = false,
  }) async {
    try {
      final input = GetPostsOfGroupInput.rejectedPosts(
        groupId: groupId,
      );

      final res = await _repository.getPostsOfGroup(
        input,
        offset: offset,
        limit: limit,
      );
      final members = res.data;

      if (isRefresh) {
        state.posts.clear();
      }
      state.posts.addAll(members);
      state.canLoadMore.value = members.length == limit;
      state.currentOffset.value = state.posts.length;
    } catch (e) {
      print("getGroupMembers");
      print(e);
      if (e is DioError) {
        print(e.response);
      }
    }
  }
}
