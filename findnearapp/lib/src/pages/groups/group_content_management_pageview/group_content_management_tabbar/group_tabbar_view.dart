import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/pages/groups/group_content_management_pageview/group_content_management_approval/logic.dart';
import 'package:findnear/src/pages/groups/group_content_management_pageview/group_content_management_approval/state.dart';
import 'package:findnear/src/pages/groups/group_content_management_pageview/group_content_management_pending/logic.dart';
import 'package:findnear/src/pages/groups/group_content_management_pageview/group_content_management_pending/state.dart';
import 'package:findnear/src/pages/groups/group_content_management_pageview/group_content_management_rejected/logic.dart';
import 'package:findnear/src/pages/groups/group_content_management_pageview/group_content_management_rejected/state.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

enum GroupContentManagementScreenType {
  waiting_approval_post,
  browsed_post,
  rejected_post,
}

class GroupContentManagementTabBar extends StatelessWidget {
  Function itemClickCallback;
  GroupContentManagementScreenType currentGroupTabBar;

  GroupContentManagementTabBar(
      {this.itemClickCallback, this.currentGroupTabBar});

  final _groupTabBarDataSource = GroupContentManagementScreenType.values;

  final GroupContentManagementApprovalState _approvalPostsState =
      Get.find<GroupContentManagementApprovalLogic>().state;
  final GroupContentManagementPendingState _pendingPostsState =
      Get.find<GroupContentManagementPendingLogic>().state;
  final GroupContentManagementRejectedState _rejectedPostsState =
      Get.find<GroupContentManagementRejectedLogic>().state;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: double.infinity,
      child: ListView.separated(
        separatorBuilder: (BuildContext context, int index) {
          return SizedBox(
            width: 10,
          );
        },
        padding: EdgeInsets.symmetric(horizontal: 10),
        itemCount: _groupTabBarDataSource.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int index) {
          final tabbar = _groupTabBarDataSource.elementAt(index);
          return _buildGroupItem(context, tabbar);
        },
      ),
    );
  }

  String getGroupTabBarTitle(
      GroupContentManagementScreenType tabBarType, int numberOfMember) {
    switch (tabBarType) {
      case GroupContentManagementScreenType.waiting_approval_post:
        return "Bài chờ duyệt ($numberOfMember)";
      case GroupContentManagementScreenType.browsed_post:
        return "Đã duyệt ($numberOfMember)";
      case GroupContentManagementScreenType.rejected_post:
        return "Đã từ chối ($numberOfMember)";
      default:
        return "";
    }
  }

  Widget _buildGroupItem(
    BuildContext context,
    GroupContentManagementScreenType tabBarType,
  ) {
    return Obx(() {
      int numberOfMembers = 0;
      switch (tabBarType) {
        case GroupContentManagementScreenType.waiting_approval_post:
          numberOfMembers = _pendingPostsState.posts.length;
          break;
        case GroupContentManagementScreenType.browsed_post:
          numberOfMembers = _approvalPostsState.posts.length;
          break;
        case GroupContentManagementScreenType.rejected_post:
          numberOfMembers = _rejectedPostsState.posts.length;
          break;
      }

      final title = getGroupTabBarTitle(tabBarType, numberOfMembers);
      var isSelected = currentGroupTabBar == tabBarType;
      var titleTextColor = isSelected ? AppColors.white : AppColors.gray;
      var groupBgColor = isSelected ? AppColors.red : Color(0xFFF2F2F2);

      return GestureDetector(
        onTap: () {
          itemClickCallback.call(tabBarType);
        },
        child: Container(
          child: Align(
            alignment: Alignment.center,
            child: Container(
              height: 30,
              decoration: new BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: groupBgColor,
              ),
              child: Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  child: Text(
                    title,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context).textTheme.overline.merge(
                          TextStyle(
                            color: titleTextColor,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                  ),
                ),
              ),
            ),
          ),
        ),
      );
    });
  }
}
