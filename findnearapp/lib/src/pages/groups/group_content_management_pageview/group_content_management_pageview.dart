import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/pages/groups/group_content_management_pageview/group_content_management_tabbar/group_tabbar_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';

import './group_content_management_tabbar/group_tabbar_view.dart';
import 'group_content_management_approval/logic.dart';
import 'group_content_management_approval/view.dart';
import 'group_content_management_pending/logic.dart';
import 'group_content_management_pending/view.dart';
import 'group_content_management_rejected/logic.dart';
import 'group_content_management_rejected/view.dart';

class GroupContentManagementPageView extends StatefulWidget {
  GroupEntity group;

  GroupContentManagementPageView({
    Key key,
    this.group,
  }) : super(key: key);

  @override
  _GroupViewState createState() => _GroupViewState();
}

class _GroupViewState extends State<GroupContentManagementPageView> {
  PageController _pageController;
  int currentTab = 0;
  GroupContentManagementScreenType currentGroupTabBar =
      GroupContentManagementScreenType.waiting_approval_post;

  final GroupContentManagementApprovalLogic _approvalPostsLogic =
      Get.put(GroupContentManagementApprovalLogic());
  final GroupContentManagementPendingLogic _pendingPostsLogic =
      Get.put(GroupContentManagementPendingLogic());
  final GroupContentManagementRejectedLogic _rejectedPostsLogic =
      Get.put(GroupContentManagementRejectedLogic());

  @override
  void initState() {
    super.initState();

    if (widget.group != null) {
      _approvalPostsLogic.groupId = widget.group.id;
      _pendingPostsLogic.groupId = widget.group.id;
      _rejectedPostsLogic.groupId = widget.group.id;
    }

    _approvalPostsLogic.initialize();
    _pendingPostsLogic.initialize();
    _rejectedPostsLogic.initialize();

    _pageController = PageController(initialPage: currentTab);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: _getAppBar(),
      body: Column(
        children: [
          GroupContentManagementTabBar(
            itemClickCallback: (tabBarType) {
              print("Click");
              print(tabBarType);
              switch (tabBarType) {
                case GroupContentManagementScreenType.waiting_approval_post:
                  currentTab = 0;
                  break;
                case GroupContentManagementScreenType.browsed_post:
                  currentTab = 1;
                  break;
                case GroupContentManagementScreenType.rejected_post:
                  currentTab = 2;
                  break;
              }
              _pageController.jumpToPage(currentTab);
              currentGroupTabBar = tabBarType;

              setState(() {});
            },
            currentGroupTabBar: currentGroupTabBar,
          ),
          Expanded(
            child: PageView(
              controller: _pageController,
              onPageChanged: (int page) {
                switch (page) {
                  case 0:
                    currentGroupTabBar =
                        GroupContentManagementScreenType.waiting_approval_post;
                    break;
                  case 1:
                    currentGroupTabBar =
                        GroupContentManagementScreenType.browsed_post;
                    break;
                  case 2:
                    currentGroupTabBar =
                        GroupContentManagementScreenType.rejected_post;
                    break;
                }
                setState(() {});
              },
              children: [
                GroupContentManagementPendingView(),
                GroupContentManagementApprovalView(),
                GroupContentManagementRejectedView(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _getAppBar() {
    return AppBar(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      leadingWidth: 52,
      centerTitle: false,
      title: Text(
        "Quản trị nội dung",
        style: Theme.of(context).textTheme.headline2.merge(
              TextStyle(
                color: AppColors.textBlack,
                fontSize: 16,
              ),
            ),
      ),
      actions: [],
    );
  }
}
