import 'package:dio/dio.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/input/GetPostsOfGroupInput.dart';
import 'package:findnear/src/models/input/UpdatePostGroupStatusInput.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/repository/findnear_group_repository.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'state.dart';

class GroupContentManagementApprovalLogic extends GetxController {
  GroupContentManagementApprovalState state;

  final FindNearGroupRepository _repository = Get.find();

  GroupContentManagementApprovalLogic()
      : state = GroupContentManagementApprovalState();

  int groupId;
  final int limit = 10;
  OverlayEntry loader;

  void initialize() async {
    if (groupId == null) {
      return;
    }
    state.currentOffset.value = 0;
    state.isLoading.value = true;
    await getPosts(
      offset: state.currentOffset.value,
      limit: limit,
    );
    state.isLoading.value = false;
  }

  void onRefreshData() async {
    state.currentOffset.value = 0;
    state.isLoading.value = true;
    await getPosts(
      offset: state.currentOffset.value,
      limit: limit,
      isRefresh: true,
    );
    state.isLoading.value = false;
  }

  void onLoadMoreData() async {
    print("onLoadMoreData");
    if (!state.canLoadMore.value) {
      return;
    }
    state.isLoadingMore.value = true;
    state.canLoadMore.value = false;
    await getPosts(
      offset: state.currentOffset.value,
      limit: limit,
    );
    state.isLoadingMore.value = false;
  }

  Future<void> getPosts({
    offset = 0,
    limit = 10,
    isRefresh = false,
  }) async {
    try {
      final input = GetPostsOfGroupInput.approvalPost(
        groupId: groupId,
      );

      final res = await _repository.getPostsOfGroup(
        input,
        offset: offset,
        limit: limit,
      );
      final members = res.data;

      if (isRefresh) {
        state.posts.clear();
      }
      state.posts.addAll(members);
      state.canLoadMore.value = members.length == limit;
      state.currentOffset.value = state.posts.length;
    } catch (e) {
      print("getGroupMembers");
      print(e);
      if (e is DioError) {
        print(e.response);
      }
    }
  }

  void bannedPost(
      BuildContext context, Post post, int index, String reason) async {
    final postId = int.parse(post.id);
    if (groupId == null || postId == null) {
      return;
    }
    try {
      loader = Helper.overlayLoader(context);
      Overlay.of(context).insert(loader);

      final input = UpdatePostGroupStatusInput.banPosts(
        groupId: groupId,
        postId: postId,
        reason: reason,
      );
      print("input ${input.toJson()}");
      final res = await _repository.updatePostGroupStatus(input);
      state.posts.removeAt(index);

      Helper.hideLoader(loader);

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Bài viết đã bị gỡ bỏ"),
      ));
    } catch (e) {
      print("inviteMemberToGroup");
      Helper.showErrorMessage(e);
      Helper.hideLoader(loader);
    }
  }
}
