import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/pages/groups/group_content_management_pageview/group_content_management_tabbar/group_tabbar_view.dart';
import 'package:findnear/src/pages/groups/group_content_management_pageview/widgets/content_management_post_item_widget.dart';
import 'package:findnear/src/pages/groups/group_member_administration_pageview/member_admin_type_tabbar/group_tabbar_view.dart';
import 'package:findnear/src/pages/groups/remove_post_of_group_popup/view.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/widgets/shimmer/list_loading_shimmer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'logic.dart';
import 'state.dart';

class GroupContentManagementApprovalView extends StatefulWidget {
  MemberAdminStatus listStatus;

  GroupContentManagementApprovalView({Key key, this.listStatus})
      : super(key: key);

  @override
  _GroupContentManagementApprovalViewState createState() =>
      _GroupContentManagementApprovalViewState();
}

class _GroupContentManagementApprovalViewState
    extends State<GroupContentManagementApprovalView>
    with AutomaticKeepAliveClientMixin {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  final TextEditingController _textEditingController = TextEditingController();

  final GroupContentManagementApprovalLogic _logic =
      Get.find<GroupContentManagementApprovalLogic>();
  final GroupContentManagementApprovalState _state =
      Get.find<GroupContentManagementApprovalLogic>().state;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _buildBody(),
    );
  }

  Widget _buildBody() {
    return Obx(() {
      if (_state.isLoading.value) {
        return ListLoadingShimmer();
      }
      return Column(
        children: [
          SizedBox(
            height: 15,
          ),
          Expanded(
            child: Container(
              child: _buildPostsList(_state.posts),
            ),
          ),
        ],
      );
    });
  }

  Widget _buildPostsList(List<Post> posts) {
    final isHasData = posts.length > 0;

    return Obx(() {
      return RefreshConfiguration.copyAncestor(
        context: Get.context,
        footerTriggerDistance: Get.height * 4,
        child: SmartRefresher(
          enablePullUp: _state.canLoadMore.value,
          onRefresh: () async {
            print("onRefresh");
            await _logic.onRefreshData();
            _refreshController?.refreshCompleted();
          },
          onLoading: () async {
            print("on load more");
            await _logic.onLoadMoreData();
            _refreshController?.loadComplete();
          },
          controller: _refreshController,
          child: !isHasData
              ? Center(
                  child: Text(
                    "Không có bài viết",
                  ),
                )
              : ListView.separated(
                  padding: EdgeInsets.only(top: 10),
                  itemCount: posts.length ?? 0,
                  separatorBuilder: (context, index) => const Divider(
                    height: 8,
                    thickness: 8,
                    color: AppColors.white,
                  ),
                  itemBuilder: (context, index) {
                    final post = posts.elementAt(index);
                    return _buildPostItem(post, index);
                  },
                ),
        ),
      );
    });
  }

  Widget _buildPostItem(Post post, int index) {
    return Container(
      padding: const EdgeInsets.only(top: 3, bottom: 3),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 13),
        margin: const EdgeInsets.symmetric(horizontal: 15),
        decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border.all(
            color: Color(0xFFEDEDED),
          ),
          borderRadius: BorderRadius.circular(20),
        ),
        child: ContentManagementPostItemWidget(
          post: post,
          isMyProfile: post?.creator?.id == currentUser.value?.id,
          contentManagementTab: GroupContentManagementScreenType.browsed_post,
          removePostCallback: () async {
            final res = await showDialog(
              context: context,
              builder: (context) {
                return RemovePostOfGroupPopup();
              },
            );
            final isCancel = res["isCancel"];

            if (isCancel) {
              return;
            }

            final reason = res["reason"];

            _logic.bannedPost(context, post, index, reason);
          },
          onMyPostMenuPressed: () {
            // - Nothing
          },
          onSharePost: () {
            // - Nothing
          },
          onOtherUserPostMenuPressed: () async {
            // - Nothing
          },
          toggleLikePost: (id) {
            // - Nothing
          },
          didUpdateCallback: () {
            // - Nothing
          },
        ),
      ),
    );
  }

  /// Handle AutomaticKeepAliveClientMixin
  @override
  bool get wantKeepAlive => true;
}
