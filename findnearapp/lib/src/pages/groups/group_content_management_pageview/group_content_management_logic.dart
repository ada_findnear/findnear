import 'package:findnear/src/repository/newsfeed_repository.dart';
import 'package:findnear/src/services/notification_service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'group_content_management_state.dart';

class GroupContentManagementLogic extends GetxController {
  final NotificationService notificationService =
      Get.find<NotificationService>();

  final NewsfeedRepository newsfeedRepository = Get.find();

  final GroupContentManagementState state;
  final RefreshController refreshController;

  GroupContentManagementLogic(
      {GlobalKey<ScaffoldState> scaffoldKey, @required this.refreshController})
      : state = GroupContentManagementState();

  void initialize() {}
}
