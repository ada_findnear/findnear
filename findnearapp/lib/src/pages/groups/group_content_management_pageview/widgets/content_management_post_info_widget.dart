import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/elements/CircularAvatarWidget.dart';
import 'package:findnear/src/helpers/date_utils.dart';
import 'package:findnear/src/models/post.dart';
import 'package:flutter/material.dart';

class ContentManagementPostInfoWidget extends StatelessWidget {
  final Post post;
  final VoidCallback onMenuTap;
  final bool hasMenuButton;

  const ContentManagementPostInfoWidget({
    Key key,
    this.post,
    this.onMenuTap,
    this.hasMenuButton = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final groupName = post.group?.title ?? "";
    final postOwnerName = post.creator?.name ?? "";
    final dateTime = DateTime.tryParse(post.created_at ?? "");
    final postGroupCreatedAt = dateTime != null
        ? AppDateFormatUtils.getVerboseDateTimeRepresentation(dateTime)
        : "";

    return Row(
      children: [
        /// Group avatar
        CircularAvatarWidget(
          user: post?.creator,
          size: 42,
          hasBorder: post?.creator?.isOnline,
          hasLiveIndicator: post?.creator?.isLiveStreaming,
        ),
        SizedBox(width: 10),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              /// Group name
              Text(
                groupName,
                style: Theme.of(context).textTheme.headline6.merge(
                      TextStyle(
                        color: AppColors.textBlack,
                        fontSize: 14,
                        fontFamily: 'Quicksand',
                      ),
                    ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                children: [
                  /// Owner group name
                  Text(
                    postOwnerName,
                    style: Theme.of(context).textTheme.headline6.merge(
                          TextStyle(
                            color: AppColors.textBlack,
                            fontSize: 12,
                            fontFamily: 'Quicksand',
                          ),
                        ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    postGroupCreatedAt,
                    style: Theme.of(context).textTheme.bodyText1.merge(
                          TextStyle(
                            color: AppColors.gray2,
                            fontSize: 10,
                          ),
                        ),
                  )
                ],
              )
            ],
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            if (hasMenuButton)
              GestureDetector(
                onTap: () => onMenuTap?.call(),
                child: Container(
                  padding: const EdgeInsets.only(top: 0, left: 16),
                  child: Center(
                    child: Icon(
                      Icons.more_horiz,
                      color: AppColors.grayLight,
                    ),
                  ),
                ),
              ),
          ],
        ),
      ],
    );
  }
}
