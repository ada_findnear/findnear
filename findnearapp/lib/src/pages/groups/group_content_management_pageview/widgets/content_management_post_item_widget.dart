import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/elements/ProfileTopicPhotosWidget.dart';
import 'package:findnear/src/elements/ProfileTopicTextWidget.dart';
import 'package:findnear/src/elements/TopicItemWidget.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/pages/groups/group_content_management_pageview/group_content_management_tabbar/group_tabbar_view.dart';
import 'package:findnear/src/pages/newsfeed/details/newsfeed_post_detail_view.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'content_management_post_info_widget.dart';

class ContentManagementPostItemWidget extends StatelessWidget {
  final TopicItemPreviewPhotoCallback onPreviewPhoto;
  final VoidCallback onMyPostMenuPressed;
  final VoidCallback onSharePost;
  final ToggleLikeButton toggleLikePost;
  final Post post;
  final DetailTopicCallback openDetailTopic;
  final bool isMyProfile;
  final bool hasActionBars;
  final bool isSharedPost;
  final VoidCallback onPressed;
  final VoidCallback onOtherUserPostMenuPressed;
  final VoidCallback didUpdateCallback;
  final VoidCallback rejectPostCallback;
  final VoidCallback approvalPostCallback;
  final VoidCallback removePostCallback;
  final GroupContentManagementScreenType contentManagementTab;

  ContentManagementPostItemWidget({
    Key key,
    this.onPreviewPhoto,
    this.onMyPostMenuPressed,
    this.onSharePost,
    this.post,
    this.toggleLikePost,
    this.openDetailTopic,
    this.isMyProfile,
    this.hasActionBars = true,
    this.isSharedPost = false,
    this.onPressed,
    this.onOtherUserPostMenuPressed,
    this.didUpdateCallback,
    this.contentManagementTab,
    this.rejectPostCallback,
    this.approvalPostCallback,
    this.removePostCallback,
  });

  bool get hasSharedPost => post.isSharedPost;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 0),
          child: ContentManagementPostInfoWidget(
            post: post,
            hasMenuButton: !isSharedPost,
            onMenuTap: () {
              if (!isLoggedIn) {
                NavigatorUtils.toLogin();
                return;
              }
              ;
              if (isMyProfile) {
                onMyPostMenuPressed?.call();
              } else {
                onOtherUserPostMenuPressed?.call();
              }
            },
          ),
        ),
        // _buildPostInfo(context),
        GestureDetector(
          onTap: onPressed,
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
            ),
            // padding: EdgeInsets.only(top: 12, left: 15, right: 13),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.only(
                            top: 5, bottom: 10, left: 0, right: 0),
                        child: ProfileTopicTextWidget(
                          expandText: false,
                          content: post?.content ?? "",
                        ),
                      ),
                    ),
                  ],
                ),
                if (hasSharedPost)
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      border: Border.all(color: AppColors.divider),
                    ),
                    padding:
                        const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                    child: ContentManagementPostItemWidget(
                      post: post.originalPost,
                      isSharedPost: true,
                    ),
                  )
                else
                  ProfileTopicPhotosWidget(
                    onClickPhoto: (index) {
                      print(index);
                      onPreviewPhoto?.call(index);
                    },
                    post: post,
                    onExpandClicked: () => openDetailTopic?.call(),
                    maxImages: 4,
                  ),

                ///
                contentManagementTab ==
                        GroupContentManagementScreenType.waiting_approval_post
                    ? _buildRejectAndApprovalBtn()
                    : (contentManagementTab ==
                            GroupContentManagementScreenType.browsed_post
                        ? _buildRemovePostBtn()
                        : Container()),
              ],
            ),
          ),
        )
      ],
    );
  }

  Row _buildRejectAndApprovalBtn() {
    return Row(
      children: [
        Expanded(
          child: Container(
            child: ElevatedButton(
              onPressed: () {
                rejectPostCallback?.call();
              },
              style: ElevatedButton.styleFrom(
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(20.0),
                ),
                primary: AppColors.white,
                side: BorderSide(color: AppColors.grayBorder),
                elevation: 0,
                minimumSize: Size(double.minPositive, 30),
              ),
              child: Text(
                'Từ chối',
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 14,
                  color: AppColors.gray,
                ),
                textAlign: TextAlign.left,
              ),
            ),
          ),
        ),
        SizedBox(
          width: 10,
        ),
        Expanded(
          child: Container(
            child: ElevatedButton(
              onPressed: () {
                approvalPostCallback?.call();
              },
              style: ElevatedButton.styleFrom(
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(20.0),
                ),
                primary: AppColors.red200,
                side: BorderSide(color: AppColors.red),
                elevation: 0,
                minimumSize: Size(double.minPositive, 30),
              ),
              child: Text(
                'Duyệt',
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 14,
                  color: AppColors.red,
                ),
                textAlign: TextAlign.left,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Row _buildRemovePostBtn() {
    return Row(
      children: [
        Expanded(
          child: Container(
            child: ElevatedButton(
              onPressed: () {
                removePostCallback?.call();
              },
              style: ElevatedButton.styleFrom(
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(20.0),
                ),
                primary: AppColors.red200,
                side: BorderSide(color: AppColors.red),
                elevation: 0,
                minimumSize: Size(double.minPositive, 30),
              ),
              child: Text(
                'Gỡ bài đăng trên trang',
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 14,
                  color: AppColors.red,
                ),
                textAlign: TextAlign.left,
              ),
            ),
          ),
        ),
      ],
    );
  }

  void _handleDidUpdatePostCallback() async {
    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    }
    ;
    await Get.to(
      () => NewsfeedPostDetailsPage(post: post),
    );
    didUpdateCallback?.call();
  }
}
