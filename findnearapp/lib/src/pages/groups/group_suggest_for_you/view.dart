import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/CircularAvatarWidget.dart';
import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:findnear/src/widgets/shimmer/list_loading_shimmer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'logic.dart';
import 'state.dart';

class GroupSuggestForYou extends StatefulWidget {
  ScrollController scrollController;

  GroupSuggestForYou({
    Key key,
    this.scrollController,
  }) : super(key: key);

  @override
  _GroupSuggestForYouState createState() => _GroupSuggestForYouState();
}

class _GroupSuggestForYouState extends State<GroupSuggestForYou> {
  RefreshController refreshController =
      RefreshController(initialRefresh: false);

  GroupSuggestForYouLogic logic = Get.find<GroupSuggestForYouLogic>();
  GroupSuggestForYouState state = Get.find<GroupSuggestForYouLogic>().state;

  @override
  void initState() {
    super.initState();
    logic.initialize();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.symmetric(
                horizontal: 15,
              ),
              child: Text(
                S.of(context).group_suggest_for_you_title,
                style: Theme.of(context).textTheme.headline6.merge(
                      TextStyle(
                        color: AppColors.textBlack,
                        fontSize: 14,
                        fontFamily: 'Quicksand',
                      ),
                    ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: Text(
                S.of(context).group_suggest_for_you_desc,
                style: Theme.of(context).textTheme.bodyText1.merge(
                      TextStyle(
                        color: AppColors.gray2,
                        fontSize: 10,
                      ),
                    ),
              ),
            ),
            Expanded(
              child: Container(
                child: Obx(() {
                  if (state.isLoading.value) {
                    return ListLoadingShimmer();
                  }

                  final groups = state.groups;
                  final isHasData = groups.length > 0;

                  print("groups ${groups.length}");

                  return RefreshConfiguration.copyAncestor(
                    context: Get.context,
                    footerTriggerDistance: Get.height * 4,
                    child: SmartRefresher(
                      enablePullUp: state.canLoadMore.value,
                      enablePullDown: false,
                      onRefresh: () async {
                        await logic.onRefreshData();
                        refreshController?.refreshCompleted();
                      },
                      onLoading: () async {
                        await logic.onLoadMoreData();
                        refreshController?.loadComplete();
                      },
                      controller: refreshController,
                      child: !isHasData
                          ? Center(
                              child: Text("Không có nhóm gợi ý nào!"),
                            )
                          : ListView.separated(
                              controller: widget.scrollController,
                              separatorBuilder:
                                  (BuildContext context, int index) {
                                return SizedBox(
                                  height: 10,
                                );
                              },
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              itemCount: groups.length,
                              itemBuilder: (BuildContext context, int index) {
                                final group = groups.elementAt(index);
                                return _buildGroupItem(index, group);
                              },
                            ),
                    ),
                  );
                }),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildGroupItem(int index, GroupEntity group) {
    String imageUrl = group.groupThumbnail;
    String groupName = group.title ?? "";

    bool isJoined = group.joined ?? false;
    String joinGroupBtnTitle = "";
    if (isJoined) {
      joinGroupBtnTitle = "Chờ phê duyệt";
    } else {
      joinGroupBtnTitle = "Tham gia";
    }

    String numberMember = "${group.members_count ?? 0} thành viên";
    String numberPost = "${group.posts_count ?? 0} bài viết";

    return GestureDetector(
      onTap: () {
        // TODO: -
      },
      child: Container(
        child: Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(5),
              child: Builder(
                builder: (context) {
                  final double size = 50;
                  if (imageUrl == null || imageUrl == "")
                    return AppImage(
                      AppImages.icLogo,
                      fit: BoxFit.cover,
                      width: size,
                      height: size,
                    );
                  return CachedNetworkImage(
                    imageUrl: imageUrl,
                    fit: BoxFit.cover,
                    width: size,
                    height: size,
                    placeholder: (context, url) => CupertinoActivityIndicator(),
                    errorWidget: (context, url, error) =>
                        AvatarPlaceholder(size: size),
                  );
                },
              ),
            ),
            SizedBox(
              width: 5,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    groupName,
                    style: Theme.of(context).textTheme.headline6.merge(
                          TextStyle(
                            color: AppColors.textBlack,
                            fontSize: 14,
                            fontFamily: 'Quicksand',
                          ),
                        ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.people_alt_rounded,
                            size: 10,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            numberMember,
                            style: Theme.of(context).textTheme.bodyText1.merge(
                                  TextStyle(
                                    color: AppColors.gray2,
                                    fontSize: 10,
                                  ),
                                ),
                          )
                        ],
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.sticky_note_2,
                            size: 10,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            numberPost,
                            style: Theme.of(context).textTheme.bodyText1.merge(
                                  TextStyle(
                                    color: AppColors.gray2,
                                    fontSize: 10,
                                  ),
                                ),
                          )
                        ],
                      )
                    ],
                  )
                ],
              ),
            ),
            Container(
              height: 26,
              child: OutlinedButton(
                onPressed: () {
                  if (!isJoined) {
                    logic.requestJoinGroup(context, group, index);
                  }
                },
                style: OutlinedButton.styleFrom(
                  side: BorderSide(
                      color: isJoined ? AppColors.green : AppColors.red,
                      width: 1),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(13),
                  ),
                ),
                child: Text(
                  joinGroupBtnTitle,
                  style: Theme.of(context).textTheme.caption.merge(
                        TextStyle(
                          color: isJoined ? AppColors.green : AppColors.red,
                          fontSize: 12,
                          fontFamily: 'Quicksand',
                        ),
                      ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
