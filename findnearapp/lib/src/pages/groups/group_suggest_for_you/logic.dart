import 'package:dio/dio.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/models/input/invite_user_to_group_input.dart';
import 'package:findnear/src/repository/findnear_group_repository.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'state.dart';

class GroupSuggestForYouLogic extends GetxController {
  final GroupSuggestForYouState state;

  final FindNearGroupRepository _groupRepository = Get.find();

  final int limit = 10;
  OverlayEntry loader;

  GroupSuggestForYouLogic({
    GlobalKey<ScaffoldState> scaffoldKey,
  }) : state = GroupSuggestForYouState();

  void initialize() async {
    state.isLoading.value = true;
    await getGroupForSuggest(offset: 0, limit: limit);
    state.isLoading.value = false;
  }

  void onRefreshData() async {
    state.currentOffset.value = 0;
    state.isLoading.value = true;
    await getGroupForSuggest(
      offset: 0,
      limit: limit,
      isRefresh: true,
    );
    state.isLoading.value = false;
  }

  void onLoadMoreData() async {
    if (!state.canLoadMore.value) {
      return;
    }
    state.isLoadingMore.value = true;
    state.canLoadMore.value = false;
    await getGroupForSuggest(offset: state.currentOffset.value, limit: limit);
    state.isLoadingMore.value = false;
  }

  void getGroupForSuggest({
    offset = 0,
    limit = 10,
    isRefresh = false,
  }) async {
    try {
      final res = await _groupRepository.getSuggestGroups(
        offset: offset,
        limit: limit,
      );
      final groups = res.data;
      if (isRefresh) {
        state.groups.clear();
      }
      print("prev state.groups ${state.groups.length}");

      state.groups.addAll(groups);
      print("after state.groups ${state.groups.length}");

      state.canLoadMore.value = groups.length == limit;
      state.currentOffset.value = state.groups.length;
    } catch (e) {
      print("getGroupForSuggest");
      print(e);
      if (e is DioError) {
        print(e.response);
      }
    }
  }

  void requestJoinGroup(
    BuildContext context,
    GroupEntity group,
    int index,
  ) async {
    final currentUserId = currentUser.value.id;
    final ownerId = group.owner_id;
    if (currentUserId.toString() == ownerId.toString()) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Không thể tham gia nhóm!"),
      ));
      return;
    }

    state.requestJoinGroupState.value = LoadingState.loading;
    try {
      loader = Helper.overlayLoader(context);
      Overlay.of(context).insert(loader);

      final input = InviteUserToGroupInput.joinGroup(group.id);
      await _groupRepository.inviteUserToGroup(input);

      final _group = state.groups.elementAt(index);
      final updateGroup = _group.copyWith(joined: true);
      state.groups[index] = updateGroup;

      state.requestJoinGroupState.value = LoadingState.success;
      Helper.hideLoader(loader);

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Tham gia nhóm thành công"),
      ));
    } catch (e) {
      state.requestJoinGroupState.value = LoadingState.failure;
      Helper.hideLoader(loader);
      Helper.showErrorMessage(e);
    }
  }
}
