import 'package:get/get.dart';

import 'state.dart';

class RemovePostOfGroupPopupLogic extends GetxController {
  final RemovePostOfGroupPopupState state;

  RemovePostOfGroupPopupLogic() : state = RemovePostOfGroupPopupState();


  void initialize() {
    state.reasonDataSource.addAll([{
      "value": 1,
      "label": "abcd",
    }]);
  }

}
