import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'logic.dart';
import 'state.dart';

class RemovePostOfGroupPopup extends StatefulWidget {
  RemovePostOfGroupPopup({
    Key key,
  }) : super(key: key);

  @override
  _RemovePostOfGroupPopupState createState() => _RemovePostOfGroupPopupState();
}

class _RemovePostOfGroupPopupState extends State<RemovePostOfGroupPopup> {
  final RemovePostOfGroupPopupLogic logic =
      Get.put(RemovePostOfGroupPopupLogic());
  final RemovePostOfGroupPopupState state =
      Get.find<RemovePostOfGroupPopupLogic>().state;

  final _reasonTextEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();
    logic.initialize();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        child: Center(
          child: Container(
            height: 200,
            margin: EdgeInsets.symmetric(
              horizontal: 30,
            ),
            decoration: BoxDecoration(
              color: AppColors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              children: [
                Container(
                  height: 40,
                  decoration: BoxDecoration(
                    color: AppColors.red500,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10),
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      _buildTitle(context),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                _buildDesc(context),
                SizedBox(height: 10),
                _buildReasonRemove(),
                SizedBox(height: 10),
                _buildButtonPopup(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Container _buildDesc(BuildContext context) {
    return Container(
      child: Text(
        "Bạn có chắc chắn muốn gỡ bài đăng này?",
        style: Theme.of(context).textTheme.headline6.merge(
              TextStyle(
                color: AppColors.textBlack,
                fontSize: 14,
                fontFamily: 'Quicksand',
                fontWeight: FontWeight.normal,
              ),
            ),
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
    );
  }

  Widget _buildTitle(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Stack(
        children: [
          Center(
            child: Text(
              "Gỡ bài đăng trên nhóm",
              style: Theme.of(context).textTheme.headline6.merge(
                    TextStyle(
                      color: AppColors.textBlack,
                      fontSize: 16,
                      fontFamily: 'Quicksand',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
            ),
          ),
          Positioned(
            right: 5,
            top: 0,
            bottom: 0,
            child: GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Container(
                width: 48,
                child: Center(
                  child: Icon(
                    Icons.close_rounded,
                    color: Theme.of(context).iconTheme.color,
                    size: 20,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildReasonRemove() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Lý do gỡ",
            style: Theme.of(context).textTheme.headline6.merge(
                  TextStyle(
                    color: AppColors.gray,
                    fontSize: 12,
                    fontFamily: 'Quicksand',
                    fontWeight: FontWeight.normal,
                  ),
                ),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          Container(
            height: 40,
            child: TextField(
              controller: _reasonTextEditingController,
              style: TextStyle(color: Theme.of(context).hintColor),
              keyboardType: TextInputType.emailAddress,
              decoration: getInputDecoration(
                context,
                labelText: "",
                hintText: "",
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildButtonPopup() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 15),
      child: Center(
        child: Row(
          children: <Widget>[
            Expanded(
              child: Container(
                child: ElevatedButton(
                  onPressed: () {
                    Get.back(result: {
                      "isCancel": true,
                    });
                  },
                  style: ElevatedButton.styleFrom(
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(20.0),
                    ),
                    primary: AppColors.white,
                    side: BorderSide(color: AppColors.grayBorder),
                    elevation: 0,
                    minimumSize: Size(double.minPositive, 30),
                  ),
                  child: Text(
                    S.of(context).cancel,
                    style: TextStyle(
                      fontFamily: 'Quicksand',
                      fontSize: 14,
                      color: AppColors.gray,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 15,
            ),
            Expanded(
              child: Container(
                child: ElevatedButton(
                  onPressed: () {
                    Get.back(result: {
                      "isCancel": false,
                      'reason': _reasonTextEditingController.text,
                    });
                  },
                  style: ElevatedButton.styleFrom(
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(20.0),
                    ),
                    primary: AppColors.red,
                    side: BorderSide(color: AppColors.red),
                    elevation: 0,
                    minimumSize: Size(double.minPositive, 30),
                  ),
                  child: Text(
                    S.of(context).confirm,
                    style: TextStyle(
                      fontFamily: 'Quicksand',
                      fontSize: 14,
                      color: AppColors.white,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ),
            )
          ],
          mainAxisAlignment: MainAxisAlignment.end,
        ),
      ),
    );
  }
}
