import 'dart:async';

import 'package:findnear/src/commons/app_colors.dart';
import 'package:flutter/material.dart';

class CustomStopWatch extends StatefulWidget {
  final int tick;

  CustomStopWatch(this.tick):super();

  @override
  _CustomStopWatchState createState() => _CustomStopWatchState();
}

class _CustomStopWatchState extends State<CustomStopWatch> {
  Stream<int> timerStream;
  StreamSubscription<int> timerSubscription;
  String minutesStr = '00';
  String secondsStr = '00';
  bool flag = true;
  int ticktok = 0;

  @override
  void initState() {
    super.initState();
    timerStream = stopWatchStream();
    ticktok = widget.tick;
    timerSubscription = timerStream.listen((int newTick) {
      setState(() {
        int t =0;
        t = ticktok + newTick;
        minutesStr = ((t / 60) % 60).floor().toString().padLeft(2, '0');
        secondsStr = (t % 60).floor().toString().padLeft(2, '0');
      });
    });
  }

  Stream<int> stopWatchStream() {
    StreamController<int> streamController;
    Timer timer;
    Duration timerInterval = Duration(seconds: 1);
    int counter = 0;

    void stopTimer() {
      if (timer != null) {
        timer.cancel();
        timer = null;
        counter = 0;
        streamController.close();
      }
    }

    void tick(_) {
      counter++;
      streamController.add(counter);
      if (!flag) {
        stopTimer();
      }
    }

    void startTimer() {
      timer = Timer.periodic(timerInterval, tick);
    }

    streamController = StreamController<int>(
      onListen: startTimer,
      onCancel: stopTimer,
      onResume: startTimer,
      onPause: stopTimer,
    );

    return streamController.stream;
  }

  @override
  void dispose() {
    timerSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        "$minutesStr:$secondsStr",
        style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w500,
          color: AppColors.white,
        ),
        textAlign: TextAlign.center,
      ),
    );
  }
}
