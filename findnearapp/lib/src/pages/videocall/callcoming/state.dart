import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/entities/offer_call_entity.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class VideoCallComingState extends BaseState{
  RxString userName = "".obs;
  RxString userAvatar = "".obs;
  RxString callStatus = "".obs;
  RxString errorStringPermission = "".obs;
  RxBool checkPermission = false.obs;
  OfferCallEntity targetUser;
  BuildContext buildContext;
  VideoCallComingState(){}
}