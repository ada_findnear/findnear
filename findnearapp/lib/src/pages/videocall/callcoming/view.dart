import 'package:findnear/main.dart';
import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/global/global_event.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/pages/videocall/callcoming/controller.dart';
import 'package:findnear/src/pages/videocall/callcoming/state.dart';
import 'package:findnear/src/pages/voice_call/widgets/sprite_painter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:tap_debouncer/tap_debouncer.dart';

class VideoCallComingPage extends StatefulWidget {
  static const String ROUTE_NAME = '/VideoCallComing';
  final RouteArgument routeArgument;
  final Function endCallCallback;
  final Function acceptCallCallback;

  VideoCallComingPage(
      {Key key,
      this.routeArgument,
      this.endCallCallback,
      this.acceptCallCallback})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _VideoCallComingState();
  }
}

class _VideoCallComingState extends BaseWidgetState<VideoCallComingPage,
        VideoCallComingController, VideoCallComingState>
    with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  AnimationController _controller;

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      _controller = AnimationController(vsync: this);
      _startAnimation();

      state.buildContext = context;
      state.targetUser = widget.routeArgument.param['targetUser'];
      state.callStatus.value = S.current.calling;
      controller.initialize();
      await controller.searchUserFromCode(state.targetUser.userMakeCall);
      // bật chuông
      controller.loadAudio();
      GlobalEvent.instance.onCloseOverlayCall.stream.listen((data) async {
        if (data == true) {
          widget.endCallCallback();
        }
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: Scaffold(
        backgroundColor: Color(0xffFFB5B5),
        body: Obx(() {
          return Stack(
            children: [
              _buildLayoutCallComingTop(),
              Positioned(
                bottom: 55,
                left: 64,
                right: 64,
                child: _buildLayoutCallComingBottom(),
              ),
            ],
          );
        }),
      ),
    );
  }

  Widget _buildLayoutCallComingTop() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: MediaQuery.of(context).padding.top + 66),
          Container(
            width: 270,
            height: 270,
            child: Stack(
              alignment: Alignment.center,
              children: [
                CustomPaint(
                  painter: SpritePainter(_controller),
                  child: SizedBox(
                    width: 270.0,
                    height: 270.0,
                  ),
                ),
                Positioned(
                  child: Container(
                    width: 80,
                    height: 80,
                    child: CircleAvatar(
                      backgroundImage: NetworkImage(state.userAvatar.value ??
                          "${GlobalConfiguration().getValue('base_url')}images/image_default.png"),
                      radius: 80 / 2,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(80 / 2),
                      border: Border.all(color: Colors.white, width: 2),
                    ),
                  ),
                )
              ],
            ),
          ),
          Text(
            Helper.limitString(state.userName.value ?? "", limit: 20),
            style: TextStyle(
              fontSize: 20,
              color: Color(0xffFF0000),
              fontWeight: FontWeight.w800,
              fontFamily: 'Quicksand',
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            state.callStatus.value,
            style: TextStyle(
              fontSize: 16,
              color: Color(0xffFF0000),
              fontWeight: FontWeight.w400,
              fontFamily: 'Quicksand',
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildLayoutCallComingBottom() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            TapDebouncer(
              onTap: () async {
                await endCall();
              }, // your tap handler moved here
              builder: (BuildContext context, TapDebouncerFunc onTap) {
                return IconButton(
                  iconSize: 60,
                  padding: EdgeInsets.all(0),
                  onPressed: onTap,
                  icon: SvgPicture.asset(AppImages.icDecline),
                );
              },
            ),
            SizedBox(
              height: 7,
            ),
            Text(
              S.current.decline,
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontFamily: 'Quicksand',
              ),
            )
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            TapDebouncer(
              onTap: () async {
                await acceptCall();
                await Future<void>.delayed(const Duration(milliseconds: 2000));
              }, // your
              builder: (BuildContext context, TapDebouncerFunc onTap) {
                return IconButton(
                  iconSize: 60,
                  padding: EdgeInsets.all(0),
                  onPressed: onTap,
                  icon: SvgPicture.asset(AppImages.icAccept),
                );
              },
            ),
            SizedBox(
              height: 7,
            ),
            Text(
              S.current.accept,
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontFamily: 'Quicksand',
              ),
            )
          ],
        ),
      ],
    );
  }

  void _startAnimation() {
    _controller.stop();
    _controller.reset();
    _controller.repeat(
      period: Duration(seconds: 1),
    );
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.paused) {
      controller.signaling.disableLocalCamera();
    } else if (state == AppLifecycleState.resumed) {
      controller.signaling.enableLocalCamera();
    } else if (state == AppLifecycleState.detached) {
      controller.signaling.disableLocalCamera();
    }
  }

  @override
  void dispose() async {
    _controller.dispose();
    await controller.resetAudioCache();
    Get.delete<VideoCallComingController>();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  void endCall() async {
    controller.resetAudioCache();
    await controller.closeWebrtc();
    await controller.signaling?.bye();
    state.callStatus.value = "Kêt thúc";
    widget.endCallCallback();
  }

  void acceptCall() async {
    await controller.checkPermission();
    if (state.checkPermission.value == true) {
      await controller.acceptCall(state.targetUser.userMakeCall);
      widget.acceptCallCallback();
    } else {
      var error = state.errorStringPermission.value;
      Widget cancelButton = TextButton(
        child: Text("Cancel"),
        onPressed: () {
          Get.back();
        },
      );
      Widget settingButton = TextButton(
        child: Text("Open Settings"),
        onPressed: () async {
          await openAppSettings();
        },
      );
      AlertDialog alert = AlertDialog(
        title: Text("Thông báo"),
        content: Text(error),
        actions: [cancelButton, settingButton],
      );
      showDialog(
        context: Get.context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    }
  }

  @override
  VideoCallComingController createController() =>
      Get.put(VideoCallComingController());
}
