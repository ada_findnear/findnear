import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/global/global_data.dart';
import 'package:findnear/src/global/global_event.dart';
import 'package:findnear/src/repository/message_repository.dart';
import 'package:findnear/src/repository/notification_v2_repository.dart';
import 'package:findnear/src/utils/listener_event_callkit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:tap_debouncer/tap_debouncer.dart';

import '../../base/base_widget_state.dart';
import '../../commons/app_colors.dart';
import '../../commons/app_images.dart';
import '../../extensions/duration_ext.dart';
import '../../models/entities/message_chat/enums/message_call_type.dart';
import '../../models/route_argument.dart';
import '../../repository/user_repository.dart';
import '../voice_call/widgets/sprite_painter.dart';
import 'controller.dart';
import 'draggablewidget/anchoringposition.dart';
import 'draggablewidget/view.dart';
import 'state.dart';

class VideoCallPage extends StatefulWidget {
  static const String ROUTE_NAME = '/VideoCall';
  final RouteArgument routeArgument;
  final Function endCallCallback;

  VideoCallPage({Key key, this.routeArgument, this.endCallCallback})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _VideoCallPageState();
  }
}

class _VideoCallPageState
    extends BaseWidgetState<VideoCallPage, VideoCallController, VideoCallState>
    with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  Function get callback => widget.routeArgument.param["callback"];
  final notificationV2Repository = Get.find<NotificationV2Repository>();
  final eventCallKit = ListenerEventCallKit();

  AnimationController _controller;
  final dragController = DragController();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      _controller = AnimationController(vsync: this);
      state.callStatus.value = "";
      state.userName.value = "";
      state.userAvatar.value = "";
      state.isAnswer.value = false;
      GlobalData.instance.isCallShowing = true;

      _startAnimation();

      state.targetUser = widget.routeArgument.param['targetUser'];
      state.isMakeCall.value = widget.routeArgument.param['isMakeCall'];
      state.isAnswer.value = widget.routeArgument.param['isAnswer'];
      state.conversation.value = widget.routeArgument.param['conversation'];
      state.isFromCallkit.value = widget.routeArgument.param['callkit'];

      controller.initialize();
      await controller.searchUserFromCode(state.targetUser.code);
      await controller.getUserStatus(state.targetUser.code);

      if (state.isFromCallkit.value == true) {
        state.offerCallEntity = widget.routeArgument.param['offerCallEntity'];
        if (state.isAnswer.value == true) {
          controller.acceptCallFromCallKit();
        }
      } else {
        if (state.isMakeCall.value == true) {
          await controller.connectWebrtc();
          await controller.loadAudio();
          state.isMic.value = true;
          state.isCamera.value = true;
          controller.signaling.enableLocalCamera();
          controller.signaling.enableMic();
          controller.signaling.enableSpeaker();

          state.callStatus.value = "Đang nối máy";
          if (!state.statusUserCall.status) {
            // user không online ==> kill app . send message thông báo có cuộc gọi
            Get.find<MessageRepository>().sendMakeCallNotification(
              state.targetUser.code,
              "1e74092d-61a4-4e09-ad41-1adfa0609c5e",
              "VIDEO_CALL",
            );
            eventCallKit.updateUserStatus(1,currentUser.value.code, true);
          } else {
            // user online
            await Future.delayed(Duration(seconds: 1));
            if (state.statusUserCall.isCalling == true) {
              state.callStatus.value = "Người dùng bận";
              await controller.resetAudioCache();
              await Future.delayed(Duration(seconds: 1));
              widget.endCallCallback();
            } else {
              if (state.statusUserCall.devicePlatform == "IOS") {
                Get.find<MessageRepository>().sendMakeCallNotification(
                  state.targetUser.code,
                  "1e74092d-61a4-4e09-ad41-1adfa0609c5e",
                  "VIDEO_CALL",
                );
              }
              state.callStatus.value = "Đang đổ chuông";
              await controller.createCall();
            }
          }
        }

        if (state.isAnswer.value) {
          try {
            state.isRTCInitRender.value = true;
            controller.resetAudioCache();
            state.isMic.value = true;
            state.isCamera.value = true;
            controller.signaling.enableLocalCamera();
            controller.signaling.enableMic();
            controller.signaling.enableSpeaker();
            controller.startTimer();
          } catch (e) {
            print("TuanLA - $e");
          }
        }
        // setup time delay
        state.isTimeOut.value = true;
        controller.delayCall(context);
      }

      GlobalEvent.instance.onCloseOverlayCall.stream.listen((data) async {
        if (data == true) {
          widget.endCallCallback();
        }
      });
    });
  }

  @override
  void deactivate() {
    super.deactivate();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState appLifecycleState) async {
    super.didChangeAppLifecycleState(appLifecycleState);
    if (appLifecycleState == AppLifecycleState.paused) {
      controller.signaling.disableLocalCamera();
    } else if (appLifecycleState == AppLifecycleState.resumed) {
      controller.signaling.enableLocalCamera();
    } else if (appLifecycleState == AppLifecycleState.detached) {
      state.isTimeOut.value = false;
      controller.signaling.disableLocalCamera();
      if (controller.signaling.isMakeCall) {
        if (state.isAnswer.value) {
          // kết thúc cuộc gọi khi đối tác đã chấp nhận cuộc gọi
          controller.sendMessage(
              state.callDuration.value, MessageCallStateType.accept);
        } else {
          // kết thúc cuộc gọi khi đối tác chưa chấp nhận cuộc gọi
          controller.sendMessage(0, MessageCallStateType.cancel);
        }
      }
      controller.resetAudioCache();
      await controller.signaling?.bye();
    }
  }

  @override
  void dispose() {
    GlobalData.instance.isCallShowing = false;
    controller.resetAudioCache();
    state.isMic.value = false;
    state.isCamera.value = false;
    controller.signaling.disableMic();
    controller.signaling.disableLocalCamera();
    controller.signaling.disableSpeaker();
    state.timer.cancel();
    _controller.dispose();
    Get.delete<VideoCallController>();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: _buildLayoutCall(),
    );
  }

  Widget _buildLayoutCall() {
    return Scaffold(
        backgroundColor: Color(0xffFFB5B5),
        body: Stack(
          children: [
            Obx(() {
              return Container(
                child: state.isAnswer.value
                    ? _buildReceiverAvatar()
                    : _buildLayoutWaiting(),
              );
            }),

            Obx(() {
              return Positioned(
                left: 15,
                right: 15,
                top: ScreenUtil().statusBarHeight + 17,
                child: _buildLayoutTop(),
              );
            }),
            Obx(() {
              return _buildUserAvatar();
            }),
            Obx(() {
              return Positioned(
                bottom: 55,
                left: 0,
                right: 0,
                child: _buildLayoutBottom(),
              );
            }),
          ],
        ));
  }

  Widget _buildLayoutWaiting() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: MediaQuery
              .of(context)
              .padding
              .top + 66),
          Container(
            width: 270,
            height: 270,
            child: Stack(
              alignment: Alignment.center,
              children: [
                CustomPaint(
                  painter: SpritePainter(_controller),
                  child: SizedBox(
                    width: 270.0,
                    height: 270.0,
                  ),
                ),
                Positioned(
                  child: Container(
                    width: 80,
                    height: 80,
                    child: CircleAvatar(
                      backgroundImage: NetworkImage(state.userAvatar.value ??
                          "${GlobalConfiguration().getValue(
                              'base_url')}images/image_default.png"),
                      radius: 80 / 2,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(80 / 2),
                      border: Border.all(color: Colors.white, width: 2),
                    ),
                  ),
                )
              ],
            ),
          ),
          Text(
            state.callStatus.value,
            style: TextStyle(
                fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }

  void _startAnimation() {
    _controller.stop();
    _controller.reset();
    _controller.repeat(
      period: Duration(seconds: 1),
    );
  }

  Widget _buildReceiverAvatar() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child:
      (state.isAnswer.value == true && state.isRTCInitRender.value == true)
          ? RTCVideoView(controller.signaling.remoteRenderer,
          objectFit: RTCVideoViewObjectFit.RTCVideoViewObjectFitCover,
          mirror: true,
          filterQuality: FilterQuality.medium)
          : state.userAvatar.value != ""
          ? CachedNetworkImage(
          imageUrl: state.userAvatar.value, fit: BoxFit.cover)
          : Container(
        color: Color(0xffFFB5B5),
      ),
    );
  }

  Widget _buildUserAvatar() {
    return DraggableWidget(
      bottomMargin: 150,
      horizontalSpace: 20,
      topMargin: 50 + MediaQuery
          .of(context)
          .padding
          .top,
      intialVisibility: true,
      child: Container(
          width: 97,
          height: 139,
          padding: EdgeInsets.all(0),
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.white,
              width: 2.0,
            ),
            borderRadius: BorderRadius.circular(10),
            color: Colors.deepPurple.shade100,
          ),
          child: (state.isAnswer.value == true &&
              state.isRTCInitRender.value == true)
              ? ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: RTCVideoView(
              controller.signaling.localRenderer,
              mirror: true,
              objectFit: RTCVideoViewObjectFit.RTCVideoViewObjectFitCover,
            ),
          )
              : ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: CachedNetworkImage(
                imageUrl: currentUser?.value?.image?.url ??
                    "${GlobalConfiguration().getValue(
                        'base_url')}images/image_default.png",
                fit: BoxFit.cover),
          )),
      initialPosition: AnchoringPosition.bottomRight,
      dragController: dragController,
    );
  }

  Widget _buildLayoutTop() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            IconButton(
              iconSize: 37,
              padding: EdgeInsets.all(0),
              onPressed: switchCamera,
              icon: SvgPicture.asset(AppImages.icSwitchCamera),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: 37,
                  padding: EdgeInsets.only(top: 0),
                  child: Text(
                    state.userName.value ?? "",
                    style: TextStyle(
                      overflow: TextOverflow.ellipsis,
                      fontSize: 18,
                      fontWeight: FontWeight.w800,
                      fontFamily: 'Quicksand',
                      color: Colors.white,
                    ),
                    maxLines: 1,
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            IconButton(
              iconSize: 37,
              padding: EdgeInsets.all(0),
              onPressed: null,
              icon: SvgPicture.asset(AppImages.icCallPip),
            ),
          ],
        ),
        state.isAnswer.value
            ? Center(
          child: Text(
            Duration(seconds: state.callDuration.value)
                .toFormatString(separatorSymbol: ":"),
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w500,
              color: AppColors.white,
            ),
            textAlign: TextAlign.center,
          ),
        )
            : Text(""),
      ],
    );
  }

  Widget _buildLayoutBottom() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          iconSize: 37,
          padding: EdgeInsets.all(0),
          onPressed: disableMic,
          icon: state.isMic.value
              ? SvgPicture.asset(AppImages.icMicEnable)
              : SvgPicture.asset(AppImages.icMicDisable),
        ),
        SizedBox(
          width: 37,
        ),
        TapDebouncer(
          onTap: () async {
            await controller.endCall();
          }, // your tap handler moved here
          builder: (BuildContext context, TapDebouncerFunc onTap) {
            return IconButton(
              iconSize: 48,
              padding: EdgeInsets.all(0),
              onPressed: onTap,
              icon: SvgPicture.asset(AppImages.icCallVideoEnd),
            );
          },
        ),
        SizedBox(
          width: 37,
        ),
        IconButton(
          iconSize: 37,
          padding: EdgeInsets.all(0),
          onPressed: disableCamera,
          icon: state.isCamera.value
              ? SvgPicture.asset(AppImages.icCallCameraEnable)
              : SvgPicture.asset(AppImages.icCallCameraDisable),
        ),
      ],
    );
  }

  void buildTimer() {}

  void switchCamera() {
    controller.signaling.switchCamera();
  }

  void disableMic() {
    if (state.isMic.value == false) {
      state.isMic.value = true;
      controller.signaling?.enableMic();
    } else {
      state.isMic.value = false;
      controller.signaling?.disableMic();
    }
  }

  void disableCamera() {
    if (state.isCamera.value == false) {
      state.isCamera.value = true;
      controller.signaling?.enableLocalCamera();
    } else {
      state.isCamera.value = false;
      controller.signaling?.disableLocalCamera();
    }
  }

  @override
  VideoCallController createController() => Get.put(VideoCallController());
}
