import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart' as dio;
import 'package:findnear/src/models/entities/answer_call_entity.dart';
import 'package:findnear/src/models/entities/candidate_call_entity.dart';
import 'package:findnear/src/models/entities/offer_call_entity.dart';
import 'package:findnear/src/models/enums/websocket_event.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/services/websocket_service.dart';
import 'package:findnear/src/utils/listener_event_callkit.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:get/get.dart' as GETX;
import 'package:uuid/uuid.dart';

class Session {
  Session({this.sid, this.pid});

  String pid;
  String sid;
  RTCPeerConnection pc;
  RTCDataChannel dc;
  List<RTCIceCandidate> remoteCandidates = [];
}

enum CallType {
  VIDEO_CALL,
  VOICE_CALL,
}

extension CallTypeExtension on CallType {
  static CallType fromEventName(String type) {
    if (type == "VIDEO_CALL") {
      return CallType.VIDEO_CALL;
    } else if (type == "VOICE_CALL") {
      return CallType.VOICE_CALL;
    }
    return null;
  }

  String get value {
    switch (this) {
      case CallType.VIDEO_CALL:
        return "VIDEO_CALL";
      case CallType.VOICE_CALL:
        return "VOICE_CALL";
    }
  }
}

enum CallData {
  video,
  audio,
}

extension CallDataExtension on CallData {
  static CallData fromEventName(String type) {
    if (type == "video") {
      return CallData.video;
    } else if (type == "audio") {
      return CallData.audio;
    }
    return null;
  }

  String get value {
    switch (this) {
      case CallData.video:
        return "video";
      case CallData.audio:
        return "audio";
    }
  }
}

class CallSignaling extends GETX.GetxService {
  bool isMakeCall = false;
  bool isReceiver = false;
  bool comingActivityOpen = false;

  String userMakeCall = '';
  String userReceiverCall = '';
  String connectedUser = '';
  String currentUuid;
  String userReceiverCallToken;

  RTCPeerConnection rtcPeerConnection;

  OfferCallEntity offerCallEntity = OfferCallEntity();

  String get sdpSemantics =>
      WebRTC.platformIsWindows ? 'plan-b' : 'unified-plan';

  final Map<String, dynamic> _config = {
    'mandatory': {
      'OfferToReceiveAudio': true,
      'OfferToReceiveVideo': true,
    },
    'optional': [
      {'DtlsSrtpKeyAgreement': true},
    ]
  };

  Map<String, dynamic> _iceServers;

  MediaStream onLocalStream;
  MediaStream onRemoteStream;
  MediaStream onRemoveRemoteStream;

  List<MediaStream> _remoteStreams = <MediaStream>[];

  RTCVideoRenderer localRenderer = RTCVideoRenderer();
  RTCVideoRenderer remoteRenderer = RTCVideoRenderer();

  Map<String, Session> sessions = {};

  String sessionId;
  final eventCallKit = ListenerEventCallKit();

  Future<CallSignaling> init() async {
    return this;
  }

  Future<CertIceServerObj> getCertIceServer() async {
    dio.Response response;
    var dioClient = dio.Dio(); // with default Options
    dioClient.options.baseUrl = 'https://api.findnear.vn';
    dioClient.options.connectTimeout = 30000; //30s
    dioClient.options.receiveTimeout = 60000; //60s
    dioClient.options.headers = {
      'Authorization':
          'Bearer ${GETX.Get.find<LocalDataManager>().currentUser?.apiToken}'
    };
    try {
      response = await dioClient.get('/api/user/getCertIceServer');
      _logResponse(response);
      final data = response.data as Map<String, dynamic>;
      final obj = CertIceServerObj.fromJson(data);
      return obj;
    } catch (e, s) {
      logger.e(e);
      return CertIceServerObj();
    }
  }

  void _logResponse(dio.Response response) {
    final statusCode = response.statusCode;
    final uri = Uri.parse("https://api.findnear.vn/api/user/getCertIceServer");
    final data = jsonEncode(response.data);
    logger.log("✅ RESPONSE[$statusCode] => PATH: $uri\n DATA: $data");
  }

  Future<void> connect() async {
    final CertIceServerObj certIceServerObj = await getCertIceServer();
    _iceServers = {
      'iceServers': [
        {
          'urls': 'turn:turn.3t-tech.xyz:3478',
          'username': certIceServerObj.username,
          'credential': certIceServerObj.credential
        },
      ]
    };
  }

  void invite(CallData callData) async {
    sessionId = '$userMakeCall$userReceiverCall';
    Session session = await createSession(null,
        peerId: userReceiverCall,
        sessionId: sessionId,
        media: callData == CallData.video ? 'video' : 'audio');
    sessions[sessionId] = session;
    await createOffer(session, callData);
  }

  Future<MediaStream> createStream(String media) async {
    final Map<String, dynamic> mediaConstraints = {
      'audio': true,
      'video': (media == 'video')
          ? {
              'mandatory': {
                'minWidth':
                    '640', // Provide your own width, height and frame rate here
                'minHeight': '480',
                'minFrameRate': '30',
              },
              'facingMode': 'user',
              'optional': [],
            }
          : false
    };
    MediaStream stream =
        await navigator.mediaDevices.getUserMedia(mediaConstraints);

    onLocalStream = stream;
    localRenderer.srcObject = onLocalStream;

    return stream;
  }

  Future<Session> createSession(Session session,
      {String peerId, String sessionId, String media}) async {
    var newSession = session ?? Session(sid: sessionId, pid: peerId);

    onLocalStream = await createStream(media);
    RTCPeerConnection pc = await createPeerConnection({
      ..._iceServers,
      ...{'sdpSemantics': sdpSemantics}
    }, _config);

    switch (sdpSemantics) {
      case 'plan-b':
        pc.onAddStream = (MediaStream stream) {
          onRemoteStream = stream;
          remoteRenderer.srcObject = onRemoteStream;
          _remoteStreams.add(stream);
        };
        await pc.addStream(onLocalStream);
        break;
      case 'unified-plan':
        // Unified-Plan
        pc.onTrack = (event) {
          if (event.track.kind == 'video') {
            onRemoteStream = event.streams[0];
            remoteRenderer.srcObject = onRemoteStream;
          }
        };
        onLocalStream.getTracks().forEach((track) {
          pc.addTrack(track, onLocalStream);
        });
        break;
    }

    pc.onIceCandidate = (candidate) async {
      if (candidate == null) {
        print('onIceCandidate: complete!');
        return;
      }
      // This delay is needed to allow enough time to try an ICE candidate
      // before skipping to the next one. 1 second is just an heuristic value
      // and should be thoroughly tested in your own environment.
      await Future.delayed(const Duration(seconds: 1), () {
        if (isMakeCall) {
          var map = new Map<String, dynamic>();
          map["action"] = describeEnum(CallType.VIDEO_CALL);
          map["type"] = 'candidate';
          map["candidate"] = {
            'candidate': candidate.candidate,
            'sdpMLineIndex': candidate.sdpMlineIndex,
            'sdpMid': candidate.sdpMid
          };
          map["userName"] = userReceiverCall;
          GETX.Get.find<WebsocketService>()
              .subscribe(WebsocketEvent.MESSAGE.value, map);
        }

        if (isReceiver) {
          var map = new Map<String, dynamic>();
          map["action"] = describeEnum(CallType.VIDEO_CALL);
          map["type"] = 'candidate';
          map["candidate"] = {
            'candidate': candidate.candidate,
            'sdpMLineIndex': candidate.sdpMlineIndex,
            'sdpMid': candidate.sdpMid
          };
          map["userName"] = userMakeCall;
          GETX.Get.find<WebsocketService>()
              .subscribe(WebsocketEvent.MESSAGE.value, map);
        }
      });
    };

    pc.onIceConnectionState = (state) {};

    pc.onRemoveStream = (stream) {
      onRemoveRemoteStream = stream;
      remoteRenderer.srcObject = null;
      _remoteStreams.removeWhere((it) {
        return (it.id == stream.id);
      });
    };
    newSession.pc = pc;
    return newSession;
  }

  Future<void> createOffer(Session session, CallData callData) async {
    try {
      RTCSessionDescription s = await session.pc.createOffer({});
      await session.pc.setLocalDescription(s);
      var map = new Map<String, dynamic>();
      map["action"] = describeEnum(CallType.VIDEO_CALL);
      map["type"] = 'offer';
      map["offer"] = {'sdp': s.sdp, 'type': s.type};
      map["userMakeCall"] = userMakeCall;
      map["userReceiverCall"] = userReceiverCall;
      map["typeCall"] = describeEnum(callData);

      createUuId();
      // khởi tạo cuộc gọi
      var params = <String, dynamic>{
        "messageType": callData.value == CallData.video.value
            ? "call_video"
            : "call_voice",
        'id': currentUuid,
        'nameCaller': currentUser.value.name,
        'action': "call_coming",
        'appName': 'Findnear',
        'avatar': currentUser.value.image.url,
        'timeSend': DateTime.now().millisecondsSinceEpoch,
        'codeCaller': currentUser.value.code,
        'handle': currentUser.value.code,
        'number': currentUser.value.code,
      };
      map["extendData"] = params;

      GETX.Get.find<WebsocketService>()
          .subscribe(WebsocketEvent.MESSAGE.value, map);
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> hanlderOffer(OfferCallEntity offerCallEntity) async {
    try {
      var peerId = offerCallEntity.userMakeCall;
      var description = offerCallEntity.offerEntity;
      var sessionId = '$userMakeCall$userReceiverCall';
      var session = sessions[sessionId];
      var newSession = await createSession(session,
          peerId: peerId,
          sessionId: sessionId,
          media: offerCallEntity.typeCall == 'video' ? 'video' : 'audio');
      sessions[sessionId] = newSession;
      await newSession.pc?.setRemoteDescription(
          RTCSessionDescription(description.sdp, description.type));

      if (newSession.remoteCandidates.length > 0) {
        newSession.remoteCandidates.forEach((candidate) async {
          await newSession.pc?.addCandidate(candidate);
        });
        newSession.remoteCandidates.clear();
      }
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> createAnswer() async {
    try {
      var session = sessions['$userMakeCall$userReceiverCall'];
      RTCSessionDescription s = await session.pc.createAnswer();
      await session.pc.setLocalDescription(s);

      var map = new Map<String, dynamic>();
      map["action"] = describeEnum(CallType.VIDEO_CALL);
      map["type"] = 'answer';
      var answer = new Map<String, dynamic>();
      answer["sdp"] = s.sdp;
      answer["type"] = 'answer';
      map["answer"] = answer;
      map["userMakeCall"] = userMakeCall;
      map["userReceiverCall"] = userReceiverCall;
      GETX.Get.find<WebsocketService>()
          .subscribe(WebsocketEvent.MESSAGE.value, map);
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> handleAnswer(AnswerCallEntity answerCallEntity) async {
    try {
      var description = answerCallEntity.answerEntity;
      var sessionId = '$userMakeCall$userReceiverCall';
      var session = sessions[sessionId];
      await session?.pc?.setRemoteDescription(
          RTCSessionDescription(description.sdp, description.type));
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> handleCandidate(CandidateCallEntity candidateCallEntity) async {
    try {
      var peerId = candidateCallEntity.receiver;
      var candidateMap = candidateCallEntity.candidateEntity;
      var sessionId = '$userMakeCall$userReceiverCall';
      var session = sessions[sessionId];
      RTCIceCandidate candidate = RTCIceCandidate(candidateMap.candidate,
          candidateMap.sdpMid, candidateMap.sdpMLineIndex);

      if (session != null) {
        if (session.pc != null) {
          await session.pc?.addCandidate(candidate);
        } else {
          session.remoteCandidates.add(candidate);
        }
      } else {
        sessions[sessionId] = Session(pid: peerId, sid: sessionId)
          ..remoteCandidates.add(candidate);
      }
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> cleanSessions() async {
    if (onLocalStream != null) {
      onLocalStream.getTracks().forEach((element) async {
        await element.stop();
      });
      await onLocalStream.dispose();
      onLocalStream = null;
    }
    sessions.forEach((key, sess) async {
      await sess.pc?.close();
      await sess.dc?.close();
    });
    sessions.clear();
  }

  Future<void> closeSession(Session session) async {
    onLocalStream.getTracks().forEach((element) async {
      await element.stop();
    });
    await onLocalStream?.dispose();
    onLocalStream = null;

    await session.pc?.close();
    await session.dc?.close();
  }

  close() async {
    await cleanSessions();
  }

  void bye() async {
    // người gọi
    if (isMakeCall) {
      // update lại trạng thái user
      eventCallKit.updateUserStatus(1,userMakeCall, false);
      eventCallKit.userLeaveCall(1, userReceiverCall);
    }
    // người nhận
    if (isReceiver) {
      // update lại trạng thái user
      eventCallKit.updateUserStatus(1,userReceiverCall, false);
      eventCallKit.userLeaveCall(1, userMakeCall);
    }
    isMakeCall = false;
    isReceiver = false;
    comingActivityOpen = false;
    connectedUser = null;
    localRenderer.srcObject = null;
    remoteRenderer.srcObject = null;
    await cleanSessions();
    print("TuanLA - Bye");
  }

  Future<void> handleBye() async {
    // người gọi
    if (isMakeCall) {
      // update lại trạng thái user
      eventCallKit.updateUserStatus(1,userMakeCall, false);
    }
    // người nhận
    if (isReceiver) {
      // update lại trạng thái user
      eventCallKit.updateUserStatus(1,userReceiverCall, false);
    }
    isMakeCall = false;
    isReceiver = false;
    comingActivityOpen = false;
    connectedUser = null;
    localRenderer.srcObject = null;
    remoteRenderer.srcObject = null;
    await cleanSessions();
    print("TuanLA - handleBye");
  }

  void initRenderers() async {
    disposeRenderers();
    localRenderer = RTCVideoRenderer();
    remoteRenderer = RTCVideoRenderer();
    await localRenderer.initialize();
    await remoteRenderer.initialize();
  }

  void disposeRenderers() {
    localRenderer.dispose();
    remoteRenderer.dispose();
  }

  void switchCamera() {
    if (onLocalStream != null) {
      Helper.switchCamera(onLocalStream.getVideoTracks()[0]);
    }
  }

  void enableMic() {
    try {
      if (onLocalStream != null) {
        onLocalStream.getAudioTracks()[0].enabled = true;
      }
    } catch (e) {
      print("TuanLA - enableMic -$e");
    }
  }

  void disableMic() {
    try {
      if (onLocalStream != null) {
        onLocalStream.getAudioTracks()[0].enabled = false;
      }
    } catch (e) {
      print("TuanLA - disableMic -$e");
    }
  }

  void enableSpeaker() {
    try {
      if (onLocalStream != null) {
        onLocalStream.getAudioTracks().first.enableSpeakerphone(true);
      }
    } catch (e) {
      print("TuanLA - enableSpeaker -$e");
    }
  }

  void disableSpeaker() {
    try {
      if (onLocalStream != null) {
        onLocalStream.getAudioTracks().first.enableSpeakerphone(false);
      }
    } catch (e) {
      print("TuanLA - disableSpeaker -$e");
    }
  }

  void enableLocalCamera() {
    try {
      if (onLocalStream != null) {
        onLocalStream.getVideoTracks()[0].enabled = true;
      }
    } catch (e) {
      print("TuanLA - enableLocalCamera -$e");
    }
  }

  void disableLocalCamera() {
    try {
      if (onLocalStream != null) {
        onLocalStream.getVideoTracks()[0].enabled = false;
      }
    } catch (e) {
      print("TuanLA - disableLocalCamera -$e");
    }
  }

  void createUuId() {
    currentUuid = Uuid().v4();
  }

  String getCreateUuId() {
    return this.currentUuid;
  }
}

class CertIceServerObj {
  CertIceServerObj({
    this.username,
    this.credential,
  });

  String username;
  String credential;

  factory CertIceServerObj.fromJson(Map<String, dynamic> json) =>
      CertIceServerObj(
        username: json["username"],
        credential: json["credential"],
      );

  Map<String, dynamic> toJson() => {
        "username": username,
        "credential": credential,
      };
}
