import 'dart:async';
import 'dart:io';

import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/global/global_data.dart';
import 'package:findnear/src/global/global_event.dart';
import 'package:findnear/src/models/entities/message_chat/entities/message_call.dart';
import 'package:findnear/src/models/entities/message_chat/enums/message_call_type.dart';
import 'package:findnear/src/models/search_user_entity.dart';
import 'package:findnear/src/pages/videocall/state.dart';
import 'package:findnear/src/repository/market_repository.dart';
import 'package:findnear/src/repository/message_repository.dart';
import 'package:findnear/src/repository/notification_v2_repository.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/repository/user_v2_repository.dart';
import 'package:findnear/src/services/audio_app_services.dart';
import 'package:findnear/src/utils/listener_event_callkit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';
import 'package:get/get.dart';

import 'callsignaling.dart';

class VideoCallController extends BaseController<VideoCallState> {
  CallSignaling signaling = Get.find<CallSignaling>();
  AudioAppService audioAppService = Get.find<AudioAppService>();
  MessageRepository messageRepository = Get.find<MessageRepository>();
  NotificationV2Repository notificationV2Repository =
      Get.find<NotificationV2Repository>();
  UserV2Repository userV2Repository = Get.find<UserV2Repository>();
  final eventCallKit = ListenerEventCallKit();

  @override
  VideoCallState createState() => VideoCallState();
  StreamSubscription _endCallFromSocketSubscription;
  StreamSubscription _onCandidateCallSubscription;
  StreamSubscription _answerCallSubscription;
  StreamSubscription _readyCallSubscription;

  void initialize() async {
    try {
      await resetAudioCache();
      // connect listen socket
      _listenEndCallFromSocket();
      _listenCandidateCallFromSocket();
      _listenAnswerCallFromSocket();
      _listenReadyCallFromSocket();
    } catch (e) {}
  }

  void connectWebrtc() async {
    await signaling?.connect();
    await signaling?.initRenderers();
    state.isRTCInitRender.value = true;
  }
  
  void getUserStatus(String code) async {
    state.statusUserCall = await userV2Repository.getUserStatus(code);
  }

  void acceptCallFromCallKit() async {
    state.isAnswer.value = false;
    await connectWebrtc();
    signaling?.userReceiverCall = currentUser.value.code;
    signaling?.userMakeCall = state.offerCallEntity.userMakeCall;
    signaling?.isReceiver = true;
    await signaling?.hanlderOffer(state.offerCallEntity);
    await signaling?.createAnswer();
    await resetAudioCache();
    state.isAnswer.value = true;
    state.isMic.value = true;
    state.isCamera.value = true;
    signaling.enableMic();
    signaling.enableLocalCamera();
    signaling.enableSpeaker();
    startTimer();
  }

  void createCall() async {
    try {
      // khởi tạo cuộc gọi
      signaling.isMakeCall = true;
      signaling.userMakeCall = currentUser.value.code;
      signaling.connectedUser = currentUser.value.code;
      signaling.userReceiverCall = state.targetUser.code;
      await signaling?.invite(CallData.video);

      // bắn socket update trạng thái đang trong cuộc gọi
      eventCallKit.updateUserStatus(1, currentUser.value.code, true);
    } catch (e) {
      print('TuanLA - $e');
    }
  }

  @override
  void onClose() async {
    closeWebrtc();
    await resetAudioCache();
    closeSubscription();
    super.onClose();
  }

  void closeWebrtc() async {
    // dispose webrtc
    await signaling.close();
    await signaling.disposeRenderers();
    state.isRTCInitRender.value = false;
  }

  void closeSubscription() {
    _endCallFromSocketSubscription?.cancel();
    _onCandidateCallSubscription?.cancel();
    _answerCallSubscription?.cancel();
  }

  void searchUserFromCode(String code) async {
    state.isLoading = true;
    List<SearchUserEntity> listUser = <SearchUserEntity>[];

    final Stream<SearchUserEntity> stream = await getUserFromCode(code: code);
    stream.listen(
      (SearchUserEntity _market) {
        listUser.add(_market);
      },
      onError: (a) {
        state.isLoading = false;
      },
      onDone: () {
        listUser.forEach((element) {
          state.userName.value = element.name;
          state.userAvatar.value = element.media[0].thumb;
        });

        state.isLoading = false;
      },
    );
  }

  void startTimer() {
    state.callDuration.value = 0;
    state.timer?.cancel();
    state.timer = Timer.periodic(const Duration(seconds: 1), (Timer t) {
      state.callDuration.value += 1;
    });
  }

  void _listenEndCallFromSocket() async {
    await resetAudioCache();
    _endCallFromSocketSubscription?.cancel();
    _endCallFromSocketSubscription =
        GlobalEvent.instance.onEndVideoCall.stream.listen((data) async {
      state.isTimeOut.value = false;
      if (signaling.isMakeCall) {
        try {
          if (state.isAnswer.value) {
            // khi đã có sự kiện trả lời -> truyền time cuộc gọi
            sendMessage(state.callDuration.value, MessageCallStateType.accept);
            state.isAnswer.value = false;
            state.callStatus.value = "Kết thúc";
          } else {
            // khi chưa có sự kiện trả lời
            sendMessage(0, MessageCallStateType.cancel);
            state.callStatus.value = "Người nhận từ chối cuộc gọi";
          }
        } catch (e) {
          print("TuanLA - end call $e");
        }

        state.isMic.value = false;
        state.isCamera.value = false;
        signaling.disableMic();
        signaling.disableLocalCamera();
        signaling.disableSpeaker();
        await signaling.handleBye();
        if(Platform.isIOS){
          GlobalData.instance.offer = null;
          FlutterCallkitIncoming.endAllCalls();
        }
        _endCallFromSocketSubscription?.cancel();
        GlobalEvent.instance.onCloseOverlayCall.add(true);
      } else {
        state.isAnswer.value = false;
        state.callStatus.value = "Kết thúc";
        state.isMic.value = false;
        state.isCamera.value = false;
        signaling.disableMic();
        signaling.disableLocalCamera();
        signaling.disableSpeaker();
        closeSubscription();
        await signaling.handleBye();
        if(Platform.isIOS){
          GlobalData.instance.offer = null;
          FlutterCallkitIncoming.endAllCalls();
        }
        GlobalEvent.instance.onCloseOverlayCall.add(true);
      }
    });
  }

  void _listenCandidateCallFromSocket() {
    _onCandidateCallSubscription?.cancel();
    _onCandidateCallSubscription =
        GlobalEvent.instance.onCandidateCall.stream.listen((data) async {
      await signaling.handleCandidate(data);
    });
  }

  void _listenAnswerCallFromSocket() async {
    _answerCallSubscription?.cancel();
    _answerCallSubscription =
        GlobalEvent.instance.onAnswerCall.stream.listen((data) async {
      await resetAudioCache();
      await signaling?.handleAnswer(data);
      state.isAnswer.value = true;
      state.isMic.value = true;
      state.isCamera.value = true;
      signaling.enableMic();
      signaling.enableLocalCamera();
      signaling.enableSpeaker();
      startTimer();
    });
  }

  void _listenReadyCallFromSocket() {
    _readyCallSubscription?.cancel();
    _readyCallSubscription =
        GlobalEvent.instance.onReadyCall.stream.listen((data) async {
      try {
        final userMakeCall = data.userMakeCall;
        if (userMakeCall == currentUser.value.code) {
          if (state.isMakeCall.value == true) {
            if (state.statusUserCall.isCalling == true) {
              state.callStatus.value = "Người dùng bận";
              eventCallKit.updateUserStatus(1, currentUser.value.code, false);
              await resetAudioCache();
              GlobalEvent.instance.onCloseOverlayCall.add(true);
            } else {
              state.callStatus.value = "Đang đổ chuông";
              if (GlobalData.instance.isCallShowing == true) {
                await createCall();
              }
            }
          }
        }
      } catch (e) {
        print("NamNH - _listenReadyCallFromSocket - error: $e");
      }
    });
  }

  void endCall() async {
    try {
      GlobalData.instance.isCallShowing = false;
      await resetAudioCache();
      state.isTimeOut.value = false;

      if(state.statusUserCall.devicePlatform == "IOS"){
        Get.find<MessageRepository>().sendMakeCallNotification(
          state.targetUser.code,
          "1e74092d-61a4-4e09-ad41-1adfa0609c5e",
          "END_CALL",
        );
      }

      if (signaling.isMakeCall) {
        // người gọi
        if (state.isAnswer.value) {
          sendMessage(state.callDuration.value, MessageCallStateType.accept);
        } else {
          sendMessage(0, MessageCallStateType.cancel);
        }
      }
      eventCallKit.updateUserStatus(1, currentUser.value.code, false);
      await signaling?.bye();
      state.callStatus.value = "Kêt thúc";
      state.isAnswer.value = false;
      state.isMic.value = false;
      state.isCamera.value = false;
      signaling.disableMic();
      signaling.disableLocalCamera();
      signaling.disableSpeaker();

      if(Platform.isIOS){
        GlobalData.instance.offer = null;
        FlutterCallkitIncoming.endAllCalls();
      }

    } catch (e) {
      print("TuanLA -$e");
    }
    GlobalEvent.instance.onCloseOverlayCall.add(true);
  }

  void delayCall(BuildContext context) {
    Future.delayed(Duration(seconds: 60)).then((value) async {
      if (state.isTimeOut.value && state.isAnswer.value == false) {
        // khi hết thời gian chờ nhận cuộc gọi
        sendMessage(0, MessageCallStateType.cancel);
        if(state.statusUserCall.devicePlatform == "IOS"){
          Get.find<MessageRepository>().sendMakeCallNotification(
            state.targetUser.code,
            "1e74092d-61a4-4e09-ad41-1adfa0609c5e",
            "END_CALL",
          );
        }
        eventCallKit.updateUserStatus(1, currentUser.value.code, false);
        await resetAudioCache();
        await signaling.bye();
        state.callStatus.value = "Người nhận không bắt máy";
        state.isAnswer.value = false;
        state.isMic.value = false;
        state.isCamera.value = false;
        signaling.disableMic();
        signaling.disableLocalCamera();
        signaling.disableSpeaker();

        GlobalEvent.instance.onCloseOverlayCall.add(true);
      }
    });
  }

  void sendMessage(int time, MessageCallStateType type) {
    messageRepository.sendCallMessage(
      MessageCall(
          messageCallType: MessageCallType.video,
          messageCallStateType: type,
          duration: Duration(seconds: time)),
      state.conversation.value,
    );
  }

  void resetAudioCache() async {
    try {
      await audioAppService.audioCacheCallOut.clearAll();
      await audioAppService.advancedPlayerCallOut.stop();
    } catch (e) {
      print("TuanLA - resetAudioCache - $e");
    }
  }

  void loadAudio() {
    try {
      audioAppService.loadAudioCallOut();
    } catch (e) {
      print("TuanLA -loadAudio - $e");
    }
  }
}
