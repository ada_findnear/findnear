import 'dart:async';

import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/entities/offer_call_entity.dart';
import 'package:findnear/src/models/entities/state_usercall.dart';
import 'package:get/get.dart';

class VideoCallState extends BaseState{
  RxBool isMic = true.obs;
  RxBool isCamera = true.obs;
  RxBool isAnswer = false.obs; // Có cuộc gọi đến
  final conversation = Rxn<Conversation>();
  RxBool isTimeOut = false.obs;
  RxBool isMakeCall = false.obs;

  RxString userName = "".obs;
  RxString userAvatar = "".obs;
  RxString callStatus = "".obs;
  RxBool isFromCallkit = false.obs;
  RxBool isRTCInitRender = false.obs;


  Timer timer;
  final callDuration = 0.obs;

  ShortUserEntity targetUser;
  OfferCallEntity offerCallEntity;
  StateUserCall statusUserCall;

  VideoCallState() {}

}