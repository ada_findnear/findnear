import 'dart:io';
import 'package:findnear/src/controllers/chat_controller.dart';
import 'package:findnear/src/controllers/group_controller.dart';
import 'package:findnear/src/helpers/loadmore_text_builder.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/preview_media.dart';
import 'package:image_picker/image_picker.dart';
import 'package:loadmore/loadmore.dart';
import '../elements/MediaActionSheetWidget.dart';
import '../../generated/l10n.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class GroupAddUserWidget extends StatefulWidget {
  final Conversation conversation;
  final String memberGroupChat;
  GroupAddUserWidget({Key key, this.conversation, this.memberGroupChat}) : super(key: key);

  @override
  _GroupAddUserWidgetState createState() => _GroupAddUserWidgetState(this.memberGroupChat);
}

class _GroupAddUserWidgetState extends StateMVC<GroupAddUserWidget> {
  MediaActionSheetWidget _mediaActionSheetWidget;
  String pathImage;
  String nameGroup = '';
  String keyword = '';
  GroupController _groupController;
  ChatController _chatController;
  List<User> newMembers = [];
  var _textController = TextEditingController();

  _GroupAddUserWidgetState(String memberGroupChat) : super(GroupController(ignoreId: memberGroupChat)) {
    _groupController = controller;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _chatController = new ChatController();
  }

  @override
  Widget build(BuildContext context) {
    _mediaActionSheetWidget = MediaActionSheetWidget(context: context);
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios_outlined, color: Theme.of(context).hintColor),
            onPressed: () {
              Navigator.of(context).pop(false);
            },
          ),
          centerTitle: true,
          title: Text(S.of(context).addMemberToGroup),
          actions: <Widget>[
            newMembers.length > 0 ? TextButton(
              onPressed: () {
                _chatController.addUser(conversation: widget.conversation, listUser: newMembers);
                int count = 0;
                Navigator.of(context).popUntil((_) => count++ >= 2);
              },
              child: Text(S.of(context).add),
            ) : SizedBox(height: 0,),
          ],
        ),
        body: SizedBox.expand(
          child: Container(
            child: Column(
              children: <Widget>[
                editTextSearch(),
                Expanded(
                  child: SingleChildScrollView(
                    padding: EdgeInsets.only(left: 10, right: 10),
                    child: Column(
                      children: [
                        getListFriend(),
                      ],
                    ),
                  )
                ),
                Align(
                  alignment: Alignment.bottomLeft,
                  child: newMembers.length > 0 ? showAddedMembers() : SizedBox(),
                )
              ],
            ),
          ),
        )
    );
  }

  Widget editTextSearch() {
    return Container(
      padding: EdgeInsets.only(bottom: 20),
      child: TextField(
        style: TextStyle(
          height: 1.0,
        ),
        controller: _textController,
        decoration: InputDecoration(
          focusedBorder: OutlineInputBorder(
            borderSide: new BorderSide(color: Colors.transparent, width: 0),
          ),
          enabledBorder: const OutlineInputBorder(
            // width: 0.0 produces a thin "hairline" border
            borderSide: const BorderSide(color: Colors.black12, width: 1.0),
          ),
          border: OutlineInputBorder(),
          contentPadding: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
          hintText: S.of(context).searchName,
          hintStyle: TextStyle(fontSize: 16.0, color: Theme.of(context).hintColor),
          fillColor: Theme.of(context).primaryColor,
          filled: true,
          prefixIcon: Icon(Icons.search),
        ),
        onChanged: (value) async {
          keyword = value != null ? value : '';
          await _groupController.setState(() {
            _groupController.currentOffset = 0;
            _groupController.canLoadMore = true;
          });
          await _groupController.listenForFriend(keyword: value, isClearData: true);
        },
      ),
    );
  }

  Widget getListFriend() {
    return LoadMore(
        isFinish: !_groupController.canLoadMore,
        child: ListView.separated(
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          itemBuilder: (BuildContext context, int index) {
            return CheckboxListTile(
              activeColor: Colors.pink[300],
              dense: true,
              onChanged: (bool val) {
                if (newMembers.indexWhere((element) => element.id == _groupController.friends.elementAt(index).id) == -1) {
                  setState(() {
                    newMembers.add(_groupController.friends.elementAt(index));
                  });
                } else {
                  setState(() {
                    newMembers.removeWhere((item) => item.id == _groupController.friends.elementAt(index).id);
                  });
                }
              },
              title: ListTile(
                contentPadding: EdgeInsets.all(0),
                leading: CircleAvatar(
                  foregroundImage: getImage(_groupController.friends.elementAt(index).image.thumb),
                ),
                title: Text(_groupController.friends.elementAt(index).name),
              ),
              value: newMembers.indexWhere((element) => element.id == _groupController.friends.elementAt(index).id) != -1 ? true : false,
              controlAffinity: ListTileControlAffinity.leading,
            );
          },
          separatorBuilder: (BuildContext context, int index) => const Divider(),
          itemCount: _groupController.friends.length,
        ),
        onLoadMore: _loadMore,
        whenEmptyLoad: false,
        delegate: DefaultLoadMoreDelegate(),
        textBuilder: CustomLoadMoreTextBuilder.getText,
    );
  }

  Future<bool> _loadMore() async {
    await Future.delayed(Duration(seconds: 2, milliseconds: 1000));
    await _groupController.listenForFriend(keyword: keyword, ignoreId: widget.memberGroupChat);
    return true;
  }

  Widget showAddedMembers() {
    return Container(
      height: 60,
      padding: EdgeInsets.only(left: 10),
      child: ListView.separated(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            width: 67,
            padding: EdgeInsets.only(right: 20),
            child: Stack(
              children: [
                CircleAvatar(
                  foregroundImage: getImage(newMembers.elementAt(index).image.thumb),
                ),
                Positioned(
                  top: -18,
                  left: 18,
                  child: IconButton(
                    onPressed: () {
                      setState(() {
                        newMembers.removeWhere((item) => item.id == newMembers.elementAt(index).id);
                      });
                    },
                    icon: Icon(Icons.close_outlined),
                    iconSize: 15,
                  ),
                )
              ],
            ),
          );
        },
        separatorBuilder: (BuildContext context, int index) => const Divider(),
        itemCount: newMembers.length,
      ),
    );
  }

  ImageProvider getImage(url) {
    if (url.contains("http")) {
      return NetworkImage(url);
    } else {
      return FileImage(File(url));
    }
  }
}