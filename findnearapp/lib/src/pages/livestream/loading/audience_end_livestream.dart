import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/pages/livestream/model/enum/ls_broadcast_type.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AudienceEndLivestream extends StatelessWidget {
  LSBroadcastPhase phase;

  AudienceEndLivestream({this.phase});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.mediaQuery.size.width,
      height: Get.mediaQuery.size.height,
      color: AppColors.gray,
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 70,
              height: 70,
              child: phase == LSBroadcastPhase.startEnd || phase == LSBroadcastPhase.startLeft ?
                CircularProgressIndicator(color: AppColors.white) :
                Image.asset(AppImages.icLsEnd),
            ),
            SizedBox(height: 21),
            Text(
              getDesctiption(),
              textAlign: TextAlign.center,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontFamily: 'Quicksand',
                fontSize: 14,
                color: AppColors.white,
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
    );
  }


  String getDesctiption(){
    switch(this.phase){
      case LSBroadcastPhase.startEnd:
        return "Đang kết thúc buổi phát trực tiếp";
      case LSBroadcastPhase.ended:
        return "Buổi phát trực tiếp đã kết thúc";
      case LSBroadcastPhase.ended:
        return "Buổi phát trực tiếp đã kết thúc";
      case LSBroadcastPhase.startLeft:
        return "Đang rời khỏi buổi phát trực tiếp";
      case LSBroadcastPhase.left:
        return "Đã rời khỏi buổi phát trực tiếp";
      default:
        return "";
    }
  }


}