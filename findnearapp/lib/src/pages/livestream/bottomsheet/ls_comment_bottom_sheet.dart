import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/pages/livestream/bottomsheet/comment/ls_comment_box_view.dart';
import 'package:findnear/src/pages/livestream/presenter/comment_presenter.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'invite_friend_list/invite_friend_list_view.dart';

class LSCommentBottomSheet {
  static void showCommentBox({
    Function sendMessage,
    CommentPresenter replyingComment,
    Function onCancelReplyingComment,
  }) async {
    final result = await Get.bottomSheet(
      LSCommentBoxWidget(
        sendMessage: sendMessage,
        replyingComment: replyingComment,
        onCancelReplyingComment: onCancelReplyingComment,
      ),
    );
  }
}
