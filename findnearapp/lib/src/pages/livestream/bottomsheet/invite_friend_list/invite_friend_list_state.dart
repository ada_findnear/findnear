import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/user.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart' as refresh;

class InviteFriendListState extends BaseState{
  RxBool enableDoneButtonRx = false.obs;
  List<User> friends = [];
  Rxn<List<User>> selectFriends = Rxn<List<User>>([]);
  Rx<LoadStatus> fetchFriendStatusRx = LoadStatus.initial.obs;
  Rx<LoadStatus> selectFriendStatusRx = LoadStatus.initial.obs;
  // loadmore
  final canLoadmore = true.obs;
  final refreshController = refresh.RefreshController();

  InviteFriendListState() {
    ///Initialize variables
  }

  bool isUserSelected(User user) {
    if(selectFriends.value.isEmpty ?? true) return false;
    int index = selectFriends.value.indexWhere((element) => element.id == user.id);
    if(index != -1)
      return true;
    return false;
  }
}
