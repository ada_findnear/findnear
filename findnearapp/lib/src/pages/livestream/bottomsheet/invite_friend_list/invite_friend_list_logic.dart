import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/repository/communication_repository.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'invite_friend_list_state.dart';

class InviteFriendListLogic extends BaseController<InviteFriendListState> {
  CommunicationRepository _repository = CommunicationApi(isSocketApi: false);

  @override
  InviteFriendListState createState() => InviteFriendListState();

  void fetchFriend({String keyword = '', bool isReload = false}) async {
    try {
      var offset = state.friends.length;

      if (isReload) {
        state.fetchFriendStatusRx.value = LoadStatus.loading;
        offset = 0;
        state.canLoadmore.value = true;
        state.friends = [];
      }

      if (state.canLoadmore.value != true) return;
      final response = await _repository.searchFriend(
        keyword: keyword,
        currentOffset: offset,
      );

      if (response.isNotEmpty == true) {
        state.friends = state.friends..addAll(response);
        // state.friends.refresh();
      } else {
        state.canLoadmore.value = false;
      }
      state.fetchFriendStatusRx.value = LoadStatus.success;
      state.refreshController.loadComplete();
    } catch (e){
      state.fetchFriendStatusRx.value == LoadStatus.failure;
    }

  }

  void selectFriend({User user}) async {
    state.selectFriendStatusRx.value = LoadStatus.loading;
    try {
      if(state.selectFriends.value.isNotEmpty ?? false){
        int index = state.selectFriends.value.indexWhere((element) => element.id == user.id);
        if(index == -1)
          state.selectFriends.value.add(user);
      } else
        state.selectFriends.value.add(user);
      state.selectFriends.refresh();
      state.selectFriendStatusRx.value = LoadStatus.success;
      Fluttertoast.showToast(
        msg: "select, size = ${state.selectFriends.value.length}",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 5,
      );
    } catch (e){
      state.selectFriendStatusRx.value = LoadStatus.failure;
    }
  }

  void unSelectFriend({User user}) async {
    state.selectFriendStatusRx.value = LoadStatus.loading;
    try {
      if(state.selectFriends.value.isNotEmpty ?? false){
        int index = state.selectFriends.value.indexWhere((element) => element.id == user.id);
        if(index != -1)
          state.selectFriends.value.removeAt(index);
      }
      state.selectFriends.refresh();
      state.selectFriendStatusRx.value = LoadStatus.success;
      Fluttertoast.showToast(
        msg: "unselect, size = ${state.selectFriends.value.length}",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 5,
      );
    } catch (e){
      state.selectFriendStatusRx.value = LoadStatus.failure;
    }
  }
}
