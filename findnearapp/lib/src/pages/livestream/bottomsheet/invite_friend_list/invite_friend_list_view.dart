import 'dart:io';

import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/pages/livestream/bottomsheet/invite_friend_list/invite_friend_list_logic.dart';
import 'package:findnear/src/pages/livestream/bottomsheet/invite_friend_list/invite_friend_list_state.dart';
import 'package:findnear/src/pages/messages/create_conversation/loading/create_conversation_loading_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart' as refresh;

import '../../../../../main.dart';
import 'item/invite_friend_item_widget.dart';

class InviteFriendListPage extends StatefulWidget {
  VoidCallback onAddFriendPressed;

  InviteFriendListPage({this.onAddFriendPressed});

  @override
  State<InviteFriendListPage> createState() => _InviteFriendListPageState();
}

class _InviteFriendListPageState extends BaseWidgetState<InviteFriendListPage,
    InviteFriendListLogic, InviteFriendListState> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      controller.fetchFriend(keyword: "", isReload: true);
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _buildHeader(),
            Expanded(
              child: Obx(() {
                return _buildListFriends();
              }),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Obx(() {
                return _buildSelectedFriends();
              }),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildHeader() {
    return Stack(
      children: [
        Container(
          child: Column(
            children: [
              SizedBox(height: 5),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Text(
                      "Mời bạn bè vào buổi trực tiếp của bạn",
                      style: TextStyle(
                        fontSize: 16,
                        fontFamily: "Quicksand",
                        color: AppColors.black,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 4,
              ),
              Text(
                "Họ sẽ được mời tham gia cùng bạn sau khi bạn phát trực tiếp",
                style: TextStyle(
                  fontSize: 10,
                  fontFamily: "Quicksand",
                  color: AppColors.black,
                  fontWeight: FontWeight.normal,
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                width: Get.mediaQuery.size.width,
                height: 2,
                color: Color(0xffFFEAEA),
              )
            ],
          ),
        ),
        Positioned(
          top: 0,
          right: 13,
          child: GestureDetector(
            onTap: () {
              Get.back();
            },
            child: Container(
                height: 28,
                width: 28,
                color: Colors.white,
                child: Image.asset(AppImages.icClose)),
          ),
        ),
      ],
    );
  }

  Widget _buildListFriends() {
    if (state.fetchFriendStatusRx.value == LoadStatus.loading) {
      return MessagesLoadingWidget();
    } else if (state.fetchFriendStatusRx.value == LoadStatus.success ||
        state.selectFriendStatusRx.value == LoadStatus.success) {
      if (state.friends.isEmpty)
        return Center(child: Text(S.of(context).no_result));
      return refresh.SmartRefresher(
        controller: state.refreshController,
        onLoading: () => controller.fetchFriend(keyword: ""),
        enablePullUp: state.canLoadmore.value,
        enablePullDown: false,
        child: ListView.separated(
          itemBuilder: (context, index) {
            return InviteFriendItemWidget(
              onChanged: (bool val) {
                if (val)
                  controller.selectFriend(user: state.friends[index]);
                else
                  controller.unSelectFriend(user: state.friends[index]);
              },
              avatar: state.friends.elementAt(index).image.thumb,
              name: state.friends.elementAt(index).name,
              value: state.isUserSelected(state.friends[index]),
            );
          },
          itemCount: state.friends.length,
          separatorBuilder: (_, __) => Divider(
            height: 10,
            color: AppColors.dividerColor,
            thickness: 1,
          ),
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
        ),
      );
    }
    return Center(child: Text(S.of(context).no_result));
  }

  Widget _buildSelectedFriends() {
    if (state.selectFriends.value?.isNotEmpty ?? false)
      return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        height: 60,
        width: Get.mediaQuery.size.width,
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Row(
          children: [
            Expanded(
              child: ListView.separated(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    width: 40,
                    child: Stack(
                      children: [
                        CircleAvatar(
                          foregroundImage: getImage(state.selectFriends.value
                              .elementAt(index)
                              .image
                              .thumb),
                        ),
                        Positioned(
                          top: -18,
                          left: 10,
                          child: IconButton(
                            onPressed: () {
                              controller.unSelectFriend(
                                  user: state.selectFriends.value
                                      .elementAt(index));
                            },
                            icon: Image.asset(AppImages.icAddMemberClose),
                            iconSize: 15,
                          ),
                        )
                      ],
                    ),
                  );
                },
                separatorBuilder: (BuildContext context, int index) =>
                    Container(width: 10),
                itemCount: state.selectFriends.value.length,
              ),
            ),
            SizedBox(width: 10),
            GestureDetector(
              onTap: () {
                //controller.createLiveStream();
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: AppColors.red),
                child: Text(
                  "Mời",
                  textAlign: TextAlign.left,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 12,
                    color: AppColors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    return Container();
  }

  ImageProvider getImage(url) {
    if (url.contains("http")) {
      return NetworkImage(url);
    } else {
      return FileImage(File(url));
    }
  }

  @override
  InviteFriendListLogic createController() => InviteFriendListLogic();
}
