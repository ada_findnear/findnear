import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/pages/livestream/bottomsheet/comment/ls_comment_box_logic.dart';
import 'package:findnear/src/pages/livestream/bottomsheet/comment/ls_comment_box_state.dart';
import 'package:findnear/src/pages/livestream/presenter/comment_presenter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LSCommentBoxWidget extends StatefulWidget {
  Function sendMessage;
  CommentPresenter replyingComment;
  Function onCancelReplyingComment;

  LSCommentBoxWidget(
      {this.sendMessage, this.replyingComment, this.onCancelReplyingComment});

  @override
  State<LSCommentBoxWidget> createState() => _LSCommentBoxState();
}

class _LSCommentBoxState extends BaseWidgetState<LSCommentBoxWidget,
    LSCommentBoxLogic, LSCommentBoxState> {
  final TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Container(
        padding: EdgeInsets.symmetric(horizontal: 12),
        color: Colors.white,
        width: Get.mediaQuery.size.width,
        height: widget.replyingComment == null ? 60 : 100,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(child: _buildParentMessage()),
            Visibility(
              visible: (widget.replyingComment != null),
              child: Container(
                margin: EdgeInsets.only(top: 8),
                height: 1,
                width: Get.mediaQuery.size.width,
                color: Color(0xffFFEAEA),
              ),
            ),
            Flexible(
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      controller: _controller,
                      onSubmitted: (text) {
                        widget.sendMessage(text, widget.replyingComment);
                        Get.back();
                      },
                      onChanged: (String text) {
                        state.enableSendButtonRx.value =
                            (text?.isNotEmpty ?? false);
                      },
                      autofocus: true,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Nhập bình luận...",
                          hintStyle: Theme.of(context)
                              .textTheme
                              .caption
                              .merge(TextStyle(fontSize: 16))),
                      maxLines: 1,
                      style: Theme.of(context)
                          .textTheme
                          .caption
                          .merge(TextStyle(fontSize: 16, color: Colors.black)),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                    width: 40,
                    height: 40,
                    child: GestureDetector(
                      onTap: () {
                        if (state.enableSendButtonRx.value){
                          widget.sendMessage(
                              _controller.text, widget.replyingComment);
                          Get.back();
                        }
                      },
                      child: Image.asset(AppImages.icChatSend,
                          color: state.enableSendButtonRx.value
                              ? Theme.of(context).accentColor
                              : Colors.grey),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    });
  }

  Widget _buildParentMessage() {
    if (widget.replyingComment == null) {
      return Visibility(
        visible: (widget.replyingComment != null),
        child: Container(
          color: Colors.red,
        ),
      );
    } else {
      return Container(
        color: Colors.white,
        child: Row(
          children: [
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Trả lời:",
                    style: Theme.of(Get.context)
                        .textTheme
                        .bodyText1
                        .copyWith(fontSize: 14, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "${widget.replyingComment.text}",
                    maxLines: 1,
                    style: Theme.of(Get.context).textTheme.bodyText1.copyWith(
                        fontSize: 14, fontWeight: FontWeight.normal),
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            ),
            Container(
              child: GestureDetector(
                onTap: widget.onCancelReplyingComment,
                child: Image.asset(AppImages.icClose)),
            ),
          ],
        ),
      );
    }
  }

  @override
  LSCommentBoxLogic createController() => LSCommentBoxLogic();
}
