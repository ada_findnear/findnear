import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/livestream/bottomsheet/comment/ls_comment_box_state.dart';
import 'package:findnear/src/repository/communication_repository.dart';
import 'package:fluttertoast/fluttertoast.dart';

class LSCommentBoxLogic extends BaseController<LSCommentBoxState> {
  CommunicationRepository _repository = CommunicationApi(isSocketApi: false);

  @override
  LSCommentBoxState createState() => LSCommentBoxState();

}
