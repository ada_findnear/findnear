import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'invite_friend_list/invite_friend_list_view.dart';

class LSInviteFriendBottomSheet {
  static void showCreateConversationMenu({
    VoidCallback onAddFriendPressed,
  }) async {
    final result = await Get.bottomSheet(Container(
      padding: EdgeInsets.only(top: 10, bottom: 0),
      decoration: BoxDecoration(
          color: Theme.of(Get.context).scaffoldBackgroundColor,
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(20),
            topLeft: Radius.circular(20),
          )),
      child: InviteFriendListPage(
          onAddFriendPressed: onAddFriendPressed
      ),
    ));
    if (result is String) {
      if (result == "add_friend") {
        onAddFriendPressed?.call();
      }
    }
  }
}
