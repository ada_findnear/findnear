import 'package:findnear/src/models/user.dart';

class LSJoinEntity {
  int id;
  int userId;
  String description;
  String status;
  String endAt;
  String createdAt;
  String updatedAt;
  String deletedAt;
  List<User> viewers = [];

  LSJoinEntity({
    this.id,
    this.userId,
    this.description,
    this.status,
    this.endAt,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
    this.viewers,
  });

  factory LSJoinEntity.fromJSON(Map<String, dynamic> json) {
    List<User> list = [];
    try {
      list = List<User>.from(json["viewers"]
          .map((x) => User.fromJSON(x)));
    } catch (e){
      list = [];
    }
    return LSJoinEntity(
      id: json["id"] ?? 0,
      userId: json["user_id"] ?? 0,
      description: json["description"] ?? "",
      status: json["status"] ?? "",
      endAt: json["end_at"] ?? "",
      createdAt: json["created_at"] ?? "",
      updatedAt: json["updated_viewer"] ?? "",
      deletedAt: json["deleted_viewer"] ?? "",
      viewers: list,
    );
  }

  Map<String, dynamic> toJSON() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["id"] = this.id;
    data["user_id"] = this.userId;
    data["description"] = this.description;
    data["status"] = this.status;
    data["end_at"] = this.endAt;
    data["created_at"] = this.createdAt;
    data["updated_viewer"] = this.updatedAt;
    data["deleted_viewer"] = this.deletedAt;
    data['viewers'] = this.viewers != null
        ? viewers.map((e) => e.toMap()).toList()
        : [];

    return data;
  }
}
