import 'package:findnear/src/pages/livestream/model/enum/ls_comment_type.dart';
import 'package:findnear/src/pages/livestream/model/ls_sender_entity.dart';

class LSCommentEntity {
  String content;
  LSCommentType type;
  LSSenderEntity sender;

  LSCommentEntity({this.content, this.type, this.sender});
}