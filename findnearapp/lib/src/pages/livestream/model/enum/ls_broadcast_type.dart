enum LSBroadcastPhase {
  initial,
  startLive,
  living,
  startEnd,
  ended,
  startLeft,
  left,
}