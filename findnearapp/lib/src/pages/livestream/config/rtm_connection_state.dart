enum RTMConnectionState {
  /// The user is not connected.
  CONNECTION_STATE_DISCONNECTED,
  /// The user is connecting
  CONNECTION_STATE_CONNECTING,
  /// The user is connected
  CONNECTION_STATE_CONNECTED,
  /// The user is reconnecting
  CONNECTION_STATE_RECONNECTING,
  /// The user is kicked out
  CONNECTION_STATE_ABORTED,
}

extension RTMConnectionStateExtension on RTMConnectionState {
  int get value {
    switch (this) {
      case RTMConnectionState.CONNECTION_STATE_DISCONNECTED:
        return 1;
      case RTMConnectionState.CONNECTION_STATE_CONNECTING:
        return 2;
      case RTMConnectionState.CONNECTION_STATE_CONNECTED:
        return 3;
      case RTMConnectionState.CONNECTION_STATE_RECONNECTING:
        return 4;
      case RTMConnectionState.CONNECTION_STATE_ABORTED:
        return 5;
      default:
        return 2;
    }
  }

  String get content {
    switch (this) {
      case RTMConnectionState.CONNECTION_STATE_DISCONNECTED:
        return "DISCONNECTED";
      case RTMConnectionState.CONNECTION_STATE_CONNECTING:
        return "CONNECTING";
      case RTMConnectionState.CONNECTION_STATE_CONNECTED:
        return "CONNECTED";
      case RTMConnectionState.CONNECTION_STATE_RECONNECTING:
        return "RECONNECTING";
      case RTMConnectionState.CONNECTION_STATE_ABORTED:
        return "ABORTED";
      default:
        return "CONNECTING";
    }
  }

  static RTMConnectionState fromValue(int value){
    switch (value) {
      case 1:
        return RTMConnectionState.CONNECTION_STATE_DISCONNECTED;
      case 2:
        return RTMConnectionState.CONNECTION_STATE_CONNECTING;
      case 3:
        return RTMConnectionState.CONNECTION_STATE_CONNECTED;
      case 4:
        return RTMConnectionState.CONNECTION_STATE_RECONNECTING;
      case 5:
        return RTMConnectionState.CONNECTION_STATE_ABORTED;
      default:
        return RTMConnectionState.CONNECTION_STATE_CONNECTING;
    }
  }
}