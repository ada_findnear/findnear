import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/pages/livestream/config/rtm_connection_state.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_disposable.dart';
import 'package:agora_rtm/agora_rtm.dart';

import 'livestream_config.dart';

class LiveStreamRTM extends GetxService {

  AgoraRtmClient client;
  AgoraRtmChannel channel;

  Future<LiveStreamRTM> init() async {
    return this;
  }

  // Future<void> initPlatformState() async {
  //   engine = await RtcEngine.create(APP_ID);
  // }

  Future<void> createClient() async {
    client = await AgoraRtmClient.createInstance(APP_ID);
    client.onMessageReceived = (AgoraRtmMessage message, String peerId){
      print('Private Message from ${peerId}: ${message.text}');
    };
    client.onConnectionStateChanged = (int state, int reason){
      print('Connection state changed: ${state.toString()}, reason: ${reason.toString()}');
      if(state == RTMConnectionState.CONNECTION_STATE_ABORTED){
        client.logout();
        print('Logout.');
      }
    };
  }

  Future<void> login({String userId = "", String token = "", String channelId}) async {
    if(userId.isEmpty){
      print('user is empty');
      return;
    }
    try {
      await client.login(token, userId);
    } catch (errorCode) {
      print('Login error: + ${errorCode.toString()}');
    }
  }

  Future<AgoraRtmChannel> createChannel({String channelId,
    Function channelMessageReceived,
    Function onMemberJoined,
    Function onMemberLeft,
  }) async{
    AgoraRtmChannel channel = await client.createChannel(channelId);
    channel.onMemberJoined = (AgoraRtmMember member){
      onMemberJoined(member.userId);
      print('Member joined: ${member.userId} ,channel: ${member.channelId}');
    };
    channel.onMemberLeft = (AgoraRtmMember member){
      onMemberLeft(member.userId);
      print('Member left: ${member.userId} ,channel: ${member.channelId}');
    };
    channel.onMessageReceived = (AgoraRtmMessage message, AgoraRtmMember member){
      channelMessageReceived(message, member);
    };
    return channel;
  }

  Future<void> joinChannel({String channelId,
        Function onChannelMessageReceived,
        Function onMemberJoined,
        Function onMemberLeft,
      }) async{
    if(channelId.isEmpty){
      print('Please input channel id to join');
      return;
    }
    try{
      channel = await createChannel(
          channelId: channelId,
          channelMessageReceived: onChannelMessageReceived,
          onMemberJoined: onMemberJoined,
          onMemberLeft: onMemberLeft);
      await channel.join();
    } catch (errorCode){
      print('Join channel error: ${errorCode.toString()}');
    }
  }

  void sendChannelMessage(String text) async{
    AgoraRtmMessage message = AgoraRtmMessage.fromText(text);
    await channel.sendMessage(message);
  }

  void sendPeerMessage(String text, String peerId) async{
    AgoraRtmMessage message = AgoraRtmMessage.fromText(text);
    await client.sendMessageToPeer(peerId, message);
  }

  void isUserOnline(String peerId) async{
    Map<dynamic, dynamic> result = await client.queryPeersOnlineStatus([peerId]);
  }

  void leaveChannel() async {
    channel.leave();
  }

}