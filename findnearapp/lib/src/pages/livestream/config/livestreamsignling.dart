import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_disposable.dart';

import 'livestream_config.dart';

class LiveStreamSignaling extends GetxService {

  RtcEngine engine;
  RxBool isPause = false.obs;

  Future<LiveStreamSignaling> init() async {
    return this;
  }

  Future<void> initPlatformState() async {
    engine = await RtcEngine.create(APP_ID);
  }

  void enableVideo() async{
    await engine?.enableVideo();
    await engine?.enableAudio();
    isPause.value = false;
  }

  void disableVideo() async{
    await engine?.disableVideo();
    await engine?.disableAudio();
    isPause.value = true;
  }

  void switchCamera(){
    engine.switchCamera();
  }

}