import 'dart:async';
import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/database/livestreams_dao.dart';
import 'package:findnear/src/models/entities/live_token_entity.dart';
import 'package:findnear/src/models/entities/meta_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/enums/pop_enums.dart';
import 'package:findnear/src/models/response/comment_response.dart';
import 'package:findnear/src/models/response/create_comment_channel_reponse.dart';
import 'package:findnear/src/models/response/user_live_info.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/list_livestream/list_livestream_view.dart';
import 'package:findnear/src/pages/livestream/model/enum/ls_broadcast_type.dart';
import 'package:findnear/src/pages/livestream/presenter/comment_presenter.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import '../../base/base_controller.dart';
import '../../repository/livestream_repository.dart';
import 'broadcast_livestream_state.dart';
import 'config/livestream_rtm.dart';
import 'config/livestreamsignling.dart';
import 'package:rxdart/rxdart.dart' as RxDart;

class BroadcastLivestreamLogic
    extends BaseController<BroadcastLivestreamState> {
  final LiveStreamRepository _repository = Get.find<LiveStreamRepository>();
  LiveStreamRepository _commentRepository = LiveStreamApi(isSocketApi: true);
  LiveStreamSignaling signaling = Get.find<LiveStreamSignaling>();
  LiveStreamRTM rtmService = Get.find<LiveStreamRTM>();
  final _livestreamsDAO = Get.find<LivestreamsDAO>();

  Future<void> init(LiveTokenEntity token, LiveTokenEntity rtmToken, int fromTab) async{
    state.liveToken.value = token;
    state.rtmToken.value = rtmToken;
    getUserInfo(state.liveToken.value.uid);
    // createLiveStream();
    initPlatformState();
    initRTMService();
  }

  Future<void> createLiveStream({String description = ""}) async {
    state.broadcastPhaseRx.value = LSBroadcastPhase.startLive;
    try {
      final result = await _repository.createLiveStream(description);
      if(result.success){
        state.broadcastIdRx.value = result.data.user_id.toString();
        /// tạo kênh trò chuyện với id là id của channel
        await createCommentChannel();
        state.createdRx.value = true;
        AppSnackbar.showInfo(message: "Tạo livestream thành công!");
        startTimer();
        state.broadcastPhaseRx.value = LSBroadcastPhase.living;
      } else{
        state.createdRx.value = false;
        AppSnackbar.showInfo(message: "Tạo livestream không thành công!");
        state.broadcastPhaseRx.value = LSBroadcastPhase.initial;
      }
    } catch (e){
      state.broadcastPhaseRx.value = LSBroadcastPhase.initial;
    }

  }

  // Init the app
  Future<void> initPlatformState({int fromTab}) async {
    await [Permission.camera, Permission.microphone, Permission.storage]
        .request();
    // Create RTC client instance
    await signaling.initPlatformState();

    // Define event handling logic
    signaling.engine.setEventHandler(RtcEngineEventHandler (
        joinChannelSuccess: (String channel, int uid, int elapsed) {
      state.joinedRx.value = true;
    }, userJoined: (int uid, int elapsed) {
      state.remoteUidRx.value = uid;
    }, userOffline: (int uid, UserOfflineReason reason) {
      state.remoteUidRx.value = 0;
    }));
    // Enable local video
    signaling.enableVideo();
    // Set channel profile as livestreaming
    await signaling.engine.setChannelProfile(ChannelProfile.LiveBroadcasting);
    // Set user role as broadcaster
    await signaling.engine.setClientRole(ClientRole.Broadcaster);
    // Join channel with channel name as 'Quan Kun'
    // Truyền vào token của user live, uid của user live
    await signaling.engine.joinChannel(state.liveToken.value.token,
        state.liveToken.value.channel_name, null, state.liveToken.value.uid);
  }

  void endPreviewLivestream({int fromTab}) async {
    try {
      await signaling.engine?.leaveChannel();
      await signaling.engine?.destroy();

      popBack(fromTab);
    } catch (e) {
      /// do nothing
    }
  }

  void endLivestream({int fromTab}) async {
    state.broadcastPhaseRx.value = LSBroadcastPhase.startEnd;
    try {
      await stopCommentChannel();
      if (state.joinedRx.value) {
        var endLiveResponse = await _repository.endLiveStream("Live");
        await leaveRTMChannel();
        await signaling.engine?.leaveChannel();
        await signaling.engine?.destroy();
        // if(endLiveResponse.success){
        //
        // }
      }
      state.broadcastPhaseRx.value = LSBroadcastPhase.ended;
      popBack(fromTab);

    } catch (e) {
      state.broadcastPhaseRx.value = LSBroadcastPhase.living;
    }

  }

  void getUserInfo(int userId) async {
    final response = await _repository.getUserLiveInfo(userId);
    if(response.success){
      state.userinfo.value = response.data;
      state.statusGetUserInfo.value = true;
    }else{
      AppSnackbar.showInfo(message: "Không lấy được thông tin người dùng");
    }
  }

  void startTimer() {
    state.callDuration.value = 0;
    state.timer?.cancel();
    state.timer = Timer.periodic(const Duration(seconds: 1), (Timer t) {
      state.callDuration.value += 1;
    });
  }

  @override
  BroadcastLivestreamState createState() => BroadcastLivestreamState();

  Future<void> initRTMService() async {
    await rtmService.createClient();
    await rtmService.login(userId: state.rtmToken.value.uid.toString(), token: state.rtmToken.value.token);
    await rtmService.joinChannel(
        channelId: state.liveToken.value.channel_name,
        onMemberJoined: (String userId){
          addViewer(code: userId);
        },
        onMemberLeft: (String userId) {
          removeViewer(code: userId);
        }
    );

  }

  void likeComment(CommentPresenter presenter, {bool likedByMe = false}) {
    if(state.comments.isEmpty) return;
    state.updateCommentStatusRx.value = LoadStatus.loading;
    try {
      int index = state.comments.indexWhere((element) => element.id == presenter.id);
      if(index != -1){
        state.comments[index].isLiked = true;
        state.comments[index].totalLike = likedByMe ? (presenter.totalLike + 1): presenter.totalLike;
        List<String> userLikes = state.comments[index].userLikes ?? [];
        if(likedByMe){
          User currentUser = Get.find<LocalDataManager>().currentUser;
          if(userLikes.isEmpty) state.comments[index].userLikes.add(currentUser.code);
          else {
            int idx = userLikes
                ?.indexWhere((element) => currentUser.code == element);
            /// nếu k có thì thêm vào danh sách
            if (idx == -1) state.comments[index].userLikes.add(currentUser.code);
          }
        } else
          state.comments[index].userLikes = []..addAll(presenter.userLikes);
      }
      state.updateCommentStatusRx.value = LoadStatus.success;
    } catch(e) {
      state.updateCommentStatusRx.value = LoadStatus.failure;
    }
  }

  void unlikeComment(CommentPresenter presenter, {bool unlikedByMe = false}) {
    if(state.comments.isEmpty) return;
    state.updateCommentStatusRx.value = LoadStatus.loading;
    try {
      int index = state.comments.indexWhere((element) => element.id == presenter.id);
      if(index != -1){
        int totalLike = unlikedByMe ? (presenter.totalLike - 1) : presenter.totalLike;
        state.comments[index].isLiked = totalLike == 0 ? false : true;
        state.comments[index].totalLike = totalLike;
        List<String> userLikes = state.comments[index].userLikes ?? [];
        if(unlikedByMe){
          User currentUser = Get.find<LocalDataManager>().currentUser;
          if(userLikes.isNotEmpty){
            int idx = userLikes
                ?.indexWhere((element) => currentUser.code == element);
            /// nếu có thì xoá khỏi danh sách
            if (idx != -1) state.comments[index].userLikes.removeAt(idx);
          }
        } else
          state.comments[index].userLikes = []..addAll(presenter.userLikes);
      }
      state.updateCommentStatusRx.value = LoadStatus.success;
    } catch(e) {
      state.updateCommentStatusRx.value = LoadStatus.failure;
    }
  }

  void leaveRTMChannel(){
    // Leave the RTM channel
    rtmService.leaveChannel();
  }

  void switchCamera(){
    signaling.switchCamera();
  }

  void addViewer({String code}) async {
    state.updateViewersStatusRx.value = LoadStatus.loading;
    try {
      User user = await getUserFromId(userId: code);
      if(user == null) return;
      if(state.viewers.value.isEmpty) state.viewers.value.add(user);
      else {
        /// kiểm tra xem user đó có trong list chưa?
        User a = state.viewers.value
            ?.firstWhere((element) => user.code == element.code, orElse: () => null);
        /// nếu chưa có thì thêm vào danh sách
        if (a == null) state.viewers.value.add(user);
      }
      /// đổi trạng thái
      state.updateViewersStatusRx.value = LoadStatus.success;
    } catch (e){
      print('quanth: add viewer error');
      state.updateViewersStatusRx.value = LoadStatus.failure;
    }
  }

  void removeViewer({String code}) async {
    state.updateViewersStatusRx.value = LoadStatus.loading;
    try {
      User user = await getUserFromId(userId: code);
      if(user == null || state.viewers.value.isEmpty) return;
      /// kiểm tra xem user đó có trong list chưa?
      int index = state.viewers.value
          ?.indexWhere((element) => user.code == element.code);
      /// nếu có thì xoá khỏi danh sách
      if (index != -1) state.viewers.value.removeAt(index);
      /// đổi trạng thái
      state.updateViewersStatusRx.value = LoadStatus.success;
    } catch (e){
      print('quanth: add viewer error');
      state.updateViewersStatusRx.value = LoadStatus.failure;
    }
  }

  Future<User> getUserFromId({String userId}) async{
    try {
      /// check trong local db xem có không?
      User user = await _livestreamsDAO.get(userId);
      if(user!= null)
        return user;
      else {
        /// Nếu chưa có sẵn thì lấy từ api
        UserLiveResponse response = await _repository.getUserLiveInfo(int.parse(userId));
        User userFromAPI = response.data;
        /// save vào local db
        await _livestreamsDAO.put(userFromAPI);
        return userFromAPI;
      }
    } catch (e){
      print('quanth: add viewer error');
      return null;
    }
  }

  void popBack(int fromTab){
    Future<void>.delayed(const Duration(seconds: 2), () {
      if(fromTab == PopTarget.livestreamList.index)
        Get.key.currentState
            .pushNamedAndRemoveUntil(
            ListLivestreamPage.ROUTE_NAME, (route) => true);
      else
        Get.key.currentState
            .pushNamedAndRemoveUntil(
            '/Pages', (route) => true, arguments: fromTab ?? 0);
    });
  }

  Future<void> createCommentChannel({bool retried = false}) async{
    try {
      CreateCommentChannelResponse response = await _commentRepository.createCommentChannel(state.broadcastIdRx.value.toString());
      if(response != null){
      } else {
        if(!retried){
          await stopCommentChannel();
          await createCommentChannel(retried: true);
        }
      }
    } catch (e) {
      if(!retried){
        await stopCommentChannel();
        await createCommentChannel(retried: true);
      }
    }
  }

  Future<void> stopCommentChannel() async{
    try {
      bool response = await _commentRepository.stopCommentChannel(state.broadcastIdRx.value);
    } catch (e){
    }
  }

  Future<CommentResponse> sendComment(String text, MetaEntity metaEntity) async{
    try {
      CommentResponse response = await _commentRepository.sendComment(broadcastId: state.broadcastIdRx.value.toString(), text: text, metaEntity: metaEntity);
      if(response != null) return response;
      else return null;
    } catch (e){
      return null;
    }
  }

  Future<CommentResponse> reactComment({String commentId}) async{
    try {
      CommentResponse response = await _commentRepository.reactComment(broadcastId: state.broadcastIdRx.value, commentId: commentId);
      if(response != null){
        return response;
      } else
        return null;
    } catch (e){
      return null;
    }
  }

  Future<void> addComments(CommentResponse commentResponse) async{
    state.updateCommentStatusRx.value = LoadStatus.loading;
    try {
      CommentPresenter presenter = CommentPresenter.fromCommentResponse(commentResponse: commentResponse, replyingComment: commentResponse.meta);
      if(state.comments.isEmpty)
        state.comments.add(presenter);
      else {
        int index = state.comments.indexWhere((element) => element.id == commentResponse.id);
        if(index == -1) state.comments.add(presenter);
      }
      state.updateCommentStatusRx.value = LoadStatus.success;
    } catch (e) {
      state.updateCommentStatusRx.value = LoadStatus.failure;
    }
  }
}
