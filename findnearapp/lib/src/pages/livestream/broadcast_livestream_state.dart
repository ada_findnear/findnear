import 'dart:async';

import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/pages/livestream/component/viewers/ls_viewers.dart';
import 'package:findnear/src/pages/livestream/model/enum/ls_broadcast_type.dart';
import '../../base/base_state.dart';
import '../../models/user.dart';
import 'package:get/get.dart';

import '../../models/entities/live_token_entity.dart';
import 'presenter/comment_presenter.dart';

class BroadcastLivestreamState extends BaseState{
  BroadcastLivestreamState() {
    ///Initialize variables
  }

  RxBool joinedRx = false.obs;
  RxInt remoteUidRx = 0.obs;
  RxBool switchRx = false.obs;
  Rxn<LiveTokenEntity> liveToken = Rxn<LiveTokenEntity>();
  Rxn<LiveTokenEntity> rtmToken = Rxn<LiveTokenEntity>();
  List<CommentPresenter> comments = [];
  Rx<LoadStatus> updateCommentStatusRx = LoadStatus.initial.obs;
  Rxn<User> userinfo = Rxn<User>();
  RxBool statusGetUserInfo = false.obs;
  RxBool createdRx = false.obs;
  RxBool endRx = false.obs;
  Rx<LSBroadcastPhase> broadcastPhaseRx = LSBroadcastPhase.initial.obs;
  RxBool showNewCommentRx = false.obs;
  Rx<LoadStatus> updateViewersStatusRx = LoadStatus.initial.obs;
  Rx<List<User>> viewers = Rx<List<User>>([]);
  RxString broadcastIdRx = "".obs;

  Timer timer;
  final callDuration = 0.obs;
  Rxn<CommentPresenter> replyCommentRx = Rxn<CommentPresenter>();

  List<CommentPresenter> get sortedComments {
    if(comments.isEmpty) return [];
    else if(comments.length == 1) return comments;
    comments.sort((a, b) => a.createDateInMilis.compareTo(b.createDateInMilis));
    return comments;
  }
}
