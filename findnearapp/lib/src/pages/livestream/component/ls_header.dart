import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/pages/livestream/component/ls_network_circle_avatar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'ls_channel_info.dart';
import 'ls_circle_avatar.dart';
import 'ls_union.dart';

class LSHeader extends StatefulWidget {
  String name = "";
  String avatar = "";
  int viewerCount = 0;
  LSHeader({this.name, this.avatar, this.viewerCount});

  @override
  State<LSHeader> createState() => _LSHeaderState();
}

class _LSHeaderState extends State<LSHeader> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 171,
      height: 36,
      decoration: BoxDecoration(
        color: AppColors.black.withOpacity(0.5),
        borderRadius: BorderRadius.circular(20),
      ),
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(left: 3),
            child: _buildImage(),
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 6),
              child: LSChannelInfo(
                name: widget.name,
                viewCount: widget.viewerCount,
                height: 30,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(right: 3),
            child: LSUnion(
              width: 36,
              height: 30,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildImage() {
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(45 / 2)),
      child: LSNetworkCircleAvatarWidget(
        url: widget.avatar,
        size: 30.w,
        hasBorder: true,
      ),
    );
  }
}
