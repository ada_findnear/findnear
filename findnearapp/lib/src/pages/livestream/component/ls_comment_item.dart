import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/media.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/livestream/presenter/comment_presenter.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/members/circle_avatar/member_circle_avatar_widget.dart';
import 'package:findnear/src/pages/messages/conversation_detail/widgets/app_message_text.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../preview_media.dart';
import 'ls_network_circle_avatar_widget.dart';

class LSCommentItem extends StatelessWidget {
  Function reactCallback;
  Function replyCallback;
  CommentPresenter comment;

  LSCommentItem({this.comment, this.reactCallback, this.replyCallback});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          children: [
            _buildIncomingMessage(context),
            _buildFooter(),
          ],
        ),
      ],
    );
  }

  Widget _buildFooter() {
    User currentUser = Get.find<LocalDataManager>().currentUser;
    return Container(
      height: 15,
      margin: EdgeInsets.symmetric(horizontal: 50),
      alignment: Alignment.center,
      width: Get.mediaQuery.size.width,
      child: Row(
        children: [
          GestureDetector(
            onTap: () {
              reactCallback(comment.isLiked ? false : true);
            },
            child: Text(
              comment.isLikedByMe(currentUser.code) ? "Đã thích" : "Thích",
              style: TextStyle(
                fontFamily: 'Quicksand',
                fontSize: 12,
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          SizedBox(width: 19),
          GestureDetector(
            onTap: () {
              replyCallback(comment);
            },
            child: Text(
              "Trả lời",
              style: TextStyle(
                fontFamily: 'Quicksand',
                fontSize: 12,
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildIncomingMessage(context) {
    return Container(
      width: Get.mediaQuery.size.width,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Flexible(
            child: Stack(
              children: [
                Container(
                  padding: EdgeInsets.only(right: 10),
                  constraints: BoxConstraints(minWidth: 160),
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.2),
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                  margin: EdgeInsets.only(top: 8, bottom: 5),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      _buildAvatar(allowTap: true),
                      SizedBox(width: 8),
                      new Flexible(
                        child: GestureDetector(
                          child: Container(
                            padding: EdgeInsets.symmetric(vertical: 5),
                            child: new Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  comment.name,
                                  style: TextStyle(
                                    fontFamily: 'Quicksand',
                                    color: Colors.white,
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(height: 2),
                                if (comment != null)
                                  if (comment.previousComment?.replyId?.isNotEmpty ?? false)
                                    Flexible(
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Flexible(
                                            child: Row(
                                              mainAxisSize: MainAxisSize.min,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Image.asset(AppImages.icRep,
                                                    width: 10.94, height: 9.25),
                                                SizedBox(width: 5),
                                                Flexible(
                                                  child: AppMessageText(
                                                    "Trả lời",
                                                    style: TextStyle(
                                                        fontFamily: 'Quicksand',
                                                        fontSize: 12,
                                                        fontWeight:
                                                            FontWeight.normal,
                                                        color: Colors.white),
                                                  ),
                                                ),
                                                SizedBox(width: 5),
                                                Flexible(
                                                  child: AppMessageText(
                                                    comment
                                                        .previousComment.replySender,
                                                    style: TextStyle(
                                                        fontFamily: 'Quicksand',
                                                        fontSize: 12,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: Colors.white),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Flexible(
                                            child: Container(
                                              margin:
                                                  EdgeInsets.only(left: 15.94),
                                              child: Text(
                                                comment.previousComment.replyContent,
                                                style: TextStyle(
                                                  fontFamily: 'Quicksand',
                                                  color: Colors.white,
                                                  fontSize: 10,
                                                  fontWeight: FontWeight.normal,
                                                ),
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                if (comment.previousComment?.replyId?.isNotEmpty ?? false)
                                  SizedBox(height: 6),
                                (comment.previousComment?.replyId?.isEmpty ?? true)
                                    ? _buildComment()
                                    : _buildRepComment(),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 5),
                    ],
                  ),
                ),
                Visibility(
                  visible: comment.isLiked,
                  child: Positioned(
                    bottom: 0,
                    right: 18,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Image.asset(
                          AppImages.icLikeComment,
                          width: 18,
                          height: 18,
                        ),
                        SizedBox(width: 2),
                        Visibility(
                          visible: (comment.totalLike ?? 0) > 1,
                          child: Container(
                            constraints: BoxConstraints(minWidth: 18),
                            height: 18,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(60.0),
                            ),
                            padding: EdgeInsets.symmetric(horizontal: 2),
                            child: Center(
                              child: Text(
                                "${comment.totalLike ?? 0}",
                                style: TextStyle(
                                    fontFamily: 'Quicksand',
                                    fontSize: 10,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                                maxLines: 1,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildAvatar({bool allowTap = false}) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        width: 32,
        height: 32,
        margin: EdgeInsets.all(5),
        child: _buildImage(),
      ),
    );
  }

  Widget _buildImage() {
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(45 / 2)),
      child: LSNetworkCircleAvatarWidget(
        url: comment.avatar,
        size: 32.w,
        hasBorder: true,
      ),
    );
  }

  Widget _buildRepComment() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.2),
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      margin: EdgeInsets.only(left: 15.94),
      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 3),
      child: new AppMessageText(
        comment.text,
        style: TextStyle(
          fontFamily: 'Quicksand',
          color: Colors.white,
          fontWeight: FontWeight.normal,
          fontSize: 12,
        ),
      ),
    );
  }

  Widget _buildComment() {
    return Container(
      margin: EdgeInsets.only(top: 0.0),
      child: new AppMessageText(
        comment.text,
        style: TextStyle(
          fontFamily: 'Quicksand',
          color: Colors.white,
          fontWeight: FontWeight.normal,
          fontSize: 12,
        ),
      ),
    );
  }
}
