import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/profile_user/profile_user_view.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LSNetworkCircleAvatarWidget extends StatelessWidget {
  final String url;
  final double size;
  final bool hasBorder;
  final Color borderColor;
  static const double liveIndicatorAdditionalRadius = 8;

  const LSNetworkCircleAvatarWidget({
    Key key,
    this.url,
    this.size = 38,
    this.hasBorder = false,
    this.borderColor = AppColors.red,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget circularImage = ClipRRect(
      borderRadius: BorderRadius.circular(size / 2),
      child: Builder(
        builder: (context) {
          if (url?.isEmpty ?? true)
            return AvatarPlaceholder(size: size);
          return CachedNetworkImage(
            imageUrl: url,
            fit: BoxFit.cover,
            width: size,
            height: size,
            placeholder: (context, url) => CupertinoActivityIndicator(),
            errorWidget: (context, url, error) => AvatarPlaceholder(size: size),
          );
        },
      ),
    );

    if (hasBorder ?? false) {
      circularImage = Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(size),
          border: Border.all(color: borderColor),
        ),
        child: circularImage,
      );
    }
    return GestureDetector(
      onTap: (){}/* (!(user.code == currentUser.value.code ?? true)) ? _navigateToProfile : null*/,
      child: circularImage,
    );
  }
}

class AvatarPlaceholder extends StatelessWidget {
  final double size;
  const AvatarPlaceholder({Key key, this.size = 38}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size,
      height: size,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(size / 2),
        child: Image.asset(AppImages.icAvatarPlaceholder),
      ),
    );
  }
}

