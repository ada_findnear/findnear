import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/CircularAvatarWidget.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/pages/livestream/component/online_friends/loading/ls_online_friends_loading.dart';
import 'package:findnear/src/pages/livestream/component/online_friends/ls_online_friends_logic.dart';
import 'package:findnear/src/pages/livestream/component/online_friends/ls_online_friends_state.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LSOnlineFriends extends StatefulWidget {
  static const double radius = 14;
  static const double overlappingPixels = 8;

  LSOnlineFriends();

  @override
  State<LSOnlineFriends> createState() => _LSOnlineFriendsState();
}

class _LSOnlineFriendsState extends BaseWidgetState<LSOnlineFriends,
    LSOnlineFriendsLogic,
    LSOnlineFriendsState> {

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      controller.fetchFriend(keyword: "", isReload: true);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      if(state.fetchFriendStatusRx.value == LoadStatus.loading)
        return LSOnlineFriendsLoading();
      else if(state.fetchFriendStatusRx.value == LoadStatus.success){
        if(state.friends.isEmpty) return Container();
        return _buildBodyWidget();
      } else
        return Container();
    });
  }

  Widget _buildBodyWidget() {
    if (state.friends.isEmpty) return Container();

    final int count = state.friends.length > 3 ? 3 : state.friends.length;
    double firstItemOffset = 0;
    double secondItemOffset = 0;
    if (count == 1) {
      firstItemOffset = LSOnlineFriends.overlappingPixels / 9;
    } else if (count == 2) {
      firstItemOffset =
          LSOnlineFriends.radius * 2 - LSOnlineFriends.overlappingPixels;
    } else if (count > 2) {
      secondItemOffset =
          LSOnlineFriends.radius * 2 - LSOnlineFriends.overlappingPixels;
      firstItemOffset = secondItemOffset + LSOnlineFriends.radius * 2 -
          LSOnlineFriends.overlappingPixels;
    }

    return Row(
      children: [
        Container(
          width: (state.friends.length > 2)
              ? 3 * (LSOnlineFriends.radius * 2 - LSOnlineFriends.overlappingPixels)
              : (state.friends.length == 1)
              ? (LSOnlineFriends.radius * 2 - LSOnlineFriends.overlappingPixels)
              : 2 *
              (LSOnlineFriends.radius * 2 - LSOnlineFriends.overlappingPixels),
          height: LSOnlineFriends.radius * 2 + 1,
          child: Stack(
            alignment: Alignment.centerLeft,
            clipBehavior: Clip.none,
            children: [
              if (state.friends.length > 0)
                Positioned(
                  right: firstItemOffset,
                  child: CircularAvatarWidget(
                    imageUrl: (state.friends?.isNotEmpty ?? true) ? state.friends.elementAt(0).image.thumb : "",
                    size: 2 * LSOnlineFriends.radius,
                    hasBorder: true,
                    borderColor: AppColors.white,
                  ),
                ),
              if (state.friends.length > 1)
                Positioned(
                  right: secondItemOffset,
                  child: CircularAvatarWidget(
                    imageUrl: state.friends.elementAt(1).image.thumb,
                    size: 2 * LSOnlineFriends.radius,
                    hasBorder: true,
                    borderColor: AppColors.white,
                  ),
                ),
              if (state.friends.length > 2)
                Positioned(
                  right: 0,
                  child: _buildMoreUsersWidget(context),
                ),
            ],
          ),
        ),
        SizedBox(
          width: 2,
        ),
        Flexible(
          child: Text(
            (state.friends.length > 2) ? "${state.friends.elementAt(0).name}, ${state.friends.elementAt(1).name} và ${state.friends.length - 2} bạn khác đang online":
            (state.friends.length > 1) ? "${state.friends.elementAt(0).name}, ${state.friends.elementAt(1).name} đang online":
            "${state.friends.elementAt(0).name} đang online",
            textAlign: TextAlign.left,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 12,
              color: AppColors.white,
              fontWeight: FontWeight.normal,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildMoreUsersWidget(BuildContext context) =>
      Container(
        width: 2 * LSOnlineFriends.radius,
        height: 2 * LSOnlineFriends.radius,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(LSOnlineFriends.radius),
        ),
        child: FittedBox(
          fit: BoxFit.fill,
          child: Image.asset(AppImages.icLsMember),
        ),
      );

  @override
  LSOnlineFriendsLogic createController() => LSOnlineFriendsLogic();
}
