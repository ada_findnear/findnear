import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/CircularAvatarWidget.dart';
import 'package:findnear/src/pages/livestream/component/online_friends/ls_online_friends_logic.dart';
import 'package:findnear/src/pages/livestream/component/online_friends/ls_online_friends_state.dart';
import 'package:findnear/src/widgets/shimmer/app_shimmer.dart';
import 'package:flutter/material.dart';

class LSOnlineFriendsLoading extends StatelessWidget {
  static const double radius = 14;
  static const double overlappingPixels = 8;

  @override
  Widget build(BuildContext context) {
    double firstItemOffset = 0;
    double secondItemOffset = 0;
    secondItemOffset = radius * 2 - overlappingPixels;
    firstItemOffset = secondItemOffset + radius * 2 - overlappingPixels;

    return Row(
      children: [
        Container(
          width: 3 * (radius * 2 - overlappingPixels),
          height: radius * 2 + 1,
          child: Stack(
            alignment: Alignment.centerLeft,
            clipBehavior: Clip.none,
            children: [
                Positioned(
                  right: firstItemOffset,
                  child: AppShimmer(width: 2 * radius, height: 2 * radius, cornerRadius: radius),
                ),
                Positioned(
                  right: secondItemOffset,
                  child: AppShimmer(width: 2 * radius, height: 2 * radius, cornerRadius: radius)
                ),
                Positioned(
                  right: 0,
                  child: _buildMoreUsersWidget(context),
                ),
            ],
          ),
        ),
        SizedBox(
          width: 2,
        ),
        AppShimmer(width: 90, height: 18, cornerRadius: 9),
      ],
    );
  }

  Widget _buildMoreUsersWidget(BuildContext context) => AppShimmer(width: 2 * radius, height: 2 * radius, cornerRadius: radius);

  @override
  LSOnlineFriendsLogic createController() => LSOnlineFriendsLogic();
}
