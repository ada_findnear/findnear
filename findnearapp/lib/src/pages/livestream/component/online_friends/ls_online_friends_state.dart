import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/response/user_v2_response.dart';
import 'package:findnear/src/models/user.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart' as refresh;

class LSOnlineFriendsState extends BaseState{
  List<User> friends = [];
  Rx<LoadStatus> fetchFriendStatusRx = LoadStatus.initial.obs;
  // loadmore
  final canLoadmore = true.obs;

  LSOnlineFriendsState() {
    ///Initialize variables
  }
}
