import 'dart:math';

import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/database/livestreams_dao.dart';
import 'package:findnear/src/models/base/data_reponse.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/response/user_live_info.dart';
import 'package:findnear/src/models/response/user_v2_response.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/livestream/component/online_friends/ls_online_friends_state.dart';
import 'package:findnear/src/pages/livestream/component/viewers/ls_viewers_state.dart';
import 'package:findnear/src/repository/communication_repository.dart';
import 'package:findnear/src/repository/livestream_repository.dart';
import 'package:findnear/src/repository/user_v2_repository.dart';
import 'package:get/get.dart';

class LSOnlineFriendsLogic extends BaseController<LSOnlineFriendsState> {
  CommunicationRepository _repository = CommunicationApi(isSocketApi: false);

  @override
  LSOnlineFriendsState createState() => LSOnlineFriendsState();

  void fetchFriend({String keyword = '', bool isReload = false}) async {
    try {
      var offset = state.friends.length;

      if (isReload) {
        state.fetchFriendStatusRx.value = LoadStatus.loading;
        offset = 0;
        state.canLoadmore.value = true;
        state.friends = [];
      }

      if (state.canLoadmore.value != true) return;
      final response = await _repository.searchFriend(
        keyword: keyword,
        currentOffset: offset,
      );

      if (response.isNotEmpty == true) {
        state.friends = state.friends..addAll(response);
        // state.friends.refresh();
      } else {
        state.canLoadmore.value = false;
      }
      state.fetchFriendStatusRx.value = LoadStatus.success;
    } catch (e){
      state.fetchFriendStatusRx.value == LoadStatus.failure;
    }

  }

}
