import 'dart:io';

import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:flutter/material.dart';

class LSCircleGroup extends StatelessWidget{
  double width;
  double height;
  String image;

  LSCircleGroup({this.width, this.height, this.image});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      child: CircleAvatar(
        backgroundImage: AssetImage(image),
      ),
    );
  }

}