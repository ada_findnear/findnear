import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:flutter/material.dart';

class LSClaimCoin extends StatefulWidget {

  /// Timer duration in seconds
  final int duration;

  /// Initial time, 0 by default
  final int initialPosition;

  /// This call callback will be excuted when the Countdown ends.
  final Function onComplete;

  LSClaimCoin({
    this.duration,
    this.initialPosition = 0,
    this.onComplete,
  });

  @override
  _LSClaimCoinState createState() => _LSClaimCoinState();
}

class _LSClaimCoinState extends State<LSClaimCoin>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation<double> _animation;
  bool _completedCoundown = false;

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(
        seconds: widget.duration,
      ),
    );
    _animation = Tween<double>(
      begin: widget.initialPosition.toDouble(),
      end: widget.duration.toDouble(),
    ).animate(_animationController);

    _animationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        setState(() {
          _completedCoundown = true;
        });
        widget.onComplete?.call();
      }
    });

    _animationController.addListener(() {
      setState(() {});
    });

    WidgetsBinding.instance.addPostFrameCallback((_) {
      onAnimationStart();
    });

  }

  @override
  void reassemble() {
    onAnimationStart();
    super.reassemble();
  }

  void onAnimationStart() {
    _animationController.forward(from: 0);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 110,
      height: 71,
      decoration: BoxDecoration(
        color: AppColors.black.withOpacity(0.5),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        children: [
          SizedBox(height: 6),
          Container(
            margin: EdgeInsets.only(left: 10),
            child: Text(
              "Xem live nhận xu",
              style: TextStyle(
                fontFamily: 'Quicksand',
                fontSize: 10,
                fontWeight: FontWeight.bold,
                color: AppColors.white,
              ),
            ),
          ),
          SizedBox(height: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                AppImages.icCoin,
                width: 13.89,
                height: 13.22,
              ),
              SizedBox(width: 5),
              Text(
                "100",
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 13,
                  fontWeight: FontWeight.bold,
                  color: AppColors.unionBg,
                ),
              ),
            ],
          ),
          SizedBox(height: 5),
          Text(
            "Thời gian xem live còn lại",
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 7,
              fontWeight: FontWeight.normal,
              color: AppColors.white,
            ),
          ),
          SizedBox(height: 5,),
          Container(
            height: 3,
            margin: EdgeInsets.symmetric(horizontal: 9),
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              child: LinearProgressIndicator(
                color: AppColors.red,
                backgroundColor: AppColors.white,
                value: _animationController.value,
              ),
            ),
          )
        ],
      ),
    );
  }
}
