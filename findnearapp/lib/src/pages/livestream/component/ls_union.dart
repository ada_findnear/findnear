import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:flutter/material.dart';

class LSUnion extends StatelessWidget{
  double width;
  double height;

  LSUnion({this.width, this.height});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      child: Image.asset(AppImages.icUnion, width: 17, height: 17, color: Colors.white,),
      decoration: BoxDecoration(
        color: AppColors.unionBg,
        borderRadius: BorderRadius.circular(45 / 2),
      ),
    );
  }

}