import 'dart:math';

import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/database/livestreams_dao.dart';
import 'package:findnear/src/models/base/data_reponse.dart';
import 'package:findnear/src/models/response/user_live_info.dart';
import 'package:findnear/src/models/response/user_v2_response.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/livestream/component/viewers/ls_viewers_state.dart';
import 'package:findnear/src/repository/livestream_repository.dart';
import 'package:findnear/src/repository/user_v2_repository.dart';
import 'package:get/get.dart';

class LSViewersLogic extends BaseController<LSViewersState> {
  LiveStreamRepository _repository = Get.find<LiveStreamRepository>();
  final _livestreamsDAO = Get.find<LivestreamsDAO>();

  @override
  LSViewersState createState() => LSViewersState();

  void addViewer({String code}) async {
    try {
      /// check trong local db xem có không?
      User user = await _livestreamsDAO.get(code);
      if(user!= null){
        /// Nếu có sẵn r thì lấy từ local db ra check
        if(state.viewers.value.isEmpty) state.viewers.value.add(user);
        else {
          /// kiểm tra xem user đó có trong list chưa?
          User a = state.viewers.value
              ?.firstWhere((element) => code == element.code, orElse: () => null);
          /// nếu chưa có thì add vào danh sách
          if (a == null) state.viewers.value.add(user);
        }
      } else {
        /// Nếu chưa có sẵn thì lấy từ api
        UserLiveResponse response = await _repository.getUserLiveInfo(int.parse(code));
        User user = response.data;
        if(user == null) return;
        /// Nếu có sẵn r thì lấy từ local db ra check
        if(state.viewers.value.isEmpty) state.viewers.value.add(user);
        else {
          /// kiểm tra xem user đó có trong list chưa?
          User a = state.viewers.value
              ?.firstWhere((element) => code == element.code, orElse: () => null);
          /// nếu chưa có thì add vào danh sách
          if (a == null) state.viewers.value.add(user);
        }
      }
    } catch (e){
      print('quanth: add viewer error');
    }
  }

  void removeViewer({String code}) async {
    try {
      /// check trong local db xem có không?
      User user = await _livestreamsDAO.get(code);
      if(user!= null){
        /// Nếu có sẵn r thì lấy từ local db ra check
        if(state.viewers.value.isEmpty) return;
        else {
          /// kiểm tra xem user đó có trong list chưa?
          int index = state.viewers.value
              ?.indexWhere((element) => code == element.code);
          /// nếu có thì remove khỏi danh sách
          if (index != -1) state.viewers.value.removeAt(index);
        }
      } else {
        /// Nếu chưa có sẵn thì lấy từ api
        UserLiveResponse response = await _repository.getUserLiveInfo(int.parse(code));
        User user = response.data;
        if(user == null) return;
        /// Nếu có sẵn r thì lấy từ local db ra check
        if(state.viewers.value.isEmpty) state.viewers.value.add(user);
        else {
          /// kiểm tra xem user đó có trong list chưa?
          int index = state.viewers.value
              ?.indexWhere((element) => code == element.code);
          /// nếu có thì remove khỏi danh sách
          if (index != -1) state.viewers.value.removeAt(index);
        }
      }
    } catch (e){
      print('quanth: add viewer error');
    }
  }

}
