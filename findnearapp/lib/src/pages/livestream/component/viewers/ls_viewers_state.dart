import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/response/user_v2_response.dart';
import 'package:findnear/src/models/user.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart' as refresh;

class LSViewersState extends BaseState{
  Rxn<List<User>> viewers = Rxn<List<User>>([]);

  InviteFriendListState() {
    ///Initialize variables
  }

  bool isUserSelected(User user) {
    if(viewers.value.isEmpty ?? true) return false;
    int index = viewers.value.indexWhere((element) => element.id == user.id);
    if(index != -1)
      return true;
    return false;
  }
}
