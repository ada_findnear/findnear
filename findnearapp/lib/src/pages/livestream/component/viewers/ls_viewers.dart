import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/CircularAvatarWidget.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/livestream/component/viewers/ls_viewers_logic.dart';
import 'package:findnear/src/pages/livestream/component/viewers/ls_viewers_state.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';

class LSViewers extends StatelessWidget {
  List<User> viewers = [];

  LSViewers({this.viewers});

  static const double radius = 14;
  static const double overlappingPixels = 8;
  static const int JOIN_LIVESTREAM = 0;
  static const int LEAVE_LIVESTREAM = 1;

  @override
  Widget build(BuildContext context) {
    final int count = viewers.length > 3 ? 3 : viewers.length;
    double firstItemOffset = 0;
    double secondItemOffset = 0;
    if (count == 2) {
      firstItemOffset = LSViewers.radius * 2 - LSViewers.overlappingPixels;
    } else if (count > 2) {
      secondItemOffset = LSViewers.radius * 2 - LSViewers.overlappingPixels;
      firstItemOffset = secondItemOffset + LSViewers.radius * 2 - LSViewers.overlappingPixels;
    }
    final defaultImageUrl =
        '${GlobalConfiguration().getValue('base_url')}images/image_default.png';

    return Row(
      children: [
        _buildRaiseHandWidget(),
        SizedBox(width: 2),
        Container(
          width: 3 * 2 * LSViewers.radius -
              (3 - 1) * LSViewers.overlappingPixels,
          height: LSViewers.radius * 2 + 1,
          child: Stack(
            alignment: Alignment.centerLeft,
            clipBehavior: Clip.none,
            children: [
              /// member = 1
              if (viewers.length == 1)
                Positioned(
                  left: firstItemOffset,
                  child: CircularAvatarWidget(
                    imageUrl: viewers
                        .elementAt(0)
                        .image
                        .thumb ?? defaultImageUrl,
                    size: 2 * LSViewers.radius,
                    hasBorder: true,
                    borderColor: AppColors.white,
                  ),
                ),
              /// member = 2
              if (viewers.length == 2)
                Positioned(
                  left: secondItemOffset,
                  child: CircularAvatarWidget(
                    imageUrl: viewers
                        .elementAt(viewers.length - 1)
                        .image
                        .thumb ?? defaultImageUrl,
                    size: 2 * LSViewers.radius,
                    hasBorder: true,
                    borderColor: AppColors.white,
                  ),
                ),
              if (viewers.length == 2)
                Positioned(
                  left: firstItemOffset,
                  child: CircularAvatarWidget(
                    imageUrl: viewers
                        .elementAt(viewers.length - 2)
                        .image
                        .thumb ?? defaultImageUrl,
                    size: 2 * LSViewers.radius,
                    hasBorder: true,
                    borderColor: AppColors.white,
                  ),
                ),
              /// member > 2
              if (viewers.length > 2)
                Positioned(
                  left: 0/*secondItemOffset*/,
                  child: CircularAvatarWidget(
                    imageUrl: viewers
                        .elementAt(viewers.length - 1)
                        .image
                        .thumb ?? defaultImageUrl,
                    size: 2 * LSViewers.radius,
                    hasBorder: true,
                    borderColor: AppColors.white,
                  ),
                ),
              if (viewers.length > 2)
                Positioned(
                  left: secondItemOffset/*firstItemOffset*/,
                  child: CircularAvatarWidget(
                    imageUrl: viewers
                        .elementAt(viewers.length - 2)
                        .image
                        .thumb ?? defaultImageUrl,
                    size: 2 * LSViewers.radius,
                    hasBorder: true,
                    borderColor: AppColors.white,
                  ),
                ),
              if (viewers.length > 2)
                Positioned(
                  left: firstItemOffset/*0*/,
                  child: _buildMoreUsersWidget(context),
                ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildMoreUsersWidget(BuildContext context) =>
      Container(
        width: 2 * LSViewers.radius,
        height: 2 * LSViewers.radius,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(LSViewers.radius),
          color: AppColors.gray2,
        ),
        child: Center(
          child: Text(
            "+${viewers.length - 2}",
            style: Theme
                .of(context)
                .textTheme
                .bodyText1
                .merge(TextStyle(fontSize: 10, color: AppColors.white)),
          ),
        ),
      );

  Widget _buildRaiseHandWidget() =>
      Container(
        width: 2 * LSViewers.radius + 1,
        height: 2 * LSViewers.radius + 1,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(LSViewers.radius),
          color: AppColors.gray2,
        ),
        child: Center(
          child: Image.asset(
            AppImages.icGroup,
            width: 2 * LSViewers.radius + 1,
            height: 2 * LSViewers.radius + 1,
          ),
        ),
      );
}