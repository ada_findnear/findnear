import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/SearchWidget.dart';
import 'package:findnear/src/pages/livestream/model/ls_comment_entity.dart';
import 'package:findnear/src/pages/livestream/presenter/comment_presenter.dart';
import 'package:findnear/src/pages/search/search_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class LSCommentBox extends StatelessWidget {
  Function callback;
  Function onCancelReplyingPressed;
  CommentPresenter replyingComment;
  LSCommentBox({Key key, this.callback, this.replyingComment, this.onCancelReplyingPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        callback(replyingComment);
      },
      child: Column(
        children: [
          _buildParentMessage(),
          SizedBox(height: 5),
          Container(
            width: Get.mediaQuery.size.width - 26,
            margin: const EdgeInsets.symmetric(horizontal: 13),
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    height: 40,
                    padding: const EdgeInsets.symmetric(horizontal: 12),
                    decoration: BoxDecoration(
                        color: Colors.transparent,
                        border: Border.all(
                          color: Colors.white,
                        ),
                        borderRadius: BorderRadius.circular(20)),
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(right: 8, left: 0),
                          child: Image.asset(
                            AppImages.icComments,
                            width: 16,
                            height: 16,
                          ),
                        ),
                        Expanded(
                          child: Text(
                            "Bình luận...",
                            style: TextStyle(
                              fontFamily: "Quicksand",
                              fontSize: 14,
                              fontWeight: FontWeight.normal,
                              color: Colors.white.withOpacity(0.8),
                            ),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          )/*TextField(
                              onSubmitted: (text){
                                callback(text, replyingComment);
                              },
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "Bình luận...",
                                  hintStyle: Theme.of(context)
                                      .textTheme
                                      .caption
                                      .merge(TextStyle(fontSize: 14))),
                              maxLines: 1,
                              style: Theme.of(context)
                                  .textTheme
                                  .caption
                                  .merge(TextStyle(fontSize: 14, color: Colors.white)),
                          )*/,
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  width: 13,
                ),
                Image.asset(
                  AppImages.icGift,
                  width: 37,
                  height: 37,
                ),
                SizedBox(
                  width: 13,
                ),
                Image.asset(
                  AppImages.icShare,
                  width: 37,
                  height: 37,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildParentMessage() {
    if (replyingComment == null) {
      return Container();
    } else {
      return Container(
        color: Colors.transparent,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment:MainAxisAlignment.center,
          children: [
            SizedBox(width: 12),
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 8),
                  Text(
                    "Trả lời:",
                    style: Theme.of(Get.context)
                        .textTheme
                        .caption
                        .merge(TextStyle(fontSize: 12, color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Text(
                    "${replyingComment.text}",
                    maxLines: 2,
                    style: Theme.of(Get.context)
                      .textTheme
                      .caption
                      .merge(TextStyle(fontSize: 10, color: Colors.white, fontWeight: FontWeight.normal),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(width: 12),
            Container(
              padding: EdgeInsets.only(right: 12),
              child: GestureDetector(
                onTap: onCancelReplyingPressed,
                child: Icon(
                  Icons.close_rounded,
                  color: Colors.white,
                ),
              )
            ),
          ],
        ),
      );
    }
  }
}
