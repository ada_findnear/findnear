import 'package:findnear/src/commons/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'ls_channel_info.dart';
import 'ls_circle_avatar.dart';
import 'ls_network_circle_avatar_widget.dart';
import 'ls_union.dart';

class LSPreviewHeader extends StatefulWidget {
  String name = "";
  String avatar = "";
  LSPreviewHeader({this.name, this.avatar});

  @override
  State<LSPreviewHeader> createState() => _LSPreviewHeaderState();
}

class _LSPreviewHeaderState extends State<LSPreviewHeader> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 171,
      height: 36,
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(left: 3),
            child: _buildImage(),
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 6),
              child: Text(
                widget.name ?? "",
                style: TextStyle(
                    fontFamily: 'Quicksand',
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildImage() {
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(45 / 2)),
      child: LSNetworkCircleAvatarWidget(
        url: widget.avatar,
        size: 30.w,
        hasBorder: true,
      ),
    );
  }
}
