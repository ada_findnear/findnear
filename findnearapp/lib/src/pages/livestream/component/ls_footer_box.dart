import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/SearchWidget.dart';
import 'package:findnear/src/pages/livestream/model/ls_comment_entity.dart';
import 'package:findnear/src/pages/livestream/presenter/comment_presenter.dart';
import 'package:findnear/src/pages/search/search_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class LSFooterBox extends StatelessWidget {
  Function onCancelReplyingPressed;
  Function onMessagePressed;
  Function onInviteFriendPressed;
  Function onSwitchCameraPressed;
  Function onSharePressed;
  CommentPresenter replyingComment;
  Function onEndLivestream;
  LSFooterBox({
    Key key,
    this.replyingComment,
    this.onCancelReplyingPressed,
    this.onMessagePressed,
    this.onInviteFriendPressed,
    this.onSwitchCameraPressed,
    this.onSharePressed,
    this.onEndLivestream,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _buildParentMessage(),
        SizedBox(height: 5),
        Container(
          width: Get.mediaQuery.size.width - 26,
          margin: const EdgeInsets.symmetric(horizontal: 13),
          child: Row(
            children: [
              _buildSendComment(),
              SizedBox(
                width: 8,
              ),
              _buildInviteFriend(),
              SizedBox(
                width: 8,
              ),
              _buildSwitchCamera(),
              SizedBox(
                width: 8,
              ),
              _buildShare(),
              Expanded(child: Container()),
              Flexible(
                child: GestureDetector(
                  onTap: onEndLivestream,
                  child: Container(
                    height: 40,
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                        color: Colors.transparent,
                        border: Border.all(
                          color: Colors.white,
                        ),
                        borderRadius: BorderRadius.circular(20)),
                    child: Center(
                      child: Text(
                        "KẾT THÚC",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          fontSize: 12,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                        maxLines: 1,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildSendComment() {
    return GestureDetector(
      onTap: (){
        onMessagePressed();
      },
      child: Image.asset(
        AppImages.icLsMessage,
        width: 36,
        height: 36,
      ),
    );
  }

  Widget _buildInviteFriend() {
    return GestureDetector(
      onTap: (){
        onInviteFriendPressed();
      },
      child: Image.asset(
        AppImages.icLsInvite,
        width: 36,
        height: 36,
      ),
    );
  }

  Widget _buildSwitchCamera() {
    return GestureDetector(
      onTap: (){
        onSwitchCameraPressed();
      },
      child: Image.asset(
        AppImages.icLsSwitchCam,
        width: 36,
        height: 36,
      ),
    );
  }

  Widget _buildShare() {
    return GestureDetector(
      onTap: (){
        onSharePressed();
      },
      child: Image.asset(
        AppImages.icShare,
        width: 36,
        height: 36,
      ),
    );
  }

  Widget _buildParentMessage() {
    if (replyingComment == null) {
      return Container();
    } else {
      return Container(
        color: Colors.transparent,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment:MainAxisAlignment.center,
          children: [
            SizedBox(width: 12),
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 8),
                  Text(
                    "Trả lời:",
                    style: Theme.of(Get.context)
                        .textTheme
                        .caption
                        .merge(TextStyle(fontSize: 12, color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Text(
                    "${replyingComment.text}",
                    maxLines: 2,
                    style: Theme.of(Get.context)
                      .textTheme
                      .caption
                      .merge(TextStyle(fontSize: 10, color: Colors.white, fontWeight: FontWeight.normal),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(width: 12),
            Container(
              padding: EdgeInsets.only(right: 12),
              child: GestureDetector(
                onTap: onCancelReplyingPressed,
                child: Icon(
                  Icons.close_rounded,
                  color: Colors.white,
                ),
              )
            ),
          ],
        ),
      );
    }
  }
}
