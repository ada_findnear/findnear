import 'dart:io';

import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/pages/livestream/component/ls_network_circle_avatar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class LSCircleAvatar extends StatelessWidget{
  double width;
  double height;
  String url;

  LSCircleAvatar({this.width, this.height, this.url});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      child: (url?.isEmpty ?? true) ?
        CircleAvatar(
          backgroundImage: AssetImage(AppImages.icCjndy),
          radius: 45 / 2,
        ) :
        _buildNetworkAvatar(),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(45 / 2),
      ),
    );
  }

  Widget _buildNetworkAvatar(){
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(45 / 2)),
      child: LSNetworkCircleAvatarWidget(
        url: url,
        size: 19.w,
        hasBorder: true,
      ),
    );
  }

}