import 'package:findnear/src/commons/app_images.dart';
import 'package:flutter/material.dart';

class LSChannelInfo extends StatelessWidget {
  String name;
  int viewCount;
  double height;

  LSChannelInfo({this.name, this.viewCount, this.height});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            name ?? "",
            style: TextStyle(
                fontFamily: 'Quicksand',
                color: Colors.white,
                fontSize: 14,
                fontWeight: FontWeight.bold),
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
          ),
          Expanded(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Image.asset(AppImages.icEye, width: 10.99, height: 6.42, color: Colors.white,),
                SizedBox(width: 2,),
                Expanded(
                  child: Text(
                    "${viewCount}" ?? "0",
                    style: TextStyle(
                        fontFamily: 'Quicksand',
                        color: Colors.white,
                        fontSize: 10,
                        fontWeight: FontWeight.normal),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
