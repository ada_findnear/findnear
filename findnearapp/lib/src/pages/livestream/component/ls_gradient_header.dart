import 'package:flutter/material.dart';

class LSGradientHeader extends StatelessWidget{
  double width;
  double height;
  Color start;
  Color end;
  bool hasRadius;

  LSGradientHeader({this.width, this.height, this.start, this.end, this.hasRadius = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            start,
            end,
          ],
        ),
        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(hasRadius ? 10.0: 0.0), bottomRight: Radius.circular(hasRadius ? 10.0: 0.0)),
      ),
    );
  }

}