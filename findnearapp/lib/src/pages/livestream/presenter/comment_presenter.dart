import 'package:equatable/equatable.dart';
import 'package:findnear/src/models/entities/meta_entity.dart';
import 'package:findnear/src/models/response/comment_response.dart';
import 'package:findnear/src/models/user.dart';
import 'package:global_configuration/global_configuration.dart';

class CommentPresenter{
  String id;
  String avatar;
  String name;
  String text;
  bool isLiked;
  int totalLike;
  int createDateInMilis;
  List<String> userLikes;
  MetaEntity previousComment;

  CommentPresenter({
    this.id,
    this.avatar,
    this.name,
    this.text,
    this.isLiked,
    this.previousComment,
    this.totalLike,
    this.userLikes,
    this.createDateInMilis,
  });

  CommentPresenter copyWith({
    String id,
    String avatar,
    String name,
    String text,
    bool isLiked,
    int totalLike,
    MetaEntity previousComment,
    int createDateInMilis,
    List<String> userLikes,
  }) {
    return CommentPresenter(
      id: id ?? this.id,
      avatar: avatar ?? this.avatar,
      name: name ?? this.name,
      text: text ?? this.text,
      isLiked: isLiked ?? this.isLiked,
      totalLike: totalLike ?? this.totalLike,
      previousComment: previousComment ?? this.previousComment,
      createDateInMilis: createDateInMilis ?? this.createDateInMilis,
      userLikes: userLikes ?? this.userLikes,
    );
  }

  static CommentPresenter fromCommentResponse({CommentResponse commentResponse, MetaEntity replyingComment}){
    return CommentPresenter(
      id: commentResponse.id,
      avatar: commentResponse.meta.senderAvatar ?? "${GlobalConfiguration().getValue('base_url')}images/image_default.png",
      name: commentResponse.meta.senderName ?? "",
      text: commentResponse.content,
      isLiked: commentResponse.totalLike > 0,
      totalLike: commentResponse.totalLike ?? 0,
      previousComment: replyingComment,
      createDateInMilis: commentResponse.createDateInMilis,
      userLikes: commentResponse.userLikes ?? [],
    );
  }

 bool isLikedByMe(String code){
    if(userLikes.isEmpty) return false;
    int index = userLikes.indexWhere((element) => element == code);
    return (index != -1);
 }

 String toString(){
    return "id= ${id}, name= ${name}, text= ${text}, replyContent= ${previousComment.replyContent.toString()}, senderName= ${previousComment.senderName.toString()}";
 }

}
