import 'dart:async';

import 'package:agora_rtc_engine/rtc_local_view.dart' as RtcLocalView;
import 'package:agora_rtm/agora_rtm.dart';
import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/extensions/duration_ext.dart';
import 'package:findnear/src/global/global_event.dart';
import 'package:findnear/src/models/entities/meta_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/response/comment_response.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/livestream/bottomsheet/ls_comment_bottom_sheet.dart';
import 'package:findnear/src/pages/livestream/bottomsheet/ls_invite_friend_bottom_sheet.dart';
import 'package:findnear/src/pages/livestream/broadcast_livestream_state.dart';
import 'package:findnear/src/pages/livestream/component/ls_footer_box.dart';
import 'package:findnear/src/pages/livestream/component/online_friends/ls_online_friends.dart';
import 'package:findnear/src/pages/livestream/loading/broadcast_end_livestream.dart';
import 'package:findnear/src/pages/livestream/loading/broadcast_start_livestream.dart';
import 'package:findnear/src/pages/livestream/model/enum/ls_broadcast_type.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:uuid/uuid.dart';

import 'broadcast_livestream_logic.dart';
import 'component/ls_claim_coin.dart';
import 'component/ls_comment_box.dart';
import 'component/ls_comment_item.dart';
import 'component/ls_gradient_header.dart';
import 'component/ls_header.dart';
import 'component/ls_preview_header.dart';
import 'component/viewers/ls_viewers.dart';
import 'config/livestreamsignling.dart';
import 'presenter/comment_presenter.dart';

class BroadcastLivestreamPage extends StatefulWidget {
  static const String ROUTE_NAME = '/BroadcastLivestream';
  final RouteArgument routeArgument;

  BroadcastLivestreamPage({Key key, this.routeArgument}) : super(key: key);

  @override
  _BroadcastLivestreamPageState createState() =>
      _BroadcastLivestreamPageState();
}

class _BroadcastLivestreamPageState extends BaseWidgetState<
    BroadcastLivestreamPage,
    BroadcastLivestreamLogic,
    BroadcastLivestreamState>
    with WidgetsBindingObserver {

  final _scrollController = ScrollController();
  final GlobalKey<AnimatedListState> _commentKey =
  GlobalKey<AnimatedListState>();
  int get fromTab => widget.routeArgument.param['from_tab'];
  LSBroadcastPhase _broadcastPhaseRx = LSBroadcastPhase.initial;
  var _textController = TextEditingController();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      controller.init(
          widget.routeArgument.param['token'],
          widget.routeArgument.param['rtm_token'],
          fromTab,
      );
    });

    state.broadcastPhaseRx.listen((val) {
      _broadcastPhaseRx = val;
    });

    _scrollController.addListener(() {
      double a =
          _scrollController.position.maxScrollExtent - _scrollController.offset;
      if (a <= 54.0) {
        state.showNewCommentRx.value = false;
      }
    });

    GlobalEvent.instance.onNewCommentResponse.stream.listen((data) {
      if(state.comments.isEmpty){
        CommentPresenter presenter = CommentPresenter.fromCommentResponse(commentResponse: data, replyingComment: data.meta);
        _addItem(presenter);
      } else {
        CommentPresenter presenter = CommentPresenter.fromCommentResponse(commentResponse: data, replyingComment: data.meta);
        int index = state.comments.indexWhere((element) => element.id == presenter.id);
        print('quanth: addItem broadcast index= ${index}');
        if(index == -1)
          _addItem(presenter);
      }
    });

    GlobalEvent.instance.onLikeCommentResponse.stream.listen((data) {
      CommentPresenter presenter = CommentPresenter.fromCommentResponse(commentResponse: data, replyingComment: data.meta);
      if(presenter.isLiked)
        controller.likeComment(presenter);
      else
        controller.unlikeComment(presenter);
    });
  }

  Future<void> popBack() async {
    Get.back();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        if (_broadcastPhaseRx == LSBroadcastPhase.living)
          return _showDialogEnd();
        else
          return popBack();
      },
      child: Scaffold(
        body: SafeArea(
          child: Stack(
            children: [
              Obx(() {
                return _buildLivestreamPanel();
              }),
              _buildHeaderGradient(),
              _buildFooterGradient(),
              Obx(() {
                return Visibility(
                    visible: state.broadcastPhaseRx.value ==
                        LSBroadcastPhase.initial,
                    child: _buildPreviewHeader());
              }),
              Obx(() {
                return Visibility(
                    visible: state.broadcastPhaseRx.value ==
                        LSBroadcastPhase.living,
                    child: _buildHeader());
              }),
              Obx(() {
                if (state.broadcastPhaseRx.value == LSBroadcastPhase.living)
                  return _buildLive();
                return Container();
              }),
              Obx(() {
                if (state.broadcastPhaseRx.value == LSBroadcastPhase.living)
                  return _buildFooter();
                return Container();
              }),
              Obx(() {
                if (state.broadcastPhaseRx.value == LSBroadcastPhase.living)
                  return _buildNewCommentCount();
                return Container();
              }),
              Obx(() {
                if (state.broadcastPhaseRx.value == LSBroadcastPhase.initial)
                  return _buildPreviewFooter();
                return Container();
              }),
              Obx(() {
                return Visibility(
                    visible: state.broadcastPhaseRx
                        .value == LSBroadcastPhase.startLive,
                    child: BroadcastStartLivestream());
              }),
              Obx(() {
                return Visibility(
                    visible: state.broadcastPhaseRx
                        .value == LSBroadcastPhase.startEnd ||
                        state.broadcastPhaseRx
                            .value == LSBroadcastPhase.ended,
                    child: BroadcastEndLivestream(phase: state.broadcastPhaseRx
                        .value));
              }),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildLivestreamPanel() {
    if (state.joinedRx.value) {
      return _renderLocalPreview();
    } else {
      return Container(
        color: Colors.black,
      );
    }
  }

  Widget _buildHeaderGradient() {
    return Positioned(
      top: 0,
      child: LSGradientHeader(
        height: 80,
        width: Get.mediaQuery.size.width,
        start: Colors.black,
        end: Colors.transparent,
      ),
    );
  }

  Widget _buildFooterGradient() {
    return Positioned(
      bottom: 0,
      child: LSGradientHeader(
        height: 320,
        width: Get.mediaQuery.size.width,
        start: Colors.transparent,
        end: Colors.black,
      ),
    );
  }

  Widget _buildHeader() {
    return Positioned(
      top: 32,
      child: Container(
        height: 36,
        width: Get.mediaQuery.size.width,
        child: Row(
          children: [
            GestureDetector(
              onTap: () {
                _showDialogEnd();
              },
              child: Container(
                color: Colors.transparent,
                padding: EdgeInsets.symmetric(horizontal: 12, vertical: 9),
                child: Image.asset(
                  AppImages.icBack,
                  width: 10.89,
                  height: 17.63,
                  color: Colors.white,
                ),
              ),
            ),
            state.statusGetUserInfo.value
                ? LSHeader(
                    name: state.userinfo.value?.name,
                    avatar: state.userinfo.value?.image?.thumb ?? GlobalConfiguration().getValue('api_base_url'),
                    viewerCount: state.viewers.value.length,
                  )
                : Container(),
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Spacer(),
                  state.updateViewersStatusRx.value == LoadStatus.success ?
                  Container(
                    margin: EdgeInsets.only(right: 12),
                    child: LSViewers(viewers: state.viewers.value),
                  ) : Container(
                    margin: EdgeInsets.only(right: 12),
                    child: LSViewers(viewers: state.viewers.value),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildPreviewHeader() {
    return Positioned(
      top: 32,
      child: Container(
        height: 36,
        width: Get.mediaQuery.size.width,
        child: Row(
          children: [
            GestureDetector(
              onTap: () {
                if (_broadcastPhaseRx == LSBroadcastPhase.living)
                  _showDialogEnd();
                else
                  Get.back();
              },
              child: Container(
                color: Colors.transparent,
                padding: EdgeInsets.symmetric(horizontal: 12, vertical: 9),
                child: Image.asset(
                  AppImages.icBack,
                  width: 10.89,
                  height: 17.63,
                  color: Colors.white,
                ),
              ),
            ),
            state.statusGetUserInfo.value
                ? LSPreviewHeader(
                    name: state.userinfo.value?.name,
                    avatar: state.userinfo.value?.image?.thumb ?? GlobalConfiguration().getValue('api_base_url'),
                  )
                : Container(),
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Spacer(),
                  GestureDetector(
                    onTap: () {
                      LSInviteFriendBottomSheet.showCreateConversationMenu(
                          onAddFriendPressed: () {
                            /// todo add friend
                          }
                      );
                    },
                    child: _buildInviteMember(),
                  ),
                  SizedBox(width: 13),
                  GestureDetector(
                    onTap: () {
                      controller.switchCamera();
                    },
                    child: _buildSwitchCamera(),
                  ),
                  SizedBox(width: 13),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildFooter() {
    return Positioned(
      bottom: 13,
      child: Container(
        width: Get.mediaQuery.size.width,
        height: 284,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(child: _buildComments()),
            SizedBox(
              height: 12,
            ),
            Obx(() {
              return LSFooterBox(
                onEndLivestream: () {
                  _showDialogEnd();
                },
                onCancelReplyingPressed: () {
                  state.replyCommentRx.value = null;
                },
                replyingComment: state.replyCommentRx.value,
                onMessagePressed: () {
                  LSCommentBottomSheet.showCommentBox(
                    replyingComment: null,
                    onCancelReplyingComment: () {
                      Get.back();
                    },
                    sendMessage: (String text,
                        CommentPresenter replyingComment) async {
                      User currentUser = Get.find<LocalDataManager>().currentUser;
                      CommentResponse response = await controller.sendComment(text, MetaEntity.fromCommentPresenter(reply: replyingComment, sender: currentUser));
                    },
                  );
                },
                onInviteFriendPressed: () {
                  LSInviteFriendBottomSheet.showCreateConversationMenu(
                      onAddFriendPressed: () {
                        /// todo add friend
                      }
                  );
                },
                onSwitchCameraPressed: () {
                  controller.switchCamera();
                },
                onSharePressed: () {
                  /// todo share
                  Fluttertoast.showToast(
                      msg: "Share",
                      toastLength: Toast.LENGTH_SHORT,
                      backgroundColor: AppColors.black,
                      textColor: AppColors.white,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIosWeb: 1);
                },
              );
            }),
          ],
        ),
      ),
    );
  }

  Widget _buildNewCommentCount() {
    return Visibility(
      visible: state.showNewCommentRx.value,
      child: Positioned(
        bottom: 55,
        child: Container(
          width: Get.mediaQuery.size.width,
          child: Center(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
              decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.2),
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: Text(
                "Bình luận mới",
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  color: Color(0xffFFD584),
                  fontSize: 10,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildPreviewFooter() {
    return Positioned(
      bottom: 13,
      child: Container(
        width: Get.mediaQuery.size.width,
        height: 166,
        padding: EdgeInsets.symmetric(horizontal: 38),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(20), topLeft: Radius.circular(20)),
          color: AppColors.black.withOpacity(0.2),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextField(
              controller: _textController,
              onSubmitted: (text) {
                // callback(text, replyingComment);
              },
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: "Nhập mô tả phát trực tiếp...",
                  hintStyle: Theme
                      .of(context)
                      .textTheme
                      .caption
                      .merge(TextStyle(fontSize: 16))),
              maxLines: 1,
              style: Theme
                  .of(context)
                  .textTheme
                  .caption
                  .merge(TextStyle(fontSize: 16, color: Colors.white)),
            ),
            SizedBox(
              height: 15,
            ),
            Flexible(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(width: 5),
                  Flexible(child: LSOnlineFriends()),
                ],
              ),
            ),
            SizedBox(
              height: 15,
            ),
            GestureDetector(
              onTap: () {
                controller.createLiveStream(description: _textController.text);
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: AppColors.red
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Image.asset(AppImages.icLsStart),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Text(
                        "PHÁT TRỰC TIẾP",
                        textAlign: TextAlign.left,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          fontSize: 12,
                          color: AppColors.white,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildLive() {
    return Positioned(
      top: 63,
      left: 39,
      child: Image.asset(
        AppImages.icLiving,
        width: 30,
        height: 14,
      ),
    );
  }

  Widget _buildClaimCoin() {
    return Positioned(
      top: 80,
      right: 13,
      child: LSClaimCoin(duration: 5, onComplete: (){}),
    );
  }

  Widget _buildClaimGift() {
    return Positioned(
      top: 75, //77.69
      right: 105, //102
      child: Image.asset(
        AppImages.icClaimGift,
        width: 27.21,
        height: 32.31,
      ),
    );
  }

  Widget _buildInviteMember() {
    return Image.asset(
      AppImages.icLsInvite,
      width: 37,
      height: 37,
    );
  }

  Widget _buildSwitchCamera() {
    return Image.asset(
      AppImages.icLsSwitchCam,
      width: 37,
      height: 37,
    );
  }

  Widget _buildComments() {
    return Obx(() {
      if (state.updateCommentStatusRx.value == LoadStatus.success)
        return Container(
          height: 260,
          child: AnimatedList(
            padding: EdgeInsets.only(bottom: 12, left: 12, right: 63),
            controller: _scrollController,
            key: _commentKey,
            reverse: false,
            initialItemCount: 0,
            itemBuilder: (BuildContext context, int index,
                Animation animation) {
              return _buildComment(state.comments[index], animation);
            },
          ),
        );
      return Container(
        height: 260,
        child: AnimatedList(
          padding: EdgeInsets.only(bottom: 12, left: 12, right: 63),
          controller: _scrollController,
          key: _commentKey,
          reverse: false,
          initialItemCount: 0,
          itemBuilder: (BuildContext context, int index, Animation animation) {
            return _buildComment(state.comments[index], animation);
          },
        ),
      );
    });
  }

  Widget _buildComment(CommentPresenter comment, Animation _animation) {
    return SizeTransition(
      sizeFactor: _animation,
      child: LSCommentItem(
        comment: comment,
        reactCallback: (bool isLiked) {
          if (isLiked)
            controller.likeComment(comment, likedByMe: true);
          else
            controller.unlikeComment(comment, unlikedByMe: true);
          controller.reactComment(commentId: comment.id);
        },
        replyCallback: (CommentPresenter presenter) {
          // state.replyCommentRx.value = presenter;
          LSCommentBottomSheet.showCommentBox(
            replyingComment: presenter,
            onCancelReplyingComment: () {
              Get.back();
            },
            sendMessage: (String text, CommentPresenter replyingComment) async{
              User currentUser = Get.find<LocalDataManager>().currentUser;
              CommentResponse response = await controller.sendComment(text, MetaEntity.fromCommentPresenter(reply: replyingComment, sender: currentUser));
            },
          );
        },
      ),
    );
  }

  void _addItem(CommentPresenter item) {
    final int _index = state.comments.length;
    state.comments.insert(_index, item);
    double a =
        _scrollController.position.maxScrollExtent - _scrollController.offset;
    _commentKey.currentState.insertItem(_index);
    Timer(
      Duration(milliseconds: 220),
          () {
        if (a <= 54.0) {
          state.showNewCommentRx.value = false;
          _scrollAnimateToEnd(_scrollController);
        } else {
          state.showNewCommentRx.value = true;
        }
      },
    );
    logger.d("quanth: scroll 2 $a");
  }

  // Local preview
  Widget _renderLocalPreview() {
    return RtcLocalView.SurfaceView();
  }

  @override
  void dispose() {
    if(_broadcastPhaseRx == LSBroadcastPhase.living){
      controller.endLivestream(fromTab: fromTab);
    } else {
      controller.endPreviewLivestream();
    }

    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.resumed) {
      Get.find<LiveStreamSignaling>().enableVideo();
      print('TuanLA state = $state');
    } else if (state == AppLifecycleState.paused) {
      Get.find<LiveStreamSignaling>().disableVideo();
      print('TuanLA state = $state');
    } else if (state == AppLifecycleState.detached ||
        state == AppLifecycleState.inactive) {
      //controller.endLivestream();
      print('TuanLA state = $state');
    }
  }

  @override
  BroadcastLivestreamLogic createController() => BroadcastLivestreamLogic();

  Future<void> _showDialogEnd() async {
    await showDialog(
      context: context,
      builder: (_) =>
          AlertDialog(
            title: const Text('Thông báo'),
            content: const Text('Bạn có muốn tắt live stream?'),
            actions: <Widget>[
              TextButton(
                onPressed: () {
                  Get.back();
                },
                child: const Text('Cancel'),
              ),
              TextButton(
                onPressed: () async {
                  Get.back();
                  await controller.endLivestream(fromTab: fromTab);
                },
                child: const Text('OK'),
              )
            ],
          ),
    );
  }

  void _onChannelMessageReceived(AgoraRtmMessage message,
      AgoraRtmMember member) {
    //controller.updateNewComment(message.text);
    // var uuid = Uuid();
    // var id = uuid.v1();
    // CommentPresenter newComment = CommentPresenter(
    //   id: id,
    //   avatar: AppImages.icCjndy,
    //   name: "Phương Kiều",
    //   text: message.text,
    //   isLiked: false,
    //   previousComment: null,
    // );
    // _addItem(newComment);
  }

  void _scrollAnimateToEnd(ScrollController controller) {
    Future.delayed(const Duration(milliseconds: 400)).then((_) {
      try {
        controller
            .animateTo(
          controller.position.maxScrollExtent,
          duration: const Duration(seconds: 1),
          curve: Curves.fastOutSlowIn,
        )
            .then((value) {
          controller.animateTo(
            controller.position.maxScrollExtent,
            duration: const Duration(milliseconds: 200),
            curve: Curves.easeInOut,
          );
        });
      } catch (e) {
        print('error on scroll $e');
      }
    });
  }
}
