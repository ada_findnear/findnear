import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class ChewiePlayerPage extends StatefulWidget {
  String liveUrl;

  ChewiePlayerPage({this.liveUrl});

  @override
  _ChewiePlayerPageState createState() => _ChewiePlayerPageState();
}

class _ChewiePlayerPageState extends State<ChewiePlayerPage> {
  ChewieController _chewieController;
  VideoPlayerController videoPlayerController;
  bool looping = true;

  @override
  void initState() {
    super.initState();
    videoPlayerController = VideoPlayerController.network(widget.liveUrl);
    // Wrapper on top of the videoPlayerController
    _chewieController = ChewieController(
      videoPlayerController: videoPlayerController,
      // Prepare the video to be played and display the first frame
      autoInitialize: true,
      autoPlay: true,
      looping: looping,
      allowFullScreen: true,
      isLive: true,
      showControls: false,
      aspectRatio: 9/16,
      // Errors can occur for example when trying to play a video
      // from a non-existent URL
      errorBuilder: (context, errorMessage) {
        return Center(
          child: Text(
            errorMessage,
            style: TextStyle(color: Colors.black),
          ),
        );
      },
    );
  }

  void dispose() {
    super.dispose();
    // IMPORTANT to dispose of all the used resources
    videoPlayerController.dispose();
    _chewieController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.black.withOpacity(0.8),
        child: Chewie(
          controller: _chewieController,
        ),
      ),
    );
  }
}
