import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/database/livestreams_dao.dart';
import 'package:findnear/src/models/entities/live_token_entity.dart';
import 'package:findnear/src/models/entities/meta_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/enums/pop_enums.dart';
import 'package:findnear/src/models/response/comment_response.dart';
import 'package:findnear/src/models/response/fetch_comment_response.dart';
import 'package:findnear/src/models/response/user_live_info.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/livestream/config/livestream_rtm.dart';
import 'package:findnear/src/pages/livestream/config/livestreamsignling.dart';
import 'package:findnear/src/pages/livestream/model/enum/ls_broadcast_type.dart';
import 'package:findnear/src/pages/livestream/presenter/comment_presenter.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:flutter/cupertino.dart';
import '../../../base/base_controller.dart';
import '../../../repository/livestream_repository.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'audience_livestream_state.dart';
import 'package:rxdart/rxdart.dart' as RxDart;

class AudienceLivestreamLogic extends BaseController<AudienceLivestreamState> {
  final LiveStreamRepository _repository = Get.find<LiveStreamRepository>();
  LiveStreamRepository _commentRepository = LiveStreamApi(isSocketApi: true);
  LiveStreamSignaling signaling = Get.find<LiveStreamSignaling>();
  LiveStreamRTM rtmService = Get.find<LiveStreamRTM>();
  final _livestreamsDAO = Get.find<LivestreamsDAO>();

  Future<void> init(LiveTokenEntity token, LiveTokenEntity rtmToken, int broadcastToken, int fromTab) async{
    state.broadcastPhaseRx.value = LSBroadcastPhase.startLive;
    try {
      state.audienceToken.value = token;
      state.rtmToken.value = rtmToken;
      state.broadcasterID.value = broadcastToken;
      await getUserInfo(broadcastToken);
      await joinLiveStream();
      await initPlatformState(fromTab: fromTab);
      initRTMService();
      await joinCommentChannel();
      state.broadcastPhaseRx.value = LSBroadcastPhase.living;
    } catch (e){
      state.broadcastPhaseRx.value = LSBroadcastPhase.startLive;
    }
  }

  // Init the app
  Future<void> initPlatformState({int fromTab}) async {
    await [Permission.camera, Permission.microphone, Permission.storage]
        .request();

    await signaling.initPlatformState();

    // Define event handling logic
    signaling.engine.setEventHandler(RtcEngineEventHandler(
        joinChannelSuccess: (String channel, int uid, int elapsed) async {
    }, userJoined: (int uid, int elapsed) {
    }, userOffline: (int uid, UserOfflineReason reason) {
      if(uid == state.broadcasterID.value){
        state.endRx.value = true;
        leaveLivestream(leave: false, fromTab: fromTab);
      }
    }, leaveChannel: (stats) {
    }, error: (e) {

    }));
    // Enable local video
    await signaling.engine.enableLocalVideo(true);
    // Enable video
    await signaling.engine.enableVideo();
    // Set channel profile as livestreaming
    await signaling.engine.setChannelProfile(ChannelProfile.LiveBroadcasting);
    // Set user role as audience
    await signaling.engine.setClientRole(ClientRole.Audience);
    // Join channel with channel name as 'Quan Kun'
    //truyền vào Token của viewer, uid của viewer
    await signaling.engine.joinChannel(
        state.audienceToken.value.token,
        state.audienceToken.value.channel_name,
        null,
        state.audienceToken.value.uid);
  }

  void leaveLivestream({bool leave = true, int fromTab}) async {
    state.broadcastPhaseRx.value = leave ?  LSBroadcastPhase.startLeft : LSBroadcastPhase.startEnd;
    try{
      if (state.joinedRx.value) {
        await _repository.leaveLiveStream(state.broadcasterID.value);
        await leaveCommentChannel();
        await leaveRTMChannel();
        await signaling.engine?.leaveChannel();
        await signaling.engine?.destroy();
      }
      state.broadcastPhaseRx.value = leave ? LSBroadcastPhase.left : LSBroadcastPhase.ended;
      popBack(fromTab);
    } catch(e){
      state.broadcastPhaseRx.value = LSBroadcastPhase.initial;
    }
  }

  void getUserInfo(int userId) async {
    final response = await _repository.getUserLiveInfo(userId);
    if (response.success) {
      state.userinfo.value = response.data;
      state.statusGetUserInfo.value = true;
    } else {
      AppSnackbar.showInfo(message: "Không lấy được thông tin người dùng");
    }
  }

  void joinLiveStream() async{
    var response =
        await _repository.joinLiveStream(state.broadcasterID.value);
    if (response.success) {
      state.joinedRx.value = true;
      List<User> members = response.data?.viewers ?? [];
      state.viewers.value = []..addAll(members);
    } else {
      state.endRx.value = true;
    }
  }
  @override
  AudienceLivestreamState createState() => AudienceLivestreamState();

  Future<void> initRTMService() async {
    await rtmService.createClient();
    await rtmService.login(userId: state.rtmToken.value.uid.toString(), token: state.rtmToken.value.token);
    await rtmService.joinChannel(
        channelId: state.audienceToken.value.channel_name,
        onMemberJoined: (String userId){
          addViewer(code: userId);
        },
        onMemberLeft: (String userId) {
          removeViewer(code: userId);
        }
    );
  }

  void likeComment(CommentPresenter presenter, {bool likedByMe = false}) {
    if(state.comments.isEmpty) return;
    state.updateCommentStatusRx.value = LoadStatus.loading;
    try {
      int index = state.comments.indexWhere((element) => element.id == presenter.id);
      if(index != -1){
        state.comments[index].isLiked = true;
        state.comments[index].totalLike = likedByMe ? (presenter.totalLike + 1): presenter.totalLike;
        List<String> userLikes = state.comments[index].userLikes ?? [];
        if(likedByMe){
          User currentUser = Get.find<LocalDataManager>().currentUser;
          if(userLikes.isEmpty) state.comments[index].userLikes.add(currentUser.code);
          else {
            int idx = userLikes
                ?.indexWhere((element) => currentUser.code == element);
            /// nếu k có thì thêm vào danh sách
            if (idx == -1) state.comments[index].userLikes.add(currentUser.code);
          }
        } else
          state.comments[index].userLikes = []..addAll(presenter.userLikes);
      }
      state.updateCommentStatusRx.value = LoadStatus.success;
    } catch(e) {
      state.updateCommentStatusRx.value = LoadStatus.failure;
    }
  }

  void unlikeComment(CommentPresenter presenter, {bool unlikedByMe = false}) {
    if(state.comments.isEmpty) return;
    state.updateCommentStatusRx.value = LoadStatus.loading;
    try {
      int index = state.comments.indexWhere((element) => element.id == presenter.id);
      if(index != -1){
        int totalLike = unlikedByMe ? (presenter.totalLike - 1) : presenter.totalLike;
        state.comments[index].isLiked = totalLike == 0 ? false : true;
        state.comments[index].totalLike = totalLike;
        List<String> userLikes = state.comments[index].userLikes ?? [];
        if(unlikedByMe){
          User currentUser = Get.find<LocalDataManager>().currentUser;
          if(userLikes.isNotEmpty){
            int idx = userLikes
                ?.indexWhere((element) => currentUser.code == element);
            /// nếu có thì xoá khỏi danh sách
            if (idx != -1) state.comments[index].userLikes.removeAt(idx);
          }
        } else
          state.comments[index].userLikes = []..addAll(presenter.userLikes);
      }
      state.updateCommentStatusRx.value = LoadStatus.success;
    } catch(e) {
      state.updateCommentStatusRx.value = LoadStatus.failure;
    }
  }

  void leaveRTMChannel(){
    // Leave the RTM channel
    rtmService.leaveChannel();
  }

  void addViewer({String code}) async {
    state.updateViewersStatusRx.value = LoadStatus.loading;
    try {
      User user = await getUserFromId(userId: code);
      if(user == null) return;
      if(state.viewers.value.isEmpty) state.viewers.value.add(user);
      else {
        /// kiểm tra xem user đó có trong list chưa?
        User a = state.viewers.value
            ?.firstWhere((element) => user.code == element.code, orElse: () => null);
        /// nếu chưa có thì thêm vào danh sách
        if (a == null) state.viewers.value.add(user);
      }
      /// đổi trạng thái
      state.updateViewersStatusRx.value = LoadStatus.success;
    } catch (e){
      print('quanth: add viewer error');
      state.updateViewersStatusRx.value = LoadStatus.failure;
    }
  }

  void removeViewer({String code}) async {
    state.updateViewersStatusRx.value = LoadStatus.loading;
    try {
      User user = await getUserFromId(userId: code);
      if(user == null || state.viewers.value.isEmpty) return;
      /// kiểm tra xem user đó có trong list chưa?
      int index = state.viewers.value
          ?.indexWhere((element) => user.code == element.code);
      /// nếu có thì xoá khỏi danh sách
      if (index != -1) state.viewers.value.removeAt(index);
      /// đổi trạng thái
      state.updateViewersStatusRx.value = LoadStatus.success;
    } catch (e){
      print('quanth: add viewer error');
      state.updateViewersStatusRx.value = LoadStatus.failure;
    }
  }

  Future<User> getUserFromId({String userId}) async{
    try {
      /// check trong local db xem có không?
      User user = await _livestreamsDAO.get(userId);
      if(user!= null)
        return user;
      else {
        /// Nếu chưa có sẵn thì lấy từ api
        UserLiveResponse response = await _repository.getUserLiveInfo(int.parse(userId));
        User userFromAPI = response.data;
        /// save vào local db
        await _livestreamsDAO.put(userFromAPI);
        return userFromAPI;
      }
    } catch (e){
      print('quanth: add viewer error');
      return null;
    }
  }

  Future<User> getUserByCode({String sender}) async{
    try {
      /// check trong local db xem có không?
      User user = await _livestreamsDAO.get(sender);
      if(user!= null)
        return user;
      else {
        /// Nếu chưa có sẵn thì lấy từ api
        User userFromAPI = await _repository.getUserByCode(sender);
        /// save vào local db
        await _livestreamsDAO.put(userFromAPI);
        return userFromAPI;
      }
    } catch (e){
      print('quanth: add viewer error');
      return null;
    }
  }

  void popBack(int fromTab){
    Future<void>.delayed(const Duration(seconds: 2), () {
      if(fromTab == PopTarget.livestreamList.index){
        int count = 0;
        Navigator.of(Get.context).popUntil((_) => count++ >= 1);
      } else
        Get.key.currentState
            .pushNamedAndRemoveUntil(
            '/Pages', (route) => true, arguments: fromTab);
    });
  }

  Future<void> fetchComments(String broadcastId) async{
    state.fetchCommentStatusRx.value = LoadStatus.loading;
    try {
      FetchCommentResponse response = await _commentRepository.fetchComments(broadcastId);
      List<CommentResponse> commentResponses = response.data ?? [];
      List<CommentPresenter> results = await mappingWithUserInfo(commentResponses);
      state.comments = []..addAll(results);
      state.fetchCommentStatusRx.value = LoadStatus.success;
    } catch (e) {
      state.fetchCommentStatusRx.value = LoadStatus.failure;
    }
  }

  Future<List<CommentPresenter>> mappingWithUserInfo(List<CommentResponse> commentResponses) async{
    List<CommentPresenter> results = [];
    if(commentResponses.isEmpty) return [];

    commentResponses.forEach((element) {
      CommentPresenter presenter = CommentPresenter.fromCommentResponse(commentResponse: element, replyingComment: element.meta);
      results.add(presenter);
    });

    return results;
  }

  void joinCommentChannel({bool retried = false}) async{
    try {
      var response =
      await _commentRepository.joinCommentChannel(state.broadcasterID.value.toString());
      if (response) {
        await fetchComments(state.broadcasterID.value.toString());
      } else {
        if(!retried){
          await leaveCommentChannel();
          await joinCommentChannel(retried: true);
        }
      }
    } catch (e) {
      if(!retried){
        await leaveCommentChannel();
        await joinCommentChannel(retried: true);
      }
    }
  }

  Future<void> leaveCommentChannel() async{
    try {
      bool response = await _commentRepository.leaveCommentChannel(state.broadcasterID.value.toString());
    } catch (e){
    }
  }

  Future<CommentResponse> sendComment(String text, MetaEntity metaEntity) async{
    try {
      CommentResponse response = await _commentRepository.sendComment(broadcastId: state.broadcasterID.value.toString(), text: text, metaEntity: metaEntity);
      if(response != null) return response;
      else return null;
    } catch (e){
      return null;
    }
  }

  Future<CommentResponse> reactComment({String commentId}) async{
    try {
      CommentResponse response = await _commentRepository.reactComment(broadcastId: state.broadcasterID.value.toString(), commentId: commentId);
      if(response != null){
        return response;
      } else
        return null;
    } catch (e){
      return null;
    }
  }

  Future<void> addComments(CommentResponse commentResponse) async{
    state.updateCommentStatusRx.value = LoadStatus.loading;
    try {
      CommentPresenter presenter = CommentPresenter.fromCommentResponse(commentResponse: commentResponse, replyingComment: commentResponse.meta);
      if(state.comments.isEmpty)
        state.comments.add(presenter);
      else {
        int index = state.comments.indexWhere((element) => element.id == commentResponse.id);
        if(index == -1) state.comments.add(presenter);
      }
      state.updateCommentStatusRx.value = LoadStatus.success;
    } catch (e) {
      state.updateCommentStatusRx.value = LoadStatus.failure;
    }
  }
}
