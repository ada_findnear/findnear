import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/entities/live_token_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/livestream/model/enum/ls_broadcast_type.dart';
import 'package:findnear/src/pages/livestream/presenter/comment_presenter.dart';
import 'package:get/get.dart';

class AudienceLivestreamState extends BaseState{
  AudienceLivestreamState() {
    ///Initialize variables
  }

  RxBool joinedRx = false.obs;
  RxBool switchRx = false.obs;
  Rxn<LiveTokenEntity> audienceToken = Rxn<LiveTokenEntity>();
  Rxn<LiveTokenEntity> rtmToken = Rxn<LiveTokenEntity>();
  List<CommentPresenter> comments = [];
  Rx<LoadStatus> fetchCommentStatusRx = LoadStatus.initial.obs;
  Rx<LoadStatus> updateCommentStatusRx = LoadStatus.initial.obs;
  RxInt broadcasterID = 0.obs;
  Rxn<User> userinfo = Rxn<User>();
  RxBool statusGetUserInfo = false.obs;
  RxBool endRx = false.obs;
  RxBool pauseRx = false.obs;
  Rxn<CommentPresenter> replyCommentRx = Rxn<CommentPresenter>();
  RxBool showNewCommentRx = false.obs;
  Rx<LSBroadcastPhase> broadcastPhaseRx = LSBroadcastPhase.initial.obs;
  Rx<LoadStatus> updateViewersStatusRx = LoadStatus.initial.obs;
  Rx<List<User>> viewers = Rx<List<User>>([]);

  List<CommentPresenter> get sortedComments {
    if(comments.isEmpty) return [];
    else if(comments.length == 1) return comments;
    comments.sort((a, b) => a.createDateInMilis.compareTo(b.createDateInMilis));
    return comments;
  }
}
