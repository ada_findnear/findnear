import 'dart:async';

import 'package:agora_rtc_engine/rtc_remote_view.dart' as RtcRemoteView;
import 'package:agora_rtm/agora_rtm.dart';
import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/global/global_event.dart';
import 'package:findnear/src/models/entities/meta_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/response/comment_response.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/livestream/audience/audience_livestream_logic.dart';
import 'package:findnear/src/pages/livestream/audience/audience_livestream_state.dart';
import 'package:findnear/src/pages/livestream/bottomsheet/ls_comment_bottom_sheet.dart';
import 'package:findnear/src/pages/livestream/component/ls_claim_coin.dart';
import 'package:findnear/src/pages/livestream/component/ls_comment_box.dart';
import 'package:findnear/src/pages/livestream/component/ls_comment_item.dart';
import 'package:findnear/src/pages/livestream/component/ls_gradient_header.dart';
import 'package:findnear/src/pages/livestream/component/ls_header.dart';
import 'package:findnear/src/pages/livestream/component/viewers/ls_viewers.dart';
import 'package:findnear/src/pages/livestream/loading/audience_end_livestream.dart';
import 'package:findnear/src/pages/livestream/loading/audience_start_livestream.dart';
import 'package:findnear/src/pages/livestream/model/enum/ls_broadcast_type.dart';
import 'package:findnear/src/pages/livestream/presenter/comment_presenter.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:uuid/uuid.dart';

class AudienceLivestreamPage extends StatefulWidget {
  static const String ROUTE_NAME = '/AudienceLivestream';
  final RouteArgument routeArgument;

  AudienceLivestreamPage({Key key, this.routeArgument}) : super(key: key);

  @override
  _AudienceLivestreamPageState createState() => _AudienceLivestreamPageState();
}

class _AudienceLivestreamPageState extends BaseWidgetState<
    AudienceLivestreamPage,
    AudienceLivestreamLogic,
    AudienceLivestreamState> {
  ScrollController _scrollController = ScrollController();
  final GlobalKey<AnimatedListState> _commentKey =
  GlobalKey<AnimatedListState>();

  int get fromTab => widget.routeArgument.param['from_tab'];

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      controller.init(widget.routeArgument.param['token'],
          widget.routeArgument.param['rtm_token'],
          widget.routeArgument.param['broadcaster_id'],
          fromTab
      );
    });

    state.fetchCommentStatusRx.listen((val) {
      if(val == LoadStatus.success)
        _addItems(state.sortedComments);
    });

    _scrollController.addListener(() {
      double a =
          _scrollController.position.maxScrollExtent - _scrollController.offset;
      if (a <= 54.0) {
        state.showNewCommentRx.value = false;
      }
    });

    GlobalEvent.instance.onNewCommentResponse.stream.listen((data) {
      debugPrint("quanth: onNewCommentResponse= ${data}");
      if(state.comments.isEmpty){
        CommentPresenter presenter = CommentPresenter.fromCommentResponse(commentResponse: data, replyingComment: data.meta);
        _addItem(presenter);
      } else {
        CommentPresenter presenter = CommentPresenter.fromCommentResponse(commentResponse: data, replyingComment: data.meta);
        int index = state.comments.indexWhere((element) => element.id == presenter.id);
        print('quanth: addItem index= ${index}');
        if(index == -1)
          _addItem(presenter);
      }

    });
    
    GlobalEvent.instance.onLikeCommentResponse.stream.listen((data) {
      debugPrint("quanth: onLikeCommentResponse= ${data}");
      CommentPresenter presenter = CommentPresenter.fromCommentResponse(commentResponse: data, replyingComment: data.meta);
      if(presenter.isLiked)
        controller.likeComment(presenter);
      else
        controller.unlikeComment(presenter);
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Obx(() {
              return _buildLivestreamPanel();
            }),
            _buildHeaderGradient(),
            _buildFooterGradient(),
            Obx(() {
              return _buildHeader();
            }),
            _buildLive(),
            _buildFooter(),
            Obx(() {
              return _buildNewCommentCount();
            }),
            _buildClaimCoin(),
            _buildClaimGift(),
            Obx(() {
              return Visibility(
                  visible: !state.joinedRx.value,
                  child: AudienceStartLivestream());
            }),
            Obx(() {
              return Visibility(
                  visible: state.broadcastPhaseRx
                      .value == LSBroadcastPhase.startEnd ||
                      state.broadcastPhaseRx
                          .value == LSBroadcastPhase.ended ||
                      state.broadcastPhaseRx
                          .value == LSBroadcastPhase.startLeft ||
                      state.broadcastPhaseRx
                          .value == LSBroadcastPhase.left,
                  child: AudienceEndLivestream(phase: state.broadcastPhaseRx
                      .value));
            }),
          ],
        ),
      ),
    );
  }

  Widget _buildLivestreamPanel() {
    //return ChewiePlayerPage(liveUrl: liveUrl);
    return _renderRemoteVideo();
  }

  Widget _buildHeaderGradient() {
    return Positioned(
      top: 0,
      child: LSGradientHeader(
        height: 80,
        width: Get.mediaQuery.size.width,
        start: Colors.black,
        end: Colors.transparent,
      ),
    );
  }

  Widget _buildFooterGradient() {
    return Positioned(
      bottom: 0,
      child: LSGradientHeader(
        height: 320,
        width: Get.mediaQuery.size.width,
        start: Colors.transparent,
        end: Colors.black,
      ),
    );
  }

  Widget _buildHeader() {
    return Positioned(
      top: 32,
      child: Container(
        height: 36,
        width: Get.mediaQuery.size.width,
        child: Row(
          children: [
            GestureDetector(
              onTap: () async {
                await controller.leaveLivestream(fromTab: fromTab);
              },
              child: Container(
                color: Colors.transparent,
                padding: EdgeInsets.symmetric(horizontal: 12, vertical: 9),
                child: Image.asset(
                  AppImages.icBack,
                  width: 10.89,
                  height: 17.63,
                  color: Colors.white,
                ),
              ),
            ),
            state.statusGetUserInfo.value
                ? LSHeader(
                    name: state.userinfo.value?.name,
                    avatar: state.userinfo.value?.image?.thumb ?? GlobalConfiguration().getValue('api_base_url'),
                    viewerCount: state.viewers.value.length,
                  )
                : Container(),
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Spacer(),
                  state.updateViewersStatusRx.value == LoadStatus.success ?
                  Container(
                    margin: EdgeInsets.only(right: 12),
                    child: LSViewers(viewers: state.viewers.value),
                  ) : Container(
                    margin: EdgeInsets.only(right: 12),
                    child: LSViewers(viewers: state.viewers.value),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildFooter() {
    return Positioned(
      bottom: 13,
      child: Container(
        width: Get.mediaQuery.size.width,
        height: 284,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(child: _buildComments()),
            SizedBox(
              height: 12,
            ),
            Obx(() {
              return LSCommentBox(
                  callback: (CommentPresenter reply) {
                    LSCommentBottomSheet.showCommentBox(
                      replyingComment: null,
                      onCancelReplyingComment: () {
                        Get.back();
                      },
                      sendMessage: (String text, CommentPresenter replyingComment) async {
                        User currentUser = Get.find<LocalDataManager>().currentUser;
                        CommentResponse response = await controller.sendComment(text, MetaEntity.fromCommentPresenter(reply: replyingComment, sender: currentUser));
                      },
                    );
                  },
                  onCancelReplyingPressed: () {
                    state.replyCommentRx.value = null;
                  },
                  replyingComment: state.replyCommentRx.value
              );
            }),
          ],
        ),
      ),
    );
  }

  Widget _buildNewCommentCount(){
    return Visibility(
      visible: state.showNewCommentRx.value,
      child: Positioned(
        bottom: 55,
        child: Container(
          width: Get.mediaQuery.size.width,
          child: Center(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
              decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.2),
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: Text(
                "Bình luận mới",
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  color: Color(0xffFFD584),
                  fontSize: 10,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildLive() {
    return Positioned(
      top: 63,
      left: 39,
      child: Image.asset(
        AppImages.icLiving,
        width: 30,
        height: 14,
      ),
    );
  }

  Widget _buildClaimCoin() {
    return Positioned(
      top: 80,
      right: 13,
      child: LSClaimCoin(duration: 60, onComplete: (){}),
    );
  }

  Widget _buildClaimGift() {
    return Positioned(
      top: 75, //77.69
      right: 105, //102
      child: Image.asset(
        AppImages.icClaimGift,
        width: 27.21,
        height: 32.31,
      ),
    );
  }

  Widget _buildComments() {
    return Obx(() {
      if (state.updateCommentStatusRx.value == LoadStatus.success)
        return Container(
          height: 260,
          child: AnimatedList(
            padding: EdgeInsets.only(bottom: 12, left: 12, right: 63),
            controller: _scrollController,
            key: _commentKey,
            reverse: false,
            initialItemCount: 0,
            itemBuilder: (BuildContext context, int index,
                Animation animation) {
              return _buildComment(state.comments[index], animation);
            },
          ),
        );
      return Container(
        height: 260,
        child: AnimatedList(
          padding: EdgeInsets.only(bottom: 12, left: 12, right: 63),
          controller: _scrollController,
          key: _commentKey,
          reverse: false,
          initialItemCount: 0,
          itemBuilder: (BuildContext context, int index, Animation animation) {
            return _buildComment(state.comments[index], animation);
          },
        ),
      );
    });
  }

  Widget _buildComment(CommentPresenter comment, Animation _animation) {
    return SizeTransition(
      sizeFactor: _animation,
      child: LSCommentItem(
        comment: comment,
        reactCallback: (bool isLiked) {
          if (isLiked)
            controller.likeComment(comment, likedByMe: true);
          else
            controller.unlikeComment(comment, unlikedByMe: true);
          controller.reactComment(commentId: comment.id);
        },
        replyCallback: (CommentPresenter presenter) {
          // state.replyCommentRx.value = presenter;
          LSCommentBottomSheet.showCommentBox(
            replyingComment: presenter,
            onCancelReplyingComment: () {
              Get.back();
            },
            sendMessage: (String text, CommentPresenter replyingComment) async {
              User currentUser = Get.find<LocalDataManager>().currentUser;
              CommentResponse response = await controller.sendComment(text, MetaEntity.fromCommentPresenter(reply: replyingComment, sender: currentUser));
            },
          );
        },
      ),
    );
  }

  // Remote preview
  Widget _renderRemoteVideo() {
    // check viewer đã join được live chưa.
    if (state.joinedRx.value) {
      return RtcRemoteView.SurfaceView(
        uid: state.broadcasterID.value,
        channelId: state.audienceToken.value.channel_name,
      );
    } else {
      return Container(
        color: Colors.white,
      );
    }
  }

  Widget _connectLiveWidget() {
    return Container(
      color: Colors.black,
      child: Center(
        child: Text(
          'Đang kết nối',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'Quicksand',
            fontSize: 20,
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  Widget _endLiveWidget() {
    return Container(
      color: Colors.black,
      child: Center(
        child: Text(
          'Đã kết thúc live stream',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'Quicksand',
            fontSize: 20,
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  Widget _pauseLiveWidget() {
    return Container(
      color: Colors.black.withOpacity(0.5),
      child: Center(
        child: Text(
          'Tạm dừng live stream',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'Quicksand',
            fontSize: 20,
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    controller.leaveLivestream(fromTab: fromTab);
    super.dispose();
  }

  @override
  AudienceLivestreamLogic createController() => AudienceLivestreamLogic();

  void _addItem(CommentPresenter item) {
    final int _index = state.comments.length;
    print('quanth: addItem sender= ${item.toString()}, state.comments.length= ${state.comments.length}');
    state.comments.insert(_index, item);
    double a =
        _scrollController.position.maxScrollExtent - _scrollController.offset;
    _commentKey.currentState.insertItem(_index);
    Timer(
      Duration(milliseconds: 220),
          () {
        if (a <= 54.0) {
          state.showNewCommentRx.value = false;
          _scrollAnimateToEnd(_scrollController);
        } else {
          state.showNewCommentRx.value = true;
        }
      },
    );
    logger.d("quanth: scroll 2 $a");
  }

  void _addItems(List<CommentPresenter> items){
    final int _index = 0;
    // This is a bit of a hack because currentState doesn't have
    // an insertAll() method.
    for (int offset = 0; offset < items.length; offset++) {
      _commentKey.currentState.insertItem(_index + offset);
    }
    Timer(
      Duration(milliseconds: 220),
          () {
            state.showNewCommentRx.value = false;
            _scrollAnimateToEnd(_scrollController);
      },
    );
  }

  void _scrollAnimateToEnd(ScrollController controller) {
    Future.delayed(const Duration(milliseconds: 400)).then((_) {
      try {
        controller
            .animateTo(
          controller.position.maxScrollExtent,
          duration: const Duration(seconds: 1),
          curve: Curves.fastOutSlowIn,
        )
            .then((value) {
          controller.animateTo(
            controller.position.maxScrollExtent,
            duration: const Duration(milliseconds: 200),
            curve: Curves.easeInOut,
          );
        });
      } catch (e) {
        print('error on scroll $e');
      }
    });
  }
}
