import 'package:findnear/src/models/response/base_response.dart';
import 'package:findnear/src/pages/story/data/api/story_api.dart';

import 'model/create_story.dart';
import 'model/story_detail.dart';

abstract class StoryRepository {
  Future<BaseResponse<List<StoryGroup>>> getStories({offset = 0, limit = 10});

  Future<BaseResponse<Story>> createStory(CreateStory data);

  Future<BaseResponse<Story>> hideStory(Story data);

  Future<BaseResponse<Story>> unHideStory(Story data);

  Future<BaseResponse<Story>> deleteStory(Story data);

  Future<BaseResponse<Story>> getDetailStory(Story data);
}

class StoryRepositoryImpl extends StoryRepository {
  StoryApi _api;

  StoryRepositoryImpl(this._api);

  @override
  Future<BaseResponse<Story>> createStory(CreateStory data) {
    return _api.createStory(data);
  }

  @override
  Future<BaseResponse<Story>> deleteStory(Story data) {
    // TODO: implement deleteStory
    throw UnimplementedError();
  }

  @override
  Future<BaseResponse<Story>> getDetailStory(Story data) {
    // TODO: implement getDetailStory
    throw UnimplementedError();
  }

  @override
  Future<BaseResponse<List<StoryGroup>>> getStories({offset = 0, limit = 10}) {
    return _api.getStories(offset: offset, limit: limit);
  }

  @override
  Future<BaseResponse<Story>> hideStory(Story data) {
    // TODO: implement hideStory
    throw UnimplementedError();
  }

  @override
  Future<BaseResponse<Story>> unHideStory(Story data) {
    // TODO: implement unHideStory
    throw UnimplementedError();
  }
}
