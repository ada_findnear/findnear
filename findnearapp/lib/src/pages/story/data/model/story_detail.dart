import 'package:findnear/src/models/media.dart';
import 'package:findnear/src/models/user.dart';

/**
 * This class contain all info about story
 * @see [Story]
 */
class Story {
  var id;
  var createdAt;
  var userId;
  List<Media> media;
  User creator;

  Story({this.id, this.createdAt, this.userId, this.media, this.creator});

  static Story fromMap(Map<String, dynamic> map) {
    var mediaMap = map['media'];
    var media = (mediaMap is List)
        ? mediaMap.map((e) => Media.fromJSON(e)).toList()
        : <Media>[];
    User user;
    if (map.containsKey('creator')) user = User.fromJSON(map['creator']);
    return Story(
        id: map['id'],
        creator: user,
        createdAt: map['created_at'],
        userId: map['user_id'],
        media: media);
  }
}

/**
 * StoryGroup present for 'An user have multiple stories'
 */
class StoryGroup {
  User creator;
  List<Story> stories;

  StoryGroup({this.creator, this.stories});

  static StoryGroup fromMap(Map<String, dynamic> map) {
    var storiesJson = map['stoires'];
    List<Story> stories;
    if (storiesJson is List && storiesJson.isNotEmpty) {
      stories = storiesJson.map((e) => Story.fromMap(e)).toList();
    } else {
      stories = <Story>[];
    }
    return StoryGroup(creator: User.fromJSON(map['creator']), stories: stories);
  }
}
