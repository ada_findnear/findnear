import 'package:flutter/cupertino.dart';

/**
 * Use this class to create story request
 */
class CreateStory {
  String mediaPath;

  CreateStory({@required this.mediaPath});
}
