import 'package:dio/dio.dart';
import 'package:findnear/src/models/response/base_response.dart';
import 'package:findnear/src/pages/story/data/model/create_story.dart';
import 'package:findnear/src/pages/story/data/model/story_detail.dart';
import 'package:findnear/src/repository/base/base_repository.dart';

abstract class StoryApi {
  Future<BaseResponse<List<StoryGroup>>> getStories({offset = 0, limit = 10});

  Future<BaseResponse<Story>> createStory(CreateStory data);

  Future<BaseResponse<Story>> hideStory(Story data);

  Future<BaseResponse<Story>> unHideStory(Story data);

  Future<BaseResponse<Story>> deleteStory(Story data);

  Future<BaseResponse<Story>> getDetailStory(Story data);
}

class StoryApiImpl extends BaseApi implements StoryApi {
  @override
  Future<BaseResponse<List<StoryGroup>>> getStories({offset = 0, limit = 10}) async {
    final response = await sendRequest(
      GET('feed/moment'),
      authenticate: true,
      queryParameters: {
        'limit': limit,
        'offset': offset,
      },
    );
    return BaseResponse<List<StoryGroup>>.fromJson(response,
        (data) => data is List ? data.map((e) => StoryGroup.fromMap(e)).toList() : <StoryGroup>[]);
  }

  @override
  Future<BaseResponse<Story>> createStory(CreateStory data) async {
    if (data.mediaPath?.isNotEmpty != true) {
      throw 'You should select at least 1 image';
    }
    var entry = MapEntry(
      'file',
      MultipartFile.fromFileSync(data.mediaPath,
          filename: data.mediaPath.split("/").last),
    );
    var formData = FormData();
    formData.files.add(entry);
    final response = await sendRequest(
      POST('moment'),
      body: formData,
      authenticate: true,
    );
    return BaseResponse<Story>.fromJson(
        response, (data) => Story.fromMap(data));
  }

  @override
  Future<BaseResponse<Story>> deleteStory(Story data) {
    // TODO: implement deleteStory
    throw UnimplementedError();
  }

  @override
  Future<BaseResponse<Story>> getDetailStory(Story data) {
    // TODO: implement getDetailStory
    throw UnimplementedError();
  }

  @override
  Future<BaseResponse<Story>> hideStory(Story data) {
    // TODO: implement hideStory
    throw UnimplementedError();
  }

  @override
  Future<BaseResponse<Story>> unHideStory(Story data) {
    // TODO: implement unHideStory
    throw UnimplementedError();
  }
}
