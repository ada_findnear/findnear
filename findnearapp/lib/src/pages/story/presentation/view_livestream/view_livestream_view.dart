import 'package:findnear/src/models/entities/livestream_info_entity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../commons/app_colors.dart';

class ViewLivestreamPage extends StatelessWidget {
  final Function(int) onItemPressed;
  final List<LiveStreamInfoEntity> listLive;

  const ViewLivestreamPage({
    Key key,
    this.listLive,
    this.onItemPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _buildLivestreamWidget();
  }

  Widget _buildLivestreamWidget() {
    return Container(
      color: AppColors.white,
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
            child: Container(
              height: 66.w,
              width: double.infinity,
              child: ListView.separated(
                shrinkWrap: true,
                padding: EdgeInsets.fromLTRB(12.w, 0, 12.w, 0),
                scrollDirection: Axis.horizontal,
                itemBuilder: _buildItem,
                separatorBuilder: _buildStoryItemDivider,
                itemCount: listLive.length,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildItem(BuildContext context, int index) {
    final entity = listLive[index];
    return GestureDetector(
      onTap: () async {
        onItemPressed?.call(entity.user_id);
      },
      child: Container(
        width: 60.w,
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                  padding: EdgeInsets.only(bottom: 2),
                  child: _buildAvatar(entity.user.image.url),
                ),
                Positioned.fill(
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      decoration: BoxDecoration(
                        color: AppColors.red,
                        borderRadius: BorderRadius.circular(3),
                      ),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 5, vertical: 2),
                      child: Text(
                        "LIVE",
                        style: Theme.of(context)
                            .textTheme
                            .headline6
                            .merge(TextStyle(
                          color: AppColors.white,
                          fontSize: 6,
                        )),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 2),
            Text(
              entity.user.name,
              style: TextStyle(
                  fontFamily: "Quicksand",
                  fontSize: 11,
                  fontWeight: FontWeight.normal,
                  color: Colors.black),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildStoryItemDivider(BuildContext context, int index) {
    return SizedBox(width: 5.w);
  }

  Widget _buildAvatar(String url) {
    return Container(
      width: 42,
      height: 42,
      child: CircleAvatar(
        backgroundImage:NetworkImage(url),
        radius: 45 / 2,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(45 / 2),
        border: Border.all(color: Color(0xffC83E37)),
      ),
    );
    ;
  }
}

