import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/widgets/shimmer/app_shimmer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class ViewLivestreamLoading extends StatelessWidget {

  const ViewLivestreamLoading({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _buildLivestreamWidget();
  }

  Widget _buildLivestreamWidget() {
    return Container(
      color: AppColors.white,
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 2,
            width: MediaQuery.of(Get.context).size.width,
            margin: EdgeInsets.symmetric(vertical: 5),
            color: Color(0xFFEDEDED),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: Container(
              height: 62.w,
              width: double.infinity,
              child: ListView.separated(
                shrinkWrap: true,
                padding: EdgeInsets.fromLTRB(12.w, 0, 12.w, 0),
                scrollDirection: Axis.horizontal,
                itemBuilder: _buildItem,
                separatorBuilder: _buildStoryItemDivider,
                itemCount: 10,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildItem(BuildContext context, int index) {
    return Column(
      children: [
        AppShimmer(
            width: 42.w ,
            height: 42.w,
            cornerRadius: 45.w / 2),
        SizedBox(height: 8),
        AppShimmer(
            width: 42.w ,
            height: 10.w,
            cornerRadius: 10.w / 2),
      ],
    );
  }

  Widget _buildStoryItemDivider(BuildContext context, int index) {
    return SizedBox(width: 5.w);
  }
}

