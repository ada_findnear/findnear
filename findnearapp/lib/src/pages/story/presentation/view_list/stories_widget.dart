import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/CircularAvatarWidget.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/bottom_sheet_option.dart';
import 'package:findnear/src/pages/story/data/model/story_detail.dart';
import 'package:findnear/src/pages/story/presentation/create_story/create_story.controller.dart';
import 'package:findnear/src/pages/story/presentation/create_story/create_story.screen.dart';
import 'package:findnear/src/pages/story/presentation/view_detail/view_story_detail.dart';
import 'package:findnear/src/pages/story/presentation/view_list/stories_controller.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/media/media_picker_utils.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:findnear/src/widgets/shimmer/app_shimmer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class StoriesWidget extends StatelessWidget {
  final StoriesController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Obx(_buildStoriesWidget);
  }

  Widget _buildStoriesWidget() {
    return Container(
      color: AppColors.white,
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(0, 5, 0,0 ),
            child: Container(
              height: 94.w,
              width: double.infinity,
              child: ListView.separated(
                  shrinkWrap: true,
                  padding: EdgeInsets.fromLTRB(12.w, 0, 12.w, 0),
                  scrollDirection: Axis.horizontal,
                  itemBuilder: controller.state.isLoading.value
                      ? _buildLoadingStoryItem
                      : _buildItem,
                  separatorBuilder: _buildStoryItemDivider,
                  itemCount: controller.state.isLoading.value
                      ? 4
                      : controller.state.availableStoryGroupCount + 1),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildLoadingStoryItem(BuildContext context, int index) {
    if (index == 0)
      return _buildCreateStoryItem(context, index);
    else
      return AppShimmer(width: 50.w, height: 70.w, cornerRadius: 10.w);
  }

  Widget _buildStoryItemDivider(BuildContext context, int index) {
    return SizedBox(width: 16.w);
  }

  Widget _buildItem(BuildContext context, int index) {
    if (index == 0)
      return _buildCreateStoryItem(context, index);
    else {
      var item = controller.state.storyGroup[index - 1];
      return _buildStoryItem(context, index - 1, item);
    }
  }

  void _openCreateStoryScreen() async {
    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    }
    DialogHelper.showBottomSheetOptions(
        context: Get.context,
        options: [
          BottomSheetOption(
              iconPath: AppImages.icCamera,
              text: S.of(Get.context).cameraButton),
          BottomSheetOption(
              iconPath: AppImages.icImage,
              text: S.of(Get.context).galleryButton)
        ],
        onTapAtIndex: (i) {
          switch (i) {
            case 0:
              _openCamera();
              break;
            case 1:
              _openGallery();
          }
        });
  }

  void _openGallery() async {
    var file = await MediaPickerUtils.pickPhotoForStory(0);
    if (file != null && file.isNotEmpty) {
      var controllerPreview = Get.put(CreateStoryController());
      var res = await Get.to(
          CreateStoryScreen(mediaFile: PickedFile(file.first.path)));
      if (res != null) controller.addNewStory(res);
      controllerPreview.dispose();
    }
  }

  void _openCamera() async {
    PickedFile file = await ImagePicker().getImage(source: ImageSource.camera);
    if (file != null) {
      var controllerPreview = Get.put(CreateStoryController());
      var res = await Get.to(CreateStoryScreen(mediaFile: file));
      if (res != null) controller.addNewStory(res);
      controllerPreview.dispose();
    }
  }

  Widget _buildCreateStoryItem(BuildContext context, int index) {
    return InkWell(
      onTap: _openCreateStoryScreen,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Stack(
            children: [
              Container(
                width: 50.w,
                height: 70.w,
                child: Center(
                  child: AppImage(
                    AppImages.icCamera,
                    color: AppColors.red,
                    width: 21.w,
                    height: 17.w,
                  ),
                ),
                decoration: BoxDecoration(
                    color: AppColors.pink,
                    borderRadius: BorderRadius.all(Radius.circular(10))),
              )
            ],
          ),
          SizedBox(height: 5,),
          Text(
            S.of(context).label_create_new,
            style: TextStyle(
                color: Color(0xFF333333),
                fontSize: 11,
                fontFamily: 'Quicksand'),
          )
        ],
      ),
    );
  }

  Widget _buildStoryItem(BuildContext context, int index, StoryGroup item) {
    return InkWell(
      onTap: () {
        if (!isLoggedIn) {
          NavigatorUtils.toLogin();
          return;
        }
        Get.to(ViewStoryDetailSlide(controller.state.storyGroup, index));
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 50.w,
            height: 70.w,
            decoration: BoxDecoration(
                color: Colors.transparent,
                border: Border.all(
                  color: Color(0xFFFF0000),
                ),
                borderRadius: BorderRadius.circular(10)
            ),
            child: CachedNetworkImage(
                imageBuilder: (context, provider) => Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                          image: provider,
                          fit: BoxFit.cover,
                        ),
                        borderRadius:
                            BorderRadius.all(Radius.circular(10)))),
                imageUrl: () {
                  if (item.stories.isNotEmpty)
                    return item.stories.first.media.first.thumb;
                  else
                    '';
                }()),
          ),
          SizedBox(height: 5,),
          Text(
            Helper.limitString(
              item.creator.name,limit: 12),
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 11,
                color: Color(0xFF333333),
                fontFamily: 'Quicksand'),),
        ],
      ),
    );
  }
}
