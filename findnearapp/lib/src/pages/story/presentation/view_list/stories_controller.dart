import 'package:findnear/src/pages/story/data/model/story_detail.dart';
import 'package:findnear/src/pages/story/presentation/view_list/stories_state.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

abstract class StoriesController extends GetxController {
  StoriesState state;

  void addNewStory(Story model);
  void refreshData();
}
