import 'package:findnear/src/pages/story/data/model/story_detail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';

class StoriesState {
  RxList<StoryGroup> storyGroup = RxList<StoryGroup>();

  int get availableStoryGroupCount {
    if (storyGroup.isEmpty) return 0;
    if (storyGroup.first.stories.isNotEmpty)
      return storyGroup
          .length; // backend return empty group when current user doesn't have any story
    else
      return storyGroup.length - 1;
  }

  RxInt currentOffset = 0.obs;
  Rx<bool> isLoading = false.obs;
  Rx<bool> isLoadingMore = false.obs;
  RxBool canLoadMore = true.obs;

  StoriesState();
}
