import 'package:dio/dio.dart';
import 'package:findnear/src/pages/story/data/model/story_detail.dart';
import 'package:findnear/src/pages/story/data/story_repository.dart';
import 'package:findnear/src/pages/story/presentation/view_list/stories_controller.dart';
import 'package:findnear/src/pages/story/presentation/view_list/stories_state.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';

class StoriesControllerImpl extends StoriesController {
  final StoryRepository storyRepo;

  StoriesControllerImpl(this.storyRepo);

  @override
  StoriesState state = StoriesState();

  @override
  void onReady() {
    _getStories(0);
    super.onReady();
  }

  @override
  void refreshData() {
    _getStories(0);
  }

  void _notifyLoading(bool isLoading) {
    state.isLoading.value = isLoading;
  }

  void _getStories(int offset) async {
    _notifyLoading(true);
    try {
      var result = await storyRepo.getStories(offset: offset);
      state.storyGroup.value = result.data;
    } catch (e) {
      if (e is DioError) {
        DialogHelper.showError(e.error);
      } else
        DialogHelper.showError(e.toString());
    } finally {
      _notifyLoading(false);
    }
  }

  @override
  void addNewStory(Story model) {
    bool existGroup = state.storyGroup.isNotEmpty &&
        state.storyGroup.first.creator.id == model.creator.id;
    if (existGroup) {
      var first = state.storyGroup.first;
      first.stories.insert(0, model);
      state.storyGroup.removeAt(0);
      state.storyGroup.insert(0, first);
    } else {
      var newGroup = StoryGroup(creator: model.creator, stories: [model]);
      state.storyGroup.insert(0, newGroup);
    }
  }
}
