
import 'dart:async';

import 'package:get/get.dart';

import '../../data/model/story_detail.dart';


class ViewStoryDetailController extends GetxController {
  Rx<StoryGroup> storyGroup;
  RxInt currentDisplayIndex = 0.obs;
  RxBool isFinished = false.obs;
  Timer _timer;
  RxInt currentDisplayProgress = 0.obs; // [0-100]

  void setStoryGroup(StoryGroup group) {
    storyGroup = group.obs;
    if (group != null && group.stories.isNotEmpty) {
      _startCountDown();
    }
  }

  void next() {
    _doNext();
  }

  void _doNext() {
    if (currentDisplayIndex < storyGroup.value.stories.length - 1) {
      currentDisplayProgress.value = 0;
      currentDisplayIndex++;
      _startCountDown();
    } else {
      isFinished.value = true;
    }
  }

  void _doPrevious() {
    isFinished.value = false;
    if (currentDisplayIndex > 0) {
      currentDisplayProgress.value = 0;
      currentDisplayIndex--;
      _startCountDown();
    }
  }

  void previous() {
    _doPrevious();
  }

  int _TIME_PER_VIEW = 3000; // mills

  void _startCountDown() {
    _timer?.cancel();
    _timer =
        Timer.periodic(Duration(milliseconds: _TIME_PER_VIEW ~/ 100), (timer) {
          currentDisplayProgress++;
          if (currentDisplayProgress == 100) {
            _doNext();
          } else
            _startCountDown();
        });
  }
}