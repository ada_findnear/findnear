import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/helpers/date_utils.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/story/presentation/view_detail/view_story_detail_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../commons/app_colors.dart';
import '../../../../commons/app_images.dart';
import '../../../../elements/CircularAvatarWidget.dart';
import '../../../../widgets/common.dart';
import '../../data/model/story_detail.dart';

class ViewStoryDetailSlide extends StatefulWidget {
  final List<StoryGroup> _storyGroups;
  final int _clickedIndex;

  ViewStoryDetailSlide(this._storyGroups, this._clickedIndex);

  @override
  State<StatefulWidget> createState() {
    return _ViewStoryDetailSlideState();
  }
}

class _ViewStoryDetailSlideState extends State<ViewStoryDetailSlide> {
  PageController controller;

  @override
  void initState() {
    controller = PageController(initialPage: widget._clickedIndex);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SafeArea(
          child: Stack(
        children: [
          PageView.builder(
              controller: controller,
              itemBuilder: (context, index) {
                return ViewStoryDetail(widget._storyGroups[index], () {
                  if (controller.page.toInt() == widget._storyGroups.length - 1)
                    Get.back();
                  else
                    controller.nextPage(
                        duration: Duration(milliseconds: 200),
                        curve: Curves.linear);
                });
              },
              itemCount: widget._storyGroups.length),
          Positioned(
              right: 16.w,
              top: 36.w,
              child: InkWell(
                onTap: () {
                  Get.back();
                },
                child: AppImage(
                  AppImages.icClose,
                  color: AppColors.white,
                  width: 36.w,
                ),
              ))
        ],
      )),
    );
  }
}

class ViewStoryDetail extends GetWidget {
  final ViewStoryDetailController _controller = ViewStoryDetailController();
  final PageController _pageController = PageController();
  final VoidCallback onEndCallback;

  ViewStoryDetail(StoryGroup storyGroup, this.onEndCallback) {
    _controller.setStoryGroup(storyGroup);
    ever(
        _controller.currentDisplayIndex,
        (index) => _pageController.animateToPage(index,
            curve: Curves.linear, duration: Duration(milliseconds: 200)));
    ever(
        _controller.isFinished,
        (isFinished) => () {
              if (isFinished) onEndCallback.call();
            }());
  }

  Widget _buildUserInfoWidget(BuildContext context, User creator) => Row(
        children: [
          CircularAvatarWidget(
            user: creator,
            size: 42.w,
            hasBorder: true,
            hasLiveIndicator: creator.isLiveStreaming,
            replace: true,
          ),
          SizedBox(width: 9.w),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(
                  Helper.limitString(creator.name ?? "", limit: 20),
                  style: Theme.of(context).textTheme.headline6.merge(TextStyle(
                        color: AppColors.white,
                        fontSize: 14.sp,
                      )),
                ),
                Obx(() {
                  final dateTime = AppDateFormatUtils.tryParseUTC(_controller
                      .storyGroup
                      .value
                      .stories[_controller.currentDisplayIndex.value]
                      ?.createdAt);
                  return Text(
                    (dateTime != null)
                        ? AppDateFormatUtils.getVerboseDateTimeRepresentation(
                            dateTime)
                        : "",
                    style:
                        Theme.of(context).textTheme.bodyText1.merge(TextStyle(
                              color: AppColors.gray2,
                              fontSize: 12.sp,
                            )),
                  );
                }),
              ],
            ),
          )
        ],
      );

  Widget _buildProgressItem(BuildContext context, int index) {
    return Obx(() => Container(
          width: (Get.width -
                  12.w * 2 -
                  8.w * (_controller.storyGroup.value.stories.length - 1)) /
              _controller.storyGroup.value.stories.length,
          child: LinearProgressIndicator(
            value: index < _controller.currentDisplayIndex.value
                ? 1
                : (index == _controller.currentDisplayIndex.value
                    ? _controller.currentDisplayProgress / 100
                    : 0),
            color: AppColors.red,
            backgroundColor: AppColors.gray,
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black.withAlpha(20),
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.only(top: 20.w, bottom: 80.w),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                _buildProgress(),
                Padding(
                  padding: EdgeInsets.fromLTRB(16.w, 16.w, 16.w, 16.w),
                  child: _buildUserInfoWidget(
                      context, _controller.storyGroup.value.creator),
                ),
                Flexible(
                    flex: 1,
                    child: Stack(
                      children: [
                        PageView.builder(
                            controller: _pageController,
                            itemBuilder: (context, index) {
                              return Container(
                                width: double.infinity,
                                child: CachedNetworkImage(
                                    imageUrl: _controller.storyGroup.value
                                        .stories[index].media.first.url),
                              );
                            },
                            itemCount:
                                _controller.storyGroup.value.stories.length),
                        Row(
                          children: [
                            Flexible(
                                child: InkWell(onTap: () {
                                  _controller.previous();
                                }),
                                flex: 1),
                            Flexible(
                                child: InkWell(onTap: () {
                                  _controller.next();
                                }),
                                flex: 1)
                          ],
                        )
                      ],
                    ))
              ],
            ),
          ),
        ));
  }

  Widget _buildProgress() {
    return Container(
      padding: EdgeInsets.only(left: 12.w, right: 12.w),
      width: double.infinity,
      height: 4.w,
      child: ListView.separated(
          scrollDirection: Axis.horizontal,
          itemBuilder: _buildProgressItem,
          separatorBuilder: (context, index) {
            return SizedBox(width: 8.w);
          },
          itemCount: _controller.storyGroup.value.stories.length),
    );
  }
}
