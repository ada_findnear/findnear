import 'dart:io';

import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/elements/app_bar_create.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:velocity_x/velocity_x.dart';

import 'create_story.controller.dart';

class CreateStoryScreen extends GetWidget<CreateStoryController> {
  final PickedFile mediaFile;

  CreateStoryScreen({this.mediaFile});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(child: VStack([_itemAppBar(), _itemImage(mediaFile)])),
    );
  }

  Widget _itemAppBar() => AppBarCreate(
        title: S.of(Get.context).create_story,
        onClick: () => controller.postStory(mediaFile.path),
        color: AppColors.red,
      );

  Widget _itemImage(PickedFile media) {
    return Image.file(
      File(media.path),
      fit: BoxFit.cover,
      width: Get.width,
    ).box.make().p(16).expand();
  }
}
