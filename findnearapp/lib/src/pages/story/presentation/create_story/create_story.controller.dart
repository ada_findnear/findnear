import 'package:findnear/src/pages/story/data/model/create_story.dart';
import 'package:findnear/src/pages/story/data/story_repository.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:get/get.dart';

class CreateStoryController extends GetxController {
  StoryRepository _storyRepo = Get.find();

  Future postStory(String path) async {
    DialogHelper.showLoaderDialog();
    try {
      var res = await _storyRepo.createStory(CreateStory(mediaPath: path));
      goBack();
      if (res.data != null) {
        goBack(argument: res.data);
      }
    } catch (e) {
      goBack();
      goBack();
    }
  }
}
