class CreateProduct {
  String id;
  String name;
  double price;
  String description;
  List<String> imagesPath;

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'name': this.name,
      'price': this.price,
      'description': this.description
    };
  }
}
