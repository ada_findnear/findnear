import 'package:dio/dio.dart';
import 'package:findnear/src/models/product.dart';
import 'package:findnear/src/models/response/base_response.dart';
import 'package:findnear/src/pages/product/data/model/create_product.dart';
import 'package:findnear/src/repository/base/base_repository.dart';

abstract class ProductApi {
  Future<BaseResponse<Product>> createProduct(CreateProduct data);
  Future<BaseResponse<Product>> updateProduct(Product data);
}

class ProductApiImpl extends BaseApi implements ProductApi {
  @override
  Future<BaseResponse<Product>> createProduct(CreateProduct data) async {
    var formData = FormData.fromMap(data.toMap());
    if (data.imagesPath?.length != 0) {
      data.imagesPath.forEach((element) {
        var entry = MapEntry(
          'files[]',
          MultipartFile.fromFileSync(element,
              filename: element.split("/").last),
        );
        formData.files.add(entry);
      });
    }
    final response = await sendRequest(
      POST('vnp_products'),
      body: formData,
      authenticate: true,
    );
    return BaseResponse<Product>.fromJson(
        response, (data) => Product.fromJSON(data));
  }

  @override
  Future<BaseResponse<Product>> updateProduct(Product data) {
    // TODO: implement updateProduct
    throw UnimplementedError();
  }
}
