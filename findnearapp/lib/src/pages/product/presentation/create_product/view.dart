import 'dart:io';
import 'package:findnear/generated/l10n.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_bottom_sheets.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/MediaActionSheetWidget.dart';
import 'package:findnear/src/models/category.dart';
import 'package:findnear/src/models/response/product_response.dart';
import 'package:findnear/src/helpers/app_config.dart' as config;
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/pages/shop_detail/logic.dart';
import 'package:findnear/src/utils/media/media_picker_utils.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'logic.dart';
import 'state.dart';

class CreateProductPage extends StatefulWidget {
  static const String ROUTE_NAME = '/CreateProductPage';
  final RouteArgument routeArgument;

  CreateProductPage({this.routeArgument});

  @override
  _CreateProductPageState createState() => _CreateProductPageState();
}

class _CreateProductPageState extends State<CreateProductPage> {
  final CreateProductLogic logic = Get.put(CreateProductLogic());
  final CreateProductState state = Get.find<CreateProductLogic>().state;
  final logicCategory = Get.put(ShopDetailLogic());
  final stateCategory = Get.find<ShopDetailLogic>();
  final _formKey = GlobalKey<FormState>();
  bool isShowProgressDialog = false;
  MediaActionSheetWidget _mediaActionSheetWidget;
  int get shopId => widget.routeArgument.param['shopId'];

  @override
  void initState() {
    _mediaActionSheetWidget = MediaActionSheetWidget(context: context);
    logic.product = widget.routeArgument.param['product'] as Product;
    if (shopId != null) {
      logicCategory.getCategories(shopId: shopId);
    }
    logic.loadProduct();
    ever(state.isLoading, (isLoading) {
      if (isLoading) {
        isShowProgressDialog = true;
        DialogHelper.showLoaderDialog();
      } else if (isShowProgressDialog) {
        Get.back(result: null);
      }
    });
    ever(state.isSuccess, (isSuccess) {
      DialogHelper.showAlertDialog(
          context: Get.context,
          title: 'Thành công',
          okText: 'Đóng',
          okFunction: () {
            Get.back(result: logic.product);
            //Get.back();
          },
          description: (logic.isCreateNew ? "Đăng kí " : "Chỉnh sửa ") +
              "sản phẩm thành công");
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _getAppBar(),
      body: _getBody(),
    );
  }

  @override
  void dispose() {
    Get.delete<CreateProductLogic>();
    super.dispose();
  }

  AppBar _getAppBar() {
    return AppBar(title: _getTitleWidget());
  }

  Widget _getTitleWidget() {
    return Row(
      children: [
        Text(
          logic.product == null ? 'Đăng mới sản phẩm' : 'Chỉnh sửa sản phẩm ',
          style: TextStyle(
            fontSize: 16,
            color: const Color(0xff000000),
            fontWeight: FontWeight.w700,
          ),
        ),
        Spacer(),
        Builder(
          builder: (context) => ElevatedButton(
            onPressed: () {
              // print(_hasImage(context));
              if (_formKey.currentState.validate() && _hasImage(context)) {
                logic.createOrUpdateProduct();
              }
            },
            child: Text(
              logic.product == null ? 'Tạo' : 'Lưu',
              style: TextStyle(
                fontSize: 14,
                color: const Color(0xffffffff),
                fontWeight: FontWeight.w700,
              ),
            ),
            style: ElevatedButton.styleFrom(
                shape: StadiumBorder(), primary: Colors.red),
          ),
        ),
      ],
    );
  }

  Widget _getBody() {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 34, vertical: 16),
        child: Form(
            key: _formKey,
            child: ListView(
              children: [
                _textFieldValue(
                  logic.nameController,
                  'Tên sản phẩm',
                  'Nhập tên sản phẩm',
                  _validateName,
                ),
                SizedBox(height: 10.0),
                _selectCategory(),
                SizedBox(height: 10.0),
                _categoriesSelectWidget(),
                SizedBox(height: 10.0),
                _textFieldValue(logic.priceController, 'Giá sản phẩm',
                    'Nhập giá sản phẩm', _validateName,
                    suffix: 'VND',
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    inputType: TextInputType.numberWithOptions(signed: false)),
                SizedBox(height: 10.0),
                _textFieldValue(
                  logic.descriptionController,
                  'Mô tả (nếu có)',
                  'Nhập nội dung mô tả',
                  null,
                ),
                Container(padding: EdgeInsets.only(top: 6)),
                _addShowImage(),
              ],
            )));
  }

  Widget _textFieldValue(TextEditingController controller, String title,
      String hintText, FormFieldValidator<String> validator,
      {String suffix,
      List<TextInputFormatter> inputFormatters,
      TextInputType inputType = TextInputType.text}) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '$title',
            style: TextStyle(
                color: Color(0xff333333),
                fontSize: 14,
                fontWeight: FontWeight.bold),
          ),
          TextFormField(
            controller: controller,
            maxLines: null,
            keyboardType: inputType,
            inputFormatters: inputFormatters,
            decoration: InputDecoration(
                hintText: hintText,
                hintStyle: TextStyle(color: Color(0xff333333), fontSize: 14),
                enabledBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(width: 1, color: AppColors.dividerColor),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                      width: 1, color: config.CustomColors().mainColor(0.3)),
                ),
                errorBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                      width: 1, color: config.CustomColors().mainColor(1)),
                ),
                suffixIcon: suffix != null
                    ? Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Text(suffix ?? ''),
                      )
                    : null,
                suffixStyle: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 14,
                  color: const Color(0xff333333),
                )),
            // style: themeState.textLightLabel(color: AppColors.border),
            onChanged: (String value) {
              _formKey.currentState?.validate();
              _formKey.currentState?.save();
            },
            validator: validator,
          ),
        ],
      ),
    );
  }

  Widget _categoriesSelectWidget() {
    return Obx(() => Wrap(
          spacing: 10,
          children: logic.categoriesSelected
              .map((element) => Container(
                    child: Chip(
                      backgroundColor: AppColors.red400,
                      label: Text(
                        element.name,
                        style: TextStyle(color: AppColors.textBlack),
                      ),
                      onDeleted: () => logic.removeCategory(element),
                      deleteIconColor: AppColors.white,
                    ),
                  ))
              .toList(),
        ));
  }

  Widget _selectCategory() {
    return OutlinedButton(
      style: OutlinedButton.styleFrom(
          side: BorderSide.none, padding: EdgeInsets.zero),
      onPressed: () {
        showAppBottomSheet(
          context: context,
          child: Obx(() {
            return Container(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          S.of(context).select_category,
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        TextButton(
                          onPressed: () {
                            logic.onDone();
                          },
                          child: Row(
                            children: [
                              Icon(
                                Icons.check,
                                size: 18,
                                color: AppColors.red,
                              ),
                              SizedBox(width: 10.0),
                              Text(
                                S.of(context).done,
                                style: TextStyle(color: AppColors.red),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    TextButton(
                      onPressed: () {
                        Get.back();
                        showDialogCreateUpdateCategory();
                      },
                      child: Row(
                        children: [
                          SvgPicture.asset(
                            'assets/svg/ic_add_category.svg',
                          ),
                          SizedBox(width: 10.0),
                          Text(
                            S.of(context).create_category,
                            style: TextStyle(color: AppColors.red),
                          ),
                        ],
                      ),
                      style: TextButton.styleFrom(padding: EdgeInsets.all(0.0)),
                    ),
                    Divider(
                      color: AppColors.grayBorder,
                      thickness: 1,
                    ),
                    ListView.separated(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (_, index) {
                        return GestureDetector(
                          onTap: index == 0
                              ? () {
                                  if (!logic.categories
                                      .map((e) => e.id)
                                      .toList()
                                      .contains(
                                          logicCategory.categories[index].id)) {
                                    logic.addCategory(
                                        logicCategory.categories[index]);
                                  } else {
                                    logic.removeCategory(
                                        logicCategory.categories[index]);
                                  }
                                }
                              : () {
                                  Get.back();
                                  showDialogCreateUpdateCategory(
                                      category:
                                          logicCategory?.categories[index]);
                                },
                          child: Row(
                            children: [
                              Icon(
                                Icons.folder_open_outlined,
                                size: 18,
                                color: AppColors.textBlack,
                              ),
                              SizedBox(width: 10.0),
                              Expanded(
                                  child: Text(
                                      logicCategory.categories[index]?.name ??
                                          '')),
                              Obx(
                                () => Checkbox(
                                  value: logic.categories
                                      ?.map((e) => e.id)
                                      ?.toList()
                                      ?.contains(
                                          logicCategory.categories[index]?.id),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(4.0)),
                                  side: BorderSide(
                                    width: 1.0,
                                  ),
                                  checkColor: AppColors.white,
                                  onChanged: (v) {
                                    if (v) {
                                      logic.addCategory(
                                          logicCategory.categories[index]);
                                    } else {
                                      logic.removeCategory(
                                          logicCategory.categories[index]);
                                    }
                                  },
                                ),
                              ),
                              Obx(() => logic.categories
                                      .map((e) => e.id)
                                      .toList()
                                      .contains(
                                          logicCategory.categories[index]?.id)
                                  ? InkWell(
                                      onTap: () {
                                        logic.onDeleteCategory(
                                            c: logicCategory.categories[index]);
                                      },
                                      child:
                                          SvgPicture.asset(AppImages.icDelete))
                                  : SizedBox.shrink()),
                            ],
                          ),
                        );
                      },
                      separatorBuilder: (_, index) {
                        return Divider(color: AppColors.grayBorder);
                      },
                      itemCount: logicCategory.categories?.length ?? 0,
                    ),
                  ],
                ),
              ),
            );
          }),
        );
      },
      child: Container(
        decoration: BoxDecoration(
            border: Border(
          bottom: BorderSide(width: 1, color: AppColors.grayDark),
        )),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              S.of(context).category,
              style: TextStyle(color: AppColors.textBlack),
            ),
            SizedBox(height: 10.0),
            Row(
              children: [
                Expanded(
                    child: Text(
                  S.of(context).select_category,
                  style: TextStyle(color: AppColors.gray2),
                )),
                Icon(
                  Icons.keyboard_arrow_down,
                  color: AppColors.gray2,
                  size: 16,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _contentDialogAddCategory({Category category}) {
    logic.loadCategory(category);
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        _textFieldValue(
          logic.categoryNameController,
          '',
          S.of(context).enter_name_category,
          _validateName,
        ),
      ],
    );
  }

  void _pickProductImages() async {
    var media =
        await MediaPickerUtils.pickPhotoForProduct(logic.state?.images?.length);
    if (media != null && media.isNotEmpty) {
      logic.addImages(media.map((e) => e.path).toList());
    }
  }

  Widget _addShowImage() {
    return Obx(() {
      return Container(
          margin: EdgeInsets.only(top: 20),
          child: Column(
            children: [
              DottedBorder(
                color: Colors.red,
                //color of dotted/dash line
                strokeWidth: 1,
                //thickness of dash/dots
                dashPattern: [4, 4],
                borderType: BorderType.RRect,
                radius: Radius.circular(20),
                //dash patterns, 10 is dash width, 6 is space width
                child: Container(
                  height: 200.w,
                  //height of inner container
                  width: double.infinity,
                  //width to 100% match to parent container.
                  child: Stack(
                    fit: StackFit.expand,
                    children: [
                      Column(
                        children: [
                          Expanded(
                            child: (state.images.isNotEmpty)
                                ? _buildListImages()
                                : Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      if (state.image.isEmpty)
                                        _labelTitle('Chưa có ảnh sản phẩm'),
                                    ],
                                  ),
                          ),
                          SizedBox(height: 12.w),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Visibility(
                              visible: state.images.length <
                                  MediaPickerUtils.MAX_IMAGES_PER_PRODUCT,
                              child: ElevatedButton(
                                onPressed: () {
                                  _pickImage();
                                },
                                child: Text(
                                  S.of(context).add_image,
                                  style: TextStyle(
                                    fontFamily: 'Quicksand',
                                    fontSize: 14,
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                style: ElevatedButton.styleFrom(
                                  shape: StadiumBorder(),
                                  primary: Color(0xFFFFDBDB),
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          ));
    });
  }

  Widget _buildListImages() => GridView.count(
        // Create a grid with 2 columns. If you change the scrollDirection to
        // horizontal, this produces 2 rows.
        crossAxisCount: 3,
        shrinkWrap: true,
        // Generate 100 widgets that display their index in the List.
        children: List.generate(state.images.length, (index) {
          return _buildImageWidget(context, index);
        }),
      );
  Widget _buildImageWidget(BuildContext context, int index) => Stack(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              child: () {
                var path = state.images[index];
                if (path.startsWith('http'))
                  return Container(
                    width: 100.0,
                    height: 100.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      image: DecorationImage(
                        image: CachedNetworkImageProvider(path),
                        fit: BoxFit.cover,
                      ),
                    ),
                  );
                else
                  return Image.file(
                    File(path),
                    width: 100,
                    height: 100,
                    fit: BoxFit.cover,
                  );
              }(),
            ),
          ),
          Positioned(
              top: 0.w,
              right: 0.w,
              child: InkWell(
                onTap: () {
                  logic.removeImageAt(index);
                },
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Center(
                    child: Container(
                        decoration: BoxDecoration(
                            color: AppColors.red, shape: BoxShape.circle),
                        child: Icon(
                          Icons.close,
                          color: AppColors.white,
                          size: 20.w,
                        )),
                  ),
                ),
              ))
        ],
      );

  Widget _getDefaultImageWidget() {
    if (logic.product != null &&
        logic.product.media.length > 0 &&
        logic.product.media[0] != null) {
      return Container(
        width: 100.0,
        height: 100.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          image: DecorationImage(
            image: CachedNetworkImageProvider(logic.product.media[0].url),
            fit: BoxFit.cover,
          ),
        ),
      );
    }
    return MaterialButton(
      onPressed: () => _pickImage(),
      color: Color(0xFFFFDBDB),
      child: SvgPicture.asset(
        'assets/svg/ic_image.svg',
        color: Colors.red,
      ),
      padding: EdgeInsets.all(16),
      shape: CircleBorder(),
    );
  }

  void showDialogCreateUpdateCategory({Category category}) {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(24.0),
          ),
        ),
        actionsAlignment: MainAxisAlignment.center,
        content: _contentDialogAddCategory(category: category),
        title: Stack(
          children: [
            Align(
              alignment: Alignment.center,
              child: Text(
                S.of(context).create_category,
                style: TextStyle(
                  color: AppColors.textBlack,
                ),
              ),
            ),
            Positioned(
              right: 0,
              top: 0,
              child: InkWell(
                  onTap: () {
                    Get.back();
                  },
                  child: Icon(Icons.close)),
            )
          ],
        ),
        actions: [
          Container(
            width: MediaQuery.of(context).size.width / 4,
            height: 32,
            child: OutlinedButton(
              style: OutlinedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(24.0),
                ),
              ),
              onPressed: () {
                Get.back();
              },
              child: Text(S.of(context).cancel,
                  style: TextStyle(color: AppColors.gray)),
            ),
          ),
          Container(
            height: 32,
            width: MediaQuery.of(context).size.width / 4,
            child: OutlinedButton(
              style: OutlinedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(24.0),
                ),
                backgroundColor: AppColors.red,
              ),
              onPressed: () {
                logic.createOrUpdateCategory(c: category);
              },
              child: Text(
                  logic.isCreateNewCategory
                      ? S.of(context).create
                      : S.of(context).edit,
                  style: TextStyle(color: AppColors.white)),
            ),
          ),
        ],
      ),
    ).then((value) {
      if (value != null) {
        logicCategory?.categories = value;
      }
    });
  }

  _pickImage() {
    _editAvatar(context, logic.state.image);
  }

  void _editAvatar(BuildContext context, String url) async {
    _mediaActionSheetWidget.showActionSheet(
        '', [S.of(context).takePhoto, S.of(context).chooseFromGrallery],
        (int index) {
      if (index == 0) {
        _openCamera((pickedFile) {
          logic.addImages([pickedFile.path]);
        });
      }
      if (index == 1) {
        _pickProductImages();
      }
    });
  }

  void _openCamera(OnPickImageCallback onPickImageCallback) {
    _mediaActionSheetWidget.pickImageDialog(ImageSource.camera,
        (PickedFile pickedFile) {
      // setState(() {
      onPickImageCallback(pickedFile);
      // });
    });
  }

  // void _openGrallery(OnPickImageCallback onPickImageCallback) {
  //   _mediaActionSheetWidget.pickImageDialog(ImageSource.gallery,
  //       (PickedFile pickedFile) {
  //     // setState(() {
  //     onPickImageCallback(pickedFile);
  //     // });
  //   });
  // }

  Widget _labelTitle(String name) {
    return Text(name,
        textAlign: TextAlign.left,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          fontSize: 14,
          color: const Color(0xff333333),
          fontWeight: FontWeight.normal,
        ));
  }

  String _validateName(String value) {
    if (value == null || value.isEmpty) {
      return 'Vui lòng không bỏ trống';
    }
    return null;
  }

  bool _hasImage(BuildContext context) {
    if (state.images
            .isEmpty /*  &&
        (logic.product.media == null || logic.product.media.length == 0) */
        ) {
      DialogHelper.showAlertDialog(
        context: context,
        title: 'Thiếu thông tin',
        description: 'Vui lòng chọn ảnh sản phẩm',
        okText: 'Close',
        okFunction: () {},
      );
      return false;
    }
    return true;
  }
}
