import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/models/category.dart';
import 'package:findnear/src/pages/shop_detail/logic.dart';
import 'package:findnear/src/repository/category_reposity.dart';
import 'package:findnear/src/repository/shop_repository.dart';
import 'package:findnear/src/models/response/product_response.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'state.dart';

class CreateProductLogic extends GetxController {
  final state = CreateProductState();
  final ShopsRepository shopsRepository = Get.find();
  final CategoryResposity categoryResposity = Get.find();
  TextEditingController nameController = TextEditingController(text: "");
  TextEditingController priceController = TextEditingController(text: "");
  TextEditingController categoryNameController =
      TextEditingController(text: "");
  TextEditingController descriptionController = TextEditingController(text: "");

  ShopDetailLogic shopDetailLogic = Get.find();
  Product product;
  Category category;
  bool isCreateNew = true;
  bool isCreateNewCategory = true;
  List<String> addedImages = <String>[];
  List<String> removedImages = <String>[];
  final categories = List<Category>.empty().obs;

  final categoriesSelected = List<Category>.empty().obs;

  void loadProduct() async {
    isCreateNew = product == null;
    if (product != null) {
      state.addImages((product.media.map((e) => e.url).toList()));
      nameController = TextEditingController(text: product.name);
      priceController = TextEditingController(text: product.price.toString());
      descriptionController = TextEditingController(text: product.description);
      categories.addAll(product.categories);
      categoriesSelected.value = categories;
    }
  }

  void loadCategory(Category categorySelected) {
    isCreateNewCategory = categorySelected == null;
    categoryNameController.clear();
    if (categorySelected != null) {
      categoryNameController =
          TextEditingController(text: categorySelected.name);
    }
  }

  void createOrUpdateProduct() async {
    DialogHelper.showLoaderDialog();
    try {
      Product result;
      if (product == null) {
        result = await shopsRepository.createProduct(
          id: product == null ? 0 : product.id,
          name: nameController.text,
          price: priceController.text,
          description: descriptionController.text,
          categoriesId: categoriesSelected.map((e) => int.parse(e.id)).toList(),
          imagesPath: state.images,
        );
      } else {
        product.name = nameController.text;
        product.price = double.parse(priceController.text);
        product.description = descriptionController.text;
        product.categories = categoriesSelected;
        result = await shopsRepository.updateProduct(
          product,
          addedImages,
          removedImages,
          categoriesSelected.map((e) => int.parse(e.id)).toList(),
        );
      }
      product = result;
      Get.back();
      DialogHelper.showAlertDialog(
          context: Get.context,
          title: 'Thành công',
          okText: 'Đóng',
          okFunction: () {
            Get.back(result: product);
          },
          description: (isCreateNew ? "Đăng kí " : "Chỉnh sửa ") +
              "sản phẩm thành công");
    } catch (e) {
      Get.back();
      print(e);
      Get.showSnackbar(GetBar(
        title: S.of(Get.context).an_error_occurred,
        message: S.of(Get.context).please_try_again_msg,
      ));
    }
  }

  void createOrUpdateCategory({Category c}) async {
    DialogHelper.showLoaderDialog();
    try {
      Category result;
      if (c == null) {
        result = await categoryResposity.createCategory(
          categoryNameController.text,
        );
      } else {
        result = await categoryResposity.updateCategory(
            categoryNameController.text, int.parse(c?.id));
      }
      category = result;
      final shop = Get.find<LocalDataManager>().getMyShop();
      Get.back();

      DialogHelper.showAlertDialog(
          context: Get.context,
          title: S.current.success,
          okText: S.current.close,
          okFunction: () async {
            categoryNameController.clear();
            var categories =
                await shopsRepository.getCategories(shopId: shop.id);
            shopDetailLogic.categories = categories.data;
            Get.back(result: categories);
          },
          description: (isCreateNewCategory
                  ? "${S.current.create} "
                  : "${S.current.edit} ") +
              S.current.msg_create_category_success);
    } catch (err) {
      Get.back();
      print(err);
      Get.showSnackbar(GetBar(
        title: S.of(Get.context).an_error_occurred,
        message: S.of(Get.context).please_try_again_msg,
      ));
    }
  }

  void onDeleteCategory({Category c}) async {
    DialogHelper.showConfirmPopup(
      title: S.of(Get.context).deleteCategory,
      description: S.of(Get.context).msg_confirm_delete_category,
      okBtnPressed: () async {
        DialogHelper.showLoaderDialog();
        try {
          await categoryResposity.deleteCategory(int.parse(c?.id));
          final shop = Get.find<LocalDataManager>().getMyShop();
          Get.back();
          removeCategory(c);
          removeCategorySelected(c);
          DialogHelper.showAlertDialog(
              context: Get.context,
              title: S.current.success,
              okText: S.current.close,
              okFunction: () async {
                var categories =
                    await shopsRepository.getCategories(shopId: shop.id);
                shopDetailLogic.categories = categories.data;
                Get.back(result: categories);
              },
              description: "${S.current.delete} " +
                  S.current.msg_create_category_success);
        } catch (err) {
          Get.back();
          print(err);
          Get.showSnackbar(
            GetBar(
              title: S.of(Get.context).an_error_occurred,
              message: S.of(Get.context).please_try_again_msg,
            ),
          );
        }
      },
    );
  }

  void onDone() {
    categoriesSelected.value = categories;
    Get.back();
  }

  void addCategory(Category categoryId) {
    categories.add(categoryId);
  }

  void removeCategory(Category categoryId) {
    if (categories.map((e) => e.id).toList().contains(categoryId.id)) {
      var cate =
          categories.firstWhere((element) => categoryId.id == element.id);
      categories.remove(cate);
    }
  }

  void removeCategorySelected(Category categoryId) {
    categoriesSelected.remove(categoryId);
  }

  bool validatorData(String value) {
    return value.isNotEmpty;
  }

  void removeImageAt(int index) {
    if (index >= 0 && index < state.images.length) {
      var removePath = state.images[index];
      if (product != null) {
        var adaptMedias = product.media.where((element) =>
            element.url == removePath || element.thumb == removePath);
        if (adaptMedias != null && adaptMedias.isNotEmpty) {
          removedImages.add(adaptMedias.first.id);
        }
      }
      if (addedImages.contains(removePath)) addedImages.remove(removePath);
      state.removeImageAt(index);
    }
  }

  void addImages(List<String> paths) {
    if (paths != null && paths.isNotEmpty) {
      addedImages.addAll(paths);
      state.addImages(paths);
    }
  }
}
