import 'package:get/get.dart';

class CreateProductState {
  CreateProductState() {
    ///Initialize variables
  }

  final _image = ''.obs;
  final isLoading = false.obs;
  final isSuccess = false.obs;

  String get image => _image.value;
  RxList<String> images = <String>[].obs;

  set image(String value) {
    _image.value = value;
  }

  void removeImageAt(int index) {
    images.removeAt(index);
  }

  void addImages(List<String> value) {
    images.addAll(value);
  }
}
