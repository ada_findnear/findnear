import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/rating.dart';
import 'package:findnear/src/models/shop.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';

class RatingState extends BaseState {
  final _isLoading = true.obs;
  final _ratings = List<Rating>.empty().obs;
  Rx<Shop> _shop = Shop().obs;

  RxInt _shopId = 0.obs;

  Shop get shop => _shop.value;

  set shop(Shop s) {
    _shop.value = s;
  }

  int get shopId => _shopId.value;

  set shopId(int id) {
    _shopId.value = id;
  }

  List<Rating> get ratings => _ratings;
  set ratings(List<Rating> lstRating) {
    _ratings.value = lstRating;
  }

  get isLoading => _isLoading.value;

  set isLoading(value) {
    _isLoading.value = value;
  }
}
