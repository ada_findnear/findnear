import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/order_history.dart';
import 'package:get/get.dart';

class RatingOrderState {
  RatingOrderState() {}

  final _image = ''.obs;
  final isLoading = false.obs;
  final isSuccess = false.obs;
  Rx<OrderHistory> _orderHistory = OrderHistory().obs;

  Rx<int> _rating = 0.obs;

  String get image => _image.value;
  RxList<String> images = <String>[].obs;

  OrderHistory get orderHistory => _orderHistory.value;

  set orderHistory(OrderHistory orderHistory) {
    _orderHistory.value = orderHistory;
  }

  int get rating => _rating.value;
  set rating(int rate) {
    _rating.value = rate;
  }

  set image(String value) {
    _image.value = value;
  }

  /// Remove image from review image list
  void removeImageAt(int index) {
    images.removeAt(index);
  }

  /// Add image to the list of review images
  void addImages(List<String> value) {
    images.addAll(value);
  }
}
