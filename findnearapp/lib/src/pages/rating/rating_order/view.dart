import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/MediaActionSheetWidget.dart';
import 'package:findnear/src/models/order_history.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/pages/rating/rating_order/logic.dart';
import 'package:findnear/src/pages/rating/rating_order/state.dart';
import 'package:findnear/src/utils/media/media_picker_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

class RatingOrderView extends StatefulWidget {
  final RouteArgument argument;
  const RatingOrderView({Key key, this.argument}) : super(key: key);

  @override
  _RatingOrderViewState createState() => _RatingOrderViewState();
}

class _RatingOrderViewState extends State<RatingOrderView> {
  final RatingOrderLogic logic = Get.put(RatingOrderLogic());
  final RatingOrderState state = Get.find<RatingOrderLogic>().state;
  MediaActionSheetWidget _mediaActionSheetWidget;
  OrderHistory get orderHistory => widget.argument.param['orderHistory'];

  @override
  void initState() {
    super.initState();
    _mediaActionSheetWidget = MediaActionSheetWidget(context: context);
    state.orderHistory = orderHistory;
  }

  @override
  void dispose() {
    Get.delete<RatingOrderLogic>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          S.of(context).rating_commnet,
          style: TextStyle(color: AppColors.textBlack),
        ),
        titleSpacing: 0,
        leadingWidth: 28,
        leading: InkWell(
          onTap: () {
            Get.back();
          },
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child:
                SvgPicture.asset(AppImages.icArrowBack, width: 24, height: 24),
          ),
        ),
        automaticallyImplyLeading: false,
        actions: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 16.0),
            child: TextButton(
              onPressed: () {
                logic.rating();
              },
              child: Text(
                S.of(context).button_submit_post,
                style: TextStyle(color: AppColors.white),
              ),
              style: TextButton.styleFrom(
                  backgroundColor: AppColors.red,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  padding: EdgeInsets.all(0)),
            ),
          )
        ],
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              // Build order
              _orderBuild(),
              // Rating bar
              SizedBox(
                height: 10.0,
              ),
              _ratingBar(),
              // Text input comment
              SizedBox(
                height: 10.0,
              ),
              _inputComment(),
              SizedBox(
                height: 10.0,
              ),
              // Image
              _buildGridImage()
            ],
          ),
        ),
      ),
    );
  }

  Widget _orderBuild() {
    var formatter = NumberFormat('#,###,000');

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 10.0),
      decoration: BoxDecoration(color: AppColors.pink),
      child: Row(
        children: [
          Container(
            width: 56.0,
            height: 56.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              image: DecorationImage(
                image: CachedNetworkImageProvider(
                    orderHistory?.products?.first?.image?.url ?? ""),
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(
            width: 10.0,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  orderHistory?.products?.first?.name ?? "",
                  style: TextStyle(fontSize: 14, color: AppColors.textBlack),
                ),
                SizedBox(
                  height: 8.0,
                ),
                Row(
                  children: [
                    Expanded(
                        child:
                            Text("${formatter.format(orderHistory?.total)}đ")),
                    Expanded(
                        child: Text('Số lượng: ${orderHistory?.numberItem}')),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _ratingBar() {
    return RatingBar(
      initialRating: 5,
      minRating: 1,
      direction: Axis.horizontal,
      allowHalfRating: false,
      itemCount: 5,
      itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
      ratingWidget: RatingWidget(
          full: SvgPicture.asset(AppImages.icStar),
          empty: SvgPicture.asset(
            AppImages.icStarOutline,
          ),
          half: Icon(Icons.star_half_outlined)),
      unratedColor: AppColors.grayLight,
      onRatingUpdate: (rating) {
        logic.setRating(rating.toInt());
      },
    );
  }

  Widget _inputComment() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 24.0),
      child: TextField(
        decoration: InputDecoration(
            hintText: 'Viết nhận xét',
            hintStyle: TextStyle(fontSize: 14, color: AppColors.gray2)),
        controller: logic.ratingController,
      ),
    );
  }

  Widget _buildGridImage() {
    return Obx(
      () => Container(
        margin: EdgeInsets.symmetric(horizontal: 24.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              S.of(context).add_image,
              style: TextStyle(fontSize: 14, color: AppColors.textBlack),
            ),
            GridView.builder(
              padding: EdgeInsets.all(10.0),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 5,
              ),
              shrinkWrap: true,
              itemCount: state.images.length + 1,
              itemBuilder: (BuildContext ctx, index) {
                if (index == 0) {
                  return InkWell(
                    onTap: () {
                      _pickImage();
                    },
                    borderRadius: BorderRadius.circular(10.0),
                    child: Container(
                      padding: EdgeInsets.all(4.0),
                      child: DottedBorder(
                        borderType: BorderType.RRect,
                        color: AppColors.red,
                        strokeWidth: 1,
                        //thickness of dash/dots
                        dashPattern: [4, 4],
                        radius: Radius.circular(10.0),
                        child: Container(
                          decoration: BoxDecoration(
                            color: AppColors.pink,
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Center(
                              child: Icon(Icons.camera_alt_outlined,
                                  color: AppColors.red)),
                        ),
                      ),
                    ),
                  );
                } else {
                  return _buildImageWidget(context, index);
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildImageWidget(BuildContext context, int index) => Stack(
        children: [
          Container(
            padding: EdgeInsets.all(8.0),
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              child: () {
                var path = state.images[index - 1];
                if (path.startsWith('http'))
                  return Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      image: DecorationImage(
                        image: CachedNetworkImageProvider(path),
                        fit: BoxFit.fill,
                      ),
                    ),
                  );
                else
                  return Image.file(
                    File(path),
                    width: 100,
                    fit: BoxFit.fill,
                  );
              }(),
            ),
          ),
          Positioned(
            top: 0.w,
            right: 0.w,
            child: InkWell(
              onTap: () {
                logic.removeImageAt(index - 1);
              },
              child: Center(
                child: Container(
                  decoration: BoxDecoration(
                      color: AppColors.gray2, shape: BoxShape.circle),
                  child: Icon(
                    Icons.close,
                    color: AppColors.white,
                    size: 18.w,
                  ),
                ),
              ),
            ),
          )
        ],
      );

  _pickImage() {
    _editAvatar(context, logic.state.image);
  }

  void _editAvatar(BuildContext context, String url) async {
    _mediaActionSheetWidget.showActionSheet(
        '', [S.of(context).takePhoto, S.of(context).chooseFromGrallery],
        (int index) {
      if (index == 0) {
        _openCamera((pickedFile) {
          logic.addImages([pickedFile.path]);
        });
      }
      if (index == 1) {
        _pickProductImages();
      }
    });
  }

  void _pickProductImages() async {
    var media =
        await MediaPickerUtils.pickPhotoForProduct(logic.state.images.length);
    if (media != null && media.isNotEmpty) {
      logic.addImages(media.map((e) => e.path).toList());
    }
  }

  void _openCamera(OnPickImageCallback onPickImageCallback) {
    _mediaActionSheetWidget.pickImageDialog(ImageSource.camera,
        (PickedFile pickedFile) {
      onPickImageCallback(pickedFile);
    });
  }
}
