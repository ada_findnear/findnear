import 'package:findnear/src/models/order_history.dart';
import 'package:findnear/src/pages/rating/rating_logic.dart';
import 'package:findnear/src/repository/rating_repository.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../main.dart';
import 'state.dart';

class RatingOrderLogic extends GetxController {
  final RatingResposity ratingRepository = Get.find();
  final state = RatingOrderState();
  final controller = Get.put<RatingLogic>(RatingLogic());
  final RatingLogic ratingLogic = Get.find();
  List<String> addedImages = <String>[];
  List<String> removedImages = <String>[];
  TextEditingController ratingController = TextEditingController(text: "");

  /// Create rating
  ///
  /// `orderId`: id of order
  ///
  /// `rating`: rating value
  ///
  /// `comment`: Content comment
  ///
  /// `imagesPath`: list image rating
  void rating() async {
    DialogHelper.showLoaderDialog();
    try {
      await ratingRepository.rating(
          orderId: state?.orderHistory?.id,
          rating: state.rating.toDouble(),
          comment: ratingController.text,
          imagesPath: state.images);
      Get.back();
      DialogHelper.showAlertDialog(
          context: Get.context,
          title: S.current.success,
          okText: S.current.close,
          okFunction: () {
            Get.back();
            ratingLogic.setShopId(state.orderHistory.shopId);
          },
          description: S.current.rating_success);
    } catch (e) {
      Get.back();
      print(e);
      Get.showSnackbar(GetBar(
        title: S.of(Get.context).an_error_occurred,
        message: S.of(Get.context).please_try_again_msg,
      ));
    }
  }

  void removeImageAt(int index) {
    if (index >= 0 && index < state.images.length) {
      var removePath = state.images[index];
      if (addedImages.contains(removePath)) addedImages.remove(removePath);
      state.removeImageAt(index);
    }
  }

  void addImages(List<String> paths) {
    if (paths != null && paths.isNotEmpty) {
      addedImages.addAll(paths);
      state.addImages(paths);
    }
  }

  void setRating(int rating) {
    state.rating = rating;
  }
}
