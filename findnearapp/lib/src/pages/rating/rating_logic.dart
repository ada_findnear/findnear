import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/base/base_refresh_load_more.dart';
import 'package:findnear/src/models/rating.dart';
import 'package:findnear/src/models/response/rating_response.dart';
import 'package:findnear/src/pages/rating/rating_state.dart';
import 'package:findnear/src/repository/shop_repository.dart';
import 'package:get/get.dart';

class RatingLogic extends BaseController<RatingState>
    with RefreshLoadMoreControllerMixin {
  final ShopsRepository shopRepository = Get.find();

  RatingLogic();

  @override
  RatingState createState() => RatingState();

  Future<RatingResponse> getRatings() async {
    final res = await shopRepository.getRating(
      state?.shop?.id ?? state.shopId,
    );
    state.ratings = res.data ?? List<Rating>.empty();
    return res;
  }

  @override
  Future<bool> loadList() async {
    if (!canLoadMore) return true;
    try {
      if (currentPage == 1) state.isLoading = true;
      RatingResponse response = await getRatings();
      if (response?.data?.isNotEmpty == true) {
        canLoadMore = true;
        state.ratings = List<Rating>.from(currentPage == 1
            ? List<Rating>.empty()
            : (state.ratings ?? List<Rating>.empty()))
          ..addAll(response?.data ?? List.empty());
      } else {
        canLoadMore = false;
      }
      state.isLoading = false;
      return true;
    } catch (e) {
      state.error = e;
      state.isLoading = false;
      return false;
    }
  }

  void setShopId(int shopId) {
    state.shopId = shopId;
    loadList();
  }
}
