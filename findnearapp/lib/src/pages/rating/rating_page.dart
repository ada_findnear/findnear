import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/base/base_refresh_load_more.dart';
import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/media.dart';
import 'package:findnear/src/models/rating.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/models/shop.dart';
import 'package:findnear/src/pages/rating/rating_logic.dart';
import 'package:findnear/src/pages/rating/rating_state.dart';
import 'package:findnear/src/pages/shop_detail/logic.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../main.dart';

class RatingPage extends StatefulWidget {
  static const String ROUTE_NAME = '/RatingPage';
  final RouteArgument argument;
  const RatingPage({Key key, this.argument}) : super(key: key);

  @override
  _RatingPageState createState() => _RatingPageState();
}

class _RatingPageState
    extends BaseWidgetState<RatingPage, RatingLogic, RatingState>
    with RefreshLoadMoreWidgetMixin {
  Shop get shop => widget.argument.param['shop'];

  @override
  void initState() {
    super.initState();
    state.shop = shop;
    controller.loadList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${S.of(context).rating_commnet} (${state.ratings.length})",
            style: TextStyle(color: AppColors.textBlack, fontSize: 16.0)),
        automaticallyImplyLeading: false,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: Obx(
        () => Container(
          child: Column(
            children: [
              Container(
                height: 5,
                color: AppColors.pink,
              ),
              _ratingShop(),
              Divider(
                color: AppColors.pink,
                thickness: 5,
              ),
              _listRating(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _ratingShop() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "${state.shop.rating}",
            style: TextStyle(
                color: AppColors.textBlack,
                fontSize: 16.0,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(width: 14.0),
          _ratingBar(state.shop.rating.toInt(), iconSize: 16)
        ],
      ),
    );
  }

  Widget _listRating() {
    return ListView.separated(
      shrinkWrap: true,
      padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 16.0),
      itemBuilder: (c, index) {
        var rating = state.ratings[index];
        return _itemRating(rating);
      },
      separatorBuilder: (c, index) {
        return Container();
      },
      itemCount: state.ratings.length,
    );
  }

  Widget _itemRating(Rating rating) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CircleAvatar(
            child: CachedNetworkImage(
          imageUrl: rating?.user?.image?.url ?? '',
        )),
        SizedBox(
          width: 10.0,
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Name
              Text(rating?.user?.name ?? '',
                  style: TextStyle(fontSize: 14, color: AppColors.textBlack)),
              SizedBox(
                height: 8.0,
              ),
              // Rating
              _ratingBar(rating.rating),
              SizedBox(
                height: 8.0,
              ),
              // Comment
              Text(rating?.comment ?? ''),
              SizedBox(
                height: 8.0,
              ),
              _buildGridImage(rating.images)
            ],
          ),
        )
      ],
    );
  }

  Widget _ratingBar(int ratingValue, {double iconSize = 14}) {
    return RatingBar(
      initialRating: ratingValue.toDouble(),
      minRating: 1,
      direction: Axis.horizontal,
      allowHalfRating: false,
      itemCount: 5,
      ignoreGestures: true,
      itemSize: iconSize,
      itemPadding: EdgeInsets.symmetric(horizontal: 1.0),
      ratingWidget: RatingWidget(
          full: SvgPicture.asset(AppImages.icStar),
          empty: SvgPicture.asset(
            AppImages.icStarOutline,
          ),
          half: Icon(Icons.star_half_outlined)),
      unratedColor: AppColors.grayLight,
      onRatingUpdate: (rating) {},
    );
  }

  Widget _buildGridImage(List<Media> images) {
    return Container(
      child: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 6, mainAxisSpacing: 8.0, crossAxisSpacing: 8.0),
        shrinkWrap: true,
        itemCount: images.length,
        itemBuilder: (BuildContext ctx, index) {
          return _buildImageWidget(context, images[index].url ?? '');
        },
      ),
    );
  }

  Widget _buildImageWidget(BuildContext context, String path) => Container(
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              image: DecorationImage(
                image: CachedNetworkImageProvider(path),
                fit: BoxFit.fill,
              ),
            ),
          ),
        ),
      );

  @override
  RatingLogic createController() => RatingLogic();

  @override
  void dispose() {
    Get.delete<RatingLogic>();
    super.dispose();
  }
}
