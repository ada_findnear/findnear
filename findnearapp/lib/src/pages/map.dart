// import 'package:flutter/foundation.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_google_places/flutter_google_places.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:mvc_pattern/mvc_pattern.dart';
//
// import '../../generated/l10n.dart';
// import '../controllers/map_controller.dart';
// import '../elements/CardsCarouselWidget.dart';
// import '../elements/CircularLoadingWidget.dart';
// import '../models/market.dart';
// import '../models/route_argument.dart';
// import 'package:google_maps_webservice/places.dart';
// import '../repository/settings_repository.dart' as settingRepo;
//
// class MapWidget extends StatefulWidget {
//   final RouteArgument routeArgument;
//   final GlobalKey<ScaffoldState> parentScaffoldKey;
//
//   MapWidget({Key key, this.routeArgument, this.parentScaffoldKey}) : super(key: key);
//
//   @override
//   _MapWidgetState createState() => _MapWidgetState();
// }
//
// class _MapWidgetState extends StateMVC<MapWidget> {
//   MapController _con;
//
//   _MapWidgetState() : super(MapController()) {
//     _con = controller;
//   }
//
//   @override
//   void initState() {
//     _con.currentMarket = widget.routeArgument?.param as Market;
//     if (_con.currentMarket?.latitude != null) {
//       // user select a market
//       _con.getMarketLocation();
//       _con.getDirectionSteps();
//     } else {
//       _con.getCurrentLocation();
//     }
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       key: _con.scaffoldKey,
//       appBar: AppBar(
//         backgroundColor: Colors.transparent,
//         elevation: 0,
//         centerTitle: true,
//         leading: _con.currentMarket?.latitude == null
//             ? communication IconButton(
//                 icon: communication Icon(Icons.sort, color: Theme.of(context).hintColor),
//                 onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
//               )
//             : IconButton(
//                 icon: communication Icon(Icons.arrow_back, color: Theme.of(context).hintColor),
//                 onPressed: () => Navigator.of(context).pushNamed('/Pages', arguments: 1),
//               ),
//         title: Text(
//           S.of(context).maps_explorer,
//           style: Theme.of(context).textTheme.headline6.merge(TextStyle(letterSpacing: 1.3)),
//         ),
//         actions: <Widget>[
//           IconButton(
//             icon: Icon(
//               Icons.my_location,
//               color: Theme.of(context).hintColor,
//             ),
//             onPressed: () {
//               _con.goCurrentLocation();
//             },
//           ),
//           IconButton(
//             icon: Icon(
//               Icons.settings,
//               color: Theme.of(context).hintColor,
//             ),
//             onPressed: () {
//               widget.parentScaffoldKey.currentState.openEndDrawer();
//             },
//           ),
//         ],
//       ),
//       body: Stack(
//         alignment: AlignmentDirectional.topStart,
//         children: [
//           Stack(
// //        fit: StackFit.expand,
//             alignment: AlignmentDirectional.bottomStart,
//             children: <Widget>[
//               _con.cameraPosition == null
//               ? CircularLoadingWidget(height: 0)
//               : ValueListenableBuilder(
//                 valueListenable: settingRepo.satelliteMap,
//                 builder: (context, value, widget) {
//                   return GoogleMap(
//                     mapToolbarEnabled: false,
//                     mapType: settingRepo.satelliteMap.value ? MapType.satellite : MapType.normal,
//                     buildingsEnabled: false,
//                     initialCameraPosition: _con.cameraPosition,
//                     markers: Set.from(_con.allMarkers),
//                     onMapCreated: (GoogleMapController controller) {
//                       _con.mapController.complete(controller);
//                     },
//                     onCameraMove: (CameraPosition cameraPosition) {
//                       _con.cameraPosition = cameraPosition;
//                     },
//                     onTap: (LatLng position) {
//                       if (_con?.bottomSheetController != null) {
//                         _con?.bottomSheetController?.close();
//                       }
//                     },
//                     onCameraIdle: () {
//                       print("SonLT");
//                       // _con.getMarketsOfArea();
//                     },
//                     polylines: Set<Polyline>.of(_con.polyLines.values),
//                   );
//                 }
//               ),
//               CardsCarouselWidget(
//                 marketsList: _con.topMarkets,
//                 heroTag: 'map_markets',
//                 currentMarket: _con.currentMarket,
//               )
//             ],
//           ),
//           Container(
//             alignment: _con.searchPosition == null ? Alignment.topRight : Alignment.topCenter,
//             height: 60,
//             decoration: BoxDecoration(
//               boxShadow: _con.searchPosition != null ? [
//                 BoxShadow(color: Theme.of(context).focusColor.withOpacity(0.3), blurRadius: 15, offset: Offset(0, 5)),
//               ] : null,
//             ),
//             child: Padding(
//               padding: EdgeInsets.fromLTRB(16, 16, 16, 0),
//               child: GestureDetector(
//                 onTap: () async {
//                   var language = await settingRepo.getDefaultLanguage(settingRepo.setting.value.mobileLanguage.value.languageCode);
//                   Prediction p = await PlacesAutocomplete.show(
//                     context: context,
//                     apiKey: settingRepo.setting.value.googleMapsKey,
//                     language: language,
//                     location: Location.fromJson({
//                       'lat': _con.currentAddress.latitude != null ? _con.currentAddress.latitude : 21.029832,
//                       'lng': _con.currentAddress.longitude != null ? _con.currentAddress.longitude : 105.852605
//                     }),
//                     radius: 500,
//                     mode: Mode.overlay,
//                     startText: _con.searchPosition != null ? _con.searchPosition.description : '',
//                     hint: S.of(context).search
//                   );
//                   _con.displayPrediction(p);
//                 },
//                 child: _con.searchPosition == null ?
//                     Container(
//                       padding: EdgeInsets.all(10),
//                       child: Icon(Icons.search_outlined, color: Theme.of(context).primaryColor),
//                       decoration: BoxDecoration(
//                         color: Theme.of(context).accentColor,
//                         borderRadius: BorderRadius.all(Radius.circular(25)),
//                         boxShadow: [
//                           BoxShadow(color: Theme.of(context).focusColor.withOpacity(0.8), blurRadius: 15, offset: Offset(0, 5)),
//                         ]
//                       ),
//                     ) :
//                     Container(
//                       decoration: BoxDecoration(
//                         color: Theme.of(context).primaryColor,
//                         borderRadius: BorderRadius.all(Radius.circular(25)),
//                         boxShadow: [
//                           BoxShadow(color: Theme.of(context).focusColor.withOpacity(0.1), blurRadius: 15, offset: Offset(0, 5)),
//                         ],
//                       ),
//                       child: Row(
//                         children: [
//                           SizedBox(
//                             width: 10,
//                           ),
//                           Icon(Icons.location_on_sharp, color: Theme.of(context).accentColor),
//                           SizedBox(
//                             width: 20,
//                           ),
//                           Expanded(
//                             child: Text(_con.searchPosition != null ? _con.searchPosition.description.split(', ')[0] : S.of(context).search, style: TextStyle(
//                                 fontSize: 15.0,
//                                 color: _con.searchPosition != null ? Theme.of(context).hintColor : Theme.of(context).focusColor
//                             ),),
//                           ),
//                           VerticalDivider(),
//                           if (_con.searchPosition != null)
//                             IconButton(
//                               icon: Icon(Icons.close, color: Theme.of(context).focusColor),
//                               onPressed: () {
//                                 setState(() {
//                                   _con.searchPosition = null;
//                                 });
//                               },
//                             ),
//                           SizedBox(
//                             width: 10,
//                           ),
//                         ],
//                       ),
//                     ),
//               )
//             )
//           ),
//         ],
//       )
//     );
//   }
// }
