import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/BlockButtonWidget.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:findnear/src/widgets/fn_app_textfield.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:velocity_x/velocity_x.dart';
import '../../helpers/app_config.dart' as config;

import 'change_password_logic.dart';
import 'change_password_state.dart';

class ChangePasswordPage extends StatefulWidget {
  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  final ChangePasswordLogic logic = Get.put(ChangePasswordLogic());
  final ChangePasswordState state = Get.find<ChangePasswordLogic>().state;

  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: Helper.of(context).onWillPop,
      child: Scaffold(
        body: Stack(
          alignment: AlignmentDirectional.topCenter,
          children: <Widget>[
            Positioned(
              top: 0,
              child: Container(
                child: ZStack([
                  AppImage(
                    AppImages.icMarkLogin,
                    fit: BoxFit.cover,
                    width: Get.width,
                  ),
                  Align(
                      child: AppImage(
                        AppImages.icLogin,
                        width: Get.width * 0.4,
                      ),
                      alignment: Alignment.center),
                ]),
                width: config.App(context).appWidth(100),
                height: config.App(context).appHeight(29.5),
                decoration: BoxDecoration(color: Theme.of(context).accentColor),
              ),
            ),
            Positioned(
              top: config.App(context).appHeight(29.5) - 30,
              child: Column(
                children: [
                  Container(
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 50,
                            color: Theme.of(context).hintColor.withOpacity(0.2),
                          )
                        ]),
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    padding: EdgeInsets.symmetric(vertical: 30, horizontal: 27),
                    width: config.App(context).appWidth(88),
                    child: Form(
                      key: formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          FnAppTextField(
                            onChange: (input) => state.name.value = input,
                            validator: (input) => input.length < 3 ? S.of(context).should_be_more_than_3_letters : null,
                            labelText: S.of(context).full_name,
                            hintText: S.of(context).john_doe,
                            prefixIcon: Icon(Icons.person_outline, color: Theme.of(context).accentColor),
                          ),
                          SizedBox(height: 25),
                          FnAppTextField(
                            onChange: (input) => state.authCode.value = input,
                            maxLength: 6,
                            validator: (input) => input.length < 6 ? S.of(context).pin_code_letters : null,
                            labelText: S.of(context).authCodeDescription,
                            hintText: '123456',
                            inputType: TextInputType.number,
                            prefixIcon: Icon(Icons.confirmation_number_outlined, color: Theme.of(context).accentColor),
                          ),
                          SizedBox(height: 25),
                          Obx(() {
                            return FnAppTextField(
                              onChange: (input) => state.newPasswordInput.value = input,
                              obscureText: state.isNewPasswordVisible.value,
                              validator: (input) =>
                                  input.length < 6 ? S.of(context).should_be_more_than_6_letters : null,
                              labelText: S.of(context).newPassword,
                              hintText: '••••••••••••',
                              prefixIcon: Icon(Icons.lock_outline, color: Theme.of(context).accentColor),
                              suffixIcon: IconButton(
                                onPressed: () {
                                  state.isNewPasswordVisible.value = !state.isNewPasswordVisible.value;
                                },
                                color: Theme.of(context).focusColor,
                                icon: Icon(state.isNewPasswordVisible.value ? Icons.visibility : Icons.visibility_off),
                              ),
                            );
                          }),
                          SizedBox(height: 25),
                          Obx(() {
                            return FnAppTextField(
                              obscureText: state.isConfirmPasswordVisible.value,
                              onChange: (input) => state.confirmPasswordInput.value = input,
                              validator: (input) {
                                if (input.length < 6) {
                                  return S.of(context).should_be_more_than_6_letters;
                                } else {
                                  if (state.confirmPasswordInput.value != state.newPasswordInput.value) {
                                    return S.current.invalidConfirmPassword;
                                  } else {
                                    return null;
                                  }
                                }
                              },
                              labelText: S.of(context).confirmPassword,
                              hintText: '••••••••••••',
                              prefixIcon: Icon(Icons.lock_outline, color: Theme.of(context).accentColor),
                              suffixIcon: IconButton(
                                onPressed: () {
                                  state.isConfirmPasswordVisible.value = !state.isConfirmPasswordVisible.value;
                                },
                                color: Theme.of(context).focusColor,
                                icon: Icon(
                                    state.isConfirmPasswordVisible.value ? Icons.visibility : Icons.visibility_off),
                              ),
                            );
                          }),
                          SizedBox(height: 25),
                          BlockButtonWidget(
                            text: Text(
                              S.of(context).changePassword.toUpperCase(),
                              style: TextStyle(color: Theme.of(context).primaryColor),
                            ),
                            color: Theme.of(context).accentColor,
                            onPressed: () => onPasswordChangeButtonTap(context),
                          ),
                          SizedBox(height: 10),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  bool get validateAndSave {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void onPasswordChangeButtonTap(BuildContext context) {
    if (validateAndSave) {
      logic.changePasswords(context);
    }
  }

  @override
  void dispose() {
    Get.delete<ChangePasswordLogic>();
    super.dispose();
  }
}
