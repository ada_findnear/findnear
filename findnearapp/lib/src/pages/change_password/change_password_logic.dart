import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/models/create_post.dart';
import 'package:findnear/src/repository/authentication_repository.dart';
import 'package:findnear/src/repository/newsfeed_repository.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import 'change_password_state.dart';

class ChangePasswordLogic extends GetxController {
  final state = ChangePasswordState();
  final authRepository = Get.find<AuthenticationRepository>();
  final _postRepo = Get.find<NewsfeedRepository>();

  ChangePasswordLogic();

  void createPost() async {
    await _postRepo.createPost(CreatePost(
        content: 'Xin chào cộng đồng FindNear',
        viewer: '2', //Share post = public post
        ));
  }

  void changePasswords(BuildContext context) async {
    DialogHelper.showLoadingDialog();
    try {
      final result = await authRepository.changePassword(
        password: state.confirmPasswordInput.value,
        authCode: state.authCode.value,
        name: state.name.value,
      );
      if (result.success) {
        createPost();
        goBack();
        DialogHelper.showAlertDialog(
          context: context,
          description: S.current.resetPasswordSuccess,
          title: S.current.notifications,
          okText: 'OK',
          okFunction: () {
            goBack();
          },
        );
      } else {
        AppSnackbar.showError(title: S.of(context).error, message: result.message);
      }
    } catch (e) {
      goBack();
      AppSnackbar.showError(title: S.of(context).error, message: S.current.somethingWentWrong);
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
