import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:velocity_x/velocity_x.dart';

class AppTextInput extends StatelessWidget {
  final String title;
  final IconData icon;
  final bool hasPassword;
  final bool isPasswordVisible;
  final TextInputType textInputType;
  final FormFieldValidator<String> validator;
  final Function(String) onChange;
  final VoidCallback onChangePasswordVisibility;

  const AppTextInput({
    Key key,
    this.title,
    this.icon,
    this.hasPassword = false,
    this.isPasswordVisible = false,
    this.textInputType,
    this.validator,
    this.onChange,
    this.onChangePasswordVisibility,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return VStack([
      title.text.color(AppColors.textBlack).size(17).medium.make(),
      HStack([
        TextFormField(
          onSaved: onChange,
          scrollPhysics: BouncingScrollPhysics(),
          validator: validator,
          decoration: InputDecoration(
              contentPadding: const EdgeInsets.all(16),
              prefixIcon: icon != null
                  ? Icon(icon, size: 25, color: Theme.of(context).accentColor)
                  : null,
              suffixIcon: hasPassword
                  ? AppImage(
                      !isPasswordVisible
                          ? AppImages.icEnViewer
                          : AppImages.icViewer,
                      height: 15,
                      width: 25,
                      fit: BoxFit.fill,
                    ).marginOnly(right: 16).onTap(onChangePasswordVisibility)
                  : 0.widthBox,
              suffixIconConstraints:
                  BoxConstraints(maxWidth: 50, maxHeight: 15),
              hintText: hasPassword ? '••••••••••••' : '12345678',
              isDense: true,
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: AppColors.gray, width: 0.5)),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: AppColors.gray, width: 0.5)),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: AppColors.gray, width: 0.5)),
              hintStyle: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 17,
                  color: AppColors.grayLight)),
          obscureText: hasPassword ? !isPasswordVisible : false,
          keyboardType: textInputType ?? (hasPassword ? TextInputType.text : TextInputType.number),
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
          onChanged: onChange,
        ).expand(),
      ]).paddingSymmetric(vertical: 4)
    ]);
  }
}
