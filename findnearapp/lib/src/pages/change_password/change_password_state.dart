import 'package:get/get.dart';

class ChangePasswordState {
  RxBool isOldPasswordVisible = false.obs;
  RxString oldPasswordInput = "".obs;

  RxBool isNewPasswordVisible = true.obs;
  RxString newPasswordInput = "".obs;

  RxBool isConfirmPasswordVisible = true.obs;
  RxString confirmPasswordInput = "".obs;

  RxString authCode = "".obs;
  RxString name = "".obs;

  bool get passwordsMatch => newPasswordInput == confirmPasswordInput;

  bool get passwordMinLengthReached => newPasswordInput.value.length > 3;

  bool get confirmPasswordMinLengthReached => confirmPasswordInput.value.length > 3;

  bool get authCodeIsValid => authCode.value.length == 6;

  bool get isChangePasswordButtonActive =>
      passwordsMatch && passwordMinLengthReached && confirmPasswordMinLengthReached && authCodeIsValid;

  ChangePasswordState() {
    ///Initialize variables
  }
}
