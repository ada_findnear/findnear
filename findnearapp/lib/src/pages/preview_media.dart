import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class PreviewMediaWidget extends StatefulWidget {
  final String photoPath;
  final String thumbPath;
  PreviewMediaWidget({Key key, @required this.photoPath, this.thumbPath})
      : super(key: key);
  @override
  _PreviewMediaWidgetState createState() => _PreviewMediaWidgetState();
}

class _PreviewMediaWidgetState extends State<PreviewMediaWidget> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.close,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context, true);
          },
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.black,
        elevation: 0,
      ),
      body: Container(
        child: Hero(
          tag: 'preview_media',
          child: PhotoView(
            imageProvider: _getImage(),
            loadingBuilder: (context, event) => widget.thumbPath != null
                ? CachedNetworkImage(imageUrl: widget.thumbPath)
                : SizedBox(),
          ),
        ),
      ),
    );
  }

  ImageProvider _getImage() {
    if (widget.photoPath.contains("http")) {
      return NetworkImage(widget.photoPath);
    } else {
      return FileImage(File(widget.photoPath));
    }
  }
}
