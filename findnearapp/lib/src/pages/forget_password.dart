import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:velocity_x/velocity_x.dart';

import '../../generated/l10n.dart';
import '../controllers/user_controller.dart';
import '../elements/BlockButtonWidget.dart';
import '../helpers/app_config.dart' as config;
import '../helpers/helper.dart';

class ForgetPasswordWidget extends StatefulWidget {
  @override
  _ForgetPasswordWidgetState createState() => _ForgetPasswordWidgetState();
}

class _ForgetPasswordWidgetState extends StateMVC<ForgetPasswordWidget> {
  UserController _con;

  _ForgetPasswordWidgetState() : super(UserController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _itemOldForgot();
    // return _itemNewForgot();
  }

  Widget _itemNewForgot() => Scaffold(
        key: _con.scaffoldKey,
        resizeToAvoidBottomInset: false,
        body: VxBox(
          child: VStack(
            [_itemTopLogin(), _itemBody()],
          ),
        )
            .padding(EdgeInsets.only(
                top: Get.mediaQuery.viewPadding.top,
                bottom: Get.mediaQuery.viewPadding.bottom))
            .color(AppColors.red)
            .height(Get.height)
            .make(),
      );

  Widget _itemTopLogin() => ZStack([
        AppImage(
          AppImages.icMarkLogin,
          fit: BoxFit.cover,
          width: Get.width,
        ),
        Align(
            child: VStack([
              AppImage(
                AppImages.icLockLarge,
                width: Get.width * 0.17,
              )
            ]),
            alignment: Alignment.center),
        S
            .of(Get.context)
            .i_forgot_password
            .toUpperCase()
            .text
            .color(Vx.white)
            .size(20)
            .medium
            .make()
            .box
            .alignBottomCenter
            .make(),
        Icon(
          Icons.arrow_back_ios,
          color: AppColors.white,
        ).paddingAll(16).onTap(goBack),
      ]).box.height(Get.height * 0.25).make();

  Widget _itemBody() => VxBox(
              child: VStack([
        _itemDescription(),
        _itemBack(),
      ]))
          .withDecoration(BoxDecoration(
              color: Vx.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(24), topRight: Radius.circular(24))))
          .make()
          .marginOnly(top: 8)
          .expand();

  Widget _itemDescription() => VxRichText(S.of(Get.context).forgot_1)
      .withTextSpanChildren([
        VelocityXTextSpan(S.of(Get.context).hot_line)
            .color(AppColors.red)
            .bold
            .size(17)
            .make(),
        VelocityXTextSpan(S.of(Get.context).forgot_2)
            .color(AppColors.textBlack)
            .size(17)
            .make()
      ])
      .textStyle(TextStyle(color: AppColors.textBlack, fontSize: 17))
      .make()
      .paddingAll(24)
      .expand();

  Widget _itemBack() => VxBox(
          child: S
              .of(Get.context)
              .go_back
              .toUpperCase()
              .text
              .size(18)
              .medium
              .color(AppColors.white)
              .make())
      .roundedLg
      .alignCenter
      .color(AppColors.red)
      .width(Get.width)
      .padding(EdgeInsets.symmetric(vertical: 8))
      .margin(EdgeInsets.symmetric(horizontal: 24, vertical: 16))
      .make()
      .onTap(() => goBack());

  Widget _itemOldForgot() => WillPopScope(
      onWillPop: Helper.of(context).onWillPop,
      child: Scaffold(
        key: _con.scaffoldKey,
        resizeToAvoidBottomInset: false,
        body: Stack(
          alignment: AlignmentDirectional.topCenter,
          children: <Widget>[
            Positioned(
              top: 0,
              child: Container(
                width: config.App(context).appWidth(100),
                height: config.App(context).appHeight(37),
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: ExactAssetImage(AppImages.icMarkLogin)),
                    color: Theme.of(context).accentColor),
              ),
            ),
            Positioned(
              top: 45,
              child: AppImage(
                AppImages.icLockLarge,
                width: Get.width * 0.2,
              ),
            ),
            Positioned(
              top: config.App(context).appHeight(37) - 80,
              child: Container(
                width: config.App(context).appWidth(84),
                height: config.App(context).appHeight(37),
                child: Text(
                  S.of(context).email_to_reset_password,
                  style: Theme.of(context)
                      .textTheme
                      .headline2
                      .merge(TextStyle(color: Theme.of(context).primaryColor)),
                ),
              ),
            ),
            Positioned(
              top: config.App(context).appHeight(37) - 30,
              child: Container(
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 50,
                        color: Theme.of(context).hintColor.withOpacity(0.2),
                      )
                    ]),
                margin: EdgeInsets.symmetric(
                  horizontal: 20,
                ),
                padding: EdgeInsets.symmetric(vertical: 50, horizontal: 27),
                width: config.App(context).appWidth(88),
//              height: config.App(context).appHeight(55),
                child: Form(
                  key: _con.loginFormKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        onSaved: (input) => _con.user.email = input,
                        validator: (input) => !input.contains('@')
                            ? S.of(context).should_be_a_valid_email
                            : null,
                        decoration: InputDecoration(
                          labelText: S.of(context).email,
                          labelStyle:
                              TextStyle(color: Theme.of(context).accentColor),
                          contentPadding: EdgeInsets.all(12),
                          hintText: 'johndoe@gmail.com',
                          hintStyle: TextStyle(
                              color: Theme.of(context)
                                  .focusColor
                                  .withOpacity(0.7)),
                          prefixIcon: Icon(Icons.alternate_email,
                              color: Theme.of(context).accentColor),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Theme.of(context)
                                      .focusColor
                                      .withOpacity(0.2))),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Theme.of(context)
                                      .focusColor
                                      .withOpacity(0.5))),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Theme.of(context)
                                      .focusColor
                                      .withOpacity(0.2))),
                        ),
                      ),
                      SizedBox(height: 30),
                      BlockButtonWidget(
                        text: Text(
                          S.of(context).send_password_reset_link,
                          style:
                              TextStyle(color: Theme.of(context).primaryColor),
                        ),
                        color: Theme.of(context).accentColor,
                        onPressed: () {
                          _con.resetPassword();
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: 10,
              child: Column(
                children: <Widget>[
                  MaterialButton(
                    elevation: 0,
                    onPressed: () {
                      Navigator.of(context).pushReplacementNamed('/Login');
                    },
                    textColor: Theme.of(context).hintColor,
                    child: Text(
                        S.of(context).i_remember_my_password_return_to_login),
                  ),
                  // MaterialButton(
                  //   elevation: 0,
                  //   onPressed: () {
                  //     Navigator.of(context).pushReplacementNamed('/SignUp');
                  //   },
                  //   textColor: Theme.of(context).hintColor,
                  //   child: Text(S.of(context).i_dont_have_an_account),
                  // ),
                ],
              ),
            )
          ],
        ),
      ));
}
