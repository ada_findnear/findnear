// import 'package:findnear/src/pages/profile.dart';
// import 'package:findnear/src/pages/profile_v2.dart';
// import 'package:findnear/src/pages/tab_map/tab_map_view.dart';
// import 'package:flutter/material.dart';
// import 'package:mvc_pattern/mvc_pattern.dart';
//
// import '../../generated/l10n.dart';
// import '../controllers/market_controller.dart';
// import '../elements/CircularLoadingWidget.dart';
// import '../elements/DrawerWidget.dart';
// import '../elements/PermissionDeniedWidget.dart';
// import '../models/conversation.dart';
// import '../models/market.dart';
// import '../models/route_argument.dart';
// import '../repository/user_repository.dart';
// import 'chat.dart';
//
// // ignore: must_be_immutable
// class ProfileUserWidget extends StatefulWidget {
//   RouteArgument routeArgument;
//   dynamic currentTab;
//   final GlobalKey<ScaffoldState> scaffoldKey = communication GlobalKey<ScaffoldState>();
//   Widget currentPage;
//
//   ProfileUserWidget({
//     Key key,
//     this.currentTab,
//   }) {
//     if (currentTab != null) {
//       if (currentTab is RouteArgument) {
//         routeArgument = currentTab;
//         currentTab = int.parse(currentTab.id);
//       }
//     } else {
//       currentTab = 0;
//     }
//   }
//
//   @override
//   _ProfileUserWidgetState createState() {
//     return _ProfileUserWidgetState();
//   }
// }
//
// class _ProfileUserWidgetState extends StateMVC<ProfileUserWidget> {
//   MarketController _con;
//
//   _ProfileUserWidgetState() : super(MarketController()) {
//     _con = controller;
//   }
//
//   initState() {
//     if (widget.routeArgument != null) {
//       _con.getConversation(widget.routeArgument.param['idUser']);
//     }
//     _selectTab(widget.currentTab);
//     super.initState();
//   }
//
//   // @override
//   // void didUpdateWidget(ProfileUserWidget oldWidget) {
//   //   _selectTab(oldWidget.currentTab);
//   //   super.didUpdateWidget(oldWidget);
//   // }
//
//   void _selectTab(int tabItem) {
//     setState(() {
//       widget.currentTab = tabItem;
//       switch (tabItem) {
//         case 0:
//           if (widget.routeArgument != null &&
//               currentUser.value.id !=
//                   widget.routeArgument.param['idUser'].toString()) {
//             _con
//                 .listenForMarket(
//                     id: widget.routeArgument.param['idMarket'].toString())
//                 .then((value) {
//               setState(() {
//                 _con.market = value as Market;
//                 widget.currentPage = ProfileWidget(
//                     parentScaffoldKey: widget.scaffoldKey,
//                     routeArgument: RouteArgument(
//                         param: widget.routeArgument.param['idUser']));
//               });
//             });
//             // widget.currentPage = ProfileWidget(parentScaffoldKey: widget.scaffoldKey, routeArgument: RouteArgument(param: widget.routeArgument.param['idUser']));
//           } else
//             (widget.currentPage = ProfileWidget());
//           // widget.currentPage = ProfileWidget(widget.routeArgument.param);
//           break;
//         case 1:
//           if (currentUser.value?.apiToken == null) {
//             widget.currentPage = PermissionDeniedWidget();
//           } else {
//             Conversation _conversation;
//             if (_con.conversation != null) {
//               _conversation = _con.conversation;
//             } else {
//               _conversation = communication Conversation([_con.market.owner],
//                   name: _con.market.owner.name);
//             }
//
//             widget.currentPage = ChatWidget(
//               parentScaffoldKey: widget.scaffoldKey,
//               routeArgument:
//                   RouteArgument(id: _con.market.id, param: _conversation),
//               setConversation: (conversation) => {
//                 _con.setState(() {
//                   _con.conversation = conversation;
//                 })
//               },
//             );
//           }
//           break;
//         case 2:
//           // widget.currentPage = MapWidget(parentScaffoldKey: widget.scaffoldKey, routeArgument: RouteArgument(param: _con.market));
//           widget.currentPage = TabMapPage(
//               parentScaffoldKey: widget.scaffoldKey,
//               routeArgument: RouteArgument(param: _con.market));
//           break;
//         case 3:
//           // widget.currentPage = MenuWidget(parentScaffoldKey: widget.scaffoldKey, routeArgument: RouteArgument(param: _con.market));
//           break;
//       }
//     });
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     if (widget.routeArgument != null &&
//         currentUser.value.id !=
//             widget.routeArgument.param['idUser'].toString()) {
//       return Scaffold(
//           key: widget.scaffoldKey,
//           drawer: DrawerWidget(),
//           bottomNavigationBar: Container(
//             height: currentUser.value?.apiToken != null ? 66 : 0,
//             decoration: BoxDecoration(
//               color: Theme.of(context).primaryColor,
//               boxShadow: [
//                 BoxShadow(
//                     color: Theme.of(context).hintColor.withOpacity(0.10),
//                     offset: Offset(0, -4),
//                     blurRadius: 10)
//               ],
//             ),
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.spaceAround,
//               children: [
//                 IconButton(
//                   icon: Icon(
//                     Icons.account_box_outlined,
//                     size: widget.currentTab == 0 ? 28 : 24,
//                     color: widget.currentTab == 0
//                         ? Theme.of(context).accentColor
//                         : Theme.of(context).focusColor,
//                   ),
//                   onPressed: () {
//                     this._selectTab(0);
//                   },
//                 ),
//                 IconButton(
//                   icon: Icon(
//                     Icons.chat_outlined,
//                     size: widget.currentTab == 1 ? 28 : 24,
//                     color: widget.currentTab == 1
//                         ? Theme.of(context).accentColor
//                         : Theme.of(context).focusColor,
//                   ),
//                   onPressed: () {
//                     this._selectTab(1);
//                   },
//                 ),
//                 IconButton(
//                   icon: Icon(
//                     Icons.directions_outlined,
//                     size: widget.currentTab == 2 ? 28 : 24,
//                     color: widget.currentTab == 2
//                         ? Theme.of(context).accentColor
//                         : Theme.of(context).focusColor,
//                   ),
//                   onPressed: () {
//                     this._selectTab(2);
//                   },
//                 ),
//                 MaterialButton(
//                   elevation: 0,
//                   onPressed: () {
//                     _con.makeCall();
//                   },
//                   padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
//                   shape: StadiumBorder(),
//                   color: Theme.of(context).accentColor,
//                   child: Wrap(
//                     spacing: 10,
//                     crossAxisAlignment: WrapCrossAlignment.center,
//                     children: [
//                       Icon(Icons.call, color: Theme.of(context).primaryColor),
//                       Text(
//                         S.of(context).call,
//                         style: TextStyle(color: Theme.of(context).primaryColor),
//                       )
//                     ],
//                   ),
//                 ),
//               ],
//             ),
//           ),
//           body: widget.currentPage ?? CircularLoadingWidget(height: 400));
//     } else {
//       return Scaffold(
//           key: widget.scaffoldKey,
//           drawer: DrawerWidget(),
//           body: widget.currentPage ?? CircularLoadingWidget(height: 400));
//     }
//   }
// }
