import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/PostsLoaderWidget.dart';
import '../models/route_argument.dart';
import 'package:findnear/src/pages/preview_media.dart';
import '../repository/user_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../controllers/profile_controller.dart';

class ProfileListPhotoWidget extends StatefulWidget {
  final String userId;

  ProfileListPhotoWidget({Key key, this.userId}) : super(key: key);

  @override
  _ProfileListPhotoWidgetState createState() => _ProfileListPhotoWidgetState();
}

class _ProfileListPhotoWidgetState extends StateMVC<ProfileListPhotoWidget> {
  ProfileController _profileController;

  _ProfileListPhotoWidgetState() : super(ProfileController()) {
    _profileController = controller;
  }

  initState() {
    if (widget.userId != null) {
      _profileController.getListMedia(widget.userId);
    } else {
      _profileController.getListMedia(currentUser.value?.id ?? "");
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final isDark = Theme.of(context).brightness == Brightness.dark;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Image.asset(isDark ? AppImages.icBackDark : AppImages.icBackLight),
          onPressed: () {
            Navigator.pop(context, false);
          },
        ),
        leadingWidth: 40,
        titleSpacing: 0,
        centerTitle: false,
        title: Text(
          S.of(context).photo,
          style: Theme.of(context).textTheme.bodyText1.copyWith(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
        ),
      ),
      body: listPhoto(),
    );
  }

  Widget listPhoto() {
    return StaggeredGridView.countBuilder(
      crossAxisCount: 6,
      itemCount: _profileController.listImage.length,
      itemBuilder: (BuildContext context, int index) => GestureDetector(
        onTap: () {
          _previewTopicPhoto(_profileController.listImage.elementAt(index).url);
        },
        child: new Container(
          child: CachedNetworkImage(
            imageUrl: _profileController.listImage.elementAt(index).thumb,
            placeholder: (context, url) => PostsLoaderWidget(type: "photo"),
            errorWidget: (context, url, error) => Icon(Icons.error),
          ),
        ),
      ),
      staggeredTileBuilder: (int index) => new StaggeredTile.count(2, 2),
      mainAxisSpacing: 4.0,
      crossAxisSpacing: 4.0,
    );
  }

  Future<bool> _loadMore() async {
    await Future.delayed(Duration(seconds: 2, milliseconds: 1000));
    if (widget.userId != null) {
      await _profileController.getListMedia(widget.userId);
    } else {
      await _profileController.getListMedia(currentUser.value?.id ?? "");
    }
    return true;
  }

  void _previewTopicPhoto(url) {
    Navigator.of(context).push(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) => PreviewMediaWidget(photoPath: url),
      ),
    );
  }
}
