import 'package:dio/dio.dart';
import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/extensions/media_ext.dart';
import 'package:findnear/src/models/create_post.dart';
import 'package:findnear/src/pages/create_post_v2/create_post_state_v2.dart';
import 'package:findnear/src/repository/newsfeed_repository.dart';
import 'package:findnear/src/utils/media/media_picker_utils.dart';
import 'package:findnear/src/utils/media/media_utils.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:images_picker/images_picker.dart';
import 'package:findnear/src/models/media.dart' as MediaModel;

class CreatePostControllerV2 extends BaseController<CreatePostStateV2>{

  final finishedLoadPostData = false.obs;
  var indexScope = 0.obs;
  var listScope = [];
  var contentPost = "".obs;
  var _postRepo = NewsfeedApi();
  final newsfeedRepo = Get.find<NewsfeedRepository>();


  int get _selectedVideoCount =>
      state.listMedia.where((element) => element.isVideo).length;

  int get _selectedImageCount =>
      state.listMedia.where((element) => !element.isVideo).length;

  bool get _canSelectMoreImage {
    return _selectedImageCount < MediaPickerUtils.MAX_IMAGES_PER_POST;
  }

  bool get _canSelectMoreVideo {
    return _selectedVideoCount < MediaPickerUtils.MAX_VIDEO_PER_POST;
  }

  bool get canPost =>
      state.listMedia.isNotEmpty || contentPost.isNotEmpty || state.originalPost != null;

  List<Media> get toBeUploadedImages =>
      state.listMedia.where((e) => !e.path.startsWith("http")).toList();

  List<String> get uploadedImages =>
      state.listMedia.map((e) => e.path).where((e) => e.startsWith("http")).toList();

  List<MediaModel.Media> get shouldRemoveMedias {
    return state.post.value.medias
        .where((e) {
      if (!e.url.startsWith("http")) return false;
      if (uploadedImages.isEmpty) return e?.url?.isNotEmpty ?? false;
      bool isRemoved = true;
      uploadedImages.forEach((element) {
        if (element == e.url) {
          isRemoved = false;
        }
      });
      return isRemoved;
    })
        .map((e) => e)
        .toList();
  }

  int viewerConverter(int serverViewer) {
    // todo: cheat to run, refactor later
    if (serverViewer == 0)
      return 2;
    else if (serverViewer == 2)
      return 0;
    else
      return 1;
  }

  @override
  void onInit() {
    super.onInit();
    listScope = [
      S.of(Get.context).viewerPublic,
      S.of(Get.context).viewerOnlyFriend,
      S.of(Get.context).viewerPrivate
    ];
  }

  @override
  void onClose() {
    super.onClose();
  }

  void removeItem(int index) {
    state.listMedia.removeAt(index);
  }

  Future onClickImage() async {
    if (_canSelectMoreImage)
      await MediaPickerUtils.pickPhotoForFeed(_selectedImageCount)
          .then((media) {
        if (media != null) {
          var result = List<Media>.from(state.listMedia.value);
          result.addAll(media);
          state.listMedia.value = result;
        }
      });
    else
      _showError(S
          .of(Get.context)
          .media_image_over_count(MediaPickerUtils.MAX_IMAGES_PER_POST));
  }

  Future onClickVideo() async {
    if (_canSelectMoreVideo)
      await MediaPickerUtils.pickVideoForFeed(_selectedVideoCount)
          .then((media) {
        if (media != null) {
          var result = List<Media>.from(state.listMedia.value);
          result.addAll(media);
          state.listMedia.value = result;
        }
      });
    else
      _showError(S
          .of(Get.context)
          .media_video_over_count(MediaPickerUtils.MAX_VIDEO_PER_POST));
  }

  void updateScope(int index) {
    indexScope.value = index;
  }

  void onClickSubmit(String id) async {
    int viewer = () {
      // cheat to run, refactor later
      if (indexScope.value == 0)
        return 2;
      else if (indexScope.value == 2)
        return 0;
      else
        return 1;
    }();

    if (canPost) {
      DialogHelper.showLoadingDialog(dismissible: false);
      var medias = <Media>[];
      if (state.listMedia.isNotEmpty) {
        medias = await MediaUtils.optimizeMediasV2(state.listMedia);
      }
      try {
        var post = state.originalPost != null
            ? await _postRepo.createPost(CreatePost(
            content: contentPost.value,
            viewer: '2', //Share post = public post
            parentId: state.originalPost.id))
            : await _postRepo.createPost(CreatePost(
          content: contentPost.value,
          viewer: '$viewer',
          medias: medias,
        ));
        if (post.success){
          // to do
        }
      } catch (e) {
        print("TuanLA - $e");
        if (e is DioError) {
          _showError(e.error);
        } else{
          _showError(e.toString());
        }
         Get.back();
      }
    }
  }

  void _showError(String msg) {
    ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
      content: Text(msg),
    ));
  }

  @override
  CreatePostStateV2 createState() {
    return CreatePostStateV2();
  }
}