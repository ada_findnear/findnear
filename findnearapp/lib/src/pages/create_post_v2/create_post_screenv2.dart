import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/CircularAvatarWidget.dart';
import 'package:findnear/src/elements/NewsfeedItemWidget.dart';
import 'package:findnear/src/elements/app_bar_create.dart';
import 'package:findnear/src/elements/scope_new_feed.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/pages/create_post_v2/create_post_controller_v2.dart';
import 'package:findnear/src/pages/create_post_v2/create_post_state_v2.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:images_picker/images_picker.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:findnear/src/extensions/media_ext.dart';

import '../preview_video.dart';

class CreatePostPageV2 extends StatefulWidget {
  static const String ROUTE_NAME = '/CreatePostPageV2';
  final RouteArgument routeArgument;

  CreatePostPageV2({
    Key key,
    this.routeArgument,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _CreatePostPageState();
  }
}

class _CreatePostPageState extends BaseWidgetState<CreatePostPageV2,
    CreatePostControllerV2,
    CreatePostStateV2> {
  final textEditingController = TextEditingController();
  bool isSharePost = false;

  @override
  void initState() {
    state.user = widget.routeArgument.param['user'];
    state.listMedia.value = widget.routeArgument.param['medias'];
    state.originalPost = widget.routeArgument.param['originalPost'];

    if (state.originalPost != null) {
      isSharePost = true;
    }

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {});

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Obx(() {
        return SafeArea(
          child: VStack([
            _itemAppbar(context),
            _itemBody(),
            VStack([
              _itemInputContent(),
              if (state.originalPost != null)
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    border: Border.all(color: AppColors.divider),
                  ),
                  padding: const EdgeInsets.all(8),
                  margin: const EdgeInsets.all(16),
                  child: NewsfeedItemWidget(
                      post: state.originalPost, isSharedPost: true),
                )
              else
                _itemImagePost(),
            ]).scrollVertical(physics: BouncingScrollPhysics()).expand(),
            if (state.originalPost == null) _itemBottom()
          ]).paddingAll(2),
        );
      }).onTap(() {
        FocusScope.of(Get.context).unfocus();
      }),
    );
  }

  Widget _itemAppbar(BuildContext context) =>
      AppBarCreate(
        title: S
            .of(context)
            .createNewFeed,
        color: AppColors.red,
        isEdit: false,
        onClick: () {
          controller.onClickSubmit(state.user.id);
        },
      );

  Widget _itemBody() =>
      HStack([
        CircularAvatarWidget(
            imageUrl: state.user.image.url ?? "",
            hasBorder: true,
            size: kToolbarHeight * 0.9),
        8.widthBox,
        _itemNameAndScope(state.user.name ?? "",
            controller.listScope[controller.indexScope.value]),
      ]).p(16);

  Widget _itemNameAndScope(String name, String scope) =>
      VStack(
        [
          name.text.black.bold.size(14).make(),
          _itemScope(scope),
        ],
        alignment: MainAxisAlignment.spaceBetween,
        axisSize: MainAxisSize.max,
      ).box.height(kToolbarHeight * 0.9).make();

  Widget _itemScope(String scope) =>
      VxBox(
          child: HStack([
            _getIcons(scope),
            scope.text.black.size(12).make(),
            if (!isSharePost)
              Icon(
                Icons.arrow_drop_down,
                size: 20,
                color: Colors.black,
              ).p(2)
            else
              const SizedBox(width: 4),
          ]))
          .border(color: Colors.grey)
          .roundedSM
          .make()
          .onTap(_openScopeDialog);

  Widget _itemInputContent() =>
      VxTextField(
        controller: textEditingController,
        onChanged: (s) {
          state.contentPost.value = s;
        },
        hint: S
            .of(Get.context)
            .shareSomeThing,
        style: TextStyle(
            fontSize: 16,
            color: Color(0xFF707070),
            fontWeight: FontWeight.w400,
            fontFamily: 'Quicksand'),
        clear: false,
        borderColor: Vx.white,
        fillColor: Vx.white,
        maxLine: null,
        maxLength: 3000,
        inputFormatters: [
          LengthLimitingTextInputFormatter(3000),
        ],
        keyboardType: TextInputType.multiline,
      )
          .paddingSymmetric(horizontal: 16, vertical: 8)
          .scrollVertical(physics: BouncingScrollPhysics());

  Widget _itemImagePost() =>
      GridView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              childAspectRatio: 1, crossAxisCount: 2),
          itemCount: state.listMedia.length,
          itemBuilder: (BuildContext context, int index) =>
              _itemImage(media: state.listMedia[index], index: index));

  void _previewTopicVideo(String path, BuildContext context) {
    Navigator.of(context).push(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) => PreviewVideoWidget(path: path),
      ),
    );
  }

  Widget _itemImage({Media media, int index}) {
    var path = media.thumbPath ?? media.path;
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: InkWell(
        onTap: () {
          if (media.isVideo) {
            _previewTopicVideo(media.path, Get.context);
          }
        },
        child: Stack(
          alignment: Alignment.center,
          children: [
                () {
              if (path.startsWith("http")) {
                return CachedNetworkImage(
                  imageUrl: path,
                  errorWidget: (c, s, d) {
                    return Container(
                      width: Get.size.width,
                      height: Get.size.width,
                      color: Colors.black87,
                    );
                  },
                  fit: BoxFit.cover,
                  width: Get.size.width / 2,
                );
              } else
                return Image.file(
                  File(path),
                  fit: BoxFit.cover,
                  width: Get.size.width / 2,
                );
            }(),
            Center(
              child: Visibility(
                visible: media.isVideo,
                child: Container(
                  width: 33,
                  height: 33,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.black.withOpacity(0.4),
                  ),
                  padding: EdgeInsets.all(4),
                  alignment: Alignment.center,
                  child: SvgPicture.asset(
                    "assets/svg/ic_play.svg",
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            Positioned(
              right: 6,
              top: 6,
              child: InkWell(
                onTap: () {
                  controller.removeItem(index);
                },
                child: ClipOval(
                  child: Container(
                    padding: EdgeInsets.all(4),
                    color: Colors.red.shade300,
                    child: Icon(
                      Icons.close,
                      color: Vx.white,
                      size: 18,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _itemBottom() =>
      HStack(
        [
          Expanded(
            child: InkWell(
              child: Padding(
                padding:
                const EdgeInsets.symmetric(vertical: 12, horizontal: 20),
                child: Center(
                  child: _buildActionItemWidget(
                      text: S
                          .of(Get.context)
                          .upload_image_button,
                      icon: AppImage(
                        AppImages.icImageUpload,
                      )),
                ),
              ),
              onTap: () {
                controller.onClickImage();
              },
            ),
          ),
          Expanded(
            child: InkWell(
              child: Padding(
                padding:
                const EdgeInsets.symmetric(vertical: 12, horizontal: 20),
                child: Center(
                  child: _buildActionItemWidget(
                    text: S
                        .of(Get.context)
                        .upload_video_button,
                    icon: AppImage(
                      AppImages.icLive,
                    ),
                  ),
                ),
              ),
              onTap: () {
                controller.onClickVideo();
              },
            ),
          )
        ],
      ).box
          .color(Vx.white)
          .outerShadow
          .alignCenterRight
          .make();

  Widget _buildActionItemWidget({
    @required String text,
    @required Widget icon,
    VoidCallback onTap,
  }) {
    return GestureDetector(
      onTap: onTap,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          icon,
          SizedBox(width: 7),
          Text(
            text,
            style: Theme
                .of(Get.context)
                .textTheme
                .bodyText1
                .merge(TextStyle(fontSize: 12, color: AppColors.gray)),
          )
        ],
      ),
    );
  }

  void _openScopeDialog() async {
    if (isSharePost) return;
    await Get.bottomSheet(
        ScopeNewFeed(
          index: controller.indexScope.value,
          listScope: controller.listScope,
          onClick: controller.updateScope,
        ),
        isScrollControlled: true,
        backgroundColor: Vx.white);
  }

  Widget _getIcons(String title) {
    var ic;
    if (S
        .of(Get.context)
        .viewerOnlyFriend == title)
      ic = Icons.person;
    else if (S
        .of(Get.context)
        .viewerPublic == title)
      ic = Icons.public;
    else
      ic = Icons.lock;
    return Icon(ic, size: 18, color: AppColors.gray)
        .paddingSymmetric(vertical: 2, horizontal: 4);
  }

  @override
  void dispose() async {
    textEditingController.dispose();
    Get.delete<CreatePostControllerV2>();
    super.dispose();
  }

  @override
  CreatePostControllerV2 createController() =>
      Get.put(CreatePostControllerV2());
}
