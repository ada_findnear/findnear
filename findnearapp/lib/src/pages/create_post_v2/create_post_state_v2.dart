import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/models/user.dart';
import 'package:get/get.dart';
import 'package:images_picker/images_picker.dart';

class CreatePostStateV2 extends BaseState{
  User user;
  Rx<Post> post;
  Post originalPost;
  final finishedLoadPostData = false.obs;
  RxList<Media> listMedia = RxList<Media>();
  var indexScope = 0.obs;
  var listScope = [];
  var contentPost = "".obs;

  CreatePostStateV2(){}
}