
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/controllers/page_controller.dart';
import 'package:findnear/src/global/global_event.dart';
import 'package:findnear/src/models/twilio_target_user.dart';
import 'package:findnear/src/pages/change_password/change_password_view.dart';
import 'package:findnear/src/pages/game/get_coin/view.dart';
import 'package:findnear/src/pages/my_blocking/my_blocking_view.dart';
import 'package:findnear/src/pages/notification/notification_list/notification_list_view.dart';
import 'package:findnear/src/pages/side_menu/side_menu_view.dart';
import 'package:findnear/src/repository/chat_repository.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/settings/global_variable.dart';
import 'package:findnear/src/utils/FirebaseMessageManager.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:url_launcher/url_launcher.dart';

import '../helpers/helper.dart';
import '../models/route_argument.dart';
import 'find_near_page/find_near_page.dart';
import 'login_register.dart';
import 'messages/conversation_list/messages_view.dart';
import 'my_profile/my_profile_view.dart';
import 'newsfeed/newsfeed_view.dart';
import 'tab_map/tab_map_view.dart';

class PagesScreen extends StatelessWidget {
  final dynamic currentTab;

  PagesScreen({Key key, this.currentTab}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PagesWidget(
      currentTab: currentTab,
    );
  }
}

class PagesWidget extends StatefulWidget {
  static const String ROUTE_NAME = '/Pages';

  final dynamic currentTab;

  RouteArgument get routeArgument =>
      currentTab is RouteArgument ? currentTab : null;

  PagesWidget({Key key, this.currentTab});

  @override
  _PagesWidgetState createState() {
    return _PagesWidgetState();
  }
}

class _PagesWidgetState extends State<PagesWidget> with WidgetsBindingObserver {
  PagesController _pagesLogic;
  PageController _pageController;

  RefreshController newsfeedRefreshController =
      RefreshController(initialRefresh: false);
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  AppLifecycleState state;
  bool hasPushedToCall = false;

  TwilioTargetUser targetUser;
  bool shouldShowLocationSharingPopup = true;

  // chat area - just for release - TODO: refactor later
  ChatRepository _chatRepo = ChatRepository();
  Stream<QuerySnapshot> _conversations;

  @override
  initState() {
    super.initState();
    _pageController = PageController(initialPage: widget.currentTab ?? 0);
    _pagesLogic = Get.put(PagesController(initialTab: widget.currentTab ?? 0));
    WidgetsBinding.instance.addObserver(this);
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      /// Nếu cấp quyền từ bên ngoài setting thì vẫn phải cập nhật trong local
      bool location = await Permission.location.isGranted;
      bool locationAlways = await Permission.locationAlways.isGranted;
      bool locationWhenInUse = await Permission.locationWhenInUse.isGranted;

      shouldShowLocationSharingPopup = !location && !locationAlways && !locationWhenInUse;
      bool isChangingVisibleOnMapSetting =
          (await Permission.location.isGranted);
      currentUser.value?.market?.visible = isChangingVisibleOnMapSetting;
      final localDataManager = Get.find<LocalDataManager>();
      localDataManager.setIsVisibleOnMap(isChangingVisibleOnMapSetting);
      _pagesLogic.currentTab.listen(onTabChange);
      _pagesLogic.shouldRefreshNewsFeed.listen(onForceRefreshNewsfeed);
      Get.find<FirebaseMessageManager>().handleOnLaunchMessage();
      await Future.delayed(const Duration(milliseconds: 100));
      prettyLog.d('widget.currentTab = ${widget.currentTab}');
      var confirmLocationSharingNow = false;
      if (widget.currentTab is RouteArgument) {
        final index = int.parse(widget.currentTab.id);
        _pagesLogic.currentTab.value = index;
        if (index == 2) {
          confirmLocationSharingNow = true;
        }
      } else if (widget.currentTab is int) {
        _pagesLogic.currentTab.value = widget.currentTab;
        if (widget.currentTab == 2) {
          confirmLocationSharingNow = true;
        }
      } else {
        _pagesLogic.currentTab.value = 2;
        confirmLocationSharingNow = true;
      }
      // if (isLoggedIn &&
      //     shouldShowLocationSharingPopup &&
      //     confirmLocationSharingNow) {
      //   showConfirmRevealingMyLocation();
      // }
      if (currentUser.value?.id != null) {
        _pagesLogic.listenCallComing(context);
        _pagesLogic.listenAcceptCallFromCallkit(context);
        _pagesLogic.listenEndCallFromCallKit();
        _pagesLogic.listenCallComingIOS(context);
      }

    });
    print(widget.currentTab);
    if (currentUser.value?.apiToken != null) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _handleNotActivatedUser();
        _pagesLogic.checkExpiredAccount(
            currentUser.value?.expiredDate, context);
        _pagesLogic.getUnreadMessageCount(sender: currentUser.value.code);
      });
    }

    _listenNewMessage();

    GlobalEvent.instance.onEnableCheckboxMode.stream.listen((data) {
      debugPrint("quanth: onEnableCheckboxMode= ${data}");
      _pagesLogic.enableCheckboxModeRx.value = data;
    });
  }

  void _listenNewMessage() async {
    _conversations =
        await _chatRepo.getUserConversations(currentUser.value?.id);
  }

  // void showConfirmRevealingMyLocation() async {
  //   /// Nếu đã cấp quyền vị trí rồi thì thôi
  //   if (!shouldShowLocationSharingPopup) return;
  //   /// Nếu đã mở dialog lên thì đóng lại để mở lại
  //   if(isShown) Navigator.of(context).pop();
  //   /// tạo mới hội thoại
  //   _dialog = Dialog(
  //     backgroundColor: Colors.transparent,
  //     insetPadding: EdgeInsets.zero,
  //     child: NeedLocationAccessWarningWidget(),
  //   );
  //   /// gắn lại cờ đã bật dialog
  //   isShown = true;
  //   /// hiển thị dialog
  //   showDialog(
  //     context: Get.context,
  //     barrierDismissible: true,
  //     builder: (BuildContext context) => _dialog,
  //   );
  //   // if(Platform.isIOS){
  //   //   showDialog(
  //   //     context: Get.context,
  //   //     barrierDismissible: true,
  //   //     builder: (BuildContext context) => Dialog(
  //   //       backgroundColor: Colors.transparent,
  //   //       insetPadding: EdgeInsets.zero,
  //   //       child: NeedLocationAccessWarningWidget(),
  //   //     ),
  //   //   );
  //   // } else {
  //   //   shouldShowLocationSharingPopup = false;
  //   //   final localDataManager = Get.find<LocalDataManager>();
  //   //   // final String title = localDataManager.isVisibleOnMap ? S.of(context).isVisibleOnMap : S.of(context).isNotVisibleOnMap;
  //   //   final String action = S.of(context).on;//!localDataManager.isVisibleOnMap ? S.of(context).on : S.of(context).off;
  //   //   final isChangingVisibleOnMapSetting = await DialogHelper.showLocationSharingPopup(
  //   //     title: S.of(context).doYouWantToChangeYourLocationSharingStatus(action),
  //   //     okBtnText: S.of(context).ok,
  //   //   );
  //   //
  //   //   if (isChangingVisibleOnMapSetting ?? false) {
  //   //     // currentUser.value?.market?.visible = !localDataManager.isVisibleOnMap;
  //   //     // localDataManager.setIsVisibleOnMap(!localDataManager.isVisibleOnMap);
  //   //
  //   //     currentUser.value?.market?.visible = isChangingVisibleOnMapSetting;
  //   //     localDataManager.setIsVisibleOnMap(isChangingVisibleOnMapSetting);
  //   //   }
  //   // }
  // }

  void onTabChange(int indexTab) {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (!isLoggedIn && (indexTab == 3 || indexTab == 4)) {
        //Get.to(() => LoginWidget());
        Get.to(() => LoginRegisterPage());
        return;
      }
      if (_pageController?.hasClients ?? false)
        _pageController.jumpToPage(indexTab);
      // if (isLoggedIn && shouldShowLocationSharingPopup && indexTab == 2) {
      //   showConfirmRevealingMyLocation();
      // }

      if (isLoggedIn && indexTab == 4) {
        // When select tab Chat, get user infomation to check current session
        getUser(int.parse(currentUser.value?.id));
      }
    });
  }

  void onForceRefreshNewsfeed(bool isRefresh) {
    try {
      newsfeedRefreshController.requestRefresh();
    } catch (e) {
      logger.e(e);
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _pageController.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState appLifecycleState) {
    if(appLifecycleState == AppLifecycleState.paused){
      _pagesLogic.isAppOnBackground.value = true;
    }else if(appLifecycleState == AppLifecycleState.resumed){
      _pagesLogic.isAppOnBackground.value = false;
    }else if(appLifecycleState == AppLifecycleState.detached){
      _pagesLogic.isAppOnBackground.value = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_pagesLogic == null) return Center(child: LoadingView());
    return WillPopScope(
      onWillPop: Helper.of(context).onWillPop,
      child: Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: false,
        drawer: SideMenuPage(sideMenuPressed: (sideMenu) {
          if (_scaffoldKey.currentState.isDrawerOpen) {
            Navigator.pop(context);
          }
          _onSideMenuPressed?.call(sideMenu, context);
        }),
        // endDrawer: FilterWidget(onFilter: (filter) {
        //   Navigator.of(context).pushNamedAndRemoveUntil(
        //       '/Pages', (router) => router.settings.name == "/Pages",
        //       arguments: _pageController.currentTab ?? 0);
        // }),
        body: _pageController == null
            ? Center(child: LoadingView())
            : PageView(
                controller: _pageController,
                children: [
                  NewsfeedPage(
                    parentScaffoldKey: _scaffoldKey,
                    refreshController: newsfeedRefreshController,
                  ),
                  FindNearPage(parentScaffoldKey: _scaffoldKey),
                  TabMapPage(
                      parentScaffoldKey: _scaffoldKey,
                      routeArgument: widget.routeArgument),
                  MyProfilePage(parentScaffoldKey: _scaffoldKey),
                  MessagesPage(parentScaffoldKey: _scaffoldKey),
                ],
                physics: NeverScrollableScrollPhysics(),
              ),
        bottomNavigationBar: Obx(
          () {
            if (_pagesLogic.enableCheckboxModeRx.value)
              return _buildCheckboxModeBottomBar();
            else
              return _buildNormalBottomBar();
          },
        ),
        // floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        // floatingActionButton: Container(
        //   margin: EdgeInsets.only(top: 20, bottom: 2),
        //   child: FloatingActionButton(
        //     heroTag: 'ic_location_pin',
        //     backgroundColor: Colors.transparent,
        //     elevation: 0,
        //     onPressed: () {
        //       _pageController.onTabPressed(2);
        //     },
        //     child: Container(
        //       width: 42,
        //       height: 42,
        //       padding: EdgeInsets.all(8),
        //       decoration: BoxDecoration(
        //         color: Color.fromRGBO(255, 181, 181, 1),
        //         borderRadius: BorderRadius.all(
        //           Radius.circular(50),
        //         ),
        //       ),
        //       child: SvgPicture.asset(
        //         'assets/svg/ic_location_pin.svg',
        //         color: Theme.of(context).accentColor,
        //         height: 25,
        //         fit: BoxFit.contain,
        //       ),
        //     ),
        //   ),
        // ),
      ),
    );
  }

  Widget _buildNormalBottomBar() {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      iconSize: 42,
      elevation: 8,
      unselectedIconTheme: IconThemeData(color: Theme.of(context).accentColor),
      fixedColor: Theme.of(context).accentColor,
      selectedLabelStyle: TextStyle(
          color: Theme.of(context).accentColor,
          fontSize: 12,
          fontWeight: FontWeight.w500),
      showSelectedLabels: true,
      showUnselectedLabels: false,
      // selectedIconTheme: IconThemeData(size: 28),
      currentIndex: _pagesLogic.currentTab.value,
      onTap: onTabPressed,
      // this will be set when a communication tab is tapped
      items: [
        BottomNavigationBarItem(
          icon: AppImage(
            AppImages.icClock,
            fit: BoxFit.contain,
          ),
          label: S.of(context).news_feed_tab_label,
          activeIcon: AppImage(
            AppImages.icClockSelected,
            fit: BoxFit.contain,
          ),
        ),
        BottomNavigationBarItem(
          icon: SvgPicture.asset(
            'assets/svg/ic_shop.svg',
          ),
          label: S.of(context).market,
          // label: S.of(context).home,
          activeIcon: SvgPicture.asset(
            'assets/svg/ic_shop_selected.svg',
          ),
        ),
        BottomNavigationBarItem(
          icon: Container(
            width: 30,
            height: 30,
            padding: EdgeInsets.all(4),
            decoration: BoxDecoration(
              color: Color.fromRGBO(255, 181, 181, 1.0),
              shape: BoxShape.circle,
            ),
            child: SvgPicture.asset(
              'assets/svg/ic_location_pin.svg',
              color: Colors.white,
              height: 14,
              fit: BoxFit.contain,
            ),
          ),
          label: S.of(context).map_tab_label,
          activeIcon: Container(
            width: 30,
            height: 30,
            padding: EdgeInsets.all(4),
            decoration: BoxDecoration(
              color: Color.fromRGBO(255, 181, 181, 1),
              shape: BoxShape.circle,
            ),
            child: SvgPicture.asset(
              'assets/svg/ic_location_pin.svg',
              color: Theme.of(context).accentColor,
              height: 10,
              fit: BoxFit.contain,
            ),
          ),
        ),
        BottomNavigationBarItem(
          icon: SvgPicture.asset(
            'assets/svg/ic_person.svg',
          ),
          label: S.of(context).me,
          activeIcon: SvgPicture.asset(
            'assets/svg/ic_person_selected.svg',
          ),
        ),
        BottomNavigationBarItem(
          icon: (currentUser.value?.id?.isEmpty ?? true)
              ? SvgPicture.asset('assets/svg/ic_chat.svg')
              : Stack(
            children: [
              SvgPicture.asset('assets/svg/ic_chat.svg'),
              Obx(() {
                int unreadCount = 0;
                if(_pagesLogic?.unReadCountRx?.value?.isNotEmpty ?? false)
                  unreadCount = _pagesLogic?.unReadCountRx?.value[0]?.totalMessageUnRead ?? 0;
                return Visibility(
                    child: Positioned(
                      top: 0,
                      right: 0,
                      child: ClipOval(
                        child: Container(
                          child: Center(
                              child: Text('${unreadCount > 5 ? "N" :unreadCount}',
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white))),
                          color: Colors.red,
                          width: 16,
                          height: 16,
                        ),
                      ),
                    ),
                    visible: unreadCount > 0
                );
              }),
            ],
          ),
          label: S
              .of(context)
              .messages,
          activeIcon: SvgPicture.asset(
            'assets/svg/ic_chat_selected.svg',
          ),
        ),
      ],
    );
  }

  Widget _buildCheckboxModeBottomBar() {
    return Container(
      height: 56.0,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(height: 0.5, color: AppColors.grayLight.withOpacity(0.5)),
          Expanded(
            child: Row(
              children: [
                GestureDetector(
                  onTap: () {
                    bool value = _pagesLogic.selectAllConversationRx.value;
                    GlobalEvent.instance.onSelectAllConversation.add(!value);
                    _pagesLogic.selectAllConversationRx.value = !value;
                  },
                  child: Container(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      !_pagesLogic.selectAllConversationRx.value
                          ? "Chọn tất cả"
                          : "Bỏ chọn tất cả",
                      style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 12,
                        color: AppColors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                Spacer(),
                GestureDetector(
                  onTap: () {
                    Fluttertoast.showToast(
                      msg: "Đánh dấu đã đọc!",
                      toastLength: Toast.LENGTH_LONG,
                      gravity: ToastGravity.TOP,
                      timeInSecForIosWeb: 5,
                    );
                  },
                  child: Container(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      "Đánh dấu đã đọc (1)",
                      style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 12,
                        color: AppColors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void onTabPressed(int index) {
    if (_pagesLogic.currentTab.value == 0 && index == 0) {
      try {
        _pagesLogic.forceRefreshNewsFeed();
      } catch (e) {
        logger.e(e);
      }
      return;
    }
    _pagesLogic.currentTab.value = index;
  }

  void _handleNotActivatedUser() {
    if (!currentUser.value?.isActive) {
      Get.to(() => ChangePasswordPage());
    }
  }

  void _onSideMenuPressed(
      SideMenuNavigate sideMenuNavigate, BuildContext context) async {
    switch (sideMenuNavigate) {
      case SideMenuNavigate.LOGIN:
        NavigatorUtils.toLogin();
        break;
      case SideMenuNavigate.LOGOUT:
        onForceRefreshNewsfeed(true);
        break;
      case SideMenuNavigate.SIGN_UP:
        Navigator.of(context).pushNamed("/SignUp");
        break;
      case SideMenuNavigate.SETTING:
        Navigator.of(context).pushNamed("/Settings");
        break;
      case SideMenuNavigate.PAGE_HOME:
        onTabPressed(1);
        break;
      case SideMenuNavigate.PAGE_NOTIFICATION:
        Get.to(() => NotificationListPage());
        break;
      case SideMenuNavigate.FRIEND_REQUEST:
        Navigator.of(context).pushNamed('/FriendsRequest', arguments: 4);
        break;
      case SideMenuNavigate.FRIEND_LIST:
        Navigator.of(context).pushNamed('/ListFriend', arguments: 4);
        break;
      case SideMenuNavigate.PAGE_CHAT:
        onTabPressed(4);
        break;
      case SideMenuNavigate.GET_COIN:
        Get.toNamed(GetCoinPage.ROUTE_NAME);
        break;
      case SideMenuNavigate.HELP:
        Navigator.of(context).pushNamed('/Help');
        break;
      case SideMenuNavigate.POLICY:
        final privacyPolicyUrl =
            "${GlobalConfiguration().getValue(privacyPolicy)}";
        if (await canLaunch(privacyPolicyUrl)) {
          await launch(privacyPolicyUrl);
        }
        break;
      case SideMenuNavigate.LANGUAGE:
        Navigator.of(context).pushNamed('/Languages');
        break;
      case SideMenuNavigate.PAGE_MAP:
        onTabPressed(2);
        break;
      case SideMenuNavigate.PROFILE:
        // Navigator.of(context).pushNamed('/Profile');
        onTabPressed(3);
        break;
      case SideMenuNavigate.MY_BLOCKING:
        Get.to(() => MyBlockingPage());
        break;
    }
  }
}
