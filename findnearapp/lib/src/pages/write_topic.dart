import 'dart:io';

import 'package:findnear/src/utils/media/media_picker_utils.dart';
import 'package:findnear/src/widgets/compressdialog/view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:images_picker/images_picker.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:video_compress/video_compress.dart';

import '../../generated/l10n.dart';
import '../controllers/profile_controller.dart';
import '../models/post.dart';
import '../repository/user_repository.dart';
import 'video.dart';

typedef HandleSubmit(Post, List);
typedef UpdateUser();

class WriteTopicWidget extends StatefulWidget {
  final bool isWriteTopic;
  final HandleSubmit handleSubmit;
  final Post currentPost;
  final UpdateUser updateUser;

  WriteTopicWidget(
      {Key key,
      this.isWriteTopic,
      this.handleSubmit,
      this.currentPost,
      this.updateUser})
      : super(key: key);

  @override
  _WriteTopicWidgetState createState() => _WriteTopicWidgetState();
}

class _WriteTopicWidgetState extends StateMVC<WriteTopicWidget> {
  ProfileController _con;
  String content;
  bool isSubmit = false;
  String viewer = '2';
  List path = [];
  int numberImage = 0;
  int numberVideo = 0;
  String keyVideo;
  bool isValidMedia = false;
  TextEditingController _topicController = new TextEditingController();
  Color colorCount = Colors.blue;

  _WriteTopicWidgetState() : super(ProfileController()) {
    _con = controller;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (!widget.isWriteTopic) {
      _topicController.text = currentUser.value?.bio ?? "";
    }
    if (widget.currentPost != null) {
      _topicController.text = widget.currentPost.content;
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Theme.of(context).hintColor,
          ),
          onPressed: () {
            Navigator.pop(context, true);
          },
        ),
        backgroundColor: Theme.of(context).primaryColor,
        actions: [
          widget.isWriteTopic && this.widget.currentPost == null
              ? DropdownButton<String>(
                  value: viewer,
                  icon: const Icon(Icons.arrow_drop_down),
                  iconSize: 24,
                  elevation: 16,
                  underline: Container(
                    height: 0,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      viewer = newValue;
                    });
                  },
                  items: <String>['0', '1', '2']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text([
                        S.of(context).viewerPrivate,
                        S.of(context).viewerOnlyFriend,
                        S.of(context).viewerPublic
                      ][int.parse(value)]),
                    );
                  }).toList(),
                )
              : Spacer(),
          TextButton(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Text(
                  !widget.isWriteTopic
                      ? S.of(context).save
                      : (this.widget.currentPost != null
                          ? S.of(context).update
                          : S.of(context).button_submit_post),
                  style: TextStyle(fontSize: 20),
                ),
              ),
              onPressed: () {
                _submitTopic();
                Navigator.pop(context, true);
              })
        ],
      ),
      body: SizedBox.expand(
        child: Container(
          child: Column(
            children: <Widget>[
              Container(
                child: new ConstrainedBox(
                  constraints: BoxConstraints(
                    maxHeight: 200,
                  ),
                  child: TextField(
                    controller: _topicController,
                    maxLength: !widget.isWriteTopic ? 100 : null,
                    autofocus: true,
                    maxLines: null,
                    decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 20, horizontal: 15),
                      hintText: !widget.isWriteTopic
                          ? S.of(context).writeYourBio
                          : S.of(context).writeYourThoughts,
                      hintStyle: TextStyle(
                          fontSize: 18.0, color: Theme.of(context).hintColor),
                      fillColor: Theme.of(context).primaryColor,
                      filled: true,
                      counterStyle: TextStyle(color: colorCount, fontSize: 14),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            new BorderSide(color: Colors.transparent, width: 0),
                      ),
                    ),
                    onChanged: (text) {
                      Color c;
                      if (text.length < 30) {
                        c = Colors.blue;
                      } else if (text.length >= 30 && text.length < 70) {
                        c = Colors.amberAccent;
                      } else {
                        c = Colors.red;
                      }
                      if (text.length > 100) {
                        text = text.substring(0, 99);
                      }
                      setState(() {
                        colorCount = c;
                      });
                    },
                  ),
                ),
              ),
              Expanded(
                child: GridView.count(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  crossAxisCount: 2,
                  crossAxisSpacing: 4.0,
                  mainAxisSpacing: 4.0,
                  children: this._buildGridImagePreview(),
                ),
              ),
              widget.isWriteTopic && this.widget.currentPost == null
                  ? Container(
                      height: 50,
                      child: Padding(
                        padding: EdgeInsets.only(right: 10),
                        child: _fileUploadContainer(),
                      ),
                      decoration:
                          BoxDecoration(color: Theme.of(context).primaryColor),
                    )
                  : Spacer(),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> _buildGridImagePreview() {
    List<Widget> listImage = [];
    if (path.length != 0) {
      path.forEach((element) {
        listImage.add(Container(
            child: Stack(
          children: <Widget>[
            element != keyVideo
                ? Image.file(
                    File(element),
                    fit: BoxFit.cover,
                  )
                : VideoWidget(path: element),
            IconButton(
                onPressed: () {
                  if (element == keyVideo) {
                    setState(() {
                      numberVideo--;
                      keyVideo = '';
                    });
                  } else {
                    setState(() {
                      numberImage--;
                    });
                  }
                  path.removeWhere((item) => item == element);
                  setState(() {
                    path = path;
                  });
                },
                icon: Icon(Icons.cancel))
          ],
        )));
      });
    }
    return listImage;
  }

  Widget _fileUploadContainer() {
    return Row(
      children: [
        Spacer(),
        IconButton(
          icon: new Icon(
            Icons.image_sharp,
            color: Theme.of(context).hintColor,
            size: 30,
          ),
          onPressed: () async {
            _unFocusInput();
            setState(() {
              isValidMedia = false;
            });
            List<Media> res =
                await MediaPickerUtils.pickPhotoForFeed(numberImage);
            if (res != null) {
              for (var media in res) {
                if (media.size > MediaPickerUtils.MAX_MEDIA_SIZE_KB) {
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text(S.of(context).media_over_size_err),
                  ));
                  break;
                }
              }
              res.forEach((element) {
                if (element.size < MediaPickerUtils.MAX_MEDIA_SIZE_KB) {
                  setState(() {
                    path.add(element.path);
                    numberImage++;
                  });
                }
              });
            }
            _focusInput();
          },
        ),
        IconButton(
          icon: new Icon(
            Icons.video_collection_sharp,
            color: Theme.of(context).hintColor,
            size: 30,
          ),
          onPressed: () async {
            _unFocusInput();
            setState(() {
              isValidMedia = false;
            });
            if (numberVideo > 0) {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text(S.of(context).media_video_over_count(1)),
              ));
            } else {
              List<Media> res =
              await MediaPickerUtils.pickVideoForFeedV2();
              if (res != null) {
                res.forEach((element) {
                  if (element.size > MediaPickerUtils.MAX_VIDEO_SIZE_MB) {
                    showDialog(
                        context: context,
                        barrierDismissible: false,
                        builder: (BuildContext context) {
                          return CompressVideoDialog(
                              path: element.path,
                              onCompressDone: (MediaInfo media) {
                                setState(() {
                                  path.add(media.path);
                                  keyVideo = media.path;
                                  numberVideo++;
                                });
                              });
                        });
                  } else {
                    setState(() {
                      path.add(element.path);
                      keyVideo = element.path;
                      numberVideo++;
                    });
                  }
                });
              }
            }
            _focusInput();
          },
        ),
      ],
    );
  }

  void _submitTopic() {
    if (!widget.isWriteTopic) {
      // Giới thiệu bản thân
      String text = _topicController.text;
      if (text.length > 100) {
        text = text.substring(0, 99);
      }
      currentUser.value?.bio = text;
      widget.updateUser();
    } else {
      // description bài post
      if (this.widget.currentPost != null) {
        if (_topicController.text != "" || path.length > 0) {
          widget.handleSubmit(
            new Post.fromJSON({
              'id': widget.currentPost.id,
              'content': _topicController.text,
              'viewer': widget.currentPost.viewer
            }),
            path,
          );
        }
      } else {
        if (_topicController.text != "" || path.length > 0) {
          widget.handleSubmit(
            new Post.fromJSON(
                {'content': _topicController.text, 'viewer': viewer}),
            path,
          );
        }
      }
    }
  }

  void _unFocusInput() {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  void _focusInput() {
    FocusScopeNode currentFocus = FocusScope.of(context);
    FocusScope.of(context).requestFocus(currentFocus.focusedChild);
  }
}
