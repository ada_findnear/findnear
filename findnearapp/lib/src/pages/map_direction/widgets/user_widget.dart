import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/market.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/repository/settings_repository.dart';
import 'package:flutter/material.dart';
import '/../../../generated/l10n.dart';

class UserWidget extends StatelessWidget {
  final User user;
  final double distance; //km

  UserWidget({
    Key key,
    this.user,
    this.distance,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 75,
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          BoxShadow(color: Theme.of(context).focusColor.withOpacity(0.1), blurRadius: 15, offset: Offset(0, 5)),
        ],
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          SizedBox(width: 12),
          Container(
            width: 45,
            height: 45,
            child: CircleAvatar(
              backgroundImage: NetworkImage(user?.image?.thumb ?? ""),
              radius: 45 / 2,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(45 / 2),
              border: Border.all(color: Color(0xFFC83E37), width: 2),
            ),
          ),
          SizedBox(width: 12),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  user.name,
                  overflow: TextOverflow.fade,
                  softWrap: false,
                  style: Theme.of(context).textTheme.caption.copyWith(fontWeight: FontWeight.bold),
                ),
                // Text(Helper.skipHtml(market.owner.bio), overflow: TextOverflow.fade, softWrap: false, style: Theme.of(context).textTheme.caption, maxLines: 1),
                SizedBox(height: 8),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        child: Text(
                          "ID: " + user.code,
                          maxLines: 2,
                          style: Theme.of(context).textTheme.caption.copyWith(color: Colors.red),
                        ),
                      ),
                    ),
                    if ((distance ?? 0) > 0)
                      Container(
                        height: 18,
                        padding: EdgeInsets.symmetric(horizontal: 6),
                        decoration: BoxDecoration(
                            color: Color(0xFFFFD584),
                            borderRadius: BorderRadius.circular(11)),
                        child: Center(
                          child: Text(
                            Helper.getDistance(
                                distance / 1.60934,
                                Helper.of(context)
                                    .trans(text: setting.value.distanceUnit)),
                            style: Theme.of(context)
                                .textTheme
                                .overline
                                .copyWith(color: Color(0xFF333333)),
                          ),
                        ),
                      )
                  ],
                ),
              ],
            ),
          ),
          SizedBox(width: 12),
        ],
      ),
    );
  }
}
