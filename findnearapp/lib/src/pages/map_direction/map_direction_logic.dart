import 'dart:math';

import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/commons/app_configs.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/entities/place_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/services/setting_service.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'map_direction_state.dart';
import '../../repository/user_repository.dart' as repository;
import '../../repository/settings_repository.dart' as settingRepo;

class MapDirectionLogic extends GetxController {
  final state = MapDirectionState();
  final settingService = Get.find<SettingService>();

  void showDirectionUser(User user) async {
    final market = user.market;
    if (market.latitude == null || market.longitude == null) {
      var _currentAddress = await settingRepo.setCurrentLocation();
      settingRepo.deliveryAddress.value = _currentAddress;
      state.currentAddress.value = _currentAddress;
      state.cameraPosition.value = CameraPosition(
        target: LatLng(_currentAddress.latitude, _currentAddress.longitude),
        zoom: AppConfigs.defaultMapZoom,
      );

      ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
        content: Text(S.of(Get.context).no_share_location),
      ));
    } else {
      final lat = double.tryParse(market.latitude);
      final lng = double.tryParse(market.longitude);
      state.market.value = user.market;
      state.cameraPosition.value = CameraPosition(
        target: LatLng(lat, lng),
        zoom: AppConfigs.defaultMapZoom,
      );
      Helper.buildMarkerFromMarket(state.market.value,
              isDatingMarker: settingService.mapDisplayDatingDirection.value)
          .then((marker) {
        state.userMarkers.add(marker);
      });

      state.fetchData.value = LoadStatus.loading;

      try {
        await _getDirectionSteps(double.parse(state.market.value.latitude),
    double.parse(state.market.value.longitude));

        state.fetchData.value = LoadStatus.success;
      } catch (e, s) {
        logger.e(e);
        logger.e(s);
        state.fetchData.value = LoadStatus.failure;
      }
    }
  }

  void showDirectionPlace(PlaceEntity place) async {
    final lat = place.geometry.location.lat;
    final lng = place.geometry.location.lng;

    var marker =  Marker(
      // This marker id can be anything that uniquely identifies each marker.
      markerId: MarkerId(place.formattedAddress),
      position: LatLng(lat,lng),
      infoWindow: InfoWindow(
        title: place.formattedAddress,
      ),

      icon: BitmapDescriptor.defaultMarker,
    );
    state.userMarkers.add(marker);

    state.cameraPosition.value = CameraPosition(
      target: LatLng(lat, lng),
      zoom: AppConfigs.defaultMapZoom,
    );

    state.fetchData.value = LoadStatus.loading;
    try {
      await _getDirectionSteps(lat,lng);
      state.fetchData.value = LoadStatus.success;
    } catch (e, s) {
      logger.e(e);
      logger.e(s);
      state.fetchData.value = LoadStatus.failure;
    }
  }

  void _getDirectionSteps(double lat, double lng) async {
    state.polylinePoints = PolylinePoints();

    var _currentAddress = await settingRepo.setCurrentLocation();
    settingRepo.deliveryAddress.value = _currentAddress;
    state.currentAddress.value = _currentAddress;

    Helper.getMyPositionMarker(state.currentAddress.value.latitude,
            state.currentAddress.value.longitude)
        .then((marker) {
      state.userMarkers.add(marker);
    });

    PolylineResult result =
        await state.polylinePoints.getRouteBetweenCoordinates(
      settingRepo.setting.value.googleMapsKey,
      PointLatLng(_currentAddress.latitude, _currentAddress.longitude),
      PointLatLng(lat,lng),
    );

    state.polylineCoordinates = [];
    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        state.polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }

    PolylineId id = PolylineId('poly');

    Polyline polyline = Polyline(
      polylineId: id,
      color: Colors.red,
      points: state.polylineCoordinates,
      width: 5,
    );
    state.polyLines[id] = polyline;

    state.distance.value = distanceFromPoints(result.points);
  }

  ///Utils
  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }

  double distanceFromPoints(List<PointLatLng> points) {
    try {
      double totalDistance = 0;
      for (var i = 0; i < points.length - 1; i++) {
        totalDistance += calculateDistance(
          points[i].latitude,
          points[i].longitude,
          points[i + 1].latitude,
          points[i + 1].longitude,
        );
      }
      return totalDistance;
    } catch (e) {
      return 0;
    }
  }

  @override
  void onInit() {
    Future.delayed(Duration(milliseconds: 800)).then((value) {
      state.isMapLoading.value = false;
    });
    super.onInit();
  }
}
