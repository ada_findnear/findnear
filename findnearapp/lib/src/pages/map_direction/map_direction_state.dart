import 'package:findnear/src/models/address.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/market.dart';
import 'package:findnear/src/models/user.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapDirectionState {
  final fetchData = LoadStatus.initial.obs;
  final userMarkers = <Marker>[].obs;
  final user = Rxn<User>();
  final cameraPosition = Rxn<CameraPosition>();
  final distance = 0.0.obs;
  final isMapLoading = true.obs;

  PolylinePoints polylinePoints;
  final polyLines = <PolylineId, Polyline>{}.obs;
  List<LatLng> polylineCoordinates = [];
  Rxn<Address> currentAddress = Rxn<Address>();
  Rxn<Market> market = Rxn<Market>();

  MapDirectionState() {
    ///Initialize variables
  }
}
