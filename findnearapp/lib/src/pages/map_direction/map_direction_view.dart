import 'dart:async';

import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/entities/place_entity.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/map_direction/widgets/user_widget.dart';
import 'package:findnear/src/services/setting_service.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'map_direction_logic.dart';
import 'map_direction_state.dart';

class MapDirectionPage extends StatefulWidget {
  final User user;
  final PlaceEntity place;

  MapDirectionPage({this.user, this.place});

  @override
  _MapDirectionPageState createState() => _MapDirectionPageState();
}

class _MapDirectionPageState extends State<MapDirectionPage> {
  final MapDirectionLogic logic = Get.put(MapDirectionLogic());
  final MapDirectionState state = Get.find<MapDirectionLogic>().state;

  final settingService = Get.find<SettingService>();

  Completer<GoogleMapController> _controller = Completer();
  RxBool isVisiableUser = false.obs;

  @override
  void initState() {
    super.initState();
    if(widget.user != null){
      isVisiableUser.value = true;
      logic.showDirectionUser(widget.user);
    }

    if(widget.place != null){
      isVisiableUser.value = false;
      logic.showDirectionPlace(widget.place);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: Stack(
        children: [
          _buildMapWidget(),
          Positioned(
            bottom: MediaQuery.of(context).padding.bottom + 12,
            left: 12,
            right: 12,
            child: Obx(() {
              return isVisiableUser.value ?  UserWidget(
                user: widget.user,
                distance: state.distance.value,
              ):Container();
            }),
          )
        ],
      ),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Theme.of(context).brightness == Brightness.dark
              ? Image.asset(
                  AppImages.icBackDark,
                )
              : Image.asset(
                  AppImages.icBackLight,
                )),
      title: Text(widget.user?.name ?? ""),
    );
  }

  Widget _buildMapWidget() {
    return Obx(() {
      Widget child;
      if (state.isMapLoading.value) {
        child = Center(child: LoadingView());
      } else {
        child = GoogleMap(
          mapToolbarEnabled: true,
          mapType: settingService.mapDisplaySatellite.value ? MapType.satellite : MapType.normal,
          initialCameraPosition: state.cameraPosition.value,
          markers: Set.from(state.userMarkers),
          myLocationEnabled: true,
          zoomControlsEnabled: true,
          myLocationButtonEnabled: true,
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
          },
          polylines: Set<Polyline>.of(state.polyLines.values),
        );
      }
      return AnimatedSwitcher(
        duration: Duration(milliseconds: 1000),
        child: child,
      );
    });
  }

  @override
  void dispose() {
    Get.delete<MapDirectionLogic>();
    super.dispose();
  }
}
