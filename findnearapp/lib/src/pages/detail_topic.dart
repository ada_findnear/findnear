import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/elements/PostsLoaderWidget.dart';
import 'package:findnear/src/pages/preview_media.dart';
import 'package:findnear/src/pages/preview_multiple_media.dart';
import 'package:findnear/src/pages/preview_video.dart';
import 'package:findnear/src/pages/video.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:loadmore/loadmore.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../controllers/comment_controller.dart';
import '../elements/MediaActionSheetWidget.dart';
import '../elements/ProfileTopicAvatarWidget.dart';
import '../elements/ProfileTopicCommentWidget.dart';
import '../elements/ProfileTopicLikesWidget.dart';
import '../elements/ProfileTopicTextWidget.dart';
import '../helpers/loadmore_text_builder.dart';
import '../models/comment.dart';
import '../models/post.dart';

class DetailTopicWidget extends StatefulWidget {
  final Post post;

  DetailTopicWidget({Key key, this.post}) : super(key: key);

  @override
  _DetailTopicWidgetState createState() =>
      _DetailTopicWidgetState(int.parse(this.post.id));
}

class _DetailTopicWidgetState extends StateMVC<DetailTopicWidget>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation _animation;
  CommentController commentController;
  String content;
  String pathImage;
  var _textController = TextEditingController();
  MediaActionSheetWidget _mediaActionSheetWidget;

  _DetailTopicWidgetState(int postId) : super(CommentController(postId)) {
    commentController = controller;
  }

  FocusNode _focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    _controller =
        AnimationController(duration: Duration(milliseconds: 300), vsync: this);
    _animation = Tween(begin: 300.0, end: 50.0).animate(_controller)
      ..addListener(() {
        setState(() {});
      });
    setState(() {
      content = '';
      pathImage = null;
    });

    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        _controller.forward();
      } else {
        _controller.reverse();
      }
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    _focusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _mediaActionSheetWidget = MediaActionSheetWidget(context: context);
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios_outlined),
            onPressed: () {
              Navigator.of(context).pop(false);
            },
          ),
          centerTitle: true,
          title: Text(S.of(context).comment),
        ),
        body: SizedBox.expand(
          // color: Theme.of(context).primaryColor,
          child: Container(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: SingleChildScrollView(
                    padding: EdgeInsets.only(left: 10, right: 10),
                    child: Column(
                      children: [
                        ProfileTopicAvatarWidget(
                          post: widget.post,
                        ),
                        ProfileTopicTextWidget(
                            expandText: true, content: widget.post.content),
                        ListView.separated(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          scrollDirection: Axis.vertical,
                          itemBuilder: (BuildContext context, int index) =>
                              GestureDetector(
                            onTap: () {
                              var url = widget.post.medias.elementAt(index).url;
                              widget.post.medias
                                          .elementAt(index)
                                          .mime_type
                                          .indexOf("image") !=
                                      -1
                                  ? _previewMultipleMedia(
                                      currentIndex: index,
                                      medias: widget.post.medias)
                                  : _previewTopicVideo(url);
                            },
                            child: new Container(
                              color: Colors.black12,
                              child: widget.post.medias
                                          .elementAt(index)
                                          .mime_type
                                          .indexOf("image") !=
                                      -1
                                  ? CachedNetworkImage(
                                      imageUrl: widget.post.medias
                                          .elementAt(index)
                                          .thumb,
                                      fit: BoxFit.cover,
                                      placeholder: (context, url) =>
                                          PostsLoaderWidget(type: "photo"),
                                      errorWidget: (context, url, error) =>
                                          Icon(Icons.error),
                                    )
                                  : VideoWidget(
                                      path: widget.post.medias
                                          .elementAt(index)
                                          .url),
                            ),
                          ),
                          separatorBuilder: (BuildContext context, int index) =>
                              const Divider(),
                          itemCount: widget.post.medias.length,
                        ),
                        // SizedBox(
                        //   height: 10,
                        // ),
                        ProfileTopicLikesWidget(
                          showComment: false,
                          likers_count: widget.post.likers_count,
                          comment_count: widget.post.comments_count,
                        ),
                        // SizedBox(
                        //   height: 10,
                        // ),
                        _showComments(),
                        SizedBox(
                          height: 60,
                        )
                      ],
                    ),
                  ),
                ),
                Align(
                    alignment: Alignment.bottomCenter, child: _editTextField())
              ],
            ),
          ),
        ));
  }

  Widget _showComments() {
    final currentUserId = currentUser.value?.id;
    final postOwnerId = widget.post.userId;
    final isPostOwner = currentUserId == postOwnerId;
    return LoadMore(
      isFinish: !commentController.canLoadMoreComment,
      child: ListView.separated(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemBuilder: (BuildContext context, int index) {
          final comment = commentController.comments.elementAt(index);
          final commentOwnerId = comment.creatorId;
          final isCommentOwner = currentUserId == commentOwnerId;
          return ProfileTopicCommentWidget(
            comment: comment,
            previewTopicPhoto: (url) {
              print("previewTopicPhoto");
              print(url);
              _previewTopicPhoto(url);
            },
            onDeletePressed: () {
              _showConfirmDeleteComment(comment.id);
            },
            allowDelete: isPostOwner || isCommentOwner,
          );
        },
        separatorBuilder: (BuildContext context, int index) => const Divider(),
        itemCount: commentController.comments.length,
      ),
      onLoadMore: _loadMore,
      whenEmptyLoad: false,
      delegate: DefaultLoadMoreDelegate(),
      textBuilder: CustomLoadMoreTextBuilder.getText,
    );
  }

  Future<bool> _loadMore() async {
    await Future.delayed(Duration(seconds: 3, milliseconds: 1000));
    await commentController.listenForComments(widget.post.id);
    return true;
  }

  Widget _editTextField() {
    return SafeArea(
        child: Container(
      child: Column(
        children: [
          pathImage != null
              ? Container(
                  height: 150,
                  child: Stack(
                    children: <Widget>[
                      Image.file(
                        File(pathImage),
                        fit: BoxFit.contain,
                      ),
                      IconButton(
                          onPressed: () {
                            setState(() {
                              pathImage = null;
                            });
                          },
                          icon: Icon(Icons.cancel))
                    ],
                  ))
              : SizedBox.shrink(),
          Container(
            height: 46,
            child: TextField(
              controller: _textController,
              focusNode: _focusNode,
              decoration: InputDecoration(
                focusedBorder: OutlineInputBorder(
                  borderSide:
                      new BorderSide(color: Colors.transparent, width: 0),
                ),
                enabledBorder: const OutlineInputBorder(
                  // width: 0.0 produces a thin "hairline" border
                  borderSide:
                      const BorderSide(color: Colors.black12, width: 1.0),
                ),
                border: OutlineInputBorder(),
                contentPadding:
                    EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                hintText: S.of(context).writeComment,
                hintStyle: TextStyle(
                    fontSize: 16.0, color: Theme.of(context).hintColor),
                fillColor: Theme.of(context).primaryColor,
                filled: true,
                prefixIcon: IconButton(
                  onPressed: () {
                    _addImage();
                  },
                  icon: Icon(Icons.image),
                ),
                suffixIcon: IconButton(
                  onPressed: (content != '' || pathImage != null)
                      ? () async {
                          await commentController.addComment(
                              comment: new Comment.fromJSON({
                                'post_id': int.parse(widget.post.id),
                                'content': content,
                              }),
                              postId: widget.post.id,
                              pathImage: pathImage);
                          await FocusScope.of(context)
                              .requestFocus(FocusNode());
                          await _textController.clear();
                          await setState(() {
                            content = '';
                            pathImage = null;
                          });
                        }
                      : null,
                  icon: Icon(Icons.send),
                ),
              ),
              onChanged: (value) => {
                setState(() {
                  content = value;
                })
              },
            ),
          )
        ],
      ),
    ));
  }

  void _addImage() async {
    _mediaActionSheetWidget.showActionSheet(S.of(context).chooseImage, [
      S.of(context).takePhoto,
      S.of(context).chooseFromGrallery
    ], (int index) {
      if (index == 0) {
        _openCamera((pickedFile) {
          pathImage = pickedFile.path;
        });
      }
      if (index == 1) {
        _openGrallery((pickedFile) {
          pathImage = pickedFile.path;
        });
      }
    });
  }

  void _openCamera(OnPickImageCallback onPickImageCallback) {
    _mediaActionSheetWidget.pickImageDialog(ImageSource.camera,
        (PickedFile pickedFile) {
      print("PickerFile:${pickedFile.path}");
      setState(() {
        onPickImageCallback(pickedFile);
      });
    });
  }

  void _openGrallery(OnPickImageCallback onPickImageCallback) {
    _mediaActionSheetWidget.pickImageDialog(ImageSource.gallery,
        (PickedFile pickedFile) {
      print("PickerFile:${pickedFile.path}");
      setState(() {
        onPickImageCallback(pickedFile);
      });
    });
  }

  void _previewTopicPhoto(url) {
    Navigator.of(context).push(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) => PreviewMediaWidget(photoPath: url),
      ),
    );
  }

  void _previewMultipleMedia({currentIndex, medias}) {
    Navigator.of(context).push(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) => PreviewMultipleMediaWidget(
          currentIndex: currentIndex,
          medias: medias,
        ),
      ),
    );
  }

  void _previewTopicVideo(url) {
    Navigator.of(context).push(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) => PreviewVideoWidget(path: url),
      ),
    );
  }

  void _showConfirmDeleteComment(String commentId) {
    DialogHelper.showAlertDialog(
      context: context,
      title: S.of(context).confirm,
      description: S.of(context).confirmRemovePost,
      okText: S.of(context).ok,
      okFunction: () {
        commentController.deleteComment(commentId);
      },
    );
  }
}
