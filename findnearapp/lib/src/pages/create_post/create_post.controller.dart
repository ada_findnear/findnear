import 'package:dio/dio.dart';
import 'package:findnear/src/extensions/media_ext.dart';
import 'package:findnear/src/models/create_post.dart';
import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/models/input/UpdatePostGroupStatusInput.dart';
import 'package:findnear/src/models/media.dart' as MediaModel;
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/repository/findnear_group_repository.dart';
import 'package:findnear/src/repository/newsfeed_repository.dart';
import 'package:findnear/src/utils/enum_utils.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:findnear/src/utils/media/media_picker_utils.dart';
import 'package:findnear/src/utils/media/media_utils.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:images_picker/images_picker.dart';
import 'package:rxdart/rxdart.dart' as rx;

import '../../../generated/l10n.dart';

class CreatePostController extends GetxController {
  Rx<Post> post;
  Post originalPost;
  final finishedLoadPostData = false.obs;
  RxList<Media> listMedia = RxList<Media>([]);
  var indexScope = 0.obs;
  var listScope = [];
  var contentPost = "".obs;
  var user = User();
  var _postRepo = NewsfeedApi();
  final newsfeedRepo = Get.find<NewsfeedRepository>();
  final FindNearGroupRepository _groupRepository = Get.find();

  // Nếu màn tạo mới bài post trong group thì cần truyền group vào.
  GroupEntity group = null;

  CreatePostController({
    Post post,
    this.originalPost,
  }) {
    this.post = post.obs;
  }

  int get _selectedVideoCount =>
      listMedia.value.where((element) => element.isVideo).length;

  int get _selectedImageCount =>
      listMedia.value.where((element) => !element.isVideo).length;

  bool get _canSelectMoreImage {
    return _selectedImageCount < MediaPickerUtils.MAX_IMAGES_PER_POST;
  }

  bool get _canSelectMoreVideo {
    return _selectedVideoCount < MediaPickerUtils.MAX_VIDEO_PER_POST;
  }

  bool get isEditPost => post?.value != null;

  bool get canPost =>
      listMedia.isNotEmpty || contentPost.isNotEmpty || originalPost != null;

  bool get canPostEdit {
    if (!finishedLoadPostData.value) return false;
    return toBeUploadedImages.isNotEmpty ||
        shouldRemoveMedias.isNotEmpty ||
        (contentPost.value != post.value.content) ||
        (viewerConverter(indexScope.value).toString() != post.value.viewer) ||
        originalPost != null;
  }

  List<Media> get toBeUploadedImages =>
      listMedia.where((e) => !e.path.startsWith("http")).toList();

  List<String> get uploadedImages =>
      listMedia.map((e) => e.path).where((e) => e.startsWith("http")).toList();

  List<MediaModel.Media> get shouldRemoveMedias {
    return post.value.medias
        .where((e) {
          if (!e.url.startsWith("http")) return false;
          if (uploadedImages.isEmpty) return e?.url?.isNotEmpty ?? false;
          bool isRemoved = true;
          uploadedImages.forEach((element) {
            if (element == e.url) {
              isRemoved = false;
            }
          });
          return isRemoved;
        })
        .map((e) => e)
        .toList();
  }

  int viewerConverter(int serverViewer) {
    // todo: cheat to run, refactor later
    if (serverViewer == 0)
      return 2;
    else if (serverViewer == 2)
      return 0;
    else
      return 1;
  }

  @override
  void onInit() {
    user = Get.arguments;
    super.onInit();
    listScope = [
      S.of(Get.context).viewerPublic,
      S.of(Get.context).viewerOnlyFriend,
      S.of(Get.context).viewerPrivate
    ];
    if (isEditPost) {
      int viewer = viewerConverter(int.tryParse(post?.value?.viewer) ?? 0);
      indexScope.value = viewer;
      contentPost.value = post.value.content;
      listMedia.addAll(post.value.medias
          .map((e) => Media(path: e.url, thumbPath: e.thumb, size: 50))
          .toList());
    }
  }

  @override
  void onClose() {
    listMedia.clear();
    indexScope.value = 0;
    contentPost.value = '';
    post.value = null;
    finishedLoadPostData.value = false;
    super.onClose();
  }

  void removeItem(int index) {
    listMedia.removeAt(index);
  }

  Future onClickImage() async {
    if (_canSelectMoreImage)
      await MediaPickerUtils.pickPhotoForFeed(_selectedImageCount)
          .then((media) {
        if (media != null) {
          var result = List<Media>.from(listMedia.value);
          result.addAll(media);
          listMedia.value = result;
        }
      });
    else
      _showError(S
          .of(Get.context)
          .media_image_over_count(MediaPickerUtils.MAX_IMAGES_PER_POST));
  }

  Future onClickVideo() async {
    if (_canSelectMoreVideo)
      await MediaPickerUtils.pickVideoForFeed(_selectedVideoCount)
          .then((media) {
        if (media != null) {
          var result = List<Media>.from(listMedia.value);
          result.addAll(media);
          listMedia.value = result;
        }
      });
    else
      _showError(S
          .of(Get.context)
          .media_video_over_count(MediaPickerUtils.MAX_VIDEO_PER_POST));
  }

  void updateScope(int index) {
    indexScope.value = index;
  }

  void onClickSubmit(String id) async {
    int viewer = () {
      // cheat to run, refactor later
      if (indexScope.value == 0)
        return 2;
      else if (indexScope.value == 2)
        return 0;
      else
        return 1;
    }();

    if (canPost) {
      DialogHelper.showLoadingDialog(dismissible: false);
      var medias = <Media>[];
      if (listMedia.isNotEmpty) {
        medias = await MediaUtils.optimizeMediasV2(listMedia);
      }
      try {
        var res = originalPost != null
            ? await _postRepo.createPost(CreatePost(
                content: contentPost.value,
                viewer: '2', //Share post = public post
                parentId: originalPost.id))
            : await _postRepo.createPost(CreatePost(
                content: contentPost.value,
                viewer: '$viewer',
                medias: medias,
              ));
        if (res.success) {
          final post = res.data;
          await addPostToGroup(post);
          _clearData();
        }
      } catch (e) {
        logger.e(e.toString());
        goBack();
        if (e is DioError) {
          _showError(e.error);
        } else
          _showError(e.toString());
      }
    }
  }

  void onClickSubmitEdit() async {
    int viewer = viewerConverter(indexScope.value);

    if (canPostEdit) {
      if (contentPost.value != "" || listMedia.length > 0) {
        DialogHelper.showLoaderDialog();
        try {
          final result = await rx.ZipStream([
            await _postRepo
                .updatePost(
                  this.post.value.id,
                  CreatePost(content: contentPost.value, viewer: '${viewer}'),
                )
                .asStream(),
            if (shouldRemoveMedias.isNotEmpty)
              ...shouldRemoveMedias
                  .map((e) => newsfeedRepo.removeMediaFromPost(e).asStream())
                  .toList(),
            if (toBeUploadedImages.isNotEmpty)
              await newsfeedRepo
                  .addImagesToPost(this.post.value.id, toBeUploadedImages)
                  .asStream(),
          ], (value) => value.join());

          result.listen((value) {
            if (value != null) {
              _clearData();
            }
          }).onError((e) {
            _clearData();
          });
        } catch (e) {
          if (e is DioError) {
            goBack();
            _showError(e.error);
          }
        }
      } else {
        _clearData();
      }
    }
  }

  void addPostToGroup(Post post) async {
    if (group == null) {
      return;
    }
    try {
      final input = UpdatePostGroupStatusInput(
        postId: int.parse(post.id),
        groupId: group.id,
        status: PostGroupStatus.pending,
      );
      await _groupRepository.updatePostGroupStatus(input);
    } catch (e) {
      print("addPostToGroup");
      print(e);
    }
  }

  void _showError(String msg) {
    ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
      content: Text(msg),
    ));
  }

  void _clearData() {
    goBack();
    goBack(argument: true);
  }
}
