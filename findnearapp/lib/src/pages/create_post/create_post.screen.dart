import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/elements/NewsfeedItemWidget.dart';
import 'package:findnear/src/elements/app_bar_create.dart';
import 'package:findnear/src/extensions/media_ext.dart';
import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/models/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:images_picker/images_picker.dart';
import 'package:velocity_x/velocity_x.dart';

import '../../../generated/l10n.dart';
import '../../commons/app_colors.dart';
import '../../commons/app_images.dart';
import '../../elements/CircularAvatarWidget.dart';
import '../../elements/scope_new_feed.dart';
import '../../widgets/common.dart';
import '../preview_video.dart';
import 'create_post.controller.dart';

class CreatePostScreen extends GetWidget<CreatePostController> {
  final User user;
  final List<Media> medias;
  final Post originalPost;

  // Nếu màn tạo mới bài post trong group thì cần truyền group vào.
  final GroupEntity group;
  final TextEditingController textEditingController = TextEditingController();

  CreatePostScreen({
    this.user,
    this.medias,
    this.originalPost,
    this.group,
  });

  // bool get isSharePost => originalPost != null;

  @override
  Widget build(BuildContext context) {
    if (controller.listMedia.isEmpty && medias != null) {
      controller.listMedia.value = medias;
    }

    if (!controller.finishedLoadPostData.value) {
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        textEditingController.text =
            controller.isEditPost ? controller.post.value.content : "";
        controller.finishedLoadPostData.value = true;
        controller.group = group;
      });
    }

    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: VStack([
          _itemAppbar(context),
          _itemBody(),
          VStack([
            _itemInputContent(),
            if (originalPost != null)
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  border: Border.all(color: AppColors.divider),
                ),
                padding: const EdgeInsets.all(8),
                margin: const EdgeInsets.all(16),
                child:
                    NewsfeedItemWidget(post: originalPost, isSharedPost: true),
              )
            else
              _itemImagePost(),
          ]).scrollVertical(physics: BouncingScrollPhysics()).expand(),
          if (originalPost == null) _itemBottom()
        ]).paddingAll(2),
      ).onTap(() {
        FocusScope.of(Get.context).unfocus();
      }),
    );
  }

  Widget _itemAppbar(BuildContext context) => Obx(() => AppBarCreate(
        title: controller.isEditPost
            ? S.of(context).editPostTitle
            : S.of(context).createNewFeed,
        color: (controller.isEditPost
                ? controller.canPostEdit
                : controller.canPost)
            ? AppColors.red
            : Vx.red300,
        isEdit: controller.isEditPost,
        onClick: () => controller.isEditPost
            ? controller.onClickSubmitEdit()
            : controller.onClickSubmit(user.id),
      ));

  Widget _itemBody() => HStack([
        CircularAvatarWidget(
            imageUrl: user.image.url ?? "",
            hasBorder: true,
            size: kToolbarHeight * 0.9),
        8.widthBox,
        Obx(() => _itemNameAndScope(
            user.name ?? "", controller.listScope[controller.indexScope.value]))
      ]).p(16);

  Widget _itemNameAndScope(String name, String scope) => VStack(
        [
          name.text.black.bold.size(14).make(),
          _itemScope(scope),
        ],
        alignment: MainAxisAlignment.spaceBetween,
        axisSize: MainAxisSize.max,
      ).box.height(kToolbarHeight * 0.9).make();

  Widget _itemScope(String scope) => VxBox(
          child: HStack([
        _getIcons(scope),
        scope.text.black.size(12).make(),
       // if (!isSharePost)
          Icon(
            Icons.arrow_drop_down,
            size: 20,
            color: Colors.black,
          ).p(2)
        // else
        //   const SizedBox(width: 4),4
      ])).border(color: Colors.grey).roundedSM.make().onTap(_openScopeDialog);

  Widget _itemInputContent() => VxTextField(
        controller: textEditingController,
        onChanged: (s) {
          controller.contentPost.value = s;
        },
        hint: S.of(Get.context).shareSomeThing,
        style: TextStyle(
            fontSize: 16,
            color: Color(0xFF707070),
            fontWeight: FontWeight.w400,
            fontFamily: 'Quicksand'),
        clear: false,
        borderColor: Vx.white,
        fillColor: Vx.white,
        maxLine: null,
        maxLength: 3000,
        inputFormatters: [
          LengthLimitingTextInputFormatter(3000),
        ],
        keyboardType: TextInputType.multiline,
      )
          .paddingSymmetric(horizontal: 16, vertical: 8)
          .scrollVertical(physics: BouncingScrollPhysics());

  Widget _itemImagePost() => Obx(() => GridView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          childAspectRatio: 1, crossAxisCount: 2),
      itemCount: controller.listMedia.length,
      itemBuilder: (BuildContext context, int index) =>
          _itemImage(media: controller.listMedia[index], index: index)));

  void _previewTopicVideo(String path, BuildContext context) {
    Navigator.of(context).push(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) => PreviewVideoWidget(path: path),
      ),
    );
  }

  Widget _itemImage({Media media, int index}) {
    var path = media.thumbPath ?? media.path;
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: InkWell(
        onTap: () {
          if (media.isVideo) {
            _previewTopicVideo(media.path, Get.context);
          }
        },
        child: Stack(
          alignment: Alignment.center,
          children: [
            () {
              if (path.startsWith("http")) {
                return CachedNetworkImage(
                  imageUrl: path,
                  errorWidget: (c, s, d) {
                    return Container(
                      width: Get.size.width,
                      height: Get.size.width,
                      color: Colors.black87,
                    );
                  },
                  fit: BoxFit.cover,
                  width: Get.size.width / 2,
                );
              } else
                return Image.file(
                  File(path),
                  fit: BoxFit.cover,
                  width: Get.size.width / 2,
                );
            }(),
            Center(
              child: Visibility(
                visible: media.isVideo,
                child: Container(
                  width: 33,
                  height: 33,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.black.withOpacity(0.4),
                  ),
                  padding: EdgeInsets.all(4),
                  alignment: Alignment.center,
                  child: SvgPicture.asset(
                    "assets/svg/ic_play.svg",
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            Positioned(
              right: 6,
              top: 6,
              child: InkWell(
                onTap: () {
                  controller.removeItem(index);
                },
                child: ClipOval(
                  child: Container(
                    padding: EdgeInsets.all(4),
                    color: Colors.red.shade300,
                    child: Icon(
                      Icons.close,
                      color: Vx.white,
                      size: 18,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  // Align(
  // child: VxBox(
  // child: Icon(
  // Icons.close,
  // color: Vx.white,
  // size: 15,
  // ))
  //     .roundedFull
  //     .color(Vx.red300)
  //     .padding(EdgeInsets.all(2))
  //     .margin(EdgeInsets.all(8))
  //     .make()
  //     .onTap(() => controller.removeItem(index)),
  // alignment: Alignment.topRight)
  // ]).paddingAll(4);
  Widget _itemBottom() => HStack(
        [
          Expanded(
            child: InkWell(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 12, horizontal: 20),
                child: Center(
                  child: _buildActionItemWidget(
                      text: S.of(Get.context).upload_image_button,
                      icon: AppImage(
                        AppImages.icImageUpload,
                      )),
                ),
              ),
              onTap: () {
                controller.onClickImage();
              },
            ),
          ),
          Expanded(
            child: InkWell(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 12, horizontal: 20),
                child: Center(
                  child: _buildActionItemWidget(
                    text: S.of(Get.context).upload_video_button,
                    icon: AppImage(
                      AppImages.icLive,
                    ),
                  ),
                ),
              ),
              onTap: () {
                controller.onClickVideo();
              },
            ),
          )
        ],
      ).box.color(Vx.white).outerShadow.alignCenterRight.make();

  Widget _buildActionItemWidget({
    @required String text,
    @required Widget icon,
    VoidCallback onTap,
  }) {
    return GestureDetector(
      onTap: onTap,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          icon,
          SizedBox(width: 7),
          Text(
            text,
            style: Theme.of(Get.context)
                .textTheme
                .bodyText1
                .merge(TextStyle(fontSize: 12, color: AppColors.gray)),
          )
        ],
      ),
    );
  }

  void _openScopeDialog() async {
    // if (isSharePost) return;
    await Get.bottomSheet(
        ScopeNewFeed(
          index: controller.indexScope.value,
          listScope: controller.listScope,
          onClick: controller.updateScope,
        ),
        isScrollControlled: true,
        backgroundColor: Vx.white);
  }

  Widget _getIcons(String title) {
    var ic;
    if (S.of(Get.context).viewerOnlyFriend == title)
      ic = Icons.person;
    else if (S.of(Get.context).viewerPublic == title)
      ic = Icons.public;
    else
      ic = Icons.lock;
    return Icon(ic, size: 18, color: AppColors.gray)
        .paddingSymmetric(vertical: 2, horizontal: 4);
  }
}
