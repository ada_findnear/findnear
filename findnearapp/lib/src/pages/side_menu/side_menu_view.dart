import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/pages/side_menu/widgets/menu_item_widget.dart';
import 'package:findnear/src/pages/side_menu/widgets/no_user_header_widget.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/services/setting_service.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'side_menu_logic.dart';
import 'side_menu_state.dart';
import 'widgets/user_header_widget.dart';
import 'package:findnear/src/repository/settings_repository.dart' as settingRepo;

enum SideMenuNavigate {
  LOGIN,
  LOGOUT,
  SIGN_UP,
  SETTING,
  PAGE_HOME,
  PAGE_NOTIFICATION,
  FRIEND_REQUEST,
  FRIEND_LIST,
  PAGE_CHAT,
  GET_COIN,
  HELP,
  POLICY,
  LANGUAGE,
  PAGE_MAP,
  PROFILE,
  MY_BLOCKING,
}

class SideMenuPage extends StatefulWidget {
  final Function(SideMenuNavigate) sideMenuPressed;

  const SideMenuPage({Key key, this.sideMenuPressed}) : super(key: key);
  @override
  _SideMenuPageState createState() => _SideMenuPageState();
}

class _SideMenuPageState extends State<SideMenuPage> {
  final SideMenuLogic logic = Get.put(SideMenuLogic());
  final SideMenuState state = Get.find<SideMenuLogic>().state;

  final settingService = Get.find<SettingService>();

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          GestureDetector(
            onTap: () => widget.sideMenuPressed?.call(
                currentUser.value?.apiToken != null
                    ? SideMenuNavigate.PROFILE
                    : SideMenuNavigate.LOGIN),
            child: currentUser.value?.apiToken != null
                ? UserHeaderWidget(user: currentUser.value, onTapFindCoin: (){
              widget.sideMenuPressed?.call(SideMenuNavigate.GET_COIN);
            })
                : NoUserHeaderWidget(
                    onLoginPressed: () {
                      widget.sideMenuPressed?.call(SideMenuNavigate.LOGIN);
                    },
                    onRegisterPressed: () {
                      widget.sideMenuPressed?.call(SideMenuNavigate.SIGN_UP);
                    },
                  ),
          ),
          _settingWidget,
          _marketWidget,
          _notificationWidget,
          _friendRequestWidget,
          _friendsWidget,
          _myBlockingWidget,
          _chatWidget,
          //_getCoinWidget,
          Container(
              height: 4,
              width: double.infinity,
              color:
                  Theme.of(context).textTheme.bodyText2.color.withOpacity(0.1)),
          _helpWidget,
          _languageWidget,
          _policyWidget,
          // _themeWidget,
          _loginWidget,
          settingRepo.setting.value.enableVersion
              ? ListTile(
                  dense: true,
                  title: Text(
                    S.of(context).version + " " + settingRepo.setting.value.appVersion.toString(),
                    style: Theme.of(context).textTheme.bodyText2,
                  ),
                  trailing: Icon(
                    Icons.remove,
                    color: Theme.of(context).focusColor.withOpacity(0.3),
                  ),
                )
              : SizedBox(),
        ],
      ),
    );
  }

  Widget get _settingWidget {
    return MenuItemWidget(
      label: S.of(context).profile_settings,
      iconName: AppImages.icMenuProfile,
      onPressed: () {
        widget.sideMenuPressed?.call(currentUser.value?.apiToken != null
            ? SideMenuNavigate.SETTING
            : SideMenuNavigate.LOGIN);
      },
    );
  }

  Widget get _marketWidget {
    return MenuItemWidget(
      label: S.of(context).market,
      iconName: AppImages.icMenuShop,
      onPressed: () {
        widget.sideMenuPressed?.call(currentUser.value?.apiToken != null
            ? SideMenuNavigate.PAGE_HOME
            : SideMenuNavigate.LOGIN);
      },
    );
  }

  Widget get _notificationWidget {
    return MenuItemWidget(
      label: S.of(context).notifications,
      iconName: AppImages.icMenuNotification,
      onPressed: () {
        widget.sideMenuPressed?.call(SideMenuNavigate.PAGE_NOTIFICATION);
      },
    );
  }

  Widget get _friendRequestWidget {
    return currentUser.value?.apiToken != null
        ? MenuItemWidget(
            label: S.of(context).friendRequest,
            iconName: AppImages.icMenuFriendRequest,
            onPressed: () {
              widget.sideMenuPressed(currentUser.value?.apiToken != null
                  ? SideMenuNavigate.FRIEND_REQUEST
                  : SideMenuNavigate.LOGIN);
            },
          )
        : SizedBox(
            height: 0,
          );
  }

  Widget get _friendsWidget {
    return currentUser.value?.apiToken != null
        ? MenuItemWidget(
            label: S.of(context).friendList,
            iconName: AppImages.icMenuFriends,
            onPressed: () {
              widget.sideMenuPressed(currentUser.value?.apiToken != null? SideMenuNavigate.FRIEND_LIST: SideMenuNavigate.LOGIN);
            },
          )
        : SizedBox(
            height: 0,
          );
  }

  Widget get _myBlockingWidget {
    return currentUser.value?.apiToken != null
        ? MenuItemWidget(
            label: S.of(context).myBlocking,
            iconName: AppImages.icMenuBlocking,
            onPressed: () {
              widget.sideMenuPressed(SideMenuNavigate.MY_BLOCKING);
            },
          )
        : SizedBox(
            height: 0,
          );
  }

  Widget get _chatWidget {
    return MenuItemWidget(
      label: S.of(context).messages,
      iconName: AppImages.icMenuChat,
      onPressed: () {
        widget.sideMenuPressed(SideMenuNavigate.PAGE_CHAT);
      },
    );
  }

  Widget get _helpWidget {
    return MenuItemWidget(
      label: S.of(context).help__support,
      iconName: AppImages.icMenuSupport,
      onPressed: () {
        widget.sideMenuPressed(SideMenuNavigate.HELP);
      },
    );
  }

  Widget get _policyWidget {
    return MenuItemWidget(
      label: S.of(context).termsAndConditions.capitalizeFirst,
      iconName: AppImages.icPolicy,
      onPressed: () {
        widget.sideMenuPressed(SideMenuNavigate.POLICY);
      },
    );
  }

  Widget get _languageWidget {
    return MenuItemWidget(
      label: S.of(context).languages,
      iconName: AppImages.icMenuLanguage,
      onPressed: () {
        widget.sideMenuPressed(SideMenuNavigate.LANGUAGE);
      },
    );
  }

  Widget get _themeWidget {
    return MenuItemWidget(
      label: Theme.of(context).brightness == Brightness.dark
          ? S.of(context).light_mode
          : S.of(context).dark_mode,
      iconName: AppImages.icMenuTheme,
      onPressed: () {
        if (settingService.currentThemeMode.value == ThemeMode.dark) {
          settingService.changeThemeMode(ThemeMode.light);
        } else {
          settingService.changeThemeMode(ThemeMode.dark);
        }
      },
    );
  }

  Widget get _loginWidget {
    if (currentUser.value?.apiToken == null) {
      return Container();
    }
    return MenuItemWidget(
      label: currentUser.value?.apiToken != null
          ? S.of(context).log_out
          : S.of(context).login,
      iconName: AppImages.icMenuLogout,
      onPressed: () {
        if (currentUser.value?.apiToken != null) {
          DialogHelper.showLogoutPopup().then((isLogout) {
            if (isLogout == true) {
              prettyLog.d('logout() starting');
              logout().then((_) => prettyLog.d('logout() done'));
              widget.sideMenuPressed?.call(SideMenuNavigate.PAGE_MAP);
              widget.sideMenuPressed?.call(SideMenuNavigate.LOGOUT);
            }
          });
        } else {
          widget.sideMenuPressed?.call(SideMenuNavigate.LOGIN);
        }
      },
    );
  }

  @override
  void dispose() {
    Get.delete<SideMenuLogic>();
    super.dispose();
  }
}
