import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:findnear/src/widgets/shimmer/app_shimmer.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

class UserHeaderWidget extends StatelessWidget {
  final User user;

  final GestureTapCallback onTapFindCoin;

  UserHeaderWidget({Key key, this.user, this.onTapFindCoin}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 20),
      color: Theme.of(context).brightness == Brightness.light
          ? Color(0xFFFFdbdb)
          : Color(0xFF707070),
      child: Column(
        children: [
          Container(
            height: 60,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                _avatarWidget(context),
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(right: 1),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          Helper.limitString(
                              user.name, limit: 20),
                            style: Theme.of(context).textTheme.headline1.copyWith(
                                fontSize: 16, fontWeight: FontWeight.bold, ),
                          ),
                        Text(
                          "ID: ${user.code}",
                          style: Theme.of(context).textTheme.headline6.copyWith(
                              fontSize: 14, fontWeight: FontWeight.normal),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          _buildFindCoin(context),
        ],
      ),
    );
  }

  Widget _buildFindCoin(BuildContext context) {
    return GestureDetector(
      onTap: onTapFindCoin,
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(top: 20),
            width: double.infinity,
            height: 20,
            color: Theme.of(context).brightness == Brightness.light
                ? Color(0xFFFAFAFA)
                : Color(0xFF303030),
          ),
          Align(
            child: Container(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 3),
                margin: EdgeInsets.all(2),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AppImage('assets/svg/ic_dollar_gold.svg'),
                    SizedBox(width: 3),
                    Text.rich(
                      TextSpan(
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          fontSize: 16,
                          color: const Color(0xffffffff),
                        ),
                        children: [
                          TextSpan(
                            text: user.currentPoint?.toString() ?? '0',
                            style: TextStyle(
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          TextSpan(
                            text: ' Find Xu',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(width: 3),
                    AppImage('assets/svg/ic_arrow_right.svg')
                  ],
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(18.0),
                  color: const Color(0xffff0000),
                ),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(18.0),
                gradient: LinearGradient(
                  begin: Alignment(-0.87, -0.88),
                  end: Alignment(1.06, 1.3),
                  colors: [
                    const Color(0xffcb0000),
                    const Color(0xffe18588),
                    const Color(0xffe05959),
                    const Color(0xffcc3a3a),
                    const Color(0xff9a0000),
                    const Color(0xffc83e37)
                  ],
                  stops: [0.0, 0.261, 0.374, 0.663, 0.88, 1.0],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _avatarWidget(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 15),
      child: Stack(
        children: [
          Container(
            width: 60,
            height: 60,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(60)),
              child: CachedNetworkImage(
                height: 60,
                width: 60,
                fit: BoxFit.cover,
                imageUrl: user.image.thumb,
                placeholder: (context, url) =>
                    AppShimmer(width: 60, height: 60, cornerRadius: 30),
                errorWidget: (context, url, error) => Icon(Icons.error_outline),
              ),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(30)),
              border: Border.all(color: Colors.red, width: 2),
            ),
          ),
          Positioned(
            top: 0,
            right: 0,
            child: user.verifiedPhone ?? false
                ? Icon(
                    Icons.check_circle,
                    color: Theme.of(context).accentColor,
                    size: 20,
                  )
                : SizedBox(),
          ),
        ],
      ),
    );
  }
}
