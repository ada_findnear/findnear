import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MenuItemWidget extends StatelessWidget {
  final String label;
  final String iconName;
  final VoidCallback onPressed;

  MenuItemWidget({
    Key key,
    this.label,
    this.iconName,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        height: 46.w,
        child: Row(
          children: [
            Container(
              width: 46.w,
              child: iconName == AppImages.icPolicy
                  ? AppImage(iconName, width: 17.w, color: AppColors.gray2)
                  : Image.asset(iconName),
            ),
            Text(
              label ?? "",
              style: Theme.of(context).textTheme.subtitle1.copyWith(fontSize: 14),
            ),
          ],
        ),
      ),
    );
  }
}
