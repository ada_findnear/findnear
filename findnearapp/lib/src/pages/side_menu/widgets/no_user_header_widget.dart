import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/widgets/shimmer/app_shimmer.dart';
import 'package:flutter/material.dart';

class NoUserHeaderWidget extends StatelessWidget {
  VoidCallback onRegisterPressed;
  VoidCallback onLoginPressed;

  NoUserHeaderWidget({
    Key key,
    this.onRegisterPressed,
    this.onLoginPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 20, bottom: 20),
      color: Theme.of(context).brightness == Brightness.light ? Color(0xFFFFdbdb) : Color(0xFF707070),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              _avatarWidget,
              Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Chào bạn!",
                    style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "Vui lòng đăng nhập để \ntrải nghiệm nhiều thông tin hơn",
                    style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 12, fontWeight: FontWeight.normal),
                  ),
                ],
              )
            ],
          ),
          SizedBox(height: 20),
          _buttonsWidget(context),
        ],
      ),
    );
  }

  Widget get _avatarWidget {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 15),
      width: 60,
      height: 60,
      child: Image.asset(AppImages.icAvatarPlaceholder),
    );
  }

  Widget _buttonsWidget(BuildContext context) {
    return Container(
      height: 30,
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Visibility(
            visible: false,
            child: MaterialButton(
              elevation: 0,
              color: Theme.of(context).focusColor.withOpacity(0.2),
              height: 30,
              minWidth: 120,
              onPressed: onRegisterPressed,
              child: Wrap(
                runAlignment: WrapAlignment.center,
                crossAxisAlignment: WrapCrossAlignment.center,
                spacing: 9,
                children: [
                  // Icon(Icons.person_add_outlined, color: Theme.of(context).hintColor, size: 24),
                  Text(
                    S.of(context).register,
                    style: Theme.of(context).textTheme.subtitle2.merge(TextStyle(color: Theme.of(context).hintColor)),
                  ),
                ],
              ),
              shape: StadiumBorder(),
            ),
          ),
          SizedBox(width: 10),
          MaterialButton(
            elevation: 0,
            onPressed: onLoginPressed,
            color: Theme.of(context).accentColor,
            height: 30,
            minWidth: 120,
            child: Wrap(
              runAlignment: WrapAlignment.center,
              crossAxisAlignment: WrapCrossAlignment.center,
              spacing: 9,
              children: [
                // Icon(Icons.exit_to_app_outlined, color: Theme.of(context).primaryColor, size: 24),
                Text(
                  S.of(context).login,
                  style: Theme.of(context).textTheme.subtitle2.merge(TextStyle(color: Theme.of(context).primaryColor)),
                ),
              ],
            ),
            shape: StadiumBorder(),
          ),
        ],
      ),
    );
  }
}
