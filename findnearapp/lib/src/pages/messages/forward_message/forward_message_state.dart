import 'package:get/get.dart';

import '../../../models/conversation.dart';
import '../../../models/entities/message_chat/entities/message_chat.dart';
import '../../../models/enums/load_status.dart';

class ForwardMessageState {
  final loadConversionStatus = LoadStatus.initial.obs;
  final conversations = Rx<List<Conversation>>([]);
  final sendConversation = Rx<List<String>>([]);

  MessageChat quotedMessage;
}
