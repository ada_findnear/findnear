import '../../../repository/message_repository.dart';
import 'package:get/get.dart';

import '../../../database/conversations_dao.dart';
import '../../../models/conversation.dart';
import '../../../models/enums/load_status.dart';
import 'forward_message_state.dart';

class ForwardMessageLogic extends GetxController {
  final state = ForwardMessageState();

  final _conversationsDAO = Get.find<ConversationsDAO>();
  final _messageRepository = Get.find<MessageRepository>();

  void forwardMessageToConversation(Conversation conversation) {
    if (state.sendConversation.value.contains(conversation.id)) return;
    
    _messageRepository.forwardMessage(state.quotedMessage, conversation);
    state.sendConversation.value.add(conversation.id);
    state.sendConversation.refresh();
  }

  getConversations() async {
    state.loadConversionStatus.value = LoadStatus.loading;
    state.conversations.value = await _conversationsDAO.getAll()
      ..sort(
        (a, b) => a.lastMessageDateTime.compareTo(b.lastMessageDateTime),
      );
    state.loadConversionStatus.value = LoadStatus.success;
  }
}
