import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/main.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/media.dart';
import 'package:findnear/src/pages/messages/conversation_detail/widgets/app_message_text.dart';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import '../../../../models/conversation.dart' as model;
import '../../../../repository/user_repository.dart';

class ConversationWidget extends StatefulWidget {
  ConversationWidget({
    Key key,
    this.message,
    this.onDismissed,
    this.onSendPress,
    this.didSend = false,
  }) : super(key: key);
  final model.Conversation message;
  final ValueChanged<model.Conversation> onDismissed;
  final VoidCallback onSendPress;
  final bool didSend;

  @override
  _ConversationWidgetState createState() => _ConversationWidgetState();
}

class _ConversationWidgetState extends State<ConversationWidget> {
  String getAvatarUrl() {
    String avatar = this.widget.message.users.firstWhere((element) => element.id != currentUser.value.id, orElse: () => null) != null ? this.widget.message.users.firstWhere((element) => element.id != currentUser.value.id).image.thumb : (new Media()).thumb;
    return avatar;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: this.widget.message.readByUsers.contains(currentUser.value?.id) ? Colors.transparent : Theme.of(context).focusColor.withOpacity(0.05),
      padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
      height: 64,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          _buildAvatarWidget(),
          SizedBox(width: 12),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        this.widget.message.name,
                        overflow: TextOverflow.fade,
                        softWrap: false,
                        style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 14, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: AppMessageText(
                        Helper.of(context).getMessage(this.widget.message.lastMessage),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: Theme.of(context).textTheme.bodyText1.copyWith(
                              fontWeight: this.widget.message.readByUsers.contains(currentUser.value.id) ? FontWeight.w400 : FontWeight.w800,
                              fontSize: 12,
                            ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          widget.didSend
              ? Container(
                  height: 24,
                  width: 56,
                  child: Center(child: Icon(Icons.done_rounded, color: Colors.white)),
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(12),
                  ),
                )
              : GestureDetector(
                  onTap: widget.onSendPress,
                  child: Container(
                    height: 24,
                    width: 56,
                    child: Center(child: Text(S.of(context).send, style: TextStyle(fontSize: 14, color: Colors.white, fontWeight: FontWeight.bold))),
                    decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(12),
                    ),
                  ),
                ),
        ],
      ),
    );
  }

  List<String> getAvatarUrls() {
    String groupAvatar = widget.message.config?.groupAvatar ?? "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
    String user = "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
    if(widget.message.config.shortUsers?.isNotEmpty ?? false)
      user = widget.message.config.shortUsers[0]?.avatar ?? "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
    return [
      groupAvatar,
      user,
    ];
  }

  Widget _buildAvatarWidget() {
    //final isGroupChat = widget.message.idAdmin != null;
    final isGroupChat =
        widget.message.type != null && widget.message.type == "GROUP";
    if (isGroupChat) {
      return Container(
        width: 38,
        height: 38,
        child: Stack(
          children: [
            Positioned(
              top: 0,
              left: 0,
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(15)),
                child: CachedNetworkImage(
                  height: 30,
                  width: 30,
                  fit: BoxFit.cover,
                  imageUrl: (getAvatarUrls()?.length ?? 0) > 0
                      ? getAvatarUrls()[0]
                      : Media().thumb,
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              right: 0,
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(11)),
                child: CachedNetworkImage(
                  height: 22,
                  width: 22,
                  fit: BoxFit.cover,
                  imageUrl: (getAvatarUrls()?.length ?? 0) > 1
                      ? getAvatarUrls()[1]
                      : Media().thumb,
                ),
              ),
            ),
          ],
        ),
      );
    }
    return Stack(
      children: <Widget>[
        SizedBox(
          width: 38,
          height: 38,
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(60)),
            child: CachedNetworkImage(
              height: 38,
              width: double.infinity,
              fit: BoxFit.cover,
              imageUrl: (this.widget.message.config.groupAvatar == null)
                  ? this.getAvatarUrl()
                  : this.widget.message.config.groupAvatar,
              placeholder: (context, url) => Image.asset(
                'assets/img/loading.gif',
                fit: BoxFit.cover,
                width: double.infinity,
                height: 38,
              ),
              errorWidget: (context, url, error) => CachedNetworkImage(
                imageUrl: (new Media()).thumb,
                fit: BoxFit.cover,
                width: double.infinity,
                height: 38,
              ),
            ),
          ),
        ),
        Positioned(
          bottom: 3,
          right: 3,
          width: 12,
          height: 12,
          child: Container(
            decoration: BoxDecoration(shape: BoxShape.circle),
          ),
        )
      ],
    );
  }
}
