import '../../../../generated/l10n.dart';
import '../../../elements/EmptyMessagesWidget.dart';
import '../../../models/conversation.dart';
import '../../../models/enums/load_status.dart';
import '../../../models/entities/message_chat/entities/message_chat.dart';
import '../conversation_list/widgets/messages_loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'forward_message_logic.dart';
import 'forward_message_state.dart';
import 'widgets/conversation_widget.dart';

class ForwardMessagePage extends StatefulWidget {
  final MessageChat forwardMessage;

  ForwardMessagePage({this.forwardMessage});

  @override
  _ForwardMessagePageState createState() => _ForwardMessagePageState();
}

class _ForwardMessagePageState extends State<ForwardMessagePage> {
  final ForwardMessageLogic logic = Get.put(ForwardMessageLogic());
  final ForwardMessageState state = Get.find<ForwardMessageLogic>().state;

  @override
  void initState() {
    super.initState();
    state.quotedMessage = widget.forwardMessage;
    logic.getConversations();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: _buildBodyWidget(),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      centerTitle: true,
      leadingWidth: 52,
      leading: IconButton(
        onPressed: Get.back,
        icon: Icon(
          Icons.close_rounded,
          color: Theme.of(context).colorScheme.secondary,
        ),
      ),
      titleSpacing: 0,
      title: Text(
        S.of(context).forwardMessage,
        overflow: TextOverflow.fade,
        maxLines: 1,
        style: Theme.of(context)
            .textTheme
            .headline6
            .merge(TextStyle(letterSpacing: 1.3)),
      ),
    );
  }

  Widget _buildBodyWidget() {
    return Obx(() {
      if (state.loadConversionStatus == LoadStatus.loading) {
        return MessagesLoadingWidget();
      } else if (state.loadConversionStatus == LoadStatus.success) {
        return _buildConversions();
      } else {
        return Container();
      }
    });
  }

  Widget _buildConversions() {
    if (state.conversations.value.isEmpty) {
      return EmptyMessagesWidget();
    } else {
      return ListView.separated(
        itemCount: state.conversations.value.length,
        separatorBuilder: (context, index) {
          return Divider(
              color: Colors.grey.withOpacity(0.4), indent: 62, height: 0);
        },
        shrinkWrap: true,
        primary: false,
        itemBuilder: (context, index) {
          Conversation _conversation = state.conversations.value[index];
          return Obx(() {
            return ConversationWidget(
              message: _conversation,
              didSend: state.sendConversation.value.contains(_conversation.id),
              onSendPress: () {
                logic.forwardMessageToConversation(_conversation);
              },
            );
          });
        },
      );
    }
  }

  @override
  void dispose() {
    Get.delete<ForwardMessageLogic>();
    super.dispose();
  }
}
