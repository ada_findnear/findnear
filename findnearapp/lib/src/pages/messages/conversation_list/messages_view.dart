import 'dart:async';
import 'dart:developer';

import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/EmptyMessagesWidget.dart';
import 'package:findnear/src/elements/SearchBarNewsfeedWidget.dart';
import 'package:findnear/src/global/global_event.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/entities/communication/response/conversation_entity.dart';
import 'package:findnear/src/models/entities/message_chat/enums/message_chat_type.dart';
import 'package:findnear/src/models/enums/create_pin_step_type.dart';
import 'package:findnear/src/models/enums/edit_pin_step_type.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/enums/message_view_type.dart';
import 'package:findnear/src/models/twilio_target_user.dart';
import 'package:findnear/src/pages/group_add.dart';
import 'package:findnear/src/pages/messages/conversation_list/messages_state.dart';
import 'package:findnear/src/pages/messages/conversation_list/widgets/search_conversation_widget.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:passcode_screen/circle.dart';
import 'package:passcode_screen/keyboard.dart';
import 'package:passcode_screen/passcode_screen.dart';
import 'bottom_sheet/create_conversation_bottom_sheet.dart';
import 'messages_logic.dart';
import 'widgets/conversation_widget.dart';
import 'widgets/messages_loading_widget.dart';

class MessagesPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  MessagesPage({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _MessagesPageState createState() => _MessagesPageState();
}

class _MessagesPageState extends State<MessagesPage>
    with AutomaticKeepAliveClientMixin, TickerProviderStateMixin {
  final MessagesLogic logic = Get.put(MessagesLogic());
  final MessagesState state = Get
      .find<MessagesLogic>()
      .state;

  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  AnimationController _animController;
  Animation<double> _animation;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      logic.listenForConversations();
      await logic.getConversationsInCache();
      setState(() {});
    });

    _animController = AnimationController(
      duration: const Duration(milliseconds: 2000),
      vsync: this,
    )
      ..repeat(reverse: true);
    _animation = TweenSequence(
      <TweenSequenceItem<double>>[
        TweenSequenceItem<double>(
          tween: Tween<double>(begin: 0.8, end: 1.1),
          weight: 23.5,
        ),
        TweenSequenceItem<double>(
          tween: ConstantTween<double>(1.1),
          weight: 6.0,
        ),
        TweenSequenceItem<double>(
          tween: Tween<double>(begin: 1.1, end: 1.0),
          weight: 23.5,
        ),
        TweenSequenceItem<double>(
          tween: ConstantTween<double>(1.0),
          weight: 47.0,
        ),
      ],
    ).animate(_animController);
    GlobalEvent.instance.onCreateConversationResponse.stream.listen((data) {
      debugPrint("quanth: onCreateConversationResponse= ${data}");
      logic.createNewConversation(data.toConversation);
    });
    GlobalEvent.instance.onUpdateConversationResponse.stream.listen((data) {
      debugPrint("quanth: onCreateConversationResponse= ${data}");
      logic.updateMemberConversation(data);
    });
    GlobalEvent.instance.onLeaveConversationResponse.stream.listen((data) {
      debugPrint("quanth: onLeaveConversationResponse= ${data}");
      logic.deleteConversationWithoutAPICall(data.toConversation.uuid);
    });
    GlobalEvent.instance.onDeleteConversationResponse.stream.listen((data) {
      debugPrint("quanth: onDeleteConversationResponse= ${data}");
      logic.deleteConversationWithoutAPICall(data.toConversation.uuid);
    });
    GlobalEvent.instance.onPinnedConversationFromSetting.stream.listen((data) {
      debugPrint("quanth: onPinned= ${data}");
      logic.pinConversation(data);
    });
    GlobalEvent.instance.onUnpinnedConversationFromSetting.stream
        .listen((data) {
      debugPrint("quanth: onUnpinned= ${data}");
      logic.unPinConversation(data);
    });
    GlobalEvent.instance.onVisibleConversationFromSetting.stream
        .listen((data) {
      debugPrint("quanth: onVisible= ${data}");
      logic.visibleConversation(data);
    });
    GlobalEvent.instance.onHideConversationFromSetting.stream
        .listen((data) {
      debugPrint("quanth: onHide= ${data}");
      logic.hideConversation(data);
    });
    GlobalEvent.instance.onSelectAllConversation.stream
        .listen((data) {
      debugPrint("quanth: onSelectAll= ${data}");
      if(data){
        logic.addAllDeletedConversation();
      } else {
        logic.clearAllDeletedConversation();
      }
    });
    GlobalEvent.instance.onReceiveMessageResponse.stream
        .listen((data) {
      debugPrint("quanth: onReceiveMessageResponse= ${data}");
      if(data.type == MessageChatType.changeBackground)
        logic.updateConversationBackground(data);
      else if(data.type == MessageChatType.inviteMember){
        logic.addMemberToConversation(data);
      } else if(data.type == MessageChatType.leave){
        logic.removeMemberToConversation(data);
      } else
        logic.updateLastMessage(data);
    });
    GlobalEvent.instance.onUpdateUnreadResponse.stream
        .listen((data) {
      logic.updateUnreadMessage(data);
    });
    GlobalEvent.instance.onUpdateAvatarAndNameResponse.stream
        .listen((data) {
      logic.updateAvatarAndname(data);
    });
    GlobalEvent.instance.onUpdateBackgroundResponse.stream
        .listen((data) {
      logic.refreshConversations();
    });
    GlobalEvent.instance.onPinCodeUpdated.stream.listen((data) {
      debugPrint("quanth: onPinCodeUpdated= ${data}");
      logic.updatePinCode(data);
    });
    GlobalEvent.instance.onRemoveMemberResponse.stream.listen((data) {
      debugPrint("quanth: onPinCodeUpdated= ${data}");
      // logic.removeMemberToConversation2(data.toConversation);
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      key: _scaffoldKey,
      appBar: _buildAppBar(),
      body: _buildBodyWidget(),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      backgroundColor: Theme
          .of(context)
          .scaffoldBackgroundColor,
      centerTitle: true,
      leadingWidth: 52,
      leading: IconButton(
          icon: Obx(() {
            if (state.enableCheckboxModeRx.value)
              return Text(
                "Huỷ",
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 12,
                  color: AppColors.black,
                  fontWeight: FontWeight.bold,
                ),
              );
            return AppImage(
              AppImages.icHamburger,
              color: Theme
                  .of(context)
                  .accentColor,
            );
          }),
          onPressed: () {
            if (state.enableCheckboxModeRx.value)
              logic.disableCheckboxMode();
            else
              widget.parentScaffoldKey.currentState.openDrawer();
          }
      ),
      titleSpacing: 0,
      title: Obx(() {
        if(state.enableCheckboxModeRx.value)
          return Container();
        return SearchConversationWidget();
      }),
      actions: [
        Obx(() {
          return Visibility(
            visible: !state.enableCheckboxModeRx.value,
            child: IconButton(
              icon: Icon(
                Icons.add_rounded,
                color: Theme
                    .of(context)
                    .accentColor,
              ),
              onPressed: _showCreateConversationMenu,
            ),
          );
        }),
        Obx(() {
          if(state.updateDeleteConversationStatus == LoadStatus.success)
            return Visibility(
              visible: state.enableCheckboxModeRx.value,
              child: Container(
                width: 100,
                child: IconButton(
                  icon: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Spacer(),
                      Text(
                        "Xoá (${state.deleteConversations.isEmpty ? 0 : state.deleteConversations.length})",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          fontSize: 12,
                          color: AppColors.red,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                  onPressed: () {
                    showCupertinoDialog(
                      context: context,
                      builder: (context) {
                        return CupertinoAlertDialog(
                          content: Text(
                            S.of(context).delete_all_group_content,
                            style: TextStyle(
                              fontFamily: 'Quicksand',
                              fontSize: 18,
                              color: AppColors.black,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          actions: [
                            CupertinoDialogAction(
                              child: Text(
                                S.of(context).cancel_input,
                                style: TextStyle(
                                  fontFamily: 'Quicksand',
                                  fontSize: 16,
                                  color: Colors.blue,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                            CupertinoDialogAction(
                                child: Text(
                                  S.of(context)
                                      .ok,
                                  style: TextStyle(
                                    fontFamily: 'Quicksand',
                                    fontSize: 16,
                                    color: AppColors.red,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                onPressed: () {
                                  logic.deleteManyConversation();
                                  Navigator.of(context).pop();
                                }),
                          ],
                        );
                      },
                    );
                  },
                ),
              ),
            );
          return Container();
        }),
      ],
    );
  }

  Widget _buildBodyWidget() {
    return Obx(() {
      if (state.loadConversionStatus == LoadStatus.loading) {
        return MessagesLoadingWidget();
      } else if (state.loadConversionStatus == LoadStatus.success ||
          state.pinConversationStatus == LoadStatus.success ||
          state.createConversionStatus == LoadStatus.success ||
          state.deleteConversionStatus == LoadStatus.success ||
          state.hideConversionStatus == LoadStatus.success ||
          state.enableCheckboxModeStatus == LoadStatus.success ||
          state.updateDeleteConversationStatus == LoadStatus.success ||
          state.updateLastMessageStatus.value == LoadStatus.success ||
          state.updateUnreadMessageStatus == LoadStatus.success ||
          state.updateAvatarAndNameStatus == LoadStatus.success ||
          state.updatePinCodeStatus == LoadStatus.success ||
          state.leaveConversionStatus == LoadStatus.success
      ) {
        return _buildConversions();
      } else {
        return Container();
      }
    });
  }

  List<Conversation> conversationPresenters = [];

  Widget _buildConversions() {
    if (state.conversations.isNotEmpty) {
      return _buildConversationsWhenHasData(
          state.sortedConversations(), state.sortedPinnedConversations());
    } else if (state.cacheConversations.isNotEmpty) {
      return _buildConversationsWhenHasData(
          state.sortedConversations(), state.sortedPinnedConversations());
    } else {
      return EmptyMessagesWidget();
    }
  }


  Widget _buildConversationsWhenHasData(List<Conversation> conversations,
      List<Conversation> pinnedConversations) {
    return SlidableAutoCloseBehavior(
      child: CustomScrollView(
        slivers: [
          SliverList(
            delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int index) {
                Conversation item = pinnedConversations[index];
                conversationPresenters.add(item);
                return ConversationWidget(
                  type: MessageViewType.PINNED_MESSAGE,
                  message: item,
                  onDismissed: (conversation) {},
                  onLongPress: () {
                    logic.enableCheckboxMode();
                    // _onLongPressConversation(item);
                  },
                  onPinPress: () {
                    if (!state.isPinned(item))
                      logic.pinConversation(item);
                    else
                      logic.unPinConversation(item);
                  },
                  onDeletePress: () {
                    showConfirmDeleteDialog(item);
                  },
                  openActionPanel: (message) {
                    /// event open action pannel
                  },
                  closeActionPanel: (message) {
                    /// event open action pannel
                  },
                  onMorePress: (Conversation message){
                    _onLongPressConversation(message);
                  }
                );
              },
              // Or, uncomment the following line:
              childCount: pinnedConversations.length,
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int index) {
                Conversation item = conversations[index];
                conversationPresenters.add(item);
                return ConversationWidget(
                  type: MessageViewType.NORMAL,
                  message: item,
                  onDismissed: (conversation) {},
                  onLongPress: () {
                    logic.enableCheckboxMode();
                    //_onLongPressConversation(item);
                  },
                  onPinPress: () {
                    if (!state.isPinned(item))
                      logic.pinConversation(item);
                    else
                      logic.unPinConversation(item);
                  },
                  onDeletePress: () {
                    showConfirmDeleteDialog(item);
                  },
                  openActionPanel: (message) {
                    /// event open action pannel
                  },
                  closeActionPanel: (message) {
                    /// event open action pannel
                  },
                  onMorePress: (Conversation message){
                    _onLongPressConversation(message);
                  }
                );
              },
              // Or, uncomment the following line:
              childCount: conversations.length,
            ),
          ),
        ],
      ),
    );
  }

  void showConfirmDeleteDialog(Conversation presenter) {
    showCupertinoDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          content: Text(
            S.of(context).delete_group_title("${presenter.name}"),
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 18,
              color: AppColors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          actions: [
            CupertinoDialogAction(
              child: Text(
                S.of(context).cancel_input,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 16,
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            CupertinoDialogAction(
                child: Text(
                  S
                      .of(context)
                      .ok,
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 16,
                    color: AppColors.red,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                onPressed: () {
                  if (presenter.type == "GROUP") {
                    logic.leaveConversation(presenter.uuid);
                  } else if (presenter.type == "PEER") {
                    logic.deleteConversation(presenter.uuid);
                  }

                  Navigator.of(context).pop();
                }),
          ],
        );
      },
    );
  }

  @override
  void dispose() {
    Get.delete<MessagesLogic>();
    super.dispose();
  }

  void _onLongPressConversation(Conversation conversation) async {
    log(conversation.users.length.toString());
    log(conversation.users.length.toString());
    final mutedUsers = conversation.mutedUsers ?? [];
    final isMuted = mutedUsers.contains(currentUser.value?.id ?? "");
    if ((conversation.idAdmin == (currentUser.value?.id ?? "")) ||
        (conversation.users.length <= 2)) {
      final result = await Get.bottomSheet(Container(
        padding: EdgeInsets.only(top: 10, bottom: Get.bottomBarHeight),
        decoration: BoxDecoration(
            color: Theme
                .of(context)
                .scaffoldBackgroundColor,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(20),
              topLeft: Radius.circular(20),
            )),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ListTile(
              leading: conversation.isDisplay ?
              Image.asset(
                  AppImages.icHideConversation,
                  width: 25,
                  height: 25,
                  color: Theme
                      .of(context)
                      .accentColor) :
              Image.asset(
                  AppImages.icShowConversation,
                  width: 25,
                  height: 25,
                  color: Theme
                      .of(context)
                      .accentColor),
              minLeadingWidth: 0,
              title: Text( conversation.isDisplay? "Ẩn cuộc trò chuyện" : "Hiện cuộc trò chuyện",
                style: Theme
                    .of(context)
                    .textTheme
                    .bodyText1
                    .copyWith(fontWeight: FontWeight.bold),
              ),
              onTap: () {
                Get.back();
                /// bật switch button
                if (conversation.isDisplay) {
                  if (conversation.pin?.isEmpty ?? true)
                    _showCreatePINCodeDialog(conversation);
                  else
                    _goToEditPinCodeScreen(conversation);
                } else
                  _showEnableConversationDialog(conversation);
              },
            ),
            ListTile(
              leading: Icon(
                isMuted
                    ? Icons.notifications_on_outlined
                    : Icons.notifications_off_outlined,
                color: Theme
                    .of(context)
                    .accentColor,
              ),
              minLeadingWidth: 0,
              title: Text(
                isMuted
                    ? S
                    .of(context)
                    .turnOnNotification
                    : S
                    .of(context)
                    .turnOffNotification,
                style: Theme
                    .of(context)
                    .textTheme
                    .bodyText1
                    .copyWith(fontWeight: FontWeight.bold),
              ),
              onTap: () {
                isMuted
                    ? Get.back(result: "on_notification")
                    : Get.back(result: "off_notification");
              },
            ),
          ],
        ),
      ));
      if (result is String && result == "delete") {
        logic.removeConversation(conversationId: conversation.id);
      } else if (result is String && result == "on_notification") {
        logic.turnOnNotification(conversation);
      } else if (result is String && result == "off_notification") {
        logic.turnOffNotification(conversation);
      }
    }
  }

  void _onConversationCreated(ConversationEntity conversation) {
    logic.createNewConversation(conversation.toConversation);
  }

  // ChatItemBottomSheet
  void _showCreateConversationMenu() {
    CreateConversationBottomSheet.showCreateConversationMenu(
      onAddFriendPressed: () {
        Fluttertoast.showToast(msg: "Add Friend", toastLength: Toast.LENGTH_LONG);
      },
      onAddGroupPressed: () {
        NavigatorUtils.navigateToCreateConversationWidget(members: [], callback: _onConversationCreated);
        // Get.to(() => GroupAddWidget(onConversationCreated: _onConversationCreated));
      },
      onAddGroupCallPressed: () {
        Fluttertoast.showToast(msg: "Add Group Call", toastLength: Toast.LENGTH_LONG);
      },
    );
  }

  final StreamController<bool> _verificationNotifier =
    StreamController<bool>.broadcast();
  final StreamController<bool> _verificationNotifierCreate =
    StreamController<bool>.broadcast();
  EditPINStepType _editStepType = EditPINStepType.NONE;

  void _showCreatePINCodeDialog(Conversation conversation) {
    showCupertinoDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text(
            S.of(context).create_pin_code_title,
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 18,
              color: AppColors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          content: Text(
            S
                .of(context)
                .create_pin_code_content,
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 14,
              color: AppColors.black,
              fontWeight: FontWeight.normal,
            ),
          ),
          actions: [
            CupertinoDialogAction(
              child: Text(
                S
                    .of(context)
                    .cancel_input,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 16,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            CupertinoDialogAction(
                child: Text(
                  S
                      .of(context)
                      .create_pin_code,
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 16,
                    color: AppColors.red,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                  _goToCreatePinCodeScreen(conversation: conversation);
                }),
          ],
        );
      },
    );
  }

  void _goToCreatePinCodeScreen({
    Conversation conversation,
    String oldPinCode = "",
    CreatePINStepType stepType}) {
    if(stepType == null) stepType = CreatePINStepType.INPUT_NEW_PIN_CODE;
    Navigator.push(
      context,
      PageRouteBuilder(
          opaque: false,
          pageBuilder: (context, animation, secondaryAnimation) {
            return PasscodeScreen(
              title: Text(
                stepType.title,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: 'Quicksand',
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
              isValidCallback: (String enteredPasscode) async {
                switch (stepType) {
                  case CreatePINStepType.INPUT_NEW_PIN_CODE:
                    _goToCreatePinCodeScreen(
                        conversation: conversation,
                        oldPinCode: enteredPasscode,
                        stepType: CreatePINStepType.REINPUT_NEW_PIN_CODE);
                    break;
                  case CreatePINStepType.REINPUT_NEW_PIN_CODE:
                    bool success = await logic.createPINCode(
                        enteredPasscode, conversation.uuid, conversation);
                    if (success) {
                      conversation.pin = enteredPasscode;
                      GlobalEvent.instance.onHideConversationFromSetting
                          .add(conversation);
                      Fluttertoast.showToast(
                          msg: "Ẩn trò chuyện thành công",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.CENTER,
                          backgroundColor: AppColors.black,
                          textColor: AppColors.white,
                          timeInSecForIosWeb: 1);
                    } else
                      Fluttertoast.showToast(
                          msg: "Ẩn trò chuyện thất bại!",
                          toastLength: Toast.LENGTH_SHORT,
                          backgroundColor: AppColors.black,
                          textColor: AppColors.white,
                          gravity: ToastGravity.CENTER,
                          timeInSecForIosWeb: 1);
                    break;
                }
              },
              passwordEnteredCallback: (String enteredPasscode) async {
                bool isValid = oldPinCode.isEmpty || (oldPinCode.isNotEmpty && enteredPasscode == oldPinCode);
                _verificationNotifierCreate.add(isValid);
                if (!isValid)
                  Fluttertoast.showToast(
                      msg: "Mã PIN không đúng, vui lòng thử lại.",
                      toastLength: Toast.LENGTH_SHORT,
                      backgroundColor: AppColors.black,
                      textColor: AppColors.white,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIosWeb: 1);
              },
              cancelButton: Text(
                S
                    .of(context)
                    .cancel_input,
                style: const TextStyle(fontSize: 16, color: Colors.black),
                semanticsLabel: S
                    .of(context)
                    .cancel_input,
              ),
              deleteButton: Image.asset(
                AppImages.icDeletePinDigit,
                width: 40,
                height: 30,
              ),
              circleUIConfig: CircleUIConfig(
                  borderColor: AppColors.red,
                  fillColor: AppColors.red,
                  borderWidth: 2,
                  circleSize: 15),
              keyboardUIConfig: KeyboardUIConfig(
                digitBorderWidth: 2,
                primaryColor: Colors.white,
                digitFillColor: Colors.white,
                digitTextStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 28,
                    fontWeight: FontWeight.normal),
              ),
              shouldTriggerVerification: _verificationNotifierCreate.stream,
              backgroundColor: AppColors.greyBg,
              cancelCallback: _onCreatePasscodeCancelled,
              passwordDigits: 4,
              bottomWidget: Container(
                margin: EdgeInsets.symmetric(horizontal: 50),
                child: Text(
                  stepType.content,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                      fontFamily: 'Quicksand',
                      fontSize: 13,
                      color: Colors.black,
                      fontWeight: FontWeight.normal),
                ),
              ),
            );
          }),
    );
  }

  void _onCreatePasscodeCancelled() {
    Navigator.maybePop(context);
  }

  void _goToEditPinCodeScreen(Conversation conversation) {
    Navigator.push(
      context,
      PageRouteBuilder(
          opaque: false,
          pageBuilder: (context, animation, secondaryAnimation) {
            return PasscodeScreen(
              title: Text(
                S.of(context)
                    .des_pin_code_1,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: 'Quicksand', color: Colors.black, fontSize: 14),
              ),
              isValidCallback: (String enteredPasscode) async {
                /// bật switch button
                bool success = await logic.hideConversation2(conversation);
                if (success) {
                  GlobalEvent.instance.onHideConversationFromSetting
                      .add(conversation);
                  Fluttertoast.showToast(
                      msg: "Ẩn trò chuyện thành công",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.CENTER,
                      backgroundColor: AppColors.black,
                      textColor: AppColors.white,
                      timeInSecForIosWeb: 1);
                } else
                  Fluttertoast.showToast(
                      msg: "Ẩn cuộc trò chuyện thất bại!",
                      toastLength: Toast.LENGTH_SHORT,
                      backgroundColor: AppColors.black,
                      textColor: AppColors.white,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIosWeb: 1);
              },
              passwordEnteredCallback: (String enteredPasscode) async {
                bool isValid = enteredPasscode == conversation.pin;
                _verificationNotifier.add(isValid);
                if (!isValid)
                  Fluttertoast.showToast(
                      msg: "Mã PIN không đúng, vui lòng thử lại.",
                      toastLength: Toast.LENGTH_SHORT,
                      backgroundColor: AppColors.black,
                      textColor: AppColors.white,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIosWeb: 1);
              },
              cancelButton: Text(
                S
                    .of(context)
                    .cancel_input,
                style: const TextStyle(fontSize: 16, color: Colors.black),
                semanticsLabel: S
                    .of(context)
                    .cancel_input,
              ),
              deleteButton: Image.asset(
                AppImages.icDeletePinDigit,
                width: 40,
                height: 30,
              ),
              circleUIConfig: CircleUIConfig(
                  borderColor: AppColors.red,
                  fillColor: AppColors.red,
                  borderWidth: 2,
                  circleSize: 15),
              keyboardUIConfig: KeyboardUIConfig(
                digitBorderWidth: 2,
                primaryColor: Colors.white,
                digitFillColor: Colors.white,
                digitTextStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 28,
                    fontWeight: FontWeight.normal),
              ),
              shouldTriggerVerification: _verificationNotifier.stream,
              backgroundColor: AppColors.greyBg,
              cancelCallback: _onPasscodeCancelled,
              passwordDigits: 4,
              bottomWidget: _buildPasscodeRestoreButton(conversation.uuid),
              headerIcon: _editStepType == EditPINStepType.NONE
                  ? Container(
                margin: EdgeInsets.symmetric(vertical: 20),
                child: Icon(
                  Icons.lock,
                  color: Colors.black,
                  size: 40,
                ),
              )
                  : Container(),
            );
          }),
    );
  }

  void _onPasscodeCancelled() => Navigator.maybePop(context);

  void _showEnableConversationDialog(Conversation conversation) {
    showCupertinoDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          content: Text(
            "Hiện lại trò chuyện của Nhóm ăn chơi?",
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 14,
              color: AppColors.black,
              fontWeight: FontWeight.normal,
            ),
          ),
          actions: [
            CupertinoDialogAction(
              child: Text(
                S
                    .of(context)
                    .cancel_input,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 16,
                  color: AppColors.red,
                  fontWeight: FontWeight.bold,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            CupertinoDialogAction(
                child: Text(
                  "Hiện",
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 16,
                    color: AppColors.black,
                    fontWeight: FontWeight.normal,
                  ),
                ),
                onPressed: () async {
                  /// hiện cuộc trò chuyện
                  bool success = await logic.visibleConversation2(conversation);
                  if (success) {
                    GlobalEvent.instance.onVisibleConversationFromSetting
                        .add(conversation);
                    Navigator.of(context).pop();
                  } else
                    Fluttertoast.showToast(
                        msg: "Không thể hiển thị cuộc trò chuyện!",
                        toastLength: Toast.LENGTH_SHORT,
                        backgroundColor: AppColors.black,
                        textColor: AppColors.white,
                        gravity: ToastGravity.CENTER,
                        timeInSecForIosWeb: 1);
                }),
          ],
        );
      },
    );
  }

  Widget _buildPasscodeRestoreButton(String uuid) =>
      Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                S
                    .of(context)
                    .des_pin_code_2,
                textAlign: TextAlign.center,
                style: const TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 13,
                    color: Colors.black,
                    fontWeight: FontWeight.normal),
              ),
              SizedBox(height: 1),
              Flexible(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Flexible(
                      child: Text(
                        S
                            .of(context)
                            .des_pin_code_3,
                        textAlign: TextAlign.start,
                        style: const TextStyle(
                            fontFamily: 'Quicksand',
                            fontSize: 13,
                            color: Colors.black,
                            fontWeight: FontWeight.normal),
                      ),
                    ),
                    GestureDetector(
                      child: Text(
                        S
                            .of(context)
                            .des_pin_code_4,
                        textAlign: TextAlign.start,
                        style: const TextStyle(
                            fontFamily: 'Quicksand',
                            fontSize: 13,
                            color: AppColors.red,
                            fontWeight: FontWeight.bold),
                      ),
                      onTap:(){
                        _resetAppPassword(uuid);
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );

  void _resetAppPassword(String uuid) => NavigatorUtils.navigateToSettingPinCodeScreen(uuid);
}
