import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/controllers/page_controller.dart';
import 'package:findnear/src/database/conversations_dao.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/global/global_event.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/entities/communication/response/conversation_entity.dart';
import 'package:findnear/src/models/entities/communication/response/delete_conversation_response.dart';
import 'package:findnear/src/models/entities/communication/response/delete_many_conversation_response.dart';
import 'package:findnear/src/models/entities/message_chat/entities/message_chat.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/bottom_sheet/change_bg_logic.dart';
import 'package:findnear/src/repository/chat_repository.dart';
import 'package:findnear/src/repository/communication_repository.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/services/websocket_service.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

import 'messages_state.dart';

class MessagesLogic extends GetxController {
  final state = MessagesState();

  ChatRepository _chatRepository = ChatRepository();
  CommunicationRepository _repository = Get.find<CommunicationRepository>();
  final _conversationsDAO = Get.find<ConversationsDAO>();

  // for cache conversations
  Worker _saveConversationsDebounce;
  var _docConversations = Rx<List<QueryDocumentSnapshot<Object>>>([]);

  @override
  void onInit() {
    super.onInit();
    currentUser?.addListener(_logout);

    // debounce with 1.5s to save conversations to cache
    _saveConversationsDebounce = debounce(
      _docConversations,
      (_) => _handleUpdateConversationsToCache(),
      time: Duration(milliseconds: 1500),
    );
  }

  @override
  void onClose() {
    currentUser?.removeListener(_logout);
    _saveConversationsDebounce.dispose();
    super.onClose();
  }

  void getConversationsInCache() async {
    List<Conversation> conversations = await _conversationsDAO.getAll();
    state.cacheConversations = conversations.map((e) => e).toList();
    state.cacheConversations
        .sort((a, b) => b.lastMessageTime.compareTo(a.lastMessageTime));
  }

  listenForConversations() async {
    state.loadConversionStatus.value = LoadStatus.loading;
    /*state.conversations = await state.chatRepository
        .getUserConversations(currentUser.value?.id ?? "");*/
    List<ConversationEntity> results =
        await state.conversationRepository.getConversations();
    print('${results.length}');
    state.conversations = results.map((e) => e.toConversation).toList();
    state.pinnedConversations = results
        .map((e) => e.toConversation)
        .where((element) => element.pinTop == true)
        .toList();

    /// save conversation vào local database
    if (state.conversations.isNotEmpty)
      _conversationsDAO.putAll(state.conversations);
    state.loadConversionStatus.value = LoadStatus.success;
  }

  List<QueryDocumentSnapshot<Object>> orderSnapshotByTime(
    AsyncSnapshot<QuerySnapshot> snapshot,
  ) {
    final docs = snapshot.data.docs;
    docs.sort((QueryDocumentSnapshot a, QueryDocumentSnapshot b) {
      var time1 = a.get('time');
      var time2 = b.get('time');
      return time2.compareTo(time1) as int;
    });
    return docs;
  }

  removeConversation({String conversationId}) async {
    _chatRepository.removeConversation(conversationId).then((value) {
      listenForConversations();
      ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
        content: Text(S.of(Get.context).removeUserSuccessful),
      ));
    });
  }

  void turnOnNotification(Conversation conversation) {
    final userId = currentUser.value?.id;
    if (userId != null) {
      try {
        _chatRepository.turnOnNotification(conversation, userId);
      } catch (e) {}
    }
  }

  void turnOffNotification(Conversation conversation) {
    final userId = currentUser.value?.id;
    if (userId != null) {
      try {
        _chatRepository.turnOffNotification(conversation, userId);
      } catch (e) {}
    }
  }

  void updateConversationsToCache(List<QueryDocumentSnapshot<Object>> value) =>
      _docConversations.value = value;

  void _logout() {
    final _pagesLogic = Get.find<PagesController>();
    if (_pagesLogic == null || _pagesLogic.currentTab.value != 4) return;

    // Navigate to Login Screen when logged out user
    if (state.isLoggedInState != isLoggedIn) {
      state.isLoggedInState = isLoggedIn;
      if (!state.isLoggedInState) {
        NavigatorUtils.navigateToPagesWidget();

        Future.delayed(Duration.zero, () {
          NavigatorUtils.toLogin(isReplace: false);
          AppSnackbar.showInfo(
            title: S.of(Get.context).notifications,
            message: S.of(Get.context).session_expired_login_again,
          );
        });
      }
    }
  }

  void _handleUpdateConversationsToCache() {
    try {
      final conversations = _docConversations.value
          .map((e) => Conversation.fromJSON(e.data()))
          .toList();
      _conversationsDAO.clear();
      _conversationsDAO.putAll(conversations);
    } catch (e) {
      logger.e("MessagesLogic - _handleUpdateConversationsToCache - error: $e");
    }
  }

  /// pin conversation
  void pinConversation(Conversation obj) async {
    obj.pinTop = true;
    state.pinConversationStatus.value = LoadStatus.loading;
    try {
      var response =
          await state.conversationRepository.pinnedConversation(obj.uuid);
      if (response != null) {
        /// save conversation to local
        await _conversationsDAO.put(obj);

        /// update ui
        Conversation a = state.pinnedConversations
            ?.firstWhere((element) => obj.id == element.id, orElse: () => null);
        if (a == null) state.pinnedConversations.add(obj);
        state.pinnedConversations
            .sort((a, b) => b.lastMessageTime.compareTo(a.lastMessageTime));
        state.pinConversationStatus.value = LoadStatus.success;
        Fluttertoast.showToast(
            msg: "Ghim trò chuyện thành công",
            backgroundColor: AppColors.black,
            textColor: AppColors.white,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1);
      }
    } catch (e) {
      state.pinConversationStatus.value = LoadStatus.failure;
    }
  }

  /// unpin conversation
  void unPinConversation(Conversation obj) async {
    obj.pinTop = false;
    state.pinConversationStatus.value = LoadStatus.loading;
    try {
      var response =
          await state.conversationRepository.unPinnedConversation(obj.uuid);
      if (response != null) {
        /// save conversation to local
        await _conversationsDAO.put(obj);

        /// update ui
        Conversation a = state.pinnedConversations
            ?.firstWhere((element) => obj.id == element.id, orElse: () => null);
        if (a != null) state.pinnedConversations.remove(a);
        if(state.pinnedConversations.length > 1)
          state.pinnedConversations
              .sort((a, b) => b.lastMessageTime.compareTo(a.lastMessageTime));
        state.pinConversationStatus.value = LoadStatus.success;
        Fluttertoast.showToast(
            msg: "Bỏ ghim trò chuyện thành công",
            backgroundColor: AppColors.black,
            textColor: AppColors.white,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1);
      }
    } catch (e) {
      state.pinConversationStatus.value = LoadStatus.failure;
    }
  }

  void createNewConversation(Conversation conversation) {
    state.createConversionStatus.value = LoadStatus.loading;

    /// save conversation to local
    _conversationsDAO.put(conversation);

    /// update ui
    if (state.conversations.isEmpty)
      state.conversations.add(conversation);
    else {
      Conversation result = state.conversations.firstWhere(
          (element) => conversation.uuid == element.uuid,
          orElse: () => null);
      if (result == null) state.conversations.add(conversation);
    }
    state.createConversionStatus.value = LoadStatus.success;
  }

  void updateMemberConversation(Conversation obj) async {
    state.createConversionStatus.value = LoadStatus.loading;
    try {
      /// conversation
      int index =
      state.conversations.indexWhere((element) => obj.uuid == element.uuid);
      if (index != -1) {
        state.conversations[index].config.shortUsers = []..addAll(obj.config?.shortUsers ?? []);
        state.conversations[index].name = obj.name;
      }

      /// pinned conversation
      int pinnedIndex = state.pinnedConversations
          .indexWhere((element) => obj.uuid == element.uuid);
      if (pinnedIndex != -1) {
        state.pinnedConversations[index].config.shortUsers = []..addAll(obj.config?.shortUsers ?? []);
        state.conversations[index].name = obj.name;
      }

      await _conversationsDAO.put(obj);
      state.createConversionStatus.value = LoadStatus.success;
    } catch (e){
      state.createConversionStatus.value = LoadStatus.failure;
    }
  }

  void deleteConversationWithoutAPICall(String uuid) {
    state.deleteConversionStatus.value = LoadStatus.loading;

    /// delete conversation in local
    _conversationsDAO.delete(uuid);

    /// conversation
    Conversation deleteItem = null;
    if (state.conversations.isNotEmpty) {
      deleteItem = state.conversations
          .firstWhere((element) => uuid == element.uuid, orElse: () => null);
      if (deleteItem != null) state.conversations.remove(deleteItem);
    }

    /// pinned conversation
    if (state.pinnedConversations.isNotEmpty) {
      deleteItem = state.pinnedConversations
          .firstWhere((element) => uuid == element.uuid, orElse: () => null);
      if (deleteItem != null) state.pinnedConversations.remove(deleteItem);
    }
    state.deleteConversionStatus.value = LoadStatus.success;
  }

  void deleteConversation(String uuid) async {
    print('quanth: uuid= $uuid');
    state.deleteConversionStatus.value = LoadStatus.loading;

    /// delete conversation in local
    _conversationsDAO.delete(uuid);

    /// conversation
    Conversation deleteItem = null;
    if (state.conversations.isNotEmpty) {
      deleteItem = state.conversations
          .firstWhere((element) => uuid == element.uuid, orElse: () => null);
      if (deleteItem != null) state.conversations.remove(deleteItem);
    }

    /// pinned conversation
    if (state.pinnedConversations.isNotEmpty) {
      deleteItem = state.pinnedConversations
          .firstWhere((element) => uuid == element.uuid, orElse: () => null);
      if (deleteItem != null) state.pinnedConversations.remove(deleteItem);
    }
    state.deleteConversionStatus.value = LoadStatus.success;

    DeleteConversationResponse result =
        await state.conversationRepository.deleteConversation(uuid);
    if (result != null)
      AppSnackbar.showSuccess(
          title: "Thông báo", message: "Xoá cuộc hội thoại thành công");
    else
      AppSnackbar.showError(
          title: "Thông báo", message: "Xoá cuộc hội thoại thất bại");
  }

  void leaveConversation(String uuid) async {
    print('quanth: uuid= $uuid');
    state.deleteConversionStatus.value = LoadStatus.loading;

    /// delete conversation in local
    _conversationsDAO.delete(uuid);

    /// update ui
    Conversation deleteItem = null;

    /// conversation
    if (state.conversations.isNotEmpty) {
      deleteItem = state.conversations
          .firstWhere((element) => uuid == element.uuid, orElse: () => null);
      if (deleteItem != null) state.conversations.remove(deleteItem);
    }

    /// pinned conversation
    if (state.pinnedConversations.isNotEmpty) {
      deleteItem = state.pinnedConversations
          .firstWhere((element) => uuid == element.uuid, orElse: () => null);
      if (deleteItem != null) state.pinnedConversations.remove(deleteItem);
    }
    state.deleteConversionStatus.value = LoadStatus.success;

    DeleteConversationResponse result =
        await state.conversationRepository.leaveConversation(uuid);
    if (result != null)
      AppSnackbar.showSuccess(
          title: "Thông báo", message: "Rời cuộc hội thoại thành công");
    else
      AppSnackbar.showError(
          title: "Thông báo", message: "Rời cuộc hội thoại thất bại");
  }

  void deleteManyConversation() async {
    state.deleteConversionStatus.value = LoadStatus.loading;

    /// save ids list
    List<String> ids = [];
    if(state.deleteConversations.isNotEmpty){
      state.deleteConversations.forEach((element) {
        ids.add(element.uuid);
      });
    }

    /// delete conversation in local
    _conversationsDAO.deleteFromList(state.deleteConversations);

    state.deleteConversations.forEach((conversation) {
      /// conversation
      Conversation deleteItem = null;
      if (state.conversations.isNotEmpty) {
        deleteItem = state.conversations
            .firstWhere((element) => conversation.uuid == element.uuid, orElse: () => null);
        if (deleteItem != null) state.conversations.remove(deleteItem);
      }

      /// pinned conversation
      if (state.pinnedConversations.isNotEmpty) {
        deleteItem = state.pinnedConversations
            .firstWhere((element) => conversation.uuid == element.uuid, orElse: () => null);
        if (deleteItem != null) state.pinnedConversations.remove(deleteItem);
      }
    });

    state.deleteConversionStatus.value = LoadStatus.success;
    disableCheckboxMode();

    DeleteManyConversationResponse result =
      await state.conversationRepository.deleteManyConversation(ids);
    if (result != null)
      AppSnackbar.showSuccess(
          title: "Thông báo", message: "Xoá nhiều cuộc hội thoại thành công");
    else
      AppSnackbar.showError(
          title: "Thông báo", message: "Xoá nhiều cuộc hội thoại thất bại");
  }

  /// unpin conversation
  void visibleConversation(Conversation obj) async {
    obj.isDisplay = true;
    state.hideConversionStatus.value = LoadStatus.loading;
    try {
      /// conversation
      int index =
          state.conversations.indexWhere((element) => obj.uuid == element.uuid);
      if (index == -1)
        state.conversations.add(obj);
      else
        state.conversations[index].isDisplay = true;

      await _conversationsDAO.put(obj);
      state.hideConversionStatus.value = LoadStatus.success;
    } catch (e) {
      state.hideConversionStatus.value = LoadStatus.failure;
    }
  }

  /// unpin conversation
  void hideConversation(Conversation obj) async {
    obj.isDisplay = false;
    state.hideConversionStatus.value = LoadStatus.loading;
    try {
      /// conversation
      int index =
          state.conversations.indexWhere((element) => obj.uuid == element.uuid);
      if (index != -1) {
        state.conversations[index].isDisplay = false;
        state.conversations[index].pinTop = false;
        state.conversations[index].pin = obj.pin;
      }

      /// pinned conversation
      int pinnedIndex = state.pinnedConversations
          .indexWhere((element) => obj.uuid == element.uuid);
      if (pinnedIndex != -1) {
        state.pinnedConversations.removeAt(pinnedIndex);
      }

      await _conversationsDAO.put(obj);
      state.hideConversionStatus.value = LoadStatus.success;
    } catch (e) {
      state.hideConversionStatus.value = LoadStatus.failure;
    }
  }

  void enableCheckboxMode() {
    try {
      state.enableCheckboxModeStatus.value = LoadStatus.loading;
      state.enableCheckboxModeRx.value = true;
      GlobalEvent.instance.onEnableCheckboxMode.add(true);
      state.enableCheckboxModeStatus.value = LoadStatus.success;
    } catch (e) {
      state.enableCheckboxModeStatus.value = LoadStatus.failure;
    }
  }

  void disableCheckboxMode() {
    try {
      state.enableCheckboxModeStatus.value = LoadStatus.loading;
      state.enableCheckboxModeRx.value = false;
      clearAllDeletedConversation();
      GlobalEvent.instance.onEnableCheckboxMode.add(false);
      state.enableCheckboxModeStatus.value = LoadStatus.success;
    } catch (e) {
      state.enableCheckboxModeStatus.value = LoadStatus.failure;
    }
  }

  void pushToDeletedConversation(Conversation conversation) {
    try {
      state.updateDeleteConversationStatus.value = LoadStatus.loading;
      int index = state.deleteConversations
          .indexWhere((element) => conversation.uuid == element.uuid);
      if (index == -1) {
        state.deleteConversations.add(conversation);
      }
      state.updateDeleteConversationStatus.value = LoadStatus.success;
    } catch (e) {
      state.updateDeleteConversationStatus.value = LoadStatus.failure;
    }
  }

  void removeFromDeletedConversation(Conversation conversation) {
    try {
      state.updateDeleteConversationStatus.value = LoadStatus.loading;
      int index = state.deleteConversations
          .indexWhere((element) => conversation.uuid == element.uuid);
      if (index != -1) {
        state.deleteConversations.removeAt(index);
      }
      state.updateDeleteConversationStatus.value = LoadStatus.success;
    } catch (e) {
      state.updateDeleteConversationStatus.value = LoadStatus.failure;
    }
  }

  void clearAllDeletedConversation() {
    try {
      state.updateDeleteConversationStatus.value = LoadStatus.loading;
      state.deleteConversations.clear();
      state.updateDeleteConversationStatus.value = LoadStatus.success;
    } catch (e) {
      state.updateDeleteConversationStatus.value = LoadStatus.failure;
    }
  }

  void addAllDeletedConversation() {
    try {
      state.updateDeleteConversationStatus.value = LoadStatus.loading;
      if (state.conversations.isNotEmpty)
        state.conversations.forEach((conversation) {
          int index = state.deleteConversations
              .indexWhere((element) => conversation.uuid == element.uuid);
          if (index == -1) {
            state.deleteConversations.add(conversation);
          }
        });
      state.updateDeleteConversationStatus.value = LoadStatus.success;
    } catch (e) {
      state.updateDeleteConversationStatus.value = LoadStatus.failure;
    }
  }

  void updateLastMessage(MessageChat message) async{
    state.updateLastMessageStatus.value = LoadStatus.loading;

    String uuid = message.conversationId;

    if (state.conversations.isNotEmpty) {
      int index =
      state.conversations.indexWhere((element) => uuid == element.originId);
      if (index != -1) {
        state.conversations[index].lastMessageEntity = message;
        if(state.conversations[index].totalMessageUnRead == null)
          state.conversations[index].totalMessageUnRead = 1;
        else
          state.conversations[index].totalMessageUnRead++;
        state.sortedConversations();
        await _conversationsDAO.put(state.conversations[index]);
      }
    }

    /// pinned conversation
    if (state.pinnedConversations.isNotEmpty) {
      int index =
      state.pinnedConversations.indexWhere((element) => uuid == element.originId);
      if (index != -1) {
        state.pinnedConversations[index].lastMessageEntity = message;
        if(state.pinnedConversations[index].totalMessageUnRead == null)
          state.pinnedConversations[index].totalMessageUnRead = 1;
        else
          state.pinnedConversations[index].totalMessageUnRead++;
        await _conversationsDAO.put(state.pinnedConversations[index]);
      }
    }
    state.updateLastMessageStatus.value = LoadStatus.success;
  }

  void updateConversationBackground(MessageChat message) async{
    state.updateLastMessageStatus.value = LoadStatus.loading;

    String uuid = message.conversationId;
    String groupBackground = message.config?.data?.background?.groupBackground ?? ChangeBgController.DEFAULT_BACKGROUND;
    String privateBackground = message.config?.data?.background?.privateBackground ?? ChangeBgController.DEFAULT_BACKGROUND;

    if (state.conversations.isNotEmpty) {
      Conversation conversation = state.conversations.firstWhere((element) => uuid == element.originId, orElse: () => null);
      if(conversation != null) {
        conversation.config.groupBackground = groupBackground;
        conversation.config.privateBackground = privateBackground;
        conversation.lastMessageEntity = message;
        if(conversation.totalMessageUnRead == null)
          conversation.totalMessageUnRead = 1;
        else
          conversation.totalMessageUnRead++;
        state.sortedConversations();
        await _conversationsDAO.put(conversation);
      }
    }

    if (state.pinnedConversations.isNotEmpty) {
      /// pinned conversation
      Conversation pinnedConversation = state.pinnedConversations.firstWhere((element) => uuid == element.originId, orElse: () => null);
      if(pinnedConversation != null) {
        pinnedConversation.config.groupBackground = groupBackground;
        pinnedConversation.config.privateBackground = privateBackground;
        pinnedConversation.lastMessageEntity = message;
        pinnedConversation.totalMessageUnRead++;
        if(pinnedConversation.totalMessageUnRead == null)
          pinnedConversation.totalMessageUnRead = 1;
        else
          pinnedConversation.totalMessageUnRead++;
        await _conversationsDAO.put(pinnedConversation);
      }
    }

    state.updateLastMessageStatus.value = LoadStatus.success;
  }

  void updateUnreadMessage(Conversation conversation)async{
    state.updateUnreadMessageStatus.value = LoadStatus.loading;
    state.conversationRepository.updateUnreadMessage(conversationId: conversation.uuid, numberRead: conversation.totalMessageUnRead);

    String uuid = conversation.uuid;

    if (state.conversations.isNotEmpty) {
      int index =
      state.conversations.indexWhere((element) => uuid == element.uuid);
      if (index != -1) {
        state.conversations[index].totalMessageUnRead = 0;
        await _conversationsDAO.put(state.conversations[index]);
      }
    }

    /// pinned conversation
    if (state.pinnedConversations.isNotEmpty) {
      int index =
      state.pinnedConversations.indexWhere((element) => uuid == element.uuid);
      if (index != -1) {
        state.pinnedConversations[index].totalMessageUnRead = 0;
        await _conversationsDAO.put(state.pinnedConversations[index]);
      }
    }
    state.updateUnreadMessageStatus.value = LoadStatus.success;
  }

  void updateAvatarAndname(Conversation conversation)async{
    state.updateAvatarAndNameStatus.value = LoadStatus.loading;

    String uuid = conversation.uuid;

    if (state.conversations.isNotEmpty) {
      int index =
      state.conversations.indexWhere((element) => uuid == element.uuid);
      if (index != -1) {
        state.conversations[index].config.groupAvatar = conversation.config.groupAvatar;
        state.conversations[index].name = conversation.name;
        await _conversationsDAO.put(state.conversations[index]);
      }
    }

    /// pinned conversation
    if (state.pinnedConversations.isNotEmpty) {
      int index =
      state.pinnedConversations.indexWhere((element) => uuid == element.uuid);
      if (index != -1) {
        state.pinnedConversations[index].config.groupAvatar = conversation.config.groupAvatar;
        state.pinnedConversations[index].name = conversation.name;
        await _conversationsDAO.put(state.pinnedConversations[index]);
      }
    }
    state.updateAvatarAndNameStatus.value = LoadStatus.success;
  }

  void updatePinCode(Conversation conversation) async{
    state.updatePinCodeStatus.value = LoadStatus.loading;

    String pinCode = conversation.pin;

    if (state.conversations.isNotEmpty) {
      int index =
      state.conversations.indexWhere((element) => conversation.uuid == element.uuid);
      if (index != -1) {
        state.conversations[index].pin = pinCode;
        await _conversationsDAO.put(state.conversations[index]);
      }
    }

    /// pinned conversation
    if (state.pinnedConversations.isNotEmpty) {
      int index =
      state.pinnedConversations.indexWhere((element) => conversation.uuid == element.uuid);
      if (index != -1) {
        state.pinnedConversations[index].pin = pinCode;
        await _conversationsDAO.put(state.pinnedConversations[index]);
      }
    }
    state.updatePinCodeStatus.value = LoadStatus.success;
  }

  refreshConversations() async {
    state.loadConversionStatus.value = LoadStatus.loading;
    List<Conversation> results = await _conversationsDAO.getAll();
    state.conversations.clear();
    state.conversations.addAll(results);
    state.pinnedConversations.clear();
    state.pinnedConversations = results
        .map((e) => e)
        .where((element) => element.pinTop == true)
        .toList();
    state.loadConversionStatus.value = LoadStatus.success;
  }

  Future<bool> createPINCode(String pinCode, String uuid, Conversation input) async{
    state.hideConversionStatus.value = LoadStatus.loading;
    try {
      /// Tạo mã pin và ẩn luôn cuộc trò chuyện
      Conversation conversation = input.copyWith(pin: pinCode, isDisplay: false);
      var response = await _repository.updateConversation(conversation); // _repository.createPinCode(uuid: uuid, pinCode: pinCode)
      if(response != null){
        conversation.pin = pinCode;
        conversation.isDisplay = false;
        conversation.pinTop = false;
        _conversationsDAO.put(conversation);
        state.hideConversionStatus.value = LoadStatus.success;
        return true;
      }
      return false;
    } catch (e){
      state.hideConversionStatus.value = LoadStatus.failure;
      return false;
    }
  }

  Future<bool> visibleConversation2(Conversation input) async{
    state.hideConversionStatus.value = LoadStatus.loading;
    try {
      /// Tạo mã pin và ẩn luôn cuộc trò chuyện
      int index = state.conversations.indexWhere((element) => input.uuid == element.uuid);
      if(index != -1)
        state.conversations[index].isDisplay = true;

      /// pinned conversation
      int pinnedIndex = state.pinnedConversations.indexWhere((element) => input.uuid == element.uuid);
      if(pinnedIndex != -1)
        state.pinnedConversations[index].isDisplay = true;

      state.hideConversionStatus.value = LoadStatus.success;
      return true;
    } catch (e){
      state.hideConversionStatus.value = LoadStatus.failure;
      return false;
    }
  }

  Future<bool> hideConversation2(Conversation input) async{
    state.hideConversionStatus.value = LoadStatus.loading;
    try {
      /// conversation
      int index = state.conversations.indexWhere((element) => input.uuid == element.uuid);
      if(index != -1)
        state.conversations[index].isDisplay = false;

      /// pinned conversation
      int pinnedIndex = state.pinnedConversations.indexWhere((element) => input.uuid == element.uuid);
      if(pinnedIndex != -1)
        state.pinnedConversations[index].isDisplay = false;

      state.hideConversionStatus.value = LoadStatus.success;
      return true;
    } catch (e){
      state.hideConversionStatus.value = LoadStatus.failure;
      return false;
    }
  }

  void addMemberToConversation(MessageChat data) {
    Conversation conversation = data.config.data.messageInvite.conversation;
    if(state.conversations.isEmpty)
      createNewConversationWithLastMess(data);
    else {
      int index = state.conversations.indexWhere((element) => element.originId == conversation.originId);
      if(index == -1)
        createNewConversationWithLastMess(data);
      else
        updateLastMessage(data);
    }
  }

  void createNewConversationWithLastMess(MessageChat data) {
    state.createConversionStatus.value = LoadStatus.loading;

    Conversation conversation = data.config.data.messageInvite.conversation;
    conversation.lastMessageEntity = data;

    /// save conversation to local
    _conversationsDAO.put(conversation);

    /// update ui
    if (state.conversations.isEmpty)
      state.conversations.add(conversation);
    else {
      Conversation result = state.conversations.firstWhere(
              (element) => conversation.originId == element.originId,
          orElse: () => null);
      if (result == null) state.conversations.add(conversation);
    }
    state.createConversionStatus.value = LoadStatus.success;
  }

  void removeMemberToConversation(MessageChat message) {
    state.leaveConversionStatus.value = LoadStatus.loading;
    try {
      /// lấy ra thông tin conversation và member đã remove
      Conversation conversation = message.config.data.messageLeave.conversation;
      ShortUserEntity removeMember = message.config.data.messageLeave.shortUser;

      /// nếu remove từ hội thoại thì sẽ cập nhật lại member của hội thoại
      if(state.conversations.isNotEmpty) {
        /// tìm vị trí của hội thoại cần cập nhật lại member
        int index = state.conversations.indexWhere((element) => element.originId == conversation.originId);
        /// tìm được thì thực hiện việc cập nhật
        if(index != -1) {
          /// lấy ra danh sách member trong hội thoại nhóm
          List<ShortUserEntity> shortUsers = state.conversations[index].config.shortUsers ?? [];
          /// tìm vị trí của member cần remove ra
          int removedIndex = shortUsers.indexWhere((element) => element.code == removeMember.code);
          /// tìm đượt thì tiến hành cập nhật lại member cho hội thoại đó
          if(removedIndex != -1 && state.conversations[index].config.shortUsers.isNotEmpty) {
            state.conversations[index].config.shortUsers.removeAt(removedIndex);
            state.conversations[index].lastMessageEntity = message;
            if(state.conversations[index].totalMessageUnRead == null)
              state.conversations[index].totalMessageUnRead = 1;
            else
              state.conversations[index].totalMessageUnRead++;
            _conversationsDAO.put(state.conversations[index]);
          }
        }
      }

      /// nếu remove từ hội thoại đã ghim  thì sẽ cập nhật lại member của hội thoại đã ghim
      if(state.pinnedConversations.isNotEmpty) {
        /// tìm vị trí của hội thoại đã ghim cần cập nhật lại member
        int pinnedIndex = state.pinnedConversations.indexWhere((element) => element.originId == conversation.originId);
        /// tìm được thì thực hiện việc cập nhật
        if(pinnedIndex != -1) {
          /// lấy ra danh sách member trong hội thoại đã ghim
          List<ShortUserEntity> shortUsers = state.pinnedConversations[pinnedIndex].config.shortUsers ?? [];
          /// tìm vị trí của member cần remove ra
          int removedIndex = shortUsers.indexWhere((element) => element.code == removeMember.code);
          /// tìm đượt thì tiến hành cập nhật lại member cho hội thoại đã ghim đó
          if(removedIndex != -1 && state.pinnedConversations[pinnedIndex].config.shortUsers.isNotEmpty) {
            state.pinnedConversations[pinnedIndex].config.shortUsers.removeAt(removedIndex);
            state.pinnedConversations[pinnedIndex].lastMessageEntity = message;
            if(state.pinnedConversations[pinnedIndex].totalMessageUnRead == null)
              state.pinnedConversations[pinnedIndex].totalMessageUnRead = 1;
            else
              state.pinnedConversations[pinnedIndex].totalMessageUnRead++;
            state.sortedPinnedConversations();
            _conversationsDAO.put(state.pinnedConversations[pinnedIndex]);
          }
        }
      }
      state.leaveConversionStatus.value = LoadStatus.success;
    } catch(e) {
      state.leaveConversionStatus.value = LoadStatus.failure;
    }
  }

  void removeMemberToConversation2(Conversation conversation) {
    state.leaveConversionStatus.value = LoadStatus.loading;
    try {
      /// nếu remove từ hội thoại thì sẽ cập nhật lại member của hội thoại
      if(state.conversations.isNotEmpty) {
        /// tìm vị trí của hội thoại cần cập nhật lại member
        int index = state.conversations.indexWhere((element) => element.originId == conversation.originId);
        /// tìm được thì thực hiện việc cập nhật
        if(index != -1) {
          /// tiến hành cập nhật lại member cho hội thoại đó
          state.conversations[index].config = conversation.config;
          state.conversations[index].lastMessageEntity = conversation.lastMessageEntity;
          if(state.conversations[index].totalMessageUnRead == null)
            state.conversations[index].totalMessageUnRead = 1;
          else
            state.conversations[index].totalMessageUnRead++;
          _conversationsDAO.put(state.conversations[index]);
        }
      }

      /// nếu remove từ hội thoại đã ghim  thì sẽ cập nhật lại member của hội thoại đã ghim
      if(state.pinnedConversations.isNotEmpty) {
        /// tìm vị trí của hội thoại đã ghim cần cập nhật lại member
        int pinnedIndex = state.pinnedConversations.indexWhere((element) => element.originId == conversation.originId);
        /// tìm được thì thực hiện việc cập nhật
        if(pinnedIndex != -1) {
          /// tiến hành cập nhật lại member cho hội thoại đã ghim đó
          state.pinnedConversations[pinnedIndex].config = conversation.config;
          state.pinnedConversations[pinnedIndex].lastMessageEntity = conversation.lastMessageEntity;
          if(state.pinnedConversations[pinnedIndex].totalMessageUnRead == null)
            state.pinnedConversations[pinnedIndex].totalMessageUnRead = 1;
          else
            state.pinnedConversations[pinnedIndex].totalMessageUnRead++;
          _conversationsDAO.put(state.pinnedConversations[pinnedIndex]);
        }
      }
      state.leaveConversionStatus.value = LoadStatus.success;
    } catch(e) {
      state.leaveConversionStatus.value = LoadStatus.failure;
    }
  }
}
