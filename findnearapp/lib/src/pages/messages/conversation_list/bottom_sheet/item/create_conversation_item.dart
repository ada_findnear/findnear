import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CreateConversationTile extends StatelessWidget {

  String title;
  String icon;
  VoidCallback onPressed;

  CreateConversationTile({
    this.title,
    this.icon,
    this.onPressed,
  });


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        onPressed.call();
      },
      child: Container(
        padding: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Image.asset(icon, width: 23, height: 23),
            SizedBox(width: 10),
            Text(title, style: TextStyle(
              fontFamily: "Quicksand",
              fontSize: 16,
              fontWeight: FontWeight.normal,
              color: Colors.black,
            ),)
          ],
        ),
      ),
    );
  }
}
