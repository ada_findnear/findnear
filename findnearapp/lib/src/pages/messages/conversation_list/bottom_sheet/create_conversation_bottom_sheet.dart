import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'item/create_conversation_item.dart';

class CreateConversationBottomSheet {
  static void showCreateConversationMenu({
    VoidCallback onAddFriendPressed,
    VoidCallback onAddGroupPressed,
    VoidCallback onAddGroupCallPressed,
  }) async {
    final result = await Get.bottomSheet(Container(
      padding: EdgeInsets.only(top: 10, bottom: Get.bottomBarHeight),
      decoration: BoxDecoration(
          color: Theme.of(Get.context).scaffoldBackgroundColor,
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(20),
            topLeft: Radius.circular(20),
          )),
      child: Column(mainAxisSize: MainAxisSize.min, children: [
        Row(
          children: [
            SizedBox(width: 10),
            Text(
              "Thêm bạn",
              style: TextStyle(
                fontFamily: "Quicksand",
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
            Spacer(),
            InkWell(
              onTap: () {
                Get.back();
              },
              child: AppImage(
                AppImages.icClose,
                width: 29,
              ),
            ),
            SizedBox(width: 10),
          ],
        ),
        SizedBox(height: 10),
        CreateConversationTile(
          icon: AppImages.icGroupAddFriend2,
          title: "Thêm bạn bè mới",
          onPressed: () {
            Get.back(result: "add_friend");
          },
        ),
        Container(margin: EdgeInsets.only(left: 10, right: 10), height: 0.5, color: AppColors.grayLight.withOpacity(0.5)),
        CreateConversationTile(
          icon: AppImages.icGroupAddFriend,
          title: "Tạo nhóm mới",
          onPressed: () {
            Get.back(result: "add_group");
          },
        ),
        Container(margin: EdgeInsets.only(left: 10, right: 10), height: 0.5, color: AppColors.grayLight.withOpacity(0.5)),
        CreateConversationTile(
          icon: AppImages.icGroupCalling,
          title: "Tạo cuộc gọi nhóm",
          onPressed: () {
            Get.back(result: "add_group_call");
          },
        ),
        SizedBox(height: 20),
      ]),
    ));
    if (result is String) {
      if (result == "add_friend") {
        onAddFriendPressed?.call();
      } else if (result == "add_group") {
        onAddGroupPressed?.call();
      } else if (result == "add_group_call") {
        onAddGroupCallPressed?.call();
      }
    }
  }
}
