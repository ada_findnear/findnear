import 'dart:io';

import 'package:findnear/src/commons/app_images.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';

class SearchMemberWidget extends StatelessWidget {
  Function onChanged;
  String avatar;
  String name;
  bool value;

  SearchMemberWidget({
    this.onChanged,
    this.avatar,
    this.name,
    this.value,
  });

  @override
  Widget build(BuildContext context) {
    return _buildBodyWidget();
  }

  Widget _buildBodyWidget() {
    return GestureDetector(
      onTap: (){
        onChanged(!value);
      },
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _buildAvatarWidget(),
              SizedBox(width: 12),
              Expanded(
                child: Text(
                  name,
                  overflow: TextOverflow.fade,
                  softWrap: false,
                  style: Theme.of(Get.context).textTheme.bodyText1.copyWith(
                      fontSize: 14, fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(width: 10),
              GFCheckbox(
                size: 18,
                value: value,
                type: GFCheckboxType.square,
                inactiveBgColor: Colors.transparent,
                inactiveBorderColor: Colors.transparent,
                activeBorderColor: Colors.transparent,
                activeBgColor: Colors.transparent,
                activeIcon: Image.asset(AppImages.icCheckedBox),
                inactiveIcon: Image.asset(AppImages.icUncheckedBox),
                onChanged: onChanged,
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildAvatarWidget() {
    return SizedBox(
      width: 41,
      height: 41,
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(60)),
        child: CircleAvatar(
          foregroundImage: getImage(avatar),
        ),
      ),
    );
  }

  ImageProvider getImage(url) {
    if (url.contains("http")) {
      return NetworkImage(url);
    } else {
      return FileImage(File(url));
    }
  }
}