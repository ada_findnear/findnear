import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/entities/message_chat/entities/message_chat.dart';
import 'package:findnear/src/models/entities/message_chat/enums/message_chat_type.dart';
import 'package:findnear/src/models/enums/message_view_type.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/messages/conversation_detail/conversation_detail_view.dart';
import 'package:findnear/src/pages/messages/conversation_detail/widgets/app_message_text.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';
import 'package:findnear/src/models/media.dart';
import 'package:flutter/material.dart';
import 'package:getwidget/getwidget.dart';
import 'package:global_configuration/global_configuration.dart';
import '../../../../models/conversation.dart' as model;
import '../../../../repository/user_repository.dart';

import '../messages_logic.dart';
import '../messages_state.dart';

class ConversationWidget extends StatefulWidget {
  ConversationWidget({
    Key refKey,
    this.message,
    this.onDismissed,
    this.onLongPress,
    this.onPinPress,
    this.onDeletePress,
    this.openActionPanel,
    this.closeActionPanel,
    this.type,
    this.onMorePress
  }) : super(key: refKey);

  final MessageViewType type;
  final model.Conversation message;
  final ValueChanged<model.Conversation> onDismissed;
  final VoidCallback onLongPress;
  final VoidCallback onPinPress;
  final VoidCallback onDeletePress;
  final Function onMorePress;

  /// catch event open ActionPanel
  final Function openActionPanel;

  /// catch event close Slidable
  final Function closeActionPanel;

  @override
  ConversationWidgetState createState() => ConversationWidgetState();
}

class ConversationWidgetState extends State<ConversationWidget> {
  final MessagesLogic logic = Get.find<MessagesLogic>();
  final MessagesState state = Get.find<MessagesLogic>().state;
  GlobalKey<ActionPaneState> _slidableKey = GlobalKey<ActionPaneState>();

  String getAvatarUrl() {
    String avatar = this.widget.message.users.firstWhere(
                (element) => element.id != currentUser.value?.id,
                orElse: () => null) !=
            null
        ? this
            .widget
            .message
            .users
            .firstWhere((element) => element.id != currentUser.value?.id)
            .image
            .thumb
        : (new Media()).thumb;
    return avatar;
  }

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: widget.type == MessageViewType.NORMAL ? state.isVisible(widget.message) : true,
      child: GestureDetector(
        onTap: () {
          if(state.enableCheckboxModeRx.value){
            if(!state.isCheckedTrue(widget.message))
              logic.pushToDeletedConversation(widget.message);
            else
              logic.removeFromDeletedConversation(widget.message);
          } else {
            NavigatorUtils.navigateToConversationDetailPage(widget.message);
          }

        },
        onLongPress: widget.onLongPress,
        child: Slidable(
          enabled: !state.enableCheckboxModeRx.value,
          // The end action pane is the one at the right or the bottom side.
          endActionPane: ActionPane(
            key: _slidableKey,
            extentRatio: 0.7,
            motion: DrawerMotion(),
            children: [
              SlidableAction(
                onPressed: (context) {
                  widget.onMorePress(widget.message);
                },
                backgroundColor: Color(0xff8F99A5),
                foregroundColor: Colors.white,
                customImage: Image.asset(AppImages.icGroupMore,
                    width: 22, height: 15, color: AppColors.white),
                customLabel: FittedBox(
                  child: Text(
                    "Thêm",
                    style: TextStyle(
                      fontFamily: "Quicksand",
                      color: AppColors.white,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ),
              ),
              SlidableAction(
                onPressed: (context) {
                  widget.onPinPress();
                },
                backgroundColor: Color(0xFF4752BA),
                foregroundColor: Colors.white,
                customImage: Image.asset(
                    state.isPinned(widget.message)
                        ? AppImages.icGroupUnpin
                        : AppImages.icBoxPin,
                    width: 15,
                    height: 15,
                    color: AppColors.white),
                customLabel: FittedBox(
                  child: Text(
                    state.isPinned(widget.message) ? "Bỏ ghim" : "Ghim",
                    style: TextStyle(
                      fontFamily: "Quicksand",
                      color: AppColors.white,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ),
              ),
              SlidableAction(
                onPressed: (context) {
                  widget.onDeletePress();
                },
                backgroundColor: Color(0xffEE4E49),
                foregroundColor: Colors.white,
                customImage: Image.asset(AppImages.icBoxTrash,
                    width: 15, height: 15, color: AppColors.white),
                customLabel: FittedBox(
                  child: Text(
                    "Xoá",
                    style: TextStyle(
                      fontFamily: "Quicksand",
                      color: AppColors.white,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ),
              ),
            ],
            openActionPanel: () {
              //widget.openActionPanel(widget.message);
            },
            closeActionPanel: () {
              //widget.closeActionPanel(widget.message);
            },
          ),

          // The child of the Slidable is what the user sees when the
          // component is not dragged.
          child: _buildBodyWidget(),
        ),
      ),
    );
  }

  Widget _buildBodyWidget() {
    User _user = Get.find<LocalDataManager>().currentUser;
    final isGroupChat =
        widget.message.type != null && widget.message.type == "GROUP";

    String groupName = this.widget.message.name;
    if (!isGroupChat){
      if(widget.message?.config?.shortUsers?.isNotEmpty ?? false){
        ShortUserEntity result = widget.message?.config?.shortUsers.firstWhere((element) => element.id != _user.id);
        groupName = result.name;
      }
    }

    String lastMessage = "";
    switch(widget.message.lastMessageEntity.type){
      case MessageChatType.initialization:
        lastMessage = getLastMessageDescription(this.widget.message.lastMessageEntity);
        break;
      case MessageChatType.inviteMember:
        lastMessage = getInviteLastMessageDescription(this.widget.message.lastMessageEntity, widget.message);
        break;
      default:
        lastMessage = this.widget.message.lastMessageEntity.content ?? "";
        break;
    }

    return Container(
      color: state.isPinned(widget.message)
          ? AppColors.newChatColor
          : Colors.white, // #
      padding: EdgeInsets.fromLTRB(12, 10, 12, 0),
      // height: 70,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Visibility(
                visible: state.enableCheckboxModeRx.value,
                child: Row(
                  children: [
                    GFCheckbox(
                      size: 18,
                      value: state.isCheckedTrue(widget.message),
                      type: GFCheckboxType.square,
                      inactiveBgColor: Colors.transparent,
                      inactiveBorderColor: Colors.transparent,
                      activeBorderColor: Colors.transparent,
                      activeBgColor: Colors.transparent,
                      activeIcon: Image.asset(AppImages.icCheckedBox),
                      inactiveIcon: Image.asset(AppImages.icUncheckedBox),
                      onChanged: (e){
                        if(e)
                          logic.pushToDeletedConversation(widget.message);
                        else
                          logic.removeFromDeletedConversation(widget.message);
                      },
                    ),
                    SizedBox(width: 10),
                  ],
                ),
              ),
              _buildAvatarWidget(),
              SizedBox(width: 12),
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            groupName ?? "",
                            overflow: TextOverflow.fade,
                            softWrap: false,
                            style: Theme.of(context).textTheme.bodyText1.copyWith(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 4),
                          child: _buildNotificationIcon(),
                        ),
                      ],
                    ),
                    SizedBox(height: 4),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: AppMessageText(
                            lastMessage,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            style: Theme.of(context).textTheme.bodyText1.copyWith(
                                  fontWeight: (widget.message.totalMessageUnRead ?? 0) > 0
                                      ? FontWeight.w800
                                      : FontWeight.w400,
                                  fontSize: 14,
                                ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    children: [
                      Visibility(
                          visible: state.isPinned(widget.message),
                          child: Row(
                            children: [
                              Image.asset(
                                AppImages.icBoxPin,
                                width: 15,
                                height: 15,
                                color: AppColors.gray,
                              ),
                              SizedBox(width: 4),
                            ],
                          )),
                      Text(
                        widget.message.beautyCreateDate,
                        overflow: TextOverflow.fade,
                        softWrap: false,
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1
                            .copyWith(fontSize: 12),
                      ),
                    ],
                  ),
                  SizedBox(height: 4),
                  Visibility(
                    visible: widget.message.totalMessageUnRead != null && widget.message.totalMessageUnRead > 0,
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 2),
                      decoration: BoxDecoration(
                        color: Color(0xffFF0000),
                        borderRadius: BorderRadius.circular(24),
                      ),
                      child: Center(
                        child: Text(
                          widget.message.totalMessageUnRead != null && widget.message.totalMessageUnRead > 0 ?
                            "+${widget.message.totalMessageUnRead}":
                            "",
                          style: TextStyle(
                            fontFamily: 'Quicksand',
                            fontSize: 12,
                            color: Colors.white,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(height: 10),
          Container(margin: EdgeInsets.only(left: 50), height: 0.5, color: AppColors.grayLight.withOpacity(0.5)),
        ],
      ),
    );
  }

  List<String> getAvatarUrls() {
    /// trả về mảng 2 phần tử, phần tử 1 là group avatar còn phần tử 2 là user avatar
    String groupAvatar = widget.message.config?.groupAvatar ?? "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
    String user = "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
    List<ShortUserEntity> shortUsers = widget.message.config?.shortUsers ?? [];
    /// Nếu k có last message thì lấy avatar của người tạo
    if(!widget.message.isHadLastMessageTime) {
      if(shortUsers.isNotEmpty ?? false)
        /// thường người tạo sẽ có vị trí đầu tiên trong shortUser
        user = shortUsers[0]?.avatar ?? "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
    } else {
      /// Nếu có last message thì lấy avatar của người gửi tin nhắn cuối cùng
      MessageChat lastMessage = widget.message.lastMessageEntity;
      if(lastMessage != null && (shortUsers.isNotEmpty ?? false)){
        String sender = lastMessage.sender;
        ShortUserEntity findUser = shortUsers.firstWhere((element) => sender == element.code, orElse: () => null);
        if(findUser != null)
          user = findUser?.avatar ?? "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
      }
    }

    // if(widget.message.config.shortUsers?.isNotEmpty ?? false)
    //   user = widget.message.config.shortUsers[0]?.avatar ?? "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
    return [
      groupAvatar,
      user,
    ];
  }

  Widget _buildNotificationIcon() {
    final mutedUsers = widget.message.mutedUsers ?? [];
    final isMuted = mutedUsers.contains(currentUser.value?.id ?? "");
    if (isMuted) {
      return Icon(Icons.notifications_off_outlined,
          color: Theme.of(context).accentColor, size: 16);
    } else {
      return Container();
    }
  }

  Widget _buildAvatarWidget() {
    User _user = Get.find<LocalDataManager>().currentUser;
    final isGroupChat =
        widget.message.type != null && widget.message.type == "GROUP";
    if (isGroupChat) {
      return Container(
        width: 38,
        height: 38,
        child: Stack(
          children: [
            Positioned(
              top: 0,
              left: 0,
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(15)),
                child: CachedNetworkImage(
                  height: 30,
                  width: 30,
                  fit: BoxFit.cover,
                  imageUrl: (getAvatarUrls()?.length ?? 0) > 0
                      ? getAvatarUrls()[0]
                      : Media().thumb,
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              right: 0,
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(11)),
                child: CachedNetworkImage(
                  height: 22,
                  width: 22,
                  fit: BoxFit.cover,
                  imageUrl: (getAvatarUrls()?.length ?? 0) > 1
                      ? getAvatarUrls()[1]
                      : Media().thumb,
                ),
              ),
            ),
          ],
        ),
      );
    }

    String groupAvatar = "${GlobalConfiguration().getValue('base_url')}images/image_default.png";;
    if(widget.message?.config?.shortUsers?.isNotEmpty ?? false){
      ShortUserEntity result = widget.message?.config?.shortUsers.firstWhere((element) => element.id != _user.id);
      groupAvatar = result.avatar;
    }

    return Stack(
      children: <Widget>[
        SizedBox(
          width: 38,
          height: 38,
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(60)),
            child: CachedNetworkImage(
              height: 38,
              width: double.infinity,
              fit: BoxFit.cover,
              imageUrl: groupAvatar,
              placeholder: (context, url) => Image.asset(
                'assets/img/loading.gif',
                fit: BoxFit.cover,
                width: double.infinity,
                height: 38,
              ),
              errorWidget: (context, url, error) => CachedNetworkImage(
                imageUrl: (new Media()).thumb,
                fit: BoxFit.cover,
                width: double.infinity,
                height: 38,
              ),
            ),
          ),
        ),
        Positioned(
          bottom: 3,
          right: 3,
          width: 12,
          height: 12,
          child: Container(
            decoration: BoxDecoration(shape: BoxShape.circle),
          ),
        )
      ],
    );
  }

  String getLastMessageDescription(MessageChat message){
    User _user = Get.find<LocalDataManager>().currentUser;
    String createdBy = message.config?.data?.messageInit?.createBy ?? "";

    bool yourAreCreator = createdBy.isNotEmpty && _user.code == createdBy;
    if(yourAreCreator) return "[Bạn đã tạo mới cuộc trò chuyện]";

    List<ShortUserEntity> shortUsers = message.config?.data?.messageInit?.shortUsers ?? [];
    ShortUserEntity user = shortUsers.firstWhere((element) => element.code == createdBy);
    return "[${user.name} đã tạo mới cuộc trò chuyện]";
  }

  String getInviteLastMessageDescription(MessageChat message, Conversation conversation){
    User _user = Get.find<LocalDataManager>().currentUser;
    String createdBy = message.config?.data?.messageInvite?.createBy ?? "";

    bool yourAreCreator = createdBy.isNotEmpty && _user.code == createdBy;
    if(yourAreCreator) return "[Bạn đã thêm thành viên mới]";

    List<ShortUserEntity> shortUsers = conversation.config?.shortUsers ?? [];
    if(shortUsers.isEmpty) return "";
    ShortUserEntity user = shortUsers.firstWhere((element) => element.code == createdBy, orElse: () => null);
    return "[${user?.name ?? ""} đã thêm thành viên mới]";
  }

}
