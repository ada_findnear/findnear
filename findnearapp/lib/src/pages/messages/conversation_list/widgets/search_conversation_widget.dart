import 'package:findnear/src/pages/search/search_view.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../main.dart';

class SearchConversationWidget extends StatelessWidget {

  const SearchConversationWidget({Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        NavigatorUtils.navigateToListSearchConversation();
      },
      child: Container(
        height: 40,
        decoration: BoxDecoration(
            color: Colors.transparent,
            border: Border.all(
              color: Theme.of(context).focusColor.withOpacity(0.2),
            ),
            borderRadius: BorderRadius.circular(20)),
        child: Row(
          children: <Widget>[
            Expanded(
              child: TextField(
                enabled: false,
                textInputAction: TextInputAction.search,
                style: Theme.of(context)
                    .textTheme
                    .caption
                    .merge(TextStyle(fontSize: 16)),
                decoration: new InputDecoration(
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  hintText: S.of(context).searchForFriends,
                  prefixIcon: Icon(Icons.search, color: Theme.of(context).focusColor),
                  hintStyle: Theme.of(context)
                      .textTheme
                      .caption
                      .merge(TextStyle(fontSize: 16)),
                ),
                onChanged: (s) {
                  // _keySearch.value = s;
                },
                onSubmitted: (text) {
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
