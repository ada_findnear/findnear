import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/repository/chat_repository.dart';
import 'package:findnear/src/repository/communication_repository.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:get/get.dart';
import 'package:images_picker/images_picker.dart';

class MessagesState {
  Rx<LoadStatus> loadConversionStatus = LoadStatus.initial.obs;
  Conversation conversation;
  List<Conversation> cacheConversations = [];
  List<Conversation> conversations = [];
  ChatRepository chatRepository;
  CommunicationRepository conversationRepository;
  // Stream<QuerySnapshot> conversations;
  Stream<QuerySnapshot> chats;
  Media currentMedia = null;
  List newUser = [];
  Rx<LoadStatus> pinConversationStatus = LoadStatus.initial.obs;
  List<Conversation> pinnedConversations = [];
  Rx<LoadStatus> createConversionStatus = LoadStatus.initial.obs;
  Rx<LoadStatus> deleteConversionStatus = LoadStatus.initial.obs;
  Rx<LoadStatus> hideConversionStatus = LoadStatus.initial.obs;
  Rx<LoadStatus> enableCheckboxModeStatus = LoadStatus.initial.obs;
  Rx<LoadStatus> updateDeleteConversationStatus = LoadStatus.initial.obs;
  Rx<LoadStatus> updateLastMessageStatus = LoadStatus.initial.obs;
  Rx<LoadStatus> updateUnreadMessageStatus = LoadStatus.initial.obs;
  Rx<LoadStatus> updateAvatarAndNameStatus = LoadStatus.initial.obs;
  Rx<LoadStatus> updatePinCodeStatus = LoadStatus.initial.obs;
  Rx<LoadStatus> leaveConversionStatus = LoadStatus.initial.obs;
  RxBool enableCheckboxModeRx = false.obs;
  List<Conversation> deleteConversations = [];

  bool isLoggedInState = isLoggedIn;

  MessagesState() {
    chatRepository = new ChatRepository();
    conversationRepository = new CommunicationApi();
    ///Initialize variables
  }

  bool isPinned(Conversation message) {
    // return message.pin == 1;
    Conversation a = pinnedConversations
        ?.firstWhere((element) => message.uuid == element.uuid, orElse: () => null);
    return a != null;
  }

  bool isVisible(Conversation message) {
    return message.isDisplay == true && !isPinned(message);
  }

  List<Conversation> sortedPinnedConversations() {
    pinnedConversations.sort((a, b) => b.createDateInMilis.compareTo(a.createDateInMilis));
    return pinnedConversations;
  }

  List<Conversation> sortedConversations() {
    conversations.sort((a, b) => b.createDateInMilis.compareTo(a.createDateInMilis));
    return conversations;
  }

  bool isCheckedTrue(Conversation message) {
    // return message.pin == 1;
    Conversation a = deleteConversations
        ?.firstWhere((element) => message.uuid == element.uuid, orElse: () => null);
    return a != null;
  }
}
