import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

import '../../../../base/base_controller.dart';
import '../../../../models/entities/message_chat/entities/message_poll.dart';
import '../../../../models/entities/message_chat/enums/message_poll_type.dart';
import '../../../../repository/message_poll_repository.dart';
import '../../../../repository/message_repository.dart';
import '../../../../repository/user_repository.dart';
import '../../../../widgets/dialog_helper.dart';
import 'poll_detail_dialog_state.dart';

class PollDetailDialogController extends BaseController<PollDetailDialogState> {
  final _messagePollRepository = Get.find<MessagePollRepository>();
  final _messageRepository = Get.find<MessageRepository>();

  void onCloseTapped() {
    if (state.optionFullyDisplaying.value == null) {
      Get.back();
    } else {
      state.optionFullyDisplaying.value = null;
    }
  }

  void onTextAddOptionFormChanged(String value) {
    state.isShowSendFromAddOption.value = !value.isBlank;
  }

  void onShowAddOption() {
    state.isShowAddOption.value = true;
    state.addOptionFocusNode.requestFocus();
  }

  void onCloseAddOption() {
    state.addOptionTextController.clear();
    state.isShowAddOption.value = false;
    state.isShowSendFromAddOption.value = false;
  }

  void onSelectVote(int index) {
    if (state.messagePoll.value.isLocked()) {
      return;
    }

    if (index < 0 || index >= state.messagePoll.value.options.length) {
      return;
    }

    final selectOption = state.messagePoll.value.options[index];
    if (!state.messagePoll.value.allowMultipleVoting &&
        state.selectedOptions.length >= 1) {
      if (selectOption.id != state.selectedOptions.first?.id) {
        state.selectedOptions.value = [selectOption];
        return;
      }
    }

    for (var i = 0; i < state.selectedOptions.length; i++) {
      if (state.selectedOptions[i].id == selectOption.id) {
        state.selectedOptions.removeAt(i);
        return;
      }
    }
    state.selectedOptions.add(selectOption);
  }

  bool isChangedVote() {
    final realSelectedOptions =
        getRealSelectedOptions().map((e) => e.id).toSet();
    final currentSelectedOptions =
        state.selectedOptions.map((e) => e.id).toSet();
    return !SetEquality().equals(realSelectedOptions, currentSelectedOptions);
  }

  List<MessagePollOption> getRealSelectedOptions() {
    List<MessagePollOption> result = [];
    for (final option in state.messagePoll.value.options) {
      if (option.pollters.contains(currentUser.value.code)) {
        result.add(option);
      }
    }

    return result;
  }

  void onVote() async {
    state.isSubmitting = true;
    final realSelectedOptions =
        getRealSelectedOptions().map((e) => e.id).toSet();
    final currentSelectedOptions =
        state.selectedOptions.map((e) => e.id).toSet();
    final changeOptions = (realSelectedOptions.toSet()
          ..addAll(currentSelectedOptions))
        .where((e) =>
            !realSelectedOptions.contains(e) ||
            !currentSelectedOptions.contains(e))
        .toSet();

    final voteType = realSelectedOptions.isEmpty
        ? MessagePollType.vote
        : MessagePollType.changeVote;
    final response = await _messagePollRepository.votePoll(
      state.messagePoll.value.id,
      changeOptions.toList(),
    );
    state.isSubmitting = false;

    if (response == null) {
      Fluttertoast.showToast(
        msg: "Không thể bình chọn lúc này, vui lòng thử lại sau",
        toastLength: Toast.LENGTH_LONG,
        timeInSecForIosWeb: 2,
      );
    } else {
      state.messagePoll.value = response;
      state.selectedOptions.value = getRealSelectedOptions();

      if (state.conversation == null) return;
      _messageRepository.sendPoll(
        voteType,
        response,
        state.conversation,
      );
      Get.back();
    }
  }

  void onSendAddOption() async {
    final newOption = state.addOptionTextController.text.trim();

    if (state.messagePoll.value.options
        .where((element) => element.name == newOption)
        .isNotEmpty) {
      Fluttertoast.showToast(
        msg: "Phương án mà bạn thêm đã tồn tại",
        toastLength: Toast.LENGTH_SHORT,
        timeInSecForIosWeb: 1,
      );
      onCloseAddOption();
      return;
    }

    state.isSubmitting = true;
    final response = await _messagePollRepository.addOption(
      state.messagePoll.value.id,
      newOption,
    );
    state.isSubmitting = false;

    if (response == null) {
      Fluttertoast.showToast(
        msg: "Không thể thêm phương án lúc này, vui lòng thử lại sau",
        toastLength: Toast.LENGTH_LONG,
        timeInSecForIosWeb: 2,
      );
    } else {
      state.messagePoll.value = response;
      Future.delayed(Duration(seconds: 1), () {
        state.listOptionsScrollController.animateTo(
          state.listOptionsScrollController.position.maxScrollExtent,
          duration: Duration(seconds: 1),
          curve: Curves.ease,
        );
      });

      if (state.conversation == null) return;
      _messageRepository.sendPoll(
        MessagePollType.addOption,
        response,
        state.conversation,
      );
    }
    onCloseAddOption();
  }

  void onShare() {
    if (state.conversation == null) return;
    _messageRepository.sendPoll(
      MessagePollType.share,
      state.messagePoll.value,
      state.conversation,
    );
    Fluttertoast.showToast(
      msg: "Chia sẻ bình chọn thành công",
      toastLength: Toast.LENGTH_SHORT,
      timeInSecForIosWeb: 1,
    );
  }

  void onLock() async {
    DialogHelper.showConfirmPopup(
      title: "Khoá bình chọn?",
      description:
          "Mọi người sẽ không thể tham gia bình chọn nhưng vẫn có thể xem được kết quả hiện tại.",
      okBtnText: "Khoá",
      cancelText: "Hủy",
      okBtnPressed: () {
        _handleLock();
      },
    );
  }

  void _handleLock() async {
    state.isSubmitting = true;
    final response = await _messagePollRepository.lockPoll(
      state.messagePoll.value.id,
    );
    state.isSubmitting = false;
    
    if (response == null) {
      Fluttertoast.showToast(
        msg: "Không thể khóa bình chọn lúc này, vui lòng thử lại sau",
        toastLength: Toast.LENGTH_LONG,
        timeInSecForIosWeb: 2,
      );
    } else {
      state.messagePoll.value = response;

      if (state.conversation == null) return;
      _messageRepository.sendPoll(
        MessagePollType.close,
        response,
        state.conversation,
      );
    }
  }

  @override
  PollDetailDialogState createState() => PollDetailDialogState();
}
