import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../commons/app_colors.dart';
import '../../../../../commons/app_images.dart';
import '../../../../../widgets/common.dart';
import '../../../../../widgets/touchable_opacity.dart';

class PollDetailBottomSheet extends StatelessWidget {
  final String pollName;
  final bool isShowLockOption;
  final Function() onShare;
  final Function() onLock;

  const PollDetailBottomSheet({
    this.pollName,
    this.isShowLockOption,
    this.onShare,
    this.onLock,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 26, bottom: 26),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildHeader(),
          SizedBox(height: 8),
          _buildOption(
            name: "Gửi vào nhóm chat",
            onSelect: onShare,
            isHasDivider: isShowLockOption,
          ),
          _buildOption(
            name: "Khoá bình chọn",
            isVisible: isShowLockOption,
            isHasDivider: false,
            onSelect: onLock,
          ),
        ],
      ),
    );
  }

  Widget _buildHeader() {
    return Container(
      padding: EdgeInsets.only(
        left: 32,
        right: 16,
      ),
      child: Row(
        children: [
          Expanded(
            child: Text(
              pollName,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: AppColors.textBlack,
              ),
            ),
          ),
          SizedBox(width: 8),
          TouchableOpacity(
            onTap: () => Get.back(),
            child: AppImage(
              AppImages.icClose,
              width: 28,
              height: 28,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildOption({
    String name,
    bool isVisible = true,
    bool isHasDivider = true,
    Function onSelect,
  }) {
    return Visibility(
      visible: isVisible,
      child: Column(
        children: [
          TouchableOpacity(
            onTap: () {
              Get.back();
              onSelect();
            },
            child: Container(
              width: double.infinity,
              padding: EdgeInsets.symmetric(
                horizontal: 32,
                vertical: 18,
              ),
              child: Text(
                name,
                style: TextStyle(
                  fontSize: 14,
                  color: AppColors.textBlack,
                ),
              ),
            ),
          ),
          Visibility(
            visible: isHasDivider,
            child: Container(
              width: double.infinity,
              height: 1,
              color: AppColors.divider,
            ),
          ),
        ],
      ),
    );
  }
}
