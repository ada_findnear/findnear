import 'package:flutter/material.dart';

import '../../../../../commons/app_colors.dart';
import '../../../../../commons/app_images.dart';
import '../../../../../widgets/common.dart';
import '../../../../../widgets/touchable_opacity.dart';

class PollAddOptionWidget extends StatelessWidget {
  final TextEditingController controller;
  final FocusNode focusNode;
  final isShowSendButton;
  final isEnableSend;
  final Function(String value) onChange;
  final Function onSend;
  final Function onClose;

  const PollAddOptionWidget({
    this.controller,
    this.focusNode,
    this.isShowSendButton = false,
    this.isEnableSend = true,
    this.onChange,
    this.onSend,
    this.onClose,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 48,
      padding: EdgeInsets.symmetric(horizontal: 12),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            blurRadius: 6,
            offset: Offset(0, -3),
          )
        ],
      ),
      child: Row(
        children: [
          Expanded(
            child: TextField(
              controller: controller,
              focusNode: focusNode,
              style: TextStyle(
                fontSize: 16,
                color: AppColors.textBlack,
              ),
              decoration: InputDecoration(
                hintText: "Thêm phương án ...",
                hintStyle: TextStyle(
                  fontSize: 16,
                  color: AppColors.textBlack.withOpacity(0.5),
                ),
                border: InputBorder.none,
                isDense: true,
              ),
              onChanged: onChange,
            ),
          ),
          SizedBox(
            width: 8,
          ),
          TouchableOpacity(
            isEnable: isShowSendButton ? isEnableSend : true,
            onTap: () {
              if (isShowSendButton) {
                onSend?.call();
              } else {
                onClose?.call();
              }
            },
            child: isShowSendButton
                ? AppImage(
                    AppImages.icChatSend,
                    width: 21,
                    height: 19,
                  )
                : AppImage(
                    AppImages.icClose,
                    width: 20,
                    height: 20,
                  ),
          ),
        ],
      ),
    );
  }
}
