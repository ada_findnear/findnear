import 'dart:math';

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../base/base_widget_state.dart';
import '../../../../commons/app_colors.dart';
import '../../../../commons/app_images.dart';
import '../../../../elements/CircularAvatarWidget.dart';
import '../../../../models/conversation.dart';
import '../../../../models/entities/message_chat/entities/message_poll.dart';
import '../../../../models/response/user_v2_response.dart';
import '../../../../widgets/common.dart';
import '../../../../widgets/touchable_opacity.dart';
import 'poll_detail_dialog_controller.dart';
import 'poll_detail_dialog_state.dart';
import 'widgets/poll_add_option_widget.dart';
import 'widgets/poll_detail_bottom_sheet.dart';

class PollDetailDialog extends StatefulWidget {
  final MessagePoll messagePoll;
  final List<UserV2Response> users;
  final Conversation conversation;

  const PollDetailDialog({
    @required this.messagePoll,
    this.users = const [],
    this.conversation,
    Key key,
  }) : super(key: key);

  @override
  _PollDetailDialogState createState() => _PollDetailDialogState();
}

class _PollDetailDialogState extends BaseWidgetState<PollDetailDialog,
    PollDetailDialogController, PollDetailDialogState> {
  @override
  void initState() {
    super.initState();
    state.messagePoll.value = widget.messagePoll;
    state.selectedOptions.value = controller.getRealSelectedOptions();
    state.conversation = widget.conversation;
  }

  @override
  void dispose() {
    Get.delete<PollDetailDialogController>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        children: [
          Center(
            child: Container(
              width: double.infinity,
              padding: EdgeInsets.all(40),
              child: Obx(
                () => _buildDialogWidget(),
              ),
            ),
          ),
          Obx(() => _buildAddOptionForm()),
        ],
      ),
    );
  }

  Widget _buildDialogWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        _buildHeader(),
        _buildBody(),
      ],
    );
  }

  Widget _buildHeader() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(vertical: 12),
      decoration: BoxDecoration(
        color: AppColors.red200,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
      child: Center(
        child: Row(
          children: [
            SizedBox(
              width: 34,
            ),
            Expanded(
              child: Text(
                state.optionFullyDisplaying.value?.name ?? "Chi tiết bình chọn",
                textAlign: TextAlign.center,
                maxLines: 4,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: AppColors.textBlack,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ),
            TouchableOpacity(
              onTap: controller.onCloseTapped,
              child: AppImage(
                AppImages.icClose,
                width: 26,
                height: 26,
              ),
            ),
            SizedBox(
              width: 8,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBody() {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        color: AppColors.white,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(20),
          bottomRight: Radius.circular(20),
        ),
      ),
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: AnimatedSize(
        duration: Duration(
          milliseconds: 100,
        ),
        child: state.optionFullyDisplaying.value == null
            ? _buildPollDetail()
            : _buildUsersVotedOption(),
      ),
    );
  }

  Widget _buildPollDetail() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _buildPollName(),
                  _buildPollDeadline(),
                ],
              ),
            ),
            Visibility(
              visible: state.messagePoll.value.isLocked(),
              child: Container(
                padding: EdgeInsets.only(left: 8),
                child: AppImage(
                  AppImages.icPollLock,
                  width: 13,
                  height: 17,
                  color: AppColors.red,
                ),
              ),
            ),
          ],
        ),
        _buildPollCanMultiSelect(),
        SizedBox(height: 10),
        _buildListOptions(),
        Visibility(
          visible: !state.messagePoll.value.isLocked(),
          child: _buildAddOption(),
        ),
        SizedBox(height: 10),
        _buildBottomButtons(),
        SizedBox(height: 24),
      ],
    );
  }

  Widget _buildPollName() {
    return Text(
      state.messagePoll.value.topic,
      maxLines: 4,
      overflow: TextOverflow.ellipsis,
      style: TextStyle(
        color: AppColors.textBlack,
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  Widget _buildPollDeadline() {
    return Visibility(
      visible: state.messagePoll.value.endTime != null,
      child: Padding(
        padding: EdgeInsets.only(top: 4),
        child: Text(
          state.messagePoll.value.getDeadlineFormated(),
          style: TextStyle(
            color: AppColors.gray,
            fontSize: 12,
          ),
        ),
      ),
    );
  }

  Widget _buildPollCanMultiSelect() {
    return Padding(
      padding: EdgeInsets.only(top: 8),
      child: Row(
        children: [
          AppImage(
            AppImages.icPollMultiChoice,
            width: 10,
            height: 10,
          ),
          SizedBox(
            width: 4,
          ),
          Text(
            state.messagePoll.value.allowMultipleVoting
                ? "Được chọn nhiều phương án"
                : "Chỉ chọn một phương án",
            style: TextStyle(
              color: AppColors.gray,
              fontSize: 12,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildListOptions() {
    return Container(
      constraints: BoxConstraints(
        maxHeight: Get.height * 0.4,
      ),
      child: SingleChildScrollView(
        controller: state.listOptionsScrollController,
        physics: BouncingScrollPhysics(),
        child: ListView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: state.messagePoll.value.options.length,
          itemBuilder: (context, index) {
            final option = state.messagePoll.value.options[index];
            return Column(
              children: [
                GestureDetector(
                  onTap: () => controller.onSelectVote(index),
                  child: _buildOptionValueItem(option),
                ),
                SizedBox(
                  height: 4,
                ),
                _buildUsersVotedItem(option),
                SizedBox(
                  height: 6,
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  Widget _buildOptionValueItem(MessagePollOption option) {
    final isSelected = state.selectedOptions
        .where((e) => e.id == option.id)
        .toList()
        .isNotEmpty;
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(5),
        ),
        color: isSelected ? AppColors.red400 : Color(0xFFF2F2F2),
      ),
      padding: EdgeInsets.all(8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Text(
              option.name,
              style: TextStyle(
                color: AppColors.textBlack,
                fontSize: 14,
              ),
            ),
          ),
          SizedBox(
            width: 8,
          ),
          Text(
            option.pollters.length.toString(),
            textAlign: TextAlign.end,
            style: TextStyle(
              color: AppColors.red,
              fontSize: 14,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildUsersVotedItem(MessagePollOption option) {
    return Visibility(
      visible: option.pollters.isNotEmpty,
      child: TouchableOpacity(
        onTap: () {
          state.optionFullyDisplaying.value = option;
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: Wrap(
                children: option.pollters
                    .sublist(
                        0,
                        option.pollters.length <= 33
                            ? option.pollters.length
                            : 33)
                    .map((e) {
                  if (widget.users.isEmpty) {
                    return Container(
                      padding: EdgeInsets.all(2),
                      child: AvatarPlaceholder(
                        size: 16,
                      ),
                    );
                  } else {
                    final user = (widget.users ?? [])
                        .firstWhereOrNull((element) => element.code == e);

                    if (user != null) {
                      return Container(
                        padding: EdgeInsets.all(2),
                        child: CircleAvatar(
                          foregroundImage:
                              NetworkImage(user?.media?.first?.thumb ?? ""),
                          radius: 10,
                        ),
                      );
                    } else {
                      return Container(
                        padding: EdgeInsets.all(2),
                        child: AvatarPlaceholder(
                          size: 16,
                        ),
                      );
                    }
                  }
                }).toList(),
              ),
            ),
            SizedBox(
              width: 4,
            ),
            Transform.rotate(
              angle: pi,
              child: AppImage(
                AppImages.icBack,
                width: 4,
                height: 7,
                color: AppColors.textBlack,
              ),
            ),
            SizedBox(
              width: 2,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildAddOption() {
    return Visibility(
      visible: state.messagePoll.value.allowAdditionOption,
      child: TouchableOpacity(
        onTap: controller.onShowAddOption,
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.add,
                color: AppColors.red,
                size: 16,
              ),
              SizedBox(
                width: 8,
              ),
              Text(
                "Thêm phương án",
                style: TextStyle(
                  color: AppColors.red,
                  fontSize: 16,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildBottomButtons() {
    final isChangedVote = controller.isChangedVote();
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        TouchableOpacity(
          onTap: () {
            Get.bottomSheet(
              PollDetailBottomSheet(
                pollName: state.messagePoll.value.topic,
                isShowLockOption: !state.messagePoll.value.isLocked(),
                onShare: controller.onShare,
                onLock: controller.onLock,
              ),
            );
          },
          child: Container(
            width: 118,
            height: 32,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(16),
              ),
              border: Border.all(
                color: Color(0xFFCCCCCC),
              ),
            ),
            child: Center(
              child: Text(
                "Thiết lập",
                style: TextStyle(
                  fontSize: 16,
                  color: AppColors.textBlack,
                ),
              ),
            ),
          ),
        ),
        Visibility(
          visible: !state.messagePoll.value.isLocked(),
          child: Row(
            children: [
              SizedBox(
                width: 10,
              ),
              TouchableOpacity(
                isEnable: !state.isSubmitting && isChangedVote,
                onTap: controller.onVote,
                child: Container(
                  width: 118,
                  height: 32,
                  decoration: BoxDecoration(
                    color: AppColors.red,
                    borderRadius: BorderRadius.all(
                      Radius.circular(16),
                    ),
                  ),
                  child: Center(
                    child: Text(
                      "Bình chọn",
                      style: TextStyle(
                        fontSize: 16,
                        color: AppColors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildUsersVotedOption() {
    return Container(
      constraints: BoxConstraints(
        maxHeight: Get.height * 0.6,
      ),
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: ListView.separated(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: state.optionFullyDisplaying.value?.pollters?.length ?? 0,
          itemBuilder: (context, index) {
            final userCode =
                state.optionFullyDisplaying.value?.pollters[index] ?? "";
            Widget avatar = AvatarPlaceholder(
              size: 40,
            );

            final user = (widget.users ?? [])
                .firstWhereOrNull((element) => element.code == userCode);

            if (user != null) {
              avatar = CircleAvatar(
                foregroundImage: NetworkImage(user?.media?.first?.thumb ?? ""),
                radius: 20,
              );
            }

            return Container(
              padding: EdgeInsets.only(
                left: 20,
                right: 20,
                top: index == 0 ? 16 : 0,
                bottom: (index + 1) ==
                        (state.optionFullyDisplaying.value?.pollters?.length ??
                            0)
                    ? 16
                    : 0,
              ),
              child: Row(
                children: [
                  avatar,
                  SizedBox(
                    width: 16,
                  ),
                  Flexible(
                    child: Text(
                      user?.name ?? "",
                      style: TextStyle(
                        fontSize: 16,
                        color: AppColors.textBlack,
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
          separatorBuilder: (context, index) => SizedBox(
            height: 10,
          ),
        ),
      ),
    );
  }

  Widget _buildAddOptionForm() {
    return Visibility(
      visible: state.isShowAddOption.value,
      child: Positioned(
        left: 0,
        bottom: 0,
        right: 0,
        child: PollAddOptionWidget(
          controller: state.addOptionTextController,
          focusNode: state.addOptionFocusNode,
          isShowSendButton: state.isShowSendFromAddOption.value,
          isEnableSend: !state.isSubmitting,
          onChange: controller.onTextAddOptionFormChanged,
          onSend: controller.onSendAddOption,
          onClose: controller.onCloseAddOption,
        ),
      ),
    );
  }

  @override
  PollDetailDialogController createController() =>
      Get.put(PollDetailDialogController());
}
