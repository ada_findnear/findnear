import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../base/base_state.dart';
import '../../../../models/conversation.dart';
import '../../../../models/entities/message_chat/entities/message_poll.dart';

class PollDetailDialogState extends BaseState {
  Conversation conversation;

  final messagePoll = MessagePoll().obs;
  final selectedOptions = <MessagePollOption>[].obs;
  final optionFullyDisplaying = Rxn<MessagePollOption>(null);

  final listOptionsScrollController = ScrollController();

  final addOptionTextController = TextEditingController();
  final addOptionFocusNode = FocusNode();

  final isShowAddOption = false.obs;
  final isShowSendFromAddOption = false.obs;
}
