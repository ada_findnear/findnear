import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../base/base_state.dart';
import '../../../../models/conversation.dart';

class CreatePollState extends BaseState {
  Conversation conversation;

  final canCreatePoll = false.obs;
  final isPin = false.obs;
  final isMultiVote = true.obs;
  final canAddMoreOption = true.obs;

  final topicTextController = TextEditingController();
  final options = <TextEditingController>[
    TextEditingController(),
    TextEditingController(),
  ].obs;

  final deadlineDateTime = Rxn<DateTime>(null);
}
