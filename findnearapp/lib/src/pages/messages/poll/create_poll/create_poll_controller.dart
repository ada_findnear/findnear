import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

import '../../../../base/base_controller.dart';
import '../../../../models/entities/message_chat/entities/message_poll.dart';
import '../../../../models/entities/message_chat/enums/message_poll_type.dart';
import '../../../../repository/message_poll_repository.dart';
import '../../../../repository/message_repository.dart';
import '../../../../widgets/dialog_helper.dart';
import 'create_poll_state.dart';

class CreatePollController extends BaseController<CreatePollState> {
  final _messagePollRepository = Get.find<MessagePollRepository>();
  final _messageRepository = Get.find<MessageRepository>();

  void onChangeIsPin() {
    state.isPin.value = !state.isPin.value;
  }

  void onChangeIsMultiVote(bool value) {
    state.isMultiVote.value = value;
  }

  void onChangeCanAddMoreOption(bool value) {
    state.canAddMoreOption.value = value;
  }

  void onAddMoreOption() {
    state.options.add(TextEditingController());
    validateRequest();
  }

  void onRemoveOption(int index) {
    if (index < 0 || index >= state.options.length) {
      return;
    }
    if (state.options.length <= 2) {
      Fluttertoast.showToast(
        msg: "Bình chọn cần tối thiểu 2 phương án",
        toastLength: Toast.LENGTH_SHORT,
        timeInSecForIosWeb: 1,
      );
      return;
    }

    state.options.removeAt(index);
    validateRequest();
  }

  void validateRequest() {
    final emptyOptions =
        state.options.where((element) => element.text.isBlank).toList();

    List<String> duplicateOptions = [];
    List<String> options = [];
    state.options.forEach((e) {
      final text = e.text.trim();
      if (options.contains(text))
        duplicateOptions.add(text);
      else
        options.add(text);
    });

    state.canCreatePoll.value = !state.topicTextController.text.isBlank &&
        emptyOptions.isEmpty &&
        duplicateOptions.isEmpty;
  }

  void onCreatePoll() async {
    if (state.deadlineDateTime.value != null &&
        DateTime.now().compareTo(state.deadlineDateTime.value) > 0) {
      DialogHelper.showAlertDialog(
        context: Get.context,
        title: "Thời hạn không hợp lệ",
        description: "Xin vui lòng chọn thời hạn trong tương lai",
        okText: "Close",
      );
      return;
    }

    state.isSubmitting = true;
    final request = MessagePoll.createRequest(
      topic: state.topicTextController.text.trim(),
      endTime: state.deadlineDateTime.value,
      options: state.options
          .map((element) =>
              MessagePollOption.createRequest(name: element.text.trim()))
          .toList(),
      allowMultipleVoting: state.isMultiVote.value,
      allowAdditionOption: state.canAddMoreOption.value,
    );

    final response = await _messagePollRepository.createPoll(
      state.conversation?.originId ?? "",
      request,
    );
    state.isSubmitting = false;

    if (response != null) {
      _messageRepository.sendPoll(
        MessagePollType.create,
        response,
        state.conversation,
      );
      Get.back();
    } else {
      Fluttertoast.showToast(
        msg: "Tạo bình chọn thất bại",
        toastLength: Toast.LENGTH_LONG,
        timeInSecForIosWeb: 2,
      );
    }
  }

  @override
  CreatePollState createState() => CreatePollState();
}
