import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';
import 'package:intl/intl.dart';

import '../../../../base/base_widget_state.dart';
import '../../../../commons/app_colors.dart';
import '../../../../commons/app_images.dart';
import '../../../../models/route_argument.dart';
import '../../../../widgets/common.dart';
import '../../../../widgets/touchable_opacity.dart';
import 'create_poll_controller.dart';
import 'create_poll_state.dart';
import 'poll_deadline_bottom_sheet/poll_deadline_bottom_sheet_view.dart';

class CreatePollPage extends StatefulWidget {
  static const String ROUTE_NAME = '/CreatePollPage';
  final RouteArgument routeArgument;

  const CreatePollPage({this.routeArgument, Key key}) : super(key: key);

  @override
  _CreatePollPageState createState() => _CreatePollPageState();
}

class _CreatePollPageState extends BaseWidgetState<CreatePollPage,
    CreatePollController, CreatePollState> {
  final double _defaultPaddingHorizontal = 36;

  @override
  void initState() {
    state.conversation = widget.routeArgument.param["conversation"];
    super.initState();
  }

  @override
  void dispose() {
    Get.delete<CreatePollController>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        leading: IconButton(
          icon: Image.asset(
            AppImages.icBackDark,
            color: Theme.of(context).hintColor,
          ),
          onPressed: () {
            Navigator.of(context).pop(false);
          },
        ),
        title: Text(
          "Bình chọn",
          style: TextStyle(
            fontFamily: 'Quicksand',
            fontSize: 16,
            color: AppColors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: <Widget>[
          Obx(
            () => Visibility(
              visible: state.canCreatePoll.value,
              child: TouchableOpacity(
                isEnable: !state.isSubmitting,
                onTap: () => controller.onCreatePoll(),
                child: Container(
                  width: 72,
                  height: 28,
                  margin: EdgeInsets.only(right: 12),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(14.0),
                          color: const Color(0xffFF0000),
                        ),
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 7),
                        child: Center(
                          child: Text(
                            "Tạo",
                            style: TextStyle(
                              fontFamily: 'Quicksand',
                              fontSize: 12,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return SafeArea(
      bottom: true,
      top: false,
      child: Container(
        height: double.infinity,
        width: double.infinity,
        color: AppColors.white,
        child: SingleChildScrollView(
          child: Column(
            children: [
              // _buildPinWidget(),
              // _buildDivider(5),
              _buildListOptionsWidget(),
              _buildDivider(5),
              _buildSettingWidget(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildPinWidget() {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.symmetric(
        horizontal: _defaultPaddingHorizontal,
        vertical: 18,
      ),
      child: GestureDetector(
        onTap: controller.onChangeIsPin,
        child: Row(
          children: [
            Obx(
              () => GFCheckbox(
                size: 18,
                value: state.isPin.value,
                type: GFCheckboxType.square,
                inactiveBgColor: Colors.transparent,
                inactiveBorderColor: Colors.transparent,
                activeBorderColor: Colors.transparent,
                activeBgColor: Colors.transparent,
                activeIcon: Image.asset(AppImages.icCheckedBox),
                inactiveIcon: Image.asset(AppImages.icUncheckedBox),
                onChanged: (value) => controller.onChangeIsPin(),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Flexible(
              child: Text(
                "Ghim lên đầu trò chuyện",
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildListOptionsWidget() {
    return Container(
      width: double.infinity,
      color: Colors.white,
      padding: EdgeInsets.symmetric(
        horizontal: _defaultPaddingHorizontal,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 4),
          TextField(
            controller: state.topicTextController,
            maxLines: null,
            style: TextStyle(
              fontSize: 16,
              color: AppColors.textBlack,
              fontWeight: FontWeight.bold,
            ),
            decoration: InputDecoration(
              hintText: "Đặt câu hỏi bình chọn",
              hintStyle: TextStyle(
                fontSize: 16.0,
                color: AppColors.textBlack.withOpacity(0.5),
              ),
              border: InputBorder.none,
              isDense: true,
            ),
            onChanged: (value) => controller.validateRequest(),
          ),
          Obx(
            () => ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: state.options.length,
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: TextField(
                            controller: state.options[index],
                            maxLines: null,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: AppColors.textBlack,
                            ),
                            decoration: InputDecoration(
                              hintText: "Phương án ${index + 1}",
                              hintStyle: TextStyle(
                                fontSize: 14.0,
                                color: AppColors.textBlack.withOpacity(0.5),
                              ),
                              border: InputBorder.none,
                            ),
                            onChanged: (value) => controller.validateRequest(),
                          ),
                        ),
                        SizedBox(
                          width: 4,
                        ),
                        TouchableOpacity(
                          onTap: () => controller.onRemoveOption(index),
                          child: AppImage(
                            AppImages.icClose,
                            width: 16,
                            height: 16,
                          ),
                        )
                      ],
                    ),
                    _buildDivider(1),
                  ],
                );
              },
            ),
          ),
          SizedBox(height: 16),
          TouchableOpacity(
            onTap: controller.onAddMoreOption,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(
                  Icons.add,
                  color: Theme.of(Get.context).colorScheme.secondary,
                  size: 16,
                ),
                SizedBox(
                  width: 8,
                ),
                Flexible(
                  child: Text(
                    "Thêm phương án",
                    style: TextStyle(
                      fontSize: 14.0,
                      color: Theme.of(Get.context).colorScheme.secondary,
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 16),
        ],
      ),
    );
  }

  Widget _buildSettingWidget() {
    return Column(
      children: [
        Container(
          width: double.infinity,
          color: Colors.white,
          padding: EdgeInsets.symmetric(
            horizontal: _defaultPaddingHorizontal,
            vertical: 16,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              AppImage(
                AppImages.icCoin,
                width: 16,
                height: 16,
              ),
              SizedBox(
                width: 8,
              ),
              Flexible(
                child: Text(
                  "Thiết lập bình chọn",
                  style: TextStyle(
                    fontSize: 16.0,
                    color: AppColors.textBlack,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
        ),
        _buildDivider(1),
        TouchableOpacity(
          onTap: _showDeadlineDialog,
          child: Container(
            width: double.infinity,
            color: Colors.white,
            padding: EdgeInsets.symmetric(
              horizontal: _defaultPaddingHorizontal,
              vertical: 16,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Thời hạn bình chọn",
                        style: TextStyle(
                          fontSize: 14.0,
                          color: AppColors.textBlack,
                        ),
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Obx(
                        () => Text(
                          _getDeadlineText(),
                          style: TextStyle(
                            fontSize: 10.0,
                            color: AppColors.textBlack,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: 8,
                ),
                Transform.rotate(
                  angle: pi,
                  child: AppImage(
                    AppImages.icBack,
                    width: 7,
                    height: 11,
                    color: AppColors.textBlack,
                  ),
                ),
              ],
            ),
          ),
        ),
        _buildDivider(1),
        Container(
          width: double.infinity,
          color: Colors.white,
          padding: EdgeInsets.only(
            left: _defaultPaddingHorizontal,
            right: _defaultPaddingHorizontal - 12,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Text(
                  "Cho phép bình chọn nhiều phương án",
                  style: TextStyle(
                    fontSize: 14.0,
                    color: AppColors.textBlack,
                  ),
                ),
              ),
              SizedBox(
                width: 8,
              ),
              Obx(
                () => Switch(
                  onChanged: controller.onChangeIsMultiVote,
                  value: state.isMultiVote.value,
                ),
              ),
            ],
          ),
        ),
        _buildDivider(1),
        Container(
          width: double.infinity,
          color: Colors.white,
          padding: EdgeInsets.only(
            left: _defaultPaddingHorizontal,
            right: _defaultPaddingHorizontal - 12,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Text(
                  "Cho phép thêm phương án",
                  style: TextStyle(
                    fontSize: 14.0,
                    color: AppColors.textBlack,
                  ),
                ),
              ),
              SizedBox(
                width: 8,
              ),
              Obx(
                () => Switch(
                  onChanged: controller.onChangeCanAddMoreOption,
                  value: state.canAddMoreOption.value,
                ),
              ),
            ],
          ),
        ),
        _buildDivider(1),
      ],
    );
  }

  Widget _buildDivider(double height) {
    return Container(
      width: double.infinity,
      height: height,
      color: AppColors.divider,
    );
  }

  String _getDeadlineText() {
    if (state.deadlineDateTime.value == null) {
      return "Không thời hạn";
    } else {
      final dateFormat = DateFormat("HH:mm, dd/MM/yyyy");
      return "Kết thúc lúc ${dateFormat.format(state.deadlineDateTime.value)}";
    }
  }

  void _showDeadlineDialog() {
    Get.bottomSheet(PollDeadlineBottomSheet(
      deadline: state.deadlineDateTime.value,
      onSelectedDeadline: (value) => state.deadlineDateTime.value = value,
    ));
  }

  @override
  CreatePollController createController() => Get.put(CreatePollController());
}
