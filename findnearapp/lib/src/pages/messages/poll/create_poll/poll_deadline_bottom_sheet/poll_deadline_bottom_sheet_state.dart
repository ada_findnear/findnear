import 'package:get/state_manager.dart';
import 'package:intl/intl.dart';

import '../../../../../base/base_state.dart';

class PollDeadlineBottomSheetState extends BaseState {
  final deadline = Rxn<DateTime>(null);
  final isShowDateTimePicker = false.obs;

  int getRadioDeadlineValue() => deadline.value == null ? 0 : 1;

  String getDeadlineFormated() {
    if (deadline.value == null) {
      return "";
    } else {
      final dateFormat = DateFormat("dd/MM/yyyy, HH:mm");
      return "Ngày ${dateFormat.format(deadline.value)}";
    }
  }
}
