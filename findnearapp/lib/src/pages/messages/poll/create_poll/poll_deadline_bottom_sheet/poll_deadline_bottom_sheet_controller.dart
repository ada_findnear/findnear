import '../../../../../base/base_controller.dart';
import 'poll_deadline_bottom_sheet_state.dart';

class PollDeadlineBottomSheetController
    extends BaseController<PollDeadlineBottomSheetState> {
  void onSelectNoDeadline() {
    state.deadline.value = null;
  }

  void onSelectDeadline() {
    state.isShowDateTimePicker.value = true;
    if (state.deadline.value == null) {
      state.deadline.value = DateTime.now();
    }
  }

  void onCloseDateTimePicker() {
    state.isShowDateTimePicker.value = false;
  }

  void onChangedDeadline(DateTime value) {
    state.deadline.value = value;
  }

  @override
  PollDeadlineBottomSheetState createState() => PollDeadlineBottomSheetState();
}
