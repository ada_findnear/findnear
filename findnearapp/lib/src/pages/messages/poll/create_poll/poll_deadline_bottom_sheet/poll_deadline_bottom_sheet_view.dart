import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:get/get.dart';

import '../../../../../base/base_widget_state.dart';
import '../../../../../commons/app_colors.dart';
import '../../../../../commons/app_images.dart';
import '../../../../../widgets/common.dart';
import '../../../../../widgets/touchable_opacity.dart';
import 'poll_deadline_bottom_sheet_controller.dart';
import 'poll_deadline_bottom_sheet_state.dart';

class PollDeadlineBottomSheet extends StatefulWidget {
  final DateTime deadline;
  final Function(DateTime) onSelectedDeadline;

  const PollDeadlineBottomSheet({
    this.deadline = null,
    this.onSelectedDeadline = null,
    Key key,
  }) : super(key: key);

  @override
  _PollDeadlineBottomSheetState createState() =>
      _PollDeadlineBottomSheetState();
}

class _PollDeadlineBottomSheetState extends BaseWidgetState<
    PollDeadlineBottomSheet,
    PollDeadlineBottomSheetController,
    PollDeadlineBottomSheetState> {
  @override
  void initState() {
    super.initState();
    state.deadline.value = widget.deadline;
  }

  @override
  void dispose() {
    Get.delete<PollDeadlineBottomSheetController>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Container(
        padding: EdgeInsets.only(
          top: 26,
          bottom: state.isShowDateTimePicker.value ? 26 : 76,
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: AnimatedSize(
          duration: Duration(
            milliseconds: 300,
          ),
          curve: Curves.fastOutSlowIn,
          child: state.isShowDateTimePicker.value
              ? _buildDateTimePicker()
              : _buildSelectDeadlineOptions(),
        ),
      ),
    );
  }

  Widget _buildSelectDeadlineOptions() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        _buildHeaderWidget(),
        _buildNoDeadlineOption(),
        Container(
          width: double.infinity,
          height: 1,
          color: AppColors.divider,
        ),
        _buildDeadlineOption(),
      ],
    );
  }

  Container _buildHeaderWidget() {
    return Container(
      padding: EdgeInsets.only(
        left: 32,
        right: 16,
      ),
      child: Row(
        children: [
          Expanded(
            child: Text(
              "Thời hạn bình chọn",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          TouchableOpacity(
            onTap: () => Get.back(),
            child: AppImage(
              AppImages.icClose,
              width: 28,
              height: 28,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildNoDeadlineOption() {
    return GestureDetector(
      onTap: () {
        controller.onSelectNoDeadline();
        widget.onSelectedDeadline?.call(null);
      },
      child: Container(
        padding: EdgeInsets.only(
          left: 16,
          right: 8,
        ),
        child: Row(
          children: [
            Container(
              child: Radio(
                value: 0,
                toggleable: true,
                activeColor: AppColors.red,
                groupValue: state.getRadioDeadlineValue(),
                onChanged: (v) => controller.onSelectNoDeadline(),
              ),
            ),
            Expanded(
              child: Text(
                "Không giới hạn",
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildDeadlineOption() {
    return GestureDetector(
      onTap: () {
        if (state.deadline.value == null) {
          widget.onSelectedDeadline?.call(DateTime.now());
        }
        controller.onSelectDeadline();
      },
      child: Container(
        padding: EdgeInsets.only(
          left: 16,
          right: 24,
        ),
        child: Row(
          children: [
            Container(
              child: Radio(
                value: 1,
                toggleable: true,
                activeColor: AppColors.red,
                groupValue: state.getRadioDeadlineValue(),
                onChanged: (v) => controller.onSelectDeadline(),
              ),
            ),
            Expanded(
              child: Text(
                "Chọn thời điểm kết thúc",
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
            ),
            SizedBox(
              width: 8,
            ),
            Transform.rotate(
              angle: pi,
              child: AppImage(
                AppImages.icBack,
                width: 7,
                height: 11,
                color: AppColors.textBlack,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildDateTimePicker() {
    return Container(
        width: double.infinity,
        height: 300,
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: 40,
              child: Align(
                alignment: Alignment.topCenter,
                child: Text(
                  "Chọn thời gian",
                  style: TextStyle(
                    fontSize: 16,
                    color: AppColors.textBlack,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            Container(
              width: double.infinity,
              height: 50,
              color: AppColors.newChatColor,
              child: Center(
                child: Text(
                  state.getDeadlineFormated(),
                  style: TextStyle(
                    fontSize: 16,
                    color: AppColors.textBlack,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            Container(
              width: double.infinity,
              padding: EdgeInsets.only(
                top: 16,
                bottom: 4,
              ),
              child: Row(
                children: [
                  Expanded(
                    flex: 6,
                    child: Center(
                      child: Text(
                        "Ngày",
                        style: TextStyle(
                          fontSize: 16,
                          color: AppColors.red,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 4,
                    child: Center(
                      child: Text(
                        "Giờ",
                        style: TextStyle(
                          fontSize: 16,
                          color: AppColors.red,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            DateTimePickerWidget(
              minDateTime: DateTime.now(),
              initDateTime: state.deadline.value,
              dateFormat: "dd/MM/yyyy HH:mm",
              locale: DateTimePickerLocale.vi,
              pickerTheme: DateTimePickerTheme(
                showTitle: false,
                pickerHeight: 100,
                itemTextStyle: TextStyle(
                  fontFamily: "Quicksand",
                  fontSize: 20,
                  color: AppColors.textBlack,
                ),
              ),
              onChange: (dateTime, selectedIndex) =>
                  controller.onChangedDeadline(dateTime),
            ),
            SizedBox(
              height: 28,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TouchableOpacity(
                  onTap: controller.onCloseDateTimePicker,
                  child: Container(
                    width: 84,
                    height: 32,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(16),
                      ),
                      border: Border.all(
                        color: Color(0xFFCCCCCC),
                      ),
                    ),
                    child: Center(
                      child: Text(
                        "Đóng",
                        style: TextStyle(
                          fontSize: 14,
                          color: AppColors.textBlack,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                TouchableOpacity(
                  onTap: () {
                    widget.onSelectedDeadline?.call(state.deadline.value);
                    Get.back();
                  },
                  child: Container(
                    width: 84,
                    height: 32,
                    decoration: BoxDecoration(
                      color: AppColors.red,
                      borderRadius: BorderRadius.all(
                        Radius.circular(16),
                      ),
                    ),
                    child: Center(
                      child: Text(
                        "Chọn",
                        style: TextStyle(
                          fontSize: 14,
                          color: AppColors.white,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ));
  }

  @override
  PollDeadlineBottomSheetController createController() =>
      Get.put(PollDeadlineBottomSheetController());
}
