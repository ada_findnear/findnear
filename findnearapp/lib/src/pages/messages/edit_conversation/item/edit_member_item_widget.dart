import 'dart:io';

import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';

class EditMemberItemWidget extends StatelessWidget {
  Function onChanged;
  String avatar;
  String name;
  bool value;
  bool isOldMember;

  EditMemberItemWidget({
    this.onChanged,
    this.avatar,
    this.name,
    this.value,
    this.isOldMember,
  });

  @override
  Widget build(BuildContext context) {
    return _buildBodyWidget();
  }

  Widget _buildBodyWidget() {
    return GestureDetector(
      onTap: !isOldMember ? (){
        onChanged(!value);
      } : () {},
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _buildAvatarWidget(),
              SizedBox(width: 12),
              Expanded(
                child: Text(
                  name,
                  overflow: TextOverflow.fade,
                  softWrap: false,
                  style: Theme.of(Get.context).textTheme.bodyText1.copyWith(
                      fontSize: 14, fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(width: 10),
              GFCheckbox(
                size: 18,
                value: value,
                type: GFCheckboxType.square,
                inactiveBgColor: Colors.transparent,
                inactiveBorderColor: Colors.transparent,
                activeBorderColor: Colors.transparent,
                activeBgColor: Colors.transparent,
                activeIcon: !isOldMember ? Image.asset(AppImages.icCheckedBox): Image.asset(AppImages.icGroupTickGray),
                inactiveIcon: Image.asset(AppImages.icUncheckedBox),
                onChanged: !isOldMember ? onChanged: (bool value) {},
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildAvatarWidget() {
    return SizedBox(
      width: 41,
      height: 41,
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(60)),
        child: CircleAvatar(
          foregroundImage: getImage(avatar),
        ),
      ),
    );
  }

  ImageProvider getImage(url) {
    if (url.contains("http")) {
      return NetworkImage(url);
    } else {
      return FileImage(File(url));
    }
  }
}