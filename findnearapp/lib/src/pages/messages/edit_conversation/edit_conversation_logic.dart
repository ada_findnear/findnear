import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/base/base_refresh_load_more.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/communication/entities/group_config_entity.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/entities/communication/response/conversation_entity.dart';
import 'package:findnear/src/models/entities/communication/response/pinned_conversation_response.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/media.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/bottom_sheet/change_bg_logic.dart';
import 'package:findnear/src/repository/communication_repository.dart';
import 'package:findnear/src/repository/message_repository.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';

import 'edit_conversation_state.dart';

class EditConversationLogic extends BaseController<EditConversationState> with RefreshLoadMoreControllerMixin{
  final EditConversationState state = EditConversationState();
  CommunicationRepository _repository = CommunicationApi(isSocketApi: false);
  CommunicationRepository _socketRepository = CommunicationApi(isSocketApi: true);
  MessageRepository _messageRepository = MessageApi();

  @override
  EditConversationState createState() => EditConversationState();

  void initMemberCheck(List<ShortUserEntity> ShortUserEntities) async {
    state.selectFriends = []..addAll(ShortUserEntities);
    state.oldFriends = []..addAll(ShortUserEntities);
    searchFriend(keyword: '', isReload: true);
  }

  void searchFriend({String keyword = '', bool isReload = false}) async {
    var offset = state.friends.length;

    if (isReload) {
      state.searchFriendStatusRx.value = LoadStatus.loading;
      offset = 0;
      state.canLoadmore.value = true;
      state.friends = [];
    }

    if (state.canLoadmore.value != true) return;
    final response = await _repository.searchFriend(
      keyword: keyword,
      currentOffset: offset,
    );

    if (response.isNotEmpty == true) {
      state.friends = state.friends..addAll(response);
      // state.friends.refresh();
    } else {
      state.canLoadmore.value = false;
    }
    state.searchFriendStatusRx.value = LoadStatus.success;
    state.refreshController.loadComplete();
  }

  @override
  void loadList() {
    // TODO: implement loadList
  }

  void selectFriend({User user}) async {
    state.selectFriendStatusRx.value = LoadStatus.loading;
    try {
      if(state.selectFriends.isNotEmpty ?? false){
        int index = state.selectFriends.indexWhere((element) => element.id == user.id);
        if(index == -1)
          state.selectFriends.add(ShortUserEntity.fromUser(user));
      } else
        state.selectFriends.add(ShortUserEntity.fromUser(user));
      state.enableDoneButtonRx.value = state.selectFriends.length != state.oldFriends.length;
      state.selectFriendStatusRx.value = LoadStatus.success;
    } catch (e){
      state.selectFriendStatusRx.value = LoadStatus.failure;
    }
  }

  void unSelectFriend({User user}) async {
    state.selectFriendStatusRx.value = LoadStatus.loading;
    try {
      if(state.selectFriends.isNotEmpty ?? false){
        int index = state.selectFriends.indexWhere((element) => element.id == user.id);
        if(index != -1)
          state.selectFriends.removeAt(index);
      }
      state.enableDoneButtonRx.value = state.selectFriends.length != state.oldFriends.length;
      state.selectFriendStatusRx.value = LoadStatus.success;
    } catch (e){
      state.selectFriendStatusRx.value = LoadStatus.failure;
    }
  }

  Future<List<ConversationEntity>> findConversationByReceiver(List<ShortUserEntity> listFriend) async {
    User currentUser = Get.find<LocalDataManager>().currentUser;
    List<ConversationEntity> result = await _socketRepository.findConversationByReceiver(receiver: listFriend[0].code, sender: currentUser.code);
    return result;
  }

  Future<PinnedConversationResponse> addMoreMember({List<ShortUserEntity> members, Conversation conversation}) async {
    try {
      PinnedConversationResponse result = await _socketRepository.addMoreMember(conversation: conversation, members: members);
      /// send message sau khi tạo conversation
      if(result != null) {
        List<ShortUserEntity> newMember = [];
        members.forEach((element1) {
          int count = conversation.config.shortUsers.indexWhere((element) => element.code == element1.code);
          if(count == -1)
            newMember.add(element1);
        });
        conversation.config.shortUsers = []..addAll(members);
        _doAfterCreateConversation(conversation, newMember);
      }
      print('quan ${result.toJson().toString()}');
      return result;
    } catch (e) {
      return null;
    }
  }

  void _doAfterCreateConversation(Conversation conversation, List<ShortUserEntity> newMember) {
    _messageRepository.sendInviteMember(conversation, newMember);
  }
}
