import 'dart:io';

import 'package:findnear/src/base/base_refresh_load_more.dart';
import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/MediaActionSheetWidget.dart';
import 'package:findnear/src/helpers/debouncer_helper.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/entities/communication/response/conversation_entity.dart';
import 'package:findnear/src/models/entities/communication/response/pinned_conversation_response.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/members/chat_group_members_view.dart';
import 'package:findnear/src/pages/messages/conversation_detail/conversation_detail_view.dart';
import 'package:findnear/src/pages/messages/conversation_list/widgets/search_member_widget.dart';
import 'package:findnear/src/pages/messages/edit_conversation/item/edit_member_item_widget.dart';
import 'package:findnear/src/pages/pages.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart' as refresh;

import '../../../../main.dart';
import '../../preview_media.dart';
import 'edit_conversation_logic.dart';
import 'edit_conversation_state.dart';
import 'loading/edit_conversation_loading_widget.dart';

class EditConversationPage extends StatefulWidget {
  final RouteArgument routeArgument;

  EditConversationPage({Key key, this.routeArgument}) : super(key: key);
  static const String ROUTE_NAME = '/EditConversationPage';

  @override
  State<EditConversationPage> createState() => _EditConversationPageState();
}

class _EditConversationPageState extends BaseWidgetState<
    EditConversationPage,
    EditConversationLogic,
    EditConversationState>
    with RefreshLoadMoreWidgetMixin {
  Function get onConversationEdited =>
      widget.routeArgument.param["conversation_edited"];

  List<ShortUserEntity> get members => widget.routeArgument.param["members"];

  Conversation get conversation => widget.routeArgument.param["conversation"];

  @override
  EditConversationLogic createController() => EditConversationLogic();

  MediaActionSheetWidget _mediaActionSheetWidget;
  var _textController = TextEditingController();
  final _debouncer = DebouncerHelper();

  @override
  void initState() {
    super.initState();
  }

  @override
  void onReady() {
    super.onReady();
    state.isLoadingRx.value = true;
    controller.initMemberCheck(members);
    //controller.searchFriend(keyword: '', isReload: true);
  }

  @override
  Widget build(BuildContext context) {
    _mediaActionSheetWidget = MediaActionSheetWidget(context: context);
    return Obx(() {
      return WillPopScope(
        onWillPop: () async {
          if (state.selectFriends.length != state.oldFriends.length)
            showDialog(
              context: context,
              builder: (BuildContext context) =>
                  CupertinoAlertDialog(
                    content: Text(S
                        .of(context)
                        .cancel_edit_group),
                    actions: <Widget>[
                      CupertinoDialogAction(
                        isDefaultAction: true,
                        onPressed: () async {
                          Navigator.of(context, rootNavigator: true).pop();
                        },
                        child: Text(S
                            .of(context)
                            .cancel_input),
                      ),
                      CupertinoDialogAction(
                        isDefaultAction: true,
                        onPressed: () async {
                          int count = 0;
                          Navigator.of(context).popUntil((_) => count++ >= 2);
                        },
                        child: Text(S
                            .of(context)
                            .ok),
                      )
                    ],
                  ),
            );
          else
            Navigator.of(context, rootNavigator: true).pop();
          return false;
        },
        child: Scaffold(
            appBar: AppBar(
              title: Obx(() {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Thêm thành viên",
                      style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 16,
                        color: AppColors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      "Đã chọn: ${(state.isSelectedCount()) ?? 0}",
                      style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 12,
                        color: AppColors.black,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ],
                );
              }),
              actions: <Widget>[
                IgnorePointer(
                  ignoring: !state.enableDoneButtonRx.value,
                  child: GestureDetector(
                    onTap: () {
                      _updateConversation();
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: 10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(14.0),
                              color: state.enableDoneButtonRx.value
                                  ? const Color(0xffFF0000)
                                  : AppColors.gray2,
                            ),
                            padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 7),
                            // margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                            child: Center(
                              child: Text(
                                "Hoàn tất",
                                style: TextStyle(
                                  fontFamily: 'Quicksand',
                                  fontSize: 12,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            body: _buildBodyWidget()),
      );
    });
  }

  Widget _buildBodyWidget() {
    return Column(
      children: [
        SizedBox(height: 20),
        _buildTextSearch(),
        Expanded(child: _buildListFriend()),
        Align(
          alignment: Alignment.bottomLeft,
          child: showSelectMembers(),
        )
      ],
    );
  }

  Widget _buildListFriend() {
    if (state.searchFriendStatusRx.value == LoadStatus.loading) {
      return EditLoadingWidget();
    } else if (state.searchFriendStatusRx.value == LoadStatus.success ||
        state.selectFriendStatusRx.value == LoadStatus.success) {
      if (state.friends.isEmpty)
        return Center(child: Text(S
            .of(context)
            .no_result));
      return buildList(
        child: refresh.SmartRefresher(
          controller: state.refreshController,
          onLoading: () =>
              controller.searchFriend(
                keyword: state.textSearchController.text,
              ),
          enablePullUp: state.canLoadmore.value,
          enablePullDown: false,
          child: ListView.separated(
            itemBuilder: (context, index) {
              return EditMemberItemWidget(
                isOldMember: state.isOldMember(state.friends[index]),
                onChanged: (bool val) {
                  if (val)
                    controller.selectFriend(user: state.friends[index]);
                  else
                    controller.unSelectFriend(user: state.friends[index]);
                },
                avatar: state.friends
                    .elementAt(index)
                    .image
                    .thumb,
                name: state.friends
                    .elementAt(index)
                    .name,
                value: state.isUserSelected(state.friends[index]),
              );
            },
            itemCount: state.friends.length,
            separatorBuilder: (_, __) =>
                Divider(
                  height: 10,
                  color: AppColors.dividerColor,
                  thickness: 1,
                ),
            padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
          ),
        ),
      );
    }
    return Center(child: Text(S
        .of(context)
        .no_result));
  }

  Widget _buildTextSearch() {
    return Container(
      height: 40,
      margin: EdgeInsets.only(bottom: 2, left: 10, right: 10),
      decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border.all(
            color: Color(0xffCCCCCC),
          ),
          borderRadius: BorderRadius.circular(20)),
      child: TextField(
        controller: _textController,
        textInputAction: TextInputAction.search,
        style: TextStyle(
          fontFamily: 'Quicksand',
          fontSize: 14,
          fontWeight: FontWeight.normal,
          color: Color(0xff333333),
        ),
        decoration: new InputDecoration(
          border: InputBorder.none,
          focusedBorder: InputBorder.none,
          enabledBorder: InputBorder.none,
          errorBorder: InputBorder.none,
          disabledBorder: InputBorder.none,
          hintText: S
              .of(context)
              .searchForFriends,
          prefixIcon: Icon(Icons.search, color: Theme
              .of(context)
              .focusColor),
          hintStyle: TextStyle(
            fontFamily: 'Quicksand',
            fontSize: 14,
            fontWeight: FontWeight.normal,
            color: Color(0xff333333),
          ),
        ),
        onChanged: (value) async {
          state.keyword.value = value.isNotEmpty ? value : '';
          _debouncer.debounce(() {
            // handle sự kiện bấm SEARCH
            if ((value ?? '').isEmpty) {
              controller.searchFriend(keyword: '', isReload: true);
            } else {
              controller.searchFriend(keyword: value, isReload: true);
            }
          });
        },
        onSubmitted: (value) async {
          state.keyword.value = value.isNotEmpty ? value : '';
        },
      ),
    );
  }

  ImageProvider getImage(url) {
    if (url.contains("http")) {
      return NetworkImage(url);
    } else {
      return FileImage(File(url));
    }
  }

  void updateImage() async {
    _mediaActionSheetWidget.showActionSheet(S
        .of(context)
        .chooseImage, [
      S
          .of(context)
          .viewPicture,
      S
          .of(context)
          .takePhoto,
      S
          .of(context)
          .chooseFromGrallery
    ], (int index) {
      if (index == 0) {
        _previewTopicPhoto(state.pathImage.value);
      }
      if (index == 1) {
        _openCamera((pickedFile) {
          state.pathImage.value = pickedFile.path;
        });
      }
      if (index == 2) {
        _openGrallery((pickedFile) {
          state.pathImage.value = pickedFile.path;
        });
      }
    });
  }

  void _previewTopicPhoto(url) {
    Navigator.of(context).push(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) => PreviewMediaWidget(photoPath: url),
      ),
    );
  }

  void _openCamera(OnPickImageCallback onPickImageCallback) {
    _mediaActionSheetWidget.pickImageDialog(ImageSource.camera,
            (PickedFile pickedFile) {
          print("PickerFile:${pickedFile.path}");
          setState(() {
            onPickImageCallback(pickedFile);
          });
        });
  }

  void _openGrallery(OnPickImageCallback onPickImageCallback) {
    _mediaActionSheetWidget.pickImageDialog(ImageSource.gallery,
            (PickedFile pickedFile) {
          print("PickerFile:${pickedFile.path}");
          setState(() {
            onPickImageCallback(pickedFile);
          });
        });
  }

  void chooseImage() async {
    _mediaActionSheetWidget.showActionSheet(S
        .of(context)
        .chooseImage, [
      S
          .of(context)
          .takePhoto,
      S
          .of(context)
          .chooseFromGrallery
    ], (int index) {
      if (index == 0) {
        _openCamera((pickedFile) {
          state.pathImage.value = pickedFile.path;
        });
      }
      if (index == 1) {
        _openGrallery((pickedFile) {
          state.pathImage.value = pickedFile.path;
        });
      }
    });
  }

  void _updateConversation() async {
    List<ShortUserEntity> members = state.selectFriends;
    PinnedConversationResponse entity = await controller.addMoreMember(
      members: members,
      conversation: conversation,
    );
    if (entity != null) {
      conversation.config.shortUsers = []..addAll(members);
      onConversationEdited(conversation);
      Navigator.pop(context, true);
    } else {
      Fluttertoast.showToast(
          msg: "Thêm thành viên vào cuộc trò chuyện thất bại!",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: AppColors.black,
          textColor: AppColors.white,
          timeInSecForIosWeb: 1);
    }
  }

  Widget showSelectMembers() {
    if (state.searchFriendStatusRx.value == LoadStatus.success ||
        state.selectFriendStatusRx.value == LoadStatus.success) {
      User currentUser = Get
          .find<LocalDataManager>()
          .currentUser;
      List<ShortUserEntity> shortUserEntities = [];
      if (state.selectFriends?.isNotEmpty ?? false) {
        state.selectFriends.forEach((element) {
          if (currentUser.code != element.code &&
              !state.isOldMemberForShort(element))
            shortUserEntities.add(element);
        });
      }
      if (shortUserEntities?.isNotEmpty ?? false)
        return Container(
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 5,
                blurRadius: 7,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          height: 60,
          width: Get.mediaQuery.size.width,
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          child: ListView.separated(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                width: 40,
                child: Stack(
                  children: [
                    CircleAvatar(
                      foregroundImage: getImage(
                          shortUserEntities
                              .elementAt(index)
                              .avatar),
                    ),
                    Positioned(
                      top: -18,
                      left: 10,
                      child: IconButton(
                        onPressed: () {},
                        icon: Image.asset(AppImages.icAddMemberClose),
                        iconSize: 15,
                      ),
                    )
                  ],
                ),
              );
            },
            separatorBuilder: (BuildContext context, int index) =>
                Container(width: 10),
            itemCount: shortUserEntities.length,
          ),
        );
      return Container();
    }
    return Container();
  }

  showAlertDialog(BuildContext context, String text) {
    // set up the button
    Widget okButton = TextButton(
      child: Text(S
          .of(context)
          .ok),
      onPressed: () {
        Navigator.pop(context, true);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(S
          .of(context)
          .notifications),
      content: Text(text),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    state.nameGroup.value = "";
    state.pathImage.value = "";
    state.keyword.value = "";
    state.friends.clear();
    state.selectFriends.clear();
  }
}
