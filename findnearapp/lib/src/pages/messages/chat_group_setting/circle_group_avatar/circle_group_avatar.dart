import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/media.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';

class CircleGroupAvatar extends StatelessWidget {
  String url;
  Function onChooseImage;
  bool isGroupChat;

  CircleGroupAvatar({this.url, this.onChooseImage, this.isGroupChat = false});

  @override
  Widget build(BuildContext context) {
    return Container(child: _buildAvatarWidget());
  }

  Widget _buildAvatarWidget() {
    return Stack(
      children: <Widget>[
        Card(
          elevation: 4.0,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(60)),
              side: BorderSide(width: 2, color: Colors.red)),
          child: SizedBox(
            width: 80,
            height: 80,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(60)),
              child: CachedNetworkImage(
                fit: BoxFit.cover,
                imageUrl: url,
                placeholder: (context, url) => Image.asset(
                  'assets/img/loading.gif',
                  fit: BoxFit.cover,
                  width: double.infinity,
                  height: 38,
                ),
                errorWidget: (context, url, error) => CachedNetworkImage(
                  imageUrl: (new Media()).thumb,
                  fit: BoxFit.cover,
                  width: double.infinity,
                  height: 80,
                ),
              ),
            ),
          ),
        ),
        Visibility(
          visible: isGroupChat,
          child: Positioned(
            bottom: 0,
            right: 0,
            width: 26,
            height: 26,
            child: GestureDetector(
              onTap: onChooseImage,
              child: Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image:
                      DecorationImage(image: AssetImage(AppImages.icChooseAvatar)),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
