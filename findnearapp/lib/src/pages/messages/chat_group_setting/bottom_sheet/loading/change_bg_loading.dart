import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/widgets/shimmer/app_shimmer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ChangeBgLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery
          .of(context)
          .size
          .width,
      height: MediaQuery
          .of(context)
          .size
          .height / 3,
      child: Column(
        children: [
          Expanded(
            flex: 1,
            child: Container(
              color: Colors.white,
              child: Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 10),
                    child: Text(
                      "Chọn ảnh nền",
                      style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 14,
                        color: const Color(0xff404040),
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ),
                  Spacer(),
                  GestureDetector(
                    onTap: () {
                      Get.back();
                    },
                    child: Container(
                        color: Colors.white,
                        padding: EdgeInsets.all(5),
                        child: Image.asset(AppImages.icClose)),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            flex: 4,
            child: GridView.count(
              shrinkWrap: true,
              primary: false,
              // mainAxisSpacing: smallMargin,
              padding: EdgeInsets.zero,
              // set padding to zero
              // crossAxisSpacing: smallMargin,
              crossAxisCount: 6,
              childAspectRatio: 1,
              children: List.generate(28, (index) {
                if (index == 0)
                  return Container(
                    color: Color(0xffF5F5F5),
                    padding: EdgeInsets.all(5),
                    child: Image.asset(AppImages.icChooseBgCam),
                  );
                else if (index == 1)
                  return Container(
                    color: Colors.white,
                    padding: EdgeInsets.all(5),
                  );
                return Container(
                  child: AppShimmer(cornerRadius: 1),
                );
              }),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              color: Colors.white,
              child: Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 10),
                    child: AppShimmer(width: 18, height: 18, cornerRadius: 1),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 10),
                    child: AppShimmer(width: 80, height: 18, cornerRadius: 5),
                  ),
                  Spacer(),
                  GestureDetector(
                    onTap: () {
                      Get.back();
                    },
                    child: Container(
                        margin: EdgeInsets.only(right: 10),
                        child: AppShimmer(width: 70, height: 25, cornerRadius: 5),),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

}