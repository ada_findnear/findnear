import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getwidget/components/checkbox/gf_checkbox.dart';
import 'package:getwidget/types/gf_checkbox_type.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'dart:math' as math;

import 'change_bg_logic.dart';
import 'change_bg_state.dart';
import 'loading/change_bg_loading.dart';

class ChangeBgBottomSheet {
  static void showChangeBgMenu({
    String oldBgImage,
    VoidCallback onCameraPressed,
    VoidCallback onClosePressed,
    Function onBackgroundPressed,
    Function onSavePressed,
  }) async {
    showMaterialModalBottomSheet(
        context: Get.context,
        isDismissible: false,
        builder: (context) {
          /// Ko cho back về, ko cho gạt xuống, chỉ có cách bấm close mới tắt dc bottom sheet
          return WillPopScope(
            onWillPop: () async => false,
            child: GestureDetector(
              onVerticalDragStart: (_) {},
              child: ChangeBgWidget(
                onCameraPressed: onCameraPressed,
                onBackgroundPressed: onBackgroundPressed,
                onClosePressed: onClosePressed,
                onSavePressed: onSavePressed,
                oldBgImage: oldBgImage,
              ),
            ),
          );
        });
  }
}

extension RandomInt on int {
  static int generate({int min = 0, @required int max}) {
    final _random = Random();
    return min + _random.nextInt(max - min);
  }
}

class ChangeBgWidget extends StatefulWidget {
  static const String ROUTE_NAME = '/ChangeBgWidget';
  VoidCallback onCameraPressed;
  Function onBackgroundPressed;
  VoidCallback onClosePressed;
  Function onSavePressed;
  String oldBgImage;

  ChangeBgWidget(
      {this.onCameraPressed,
      this.onBackgroundPressed,
      this.onClosePressed,
      this.onSavePressed,
      this.oldBgImage});

  @override
  _ChangeBgWidgetState createState() => _ChangeBgWidgetState();
}

class _ChangeBgWidgetState
    extends BaseWidgetState<ChangeBgWidget, ChangeBgController, ChangeBgState> {
  bool _isChecked = false;

  @override
  void onReady() {
    controller.initialize(widget.oldBgImage);
  }

  @override
  Widget build(BuildContext context) {
    return _buildBody();
  }

  Widget _buildBody() {
    return Obx(() {
      if (state.fetchBgImageStatus == LoadStatus.loading) {
        return ChangeBgLoading();
      } else if (state.selectBgImageStatus == LoadStatus.success ||
          state.fetchBgImageStatus == LoadStatus.success) {
        return _buildBodyWidget();
      }
      return Center(child: Text('Chưa Background nào'));
    });
  }

  Widget _buildBodyWidget() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height / 3,
      child: Column(
        children: [
          Expanded(
            flex: 1,
            child: Container(
              color: Colors.white,
              child: Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 10),
                    child: Text(
                      "Chọn ảnh nền",
                      style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 14,
                        color: const Color(0xff404040),
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ),
                  Spacer(),
                  GestureDetector(
                    onTap: () {
                      widget.onClosePressed.call();
                      Get.back();
                    },
                    child: Container(
                        color: Colors.white,
                        padding: EdgeInsets.all(5),
                        child: Image.asset(AppImages.icClose)),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            flex: 4,
            child: GridView.count(
              shrinkWrap: true,
              primary: false,
              // mainAxisSpacing: smallMargin,
              padding: EdgeInsets.zero,
              // set padding to zero
              // crossAxisSpacing: smallMargin,
              crossAxisCount: 6,
              childAspectRatio: 1,
              children: List.generate(state.bgImages.length, (index) {
                if (state.bgImages[index] == "0")
                  return GestureDetector(
                    onTap: () {
                      // Get.back();
                      controller.selectBgImage(state.bgImages[index]);
                      widget.onCameraPressed.call();
                    },
                    child: Container(
                      color: Color(0xffF5F5F5),
                      padding: EdgeInsets.all(5),
                      child: Image.asset(AppImages.icChooseBgCam),
                    ),
                  );
                if (state.bgImages[index] == ChangeBgController.DEFAULT_BACKGROUND)
                  return GestureDetector(
                    onTap: () {
                      widget.onBackgroundPressed.call(state.bgImages[index]);
                      controller.selectBgImage(state.bgImages[index]);
                    },
                    child: Stack(
                      children: [
                        Container(
                          color: Colors.white54,
                          padding: EdgeInsets.all(5),
                          child: Center(
                            child: FittedBox(
                              child: Text(
                                "NONE",
                                style: TextStyle(
                                    color: AppColors.grayLight,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ),
                        Visibility(
                          visible: state.bgImages[index] == state.selectImage,
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.transparent,
                              border: Border.all(
                                  color: AppColors.red, width: 5),
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                return GestureDetector(
                  onTap: () {
                    widget.onBackgroundPressed.call(state.bgImages[index]);
                    controller.selectBgImage(state.bgImages[index]);
                  },
                  child: Stack(
                    children: [
                      Positioned.fill(
                        child: CachedNetworkImage(
                          imageUrl: state.bgImages[index],
                          fit: BoxFit.cover,
                          // width: double.infinity,
                          errorWidget: (context, url, error) => Image.network(
                            "${GlobalConfiguration().getValue('base_url')}images/image_default.png",
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Visibility(
                        visible: state.bgImages[index] == state.selectImage,
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.transparent,
                            border:
                                Border.all(color: AppColors.red, width: 5),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              }),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              color: Colors.white,
              child: Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 10),
                    child: GFCheckbox(
                      size: 18,
                      value: state.enableBothTwoSide,
                      type: GFCheckboxType.square,
                      inactiveBgColor: Colors.transparent,
                      inactiveBorderColor: Colors.transparent,
                      activeBorderColor: Colors.transparent,
                      activeBgColor: Colors.transparent,
                      activeIcon: Image.asset(AppImages.icCheckedBox),
                      inactiveIcon: Image.asset(AppImages.icUncheckedBox),
                      onChanged: (e) {
                        controller.enableBothTwoSide(e);
                      },
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 10),
                    child: Text(
                      "Đổi ảnh nền cho cả 2 bên",
                      style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 14,
                        color: const Color(0xff404040),
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ),
                  Spacer(),
                  GestureDetector(
                    onTap: () {
                      Get.back();
                      widget.onSavePressed(state.enableBothTwoSide);
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5.0),
                        color: const Color(0xffFF0000),
                      ),
                      padding: EdgeInsets.all(7),
                      margin: EdgeInsets.only(right: 10),
                      child: Row(
                        children: [
                          Image.asset(AppImages.icChooseBgTick),
                          SizedBox(width: 5),
                          Text(
                            "Áp dụng",
                            style: TextStyle(
                              fontFamily: 'Quicksand',
                              fontSize: 12,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  ChangeBgController createController() => ChangeBgController();
}
