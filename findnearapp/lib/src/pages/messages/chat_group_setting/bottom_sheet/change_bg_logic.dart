
import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/models/enums/load_status.dart';

import 'change_bg_state.dart';

class ChangeBgController extends BaseController<ChangeBgState>{
  static String DEFAULT_BACKGROUND = "default_background";

  @override
  ChangeBgState createState() => ChangeBgState();

  void initialize(String oldImage) async{
    state.fetchBgImageStatus.value = LoadStatus.loading;
    try {
      state.selectImage = oldImage;
      Future.delayed(const Duration(milliseconds: 2000), () {
        state.bgImages = [
          "0",
          ChangeBgController.DEFAULT_BACKGROUND,
          "https://i.pinimg.com/564x/6b/d2/48/6bd248add72ff4127715c1529b177555.jpg",
          "https://cdn.tgdd.vn/Files/2021/09/15/1382765/cach-tai-hinh-nen-iphone-13-1_1280x2772-800-resize.jpg",
          "https://i.pinimg.com/736x/a7/d6/6b/a7d66b55963f059b3ce6df6080471d48.jpg",
          "https://anhdep123.com/wp-content/uploads/2020/10/hinh-nen-samsung-1.jpg",
          "https://img3.thuthuatphanmem.vn/uploads/2019/08/13/anh-nen-4k-rat-dep-lam-nen-dien-thoai-nguoi-nhen_052950557.jpg",
          "https://1.bp.blogspot.com/-YHENl2tCaJI/X7aK8OXhsrI/AAAAAAAA0u0/SyCgArikodsdU0gBiXNB5l_LQVHloGQ1ACLcBGAsYHQ/s0/Rog-Phone-3%2B%25281%2529.jpg",
          "https://img3.thuthuatphanmem.vn/uploads/2019/08/13/anh-4k-lam-nen-dien-thoai-cuc-dep-star-war-cuoc-chien-giua-c_sEYyh_052935744.jpg",
          "https://wallpaperaccess.com/full/6081319.jpg",
          "https://cellphones.com.vn/sforum/wp-content/uploads/2021/07/Abstract-waves-wallpaper-iPhone-yudhajit_ghosh-idownloadblog-mockup.png",
          "https://1.bp.blogspot.com/-R_oj45KKbu8/X7aK_MeYrDI/AAAAAAAA0vk/v4D9TV-ZDgQN_syxZUfbl-e_b3jTLw23QCLcBGAsYHQ/s0/Rog-Phone-3%2B%25283%2529.jpg",
          "https://tenovi.net/public/upload/images/2019/08/04/moi-tai-ve-bo-anh-nen-mac-dinh-tren--asus-rog-phone-2-black-shark-2-pro-realme-x-spiderman-edition-va-mot-so-thiet-bi-moi-khac_1_24.png",
          "https://1.bp.blogspot.com/-fk-KMBpyzT8/X7aK_cogz3I/AAAAAAAA0vs/L_kZVQLgvRUMubbMQZctcRnlUNNZabRSwCLcBGAsYHQ/s0/Rog-Phone-3%2B%25284%2529.jpg",
          "https://i.pinimg.com/564x/51/02/16/5102162bc52d172d2195a8edfdb0b50f.jpg",
          "https://anhdep123.com/wp-content/uploads/2020/11/anh-nen-iphone-x.jpg",
          "https://cellphones.com.vn/sforum/wp-content/uploads/2020/03/Abstract-Curves-iPhone-wallpaper-idownloadblog-ieditwalls-1.jpeg",
          "https://cdn-aooek.nitrocdn.com/jADLkKldCLyvbzneZqCzAXMpXrnxnSeV/assets/static/optimized/rev-fe9ac44/originals/5d/62/95/5d6295309a072c8f5a4f49cdd0bf762e.jpg",
          "https://tophinhanhdep.com/wp-content/uploads/2021/11/Snow-Mountain-Phone-Wallpapers-576x1024.jpg",
          "https://i.pinimg.com/originals/00/f4/2b/00f42ba9d9c45ecc3a3cc36729ac4766.jpg",
          "https://anhhd.com/wp-content/uploads/2021/10/hinh-nen-dien-thoai-ngau-6.jpg"
        ];
        state.fetchBgImageStatus.value = LoadStatus.success;
      });

    } catch (e) {
      state.fetchBgImageStatus.value = LoadStatus.failure;
    }
  }

  void selectBgImage(String image) async{
    state.selectBgImageStatus.value = LoadStatus.loading;
    try {
      state.selectImage = image;
      state.selectBgImageStatus.value = LoadStatus.success;
    } catch (e) {
      state.selectBgImageStatus.value = LoadStatus.failure;
    }
  }

  void enableBothTwoSide(bool value) async{
    state.selectBgImageStatus.value = LoadStatus.loading;
    try {
      state.enableBothTwoSide = value;
      state.selectBgImageStatus.value = LoadStatus.success;
    } catch (e) {
      state.selectBgImageStatus.value = LoadStatus.failure;
    }
  }

}