import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:get/get.dart';

class ChangeBgState extends BaseState {
  RxBool enableBothTwoSideRx = true.obs;
  List<String> bgImages = [];
  Rx<LoadStatus> fetchBgImageStatus = LoadStatus.initial.obs;
  Rx<LoadStatus> selectBgImageStatus = LoadStatus.initial.obs;
  String selectImage;

  set enableBothTwoSide(value) {
    enableBothTwoSideRx.value = value;
  }

  bool get enableBothTwoSide => enableBothTwoSideRx.value;
}