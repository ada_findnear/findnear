import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/database/chats_dao.dart';
import 'package:findnear/src/database/conversations_dao.dart';
import 'package:findnear/src/global/global_event.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/entities/communication/response/conversation_entity.dart';
import 'package:findnear/src/models/entities/communication/response/delete_conversation_response.dart';
import 'package:findnear/src/models/entities/message_chat/entities/message_chat.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/pages.dart';
import 'package:findnear/src/repository/communication_repository.dart';
import 'package:findnear/src/repository/message_repository.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';

import 'chat_group_setting_state.dart';

class ChatGroupSettingLogic extends BaseController<ChatGroupSettingState> {
  final _conversationsDAO = Get.find<ConversationsDAO>();
  var _chatsDAO = ChatsDAO(tableName: null);
  CommunicationRepository _repository = CommunicationApi();
  MessageRepository _messageRepository = MessageApi();

  @override
  ChatGroupSettingState createState() => ChatGroupSettingState();

  void updateHiddenState(bool value) {
    state.hideConversation = value;
  }

  void updateHasPINCodeState(bool value) {
    state.hasPINCode = value;
  }

  void updatePinConversationState(bool value) {
    state.pinConversation = value;
  }

  void fetchConversationFromLocal(String uuid) async {
    state.fetchConversationStatus.value = LoadStatus.loading;
    try {
      state.conversation = await _conversationsDAO.get(uuid);
      state.hideConversation = !state.conversation.isDisplay;
      state.pinConversation = state.conversation.pinTop;

      /// Lấy danh sách conversation ra
      await _chatsDAO.closeBox();
      _chatsDAO = ChatsDAO(tableName: state.conversation.id);
      List<MessageChat> results = await _chatsDAO.getImageOnly()
        ..sort((a, b) => b.createDate.compareTo(a.createDate));

      /// tối đa chỉ recommend 4 ảnh thôi
      int maxLength = results.length <= 4 ? results.length : 4;
      for (int i = 0; i < maxLength; i++) {
        state.recommendImages.add(results[i]
                .config
                ?.data
                ?.image
                ?.media
                ?.thumb ??
            "${GlobalConfiguration().getValue('base_url')}images/image_default.png");
      }

      /// Nếu recommend image empty thì khỏi cần add icon more vào nữa
      if (state.recommendImages.isNotEmpty) state.recommendImages.add("");
      state.fetchConversationStatus.value = LoadStatus.success;
    } catch (e) {
      state.fetchConversationStatus.value = LoadStatus.failure;
    }
  }

  Future<bool> createPINCode(String pinCode, String uuid) async {
    state.fetchConversationStatus.value = LoadStatus.loading;
    try {
      /// Tạo mã pin và ẩn luôn cuộc trò chuyện
      Conversation conversation =
          state.conversation.copyWith(pin: pinCode, isDisplay: false);
      var response = await _repository.updateConversation(
          conversation); // _repository.createPinCode(uuid: uuid, pinCode: pinCode)
      if (response != null) {
        state.conversation.pin = pinCode;
        state.conversation.isDisplay = false;
        state.conversation.pinTop = false;
        _conversationsDAO.put(state.conversation);
        updateHiddenState(true);
        state.fetchConversationStatus.value = LoadStatus.success;
        return true;
      }
      return false;
    } catch (e) {
      state.fetchConversationStatus.value = LoadStatus.failure;
      return false;
    }
  }

  Future<bool> visibleConversation() async {
    state.fetchConversationStatus.value = LoadStatus.loading;
    try {
      /// Tạo mã pin và ẩn luôn cuộc trò chuyện
      Conversation conversation = state.conversation.copyWith(isDisplay: true);
      var response = await _repository.updateConversation(conversation);
      if (response != null) {
        state.conversation.isDisplay = true;
        _conversationsDAO.put(state.conversation);
        updateHiddenState(false);
        state.fetchConversationStatus.value = LoadStatus.success;
        return true;
      }
      return false;
    } catch (e) {
      state.fetchConversationStatus.value = LoadStatus.failure;
      return false;
    }
  }

  Future<bool> hideConversation() async {
    state.fetchConversationStatus.value = LoadStatus.loading;
    try {
      /// ẩn cuộc trò chuyện
      Conversation conversation = state.conversation.copyWith(isDisplay: false);
      var response = await _repository.updateConversation(conversation);
      if (response != null) {
        state.conversation.isDisplay = false;
        state.conversation.pinTop = false;
        _conversationsDAO.put(state.conversation);
        updateHiddenState(true);
        state.fetchConversationStatus.value = LoadStatus.success;
        return true;
      }
      return false;
    } catch (e) {
      state.fetchConversationStatus.value = LoadStatus.failure;
      return false;
    }
  }

  Future<String> uploadAvatar(String pathImage) async {
    state.uploadAvatarStatus.value = LoadStatus.loading;
    try {
      var image = await _repository.uploadFile(pathImage, "group_avatar");
      if (image?.url?.isNotEmpty ?? false) {
        state.conversation.config.groupAvatar = image.url;
        var response = await _repository.updateConversation(state.conversation);
        if (response != null) {
          _conversationsDAO.put(state.conversation);
          state.uploadAvatarStatus.value = LoadStatus.success;
          GlobalEvent.instance.onUpdateAvatarAndNameResponse
              .add(state.conversation);
          return image.url;
        }
        state.uploadAvatarStatus.value = LoadStatus.failure;
        return "";
      }
    } catch (e) {
      state.uploadAvatarStatus.value = LoadStatus.failure;
    }
    return "";
  }

  Future<bool> changeGroupName(String name) async {
    state.changeGroupNameStatus.value = LoadStatus.loading;
    try {
      state.conversation.name = name;
      var response = await _repository.updateConversation(state.conversation);
      if (response != null) {
        _conversationsDAO.put(state.conversation);
        state.changeGroupNameStatus.value = LoadStatus.success;
        GlobalEvent.instance.onUpdateAvatarAndNameResponse
            .add(state.conversation);
        return true;
      }
      return false;
    } catch (e) {
      state.changeGroupNameStatus.value = LoadStatus.failure;
      return false;
    }
  }

  void leaveConversation(String uuid) async {
    /// Leave khỏi hội thoại r thì k send message dc nữa nên phải send leave message trước
    User currentUser = Get.find<LocalDataManager>().currentUser;
    /// send message leave cuộc trò truyện
    _messageRepository.sendLeaveConversation(state.conversation, ShortUserEntity.fromUser(currentUser));
    /// Leave khỏi cuộc trò chuyện
    DeleteConversationResponse result =
        await _repository.leaveConversation(uuid);
    if (result != null) {
      /// set global event để cập nhật lại danh sách cuộc trò chuyện
      GlobalEvent.instance.onLeaveConversationResponse
          .add(ConversationEntity.fromConversation(state.conversation));
      /// pop cho đến khi về lại màn hình danh sách hội thoại
      Navigator.of(Get.context, rootNavigator: true).popUntil(
          ModalRoute.withName(PagesWidget.ROUTE_NAME));
      Fluttertoast.showToast(
          msg: "Rời cuộc hội thoại thành công!",
          toastLength: Toast.LENGTH_SHORT,
          backgroundColor: AppColors.black,
          textColor: AppColors.white,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1);
    } else
      AppSnackbar.showError(
          title: "Thông báo", message: "Rời cuộc hội thoại thất bại");
  }

  void updateMemberConversation(Conversation obj) async {
    state.fetchConversationStatus.value = LoadStatus.loading;
    try {
      state.conversation.config.shortUsers = []..addAll(obj.config?.shortUsers ?? []);
      state.conversation.name = obj.name;

      await _conversationsDAO.put(obj);
      state.fetchConversationStatus.value = LoadStatus.success;
    } catch (e){
      state.fetchConversationStatus.value = LoadStatus.failure;
    }
  }

}
