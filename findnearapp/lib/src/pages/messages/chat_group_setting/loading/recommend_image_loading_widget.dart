import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/widgets/shimmer/app_shimmer.dart';
import 'package:flutter/material.dart';

class RecommendImageLoadingWidget extends StatelessWidget {
  const RecommendImageLoadingWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Image.asset(
          AppImages.icGroupLibrary,
          width: 18.47,
          height: 14.8,
          color: AppColors.red,
        ),
        SizedBox(width: 13),
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                children: [
                  Text(
                    "Thư viện hình ảnh",
                    style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: AppColors.black),
                  ),
                ],
              ),
              Text(
                "Xem ảnh, link, file đã chia sẻ",
                style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 12,
                    fontWeight: FontWeight.normal,
                    color: AppColors.black),
              ),
              SizedBox(height: 10),
              Flexible(
                child: Container(
                  height: 50,
                  child: ListView.separated(
                    scrollDirection: Axis.horizontal,
                    itemCount: 5,
                    separatorBuilder: (BuildContext context, int index) =>
                        SizedBox(width: 10),
                    itemBuilder: (_, int index) =>
                        AppShimmer(width: 50, height: 50, cornerRadius: 5)
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }


}
