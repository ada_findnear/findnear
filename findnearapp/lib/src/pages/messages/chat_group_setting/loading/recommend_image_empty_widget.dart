import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/widgets/shimmer/app_shimmer.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

class RecommendImageEmptyWidget extends StatelessWidget {
  const RecommendImageEmptyWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Image.asset(
          AppImages.icGroupLibrary,
          width: 18.47,
          height: 14.8,
          color: AppColors.red,
        ),
        SizedBox(width: 13),
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                children: [
                  Text(
                    "Thư viện hình ảnh",
                    style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: AppColors.black),
                  ),
                ],
              ),
              Text(
                "Xem ảnh, link, file đã chia sẻ",
                style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 12,
                    fontWeight: FontWeight.normal,
                    color: AppColors.black),
              ),
              SizedBox(height: 10),
              Flexible(
                child: Container(
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    color: AppColors.red.withOpacity(0.1),
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(width: 5),
                      ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        child: Icon(Icons.cloud_circle, color: AppColors.red.withOpacity(0.2), size: 40),
                      ),
                      SizedBox(width: 13),
                      Expanded(
                        child: Text(
                          "Hình mới nhất của trò chuyện sẽ xuất hiện tại đây",
                          style: TextStyle(
                            fontFamily: "Quicksand",
                            fontSize: 12,
                            color: AppColors.black,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ),
                      SizedBox(width: 5),
                    ],
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }


}
