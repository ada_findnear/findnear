class ChatGroupParams {
  String uuid;
  Function onTapChangeBackground;

  ChatGroupParams({this.uuid, this.onTapChangeBackground});
}