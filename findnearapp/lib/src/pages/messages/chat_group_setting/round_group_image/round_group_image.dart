import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/media.dart';
import 'package:flutter/cupertino.dart';

class RoundGroupImage extends StatelessWidget {
  String imageUrl;

  RoundGroupImage({this.imageUrl});

  @override
  Widget build(BuildContext context) {
    return Container(child: _buildAvatarWidget());
  }

  Widget _buildAvatarWidget() {
    return imageUrl.isNotEmpty
        ? ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            child: CachedNetworkImage(
              height: 50,
              width: 50,
              fit: BoxFit.cover,
              imageUrl: imageUrl,
              placeholder: (context, url) => Image.asset(
                'assets/img/loading.gif',
                fit: BoxFit.cover,
                width: 50,
                height: 50,
              ),
              errorWidget: (context, url, error) => CachedNetworkImage(
                imageUrl: (new Media()).thumb,
                fit: BoxFit.cover,
                width: 50,
                height: 50,
              ),
            ),
          )
        : ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            child: Container(
              height: 50,
              width: 50,
              color: Color(0xffFFDBDB),
              child: Image.asset(AppImages.icGroupMoreImg),
            ),
          );
  }
}
