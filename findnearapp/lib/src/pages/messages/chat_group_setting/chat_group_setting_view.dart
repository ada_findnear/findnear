import 'dart:async';
import 'dart:io';

import 'package:findnear/main.dart';
import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/MediaActionSheetWidget.dart';
import 'package:findnear/src/global/global_event.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/entities/communication/response/conversation_entity.dart';
import 'package:findnear/src/models/enums/create_pin_step_type.dart';
import 'package:findnear/src/models/enums/edit_pin_step_type.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/circle_group_avatar/circle_group_avatar.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/loading/recommend_image_loading_widget.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/panel_item/panel_item.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/params/chat_group_params.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/round_group_image/round_group_image.dart';
import 'package:findnear/src/pages/messages/conversation_list/messages_logic.dart';
import 'package:findnear/src/pages/profile_user/profile_user_view.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/shimmer/app_shimmer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:image_picker/image_picker.dart';
import 'package:passcode_screen/circle.dart';
import 'package:passcode_screen/keyboard.dart';
import 'package:passcode_screen/passcode_screen.dart';

import 'bottom_sheet/change_bg_bottom_sheet.dart';
import 'chat_group_setting_logic.dart';
import 'chat_group_setting_state.dart';
import 'dialog/change_group_name_widget.dart';
import 'loading/recommend_image_empty_widget.dart';

class ChatGroupSettingPage extends StatefulWidget {
  static const String ROUTE_NAME = 'ChatGroupSettingPage';
  final RouteArgument routeArgument;

  ChatGroupSettingPage({this.routeArgument});

  @override
  State<ChatGroupSettingPage> createState() => _ChatGroupSettingPageState();
}

class _ChatGroupSettingPageState extends BaseWidgetState<ChatGroupSettingPage,
    ChatGroupSettingLogic, ChatGroupSettingState> {
  final StreamController<bool> _verificationNotifierCreate =
      StreamController<bool>.broadcast();
  final StreamController<bool> _verificationNotifier =
      StreamController<bool>.broadcast();
  EditPINStepType _editStepType = EditPINStepType.NONE;

  @override
  ChatGroupSettingLogic createController() => ChatGroupSettingLogic();

  ChatGroupParams get chatGroupParams => widget.routeArgument.param;

  String get uuid => chatGroupParams.uuid;

  Function get onTapChangeBackground => chatGroupParams.onTapChangeBackground;

  @override
  void initState() {
    super.initState();
    controller.fetchConversationFromLocal(uuid);
    GlobalEvent.instance.onPinCodeUpdated.stream.listen((data) {
      debugPrint("quanth: onPinCodeUpdated= ${data}");
      controller.fetchConversationFromLocal(data.uuid);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.white,
        title: Text(
          'Cài đặt',
          style: TextStyle(
            fontFamily: 'Quicksand',
            fontSize: 16,
            color: AppColors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            _buildAvatar(),
            _buildName(),
            _buildPanel(),
            Container(height: 10),
            _buildSeperate(verticalMargin: 5),
            Flexible(child: _buildLibrary()),
            _buildSeperate(verticalMargin: 5),
            _buildCreateGroup(),
            _buildMembers(),
            _buildPinMessage(),
            _buildHideConversation(),
            _buildSeperate(verticalMargin: 5),
            _buildChangeBackground(),
            _buildSeperate(verticalMargin: 5),
            _buildOffNotification(),
            _buildSeperate(verticalMargin: 5),
            _buildClearHistory(),
            _buildSeperate(verticalMargin: 5),
            _buildLeaveGroup(),
          ],
        ),
      ),
    );
  }

  Widget _buildAvatar() {
    return Container(
      margin: EdgeInsets.only(top: 18),
      width: Get.mediaQuery.size.width,
      child: Center(child: Obx(() {
        String url =
            "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
        if (state.fetchConversationStatus == LoadStatus.success ||
            state.uploadAvatarStatus == LoadStatus.success)
          url = state.conversation.config.groupAvatar;
        return CircleGroupAvatar(
            url: url,
            onChooseImage: _onChooseImage,
            isGroupChat: state.conversation?.type == "GROUP" ?? false);
      })),
    );
  }

  Widget _buildName() {
    return Container(
      width: Get.mediaQuery.size.width,
      margin: EdgeInsets.only(top: 6, left: 20, right: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Flexible(
            child: Obx(() {
              if (state.fetchConversationStatus == LoadStatus.loading ||
                  state.changeGroupNameStatus == LoadStatus.loading)
                return AppShimmer(width: 100, height: 20, cornerRadius: 8);
              else if (state.fetchConversationStatus == LoadStatus.success ||
                  state.changeGroupNameStatus == LoadStatus.success)
                return Text(
                  state.conversation.name ?? "",
                  style: TextStyle(
                    fontFamily: "Quicksand",
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: AppColors.black,
                  ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                );
              return AppShimmer(width: 100, height: 20, cornerRadius: 8);
            }),
          ),
          SizedBox(width: 3),
          Obx(() {
            if (state.fetchConversationStatus == LoadStatus.loading ||
                state.changeGroupNameStatus == LoadStatus.loading)
              return AppShimmer(width: 20, height: 20, cornerRadius: 8);
            else if (state.fetchConversationStatus == LoadStatus.success ||
                state.changeGroupNameStatus == LoadStatus.success)
              return Visibility(
                visible: state.conversation?.type == "GROUP" ?? false,
                child: GestureDetector(
                  onTap: () {
                    _showChangeNameDialog();
                  },
                  child: Container(
                    padding: EdgeInsets.all(10),
                    color: AppColors.transparent,
                    child: Image.asset(
                      AppImages.icChangeName,
                      width: 10,
                      height: 10,
                    ),
                  ),
                ),
              );
            return AppShimmer(width: 20, height: 20, cornerRadius: 8);
          })
        ],
      ),
    );
  }

  Widget _buildPanel() {
    return Obx(() {
      if (state.fetchConversationStatus == LoadStatus.success)
        return Container(
          margin: EdgeInsets.only(top: 13, left: 30, right: 30),
          width: Get.mediaQuery.size.width,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  child: PanelItem(
                      image: AppImages.icGroupSearch, text: "Tìm tin nhắn"),
                ),
              ),
              Visibility(
                visible: state.conversation?.type != "GROUP" ?? false,
                child: Expanded(
                  flex: 1,
                  child: GestureDetector(
                    onTap: () {
                      User currentUser = Get.find<LocalDataManager>().currentUser;
                      int index = state.conversation.config.shortUsers
                          .indexWhere((element) => element.id != currentUser.id);
                      NavigatorUtils.navigateToProfilePage(state.conversation.config.shortUsers[index].id);
                    },
                    child: Container(
                      child: PanelItem(
                          image: AppImages.icGroupProfile,
                          text: "Trang cá nhân"),
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: state.conversation?.type == "GROUP" ?? false,
                child: Expanded(
                  flex: 1,
                  child: GestureDetector(
                    onTap: () {
                      List<ShortUserEntity> shortUserEntities = state.conversation?.config?.shortUsers ?? [];
                      NavigatorUtils.navigateToEditConversationPage(
                          conversation: state.conversation,
                          members: shortUserEntities,
                          callback:  (Conversation entity){
                            if(entity != null){
                              controller.updateMemberConversation(entity);
                              /// cho danh sách conversation cập nhật lại
                              GlobalEvent.instance.onUpdateConversationResponse.add(entity);
                              /// Thông báo thêm thành công
                              Fluttertoast.showToast(
                                  msg: "Thêm thành viên thành công!",
                                  toastLength: Toast.LENGTH_SHORT,
                                  backgroundColor: AppColors.black,
                                  textColor: AppColors.white,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 1);
                            }
                          }
                      );
                    },
                    child: Container(
                      child: PanelItem(
                          image: AppImages.icGroupAddFriend,
                          text: "Thêm thành viên"),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      return Container();
    });
  }

  Widget _buildLibrary() {
    return Container(
      width: Get.mediaQuery.size.width,
      color: Colors.white,
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      child: Obx(() {
        if (state.fetchConversationStatus == LoadStatus.loading)
          return RecommendImageLoadingWidget();
        else if (state.fetchConversationStatus == LoadStatus.success) if (state
            .recommendImages.isNotEmpty)
          return Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset(
                AppImages.icGroupLibrary,
                width: 18.47,
                height: 14.8,
                color: AppColors.red,
              ),
              SizedBox(width: 13),
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      children: [
                        Text(
                          "Thư viện hình ảnh",
                          style: TextStyle(
                              fontFamily: 'Quicksand',
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              color: AppColors.black),
                        ),
                        SizedBox(width: 5),
                        Text(
                          "(${(state.recommendImages.length ?? 1) - 1})",
                          style: TextStyle(
                              fontFamily: 'Quicksand',
                              fontSize: 14,
                              fontWeight: FontWeight.normal,
                              color: AppColors.black),
                        )
                      ],
                    ),
                    Text(
                      "Xem ảnh, link, file đã chia sẻ",
                      style: TextStyle(
                          fontFamily: 'Quicksand',
                          fontSize: 12,
                          fontWeight: FontWeight.normal,
                          color: AppColors.black),
                    ),
                    SizedBox(height: 10),
                    Flexible(
                      child: GestureDetector(
                        onTap: () {
                          NavigatorUtils.navigateToImageFileSummaryPage(
                              conversation: state.conversation);
                        },
                        child: Container(
                          height: 50,
                          child: ListView.separated(
                            scrollDirection: Axis.horizontal,
                            itemCount: state.recommendImages.length,
                            separatorBuilder:
                                (BuildContext context, int index) =>
                                    SizedBox(width: 10),
                            itemBuilder: (_, int index) => Container(
                              height: 50,
                              child: RoundGroupImage(
                                imageUrl: state.recommendImages[index],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          );
        else
          return GestureDetector(
            onTap: () {
              NavigatorUtils.navigateToImageFileSummaryPage(
                  conversation: state.conversation);
            },
            child: GestureDetector(child: RecommendImageEmptyWidget()),
          );
        else
          return Container();
      }),
    );
  }

  Widget _buildCreateGroup() {
    return Obx(() {
      if (state.fetchConversationStatus == LoadStatus.success)
        return Visibility(
          visible: state.conversation.type == "PEER",
          child: GestureDetector(
            onTap: (){
              User currentUser = Get.find<LocalDataManager>().currentUser;
              int index = state.conversation.config.shortUsers
                  .indexWhere((element) => element.id != currentUser.id);

              List<ShortUserEntity> shortUserEntities = []..add(state.conversation.config.shortUsers[index]);
              NavigatorUtils.navigateToCreateConversationWidget(
                  members: shortUserEntities,
                  callback:  (ConversationEntity entity){
                    GlobalEvent.instance.onCreateConversationResponse
                        .add(entity);
                    Fluttertoast.showToast(
                        msg: "Tạo mới cuộc trò chuyện thành công!",
                        toastLength: Toast.LENGTH_SHORT,
                        backgroundColor: AppColors.black,
                        textColor: AppColors.white,
                        gravity: ToastGravity.CENTER,
                        timeInSecForIosWeb: 1);
                  }
              );
            },
            child: Column(
              children: [
                Container(
                  color: AppColors.white,
                  width: Get.mediaQuery.size.width,
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Image.asset(
                        AppImages.icGroupAddFriend,
                        width: 18.37,
                        height: 14.8,
                        color: AppColors.red,
                      ),
                      SizedBox(width: 13),
                      Text(
                        "Tạo nhóm với ${state.conversation.name}",
                        style: TextStyle(
                            fontFamily: 'Quicksand',
                            fontSize: 14,
                            fontWeight: FontWeight.normal,
                            color: AppColors.black),
                      )
                    ],
                  ),
                ),
                _buildSeperate(verticalMargin: !state.hideConversation ? 5 : 0),
              ],
            ),
          ),
        );
      return Container();
    });
  }

  Widget _buildPinMessage() {
    return Obx(() {
      return Visibility(
        visible: !state.hideConversation,
        child: Column(
          children: [
            Container(
              width: Get.mediaQuery.size.width,
              color: AppColors.white,
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    AppImages.icPinMessage,
                    width: 22.34,
                    height: 16.26,
                    color: AppColors.red,
                  ),
                  SizedBox(width: 13),
                  Text(
                    "Ghim trò chuyện",
                    style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 14,
                        fontWeight: FontWeight.normal,
                        color: AppColors.black),
                  ),
                  Spacer(),
                  Container(
                    height: 18,
                    color: AppColors.white,
                    child: Transform.scale(
                      scale: 0.8,
                      child: CupertinoSwitch(
                        value: state.pinConversation ?? false,
                        activeColor: AppColors.red,
                        onChanged: (bool value) {
                          controller.updatePinConversationState(value);
                          if (value)
                            GlobalEvent.instance.onPinnedConversationFromSetting
                                .add(state.conversation);
                          else
                            GlobalEvent
                                .instance.onUnpinnedConversationFromSetting
                                .add(state.conversation);
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
            _buildSeperate(verticalMargin: 5),
          ],
        ),
      );
    });
  }

  Widget _buildHideConversation() {
    return Container(
      color: AppColors.white,
      width: Get.mediaQuery.size.width,
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      child: Obx(() {
        return Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              AppImages.icHideConversation,
              width: 22.34,
              height: 16.26,
              color: AppColors.red,
            ),
            SizedBox(width: 13),
            Text(
              "Ẩn trò chuyện",
              style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 14,
                  fontWeight: FontWeight.normal,
                  color: AppColors.black),
            ),
            Spacer(),
            Container(
              height: 18,
              color: AppColors.white,
              child: Transform.scale(
                scale: 0.8,
                child: CupertinoSwitch(
                  value: state.hideConversation,
                  activeColor: AppColors.red,
                  onChanged: (bool value) async {
                    /// bật switch button
                    if (value) {
                      if (state.conversation.pin?.isEmpty ?? true)
                        _showCreatePINCodeDialog();
                      else
                        _goToEditPinCodeScreen();
                    } else
                      _showEnableConversationDialog();
                  },
                ),
              ),
            ),
          ],
        );
      }),
    );
  }

  Widget _buildClearHistory() {
    return Container(
      color: AppColors.white,
      width: Get.mediaQuery.size.width,
      padding: EdgeInsets.symmetric(vertical: 10),
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(
            AppImages.icGroupHistory,
            width: 22.34,
            height: 16.26,
            color: AppColors.red,
          ),
          SizedBox(width: 13),
          Text(
            "Xoá lịch sử trò chuyện",
            style: TextStyle(
                fontFamily: 'Quicksand',
                fontSize: 14,
                fontWeight: FontWeight.normal,
                color: AppColors.black),
          )
        ],
      ),
    );
  }

  Widget _buildChangeBackground() {
    return GestureDetector(
      onTap: _showChangeBgMenu,
      child: Container(
        color: AppColors.white,
        width: Get.mediaQuery.size.width,
        padding: EdgeInsets.symmetric(vertical: 10),
        margin: EdgeInsets.symmetric(horizontal: 20),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.asset(
              AppImages.icChangeImage,
              width: 22.34,
              height: 16.26,
              color: AppColors.red,
            ),
            SizedBox(width: 13),
            Text(
              "Đổi hình nền",
              style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 14,
                  fontWeight: FontWeight.normal,
                  color: AppColors.black),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildOffNotification() {
    return Container(
      color: AppColors.white,
      width: Get.mediaQuery.size.width,
      padding: EdgeInsets.symmetric(vertical: 10),
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(
            AppImages.icGroupNotification,
            width: 22.34,
            height: 16.26,
            color: AppColors.red,
          ),
          SizedBox(width: 13),
          Text(
            "Tắt thông báo",
            style: TextStyle(
                fontFamily: 'Quicksand',
                fontSize: 14,
                fontWeight: FontWeight.normal,
                color: AppColors.black),
          )
        ],
      ),
    );
  }

  Widget _buildLeaveGroup() {
    return Obx(() {
      if (state.fetchConversationStatus == LoadStatus.success)
        return GestureDetector(
          onTap: (){
            showConfirmLeaveDialog(state.conversation);
          },
          child: Visibility(
            visible: state.conversation.type == "GROUP",
            child: Column(
              children: [
                Container(
                  color: AppColors.white,
                  width: Get.mediaQuery.size.width,
                  padding: EdgeInsets.symmetric(vertical: 10),
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Image.asset(
                        AppImages.icGroupLeave,
                        width: 22.34,
                        height: 16.26,
                        color: AppColors.red,
                      ),
                      SizedBox(width: 13),
                      Text(
                        "Rời khỏi nhóm",
                        style: TextStyle(
                            fontFamily: 'Quicksand',
                            fontSize: 14,
                            fontWeight: FontWeight.normal,
                            color: AppColors.black),
                      )
                    ],
                  ),
                ),
                _buildSeperate(verticalMargin: 5),
              ],
            ),
          ),
        );
      return Container();
    });
  }

  void showConfirmLeaveDialog(Conversation conversation) {
    showCupertinoDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          content: Text(
            S.of(context).leave_group_title("${conversation.name}"),
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 18,
              color: AppColors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          actions: [
            CupertinoDialogAction(
              child: Text(
                S.of(context).cancel_input,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 16,
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            CupertinoDialogAction(
                child: Text(
                  S.of(context).ok,
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 16,
                    color: AppColors.red,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                onPressed: () {
                  controller.leaveConversation(conversation.uuid);
                }),
          ],
        );
      },
    );
  }

  Widget _buildMembers() {
    return Obx(() {
      if (state.fetchConversationStatus == LoadStatus.success)
        return Visibility(
          visible: state.conversation.type == "GROUP",
          child: GestureDetector(
            onTap: (){
              NavigatorUtils.navigateToChatGroupMembersPage(state.conversation ,state.conversation?.config?.shortUsers ?? []);
            },
            child: Column(
              children: [
                Container(
                  color: AppColors.white,
                  width: Get.mediaQuery.size.width,
                  padding: EdgeInsets.symmetric(vertical: 10),
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Image.asset(
                        AppImages.icGroupMembers,
                        width: 22.34,
                        height: 16.26,
                        color: AppColors.red,
                      ),
                      SizedBox(width: 13),
                      Text(
                        "Thành viên",
                        style: TextStyle(
                            fontFamily: 'Quicksand',
                            fontSize: 14,
                            fontWeight: FontWeight.normal,
                            color: AppColors.black),
                      )
                    ],
                  ),
                ),
                _buildSeperate(verticalMargin: 5),
              ],
            ),
          ),
        );
      return Container();
    });
  }

  Widget _buildSeperate({double verticalMargin}) {
    return Container(
      width: Get.mediaQuery.size.width,
      color: Colors.white,
      padding: EdgeInsets.symmetric(vertical: verticalMargin ?? 10),
      child: Container(
        width: Get.mediaQuery.size.width,
        height: 2,
        color: Color(0xffEDEDED),
      ),
    );
  }

  void _showCreatePINCodeDialog() {
    showCupertinoDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text(
            S.of(context).create_pin_code_title,
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 18,
              color: AppColors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          content: Text(
            S.of(context).create_pin_code_content,
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 14,
              color: AppColors.black,
              fontWeight: FontWeight.normal,
            ),
          ),
          actions: [
            CupertinoDialogAction(
              child: Text(
                S.of(context).cancel_input,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 16,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            CupertinoDialogAction(
                child: Text(
                  S.of(context).create_pin_code,
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 16,
                    color: AppColors.red,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                  _goToCreatePinCodeScreen();
                }),
          ],
        );
      },
    );
  }

  void _goToEditPinCodeScreen() {
    Navigator.push(
      context,
      PageRouteBuilder(
          opaque: false,
          pageBuilder: (context, animation, secondaryAnimation) {
            return PasscodeScreen(
              title: Text(
                S.of(context).des_pin_code_1,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: 'Quicksand', color: Colors.black, fontSize: 14),
              ),
              isValidCallback: (String enteredPasscode) async {
                /// bật switch button
                bool success = await controller.hideConversation();
                if (success) {
                  GlobalEvent.instance.onHideConversationFromSetting
                      .add(state.conversation);
                  Fluttertoast.showToast(
                      msg: "Ẩn trò chuyện thành công",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.CENTER,
                      backgroundColor: AppColors.black,
                      textColor: AppColors.white,
                      timeInSecForIosWeb: 1);
                } else
                  Fluttertoast.showToast(
                      msg: "Ẩn cuộc trò chuyện thất bại!",
                      toastLength: Toast.LENGTH_SHORT,
                      backgroundColor: AppColors.black,
                      textColor: AppColors.white,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIosWeb: 1);
              },
              passwordEnteredCallback: (String enteredPasscode) async {
                bool isValid = enteredPasscode == state.conversation.pin;
                _verificationNotifier.add(isValid);
                if (!isValid)
                  Fluttertoast.showToast(
                      msg: "Mã PIN không đúng, vui lòng thử lại.",
                      toastLength: Toast.LENGTH_SHORT,
                      backgroundColor: AppColors.black,
                      textColor: AppColors.white,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIosWeb: 1);
              },
              cancelButton: Text(
                S.of(context).cancel_input,
                style: const TextStyle(fontSize: 16, color: Colors.black),
                semanticsLabel: S.of(context).cancel_input,
              ),
              deleteButton: Image.asset(
                AppImages.icDeletePinDigit,
                width: 40,
                height: 30,
              ),
              circleUIConfig: CircleUIConfig(
                  borderColor: AppColors.red,
                  fillColor: AppColors.red,
                  borderWidth: 2,
                  circleSize: 15),
              keyboardUIConfig: KeyboardUIConfig(
                digitBorderWidth: 2,
                primaryColor: Colors.white,
                digitFillColor: Colors.white,
                digitTextStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 28,
                    fontWeight: FontWeight.normal),
              ),
              shouldTriggerVerification: _verificationNotifier.stream,
              backgroundColor: AppColors.greyBg,
              cancelCallback: _onPasscodeCancelled,
              passwordDigits: 4,
              bottomWidget: _buildPasscodeRestoreButton(),
              headerIcon: _editStepType == EditPINStepType.NONE
                  ? Container(
                      margin: EdgeInsets.symmetric(vertical: 20),
                      child: Icon(
                        Icons.lock,
                        color: Colors.black,
                        size: 40,
                      ),
                    )
                  : Container(),
            );
          }),
    );
  }

  void _showEnableConversationDialog() {
    showCupertinoDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          content: Text(
            "Hiện lại trò chuyện của Nhóm ăn chơi?",
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 14,
              color: AppColors.black,
              fontWeight: FontWeight.normal,
            ),
          ),
          actions: [
            CupertinoDialogAction(
              child: Text(
                S.of(context).cancel_input,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 16,
                  color: AppColors.red,
                  fontWeight: FontWeight.bold,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            CupertinoDialogAction(
                child: Text(
                  "Hiện",
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 16,
                    color: AppColors.black,
                    fontWeight: FontWeight.normal,
                  ),
                ),
                onPressed: () async {
                  /// hiện cuộc trò chuyện
                  bool success = await controller.visibleConversation();
                  if (success) {
                    GlobalEvent.instance.onVisibleConversationFromSetting
                        .add(state.conversation);
                    Navigator.of(context).pop();
                  } else
                    Fluttertoast.showToast(
                        msg: "Không thể hiển thị cuộc trò chuyện!",
                        toastLength: Toast.LENGTH_SHORT,
                        backgroundColor: AppColors.black,
                        textColor: AppColors.white,
                        gravity: ToastGravity.CENTER,
                        timeInSecForIosWeb: 1);
                }),
          ],
        );
      },
    );
  }

  void _goToCreatePinCodeScreen(
      {String oldPinCode = "", CreatePINStepType stepType}) {
    if (stepType == null) stepType = CreatePINStepType.INPUT_NEW_PIN_CODE;
    Navigator.push(
      context,
      PageRouteBuilder(
          opaque: false,
          pageBuilder: (context, animation, secondaryAnimation) {
            return PasscodeScreen(
              title: Text(
                stepType.title,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: 'Quicksand',
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
              isValidCallback: (String enteredPasscode) async {
                switch (stepType) {
                  case CreatePINStepType.INPUT_NEW_PIN_CODE:
                    Navigator.pop(context);
                    _goToCreatePinCodeScreen(
                        oldPinCode: enteredPasscode,
                        stepType: CreatePINStepType.REINPUT_NEW_PIN_CODE);
                    break;
                  case CreatePINStepType.REINPUT_NEW_PIN_CODE:
                    bool success = await controller.createPINCode(
                        enteredPasscode, state.conversation.uuid);
                    if (success) {
                      GlobalEvent.instance.onHideConversationFromSetting
                          .add(state.conversation);
                      _showSuccessDialog();
                    } else
                      Fluttertoast.showToast(
                          msg: "Tạo mã PIN thất bại!",
                          toastLength: Toast.LENGTH_SHORT,
                          backgroundColor: AppColors.black,
                          textColor: AppColors.white,
                          gravity: ToastGravity.CENTER,
                          timeInSecForIosWeb: 1);
                    break;
                }
              },
              passwordEnteredCallback: (String enteredPasscode) async {
                bool isValid = oldPinCode.isEmpty ||
                    (oldPinCode.isNotEmpty && enteredPasscode == oldPinCode);
                _verificationNotifierCreate.add(isValid);
                if (!isValid)
                  Fluttertoast.showToast(
                      msg: "Mã PIN không đúng, vui lòng thử lại.",
                      toastLength: Toast.LENGTH_SHORT,
                      backgroundColor: AppColors.black,
                      textColor: AppColors.white,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIosWeb: 1);
              },
              cancelButton: Text(
                S.of(context).cancel_input,
                style: const TextStyle(fontSize: 16, color: Colors.black),
                semanticsLabel: S.of(context).cancel_input,
              ),
              deleteButton: Image.asset(
                AppImages.icDeletePinDigit,
                width: 40,
                height: 30,
              ),
              circleUIConfig: CircleUIConfig(
                  borderColor: AppColors.red,
                  fillColor: AppColors.red,
                  borderWidth: 2,
                  circleSize: 15),
              keyboardUIConfig: KeyboardUIConfig(
                digitBorderWidth: 2,
                primaryColor: Colors.white,
                digitFillColor: Colors.white,
                digitTextStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 28,
                    fontWeight: FontWeight.normal),
              ),
              shouldTriggerVerification: _verificationNotifierCreate.stream,
              backgroundColor: AppColors.greyBg,
              cancelCallback: _onCreatePasscodeCancelled,
              passwordDigits: 4,
              bottomWidget: Container(
                margin: EdgeInsets.symmetric(horizontal: 50),
                child: Text(
                  stepType.content,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                      fontFamily: 'Quicksand',
                      fontSize: 13,
                      color: Colors.black,
                      fontWeight: FontWeight.normal),
                ),
              ),
            );
          }),
    );
  }

  _onCreatePasscodeCancelled() {
    Navigator.maybePop(context);
  }

  _showSuccessDialog() {
    showCupertinoDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text(
            S.of(context).setup_PIN_code_title,
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 16,
              color: AppColors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          content: Text(
            S.of(context).setup_PIN_code_content,
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 14,
              color: AppColors.black,
              fontWeight: FontWeight.normal,
            ),
          ),
          actions: [
            CupertinoDialogAction(
                child: Text(
                  S.of(context).close,
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 16,
                    color: AppColors.red,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                }),
          ],
        );
      },
    );
  }

  _onPasscodeCancelled() {
    Navigator.maybePop(context);
  }

  _buildPasscodeRestoreButton() => Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                S.of(context).des_pin_code_2,
                textAlign: TextAlign.center,
                style: const TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 13,
                    color: Colors.black,
                    fontWeight: FontWeight.normal),
              ),
              SizedBox(height: 1),
              Flexible(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Flexible(
                      child: Text(
                        S.of(context).des_pin_code_3,
                        textAlign: TextAlign.start,
                        style: const TextStyle(
                            fontFamily: 'Quicksand',
                            fontSize: 13,
                            color: Colors.black,
                            fontWeight: FontWeight.normal),
                      ),
                    ),
                    GestureDetector(
                      child: Text(
                        S.of(context).des_pin_code_4,
                        textAlign: TextAlign.start,
                        style: const TextStyle(
                            fontFamily: 'Quicksand',
                            fontSize: 13,
                            color: AppColors.red,
                            fontWeight: FontWeight.bold),
                      ),
                      onTap: _resetAppPassword,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );

  _resetAppPassword() {
    NavigatorUtils.navigateToSettingPinCodeScreen(uuid);
  }

  // ChatItemBottomSheet
  void _showChangeBgMenu() {
    Get.back();
    onTapChangeBackground("background ${state.conversation.name}");
  }

  void _onChooseImage() {
    _mediaActionSheetWidget.showActionSheet(S.of(context).chooseImage, [
      S.of(context).takePhoto,
      S.of(context).chooseFromGrallery
    ], (int index) {
      if (index == 0) {
        _openCamera((pickedFile) async {
          if (pickedFile.path != null) {
            String imgUrl = await controller.uploadAvatar(pickedFile.path);
            if (imgUrl?.isNotEmpty ?? false)
              Fluttertoast.showToast(
                  msg: "Cập nhật ảnh đại diện thành công",
                  toastLength: Toast.LENGTH_LONG);
            else
              Fluttertoast.showToast(
                  msg: "Cập nhật ảnh đại diện thất bại",
                  toastLength: Toast.LENGTH_LONG);
          }
        });
      }
      if (index == 1) {
        _openGallery((pickedFile) async {
          if (pickedFile.path != null) {
            String imgUrl = await controller.uploadAvatar(pickedFile.path);
            if (imgUrl?.isNotEmpty ?? false)
              Fluttertoast.showToast(
                  msg: "Cập nhật ảnh đại diện thành công",
                  toastLength: Toast.LENGTH_LONG);
            else
              Fluttertoast.showToast(
                  msg: "Cập nhật ảnh đại diện thất bại",
                  toastLength: Toast.LENGTH_LONG);
          }
        });
      }
    });
  }

  MediaActionSheetWidget _mediaActionSheetWidget =
      MediaActionSheetWidget(context: Get.context);

  void _openCamera(Function onPickImageCallback) {
    _mediaActionSheetWidget.pickImageDialog(ImageSource.camera,
        (PickedFile pickedFile) {
      print("PickerFile:${pickedFile.path}");
      onPickImageCallback(pickedFile);
    });
  }

  void _openGallery(Function onPickImageCallback) {
    _mediaActionSheetWidget.pickImageDialog(ImageSource.gallery,
        (PickedFile pickedFile) {
      print("PickerFile:${pickedFile.path}");
      onPickImageCallback(pickedFile);
    });
  }

  void _showChangeNameDialog() {
    showDialog(
      context: Get.context,
      builder: (BuildContext context) => ChangeGroupNameWidget(
          text: state.conversation.name,
          onTapSave: _handleChangeGroupNameEvent),
    );
  }

  void _handleChangeGroupNameEvent(String text) {
    controller.changeGroupName(text);
  }
}
