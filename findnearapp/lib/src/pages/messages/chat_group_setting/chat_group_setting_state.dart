import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/message_chat/entities/message_chat.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:get/get.dart';

class ChatGroupSettingState extends BaseState{
  RxBool hideConversationRx = false.obs;
  RxBool hasPINCodeRx = false.obs;
  RxBool pinConversationRx = false.obs;
  Conversation conversation;
  Rx<LoadStatus> fetchConversationStatus = LoadStatus.initial.obs;
  Rx<LoadStatus> createPinCodeStatus = LoadStatus.initial.obs;
  Rx<LoadStatus> uploadAvatarStatus = LoadStatus.initial.obs;
  Rx<LoadStatus> changeGroupNameStatus = LoadStatus.initial.obs;
  // Rx<LoadStatus> fetchRecommendImageStatus = LoadStatus.initial.obs;
  List<String> recommendImages = [];

  ChatGroupSettingState() {
    ///Initialize variables
  }

  /// hide conversation status
  bool get hideConversation => hideConversationRx.value;
  set hideConversation(bool value) => hideConversationRx.value = value;

  /// has PIN code status
  bool get hasPINCode => hasPINCodeRx.value;
  set hasPINCode(bool value) => hasPINCodeRx.value = value;

  /// pin conversation status
  bool get pinConversation => pinConversationRx.value;
  set pinConversation(bool value) => pinConversationRx.value = value;

}
