import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/media.dart';
import 'package:flutter/cupertino.dart';

class PanelItem extends StatelessWidget {
  String image;
  String text;

  PanelItem({this.image, this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            image,
            width: 17,
            height: 17,
            color: AppColors.red,
          ),
          SizedBox(height: 5),
          Container(
            child: Text(
              text,
              style: TextStyle(
                fontFamily: 'Quicksand',
                color: AppColors.black,
                fontSize: 12,
                fontWeight: FontWeight.normal,
              ),
              maxLines: 2,
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }

}
