import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:get/get.dart';

class ChatGroupMembersState extends BaseState{

  Conversation conversation;
  List<ShortUserEntity> shortUsers;
  Rx<LoadStatus> updateConversationRx = LoadStatus.initial.obs;

  ChatGroupMembersState() {
    ///Initialize variables
  }
}
