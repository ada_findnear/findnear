import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:get/get.dart';

import 'chat_group_members_state.dart';

class ChatGroupMembersLogic extends BaseController<ChatGroupMembersState> {

  @override
  ChatGroupMembersState createState() => ChatGroupMembersState();

  void updateConversation(Conversation conversation, List<ShortUserEntity> shortUsers){
    state.updateConversationRx.value = LoadStatus.loading;
    try {
      state.conversation = conversation;
      state.shortUsers = []..addAll(shortUsers);
      state.updateConversationRx.value = LoadStatus.success;
    } catch (e){
      state.updateConversationRx.value = LoadStatus.failure;
    }
  }
}
