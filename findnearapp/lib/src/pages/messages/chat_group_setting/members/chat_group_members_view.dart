import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/global/global_event.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

import 'chat_group_members_logic.dart';
import 'chat_group_members_state.dart';
import 'item/chat_group_members_item.dart';

class ChatGroupMembersPage extends StatefulWidget {
  static const String ROUTE_NAME = '/ChatGroupMembersPage';
  final RouteArgument routeArgument;

  ChatGroupMembersPage({this.routeArgument});

  @override
  State<ChatGroupMembersPage> createState() => _ChatGroupMembersPageState();
}

class _ChatGroupMembersPageState extends BaseWidgetState<ChatGroupMembersPage,
    ChatGroupMembersLogic,
    ChatGroupMembersState> {

  List<ShortUserEntity> get users => widget.routeArgument.param["members"];

  Conversation get conversation => widget.routeArgument.param["conversation"];

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.updateConversation(conversation, users);
    });
  }

  @override
  ChatGroupMembersLogic createController() => ChatGroupMembersLogic();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.white,
        title: Text(
          S.of(Get.context)
              .members_list_title,
          style: TextStyle(
            fontFamily: 'Quicksand',
            fontSize: 16,
            color: AppColors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
        child: Obx(() {
          if (state.updateConversationRx.value != LoadStatus.success)
            return Container();
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () {
                  NavigatorUtils.navigateToEditConversationPage(
                      conversation: state.conversation,
                      members: state.shortUsers,
                      callback: (Conversation entity) {
                        controller.updateConversation(entity, entity.config.shortUsers);

                        /// cho danh sách conversation cập nhật lại
                        GlobalEvent.instance.onUpdateConversationResponse.add(
                            entity);

                        /// Thông báo thêm thành công
                        Fluttertoast.showToast(
                            msg: "Thêm thành viên thành công!",
                            toastLength: Toast.LENGTH_SHORT,
                            backgroundColor: AppColors.black,
                            textColor: AppColors.white,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIosWeb: 1);
                      }
                  );
                },
                child: _buildAddMemberButton(),
              ),
              SizedBox(height: 10),
              Text(
                "Danh sách thành viên (${state.shortUsers.length})",
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 14,
                  color: const Color(0xff404040),
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 10),
              Expanded(
                child: ListView.separated(
                  scrollDirection: Axis.vertical,
                  itemCount: state.shortUsers.length,
                  separatorBuilder: (BuildContext context, int index) =>
                      SizedBox(height: 10),
                  itemBuilder: (_, int index) =>
                      ChatGroupMembersItem(
                        user: state.shortUsers[index],
                        isOwner: state.shortUsers[index].code == conversation.createBy
                      ),
                ),
              ),
            ],
          );
        }),
      ),
    );
  }

  Widget _buildAddMemberButton() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8),
      padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
      decoration: BoxDecoration(
          color: Color(0xffFFDBDB), borderRadius: BorderRadius.circular(24)),
      // #FFDBDB
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(AppImages.icGroupAddFriend2, width: 16,
              height: 17,
              color: const Color(0xffFF0000)),
          SizedBox(width: 8),
          Text(
            "Thêm thành viên",
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 14,
              color: const Color(0xffFF0000),
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }

}
