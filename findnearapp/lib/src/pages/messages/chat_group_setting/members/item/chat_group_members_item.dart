import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/CircularAvatarWidget.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/members/circle_avatar/member_circle_avatar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ChatGroupMembersItem extends StatelessWidget{
  ShortUserEntity user;
  bool isOwner;
  ChatGroupMembersItem({this.user, this.isOwner = false});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        _buildAvatar(),
        SizedBox(width: 12),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                  user.name,
                  overflow: TextOverflow.fade,
                  softWrap: false,
                  style: TextStyle(
                    fontFamily: "Quicksand",
                    fontSize: 14,
                    color: AppColors.black,
                    fontWeight: FontWeight.bold,
                  )
              ),
              Visibility(
                visible: isOwner,
                child: Text(
                    "Trưởng nhóm",
                    overflow: TextOverflow.fade,
                    softWrap: false,
                    style: TextStyle(
                      fontFamily: "Quicksand",
                      fontSize: 12,
                      color: AppColors.gray,
                      fontWeight: FontWeight.bold,
                    )
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildAvatar(){
    return Stack(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          child: MemberCircleAvatarWidget(
            user: user,
            size: 42.w,
            hasBorder: true,
          ),
        ),
        Positioned.fill(
          child: Visibility(
            visible: isOwner,
            child: Align(
              alignment: Alignment.bottomRight,
              child: Icon(Icons.stars_rounded, size: 20, color: AppColors.red),
            ),
          ),
        ),
      ],
    );
  }

}