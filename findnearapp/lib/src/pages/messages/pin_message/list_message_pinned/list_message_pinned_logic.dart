import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/base/base_state.dart';
import 'package:get/get.dart';

import 'list_message_pinned_state.dart';

class ListMessagePinnedLogic extends BaseController<ListMessagePinnedState> {

  @override
  ListMessagePinnedState createState() => ListMessagePinnedState();
}
