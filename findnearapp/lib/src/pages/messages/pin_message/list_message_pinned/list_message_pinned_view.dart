import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'item/message_pinned_item.dart';
import 'list_message_pinned_logic.dart';
import 'list_message_pinned_state.dart';

class ListMessagePinnedPage extends StatefulWidget {
  static const String ROUTE_NAME = '/ListMessagePinnedPage';
  final RouteArgument routeArgument;

  ListMessagePinnedPage({this.routeArgument});

  @override
  State<ListMessagePinnedPage> createState() => _ListMessagePinnedState();
}

class _ListMessagePinnedState extends BaseWidgetState<ListMessagePinnedPage,
    ListMessagePinnedLogic, ListMessagePinnedState> {
  List<MessagePinnedPresenter> pinnedMessage = [
    MessagePinnedPresenter(
      imageUrl:
          "http://cdn.resfu.com/media/img_news/agencia-efe_multimedia_4662042.multimedia.photos.18000784.file.jpg?size=1000x&ext=jpeg",
      name: "Paul Pogba",
      time: "11:23",
      messageOwner: "Nguyễn Duy Tấn Phát",
      messagePinned:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text",
    ),
    MessagePinnedPresenter(
      imageUrl:
          "https://danviet.mediacdn.vn/thumb_w/650/2021/3/5/11-16148991382031073501199.jpg",
      name: "Bruno Fesnandes",
      time: "01:13",
      messageOwner: "Nguyễn Duy Tấn Phát",
      messagePinned: "Lorem Ipsum is simply dummy ",
    ),
    MessagePinnedPresenter(
      imageUrl:
          "https://eu-images.contentstack.com/v3/assets/bltcc7a7ffd2fbf71f5/bltaa4a151617523afa/60db3fdffd14d30f3eac7482/abcff5621f0aafe8449ea6367c6c4067109451f5.jpg?auto=webp&fit=crop&format=jpg&quality=100",
      name: "Fred",
      time: "21:00",
      messageOwner: "Nguyễn Duy Tấn Phát",
      messagePinned:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text",
    ),
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  ListMessagePinnedLogic createController() => ListMessagePinnedLogic();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.seperateColor,
      appBar: AppBar(
        backgroundColor: AppColors.white,
        title: Text(
          S.of(context).pinned_message_list,
          style: TextStyle(
            fontFamily: 'Quicksand',
            fontSize: 16,
            color: AppColors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 10),
        child: ListView.separated(
          scrollDirection: Axis.vertical,
          itemCount: pinnedMessage.length,
          separatorBuilder: (BuildContext context, int index) => Container(
            height: 7,
            color: AppColors.seperateColor,
          ),
          itemBuilder: (_, int index) => Container(
            color: AppColors.white,
            padding: EdgeInsets.symmetric(horizontal: 12),
            child: MessagePinnedItem(
              messagePresenter: pinnedMessage[index],
              onTapMenu: _onTapMenu,
            ),
          ),
        ),
      ),
    );
  }

  void _onTapMenu() {
    _showChatMenu();
  }

  void _showChatMenu() async {
    final result = await Get.bottomSheet(
      Container(
        padding: EdgeInsets.only(top: 10, bottom: Get.bottomBarHeight),
        decoration: BoxDecoration(
            color: AppColors.white,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(20),
              topLeft: Radius.circular(20),
            )),
        child: Stack(
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    Get.back();
                  },
                  child: Container(
                    color: AppColors.white,
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.only(top: 10, left: 10, right: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset(
                          AppImages.icPinnedMessage,
                          width: 22,
                          height: 13,
                        ),
                        SizedBox(width: 14),
                        Text(
                          S.of(context).show_og_message,
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1
                              .copyWith(fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  color: AppColors.white,
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Center(
                    child: Container(
                      height: 1,
                      width: Get.mediaQuery.size.width,
                      color: AppColors.seperateColor,
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Get.back();
                  },
                  child: Container(
                    color: AppColors.white,
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.only(left: 10, right: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset(
                          AppImages.icGroupUnpin,
                          width: 22,
                          height: 22,
                        ),
                        SizedBox(width: 14),
                        Text(
                          S.of(context).unpin_message,
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1
                              .copyWith(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  height: 20,
                  color: AppColors.white,
                ),
              ],
            ),
            Positioned(
                top: 5,
                right: 15,
                child: GestureDetector(
                  onTap: (){
                    Get.back();
                  },
                  child: Image.asset(AppImages.icClosePinMessage,
                      width: 28.28, height: 28.28),
                ))
          ],
        ),
      ),
    );
  }
}
