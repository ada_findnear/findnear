import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/CircularAvatarWidget.dart';
import 'package:findnear/src/models/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MessagePinnedItem extends StatelessWidget {
  Function onTapMenu;
  MessagePinnedPresenter messagePresenter;

  MessagePinnedItem({this.messagePresenter, this.onTapMenu});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 9),
        _buildHeader(),
        SizedBox(height: 15),
        _buildFooter(),
        SizedBox(height: 14),
      ],
    );
  }

  Widget _buildHeader() {
    return Row(
      children: [
        _buildAvatar(),
        SizedBox(width: 10),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text(
                    messagePresenter.name.toUpperCase(),
                    overflow: TextOverflow.fade,
                    softWrap: false,
                    style: TextStyle(
                      fontFamily: "Quicksand",
                      fontSize: 14,
                      color: AppColors.black,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(width: 10),
                  Text(messagePresenter.time,
                      overflow: TextOverflow.fade,
                      softWrap: false,
                      style: TextStyle(
                        fontFamily: "Quicksand",
                        fontSize: 12,
                        color: AppColors.black,
                        fontWeight: FontWeight.normal,
                      )),
                ],
              ),
              SizedBox(height: 6),
              Text(
                "Đã ghim 1 tin nhắn",
                overflow: TextOverflow.fade,
                softWrap: false,
                style: TextStyle(
                  fontFamily: "Quicksand",
                  fontSize: 12,
                  color: AppColors.black,
                  fontWeight: FontWeight.normal,
                ),
              ),
            ],
          ),
        ),
        GestureDetector(
          onTap: (){
            onTapMenu();
          },
          child: Container(
            color: AppColors.white,
            padding: EdgeInsets.symmetric(vertical: 8, horizontal: 13),
            child: Image.asset(AppImages.icGroupMenu,
                width: 5, height: 21, color: AppColors.gray),
          ),
        ),
      ],
    );
  }

  Widget _buildFooter() {
    return Container(
      color: Color(0xffFFB5B5),
      padding: EdgeInsets.only(left: 2),
      child: Container(
        color: AppColors.white,
        padding: EdgeInsets.only(left: 13),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              messagePresenter.messageOwner.toUpperCase(),
              overflow: TextOverflow.fade,
              softWrap: false,
              style: TextStyle(
                fontFamily: "Quicksand",
                fontSize: 14,
                color: AppColors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 6),
            Text(
              messagePresenter.messagePinned,
              style: TextStyle(
                fontFamily: "Quicksand",
                fontSize: 14,
                color: AppColors.black,
                fontWeight: FontWeight.normal,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildAvatar() {
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(20)),
      child: CircularAvatarWidget(
        imageUrl: messagePresenter.imageUrl,
        size: 50.w,
        hasBorder: true,
        hasLiveIndicator: false,
      ),
    );
  }
}

class MessagePinnedPresenter {
  String imageUrl;
  String name;
  String time;
  String messageOwner;
  String messagePinned;

  MessagePinnedPresenter({
    this.imageUrl,
    this.name,
    this.time,
    this.messageOwner,
    this.messagePinned,
  });
}
