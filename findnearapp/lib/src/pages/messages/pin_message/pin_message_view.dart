import 'package:findnear/src/commons/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'pin_message_logic.dart';

class PinMessagePage extends StatelessWidget {
  static const String ROUTE_NAME = 'PinMessagePage';
  final logic = Get.put(PinMessageLogic());
  final state = Get.find<PinMessageLogic>().state;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.all(10),
                height: 500,
                color: AppColors.green,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 30),
                height: 50,
                color: AppColors.red,
              ),
              Container(
                margin: EdgeInsets.all(10),
                height: 100,
                color: AppColors.yellow,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
