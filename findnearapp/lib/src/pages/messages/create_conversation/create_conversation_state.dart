import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart' as refresh;

class CreateConversationState extends BaseState{
  RxString pathImage = ''.obs;
  RxString nameGroup = ''.obs;
  RxString keyword = ''.obs;
  RxBool enableDoneButtonRx = false.obs;
  List<User> friends = [];
  List<ShortUserEntity> selectFriends = [];
  Rx<LoadStatus> searchFriendStatusRx = LoadStatus.initial.obs;
  Rx<LoadStatus> selectFriendStatusRx = LoadStatus.initial.obs;
  // loadmore
  final canLoadmore = true.obs;
  final refreshController = refresh.RefreshController();

  // search
  var textSearchController = TextEditingController();
  final keywordSearch = "".obs;

  CreateConversationState() {}

  bool isUserSelected(User user) {
    if(selectFriends.isEmpty ?? true) return false;
    int index = selectFriends.indexWhere((element) => element.id == user.id);
    if(index != -1)
      return true;
    return false;
  }
}
