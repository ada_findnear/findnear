import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/widgets/shimmer/app_shimmer.dart';
import 'package:flutter/material.dart';

class MessagesLoadingWidget extends StatelessWidget {
  const MessagesLoadingWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
      separatorBuilder: (_, __) => Divider(
        height: 10,
        color: AppColors.dividerColor,
        thickness: 1,
      ),
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        return Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                AppShimmer(width: 38, height: 38, cornerRadius: 19),
                SizedBox(width: 12),
                Flexible(
                  child: AppShimmer(width: 150, height: 18, cornerRadius: 9),
                ),
                Spacer(),
                AppShimmer(width: 25, height: 25, cornerRadius: 9),
              ],
            ),
          ],
        );
      },
      itemCount: 20,
    );
  }
}
