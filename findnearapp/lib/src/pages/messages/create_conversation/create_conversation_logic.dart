import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/base/base_refresh_load_more.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/communication/entities/group_config_entity.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/entities/communication/response/conversation_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/media.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/bottom_sheet/change_bg_logic.dart';
import 'package:findnear/src/repository/communication_repository.dart';
import 'package:findnear/src/repository/message_repository.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';

import 'create_conversation_state.dart';

class CreateConversationLogic extends BaseController<CreateConversationState> with RefreshLoadMoreControllerMixin{
  final CreateConversationState state = CreateConversationState();
  CommunicationRepository _repository = CommunicationApi(isSocketApi: false);
  CommunicationRepository _socketRepository = CommunicationApi(isSocketApi: true);
  MessageRepository _messageRepository = MessageApi();

  @override
  CreateConversationState createState() => CreateConversationState();

  void initMemberTicked(List<ShortUserEntity> ShortUserEntities) async {
    state.selectFriends = []..addAll(ShortUserEntities);
    searchFriend(keyword: '', isReload: true);
  }

  void searchFriend({String keyword = '', bool isReload = false}) async {
    var offset = state.friends.length;

    if (isReload) {
      state.searchFriendStatusRx.value = LoadStatus.loading;
      offset = 0;
      state.canLoadmore.value = true;
      state.friends = [];
    }

    if (state.canLoadmore.value != true) return;
    final response = await _repository.searchFriend(
      keyword: keyword,
      currentOffset: offset,
    );

    if (response.isNotEmpty == true) {
      state.friends = state.friends..addAll(response);
      // state.friends.refresh();
    } else {
      state.canLoadmore.value = false;
    }
    state.searchFriendStatusRx.value = LoadStatus.success;
    state.refreshController.loadComplete();
  }

  @override
  void loadList() {
    // TODO: implement loadList
  }

  void selectFriend({User user}) async {
    state.selectFriendStatusRx.value = LoadStatus.loading;
    try {
      if(state.selectFriends.isNotEmpty ?? false){
        int index = state.selectFriends.indexWhere((element) => element.id == user.id);
        if(index == -1)
          state.selectFriends.add(ShortUserEntity.fromUser(user));
      } else
        state.selectFriends.add(ShortUserEntity.fromUser(user));
      state.enableDoneButtonRx.value = state.selectFriends.isNotEmpty;
      state.selectFriendStatusRx.value = LoadStatus.success;
    } catch (e){
      state.selectFriendStatusRx.value = LoadStatus.failure;
    }
  }

  void unSelectFriend({User user}) async {
    state.selectFriendStatusRx.value = LoadStatus.loading;
    try {
      if(state.selectFriends.isNotEmpty ?? false){
        int index = state.selectFriends.indexWhere((element) => element.id == user.id);
        if(index != -1)
          state.selectFriends.removeAt(index);
      }
      state.enableDoneButtonRx.value = state.selectFriends.isNotEmpty;
      state.selectFriendStatusRx.value = LoadStatus.success;
    } catch (e){
      state.selectFriendStatusRx.value = LoadStatus.failure;
    }
  }

  Future<List<ConversationEntity>> findConversationByReceiver(List<ShortUserEntity> listFriend) async {
    User currentUser = Get.find<LocalDataManager>().currentUser;
    List<ConversationEntity> result = await _socketRepository.findConversationByReceiver(receiver: listFriend[0].code, sender: currentUser.code);
    return result;
  }

  Future<ConversationEntity> createGroup({String nameGroup = '', listFriend, String pathImage = ''}) async {
    List<User> users = [];
    List<ShortUserEntity> shortUsers = [];
    Media image = new Media();
    User currentUser = Get.find<LocalDataManager>().currentUser;

    /// thêm bản thân vào danh sách
    users.add(currentUser);
    shortUsers.add(ShortUserEntity.fromUser(currentUser));
    Conversation _conversation = new Conversation(users);

    _conversation.id = UniqueKey().toString();
    _conversation.lastMessageTime = DateTime.now().toUtc().millisecondsSinceEpoch;
    _conversation.name = nameGroup;
    _conversation.readByUsers = [currentUser?.id ?? ""];
    _conversation.idAdmin = currentUser?.id ?? "";
    if (_conversation.visibleToUsers == null) {
      _conversation.visibleToUsers = [];
    }
    listFriend.forEach((element) {
      _conversation.users.add(User.fromShortUserEntity(element));
      _conversation.visibleToUsers.add(element.id);
      shortUsers.add(element);
    });
    _conversation.visibleToUsers.sort((a, b) => a.compareTo(b));
    _conversation.type = _conversation.users.length > 2 ? "GROUP": "PEEK";
    if(_conversation.type == "GROUP"){
      if (pathImage != '' && pathImage != null) {
        image = await _repository.uploadFile(pathImage, "group_avatar");
      }
      _conversation.groupAvatar = image.url;
      _conversation.config = GroupConfigEntity(
          groupAvatar: image.url,
          shortUsers: shortUsers,
          groupBackground: ChangeBgController.DEFAULT_BACKGROUND,
          privateBackground: ChangeBgController.DEFAULT_BACKGROUND,
      );
    } else {
      String imgUrl = "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
      _conversation.config = GroupConfigEntity(
          groupAvatar: imgUrl,
          shortUsers: shortUsers,
          groupBackground: ChangeBgController.DEFAULT_BACKGROUND,
          privateBackground: ChangeBgController.DEFAULT_BACKGROUND,
      );
    }
    ConversationEntity result = await _socketRepository.createConversations(_conversation);
    /// send message sau khi tạo conversation
    if(result != null && result.type == "GROUP")
      _doAfterCreateConversation(result.toConversation);
    print('quan ${result.toJson().toString()}');
    return result;
  }

  void _doAfterCreateConversation(Conversation conversation) {
    _messageRepository.sendInitialization(conversation);
  }
}
