import 'dart:io';

import 'package:findnear/src/base/base_refresh_load_more.dart';
import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/MediaActionSheetWidget.dart';
import 'package:findnear/src/helpers/debouncer_helper.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/entities/communication/response/conversation_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/messages/conversation_detail/conversation_detail_view.dart';
import 'package:findnear/src/pages/messages/conversation_list/widgets/search_member_widget.dart';
import 'package:findnear/src/pages/pages.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart' as refresh;

import '../../../../main.dart';
import '../../preview_media.dart';
import 'create_conversation_logic.dart';
import 'create_conversation_state.dart';
import 'loading/create_conversation_loading_widget.dart';

class CreateConversationPage extends StatefulWidget {
  final RouteArgument routeArgument;

  CreateConversationPage({Key key, this.routeArgument}) : super(key: key);
  static const String ROUTE_NAME = '/CreateConversationPage';

  @override
  State<CreateConversationPage> createState() => _CreateConversationPageState();
}

class _CreateConversationPageState extends BaseWidgetState<
    CreateConversationPage,
    CreateConversationLogic,
    CreateConversationState>
    with RefreshLoadMoreWidgetMixin {
  Function get onConversationCreated => widget.routeArgument.param["conversation_edited"];
  List<ShortUserEntity> get members => widget.routeArgument.param["members"];

  @override
  CreateConversationLogic createController() => CreateConversationLogic();

  MediaActionSheetWidget _mediaActionSheetWidget;
  var _textController = TextEditingController();
  final _debouncer = DebouncerHelper();

  @override
  void initState() {
    super.initState();
  }

  @override
  void onReady() {
    super.onReady();
    state.isLoadingRx.value = true;
    controller.initMemberTicked(members);
  }

  @override
  Widget build(BuildContext context) {
    _mediaActionSheetWidget = MediaActionSheetWidget(context: context);
    return Obx(() {
      return WillPopScope(
        onWillPop: () async {
          if((state.selectFriends?.isNotEmpty ?? false) || _textNameController.text.isNotEmpty || (state.pathImage?.isNotEmpty ?? false))
            showDialog(
              context: context,
              builder: (BuildContext context) =>
                  CupertinoAlertDialog(
                    content: Text(S
                        .of(context)
                        .cancel_create_group),
                    actions: <Widget>[
                      CupertinoDialogAction(
                        isDefaultAction: true,
                        onPressed: () async {
                          Navigator.of(context, rootNavigator: true).pop();
                        },
                        child: Text(S
                            .of(context)
                            .cancel_input),
                      ),
                      CupertinoDialogAction(
                        isDefaultAction: true,
                        onPressed: () async {
                          Navigator.of(context, rootNavigator: true).popUntil(
                              ModalRoute.withName(PagesWidget.ROUTE_NAME));
                        },
                        child: Text(S
                            .of(context)
                            .ok),
                      )
                    ],
                  ),
            );
          else
            Navigator.of(context, rootNavigator: true).pop();
          return false;
        },
        child: Scaffold(
            appBar: AppBar(
              title: Text(
                "Tạo nhóm",
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 16,
                  color: AppColors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
              actions: <Widget>[
                IgnorePointer(
                  ignoring: !state.enableDoneButtonRx.value,
                  child: GestureDetector(
                    onTap: () {
                      _debouncer.debounce(() {
                        // handle sự kiện bấm hoàn tất
                        _createConversation();
                      });
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: 10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(14.0),
                              color: state.enableDoneButtonRx.value
                                  ? const Color(0xffFF0000)
                                  : AppColors.gray2,
                            ),
                            padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 7),
                            // margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                            child: Center(
                              child: Text(
                                "Hoàn tất",
                                style: TextStyle(
                                  fontFamily: 'Quicksand',
                                  fontSize: 12,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            body: _buildBodyWidget()),
      );
    });
  }

  Widget _buildBodyWidget() {
    return Column(
      children: [
        _buildNameAndImage(),
        SizedBox(height: 20),
        _buildTextSearch(),
        Expanded(child: _buildListFriend()),
        Align(
          alignment: Alignment.bottomLeft,
          child: showSelectMembers(),
        )
      ],
    );
  }

  Widget _buildListFriend() {
    if (state.searchFriendStatusRx.value == LoadStatus.loading) {
      return MessagesLoadingWidget();
    } else if (state.searchFriendStatusRx.value == LoadStatus.success ||
        state.selectFriendStatusRx.value == LoadStatus.success) {
      if (state.friends.isEmpty)
        return Center(child: Text(S
            .of(context)
            .no_result));
      return buildList(
        child: refresh.SmartRefresher(
          controller: state.refreshController,
          onLoading: () => controller.searchFriend(
            keyword: state.textSearchController.text,
          ),
          enablePullUp: state.canLoadmore.value,
          enablePullDown: false,
          child: ListView.separated(
            itemBuilder: (context, index) {
              return SearchMemberWidget(
                onChanged: (bool val) {
                  if (val)
                    controller.selectFriend(user: state.friends[index]);
                  else
                    controller.unSelectFriend(user: state.friends[index]);
                },
                avatar: state.friends
                    .elementAt(index)
                    .image
                    .thumb,
                name: state.friends
                    .elementAt(index)
                    .name,
                value: state.isUserSelected(state.friends[index]),
              );
            },
            itemCount: state.friends.length,
            separatorBuilder: (_, __) =>
                Divider(
                  height: 10,
                  color: AppColors.dividerColor,
                  thickness: 1,
                ),
            padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
          ),
        ),
      );
    }
    return Center(child: Text(S
        .of(context)
        .no_result));
  }

  Widget _buildTextSearch() {
    return Container(
      height: 40,
      margin: EdgeInsets.only(bottom: 2, left: 10, right: 10),
      decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border.all(
            color: Color(0xffCCCCCC),
          ),
          borderRadius: BorderRadius.circular(20)),
      child: TextField(
        controller: _textController,
        textInputAction: TextInputAction.search,
        style: TextStyle(
          fontFamily: 'Quicksand',
          fontSize: 14,
          fontWeight: FontWeight.normal,
          color: Color(0xff333333),
        ),
        decoration: new InputDecoration(
          border: InputBorder.none,
          focusedBorder: InputBorder.none,
          enabledBorder: InputBorder.none,
          errorBorder: InputBorder.none,
          disabledBorder: InputBorder.none,
          hintText: S
              .of(context)
              .searchForFriends,
          prefixIcon: Icon(Icons.search, color: Theme
              .of(context)
              .focusColor),
          hintStyle: TextStyle(
            fontFamily: 'Quicksand',
            fontSize: 14,
            fontWeight: FontWeight.normal,
            color: Color(0xff333333),
          ),
        ),
        onChanged: (value) async {
          state.keyword.value = value.isNotEmpty ? value : '';
          _debouncer.debounce(() {
            // handle sự kiện bấm SEARCH
            if ((value ?? '').isEmpty) {
              controller.searchFriend(keyword: '', isReload: true);
            } else {
              controller.searchFriend(keyword: value, isReload: true);
            }
          });
        },
        onSubmitted: (value) async {
          state.keyword.value = value.isNotEmpty ? value : '';
        },
      ),
    );
  }
  var _textNameController = TextEditingController();
  Widget _buildNameAndImage() {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: TextField(
          controller: _textNameController,
          decoration: InputDecoration(
            focusedBorder: OutlineInputBorder(
              borderSide: new BorderSide(color: Colors.transparent, width: 0),
            ),
            enabledBorder: OutlineInputBorder(
              // width: 0.0 produces a thin "hairline" border
              borderSide: BorderSide(
                  color: Theme
                      .of(context)
                      .primaryColor, width: 1.0),
            ),
            border: OutlineInputBorder(),
            contentPadding:
            EdgeInsets.symmetric(vertical: 15, horizontal: 15),
            hintText: S
                .of(context)
                .nameGroup,
            hintStyle:
            TextStyle(fontSize: 16.0, color: Theme
                .of(context)
                .hintColor),
            fillColor: AppColors.transparent,
            filled: true,
            prefixIcon: GestureDetector(
              onTap: () {
                FocusManager.instance.primaryFocus?.unfocus();
                state.pathImage.value.isNotEmpty
                    ? updateImage()
                    : chooseImage();
              },
              child: Container(
                width: 50,
                height: 50,
                margin: EdgeInsets.symmetric(horizontal: 10),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: AppColors.grayLight,
                ),
                child: state.pathImage.value.isNotEmpty
                    ? new CircleAvatar(
                  foregroundImage: getImage(state.pathImage.value),
                )
                    : Icon(Icons.camera_alt_rounded, color: AppColors.gray,),
              ),
            ),
          ),
          onChanged: (value) => state.nameGroup.value = value),
    );
  }

  ImageProvider getImage(url) {
    if (url.contains("http")) {
      return NetworkImage(url);
    } else {
      return FileImage(File(url));
    }
  }

  void updateImage() async {
    _mediaActionSheetWidget.showActionSheet(S
        .of(context)
        .chooseImage, [
      S
          .of(context)
          .viewPicture,
      S
          .of(context)
          .takePhoto,
      S
          .of(context)
          .chooseFromGrallery
    ], (int index) {
      if (index == 0) {
        _previewTopicPhoto(state.pathImage.value);
      }
      if (index == 1) {
        _openCamera((pickedFile) {
          state.pathImage.value = pickedFile.path;
        });
      }
      if (index == 2) {
        _openGrallery((pickedFile) {
          state.pathImage.value = pickedFile.path;
        });
      }
    });
  }

  void _previewTopicPhoto(url) {
    Navigator.of(context).push(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) => PreviewMediaWidget(photoPath: url),
      ),
    );
  }

  void _openCamera(OnPickImageCallback onPickImageCallback) {
    _mediaActionSheetWidget.pickImageDialog(ImageSource.camera,
            (PickedFile pickedFile) {
          print("PickerFile:${pickedFile.path}");
          setState(() {
            onPickImageCallback(pickedFile);
          });
        });
  }

  void _openGrallery(OnPickImageCallback onPickImageCallback) {
    _mediaActionSheetWidget.pickImageDialog(ImageSource.gallery,
            (PickedFile pickedFile) {
          print("PickerFile:${pickedFile.path}");
          setState(() {
            onPickImageCallback(pickedFile);
          });
        });
  }

  void chooseImage() async {
    _mediaActionSheetWidget.showActionSheet(S
        .of(context)
        .chooseImage, [
      S
          .of(context)
          .takePhoto,
      S
          .of(context)
          .chooseFromGrallery
    ], (int index) {
      if (index == 0) {
        _openCamera((pickedFile) {
          state.pathImage.value = pickedFile.path;
        });
      }
      if (index == 1) {
        _openGrallery((pickedFile) {
          state.pathImage.value = pickedFile.path;
        });
      }
    });
  }

  void _createConversation() async {
    if (state.nameGroup.isEmpty && state.selectFriends.length < 1) {
      showAlertDialog(context, S
          .of(context)
          .errorCreateGroup);
    } else if (state.nameGroup.isNotEmpty && state.selectFriends.length < 1) {
      showAlertDialog(context, S
          .of(context)
          .errorCreateGroup1);
    } else if (state.nameGroup.isEmpty && state.selectFriends.length == 1) {
      List<ConversationEntity> result = await controller.findConversationByReceiver(state.selectFriends);
      if(result.isNotEmpty){
        Conversation _conversation = result[0].toConversation;
        // Navigator.pop(context, true);
        NavigatorUtils.navigateToConversationDetailPage(_conversation, isReplace: true);
      } else {
        String nameGroup = state.selectFriends[0].name;
        List<ShortUserEntity> listFriend = state.selectFriends;
        String pathImage = state.pathImage.value;
        ConversationEntity entity = await controller.createGroup(
            nameGroup: nameGroup,
            listFriend: listFriend,
            pathImage: pathImage);
        onConversationCreated(entity);
        Navigator.pop(context, true);
      }
    } else if (state.nameGroup.isNotEmpty && state.selectFriends.length == 1) {
      showAlertDialog(context, S
          .of(context)
          .errorCreateGroup2);
    } else if (state.nameGroup.isEmpty && state.selectFriends.length > 1) {
      String nameGroup = "";
      for (int i = 0; i < state.selectFriends.length; i++) {
        if (i == state.selectFriends.length - 1)
          nameGroup += "${state.selectFriends[i].name}";
        else
          nameGroup += "${state.selectFriends[i].name}, ";
      }
      List<ShortUserEntity> listFriend = state.selectFriends;
      String pathImage = state.pathImage.value;
      ConversationEntity entity = await controller.createGroup(
          nameGroup: nameGroup, listFriend: listFriend, pathImage: pathImage);
      onConversationCreated(entity);
      Navigator.pop(context, true);
    } else if (state.nameGroup.isNotEmpty && state.selectFriends.length > 1) {
      String nameGroup = state.nameGroup.value;
      List<ShortUserEntity> listFriend = state.selectFriends;
      String pathImage = state.pathImage.value;
      ConversationEntity entity = await controller.createGroup(
          nameGroup: nameGroup, listFriend: listFriend, pathImage: pathImage);
      onConversationCreated(entity);
      Navigator.pop(context, true);
    }
  }

  Widget showSelectMembers() {
    if (state.searchFriendStatusRx.value == LoadStatus.success ||
        state.selectFriendStatusRx.value == LoadStatus.success) {
      if (state.selectFriends?.isNotEmpty ?? false)
        return Container(
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 5,
                blurRadius: 7,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          height: 60,
          width: Get.mediaQuery.size.width,
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          child: ListView.separated(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                width: 40,
                child: Stack(
                  children: [
                    CircleAvatar(
                      foregroundImage: getImage(
                          state.selectFriends
                              .elementAt(index)
                              .avatar),
                    ),
                    Positioned(
                      top: -18,
                      left: 10,
                      child: IconButton(
                        onPressed: () {},
                        icon: Image.asset(AppImages.icAddMemberClose),
                        iconSize: 15,
                      ),
                    )
                  ],
                ),
              );
            },
            separatorBuilder: (BuildContext context, int index) =>
                Container(width: 10),
            itemCount: state.selectFriends.length,
          ),
        );
      return Container();
    }
    return Container();
  }

  showAlertDialog(BuildContext context, String text) {
    // set up the button
    Widget okButton = TextButton(
      child: Text(S
          .of(context)
          .ok),
      onPressed: () {
        Navigator.pop(context, true);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(S
          .of(context)
          .notifications),
      content: Text(text),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    state.nameGroup.value = "";
    state.pathImage.value = "";
    state.keyword.value = "";
    state.friends.clear();
    state.selectFriends.clear();
  }
}
