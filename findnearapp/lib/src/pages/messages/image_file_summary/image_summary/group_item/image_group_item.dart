import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/models/entities/message_chat/entities/message_chat.dart';
import 'package:findnear/src/pages/messages/image_file_summary/link_summary/item/link_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';

import '../../../../../../main.dart';

class ImageGroupItem extends StatelessWidget {
  List<MessageChat> images;
  String date;
  ImageGroupItem({this.date, this.images});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
          child: Text(
            date/*"28 tháng 12, 2021"*/,
            style: TextStyle(
                fontFamily: "Quicksand",
                fontSize: 14,
                fontWeight: FontWeight.bold,
                color: Colors.black),
          ),
        ),
        GridView.count(
          shrinkWrap: true,
          primary: false,
          padding: EdgeInsets.zero,
          crossAxisSpacing: 2,
          mainAxisSpacing: 2,
          crossAxisCount: 3,
          childAspectRatio: 1,
          children: List.generate(images.length, (index) {
            return CachedNetworkImage(
              imageUrl: images[index].config?.data?.image?.media?.url ?? "${GlobalConfiguration().getValue('base_url')}images/image_default.png",
              errorWidget: (context, s, i) => _buildLoadImageError(),
              placeholder: (context, url) => Image.asset(
                'assets/img/loading.gif',
                fit: BoxFit.cover,
                width: double.infinity,
                height: 200.w,
              ),
              fit: BoxFit.cover,
            );
          }),
        ),
      ],
    );
  }

  Widget _buildLoadImageError() {
    return Container(
      width: double.infinity,
      alignment: Alignment.center,
      height: 160.w,
      child: Text(S.of(Get.context).load_image_failed_message),
    );
  }

}