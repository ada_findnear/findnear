import 'package:findnear/src/widgets/shimmer/app_shimmer.dart';
import 'package:flutter/cupertino.dart';

class ImageSummaryLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        itemCount: 5,
        itemBuilder: (context, index) {
          return _buildItem();
        });
  }

  Widget _buildItem(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
          child: AppShimmer(width: 100, height: 18, cornerRadius: 9),
        ),
        GridView.count(
          shrinkWrap: true,
          primary: false,
          // mainAxisSpacing: smallMargin,
          padding: EdgeInsets.zero,
          // set padding to zero
          // crossAxisSpacing: smallMargin,
          crossAxisCount: 3,
          childAspectRatio: 1,
          children: List.generate(9, (index) {
            return AppShimmer();
          }),
        ),
      ],
    );
  }

}