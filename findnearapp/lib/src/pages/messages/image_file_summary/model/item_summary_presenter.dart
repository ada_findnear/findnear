import 'package:findnear/src/models/entities/message_chat/entities/message_chat.dart';

class ItemSummaryPresenter {
  List<MessageChat> messages = [];
  String title;

  ItemSummaryPresenter({this.title, this.messages});

  void push(MessageChat message){
    if(messages?.isEmpty ?? true)
      messages.add(message);
    else {
      int index = messages.indexWhere((element) => element.uuid == message.uuid);
      if(index == -1)
        messages.add(message);
    }
  }
}