import 'package:collection/src/iterable_extensions.dart';
import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/database/chats_dao.dart';
import 'package:findnear/src/database/conversations_dao.dart';
import 'package:findnear/src/models/base/data_reponse.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/message_chat/entities/message_chat.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/response/user_v2_response.dart';
import 'package:findnear/src/pages/messages/image_file_summary/model/item_summary_presenter.dart';
import 'package:findnear/src/repository/message_repository.dart';
import 'package:findnear/src/repository/user_v2_repository.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../../../main.dart';
import 'link_summary_state.dart';

class LinkSummaryLogic extends BaseController<LinkSummaryState> {
  final LinkSummaryState state = LinkSummaryState();

  // repositories
  final _messageRepository = Get.find<MessageRepository>();
  final _userV2Repository = Get.find<UserV2Repository>();

  // DAOs
  final _conversationsDAO = Get.find<ConversationsDAO>();
  var _chatsDAO = ChatsDAO(tableName: null);

  // #region [ Public functions ]
  void initialize(Conversation conversation) async {
    state.conversation.value = conversation;
    await _chatsDAO.closeBox();
    _chatsDAO = ChatsDAO(tableName: conversation.id);

    // get messages from cache to show first
    await _getChatsCache();

    // get users profile
    await _getUsersProfile();

    // listen conversation in local DB
    _watchConversation();

    // fetch messages from API
    fetchLinks(isReload: true);

    // check block
    if (state.users.value.length == 2) {
      _checkBlockUser(state.users.value.first);
    }

    // reload ListView
    state.originalMessages.refresh();
    state.messages.refresh();
  }

  void fetchLinks({bool isReload = false}) async {
    if (isReload) {
      state.currentPageIndex.value = 0;
      state.canLoadmore.value = true;
    }

    if (state.canLoadmore.value != true) return;
    final response = await _messageRepository.getLinkMessage(
      state.conversation.value.originId,
      state.currentPageIndex.value,
      20,
    );

    if (isReload) {
      state.originalMessages.value = [];
      state.messages.value = [];
    }

    state.currentPageIndex.value += 1;
    if (response?.data?.isNotEmpty == true) {
      final messages = response.data.map((e) {
        e.user = state.users.value
            .where((element) => element.code == e.sender)
            .firstOrNull;
        return e;
      }).toList();
      _chatsDAO.putAll(messages);
      state.originalMessages.value = state.originalMessages.value..addAll(messages);
      state.originalMessages.refresh();
      /// parse từ message sang group ảnh
      if(state.originalMessages.value.isNotEmpty) {
        Map<String, List<MessageChat>> groups = state.originalMessages.value.groupListsBy((element) => fromDateTime(element.createDate));
        List<ItemSummaryPresenter> results = [];
        groups.forEach((key, value) {
          results.add(ItemSummaryPresenter(title: key, messages: value));
        });
        state.messages.value = state.messages.value..addAll(results);
        state.messages.refresh();
      }
    } else {
      state.canLoadmore.value = false;
    }

    if (state.originalMessages.value.isEmpty) {
      state.originalMessages.value = []..addAll(state.messagesCache);
    }

    state.refreshController.loadComplete();
  }

  String fromDateTime(DateTime key){
    var pattern = 'dd/MM/yyyy';
    var format = DateFormat(pattern)
        .format(key);
    return format;
  }

  Future<void> _getChatsCache() async {
    state.messagesCache = await _chatsDAO.getLinkOnly()
      ..sort(
            (a, b) => b.createDate.compareTo(a.createDate),
      );
    state.originalMessages.value = []..addAll(state.messagesCache);
    /// parse từ message sang group ảnh
    if(state.originalMessages.value.isNotEmpty) {
      Map<String, List<MessageChat>> groups = state.originalMessages.value.groupListsBy((element) => fromDateTime(element.createDate));
      List<ItemSummaryPresenter> results = [];
      groups.forEach((key, value) {
        results.add(ItemSummaryPresenter(title: key, messages: value));
      });
      state.messages.value = []..addAll(results);
    }
  }

  void _getUsersProfile() async {
    if (state.conversation.value == null) return;

    final futures = <Future<ArrayDataResponse<UserV2Response>>>[];
    for (final userCode in state.conversation.value.userIds) {
      futures.add(_userV2Repository.getUser(userCode));
    }
    final listResponse = await Future.wait(futures);
    state.users.value = listResponse.map((e) {
      if (e.data.isNotEmpty) {
        return e.data.first;
      }
    }).toList();
  }

  void _watchConversation() {
    _conversationsDAO.watch(state.conversation.value.uuid).listen((event) {
      var needUpdateUsers = false;
      if (state.conversation.value.userIds.length != event.userIds.length) {
        needUpdateUsers = true;
      }

      state.conversation.value = event;

      if (needUpdateUsers) {
        _getUsersProfile();
      }
    });
  }

  void _checkBlockUser(UserV2Response user) {
    if (user == null) return;

    if (user.isBlocker) {
      DialogHelper.showAlertDialog(
        context: Get.context,
        title: S.of(Get.context).error,
        description: "Không thể nhắn tin do bạn đang bị chặn",
        okText: S.of(Get.context).ok,
        okFunction: () {
          Get.back();
        },
      );
    } else if (user.isBlocking) {
      DialogHelper.showAlertDialog(
        context: Get.context,
        title: S.of(Get.context).error,
        description: "Không thể nhắn tin do bạn đang chặn người này",
        okText: S.of(Get.context).ok,
        okFunction: () {
          Get.back();
        },
      );
    }
  }

  @override
  LinkSummaryState createState() => LinkSummaryState();

}
