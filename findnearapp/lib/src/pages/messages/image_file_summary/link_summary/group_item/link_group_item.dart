import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/models/entities/message_chat/entities/message_chat.dart';
import 'package:findnear/src/pages/messages/image_file_summary/link_summary/item/link_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../../../main.dart';

class LinkGroupItem extends StatelessWidget {
  List<MessageChat> links;
  String date;
  LinkGroupItem({this.date, this.links});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
          child: Text(
            date/*"28 tháng 12, 2021"*/,
            style: TextStyle(
                fontFamily: "Quicksand",
                fontSize: 14,
                fontWeight: FontWeight.bold,
                color: Colors.black),
          ),
        ),
        Column(
          children: List.generate(links.length, (index) {
            return LinkItem();
          }),
        ),
      ],
    );
  }

}