import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/pages/messages/image_file_summary/link_summary/link_summary_state.dart';
import 'package:findnear/src/pages/messages/image_file_summary/model/item_summary_presenter.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'group_item/link_group_item.dart';
import 'link_summary_logic.dart';
import 'package:findnear/src/helpers/app_config.dart' as config;

class LinkSummaryPage extends StatefulWidget {
  Conversation conversation;

  LinkSummaryPage({this.conversation});

  @override
  State<LinkSummaryPage> createState() => _LinkSummaryPageState();
}

class _LinkSummaryPageState
    extends BaseWidgetState<LinkSummaryPage, LinkSummaryLogic, LinkSummaryState>
    with AutomaticKeepAliveClientMixin<LinkSummaryPage> {
  final _myListKey = GlobalKey<AnimatedListState>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.initialize(widget.conversation);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Obx(_buildLinkList);
  }

  Widget _buildLinkList() {
    if (state.messages.value.isNotEmpty) {
      return Obx(
        () => SmartRefresher(
          controller: state.refreshController,
          onLoading: () => controller.fetchLinks(),
          enablePullUp: state.canLoadmore.value,
          enablePullDown: false,
          child: ListView.builder(
            key: _myListKey,
            physics: const AlwaysScrollableScrollPhysics(),
            padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
            itemCount: state.messages.value.length,
            primary: true,
            itemBuilder: (context, index) {
              ItemSummaryPresenter item = state.messages.value[index];
              return LinkGroupItem(date: item.title, links: item.messages);
            },
          ),
        ),
      );
    } else {
      return Container(
        alignment: AlignmentDirectional.center,
        padding: EdgeInsets.symmetric(horizontal: 30),
        height: config.App(context).appHeight(30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Opacity(
              opacity: 0.4,
              child: Text(
                "Bạn không có bất kỳ link nào",
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline3.merge(TextStyle(fontWeight: FontWeight.w300)),
              ),
            ),
          ],
        ),
      );
    }
  }

  @override
  bool get wantKeepAlive => true;

  @override
  LinkSummaryLogic createController() => LinkSummaryLogic();
}
