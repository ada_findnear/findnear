import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'file_summary/file_summary_view.dart';
import 'image_file_summary_logic.dart';
import 'image_summary/image_summary_view.dart';
import 'link_summary/link_summary_view.dart';

class ImageFileSummaryPage extends StatefulWidget {
  static const String ROUTE_NAME = 'ImageFileSummaryPage';

  final RouteArgument routeArgument;
  const ImageFileSummaryPage({Key key, this.routeArgument}) : super(key: key);

  @override
  State<ImageFileSummaryPage> createState() => _ImageFileSummaryPageState();
}

class _ImageFileSummaryPageState extends State<ImageFileSummaryPage>{
  final logic = Get.put(ImageFileSummaryLogic());

  final state = Get.find<ImageFileSummaryLogic>().state;

  Conversation get conversation => widget.routeArgument.param;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: 3,
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: AppColors.white,
            title: Text(
              'Ảnh, link, file đã gửi',
              style: TextStyle(
                fontFamily: 'Quicksand',
                fontSize: 16,
                color: AppColors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
            iconTheme: IconThemeData(
              color: Colors.black, //change your color here
            ),
          ),
          body: Column(
            children: [
              TabBar(
                tabs: <Widget>[
                  Tab(
                    icon: Text("ẢNH", style: TextStyle(fontFamily: "Quicksand", fontSize: 14, color: AppColors.black),),
                  ),
                  Tab(
                    icon: Text("LINK", style: TextStyle(fontFamily: "Quicksand", fontSize: 14, color: AppColors.black),),
                  ),
                  Tab(
                    icon: Text("FILE", style: TextStyle(fontFamily: "Quicksand", fontSize: 14, color: AppColors.black),),
                  ),
                ],
              ),
              Expanded(
                child: TabBarView(
                  children: <Widget>[
                    ImageSummaryPage(conversation: conversation),
                    LinkSummaryPage(conversation: conversation),
                    FileSummaryPage(conversation: conversation),
                  ],
                ),
              ),
            ],
          ),
      ),
    );
  }
}
