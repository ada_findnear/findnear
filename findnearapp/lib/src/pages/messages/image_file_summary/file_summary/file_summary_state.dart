import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/message_chat/entities/message_chat.dart';
import 'package:findnear/src/models/response/user_v2_response.dart';
import 'package:findnear/src/pages/messages/image_file_summary/model/item_summary_presenter.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart' as refresh;

class FileSummaryState extends BaseState{
  FileSummaryState() {
    ///Initialize variables
  }

  // conversation
  final conversation = Rxn<Conversation>();

  // messages
  final originalMessages = Rx<List<MessageChat>>([]);
  final messages = Rx<List<ItemSummaryPresenter>>([]);
  List<MessageChat> messagesCache = [];

  // users in conversation
  final users = Rx<List<UserV2Response>>([]);

  // loadmore
  final currentPageIndex = 0.obs;
  final canLoadmore = true.obs;
  final refreshController = refresh.RefreshController();
}
