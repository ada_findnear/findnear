import 'package:findnear/src/models/entities/message_chat/entities/message_chat.dart';
import 'package:findnear/src/pages/messages/image_file_summary/file_summary/item/file_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FileGroupItem extends StatelessWidget {
  List<MessageChat> files;
  String date;
  FileGroupItem({this.date, this.files});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
          child: Text(
            date,
            style: TextStyle(
                fontFamily: "Quicksand",
                fontSize: 14,
                fontWeight: FontWeight.bold,
                color: Colors.black),
          ),
        ),
        Column(
          children: List.generate(files.length, (index) {
            return FileItem(file: files[index]);
          }),
        ),
      ],
    );
  }

}