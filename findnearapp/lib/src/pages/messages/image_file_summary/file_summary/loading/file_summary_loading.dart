import 'package:findnear/src/widgets/shimmer/app_shimmer.dart';
import 'package:flutter/cupertino.dart';

class FileSummaryLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        itemCount: 5,
        itemBuilder: (context, index) {
          return _buildGroupItem();
        });
  }

  Widget _buildGroupItem(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
          child: AppShimmer(width: 100, height: 18, cornerRadius: 9),
        ),
        Column(
          children: List.generate(4, (index) {
            return _buildItem();
          }),
        ),
      ],
    );
  }

  Widget _buildItem(){
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          AppShimmer(width: 60, height: 60, cornerRadius: 5),
          Container(width: 10),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AppShimmer(width: 250, height: 18, cornerRadius: 9),
                Container(height: 2),
                AppShimmer(width: 250, height: 18, cornerRadius: 9),
                Container(height: 2),
                Row(
                  children: [
                    AppShimmer(width: 50, height: 18, cornerRadius: 9),
                    Container(width: 2),
                    AppShimmer(width: 80, height: 18, cornerRadius: 9),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

}