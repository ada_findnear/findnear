import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class ListSearchConversationState extends BaseState{
  List<Conversation> conversations = [];
  Rx<LoadStatus> fetchConversationStatus = LoadStatus.initial.obs;
  TextEditingController textEditingController = TextEditingController();
  RxString keyword = "".obs;

  ListSearchConversationState() {
    ///Initialize variables
    textEditingController.addListener(() {
      keyword.value = textEditingController.text;
    });
  }

}
