import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/database/conversations_dao.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/communication/response/conversation_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/repository/communication_repository.dart';
import 'package:get/get.dart';
import 'list_search_conversation_state.dart';

class ListSearchConversationLogic extends BaseController<ListSearchConversationState> {
  final _conversationsDAO = Get.find<ConversationsDAO>();
  CommunicationRepository _repository = Get.find<CommunicationRepository>();

  @override
  ListSearchConversationState createState() => ListSearchConversationState();

  void searchConversations({String text}) async {
    state.fetchConversationStatus.value = LoadStatus.loading;
    try {
      List<ConversationEntity> conversations = await _repository.searchConversations(text ?? "");
      state.conversations = conversations.map((e) => e.toConversation).toList();
      /// save conversation vào local database
      if(state.conversations.isNotEmpty)
        _conversationsDAO.putAll(state.conversations);
      state.fetchConversationStatus.value = LoadStatus.success;
    } catch (e){
      state.fetchConversationStatus.value = LoadStatus.failure;
    }
  }

  void clearKeyword() {
    state.textEditingController.text = "";
    searchConversations(text: "");
  }

  Future<bool> createPINCode(String pinCode, String uuid, Conversation input) async{
    state.fetchConversationStatus.value = LoadStatus.loading;
    try {
      /// Tạo mã pin và ẩn luôn cuộc trò chuyện
      Conversation conversation = input.copyWith(pin: pinCode, isDisplay: false);
      var response = await _repository.updateConversation(conversation); // _repository.createPinCode(uuid: uuid, pinCode: pinCode)
      if(response != null){
        conversation.pin = pinCode;
        conversation.isDisplay = false;
        conversation.pinTop = false;
        _conversationsDAO.put(conversation);
        state.fetchConversationStatus.value = LoadStatus.success;
        return true;
      }
      return false;
    } catch (e){
      state.fetchConversationStatus.value = LoadStatus.failure;
      return false;
    }
  }

  Future<bool> visibleConversation(Conversation input) async{
    state.fetchConversationStatus.value = LoadStatus.loading;
    try {
      /// Tạo mã pin và ẩn luôn cuộc trò chuyện
      int index = state.conversations.indexWhere((element) => input.uuid == element.uuid);
      if(index != -1)
        state.conversations[index].isDisplay = true;
      state.fetchConversationStatus.value = LoadStatus.success;
      return true;
    } catch (e){
      state.fetchConversationStatus.value = LoadStatus.failure;
      return false;
    }
  }

  Future<bool> hideConversation(Conversation input) async{
    state.fetchConversationStatus.value = LoadStatus.loading;
    try {
      int index = state.conversations.indexWhere((element) => input.uuid == element.uuid);
      if(index != -1)
        state.conversations[index].isDisplay = false;
      state.fetchConversationStatus.value = LoadStatus.success;
      return true;
    } catch (e){
      state.fetchConversationStatus.value = LoadStatus.failure;
      return false;
    }
  }
}
