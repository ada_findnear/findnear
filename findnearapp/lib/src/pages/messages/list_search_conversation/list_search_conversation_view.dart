import 'dart:async';

import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/global/global_event.dart';
import 'package:findnear/src/helpers/debouncer_helper.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/enums/create_pin_step_type.dart';
import 'package:findnear/src/models/enums/edit_pin_step_type.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/params/chat_group_params.dart';
import 'package:findnear/src/pages/messages/conversation_detail/conversation_detail_view.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:passcode_screen/circle.dart';
import 'package:passcode_screen/keyboard.dart';
import 'package:passcode_screen/passcode_screen.dart';

import 'item/search_conversation_item.dart';
import 'list_search_conversation_logic.dart';
import 'list_search_conversation_state.dart';
import 'loading/list_conversation_loading_widget.dart';

class ListSearchConversationPage extends StatefulWidget {
  static const String ROUTE_NAME = '/ListSearchConversationPage';

  ListSearchConversationPage();

  @override
  State<ListSearchConversationPage> createState() =>
      _ListSearchConversationState();
}

class _ListSearchConversationState extends BaseWidgetState<
    ListSearchConversationPage,
    ListSearchConversationLogic,
    ListSearchConversationState> {
  final _debouncer = DebouncerHelper();
  final StreamController<bool> _verificationNotifier =
    StreamController<bool>.broadcast();
  final StreamController<bool> _verificationNotifierCreate =
    StreamController<bool>.broadcast();
  EditPINStepType _editStepType = EditPINStepType.NONE;

  @override
  void initState() {
    super.initState();
    GlobalEvent.instance.onVisibleConversationFromSetting.stream
        .listen((data) {
      debugPrint("quanth: onVisible= ${data}");
      controller.visibleConversation(data);
    });
    GlobalEvent.instance.onHideConversationFromSetting.stream
        .listen((data) {
      debugPrint("quanth: onHide= ${data}");
      controller.hideConversation(data);
    });

    controller.searchConversations();
  }

  @override
  ListSearchConversationLogic createController() =>
      ListSearchConversationLogic();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: Container(
        child: Obx(() {
          if (state.fetchConversationStatus == LoadStatus.loading)
            return Container(
              child: ListConversationLoadingWidget(),
            );
          else if (state.fetchConversationStatus == LoadStatus.success)
            return SlidableAutoCloseBehavior(
              child: ListView.separated(
                scrollDirection: Axis.vertical,
                itemCount: state.conversations.length,
                separatorBuilder: (BuildContext context, int index) =>
                    Container(),
                itemBuilder: (_, int index) => GestureDetector(
                  onTap: () {
                    String pin = state.conversations[index].pin;
                    if(pin?.isNotEmpty ?? false){
                      _goToEnterPinCodeScreen(state.conversations[index]);
                    } else {
                      NavigatorUtils.navigateToConversationDetailPage(state.conversations[index], isReplace: true);
                    }
                  },
                  child: SearchConversationItem(
                    conversation: state.conversations[index],
                      onUpdatePress: (bool isDisplay){
                        Conversation conversation = state.conversations[index];
                        /// bật switch button
                        if (!isDisplay) {
                          if (conversation.pin?.isEmpty ?? true)
                            _showCreatePINCodeDialog(state.conversations[index]);
                          else
                            _goToEditPinCodeScreen(state.conversations[index]);
                        } else
                          _showEnableConversationDialog(state.conversations[index]);
                    }
                  ),
                ),
              ),
            );
          return Container(
            child: Center(
              child: Text(
                "Không có dữ liệu",
                style:TextStyle(
                  fontSize: 16,
                ),
              ),
            ),
          );
        }),
      ),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      centerTitle: true,
      leadingWidth: 52,
      leading: IconButton(
        icon: AppImage(
          AppImages.icBackDark,
          color: Theme.of(context).accentColor,
        ),
        onPressed: () => Get.back(),
      ),
      titleSpacing: 0,
      title: Container(
        height: 40,
        margin: const EdgeInsets.only(right: 12),
        decoration: BoxDecoration(
            color: Colors.transparent,
            border: Border.all(
              color: Theme.of(context).focusColor.withOpacity(0.2),
            ),
            borderRadius: BorderRadius.circular(20)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: TextField(
                controller: state.textEditingController,
                textInputAction: TextInputAction.search,
                style: Theme.of(context)
                    .textTheme
                    .caption
                    .merge(TextStyle(fontSize: 16)),
                decoration: new InputDecoration(
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  hintText: S.of(context).searchForFriends,
                  prefixIcon:
                      Icon(Icons.search, color: Theme.of(context).focusColor),
                  hintStyle: Theme.of(context)
                      .textTheme
                      .caption
                      .merge(TextStyle(fontSize: 16)),
                ),
                onChanged: (text) {
                  _debouncer.debounce(() {
                    // handle sự kiện bấm SEARCH
                    if ((text ?? '').isEmpty) {
                      controller.searchConversations();
                    } else {
                      controller.searchConversations(text: text);
                      FocusManager.instance.primaryFocus?.unfocus();
                    }
                  });
                },
                onSubmitted: (text) {
                  // handle sự kiện bấm SEARCH
                  // if ((text ?? '').isEmpty) {
                  //   controller.searchConversations();
                  // } else {
                  //   controller.searchConversations(text: text);
                  // }
                },
              ),
            ),
            Obx(() {
              return Visibility(
                visible: (state.keyword.value ?? "").isNotEmpty,
                child: GestureDetector(
                  onTap: () {
                    controller.clearKeyword();
                  },
                  child: Container(
                    width: 48,
                    child: Center(
                      child: Icon(
                        Icons.close_rounded,
                        color: Theme.of(context).iconTheme.color,
                        size: 20,
                      ),
                    ),
                  ),
                ),
              );
            }),
          ],
        ),
      ),
    );
  }

  InputDecoration getInputDecoration({String hintText, String labelText}) {
    return new InputDecoration(
      floatingLabelBehavior: FloatingLabelBehavior.never,
      hintText: hintText,
      hintStyle: Theme.of(context).textTheme.bodyText2.merge(
            TextStyle(color: Theme.of(context).focusColor),
          ),
      border: InputBorder.none,
    );
  }

  void _onTapChangeBackground(String text) {
    /// todo
  }

  void _goToEnterPinCodeScreen(Conversation conversation) {
    Navigator.push(
      context,
      PageRouteBuilder(
          opaque: false,
          pageBuilder: (context, animation, secondaryAnimation) {
            return PasscodeScreen(
              isValidCallback: (String enteredPasscode){
                NavigatorUtils.navigateToConversationDetailPage(conversation, isReplace: true);
              },
              title: Text(
                S.of(context)
                    .des_open_conversation_1,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: 'Quicksand', color: Colors.black, fontSize: 14),
              ),
              passwordEnteredCallback: (String enteredPasscode) {
                bool isValid = enteredPasscode == conversation.pin;
                _verificationNotifier.add(isValid);
                if (!isValid)
                  Fluttertoast.showToast(
                      msg: "Mã PIN không đúng, vui lòng thử lại.",
                      toastLength: Toast.LENGTH_LONG,
                      backgroundColor: AppColors.black,
                      textColor: AppColors.white,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIosWeb: 1);
              },
              cancelButton: Text(
                S.of(context)
                    .cancel_input,
                style: const TextStyle(fontSize: 16, color: Colors.black),
                semanticsLabel: S
                    .of(context)
                    .cancel_input,
              ),
              deleteButton: Image.asset(
                AppImages.icDeletePinDigit,
                width: 40,
                height: 30,
              ),
              circleUIConfig: CircleUIConfig(
                  borderColor: AppColors.red,
                  fillColor: AppColors.red,
                  borderWidth: 2,
                  circleSize: 15),
              keyboardUIConfig: KeyboardUIConfig(
                digitBorderWidth: 2,
                primaryColor: Colors.white,
                digitFillColor: Colors.white,
                digitTextStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 28,
                    fontWeight: FontWeight.normal),
              ),
              shouldTriggerVerification: _verificationNotifier.stream,
              backgroundColor: AppColors.greyBg,
              cancelCallback: _onPasscodeCancelled,
              passwordDigits: 4,
              bottomWidget: _buildPasscodeRestoreButton(conversation.uuid),
              headerIcon: _editStepType == EditPINStepType.NONE
                  ? Container(
                margin: EdgeInsets.symmetric(vertical: 20),
                child: Icon(
                  Icons.lock,
                  color: Colors.black,
                  size: 40,
                ),
              )
                  : Container(),
            );
          }),
    );
  }

  void _goToEditPinCodeScreen(Conversation conversation) {
    Navigator.push(
      context,
      PageRouteBuilder(
          opaque: false,
          pageBuilder: (context, animation, secondaryAnimation) {
            return PasscodeScreen(
              title: Text(
                S.of(context)
                    .des_pin_code_1,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: 'Quicksand', color: Colors.black, fontSize: 14),
              ),
              isValidCallback: (String enteredPasscode) async {
                /// bật switch button
                bool success = await controller.hideConversation(conversation);
                if (success) {
                  GlobalEvent.instance.onHideConversationFromSetting
                      .add(conversation);
                  Fluttertoast.showToast(
                      msg: "Ẩn trò chuyện thành công",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.CENTER,
                      backgroundColor: AppColors.black,
                      textColor: AppColors.white,
                      timeInSecForIosWeb: 1);
                } else
                  Fluttertoast.showToast(
                      msg: "Ẩn cuộc trò chuyện thất bại!",
                      toastLength: Toast.LENGTH_SHORT,
                      backgroundColor: AppColors.black,
                      textColor: AppColors.white,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIosWeb: 1);
              },
              passwordEnteredCallback: (String enteredPasscode) async {
                bool isValid = enteredPasscode == conversation.pin;
                _verificationNotifier.add(isValid);
                if (!isValid)
                  Fluttertoast.showToast(
                      msg: "Mã PIN không đúng, vui lòng thử lại.",
                      toastLength: Toast.LENGTH_SHORT,
                      backgroundColor: AppColors.black,
                      textColor: AppColors.white,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIosWeb: 1);
              },
              cancelButton: Text(
                S
                    .of(context)
                    .cancel_input,
                style: const TextStyle(fontSize: 16, color: Colors.black),
                semanticsLabel: S
                    .of(context)
                    .cancel_input,
              ),
              deleteButton: Image.asset(
                AppImages.icDeletePinDigit,
                width: 40,
                height: 30,
              ),
              circleUIConfig: CircleUIConfig(
                  borderColor: AppColors.red,
                  fillColor: AppColors.red,
                  borderWidth: 2,
                  circleSize: 15),
              keyboardUIConfig: KeyboardUIConfig(
                digitBorderWidth: 2,
                primaryColor: Colors.white,
                digitFillColor: Colors.white,
                digitTextStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 28,
                    fontWeight: FontWeight.normal),
              ),
              shouldTriggerVerification: _verificationNotifier.stream,
              backgroundColor: AppColors.greyBg,
              cancelCallback: _onPasscodeCancelled,
              passwordDigits: 4,
              bottomWidget: _buildPasscodeRestoreButton(conversation.uuid),
              headerIcon: _editStepType == EditPINStepType.NONE
                  ? Container(
                margin: EdgeInsets.symmetric(vertical: 20),
                child: Icon(
                  Icons.lock,
                  color: Colors.black,
                  size: 40,
                ),
              )
                  : Container(),
            );
          }),
    );
  }

  void _goToCreatePinCodeScreen({
    Conversation conversation,
    String oldPinCode = "",
    CreatePINStepType stepType}) {
    if(stepType == null) stepType = CreatePINStepType.INPUT_NEW_PIN_CODE;
    Navigator.push(
      context,
      PageRouteBuilder(
          opaque: false,
          pageBuilder: (context, animation, secondaryAnimation) {
            return PasscodeScreen(
              title: Text(
                stepType.title,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: 'Quicksand',
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
              isValidCallback: (String enteredPasscode) async{
                switch (stepType) {
                  case CreatePINStepType.INPUT_NEW_PIN_CODE:
                    Navigator.pop(context);
                    _goToCreatePinCodeScreen(
                        oldPinCode: enteredPasscode,
                        stepType: CreatePINStepType.REINPUT_NEW_PIN_CODE);
                    break;
                  case CreatePINStepType.REINPUT_NEW_PIN_CODE:
                    bool success = await controller.createPINCode(
                        enteredPasscode, conversation.uuid, conversation);
                    if (success) {
                      GlobalEvent.instance.onHideConversationFromSetting
                          .add(conversation);
                      _showSuccessDialog();
                    } else
                      Fluttertoast.showToast(
                          msg: "Tạo mã PIN thất bại!",
                          toastLength: Toast.LENGTH_SHORT,
                          backgroundColor: AppColors.black,
                          textColor: AppColors.white,
                          gravity: ToastGravity.CENTER,
                          timeInSecForIosWeb: 1);
                    break;
                }
              },
              passwordEnteredCallback: (String enteredPasscode) async {
                bool isValid = oldPinCode.isEmpty || (oldPinCode.isNotEmpty && enteredPasscode == oldPinCode);
                _verificationNotifierCreate.add(isValid);
                if (!isValid)
                  Fluttertoast.showToast(
                      msg: "Mã PIN không đúng, vui lòng thử lại.",
                      toastLength: Toast.LENGTH_SHORT,
                      backgroundColor: AppColors.black,
                      textColor: AppColors.white,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIosWeb: 1);
              },
              cancelButton: Text(
                S
                    .of(context)
                    .cancel_input,
                style: const TextStyle(fontSize: 16, color: Colors.black),
                semanticsLabel: S
                    .of(context)
                    .cancel_input,
              ),
              deleteButton: Image.asset(
                AppImages.icDeletePinDigit,
                width: 40,
                height: 30,
              ),
              circleUIConfig: CircleUIConfig(
                  borderColor: AppColors.red,
                  fillColor: AppColors.red,
                  borderWidth: 2,
                  circleSize: 15),
              keyboardUIConfig: KeyboardUIConfig(
                digitBorderWidth: 2,
                primaryColor: Colors.white,
                digitFillColor: Colors.white,
                digitTextStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 28,
                    fontWeight: FontWeight.normal),
              ),
              shouldTriggerVerification: _verificationNotifierCreate.stream,
              backgroundColor: AppColors.greyBg,
              cancelCallback: _onCreatePasscodeCancelled,
              passwordDigits: 4,
              bottomWidget: Container(
                margin: EdgeInsets.symmetric(horizontal: 50),
                child: Text(
                  stepType.content,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                      fontFamily: 'Quicksand',
                      fontSize: 13,
                      color: Colors.black,
                      fontWeight: FontWeight.normal),
                ),
              ),
            );
          }),
    );
  }

  void _onPasscodeCancelled() => Navigator.maybePop(context);

  Widget _buildPasscodeRestoreButton(String uuid) =>
      Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                S
                    .of(context)
                    .des_pin_code_2,
                textAlign: TextAlign.center,
                style: const TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 13,
                    color: Colors.black,
                    fontWeight: FontWeight.normal),
              ),
              SizedBox(height: 1),
              Flexible(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Flexible(
                      child: Text(
                        S
                            .of(context)
                            .des_pin_code_3,
                        textAlign: TextAlign.start,
                        style: const TextStyle(
                            fontFamily: 'Quicksand',
                            fontSize: 13,
                            color: Colors.black,
                            fontWeight: FontWeight.normal),
                      ),
                    ),
                    GestureDetector(
                      child: Text(
                        S
                            .of(context)
                            .des_pin_code_4,
                        textAlign: TextAlign.start,
                        style: const TextStyle(
                            fontFamily: 'Quicksand',
                            fontSize: 13,
                            color: AppColors.red,
                            fontWeight: FontWeight.bold),
                      ),
                      onTap:(){
                        _resetAppPassword(uuid);
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );

  void _resetAppPassword(String uuid) => NavigatorUtils.navigateToSettingPinCodeScreen(uuid);

  void _showCreatePINCodeDialog(Conversation conversation) {
    showCupertinoDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text(
            S
                .of(context)
                .create_pin_code_title,
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 18,
              color: AppColors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          content: Text(
            S
                .of(context)
                .create_pin_code_content,
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 14,
              color: AppColors.black,
              fontWeight: FontWeight.normal,
            ),
          ),
          actions: [
            CupertinoDialogAction(
              child: Text(
                S
                    .of(context)
                    .cancel_input,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 16,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            CupertinoDialogAction(
                child: Text(
                  S
                      .of(context)
                      .create_pin_code,
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 16,
                    color: AppColors.red,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                  _goToCreatePinCodeScreen();
                }),
          ],
        );
      },
    );
  }

  void _showEnableConversationDialog(Conversation conversation) {
    showCupertinoDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          content: Text(
            "Hiện lại trò chuyện của Nhóm ăn chơi?",
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 14,
              color: AppColors.black,
              fontWeight: FontWeight.normal,
            ),
          ),
          actions: [
            CupertinoDialogAction(
              child: Text(
                S
                    .of(context)
                    .cancel_input,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 16,
                  color: AppColors.red,
                  fontWeight: FontWeight.bold,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            CupertinoDialogAction(
                child: Text(
                  "Hiện",
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 16,
                    color: AppColors.black,
                    fontWeight: FontWeight.normal,
                  ),
                ),
                onPressed: () async {
                  /// hiện cuộc trò chuyện
                  bool success = await controller.visibleConversation(conversation);
                  if (success) {
                    GlobalEvent.instance.onVisibleConversationFromSetting
                        .add(conversation);
                    Navigator.of(context).pop();
                    Fluttertoast.showToast(
                        msg: "Đã hiển thị cuộc trò chuyện!",
                        toastLength: Toast.LENGTH_SHORT,
                        backgroundColor: AppColors.black,
                        textColor: AppColors.white,
                        gravity: ToastGravity.CENTER,
                        timeInSecForIosWeb: 1);
                  } else
                    Fluttertoast.showToast(
                        msg: "Không thể hiển thị cuộc trò chuyện!",
                        toastLength: Toast.LENGTH_SHORT,
                        backgroundColor: AppColors.black,
                        textColor: AppColors.white,
                        gravity: ToastGravity.CENTER,
                        timeInSecForIosWeb: 1);
                }),
          ],
        );
      },
    );
  }

  void _onCreatePasscodeCancelled() {
    Navigator.maybePop(context);
  }

  void _showSuccessDialog() {
    showCupertinoDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text(
            S
                .of(context)
                .setup_PIN_code_title,
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 16,
              color: AppColors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          content: Text(
            S
                .of(context)
                .setup_PIN_code_content,
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 14,
              color: AppColors.black,
              fontWeight: FontWeight.normal,
            ),
          ),
          actions: [
            CupertinoDialogAction(
                child: Text(
                  S
                      .of(context)
                      .close,
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 16,
                    color: AppColors.red,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                }),
          ],
        );
      },
    );
  }

}
