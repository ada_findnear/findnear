import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/CircularAvatarWidget.dart';
import 'package:findnear/src/helpers/date_utils.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/media.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/messages/conversation_detail/widgets/app_message_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';

import '../../../../../main.dart';

class SearchConversationItem extends StatefulWidget {
  Conversation conversation;
  final Function onUpdatePress;

  SearchConversationItem({this.conversation, this.onUpdatePress});

  @override
  State<SearchConversationItem> createState() => _SearchConversationItemState();
}

class _SearchConversationItemState extends State<SearchConversationItem> {
  GlobalKey<ActionPaneState> _slidableKey = GlobalKey<ActionPaneState>();

  @override
  Widget build(BuildContext context) {
    return _buildBodyWidget();
  }

  Widget _buildBodyWidget() {
    return Slidable(
      endActionPane: ActionPane(
        key: _slidableKey,
        extentRatio: 0.25,
        motion: DrawerMotion(),
        children: [
          SlidableAction(
            onPressed: (context) {
              bool isDisplay = widget.conversation.isDisplay ?? true;
              widget.onUpdatePress(!isDisplay);
            },
            backgroundColor: widget.conversation.isDisplay ? Color(0xffEE4E49) : Color(0xFF4752BA),
            foregroundColor: Colors.white,
            customImage: widget.conversation.isDisplay ?
            Image.asset(
                AppImages.icHideConversation,
                width: 15,
                height: 15,
                color: AppColors.white) :
            Image.asset(
                AppImages.icShowConversation,
                width: 15,
                height: 15,
                color: AppColors.white),
            customLabel: FittedBox(
              child: Text(
                widget.conversation.isDisplay? S.of(context).hide_conversation_des : S.of(context).show_conversation_des,
                style: TextStyle(
                  fontFamily: "Quicksand",
                  color: AppColors.white,
                  fontWeight: FontWeight.normal,
                ),
              ),
            ),
          ),
        ],
        openActionPanel: () {
          //widget.openActionPanel(widget.message);
        },
        closeActionPanel: () {
          //widget.closeActionPanel(widget.message);
        },
      ),
      child: Container(
        color: Colors.white, // #
        padding: EdgeInsets.fromLTRB(12, 10, 12, 0),
        // height: 70,
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                _buildAvatarWidget(),
                SizedBox(width: 12),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              widget.conversation.name,
                              overflow: TextOverflow.fade,
                              softWrap: false,
                              style: Theme.of(Get.context)
                                  .textTheme
                                  .bodyText1
                                  .copyWith(
                                      fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 4),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                            child: AppMessageText(
                              widget.conversation.lastMessageEntity.content ?? "",
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              style: Theme.of(Get.context)
                                  .textTheme
                                  .bodyText1
                                  .copyWith(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 14,
                                  ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Text(
                  widget.conversation.beautyCreateDate,
                  overflow: TextOverflow.fade,
                  softWrap: false,
                  style: Theme.of(Get.context)
                      .textTheme
                      .bodyText1
                      .copyWith(fontSize: 12),
                ),
              ],
            ),
            SizedBox(height: 10),
            Container(
                margin: EdgeInsets.only(left: 50),
                height: 0.5,
                color: AppColors.grayLight.withOpacity(0.5)),
          ],
        ),
      ),
    );
  }

  Widget _buildNotificationIcon() {
    return Icon(Icons.notifications_off_outlined,
        color: Theme.of(Get.context).accentColor, size: 16);
  }

  Widget _buildAvatarWidget() {
    final isGroupChat =
        widget.conversation.type != null && widget.conversation.type == "GROUP";
    if (isGroupChat) {
      return Container(
        width: 38,
        height: 38,
        child: Stack(
          children: [
            Positioned(
              top: 0,
              left: 0,
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(15)),
                child: CachedNetworkImage(
                  height: 30,
                  width: 30,
                  fit: BoxFit.cover,
                  imageUrl: (getAvatarUrls()?.length ?? 0) > 0
                      ? getAvatarUrls()[0]
                      : Media().thumb,
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              right: 0,
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(11)),
                child: CachedNetworkImage(
                  height: 22,
                  width: 22,
                  fit: BoxFit.cover,
                  imageUrl: (getAvatarUrls()?.length ?? 0) > 1
                      ? getAvatarUrls()[1]
                      : Media().thumb,
                ),
              ),
            ),
          ],
        ),
      );
    }
    return Stack(
      children: <Widget>[
        SizedBox(
          width: 38,
          height: 38,
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(60)),
            child: CachedNetworkImage(
              height: 38,
              width: double.infinity,
              fit: BoxFit.cover,
              imageUrl: (widget.conversation.config.groupAvatar == null)
                  ? "${GlobalConfiguration().getValue('base_url')}images/image_default.png"
                  : widget.conversation.config.groupAvatar,
              placeholder: (context, url) => Image.asset(
                'assets/img/loading.gif',
                fit: BoxFit.cover,
                width: double.infinity,
                height: 38,
              ),
              errorWidget: (context, url, error) => CachedNetworkImage(
                imageUrl: (new Media()).thumb,
                fit: BoxFit.cover,
                width: double.infinity,
                height: 38,
              ),
            ),
          ),
        ),
        Positioned(
          bottom: 3,
          right: 3,
          width: 12,
          height: 12,
          child: Container(
            decoration: BoxDecoration(shape: BoxShape.circle),
          ),
        )
      ],
    );
  }

  List<String> getAvatarUrls() {
    String groupAvatar = widget.conversation.config?.groupAvatar ?? "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
    String user = "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
    if(widget.conversation.config.shortUsers?.isNotEmpty ?? false)
      user = widget.conversation.config.shortUsers[0]?.avatar ?? "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
    return [
      groupAvatar,
      user,
    ];
  }
}
