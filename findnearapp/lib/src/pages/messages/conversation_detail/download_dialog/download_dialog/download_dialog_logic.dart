import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';

import 'download_dialog_state.dart';

class DownloadDialogLogic extends GetxController {
  final state = DownloadDialogState();

  downloadImage({String fileUrl, String fileName}) async {
    try {
      var response = await Dio().get(
        fileUrl,
        options: Options(responseType: ResponseType.bytes),
        onReceiveProgress: (received, total) {
          int percentage = ((received / total) * 100).floor();
          state.downloadingPercentage.value = percentage;
        },
      );
      final result = await ImageGallerySaver.saveImage(
        Uint8List.fromList(response.data),
        quality: 100,
        name: fileName,
      );
      Get.back(result: "success");
    } catch (e) {
      logger.e(e);
      Get.back(result: "failure");
    }
  }

  downloadVideo({String fileUrl, String fileName}) async {
    try {
      var appDocDir = await getTemporaryDirectory();
      String savePath = appDocDir.path + "/${fileName}";
      await Dio().download(
        fileUrl,
        savePath,
        onReceiveProgress: (received, total) {
          int percentage = ((received / total) * 100).floor();
          state.downloadingPercentage.value = percentage;
        },
      );
      final result = await ImageGallerySaver.saveFile(savePath);
      Get.back(result: "success");
    } catch (e) {
      logger.e(e);
      Get.back(result: "failure");
    }
  }
}
