import 'dart:developer';

import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'download_dialog_logic.dart';
import 'download_dialog_state.dart';

enum FileType {
  video,
  image,
}

class DownloadDialogPage extends StatefulWidget {
  final FileType fileType;
  final String fileUrl;
  final String fileName;

  DownloadDialogPage({
    this.fileType,
    this.fileUrl,
    this.fileName,
  });

  @override
  _DownloadDialogPageState createState() => _DownloadDialogPageState();
}

class _DownloadDialogPageState extends State<DownloadDialogPage> {
  final DownloadDialogLogic logic = Get.put(DownloadDialogLogic());
  final DownloadDialogState state = Get.find<DownloadDialogLogic>().state;

  @override
  void initState() {
    super.initState();
    logger.d(widget.fileType);
    logger.d(widget.fileUrl);
    logger.d(widget.fileName);
    if (widget.fileType == FileType.video) {
      logic.downloadVideo(fileName: widget.fileName, fileUrl: widget.fileUrl);
    } else {
      logic.downloadImage(fileName: widget.fileName, fileUrl: widget.fileUrl);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
        child: Container(
          margin: EdgeInsets.all(20),
          padding: EdgeInsets.all(12),
          decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(12))),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                S.of(context).downloading,
                style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 12),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  widget.fileName,
                  style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 14, fontWeight: FontWeight.bold, fontStyle: FontStyle.italic),
                ),
              ),
              SizedBox(height: 8),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Container(
                      child: LinearProgressIndicator(
                        backgroundColor: Theme.of(context).accentColor.withOpacity(0.2),
                      ),
                    ),
                  ),
                  Obx(() {
                    return Container(
                      alignment: Alignment.centerRight,
                      width: 40,
                      child: Text(
                        state.downloadingPercentage.value.toString() + "%",
                        style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                    );
                  }),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    Get.delete<DownloadDialogLogic>();
    super.dispose();
  }
}
