import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/media.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tap_debouncer/tap_debouncer.dart';

class ConversationAppBar extends AppBar {
  ConversationAppBar({
    Conversation conversation,
    String avatar,
    VoidCallback onBackPressed,
    VoidCallback onTitlePressed,
    VoidCallback onSettingPressed,
    VoidCallback onMenuPressed,
    VoidCallback onCallPressed,
    Color backgroundColor,
    VoidCallback onCallVideoPressed,
  }) : super(
          backgroundColor: backgroundColor ?? Colors.transparent,
          elevation: 2,
          centerTitle: false,
          leading: new IconButton(
            icon: Image.asset(
              AppImages.icBackDark,
              color: Theme.of(Get.context).hintColor,
            ),
            onPressed: onBackPressed,
          ),
          titleSpacing: 0,
          automaticallyImplyLeading: false,
          title: GestureDetector(
            onTap: onTitlePressed,
            child: Row(
              children: [
                Container(
                  width: 36,
                  height: 36,
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(36)),
                    child: CachedNetworkImage(
                      width: double.infinity,
                      fit: BoxFit.cover,
                      imageUrl: avatar,
                      placeholder: (context, url) => Image.asset(
                        'assets/img/loading.gif',
                        fit: BoxFit.cover,
                        width: double.infinity,
                      ),
                      errorWidget: (context, url, error) => CachedNetworkImage(
                        imageUrl: (new Media()).thumb,
                        fit: BoxFit.cover,
                        width: double.infinity,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                Flexible(
                  child: Text(
                    conversation?.name ?? "",
                    overflow: TextOverflow.fade,
                    maxLines: 1,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            conversation?.idAdmin != null ?? false
                ? IconButton(
                    visualDensity: VisualDensity(horizontal: -3.0),
                    padding: EdgeInsets.zero,
                    onPressed: onSettingPressed,
                    icon: Icon(
                      Icons.settings_outlined,
                      color: Theme.of(Get.context).colorScheme.secondary,
                    ),
                  )
                : Visibility(
                    visible:
                        (conversation?.userIds?.length ?? 0) > 2 ? false : true,
                    child: TapDebouncer(
                      onTap: () async {
                        await onCallPressed();
                        await Future<void>.delayed(
                            const Duration(milliseconds: 1000));
                      }, // your tap handler moved here
                      builder: (BuildContext context, TapDebouncerFunc onTap) {
                        return IconButton(
                          visualDensity: VisualDensity(horizontal: -3.0),
                          padding: EdgeInsets.zero,
                          onPressed: onTap,
                          icon: Icon(Icons.phone,
                              color:
                                  Theme.of(Get.context).colorScheme.secondary),
                        );
                      },
                    ),
                  ),
            Visibility(
              visible: (conversation?.userIds?.length ?? 0) > 2 ? false : true,
              child: TapDebouncer(
                onTap: () async {
                  await onCallVideoPressed();
                  await Future<void>.delayed(
                      const Duration(milliseconds: 1000));
                },
                // your tap handler moved here
                builder: (BuildContext context, TapDebouncerFunc onTap) {
                  return IconButton(
                    visualDensity: VisualDensity(horizontal: -3.0),
                    padding: EdgeInsets.zero,
                    onPressed: onTap,
                    icon: Icon(Icons.videocam,
                        color: Theme.of(Get.context).colorScheme.secondary),
                  );
                },
              ),
            ),
            IconButton(
              visualDensity: VisualDensity(horizontal: -3.0),
              padding: EdgeInsets.zero,
              onPressed: onMenuPressed,
              icon: Icon(Icons.more_vert,
                  color: Theme.of(Get.context).colorScheme.secondary),
            ),
          ],
        );
}
