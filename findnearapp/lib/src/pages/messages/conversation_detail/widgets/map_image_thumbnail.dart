import 'package:flutter/cupertino.dart';
import 'package:findnear/src/repository/settings_repository.dart' as settingRepository;

class MapImageThumbnail extends StatelessWidget {
  final double lat;
  final double long;

  const MapImageThumbnail({
    Key key,
    this.lat,
    this.long,
  }) : super(key: key);

  String get _constructUrl => Uri(
        scheme: 'https',
        host: 'maps.googleapis.com',
        // port: 443,
        path: '/maps/api/staticmap',
        queryParameters: {
          'center': '$lat,$long',
          'zoom': '18',
          'size': '700x500',
          'maptype': 'roadmap',
          'key': "AIzaSyBtaQfYohHQE01th0fmXvKLG-DvIvcBhrI",
          'markers': 'color:red|$lat,$long'
        },
      ).toString();

  @override
  Widget build(BuildContext context) {
    return Image.network(
      _constructUrl,
      height: 300.0,
      width: 600.0,
      fit: BoxFit.fill,
    );
  }
}
