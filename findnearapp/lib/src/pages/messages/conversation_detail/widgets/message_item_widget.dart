
import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/global/global_data.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:intl/intl.dart';
import 'package:just_audio/just_audio.dart';
import 'package:open_file/open_file.dart';

import '../../../../commons/app_colors.dart';
import '../../../../helpers/date_utils.dart';
import '../../../../models/entities/message_chat/entities/message_chat.dart';
import '../../../../models/entities/message_chat/enums/message_chat_type.dart';
import '../../../../models/entities/message_chat/enums/message_text_style_type.dart';
import '../../../../models/media.dart';
import '../../../../repository/user_repository.dart';
import '../../../preview_document.dart';
import 'app_message_text.dart';
import 'message_item/message_item_call.dart';
import 'message_item/message_item_change_background.dart';
import 'message_item/message_item_file.dart';
import 'message_item/message_item_gif.dart';
import 'message_item/message_item_image.dart';
import 'message_item/message_item_init_conversation.dart';
import 'message_item/message_item_invite_member.dart';
import 'message_item/message_item_leave_conversation.dart';
import 'message_item/message_item_location.dart';
import 'message_item/message_item_name_card.dart';
import 'message_item/message_item_poll.dart';
import 'message_item/message_item_reply.dart';
import 'message_item/message_item_sticker.dart';
import 'message_item/message_item_video.dart';
import 'message_item/message_item_voice.dart';
import 'message_item/message_time_separate.dart';

class MessageItemWidget extends StatelessWidget {
  final MessageChat chat;
  final MessageChat previousChat;
  final bool isGroupChat;

  // long press
  final VoidCallback onLongPressed;

  // user
  final Function(String, String) onUserTapped;

  // location
  final Function(MessageChat) onLocationPressed;

  // file
  final Function(MessageChat) onFilePressed;

  // call
  final VoidCallback onPressedCall;
  final VoidCallback onPressedCallVoice;

  // voice
  final String voiceUrlPlaying;
  final Duration voiceTimeRemain;
  final ProcessingState voiceProcessingState;
  final Function(String) onVoicePressed;

  // poll
  final Map<String, MessageChat> pollMessages;
  final Function(MessageChat) onPollTapped;

  // private params
  bool _isOutgoingMessage() =>
      chat.sender != null && currentUser.value?.code == chat.sender;

  final Color _outgoingColor = AppColors.red200;
  final Color _incomingColor = Color(0xFFF2F2F2);

  MessageItemWidget({
    this.chat,
    this.previousChat = null,
    this.isGroupChat,
    // long press
    this.onLongPressed,
    // user
    this.onUserTapped,
    // location
    this.onLocationPressed,
    // file
    this.onFilePressed,
    // call
    this.onPressedCall,
    this.onPressedCallVoice,
    // voice
    this.voiceUrlPlaying,
    this.voiceTimeRemain,
    this.voiceProcessingState,
    this.onVoicePressed,
    // poll
    this.pollMessages = const {},
    this.onPollTapped,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        MessageTimeSeparate(
          chat: chat,
          previousChat: previousChat,
        ),
        _buildMessage(context),
      ],
    );
  }

  Widget _buildMessage(BuildContext context) {
    switch (chat.type) {
      case MessageChatType.text:
        return GestureDetector(
          onLongPress: onLongPressed,
          child: _buildMessageText(),
        );
        break;
      case MessageChatType.reply:
        return GestureDetector(
          onLongPress: onLongPressed,
          child: _buildMessageReply(),
        );
        break;
      case MessageChatType.image:
        return GestureDetector(
          onLongPress: onLongPressed,
          child: _buildMessageImage(),
        );
        break;
      case MessageChatType.video:
        return GestureDetector(
          onLongPress: onLongPressed,
          child: _buildMessageVideo(),
        );
        break;
      case MessageChatType.sticker:
        return GestureDetector(
          onLongPress: onLongPressed,
          child: _buildMessageSticker(),
        );
        break;
      case MessageChatType.gif:
        return GestureDetector(
          onLongPress: onLongPressed,
          child: _buildMessageGif(),
        );
        break;
      case MessageChatType.voice:
        return GestureDetector(
          onLongPress: onLongPressed,
          onTap: () => onVoicePressed(chat.config.data.voice.url),
          child: _buildMessageVoice(),
        );
        break;
      case MessageChatType.location:
        return GestureDetector(
          onLongPress: onLongPressed,
          onTap: () => onLocationPressed?.call(chat),
          child: _buildMessageLocation(),
        );
        break;
      case MessageChatType.nameCard:
        return GestureDetector(
          onLongPress: onLongPressed,
          child: _buildMessageNameCard(),
        );
        break;
      case MessageChatType.file:
        return GestureDetector(
          onLongPress: onLongPressed,
          onTap: () => onFilePressed?.call(chat),
          child: _buildMessageFile(),
        );
        break;
      case MessageChatType.call:
        return _buildMessageCall();
        break;
      case MessageChatType.changeBackground:
        return _buildMessageChangeBackground();
        break;
      case MessageChatType.initialization:
        return _buildMessageInitialization();
        break;
      case MessageChatType.inviteMember:
        return _buildMessageInviteMember();
        break;
      case MessageChatType.leave:
        return _buildMessageLeaveConversation();
        break;
      case MessageChatType.poll:
        return GestureDetector(
          onTap: () => onPollTapped(chat),
          child: _buildMessagePoll(),
        );
        break;
      default:
        return Container();
    }
  }

  Widget _buildMessageText() {
    final messageTextStyle = MessageTextStyleEntity.fromMessageTextStyleType(
      chat.config.data.messageTextStyle?.style ?? MessageTextStyleType.unknown,
    );
    final child = AppMessageText(
      chat.content,
      style: TextStyle(
        fontSize: 16,
        fontFamily: messageTextStyle.font,
        color: messageTextStyle.color,
        fontWeight: messageTextStyle.weight,
      ),
      onTap: onUserTapped,
    );

    if (_isOutgoingMessage()) {
      return _buildOutgoingMessage(
        _buildBackgroundOutgoingMessage(child: child),
      );
    } else {
      return _buildIncomingMessage(
        _buildBackgroundIncomingMessage(child: child),
      );
    }
  }

  Widget _buildMessageReply() {
    final child = MessageItemReply(
      chat: chat,
      onUserTapped: onUserTapped,
    );

    if (_isOutgoingMessage()) {
      return _buildOutgoingMessage(
        _buildBackgroundOutgoingMessage(child: child),
      );
    } else {
      return _buildIncomingMessage(
        _buildBackgroundIncomingMessage(child: child),
      );
    }
  }

  Widget _buildMessageImage() {
    final child = MessageItemImage(
      chat: chat,
      backgroundColor: _isOutgoingMessage() ? _outgoingColor : _incomingColor,
      isShowSender: isGroupChat && _isBuildInComingUserWidget(),
    );

    if (_isOutgoingMessage()) {
      return _buildOutgoingMessage(
        child,
      );
    } else {
      return _buildIncomingMessage(
        child,
      );
    }
  }

  Widget _buildMessageVideo() {
    final child = MessageItemVideo(
      chat: chat,
      backgroundColor: _isOutgoingMessage() ? _outgoingColor : _incomingColor,
      isShowSender: isGroupChat && _isBuildInComingUserWidget(),
    );

    if (_isOutgoingMessage()) {
      return _buildOutgoingMessage(
        child,
      );
    } else {
      return _buildIncomingMessage(
        child,
      );
    }
  }

  Widget _buildMessageSticker() {
    final child = MessageItemSticker(
      chat: chat,
    );

    if (_isOutgoingMessage()) {
      return _buildOutgoingMessage(
        _buildBackgroundOutgoingMessage(child: child),
      );
    } else {
      return _buildIncomingMessage(
        _buildBackgroundIncomingMessage(child: child),
      );
    }
  }

  Widget _buildMessageGif() {
    final child = MessageItemGif(
      chat: chat,
      backgroundColor: _isOutgoingMessage() ? _outgoingColor : _incomingColor,
      isShowSender: isGroupChat && _isBuildInComingUserWidget(),
    );

    if (_isOutgoingMessage()) {
      return _buildOutgoingMessage(
        child,
      );
    } else {
      return _buildIncomingMessage(
        child,
      );
    }
  }

  Widget _buildMessageVoice() {
    final child = MessageItemVoice(
      chat: chat,
      voiceUrlPlaying: voiceUrlPlaying,
      voiceTimeRemain: voiceTimeRemain,
      voiceProcessingState: voiceProcessingState,
    );

    if (_isOutgoingMessage()) {
      return _buildOutgoingMessage(
        _buildBackgroundOutgoingMessage(child: child),
      );
    } else {
      return _buildIncomingMessage(
        _buildBackgroundIncomingMessage(child: child),
      );
    }
  }

  Widget _buildMessageLocation() {
    final child = MessageItemLocation(
      chat: chat,
      avatarWidget: _buildAvatar(
        allowTap: true,
        size: 29,
        margin: 8.9,
      ),
      color: Colors.redAccent,
    );

    if (_isOutgoingMessage()) {
      return _buildOutgoingMessage(
        child,
      );
    } else {
      return _buildIncomingMessage(
        child,
      );
    }
  }

  Widget _buildMessageNameCard() {
    final child = GestureDetector(
      onTap: () => {
        onUserTapped?.call(
          chat.config.data.nameCard.name,
          chat.config.data.nameCard.userId,
        )
      },
      child: MessageItemNameCard(chat.config.data.nameCard),
    );

    if (_isOutgoingMessage()) {
      return _buildOutgoingMessage(
        _buildBackgroundOutgoingMessage(child: child),
      );
    } else {
      return _buildIncomingMessage(
        _buildBackgroundIncomingMessage(child: child),
      );
    }
  }

  Widget _buildMessageFile() {
    final child = MessageItemFile(
      chat: chat,
    );

    if (_isOutgoingMessage()) {
      return _buildOutgoingMessage(
        _buildBackgroundOutgoingMessage(child: child),
      );
    } else {
      return _buildIncomingMessage(
        _buildBackgroundIncomingMessage(child: child),
      );
    }
  }

  Widget _buildMessageCall() {
    final child = MessageItemCall(
      chat: chat,
      onPressedCall: onPressedCall,
      onPressedCallVoice: onPressedCallVoice,
      isOutgoingMessage: _isOutgoingMessage(),
    );

    if (_isOutgoingMessage()) {
      return _buildOutgoingMessage(
        _buildBackgroundOutgoingMessage(child: child),
      );
    } else {
      return _buildIncomingMessage(
        _buildBackgroundIncomingMessage(child: child),
      );
    }
  }

  Widget _buildMessageChangeBackground() {
    return MessageItemChangeBackground(
      isOutgoingMessage: _isOutgoingMessage(),
      userName: chat.user?.name,
    );
  }

  Widget _buildMessageInitialization() {
    return MessageItemInitConversation(
      chat: chat,
    );
  }

  Widget _buildMessageInviteMember() {
    return MessageItemInviteMember(
      chat: chat,
    );
  }

  Widget _buildMessageLeaveConversation() {
    return MessageItemLeaveConversation(
      chat: chat,
    );
  }

  Widget _buildMessagePoll() {
    final message = pollMessages[chat.config.data.poll.id];
    final isShowFullPoll = message != null && message.uuid == chat.uuid;
    return MessageItemPoll(
      chat: chat,
      poll: message?.config?.data?.poll ?? chat?.config?.data?.poll,
      isShowFullPoll: isShowFullPoll,
      isOutgoingMessage: _isOutgoingMessage(),
    );
  }

  // =============== BUILD WIDGET COMMON ===============
  Widget _buildOutgoingMessage(Widget child) {
    return Container(
      alignment: Alignment.centerRight,
      width: double.infinity,
      margin: EdgeInsets.only(
        top: previousChat?.sender != chat?.sender ? 16 : 4,
      ),
      child: FractionallySizedBox(
        widthFactor: 0.85,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          mainAxisSize: MainAxisSize.min,
          children: [
            Flexible(
              child: child,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildIncomingMessage(Widget child) {
    return Container(
      alignment: Alignment.centerLeft,
      width: double.infinity,
      margin: EdgeInsets.only(
        top: previousChat?.sender != chat?.sender ? 16 : 4,
      ),
      child: FractionallySizedBox(
        widthFactor: 0.85,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Flexible(
              child: Row(
                children: [
                  _isBuildInComingUserWidget()
                      ? _buildAvatar(allowTap: true)
                      : SizedBox(width: 28),
                  SizedBox(width: 12),
                  Flexible(
                    child: child,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBackgroundOutgoingMessage({
    Widget child,
    bool isHasTime = true,
  }) {
    final borderRadius = BorderRadius.only(
        topLeft: Radius.circular(16),
        topRight: Radius.circular(16),
        bottomRight: Radius.circular(16));
    return InkWell(
      borderRadius: borderRadius,
      onTap: () => child is MessageItemFile ?  onTapFileMessage(child) : null,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 8, horizontal: 12),
        decoration: BoxDecoration(
          color: _outgoingColor,
          borderRadius: borderRadius,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            child,
            isHasTime ? _buildTimeMessage(time: chat?.createDate) : Container(),
          ],
        ),
      ),
    );
  }

  void onTapFileMessage(MessageItemFile child) async {
    final fileUrl = child.chat.config.data.messageFile.downloadUrl;
    final filePath =
        '${GlobalData.instance.documentPath}/${fileUrl.split('/').last}';
    if (!child.chat.config.data.messageFile.isDownloaded ||
        (await OpenFile.open(filePath)).type != ResultType.done) {
      NavigatorUtils.navigateToPreviewDocumentPage(fileUrl);
    }
  }

  Widget _buildBackgroundIncomingMessage({
    Widget child,
    bool isHasTime = true,
  }) {
    final borderRadius = BorderRadius.only(
      topLeft: Radius.circular(16),
      topRight: Radius.circular(16),
      bottomRight: Radius.circular(16));

    return InkWell(
      borderRadius: borderRadius,
      onTap: () => child is MessageItemFile ?  onTapFileMessage(child) : null,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 8, horizontal: 12),
        decoration: BoxDecoration(
          color: _incomingColor,
          borderRadius: borderRadius ,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            isGroupChat
                ? Padding(
                    padding: const EdgeInsets.only(bottom: 8),
                    child: Text(
                      chat.user.name,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  )
                : SizedBox(),
            child,
            isHasTime ? _buildTimeMessage(time: chat?.createDate) : Container(),
          ],
        ),
      ),
    );
  }

  Widget _buildTimeMessage({
    @required DateTime time,
  }) {
    if (time == null) return Container();

    final format = DateFormat('HH:mm').format(time);
    return Padding(
      padding: EdgeInsets.only(top: 4),
      child: Text(
        format,
        style: TextStyle(
          color: Colors.black38,
          fontSize: 12,
        ),
      ),
    );
  }

  Widget _buildAvatar({
    bool allowTap = false,
    double size = 28,
    double margin = 6,
  }) {
    final userRemoved = this.chat.user == null;
    final defaultImageUrl =
        '${GlobalConfiguration().getValue('base_url')}images/image_default.png';
    return GestureDetector(
      onTap: () {
        if (allowTap) {
          onUserTapped?.call(chat.user.name, chat.user.id.toString());
        }
      },
      child: Container(
        width: size,
        height: size,
        margin: EdgeInsets.symmetric(vertical: margin),
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(16)),
          child: CachedNetworkImage(
            width: double.infinity,
            fit: BoxFit.cover,
            imageUrl: !userRemoved
                ? this.chat.user.media.first?.icon ?? defaultImageUrl
                : defaultImageUrl,
            placeholder: (context, url) => Image.asset(
              'assets/img/loading.gif',
              fit: BoxFit.cover,
              width: double.infinity,
            ),
            errorWidget: (context, url, error) => CachedNetworkImage(
              imageUrl: (new Media()).thumb,
              fit: BoxFit.cover,
              width: double.infinity,
            ),
          ),
        ),
      ),
    );
  }

  bool _isBuildInComingUserWidget() {
    final previousChatDay =
        previousChat != null ? previousChat.createDate : null;

    var isStartNewDay = false;
    if (previousChatDay != null &&
        !chat.createDate.isSameDay(previousChatDay)) {
      isStartNewDay = true;
    }

    final isBuildAvatar = previousChat?.sender != chat?.sender;
    var isForceBuildAvatar = false;
    if (previousChat != null) {
      switch (previousChat.type) {
        case MessageChatType.changeBackground:
        case MessageChatType.changeAvatarGroup:
        case MessageChatType.changeNameGroup:
        case MessageChatType.inviteMember:
        case MessageChatType.initialization:
        case MessageChatType.leave:
        case MessageChatType.poll:
          isForceBuildAvatar = true;
          break;
        default:
          break;
      }
    }

    return !_isOutgoingMessage() &&
        (isBuildAvatar || isForceBuildAvatar || isStartNewDay);
  }
}
