import 'package:findnear/src/utils/logger.dart';
import 'package:flutter/material.dart';
import 'package:flutter_parsed_text/flutter_parsed_text.dart';

typedef TagCallback = Function(String display, String value);

class AppMessageText extends ParsedText {
  AppMessageText(
    String text, {
    TagCallback onTap,
    TextStyle style,
    TextOverflow overflow = TextOverflow.fade,
    int maxLines,
  }) : super(
          text: text ?? "",
          style: style ?? TextStyle(),
          overflow: overflow,
          maxLines: maxLines,
          parse: <MatchText>[
            MatchText(
              pattern: r"@\[(.*?)\]\((.*?)\)",
              style: style?.copyWith(fontWeight: FontWeight.bold, color: Colors.blue) ?? TextStyle(fontWeight: FontWeight.bold, color: Colors.blue),
              renderText: ({pattern, str}) {
                RegExp displayRegExp = RegExp(r"\[(.*?)\]");
                Match displayMatch = displayRegExp.firstMatch(str);
                RegExp valueRegExp = RegExp(r"\((.*?)\)");
                Match valueMatch = valueRegExp.firstMatch(str);
                return {
                  'display': "@" + displayMatch[1],
                  'value': valueMatch[1],
                };
              },
              onTap: (text) {
                logger.d(text);
                RegExp displayRegExp = RegExp(r"\[(.*?)\]");
                Match displayMatch = displayRegExp.firstMatch(text);
                RegExp valueRegExp = RegExp(r"\((.*?)\)");
                Match valueMatch = valueRegExp.firstMatch(text);
                onTap?.call(displayMatch[1], valueMatch[1]);
                logger.d(text);
              },
            ),
          ],
        );
}
