import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../../../models/entities/message_chat/entities/message_chat.dart';
import '../../../../../models/entities/sticker_entity.dart';

class MessageItemSticker extends StatelessWidget {
  final MessageChat chat;

  const MessageItemSticker({
    @required this.chat,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 80,
      height: 80,
      child: Image.asset(
        StickerEntity(id: chat.config.data.sticker.stickerId).path,
      ),
    );
  }
}
