import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../../../helpers/date_utils.dart';
import '../../../../../models/entities/message_chat/entities/message_chat.dart';

class MessageTimeSeparate extends StatelessWidget {
  final MessageChat chat;
  final MessageChat previousChat;

  const MessageTimeSeparate({
    @required this.chat,
    @required this.previousChat,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final chatDay = chat.createDate;
    final previousChatDay =
        previousChat != null ? previousChat.createDate : null;

    if (previousChatDay != null && chatDay.isSameDay(previousChatDay)) {
      return SizedBox();
    }

    String text = null;
    if (previousChat != null &&
        previousChatDay != null &&
        !chatDay.isSameDay(previousChatDay)) {
      final now = DateTime.now();
      if (chatDay.isSameDay(now)) {
        text = 'Hôm nay';
      } else {
        var pattern = 'dd/MM/yyyy';
        text = DateFormat(pattern).format(chatDay);
      }
    } else {
      var pattern = 'dd/MM/yyyy';
      text = DateFormat(pattern).format(chatDay);
    }
    return Container(
      height: 60,
      alignment: Alignment.center,
      width: double.infinity,
      child: Container(
        width: 100,
        padding: EdgeInsets.fromLTRB(0, 3, 0, 6),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(50)),
            color: Colors.black.withOpacity(0.3)),
        child: Text(
          text,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 12,
            color: Colors.white,
          ),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
