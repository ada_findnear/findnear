import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../commons/app_colors.dart';
import '../../../../../commons/app_images.dart';
import '../../../../../models/entities/message_chat/entities/message_chat.dart';

class MessageItemLocation extends StatelessWidget {
  final MessageChat chat;
  final Widget avatarWidget;
  final Color color;

  const MessageItemLocation({
    @required this.chat,
    @required this.avatarWidget,
    @required this.color,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = Get.mediaQuery.size.width * 2 / 3;
    final height = (width * 500) / 640;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            new Flexible(
              child: Container(
                padding: EdgeInsets.all(3),
                margin: EdgeInsets.symmetric(vertical: 5),
                decoration: BoxDecoration(
                  color: color,
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                ),
                child: Container(
                  width: width,
                  height: height,
                  child: Stack(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(12),
                        child: Image.network(
                          chat.config.data.location.url ?? '',
                          errorBuilder: (context, error, stackTrace) {
                            return Center(
                              child: Icon(
                                Icons.error,
                              ),
                            );
                          },
                          loadingBuilder: (context, child, loadingProgress) {
                            if (loadingProgress == null) return child;
                            return Center(
                              child: CupertinoActivityIndicator(radius: 8),
                            );
                          },
                        ),
                      ),
                      Positioned.fill(
                        child: Align(
                          alignment: Alignment.center,
                          child: Container(
                            height: 90,
                            child: Stack(
                              children: [
                                Image.asset(
                                  AppImages.icMarkerLocation,
                                  width: 50,
                                  height: 50,
                                  color: Colors.red,
                                ),
                                Positioned.fill(
                                  child: Align(
                                    alignment: Alignment.topCenter,
                                    child: avatarWidget,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Positioned.fill(
                        child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            height: 35,
                            decoration: BoxDecoration(
                              color: color == AppColors.red400
                                  ? AppColors.red400
                                  : Colors.white,
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(12),
                                  bottomRight: Radius.circular(12)),
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                SizedBox(width: 14),
                                Text(
                                  "Vị trí của ${this.chat.user.name}",
                                  style: TextStyle(
                                    fontFamily: "Quicksand",
                                    color: Colors.black,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        )
      ],
    );
  }
}
