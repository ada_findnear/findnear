import 'package:findnear/src/commons/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';

import '../../../../../extensions/duration_ext.dart';
import '../../../../../models/entities/message_chat/entities/message_chat.dart';

class MessageItemVoice extends StatelessWidget {
  final MessageChat chat;

  final String voiceUrlPlaying;
  final Duration voiceTimeRemain;
  final ProcessingState voiceProcessingState;

  const MessageItemVoice({
    @required this.chat,
    @required this.voiceUrlPlaying,
    @required this.voiceTimeRemain,
    @required this.voiceProcessingState,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isPlaying = voiceUrlPlaying != null &&
        voiceUrlPlaying == chat.config.data.voice.url;
    final isLoading = isPlaying
        ? (voiceProcessingState == ProcessingState.loading ||
            voiceProcessingState == ProcessingState.buffering)
        : false;
    final time = isPlaying && !isLoading
        ? voiceTimeRemain
        : chat.config.data.voice.duration;

    return Container(
      padding: EdgeInsets.symmetric(vertical: 8),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          isLoading
              ? Container(
                  width: 20,
                  height: 20,
                  child: CircularProgressIndicator(),
                )
              : Container(
                  width: 20,
                  height: 20,
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.all(Radius.circular(100)),
                  ),
                  child: Icon(
                    isPlaying ? Icons.stop : Icons.play_arrow,
                    color: Colors.white,
                    size: 16,
                  )),
          SizedBox(
            width: 8,
          ),
          Icon(
            Icons.multitrack_audio,
            color: Colors.blue,
          ),
          SizedBox(
            width: 8,
          ),
          Text(
            time.toFormatString(),
            style: TextStyle(
              fontSize: 16,
              color: AppColors.textBlack,
              fontWeight: FontWeight.w500,
            )
            ),
        ],
      ),
    );
  }
}
