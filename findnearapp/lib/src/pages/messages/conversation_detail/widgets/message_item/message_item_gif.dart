import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../models/entities/message_chat/entities/message_chat.dart';

class MessageItemGif extends StatelessWidget {
  final MessageChat chat;
  final Color backgroundColor;
  final bool isShowSender;

  const MessageItemGif({
    @required this.chat,
    @required this.backgroundColor,
    this.isShowSender = false,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final widthMedia = chat.config.data.gif.width;
    final heightMedia = chat.config.data.gif.height;

    final screenWidth = Get.size.width;
    var widthWidget = screenWidth * 0.45;
    var heightWidget = widthWidget;
    if (widthMedia > 0 && heightMedia > 0) {
      if (widthMedia > heightMedia) {
        widthWidget = screenWidth * 0.6;
      }
      heightWidget = (widthWidget * heightMedia) / widthMedia;
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        isShowSender
            ? Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Flexible(
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 4, horizontal: 10),
                      margin: EdgeInsets.only(bottom: 8),
                      decoration: BoxDecoration(
                        color: backgroundColor,
                        borderRadius: BorderRadius.all(
                          Radius.circular(100),
                        ),
                      ),
                      child: Text(
                        chat.user.name,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ],
              )
            : SizedBox(),
        GestureDetector(
          onTap: () {},
          child: Container(
            width: widthWidget,
            height: heightWidget,
            decoration: BoxDecoration(
              color: backgroundColor,
              borderRadius: BorderRadius.all(Radius.circular(10)),
              border: Border.all(
                color: backgroundColor,
                width: 1,
              ),
            ),
            child: CachedNetworkImage(
              placeholder: (context, url) => Center(
                child: CupertinoActivityIndicator(radius: 8),
              ),
              imageBuilder: (context, imageProvider) => Container(
                width: widthWidget,
                height: heightWidget,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              imageUrl: chat.config.data.gif.url,
              errorWidget: (context, url, error) => Container(
                width: widthWidget,
                height: heightWidget,
                decoration: BoxDecoration(
                  color: backgroundColor,
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
                child: Center(
                  child: Icon(Icons.error),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
