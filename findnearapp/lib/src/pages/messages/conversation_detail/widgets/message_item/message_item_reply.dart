import 'package:flutter/material.dart';

import '../../../../../models/entities/message_chat/entities/message_chat.dart';
import '../../../../../models/entities/message_chat/enums/message_text_style_type.dart';
import '../app_message_text.dart';

class MessageItemReply extends StatelessWidget {
  final MessageChat chat;
  final Function(String, String) onUserTapped;

  const MessageItemReply({
    @required this.chat,
    @required this.onUserTapped,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final messageTextStyle = MessageTextStyleEntity.fromMessageTextStyleType(
      chat.config.data.messageTextStyle?.style ?? MessageTextStyleType.unknown,
    );

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              width: 3,
              height: 32,
              color: Colors.red,
            ),
            SizedBox(
              width: 6,
            ),
            Flexible(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    chat.config.data.reply.owner ?? "",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 14,
                      color: Colors.black,
                    ),
                  ),
                  SizedBox(height: 4),
                  Text(
                    chat.config.data.reply.message?.content ?? "",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        SizedBox(
          height: 8,
        ),
        AppMessageText(
          chat.content,
          style: TextStyle(
            fontSize: 16,
            fontFamily: messageTextStyle.font,
            color: messageTextStyle.color,
            fontWeight: messageTextStyle.weight,
          ),
          onTap: onUserTapped,
        ),
      ],
    );
  }
}
