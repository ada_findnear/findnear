import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../models/entities/message_chat/entities/message_chat.dart';
import '../../../../video.dart';

class MessageItemVideo extends StatelessWidget {
  final MessageChat chat;
  final Color backgroundColor;
  final bool isShowSender;

  const MessageItemVideo({
    @required this.chat,
    @required this.backgroundColor,
    this.isShowSender = false,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = Get.size.width;
    var width = screenWidth * 0.45;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        isShowSender
            ? Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Flexible(
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 4, horizontal: 10),
                      margin: EdgeInsets.only(bottom: 8),
                      decoration: BoxDecoration(
                        color: backgroundColor,
                        borderRadius: BorderRadius.all(
                          Radius.circular(100),
                        ),
                      ),
                      child: Text(
                        chat.user.name,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ],
              )
            : SizedBox(),
        Container(
          constraints: BoxConstraints(maxWidth: width),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            border: Border.all(
              color: backgroundColor,
              width: 1,
            ),
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: VideoWidget(path: chat.config.data.video.url),
          ),
        ),
      ],
    );
  }
}
