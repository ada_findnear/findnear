import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../../../extensions/duration_ext.dart';
import '../../../../../models/entities/message_chat/entities/message_chat.dart';
import '../../../../../models/entities/message_chat/enums/message_call_type.dart';

class MessageItemCall extends StatelessWidget {
  final MessageChat chat;
  final bool isOutgoingMessage;
  final VoidCallback onPressedCall;
  final VoidCallback onPressedCallVoice;

  const MessageItemCall({
    @required this.chat,
    @required this.isOutgoingMessage,
    @required this.onPressedCall,
    @required this.onPressedCallVoice,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (isOutgoingMessage) {
      return buildMessageOutCallWidget();
    } else {
      return buildMessageInCallWidget();
    }
  }

  Widget buildMessageOutCallWidget() {
    if (chat.config.data.messageCall.messageCallType == MessageCallType.video) {
      if (chat.config.data.messageCall.messageCallStateType ==
          MessageCallStateType.cancel) {
        return buildMissCallGoing();
      } else if (chat.config.data.messageCall.messageCallStateType ==
          MessageCallStateType.accept) {
        return buildAcceptCallGoing();
      } else if (chat.config.data.messageCall.messageCallStateType ==
          MessageCallStateType.reject) {
        return buildRejectCallGoing();
      } else if (chat.config.data.messageCall.messageCallStateType ==
          MessageCallStateType.unknown) {
        return Container();
      }
    } else if (chat.config.data.messageCall.messageCallType ==
        MessageCallType.audio) {
      if (chat.config.data.messageCall.messageCallStateType ==
          MessageCallStateType.cancel) {
        return buildMissCallVoiceGoing();
      } else if (chat.config.data.messageCall.messageCallStateType ==
          MessageCallStateType.accept) {
        return buildAcceptCallVoiceGoing();
      } else if (chat.config.data.messageCall.messageCallStateType ==
          MessageCallStateType.unknown) {
        return Container();
      }
    } else if (chat.config.data.messageCall.messageCallType ==
        MessageCallType.unknown) {
      return Container();
    }

    return Container();
  }

  Widget buildMessageInCallWidget() {
    if (chat.config.data.messageCall.messageCallType == MessageCallType.video) {
      if (chat.config.data.messageCall.messageCallStateType ==
          MessageCallStateType.cancel) {
        return buildMissCallComming();
      } else if (chat.config.data.messageCall.messageCallStateType ==
          MessageCallStateType.accept) {
        return buildAcceptCallComming();
      } else if (chat.config.data.messageCall.messageCallStateType ==
          MessageCallStateType.reject) {
        return buildRejectCallComming();
      } else if (chat.config.data.messageCall.messageCallStateType ==
          MessageCallStateType.unknown) {
        return Container();
      }
    } else if (chat.config.data.messageCall.messageCallType ==
        MessageCallType.audio) {
      if (chat.config.data.messageCall.messageCallStateType ==
          MessageCallStateType.cancel) {
        return buildMissCallVoiceComming();
      } else if (chat.config.data.messageCall.messageCallStateType ==
          MessageCallStateType.accept) {
        return buildAcceptCallVoiceComming();
      } else if (chat.config.data.messageCall.messageCallStateType ==
          MessageCallStateType.unknown) {
        return Container();
      }
    } else if (chat.config.data.messageCall.messageCallType ==
        MessageCallType.unknown) {
      return Container();
    }

    return Container();
  }

  Widget buildMissCallComming() {
    return Container(
      width: 200,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Bạn bị nhỡ',
            style: TextStyle(
                fontSize: 14,
                fontFamily: "Quicksand",
                color: Colors.black,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: [
              Icon(
                Icons.videocam,
                color: Colors.grey,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                'Cuộc gọi video',
                style: TextStyle(
                  fontSize: 14,
                  fontFamily: "Quicksand",
                  color: Colors.grey,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            height: 1,
            color: Colors.grey,
          ),
          SizedBox(
            height: 5,
          ),
          GestureDetector(
            onTap: onPressedCall,
            child: Center(
              child: Text(
                'GỌI LẠI'.toUpperCase(),
                style: TextStyle(
                    fontFamily: 'Quicksand', fontSize: 14, color: Colors.blue),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildRejectCallComming() {
    return Container(
      width: 200,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Người nhận từ chối',
            style: TextStyle(
                fontSize: 14,
                fontFamily: "Quicksand",
                color: Colors.black,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: [
              Icon(
                Icons.videocam,
                color: Colors.grey,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                'Cuộc gọi video',
                style: TextStyle(
                  fontSize: 14,
                  fontFamily: "Quicksand",
                  color: Colors.grey,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            height: 1,
            color: Colors.grey,
          ),
          SizedBox(
            height: 5,
          ),
          GestureDetector(
            onTap: onPressedCall,
            child: Center(
              child: Text(
                'GỌI LẠI'.toUpperCase(),
                style: TextStyle(
                    fontFamily: 'Quicksand', fontSize: 14, color: Colors.blue),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildAcceptCallComming() {
    return Container(
      width: 200,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Cuộc gọi video đến',
            style: TextStyle(
                fontSize: 14,
                fontFamily: "Quicksand",
                color: Colors.black,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: [
              Icon(
                Icons.videocam,
                color: Colors.grey,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                '${chat.config.data.messageCall.duration.toFormatString(separatorSymbol: ' phút ')} giây',
                style: TextStyle(
                    fontSize: 14, fontFamily: "Quicksand", color: Colors.grey),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            height: 1,
            color: Colors.grey,
          ),
          SizedBox(
            height: 5,
          ),
          GestureDetector(
            onTap: onPressedCall,
            child: Center(
              child: Text(
                'GỌI LẠI'.toUpperCase(),
                style: TextStyle(
                    fontFamily: 'Quicksand', fontSize: 14, color: Colors.blue),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildAcceptCallGoing() {
    return Container(
      width: 200,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Cuộc gọi video đi',
            style: TextStyle(
                fontSize: 14,
                fontFamily: "Quicksand",
                color: Colors.black,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: [
              Icon(
                Icons.videocam,
                color: Colors.grey,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                '${chat.config.data.messageCall.duration.toFormatString(separatorSymbol: ' phút ')} giây',
                style: TextStyle(
                    fontSize: 14, fontFamily: "Quicksand", color: Colors.grey),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            height: 1,
            color: Colors.grey,
          ),
          SizedBox(
            height: 5,
          ),
          GestureDetector(
            onTap: onPressedCall,
            child: Center(
              child: Text(
                'GỌI LẠI'.toUpperCase(),
                style: TextStyle(
                    fontFamily: 'Quicksand', fontSize: 14, color: Colors.blue),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildMissCallGoing() {
    return Container(
      width: 200,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Bạn đã hủy',
            style: TextStyle(
                fontSize: 14,
                fontFamily: "Quicksand",
                color: Colors.black,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: [
              Icon(
                Icons.videocam,
                color: Colors.grey,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                'Cuộc gọi video',
                style: TextStyle(
                    fontSize: 14, fontFamily: "Quicksand", color: Colors.grey),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            height: 1,
            color: Colors.grey,
          ),
          SizedBox(
            height: 5,
          ),
          GestureDetector(
            onTap: onPressedCall,
            child: Center(
              child: Text(
                'GỌI LẠI'.toUpperCase(),
                style: TextStyle(
                    fontFamily: 'Quicksand', fontSize: 14, color: Colors.blue),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildRejectCallGoing() {
    return Container(
      width: 200,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Bạn từ chối',
            style: TextStyle(
                fontSize: 14,
                fontFamily: "Quicksand",
                color: Colors.black,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: [
              Icon(
                Icons.videocam,
                color: Colors.grey,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                'Cuộc gọi video',
                style: TextStyle(
                    fontSize: 14, fontFamily: "Quicksand", color: Colors.grey),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            height: 1,
            color: Colors.grey,
          ),
          SizedBox(
            height: 5,
          ),
          GestureDetector(
            onTap: onPressedCall,
            child: Center(
              child: Text(
                'GỌI LẠI'.toUpperCase(),
                style: TextStyle(
                    fontFamily: 'Quicksand', fontSize: 14, color: Colors.blue),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildMissCallVoiceComming() {
    return Container(
      width: 200,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Bạn bị nhỡ',
            style: TextStyle(
                fontSize: 14,
                fontFamily: "Quicksand",
                color: Colors.black,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: [
              Icon(
                Icons.call,
                color: Colors.grey,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                'Cuộc gọi thoại',
                style: TextStyle(
                  fontSize: 14,
                  fontFamily: "Quicksand",
                  color: Colors.grey,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            height: 1,
            color: Colors.grey,
          ),
          SizedBox(
            height: 5,
          ),
          GestureDetector(
            onTap: onPressedCallVoice,
            child: Center(
              child: Text(
                'GỌI LẠI'.toUpperCase(),
                style: TextStyle(
                    fontFamily: 'Quicksand', fontSize: 14, color: Colors.blue),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildAcceptCallVoiceComming() {
    return Container(
      width: 200,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Cuộc gọi thoại đến',
            style: TextStyle(
                fontSize: 14,
                fontFamily: "Quicksand",
                color: Colors.black,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: [
              Icon(
                Icons.call,
                color: Colors.grey,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                '${chat.config.data.messageCall.duration.toFormatString(separatorSymbol: ' phút ')} giây',
                style: TextStyle(
                    fontSize: 14, fontFamily: "Quicksand", color: Colors.grey),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            height: 1,
            color: Colors.grey,
          ),
          SizedBox(
            height: 5,
          ),
          GestureDetector(
            onTap: onPressedCallVoice,
            child: Center(
              child: Text(
                'GỌI LẠI'.toUpperCase(),
                style: TextStyle(
                    fontFamily: 'Quicksand', fontSize: 14, color: Colors.blue),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildAcceptCallVoiceGoing() {
    return Container(
      width: 200,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Cuộc gọi thoại đi',
            style: TextStyle(
                fontSize: 14,
                fontFamily: "Quicksand",
                color: Colors.black,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: [
              Icon(
                Icons.videocam,
                color: Colors.grey,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                '${chat.config.data.messageCall.duration.toFormatString(separatorSymbol: ' phút ')} giây',
                style: TextStyle(
                    fontSize: 14, fontFamily: "Quicksand", color: Colors.grey),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            height: 1,
            color: Colors.grey,
          ),
          SizedBox(
            height: 5,
          ),
          GestureDetector(
            onTap: onPressedCallVoice,
            child: Center(
              child: Text(
                'GỌI LẠI'.toUpperCase(),
                style: TextStyle(
                    fontFamily: 'Quicksand', fontSize: 14, color: Colors.blue),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildMissCallVoiceGoing() {
    return Container(
      width: 200,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Bạn đã hủy',
            style: TextStyle(
                fontSize: 14,
                fontFamily: "Quicksand",
                color: Colors.black,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: [
              Icon(
                Icons.call,
                color: Colors.grey,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                'Cuộc gọi thoại',
                style: TextStyle(
                    fontSize: 14, fontFamily: "Quicksand", color: Colors.grey),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            height: 1,
            color: Colors.grey,
          ),
          SizedBox(
            height: 5,
          ),
          GestureDetector(
            onTap: onPressedCallVoice,
            child: Center(
              child: Text(
                'GỌI LẠI'.toUpperCase(),
                style: TextStyle(
                    fontFamily: 'Quicksand', fontSize: 14, color: Colors.blue),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
