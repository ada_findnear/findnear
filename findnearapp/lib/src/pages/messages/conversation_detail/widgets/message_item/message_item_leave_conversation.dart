import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../commons/app_colors.dart';
import '../../../../../models/entities/communication/entities/short_user_entity.dart';
import '../../../../../models/entities/message_chat/entities/message_chat.dart';
import '../../../../../models/user.dart';
import '../../../../../repository/user_repository.dart';
import '../../../../../utils/local_data_manager.dart';
import 'message_members.dart';

class MessageItemLeaveConversation extends StatelessWidget {
  final MessageChat chat;

  const MessageItemLeaveConversation({
    @required this.chat,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ShortUserEntity leaveUser = this.chat.config?.data?.messageLeave?.shortUser;

    return GestureDetector(
      child: Container(
        color: AppColors.transparent,
        width: Get.mediaQuery.size.width,
        padding: EdgeInsets.symmetric(vertical: 2),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Flexible(
              child: Container(
                padding: EdgeInsets.symmetric(
                  vertical: 8,
                  horizontal: 10,
                ),
                decoration: BoxDecoration(
                  color: Color(0xFFF2F2F2),
                  borderRadius: BorderRadius.all(Radius.circular(60)),
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(width: 5),
                    MessageMembers([leaveUser]),
                    SizedBox(width: 5),
                    Flexible(
                      child: _buildDescription(leaveUser),
                    ),
                    SizedBox(width: 5),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildDescription(ShortUserEntity leaveUser) {
    return RichText(
      textAlign: TextAlign.start,
      text: TextSpan(
        children: [
          TextSpan(
            text: leaveUser?.name ?? "",
            style: TextStyle(
              fontSize: 12,
              color: AppColors.black,
              fontWeight: FontWeight.bold,
              height: 1.4,
            ),
          ),
          TextSpan(
            text: ' đã rời khỏi cuộc trò chuyện.',
            style: TextStyle(
              fontSize: 12,
              color: AppColors.black,
              fontWeight: FontWeight.w300,
              height: 1.4,
            ),
          ),
        ],
      ),
      maxLines: 2,
      overflow: TextOverflow.ellipsis,
    );
  }
}
