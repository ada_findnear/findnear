import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';

import '../../../../../commons/app_colors.dart';
import '../../../../../commons/app_images.dart';

class MessageItemChangeBackground extends StatelessWidget {
  final bool isOutgoingMessage;
  final String userName;

  const MessageItemChangeBackground({
    @required this.isOutgoingMessage,
    @required this.userName,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      alignment: Alignment.center,
      margin: EdgeInsets.only(
        top: 10,
        bottom: 4,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          new Flexible(
            child: GestureDetector(
              child: Container(
                padding: EdgeInsets.symmetric(
                  vertical: 8,
                  horizontal: 10,
                ),
                decoration: BoxDecoration(
                  color: Color(0xFFF2F2F2),
                  borderRadius: BorderRadius.all(Radius.circular(60)),
                ),
                child: FittedBox(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      AppImage(
                        AppImages.icChangeImage,
                        width: 16,
                        color: AppColors.red,
                      ),
                      SizedBox(width: 5),
                      Text(
                        isOutgoingMessage ? "Bạn" : "${userName ?? ""}",
                        style: TextStyle(
                          fontFamily: "Quicksand",
                          fontSize: 12,
                          color: AppColors.black,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        " đã đổi hình nền.",
                        style: TextStyle(
                          fontFamily: "Quicksand",
                          fontSize: 12,
                          color: AppColors.black,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                      Visibility(
                        visible: false,
                        child: Row(
                          children: [
                            SizedBox(width: 5),
                            Text(
                              "Quản lý nhóm",
                              style: TextStyle(
                                fontFamily: "Quicksand",
                                fontSize: 12,
                                color: Colors.blue,
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
