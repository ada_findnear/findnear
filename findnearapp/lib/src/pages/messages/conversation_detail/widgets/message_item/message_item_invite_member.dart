import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../commons/app_colors.dart';
import '../../../../../models/entities/communication/entities/short_user_entity.dart';
import '../../../../../models/entities/message_chat/entities/message_chat.dart';
import '../../../../../models/user.dart';
import '../../../../../repository/user_repository.dart';
import '../../../../../utils/local_data_manager.dart';
import 'message_members.dart';

class MessageItemInviteMember extends StatelessWidget {
  final MessageChat chat;

  const MessageItemInviteMember({
    @required this.chat,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String createdBy = this.chat.config?.data?.messageInvite?.createBy ?? "";
    List<ShortUserEntity> tmp =
        this.chat.config?.data?.messageInvite?.shortUsers ?? [];
    List<ShortUserEntity> invitedUsers = [];

    if (tmp.isNotEmpty) {
      tmp.forEach((element) {
        if (element.code != createdBy) invitedUsers.add(element);
      });
    }
    User _user = Get.find<LocalDataManager>().currentUser;
    bool isCreator = _user.code == createdBy;

    return GestureDetector(
      child: Container(
        color: AppColors.transparent,
        width: Get.mediaQuery.size.width,
        padding: EdgeInsets.symmetric(vertical: 2),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Flexible(
              child: Container(
                padding: EdgeInsets.symmetric(
                  vertical: 8,
                  horizontal: 10,
                ),
                decoration: BoxDecoration(
                  color: Color(0xFFF2F2F2),
                  borderRadius: BorderRadius.all(Radius.circular(60)),
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(width: 5),
                    MessageMembers(invitedUsers),
                    SizedBox(width: 5),
                    Flexible(
                      child: isCreator
                          ? _buildInitTextSender()
                          : _buildInitTextReceiver(),
                    ),
                    SizedBox(width: 5),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  String getInitReceivers(List<ShortUserEntity> shortUsers, String createBy) {
    String result = "";
    if (shortUsers.isNotEmpty) {
      for (int i = 0; i < shortUsers.length; i++) {
        if (shortUsers[i].code != createBy) {
          if (i == shortUsers.length - 1)
            result +=
                "${shortUsers[i].code == currentUser.value.code ? "Bạn" : shortUsers[i].name}";
          else
            result +=
                "${shortUsers[i].code == currentUser.value.code ? "Bạn" : shortUsers[i].name}, ";
        }
      }
    }
    return result;
  }

  String getInitSender(List<ShortUserEntity> shortUsers, String createBy) {
    if (shortUsers.isEmpty) return "";
    ShortUserEntity result = shortUsers
        .firstWhere((element) => element.code == createBy, orElse: () => null);
    return result?.name ?? "";
  }

  Widget _buildInitTextSender() {
    return RichText(
      textAlign: TextAlign.start,
      text: TextSpan(
        children: [
          TextSpan(
            text: getInitReceivers(
                this.chat.config?.data?.messageInvite?.shortUsers ?? [],
                (this.chat.config?.data?.messageInvite?.createBy ?? "")),
            style: TextStyle(
              fontSize: 12,
              color: AppColors.black,
              fontWeight: FontWeight.bold,
              height: 1.4,
            ),
          ),
          TextSpan(
            text: ' được bạn thêm vào nhóm.',
            style: TextStyle(
              fontSize: 12,
              color: AppColors.black,
              fontWeight: FontWeight.w300,
              height: 1.4,
            ),
          ),
        ],
      ),
      maxLines: 2,
      overflow: TextOverflow.ellipsis,
    );
  }

  Widget _buildInitTextReceiver() {
    return RichText(
      textAlign: TextAlign.start,
      text: TextSpan(
        children: [
          TextSpan(
            text: getInitReceivers(
                this.chat.config?.data?.messageInvite?.shortUsers ?? [],
                (this.chat.config?.data?.messageInvite?.createBy ?? "")),
            style: TextStyle(
              fontSize: 12,
              color: AppColors.black,
              fontWeight: FontWeight.bold,
              height: 1.4,
            ),
          ),
          TextSpan(
            text: ' đã được',
            style: TextStyle(
              fontSize: 12,
              color: AppColors.black,
              fontWeight: FontWeight.w300,
              height: 1.4,
            ),
          ),
          TextSpan(
            text: getInitSender(
                (this.chat.config?.data?.messageInvite?.shortUsers ?? []),
                (this.chat.config?.data?.messageInvite?.createBy ?? "")),
            style: TextStyle(
              fontSize: 12,
              color: AppColors.black,
              fontWeight: FontWeight.bold,
              height: 1.4,
            ),
          ),
          TextSpan(
            text: " thêm vào nhóm.",
            style: TextStyle(
              fontSize: 12,
              color: AppColors.black,
              fontWeight: FontWeight.w300,
              height: 1.4,
            ),
          ),
        ],
      ),
      maxLines: 2,
      overflow: TextOverflow.ellipsis,
    );
  }
}
