import 'package:flutter/material.dart';

import '../../../../../models/entities/message_chat/entities/message_name_card.dart';

class MessageItemNameCard extends StatelessWidget {
  final MessageNameCard messageNameCard;
  const MessageItemNameCard(this.messageNameCard, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _buildAvatarWidget(),
        SizedBox(width: 8),
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Danh thiếp",
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 10,
                ),
              ),
              SizedBox(height: 2),
              Text(
                messageNameCard.name,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                "ID ${messageNameCard.userCode}",
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 12,
                ),
              ),
            ],
          ),
        ),
        SizedBox(width: 20),
      ],
    );
  }

  Widget _buildAvatarWidget() {
    return SizedBox(
      width: 40,
      height: 40,
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(60)),
        child: CircleAvatar(
          foregroundImage: NetworkImage(messageNameCard.avatarUrl),
        ),
      ),
    );
  }
}
