import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../commons/app_colors.dart';
import '../../../../../commons/app_images.dart';
import '../../../../../models/entities/message_chat/entities/message_chat.dart';
import '../../../../../models/entities/message_chat/entities/message_poll.dart';
import '../../../../../repository/user_repository.dart';
import '../../../../../widgets/common.dart';

class MessageItemPoll extends StatelessWidget {
  final MessageChat chat;
  final MessagePoll poll;
  final bool isShowFullPoll;
  final bool isOutgoingMessage;

  const MessageItemPoll({
    @required this.chat,
    @required this.poll,
    this.isShowFullPoll = false,
    this.isOutgoingMessage,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(
        left: 20,
        right: 20,
        top: 12,
      ),
      child: Column(
        children: [
          _buildMessage(),
          _buildPoll(),
        ],
      ),
    );
  }

  Widget _buildMessage() {
    final userName = isOutgoingMessage ? "Bạn" : chat.user.name;
    final messageText = chat.config.data.poll.getMessageContent(userName);
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 12,
        vertical: 6,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(1000)),
        border: Border.all(
          color: AppColors.pink,
        ),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          AppImage(
            AppImages.icPoll,
            width: 14,
            color: AppColors.red,
          ),
          SizedBox(
            width: 6,
          ),
          Flexible(
            child: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(
                children: [
                  TextSpan(
                    text: "$messageText ",
                    style: TextStyle(
                      fontSize: 12,
                      color: AppColors.red,
                    ),
                  ),
                  TextSpan(
                    text: poll.topic,
                    style: TextStyle(
                      fontSize: 12,
                      color: AppColors.red,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildPoll() {
    return Visibility(
      visible: isShowFullPoll,
      child: Column(
        children: [
          SizedBox(
            height: 4,
          ),
          Container(
            width: Get.width * 0.7,
            margin: EdgeInsets.symmetric(horizontal: 20),
            padding: EdgeInsets.all(6),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              color: AppColors.red200,
            ),
            child: Column(
              children: [
                _buildHeaderPoll(),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 4),
                  height: 1,
                  color: Colors.white,
                ),
                _buildListOptions(),
                SizedBox(height: 6),
                Text(
                  "Xem chi tiết",
                  style: TextStyle(
                    fontSize: 12,
                    color: AppColors.red,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildHeaderPoll() {
    return Row(
      children: [
        AppImage(
          AppImages.icPoll,
          width: 16,
          color: AppColors.red,
        ),
        SizedBox(width: 6),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                poll.topic,
                style: TextStyle(
                  fontSize: 14,
                  color: AppColors.textBlack,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Visibility(
                visible: poll.endTime != null,
                child: Text(
                  poll.getDeadlineFormated(),
                  style: TextStyle(
                    fontSize: 12,
                    color: AppColors.gray,
                  ),
                ),
              ),
            ],
          ),
        ),
        Visibility(
          visible: poll.isLocked(),
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6),
            child: AppImage(
              AppImages.icPollLock,
              width: 12,
              color: AppColors.gray,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildListOptions() {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: poll.options.length,
      itemBuilder: (context, index) {
        final option = poll.options[index];
        final isSelected = option.pollters.contains(currentUser.value.code);
        return Container(
          margin: EdgeInsets.only(top: 4),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            color: isSelected ? AppColors.red500 : Colors.white,
          ),
          child: Container(
            padding: EdgeInsets.only(
              top: 8,
              bottom: 8,
              left: 10,
              right: 8,
            ),
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    option.name,
                    style: TextStyle(
                      fontSize: 14,
                      color: isSelected ? Colors.white : AppColors.textBlack,
                    ),
                  ),
                ),
                SizedBox(width: 8),
                Text(
                  option.pollters.length.toString(),
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    fontSize: 14,
                    color: isSelected ? Colors.white : AppColors.red,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
