import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/entities/message_chat/entities/message_file.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../models/entities/message_chat/entities/message_chat.dart';

class MessageItemFile extends StatelessWidget {
  final MessageChat chat;
  final isDownloaded = false.obs;
  MessageFile get messageFile => chat?.config?.data?.messageFile;


  final RxDouble downloadedProgress = (-1.0).obs;
  bool get isDownloading => isDownloaded.value != false && downloadedProgress.value > 0 && downloadedProgress.value < 100;


  MessageItemFile({
    @required this.chat,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    isDownloaded.value = messageFile.isDownloaded;
    return Obx(() {
      return Container(
        padding: EdgeInsets.fromLTRB(4, 4, 0, 4),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(15)),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            AppImage(
              AppImages.icChatAttachFile,
              width: 24,
              height: 32,
            ),
            SizedBox(width: 10),
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    this.chat?.config?.data?.messageFile?.fileName ??
                        "Unknown",
                    style: TextStyle(
                      fontSize: 14,
                      fontFamily: "Quicksand",
                      color: AppColors.black,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: 2),
                  Text(
                    this.chat?.config?.data?.messageFile?.formatedSize ??
                        "0 KB",
                    style: TextStyle(
                      fontSize: 12,
                      fontFamily: "Quicksand",
                      color: AppColors.black,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(width: 10),
            if(isDownloaded.value)
              Icon(Icons.check, color: AppColors.red,)
            else
              InkWell(
                onTap: () {
                  if(isDownloading) return;
                  Utils.downloadFile(messageFile.downloadUrl, (percent) {
                    print('percent: $percent');
                    downloadedProgress.value = percent / 100.0;
                    if (percent >= 100) isDownloaded.value = true;
                  });
                },
                child: Container(
                    width: 25,
                    height: 25,
                    child: Stack(
                      children: [
                        if(downloadedProgress.value != -1.0)
                          Obx(() {
                            return CircularProgressIndicator(
                              value: downloadedProgress.value, color: AppColors
                                .red.withOpacity(0.6), strokeWidth: 2,);
                          }),
                        Center(child: AppImage(AppImages.icChatDownloadFile)),
                      ],
                    )),
              ),
          ],
        ),
      );
    });
  }
}
