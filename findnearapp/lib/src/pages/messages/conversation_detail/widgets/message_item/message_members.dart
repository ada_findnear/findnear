import 'package:flutter/material.dart';

import '../../../../../commons/app_colors.dart';
import '../../../../../elements/CircularAvatarWidget.dart';
import '../../../../../models/entities/communication/entities/short_user_entity.dart';

class MessageMembers extends StatelessWidget {
  static const double radius = 14;
  static const double overlappingPixels = 8;

  final List<ShortUserEntity> shortUsers;

  MessageMembers(this.shortUsers);

  @override
  Widget build(BuildContext context) {
    if (shortUsers.isEmpty) return Container();

    final int count = shortUsers.length > 3 ? 3 : shortUsers.length;
    double firstItemOffset = 0;
    double secondItemOffset = 0;
    if (count == 1) {
      firstItemOffset = overlappingPixels / 9;
    } else if (count == 2) {
      firstItemOffset = radius * 2 - overlappingPixels;
    } else if (count > 2) {
      secondItemOffset = radius * 2 - overlappingPixels;
      firstItemOffset = secondItemOffset + radius * 2 - overlappingPixels;
    }

    return Container(
      width: (shortUsers.length > 2)
          ? 3 * (radius * 2 - overlappingPixels)
          : (shortUsers.length == 1)
              ? (radius * 2 - overlappingPixels)
              : 2 * (radius * 2 - overlappingPixels),
      height: radius * 2 + 1,
      child: Stack(
        alignment: Alignment.centerLeft,
        clipBehavior: Clip.none,
        children: [
          if (shortUsers.length > 0)
            Positioned(
              right: firstItemOffset,
              child: CircularAvatarWidget(
                imageUrl: (shortUsers?.isNotEmpty ?? true) ? shortUsers?.elementAt(0)?.avatar: "",
                size: 2 * radius,
                hasBorder: true,
                borderColor: AppColors.white,
              ),
            ),
          if (shortUsers.length > 1)
            Positioned(
              right: secondItemOffset,
              child: CircularAvatarWidget(
                imageUrl: shortUsers.elementAt(1).avatar,
                size: 2 * radius,
                hasBorder: true,
                borderColor: AppColors.white,
              ),
            ),
          if (shortUsers.length > 2)
            Positioned(
              right: 0,
              child: _buildMoreUsersWidget(context),
            ),
        ],
      ),
    );
  }

  Widget _buildMoreUsersWidget(BuildContext context) => Container(
        width: 2 * radius,
        height: 2 * radius,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(radius),
          color: AppColors.gray2,
        ),
        child: Center(
          child: Text(
            "+${shortUsers.length - 2}",
            style: Theme.of(context).textTheme.bodyText1.merge(
                  TextStyle(
                    fontSize: 10,
                    color: AppColors.white,
                  ),
                ),
          ),
        ),
      );
}
