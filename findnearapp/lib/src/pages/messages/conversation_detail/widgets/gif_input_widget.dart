import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tenor/tenor.dart';

import '../../../../../generated/l10n.dart';
import '../../../../commons/app_colors.dart';
import '../../../../models/entities/gif_category_entity.dart';
import '../../../../services/gif_service.dart';
import '../../../../settings/global_variable.dart';
import '../../../../utils/utils.dart';

class GifInputWidget extends StatefulWidget {
  final String messageText;
  final String category;
  final ValueSetter<String> onCategoryPressed;
  final ValueSetter<GifTypes> onGifPressed;

  const GifInputWidget({
    Key key,
    this.messageText,
    this.category,
    this.onCategoryPressed,
    this.onGifPressed,
  }) : super(key: key);

  @override
  _GifInputWidgetState createState() => _GifInputWidgetState();
}

class _GifInputWidgetState extends State<GifInputWidget> {
  GifService _gifService = Get.find<GifService>();

  // states
  String currentCategory;
  List<TenorResult> listGif = [];
  List<GifCategoryEntity> categories = [];

  // for avoid re-render forever when call _handleMessageText() in build()
  String currentMessageText;

  @override
  void initState() {
    super.initState();
    _handleMessageText();
  }

  void _handleMessageText() async {
    if (currentMessageText == widget.messageText) return;
    if (widget.messageText == null) return;

    currentMessageText = widget.messageText;
    categories = _gifService.listGifCategory;

    if (currentMessageText.toLowerCase() == gifTag.toLowerCase()) {
      if (categories.isNotEmpty) {
        if (widget.category.isEmpty) {
          // when just open GIF view
          final gifCategory = categories.first;
          setState(() {
            currentCategory = gifCategory.tenorCategories.name;
            if (gifCategory.tenorResponse != null) {
              listGif = gifCategory.tenorResponse.results;
            }
          });
        } else {
          // when select GIF's category
          final response = await _gifService.getTenorResponse(widget.category);
          setState(() {
            currentCategory = widget.category;
            if (response != null) listGif = response.results;
          });
        }
      }
    } else if (currentMessageText
        .toLowerCase()
        .startsWith(gifTag.toLowerCase())) {
      // when search GIF
      final keyword = currentMessageText
          .toLowerCase()
          .replaceFirst(gifTag.toLowerCase(), "");
      _onSearch(keyword);
    }
  }

  void _onSearch(String keyword) async {
    final response = await _gifService.search(keyword);
    setState(() {
      currentCategory = "";
      if (response != null) {
        listGif = response.results;
      } else {
        listGif.clear();
      }
    });
  }

  void _onCategoryPressed(int index) async {
    final selectedCategory = categories[index].tenorCategories.name;
    final response = await _gifService.getTenorResponse(selectedCategory);
    setState(() {
      currentCategory = selectedCategory;
      if (response != null) listGif = response.results;
    });

    widget.onCategoryPressed?.call(selectedCategory);
  }

  @override
  Widget build(BuildContext context) {
    _handleMessageText();

    return Container(
      decoration: BoxDecoration(
        color: AppColors.white,
        boxShadow: [
          BoxShadow(
              color: Theme.of(context).hintColor.withOpacity(0.10),
              offset: Offset(0, 0),
              blurRadius: 6)
        ],
      ),
      child: categories.isEmpty
          ? Container(
              height: 110,
              child: _buildEmpty(context),
            )
          : Column(
              children: [
                Container(
                  height: 62,
                  child: listGif.isEmpty
                      ? _buildEmpty(context)
                      : _buildGifListView(),
                ),
                _buildGifCategories()
              ],
            ),
    );
  }

  Widget _buildEmpty(BuildContext context) {
    return Center(
      child: Text(S.of(context).gifs_not_found),
    );
  }

  Widget _buildGifListView() {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: listGif.length,
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: () {
            widget.onGifPressed?.call(listGif[index].media.gif);
          },
          child: Container(
            height: Get.width / 5,
            padding: EdgeInsets.all(8),
            color: AppColors.dividerColor,
            child: Image.network(
              Utils.switchDomainTenor(listGif[index].media.tinygif.url),
              loadingBuilder: (context, child, loadingProgress) {
                if (loadingProgress == null) return child;
                return Center(
                  child: CupertinoActivityIndicator(radius: 8),
                );
              },
            ),
          ),
        );
      },
    );
  }

  Widget _buildGifCategories() {
    return Container(
      height: 48,
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(
            color: AppColors.dividerColor,
            width: 0.5,
          ),
        ),
      ),
      child: ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: categories.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              _onCategoryPressed(index);
            },
            child: Container(
              decoration: BoxDecoration(
                  color:
                      categories[index].tenorCategories.name == currentCategory
                          ? AppColors.divider
                          : Colors.transparent,
                  borderRadius: BorderRadius.all(Radius.circular(100))),
              padding: EdgeInsets.all(8),
              margin: EdgeInsets.symmetric(vertical: 8, horizontal: 4),
              child: Center(
                child: Text(
                  categories[index].tenorCategories.name,
                  style: TextStyle(
                    color: categories[index].tenorCategories.name ==
                            currentCategory
                        ? Colors.black
                        : Colors.grey,
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
