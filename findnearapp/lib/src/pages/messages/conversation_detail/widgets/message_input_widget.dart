import 'package:flutter/material.dart';
import 'package:flutter_mentions/flutter_mentions.dart';
import 'package:get/get.dart';
import 'package:tenor/tenor.dart';

import '../../../../../generated/l10n.dart';
import '../../../../commons/app_colors.dart';
import '../../../../commons/app_images.dart';
import '../../../../models/conversation.dart';
import '../../../../models/entities/message_chat/entities/message_chat.dart';
import '../../../../models/entities/message_chat/enums/message_text_style_type.dart';
import '../../../../models/entities/sticker_entity.dart';
import '../../../../settings/global_variable.dart';
import '../../../../widgets/voice_record/voice_record_widget.dart';
import 'gif_input_widget.dart';
import 'stickers_input_widget.dart';

class MessageInputWidget extends StatefulWidget {
  final VoidCallback onMediaPressed;
  final VoidCallback onSendPressed;
  final VoidCallback onMorePressed;
  final VoidCallback onVoicePressed;
  final MessageChat replyingMessage;
  final VoidCallback onCancelReplyingPressed;

  final bool showSticker;
  final VoidCallback onShowStickerPressed;
  final VoidCallback onCancelStickerPressed;
  final ValueSetter<StickerEntity> onStickerPressed;

  final bool showGif;
  final bool showSend;
  final VoidCallback onShowGifPressed;
  final VoidCallback onCancelGifPressed;
  final ValueSetter<GifTypes> onGifPressed;
  final VoidCallback onShowSendPressed;
  final VoidCallback onCancelSendPressed;

  final bool showVoiceRecord;
  final ValueSetter<String> onSendVoice;

  final Conversation conversation;
  final GlobalKey<FlutterMentionsState> mentionsKey;

  final MessageTextStyleEntity currentMessageTextStyle;

  MessageInputWidget({
    Key key,
    this.onMediaPressed,
    this.onSendPressed,
    this.replyingMessage,
    this.onCancelReplyingPressed,
    this.showSticker,
    this.onShowStickerPressed,
    this.onCancelStickerPressed,
    this.onStickerPressed,
    this.showGif,
    this.onShowGifPressed,
    this.onCancelGifPressed,
    this.onGifPressed,
    this.showVoiceRecord,
    this.onSendVoice,
    this.conversation,
    this.mentionsKey,
    this.onShowSendPressed,
    this.onCancelSendPressed,
    this.showSend,
    this.onMorePressed,
    this.onVoicePressed,
    this.currentMessageTextStyle,
  }) : super(key: key);

  @override
  State<MessageInputWidget> createState() => _MessageInputWidgetState();
}

class _MessageInputWidgetState extends State<MessageInputWidget> {
  Worker _mentionDebounce;
  final _mentionValue = "".obs;
  final _currentMessageDebounce = "".obs;
  final _currentGifCategory = "".obs;

  @override
  void initState() {
    super.initState();
    _mentionDebounce = debounce(
      _mentionValue,
      (value) => _currentMessageDebounce.value = value ?? "",
      time: Duration(milliseconds: 500),
    );
  }

  @override
  void dispose() {
    _mentionDebounce.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          _buildGifInput(),
          _buildStickerInput(),
          _buildParentMessage(),
          Container(
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
              boxShadow: [
                BoxShadow(
                    color: Theme.of(context).hintColor.withOpacity(0.10),
                    offset: Offset(0, -4),
                    blurRadius: 10)
              ],
            ),
            child: Row(
              children: [
                SizedBox(width: 4),
                Container(
                  width: 40,
                  height: 40,
                  child: IconButton(
                    onPressed: () {
                      widget.onShowStickerPressed();
                      // hide GIF and reset input when GIF is opening
                      if (_mentionValue.value
                          .toLowerCase()
                          .startsWith(gifTag.toLowerCase())) {
                        widget.mentionsKey.currentState.controller.text = "";
                      }
                    },
                    padding: EdgeInsets.zero,
                    icon: Image.asset(AppImages.icChatEmoji),
                  ),
                ),
                /*IconButton(
                  onPressed: () {
                    return widget.onLocationPressed();
                  },
                  padding: EdgeInsets.zero,
                  icon: _buildLocationIcon(context),
                ),*/
                Expanded(child: _buildTextFieldInput()),
                IconButton(
                  onPressed: () {
                    if (widget.showSticker == true || widget.showGif == true) {
                      // when close
                      if (_mentionValue.value
                          .toLowerCase()
                          .startsWith(gifTag.toLowerCase())) {
                        widget.mentionsKey.currentState.controller.text = "";
                      }
                      widget.onCancelStickerPressed();
                      return widget.onCancelGifPressed();
                    }
                    if (widget.showSend) {
                      // when send message
                      return widget.onSendPressed();
                    } else {
                      return widget.onMorePressed();
                    }
                  },
                  padding: EdgeInsets.zero,
                  icon: _buildSendIcon(context),
                ),
                Visibility(
                  visible: !widget.showSend,
                  child: Container(
                    width: 40,
                    height: 40,
                    child: IconButton(
                      onPressed: widget.onVoicePressed,
                      padding: EdgeInsets.zero,
                      icon: Icon(Icons.keyboard_voice_outlined),
                    ),
                  ),
                ),
                Visibility(
                  visible: !widget.showSend,
                  child: Container(
                    width: 40,
                    height: 40,
                    child: IconButton(
                      onPressed: widget.onMediaPressed,
                      padding: EdgeInsets.zero,
                      icon: Image.asset(AppImages.icChatImg),
                    ),
                  ),
                ),
              ],
            ),
          ),
          _buildVoiceRecordWidget(),
        ],
      ),
    );
  }

  Widget _buildParentMessage() {
    if (widget.replyingMessage == null) {
      return Container();
    } else {
      return Container(
        color: Colors.white,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(width: 12),
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 8),
                  Text(
                    "Replying to:",
                    style: Theme.of(Get.context)
                        .textTheme
                        .bodyText1
                        .copyWith(fontSize: 14, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "${widget.replyingMessage.content}",
                    maxLines: 2,
                    style: Theme.of(Get.context)
                        .textTheme
                        .bodyText1
                        .copyWith(fontSize: 14, fontWeight: FontWeight.normal),
                  ),
                ],
              ),
            ),
            Container(
              child: IconButton(
                onPressed: widget.onCancelReplyingPressed,
                padding: EdgeInsets.zero,
                icon: Icon(
                  Icons.close_rounded,
                  color: Theme.of(Get.context).accentColor,
                ),
              ),
            ),
          ],
        ),
      );
    }
  }

  Widget _buildGifInput() {
    if (widget.showGif != true) {
      return Container();
    }
    return Obx(
      () => GifInputWidget(
        messageText: _currentMessageDebounce.value,
        category: _currentGifCategory.value,
        onCategoryPressed: (value) {
          _currentGifCategory.value = value;
          _changeTextAndMoveCursorLast(gifTag);
        },
        onGifPressed: (value) {
          widget.onGifPressed(value);
        },
      ),
    );
  }

  Widget _buildVoiceRecordWidget() {
    if (widget.showVoiceRecord == true) {
      return Column(
        children: [
          Container(
            width: double.infinity,
            height: 1,
            color: AppColors.dividerColor,
          ),
          Container(
            width: double.infinity,
            height: 200,
            color: Colors.white,
            child: VoiceRecordWidget(
              onSend: (path) {
                widget.onSendVoice?.call(path);
              },
            ),
          ),
        ],
      );
    } else {
      return Container();
    }
  }

  Widget _buildStickerInput() {
    if (widget.showSticker != true) {
      return Container();
    }
    return StickerInputWidget(
      onClosePressed: widget.onCancelStickerPressed,
      onStickerPressed: widget.onStickerPressed,
    );
  }

  Widget _buildTextFieldInput() {
    final isGroupChat = widget.conversation?.idAdmin != null;
    print("${isGroupChat}");
    print("${isGroupChat == true}");

    final context = Get.context;

    final placeHolderGif = S.of(context).search_gif_from_tenor;
    final remainSuffixLength = S.of(context).typeToStartChat.length -
        gifTag.length -
        placeHolderGif.length;
    final suffixPlaceHolderGif = " ${"a" * remainSuffixLength}";

    return Container(
      child: Stack(
        children: [
          Obx(
            () => _mentionValue.value.toLowerCase() == gifTag.toLowerCase()
                ? Positioned.fill(
                    child: Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(left: 54),
                        child: Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: placeHolderGif,
                                style: TextStyle(
                                  color: Theme.of(Get.context)
                                      .focusColor
                                      .withOpacity(0.8),
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              TextSpan(
                                text: suffixPlaceHolderGif,
                                style: TextStyle(
                                  color: Colors.transparent,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                        )),
                  )
                : Container(),
          ),
          FlutterMentions(
            onChanged: _handleMentionChanged,
            key: widget.mentionsKey,
            enableSuggestions: isGroupChat,
            suggestionPosition: SuggestionPosition.Top,
            maxLines: 5,
            minLines: 1,
            style: TextStyle(
              fontFamily: widget.currentMessageTextStyle.font,
              color: widget.currentMessageTextStyle.color,
              fontWeight: widget.currentMessageTextStyle.weight,
            ),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(horizontal: 8, vertical: 20),
              hintText: S.of(Get.context).typeToStartChat,
              hintStyle: TextStyle(
                fontFamily: widget.currentMessageTextStyle.font,
                color: widget.currentMessageTextStyle.color.withOpacity(0.4),
                fontWeight: widget.currentMessageTextStyle.weight,
              ),
              border: UnderlineInputBorder(borderSide: BorderSide.none),
              enabledBorder: UnderlineInputBorder(borderSide: BorderSide.none),
              focusedBorder: UnderlineInputBorder(borderSide: BorderSide.none),
            ),
            mentions: [
              Mention(
                  trigger: '@',
                  style: TextStyle(
                    color: Colors.blue,
                  ),
                  data: isGroupChat
                      ? widget.conversation?.users
                              ?.map((e) => {
                                    'id': e.id ?? "",
                                    'display': e.name ?? "",
                                  })
                              ?.toList() ??
                          []
                      : [],
                  matchAll: false,
                  markupBuilder:
                      (String trigger, String mention, String value) {
                    return "${trigger}[${value}](${mention})";
                  },
                  suggestionBuilder: (data) {
                    return Container(
                      padding: EdgeInsets.all(12.0),
                      color: Theme.of(Get.context).backgroundColor,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            data['display'],
                            style: Theme.of(Get.context)
                                .textTheme
                                .bodyText1
                                .copyWith(
                                    fontSize: 14, fontWeight: FontWeight.bold),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    );
                  }),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildSendIcon(BuildContext context) {
    if (widget.showSticker || widget.showGif) {
      return Icon(Icons.close_rounded);
    } else if (widget.showSend) {
      return Image.asset(AppImages.icChatSend,
          color: Theme.of(context).accentColor);
    } else {
      return Image.asset(AppImages.icChatMore);
    }
  }

  void _changeTextAndMoveCursorLast(String value) {
    widget.mentionsKey.currentState.controller.text = value;
    widget.mentionsKey.currentState.controller.selection =
        TextSelection.fromPosition(TextPosition(
      offset: widget.mentionsKey.currentState.controller.text.length,
    ));
  }

  void _handleMentionChanged(String value) {
    _mentionValue.value = value;
    if (value.toLowerCase().startsWith(gifTag.toLowerCase())) {
      widget.onShowGifPressed();
    } else {
      widget.onCancelGifPressed();
    }
    if (value.isEmpty)
      widget.onCancelSendPressed();
    else
      widget.onShowSendPressed();
  }

  Widget _buildLocationIcon(BuildContext context) {
    return Icon(Icons.location_on_outlined,
        color: Theme.of(context).accentColor);
  }
}
