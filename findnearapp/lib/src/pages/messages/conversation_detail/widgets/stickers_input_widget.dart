import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/models/entities/sticker_entity.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class StickerInputWidget extends StatelessWidget {
  final ValueSetter<StickerEntity> onStickerPressed;
  final VoidCallback onClosePressed;

  StickerInputWidget({
    Key key,
    this.onStickerPressed,
    this.onClosePressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final stickers = StickerList();
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          height: 62,
          decoration: BoxDecoration(
            color: AppColors.white,
            boxShadow: [
              BoxShadow(
                  color: Theme.of(context).hintColor.withOpacity(0.10),
                  offset: Offset(0, 0),
                  blurRadius: 6)
            ],
          ),
          child: Scrollbar(
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: List.generate(
                stickers.stickers.length,
                (index) {
                  return GestureDetector(
                    onTap: () {
                      onStickerPressed?.call(stickers.stickers[index]);
                    },
                    child: Container(
                      height: Get.width / 5,
                      padding: EdgeInsets.all(8),
                      child: Image.asset(stickers.stickers[index].path),
                    ),
                  );
                },
              ),
            ),
          ),
        ),
      ],
    );
  }
}
