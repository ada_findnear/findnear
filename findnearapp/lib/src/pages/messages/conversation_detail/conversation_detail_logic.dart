import 'dart:async';
import 'dart:io';

import 'package:collection/collection.dart';
import 'package:dio/dio.dart' as dio;
import 'package:file_picker/file_picker.dart';
import 'package:findnear/src/models/entities/message_chat/entities/message_poll.dart';
import 'package:findnear/src/repository/message_poll_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:just_audio/just_audio.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:tenor/tenor.dart';

import '../../../../main.dart';
import '../../../base/base_controller.dart';
import '../../../commons/app_colors.dart';
import '../../../database/chats_dao.dart';
import '../../../database/conversations_dao.dart';
import '../../../global/global_event.dart';
import '../../../helpers/helper.dart' as helper1;
import '../../../models/base/data_reponse.dart';
import '../../../models/conversation.dart';
import '../../../models/entities/communication/entities/short_user_entity.dart';
import '../../../models/entities/communication/response/pinned_conversation_response.dart';
import '../../../models/entities/message_chat/entities/message_chat.dart';
import '../../../models/entities/message_chat/entities/message_gif.dart';
import '../../../models/entities/message_chat/enums/message_chat_type.dart';
import '../../../models/entities/message_chat/enums/message_text_style_type.dart';
import '../../../models/entities/state_usercall.dart';
import '../../../models/response/user_v2_response.dart';
import '../../../models/user.dart';
import '../../../repository/communication_repository.dart';
import '../../../repository/message_repository.dart';
import '../../../repository/user_repository.dart';
import '../../../repository/user_v2_repository.dart';
import '../../../utils/local_data_manager.dart';
import '../../../utils/navigator_utils.dart';
import '../../../utils/utils.dart';
import '../../../widgets/dialog_helper.dart';
import '../../profile_user/profile_user_view.dart';
import '../chat_group_setting/bottom_sheet/change_bg_logic.dart';
import 'conversation_detail_state.dart';
import 'widgets/file_type/file_type.dart';

class ConversationDetailLogic extends BaseController<ConversationDetailState> {
  // repositories
  final _messageRepository = Get.find<MessageRepository>();
  final _userV2Repository = Get.find<UserV2Repository>();
  final _conversationRepository = Get.find<CommunicationRepository>();
  final _messagePollRepository = Get.find<MessagePollRepository>();

  // DAOs
  final _conversationsDAO = Get.find<ConversationsDAO>();
  var _chatsDAO = ChatsDAO(tableName: null);

  // audio player
  final _audioPlayer = AudioPlayer();

  StreamSubscription<MessageChat> _messageChatSubscription;
  StreamSubscription<MessageChat> _messageChatDeletedSubscription;

  // #region [ @override functions ]
  @override
  void onClose() async {
    _messageChatSubscription?.cancel();
    _messageChatDeletedSubscription?.cancel();
    _chatsDAO.closeBox();
    _audioPlayer.stop().then((_) => _audioPlayer.dispose());
    super.onClose();
  }

  // #endregion

  // #region [ Public functions ]
  void initialize(Conversation conversation) async {
    state.conversation.value = conversation;
    await _chatsDAO.closeBox();
    _chatsDAO = ChatsDAO(tableName: conversation.id);
    try {
      _listenAudioPlayerStream();
      _listenMessageChatFromSocket();
      _listenMessageChatDeletedFromSocket();
      _updateBackground(conversation);
    } catch (e) {
      print("ConversationDetailLogic - initialize - error: $e");
    }

    // get messages from cache to show first
    await _getChatsCache();

    // get users profile
    await _getUsersProfile();

    // listen conversation in local DB
    _watchConversation();

    // fetch messages from API
    fetchMessages(isReload: true);

    // check block
    if (state.users.value.length == 2) {
      _checkBlockUser(state.users.value.first);
    }

    // reload ListView
    state.messages.refresh();

    /// clear unread luôn khi mà vào trong chat room
    GlobalEvent.instance.onUpdateUnreadResponse.add(conversation);
  }

  void fetchMessages({bool isReload = false}) async {
    if (isReload) {
      state.currentPageIndex.value = 0;
      state.canLoadmore.value = true;
    }

    if (state.canLoadmore.value != true) return;
    final response = await _messageRepository.getMessages(
      state.conversation.value.originId,
      state.currentPageIndex.value,
      20,
    );

    if (isReload) {
      state.messages.value = [];
    }

    state.currentPageIndex.value += 1;
    if (response?.data?.isNotEmpty == true) {
      print("NamNH - message not null");
      final messages = response.data.map((e) {
        e.user = state.users.value
            .where((element) => element.code == e.sender)
            .firstOrNull;
        return e;
      }).toList();
      state.messages.value = state.messages.value..addAll(messages);
      state.messages.refresh();

      await _chatsDAO.putAll(messages);
      _chatsDAO.findAnddeleteChatsRemoved(messages);
      _updatePollMessages(messages);
    } else {
      print("NamNH - message null");
      state.canLoadmore.value = false;
    }

    if (state.messages.value.isEmpty) {
      state.messages.value = []..addAll(state.messagesCache);
    }

    state.refreshController.loadComplete();
  }

  void deleteMessage(MessageChat chat) {
    _messageRepository.deleteMessage(chat.uuid, currentUser.value.code);
  }

  Future<bool> doBlockUser(String userId) async {
    try {
      final User data = await blockUser(int.tryParse(userId) ?? 0);
      if (data != null) {
        ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
          content: Text(S.of(Get.context).blockUserSuccessfully),
        ));
        return true;
      } else {
        ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
          content: Text(S.of(Get.context).blockUserFailure),
        ));
        return false;
      }
    } catch (e) {
      ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
        content: Text(S.of(Get.context).blockUserFailure),
      ));
      return false;
    }
  }

  navigateToProfile() async {
    if (state.conversation.value.idAdmin == null) {
      var targetUser = state.users.value.firstOrNull;
      if (targetUser == null) return;
      NavigatorUtils.navigateToProfilePage(targetUser.id.toString());
    }
  }

  void voicePressed(String voiceUrl) async {
    if (voiceUrl == state.voiceUrlPlaying.value) {
      await _audioPlayer.stop();
    } else {
      await _audioPlayer.stop();
      state.voiceUrlPlaying.value = voiceUrl;
      final duration = await _audioPlayer.setUrl(voiceUrl);
      state.voiceTimeRemain.value = duration;
      _audioPlayer.play();
    }
  }

  void replyMessage(MessageChat message) {
    state.replyingMessage.value = message;
  }

  void cancelReplyMessage() {
    state.replyingMessage.value = null;
  }

  void showStickerInput() {
    state.isShowStickerInput.value = true;
    hideGifInput();
    hideVoiceRecord();
  }

  void hideStickerInput() {
    state.isShowStickerInput.value = false;
  }

  void showGifInput() {
    state.isShowGifInput.value = true;
    hideStickerInput();
    hideVoiceRecord();
  }

  void hideGifInput() {
    state.isShowGifInput.value = false;
  }

  void showVoiceRecord() {
    state.isShowVoiceRecord.value = true;
    hideStickerInput();
    hideGifInput();
  }

  void hideVoiceRecord() {
    state.isShowVoiceRecord.value = false;
  }

  void showSendInput() {
    state.isShowSendInput.value = true;
  }

  void hideSendInput() {
    state.isShowSendInput.value = false;
  }

  /// pin message
  void pinMessage() {
    state.hasPinMessageRx.value = true;
  }

  /// unpin message
  void unpinMessage() {
    state.hasPinMessageRx.value = false;
  }

  void makeCall() async {
    // TODO xin cấp quyền
    Map<Permission, PermissionStatus> statuses = await [
      Permission.microphone,
    ].request();

    if (statuses[Permission.microphone].isGranted) {
      String userReceiverCall;
      String deceiveToken;
      state.users.value.forEach((element) {
        if (element.code != currentUser.value.code) {
          userReceiverCall = element.code;
          deceiveToken = element.deviceToken;
        }
      });

      NavigatorUtils.navigateToVoiceCallWidget(
          state.buildContext,
          ShortUserEntity(
              code: userReceiverCall,
              id: "",
              avatar: "",
              deceiveToken: deceiveToken),
          true,
          false,
          conversation: state.conversation.value);
    } else {
      var error = "";
      print("TuanLA - $statuses[Permission.microphone]");
      if (statuses[Permission.microphone].isDenied ||
          statuses[Permission.microphone].isPermanentlyDenied) {
        error = "Bạn cần phải cấp quyền micro để thực hiện chức năng này";
      }

      Widget cancelButton = TextButton(
        child: Text("Cancel"),
        onPressed: () {
          Get.back();
        },
      );
      Widget settingButton = TextButton(
        child: Text("Open Settings"),
        onPressed: () async {
          await openAppSettings();
        },
      );
      AlertDialog alert = AlertDialog(
        title: Text("Thông báo"),
        content: Text(error),
        actions: [cancelButton, settingButton],
      );
      showDialog(
        context: Get.context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    }
  }

  void makeVideoCall() async {
    // TODO xin cấp quyền
    Map<Permission, PermissionStatus> statuses = await [
      Permission.microphone,
      Permission.camera,
    ].request();

    if (statuses[Permission.microphone].isGranted &&
        statuses[Permission.camera].isGranted) {
      String userReceiverCall;
      String deceiveToken;
      state.users.value.forEach((element) {
        if (element.code != currentUser.value.code) {
          userReceiverCall = element.code;
          deceiveToken = element.deviceToken;
        }
      });
      NavigatorUtils.navigateToVideoCallWidget(
          state.buildContext,
          ShortUserEntity(
              code: userReceiverCall,
              id: "",
              avatar: "",
              deceiveToken: deceiveToken),
          true,
          false,
          conversation: state.conversation.value);
    } else {
      var error = "";
      print("TuanLA - $statuses[Permission.microphone]");
      if (statuses[Permission.microphone].isDenied ||
          statuses[Permission.microphone].isPermanentlyDenied) {
        error = "Bạn cần phải cấp quyền micro để thực hiện chức năng này";
      } else if (statuses[Permission.camera].isDenied ||
          statuses[Permission.camera].isPermanentlyDenied) {
        error = "Bạn cần phải cấp quyền máy ảnh để thực hiện chức năng này";
      } else if (statuses[Permission.microphone].isDenied &&
          statuses[Permission.camera].isDenied) {
        error =
            "Bạn cần phải cấp quyền micro và máy ảnh để thực hiện chức năng này";
      }

      Widget cancelButton = TextButton(
        child: Text("Cancel"),
        onPressed: () {
          Get.back();
        },
      );
      Widget settingButton = TextButton(
        child: Text("Open Settings"),
        onPressed: () async {
          await openAppSettings();
        },
      );
      AlertDialog alert = AlertDialog(
        title: Text("Thông báo"),
        content: Text(error),
        actions: [cancelButton, settingButton],
      );
      showDialog(
        context: Get.context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    }
  }

  void createConversation(Conversation _conversation) async {
    //   var listUser = [_conversation.users[0], currentUser.value];
    //   listUser.sort((a, b) => a.id.compareTo(b.id));
    //   _conversation.users = listUser;
    //   _conversation.lastMessageTime =
    //       DateTime.now().toUtc().millisecondsSinceEpoch;
    //   _conversation.readByUsers = [currentUser.value?.id ?? ""];
    //   // setState(() {
    //   //   conversation = _conversation;
    //   // });
    //   state.conversation.value = _conversation;

    //   // update chats cache when conversationId has changed
    //   await getChatsCache();

    //   // _chatRepository.createConversation(_conversation).then((value) {
    //   //   listenForChats(_conversation);
    //   //   listenForConversation();
    //   // });
  }

  void removeConversation({String conversationId}) async {
    // _chatRepository.removeConversation(conversationId).then((value) {
    //   listenForConversations();
    //   ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
    //     content: Text(S.of(Get.context).removeUserSuccessful),
    //   ));
    // });
  }

  void updateGroupName({Conversation conversation, String nameGroup}) {
    conversation.name = nameGroup;
    // _chatRepository
    //     .updateConversation(conversation.id, conversation.toUpdatedMap())
    //     .then((value) {
    //   listenForConversations();
    //   ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
    //     content: Text(S.of(Get.context).updatePostSuccessful),
    //   ));
    // });
  }

  void updateAvatar({Conversation conversation, String pathImage}) async {
    // Media image = new Media();
    // if (pathImage != '' && pathImage != null) {
    //   image = await _chatRepository.uploadFile(pathImage, "group_avatar");
    // }
    // conversation.groupAvatar = image.url;
    // _chatRepository
    //     .updateConversation(conversation.id, conversation.toUpdatedMap())
    //     .then((value) {
    //   listenForConversations();
    //   ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
    //     content: Text(S.of(Get.context).updatePostSuccessful),
    //   ));
    // });
  }

  void addUser({Conversation conversation, List<User> listUser}) async {
    // List userNames = [];
    // listUser.forEach((element) {
    //   conversation.users.add(element);
    //   conversation.visibleToUsers.add(element.id);
    //   userNames.add(element.name);
    // });
    // _chatRepository
    //     .updateConversation(conversation.id, conversation.toUpdatedMap())
    //     .then((value) {
    //   listenForConversations();
    //   this.addMessage(
    //       conversation,
    //       jsonEncode({
    //         "text": userNames.join(', '),
    //         "typeSystem": TypeSystem.addMember.index
    //       }),
    //       ChatType.system.index);
    //   ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
    //     content: Text(S.of(Get.context).removeUserSuccessful),
    //   ));
    // });
  }

  void removeUser({Conversation conversation, String userID}) async {
    var indexUser = conversation.config.shortUsers
        .indexWhere((element) => element.id == userID);
    if (currentUser.value.id == userID) {
      conversation.idAdmin = conversation.visibleToUsers
          .firstWhere((element) => element != currentUser.value.id);
    }
    // _chatRepository
    //     .removeUserConversation(
    //         conversation.id, conversation.toUpdatedMap(), indexUser)
    //     .then((value) {
    //   listenForConversations();
    //   conversation.visibleToUsers.remove(userID);
    //   this.addMessage(
    //       conversation,
    //       jsonEncode({
    //         "text": conversation.toUpdatedMap()['users'][indexUser]['name'],
    //         "typeSystem": currentUser.value.id != userID
    //             ? TypeSystem.removeMember.index
    //             : TypeSystem.outGroup.index
    //       }),
    //       ChatType.system.index);
    //   // conversation.users.removeWhere((element) => element.id == userID);
    //   ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
    //     content: Text(S.of(Get.context).removeUserSuccessful),
    //   ));
    // });
  }

  void turnOnNotification() {
    final userId = currentUser.value?.id;
    if (userId != null) {
      try {
        // _chatRepository.turnOnNotification(state.conversation.value, userId);
      } catch (e) {}
    }
  }

  void turnOffNotification() {
    final userId = currentUser.value?.id;
    if (userId != null) {
      try {
        // _chatRepository.turnOffNotification(state.conversation.value, userId);
      } catch (e) {}
    }
  }

  String getConversationAvatar() {
    final defaultAvatar =
        "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
    if (state.conversation == null) return defaultAvatar;

    if (state.conversation.value.config?.groupAvatar != null) {
      return state.conversation.value.config?.groupAvatar ?? defaultAvatar;
    } else if (state.conversation.value.config?.shortUsers?.isNotEmpty ??
        false) {
      final result = state.conversation.value.config?.shortUsers
          ?.firstWhere((element) => element.id != currentUser.value.id);
      return result?.avatar ?? defaultAvatar;
    } else {
      return defaultAvatar;
    }
  }

  void changeMessageTextStyle(MessageTextStyleEntity messageTextStyleEntity) {
    state.currentMessageTextStyleEntity.value = messageTextStyleEntity;
    Get.find<LocalDataManager>()
        .setMessageTextStyleFromId(messageTextStyleEntity.id);
  }

  // #endregion

  // #region [ Private functions ]
  void _listenAudioPlayerStream() {
    // get position duration when stream audio
    _audioPlayer
        .createPositionStream(
      minPeriod: Duration(seconds: 1),
      maxPeriod: Duration(seconds: 1),
    )
        .listen((duration) {
      state.voiceTimeRemain.value = Duration(
          seconds:
              (_audioPlayer.duration?.inSeconds ?? 0) - duration.inSeconds);
      state.messages.refresh();
    });

    // get player processing state when stream audio
    _audioPlayer.playerStateStream.listen((playerState) {
      state.voiceProcessingState.value = playerState.processingState;
      if (playerState.processingState == ProcessingState.completed ||
          playerState.processingState == ProcessingState.idle) {
        state.voiceUrlPlaying.value = null;
        state.voiceTimeRemain.value = Duration(seconds: 0);
      }
      state.messages.refresh();
    });
  }

  void _listenMessageChatFromSocket() {
    _messageChatSubscription?.cancel();
    _messageChatSubscription =
        GlobalEvent.instance.onReceiveMessageResponse.stream.listen((event) {
      print(
          "NamNH - _listenMessageChatFromSocket - conversation.originId: ${state.conversation.value.originId} - event: ${event.toJson()}");
      if (event.conversationId != state.conversation.value.originId) return;

      event.user = state.users.value
          .where((element) => element.code == event.sender)
          .firstOrNull;
      List<MessageChat> newMessages = [event]..addAll(state.messages.value);
      state.messages.value = newMessages;
      _updatePollMessages([event]);
      _chatsDAO.put(event);

      /// mỗi lần có tin nhắn mới mà đang ở trong chat thì coi như clear unread luôn
      GlobalEvent.instance.onUpdateUnreadResponse.add(state.conversation.value);

      /// mỗi lần có ai đó cập nhật background thì mình cũng cập nhật lại
      if (event.type == MessageChatType.changeBackground) {
        String groupBackground =
            event.config?.data?.background?.groupBackground ?? "";
        String privateBackground =
            event.config?.data?.background?.privateBackground ?? "";
        state.conversation.value.config.groupBackground = groupBackground;
        state.conversation.value.config.privateBackground = privateBackground;

        /// ưu tiên privateBackground hơn groupBackground
        _updateBackground(state.conversation.value);
      }
    });
  }

  void _listenMessageChatDeletedFromSocket() {
    _messageChatDeletedSubscription?.cancel();
    _messageChatDeletedSubscription = GlobalEvent
        .instance.onDeleteMessageResponse.stream
        .listen((event) async {
      print(
          "NamNH - _listenMessageChatDeletedFromSocket - event: ${event.toJson()}");
      state.messages.value = state.messages.value
        ..removeWhere((element) => element.uuid == event.uuid);
      state.messages.refresh();

      final conversations =
          await _conversationsDAO.getAllByOriginalId(event.conversationId);
      for (var conversation in conversations) {
        final chatsDAO = ChatsDAO(tableName: conversation.id);
        chatsDAO.delete(event);
        chatsDAO.closeBox();
      }
    });
  }

  Future<void> _getChatsCache() async {
    state.messagesCache = await _chatsDAO.getAll()
      ..sort(
        (a, b) => b.createDate.compareTo(a.createDate),
      );
    state.messages.value = []..addAll(state.messagesCache);
    _updatePollMessages(state.messagesCache);
  }

  void _getUsersProfile() async {
    if (state.conversation.value == null) return;

    final futures = <Future<ArrayDataResponse<UserV2Response>>>[];
    for (final userCode in state.conversation.value.userIds) {
      futures.add(_userV2Repository.getUser(userCode));
    }
    final listResponse = await Future.wait(futures);
    state.users.value = listResponse.map((e) {
      if (e.data.isNotEmpty) {
        return e.data.first;
      }
    }).toList();
  }

  void _watchConversation() {
    _conversationsDAO.watch(state.conversation.value.uuid).listen((event) {
      var needUpdateUsers = false;
      if (state.conversation.value.userIds.length != event.userIds.length) {
        needUpdateUsers = true;
      }

      state.conversation.value = event;

      if (needUpdateUsers) {
        _getUsersProfile();
      }
    });
  }

  void _checkBlockUser(UserV2Response user) {
    if (user == null) return;

    if (user.isBlocker) {
      DialogHelper.showAlertDialog(
        context: Get.context,
        title: S.of(Get.context).error,
        description: "Không thể nhắn tin do bạn đang bị chặn",
        okText: S.of(Get.context).ok,
        okFunction: () {
          Get.back();
        },
      );
    } else if (user.isBlocking) {
      DialogHelper.showAlertDialog(
        context: Get.context,
        title: S.of(Get.context).error,
        description: "Không thể nhắn tin do bạn đang chặn người này",
        okText: S.of(Get.context).ok,
        okFunction: () {
          Get.back();
        },
      );
    }
  }

  void _updatePollMessages(List<MessageChat> messages) async {
    final messageFiltered = messages
        .where((element) => element.type == MessageChatType.poll)
        .toList()
      ..sort(
        (a, b) => b.createDate.compareTo(a.createDate),
      );

    final newPollMessages = Map<String, MessageChat>();
    for (final message in messageFiltered) {
      final pollId = message.config.data.poll.id;
      if (pollId == null || pollId.isEmpty) {
        continue;
      }

      final pollMessage = state.pollMessages[pollId];
      if (pollMessage == null) {
        state.pollMessages[pollId] = message;
        newPollMessages[pollId] = message;
      } else {
        if (message.createDate.compareTo(pollMessage.createDate) == 1) {
          state.pollMessages[pollId] = message;
          newPollMessages[pollId] = message;
        }
      }
    }

    final updateCompleteNotifier = ValueNotifier(0);
    updateCompleteNotifier.addListener(() {
      if (updateCompleteNotifier.value == newPollMessages.keys.length) {
        state.messages.refresh();
      }
    });
    for (final key in newPollMessages.keys.toList()) {
      final pollMessage = state.pollMessages[key];
      if (pollMessage == null) {
        updateCompleteNotifier.value++;
        continue;
      }
      _messagePollRepository.getPoll(key).then((poll) {
        if (poll != null) {
          state.pollMessages[key] = pollMessage.copyWith(
            config: pollMessage.config.copyWith(
              data: pollMessage.config.data.copyWith(
                poll: poll,
              ),
            ),
          );
        }
        updateCompleteNotifier.value++;
      });
    }
  }

  // #endregion

  // #region [ Send message ]
  void sendText(String text) {
    if (state.replyingMessage.value != null) {
      _messageRepository.replyMessage(
        text,
        state.replyingMessage.value,
        state.conversation.value,
      );
      cancelReplyMessage();
    } else {
      _messageRepository.sendText(text, state.conversation.value);
    }
  }

  void sendImage(String imagePath) {
    _messageRepository.sendImage(imagePath, state.conversation.value);
  }

  void sendVideo(String videoPath) {
    _messageRepository.sendVideo(videoPath, state.conversation.value);
  }

  void sendVoice(String voicePath) {
    _messageRepository.sendVoice(voicePath, state.conversation.value);
  }

  void sendSticker(String stickerId) {
    _messageRepository.sendSticker(stickerId, state.conversation.value);
  }

  void sendGif(GifTypes gifTypes) {
    _messageRepository.sendGif(
      MessageGif(
        url: Utils.switchDomainTenor(gifTypes.url),
        width: gifTypes.dims.first ?? 0,
        height: gifTypes.dims.last ?? 0,
      ),
      state.conversation.value,
    );
  }

  void sendLocation(String locationUrl) {
    _messageRepository.sendLocation(locationUrl, state.conversation.value);
  }

  // #endregion

  void sendBackground(
      {Conversation conversation, String url, bool enableBothTwoSide}) {
    _messageRepository.changeBackground(conversation, url, enableBothTwoSide);
  }

  void openImagePicker() async {
    helper1.Helper.of(Get.context).openChooseImage(
        onCameraChoose: (String path) {
      state.previewBackgroundRx.value = path;
      Fluttertoast.showToast(
          msg: "openCamera ${path}", toastLength: Toast.LENGTH_LONG);
    }, onGalleryChoose: (String path) {
      state.previewBackgroundRx.value = path;
      Fluttertoast.showToast(
          msg: "openGallery ${path}", toastLength: Toast.LENGTH_LONG);
    });
  }

  Future<bool> updateBackground(
      {Conversation conversation, String path, bool enableBothTwoSide}) async {
    try {
      if (path.contains("http")) {
        PinnedConversationResponse response =
            await _conversationRepository.updateBackgroundImageWithoutUpload(
                conversation: conversation, url: path);
        if (response != null) {
          _doAfterChangeBG(
              conversation: conversation,
              path: path,
              enableBothTwoSide: enableBothTwoSide);
          return true;
        } else {
          ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
            content: Text("Cập nhật background thất bại"),
          ));
          return false;
        }
      }
      if (path == ChangeBgController.DEFAULT_BACKGROUND) {
        PinnedConversationResponse response =
            await _conversationRepository.updateBackgroundImageWithoutUpload(
                conversation: conversation, url: path);
        if (response != null) {
          _doAfterChangeBG(
              conversation: conversation,
              path: path,
              enableBothTwoSide: enableBothTwoSide);
          return true;
        } else {
          ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
            content: Text("Reset background thất bại"),
          ));
          return false;
        }
      } else {
        var image =
            await _conversationRepository.uploadFile(path, "group_avatar");
        if (image?.url?.isNotEmpty ?? false) {
          PinnedConversationResponse response = await _conversationRepository
              .updateBackgroundImage(conversation: conversation, path: path);
          if (response != null) {
            _doAfterChangeBG(
                conversation: conversation,
                path: image.url,
                enableBothTwoSide: enableBothTwoSide);
            return true;
          } else {
            ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
              content: Text("Cập nhật background thất bại"),
            ));
            return false;
          }
        } else {
          ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
            content: Text("Cập nhật background thất bại"),
          ));
        }
        return false;
      }
    } catch (e) {
      ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
        content: Text("Cập nhật background thất bại"),
      ));
      return false;
    }
  }

  void enablePreviewMode() {
    state.isPreviewModeRx.value = true;
    state.previewBackgroundRx.value = state.backgroundRx.value;
  }

  void disablePreviewMode() {
    state.isPreviewModeRx.value = false;
    state.previewBackgroundRx.value = state.backgroundRx.value;
  }

  @override
  ConversationDetailState createState() => ConversationDetailState();

  void _updateBackground(Conversation conversation) {
    String background = ChangeBgController.DEFAULT_BACKGROUND;
    if (state.isEmptyPrivateBg)
      background = conversation.config?.groupBackground;
    else if (state.isEmptyGroupBg)
      background = conversation.config?.privateBackground;
    else
      background = conversation.config?.privateBackground;

    state.backgroundRx.value =
        background ?? ChangeBgController.DEFAULT_BACKGROUND;
    state.previewBackgroundRx.value =
        background ?? ChangeBgController.DEFAULT_BACKGROUND;
  }

  void _doAfterChangeBG(
      {Conversation conversation, bool enableBothTwoSide, String path}) {
    if (enableBothTwoSide)
      conversation.config.groupBackground = path;
    else
      conversation.config.privateBackground = path;
    _conversationsDAO.put(conversation);
    GlobalEvent.instance.onUpdateBackgroundResponse.add(conversation);

    /// send background message
    if (enableBothTwoSide)
      sendBackground(
          conversation: conversation,
          url: path,
          enableBothTwoSide: enableBothTwoSide);
    ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
      content: Text("Cập nhật background thành công"),
    ));
  }

  void openFilePicker() async {
    FilePickerResult result = await FilePicker.platform
        .pickFiles(type: FileType.custom, allowedExtensions: docFileType);

    if (result != null) {
      PlatformFile file = result.files.first;

      print(file.name);
      print(file.bytes);
      print(file.size);
      print(file.extension);
      print(file.path);
      _messageRepository.sendFile(state.conversation.value, file.path);
      Fluttertoast.showToast(
          msg: "Đính kèm File thành công!",
          toastLength: Toast.LENGTH_SHORT,
          backgroundColor: AppColors.black,
          textColor: AppColors.white,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1);
    } else {
      // User canceled the picker
      Fluttertoast.showToast(
          msg: "User canceled the picker",
          toastLength: Toast.LENGTH_SHORT,
          backgroundColor: AppColors.black,
          textColor: AppColors.white,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1);
    }
  }

  void sendInitialize() async {
    _messageRepository.sendInitialization(state.conversation.value);
    // User canceled the picker
    Fluttertoast.showToast(
        msg: "Tạo mới cuộc trò chuyện thành công!",
        toastLength: Toast.LENGTH_SHORT,
        backgroundColor: AppColors.black,
        textColor: AppColors.white,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1);
  }
}
