import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mentions/flutter_mentions.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:images_picker/images_picker.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:tenor/tenor.dart';

import '../../../../main.dart';
import '../../../commons/app_colors.dart';
import '../../../commons/app_images.dart';
import '../../../elements/EmptyMessagesWidget.dart';
import '../../../elements/MediaActionSheetWidget.dart';
import '../../../models/conversation.dart';
import '../../../models/entities/message_chat/entities/message_chat.dart';
import '../../../models/entities/message_chat/enums/message_chat_type.dart';
import '../../../models/entities/sticker_entity.dart';
import '../../../models/route_argument.dart';
import '../../../repository/user_repository.dart';
import '../../../settings/global_variable.dart' as GlobalVar;
import '../../../utils/logger.dart';
import '../../../utils/media/map_utils.dart';
import '../../../utils/navigator_utils.dart';
import '../../../utils/utils.dart';
import '../../control_panel/control_panel_view.dart';
import '../../control_panel/unpin_panel/unpin_panel_view.dart';
import '../../group_edit.dart';
import '../../profile_user/profile_user_view.dart';
import '../chat_group_setting/bottom_sheet/change_bg_bottom_sheet.dart';
import '../chat_group_setting/params/chat_group_params.dart';
import '../forward_message/forward_message_view.dart';
import '../poll/poll_detail_dialog/poll_detail_dialog_view.dart';
import 'bottom_sheets/chat_item_bottom_sheet.dart';
import 'bottom_sheets/message_bottom_sheet.dart';
import 'bottom_sheets/profile_bottom_sheet.dart';
import 'bottom_sheets/text_style_bottom_sheet.dart';
import 'conversation_detail_logic.dart';
import 'conversation_detail_state.dart';
import 'download_dialog/download_dialog/download_dialog_view.dart';
import 'widgets/conversation_appbar.dart';
import 'widgets/message_input_widget.dart';
import 'widgets/message_item_widget.dart';

class ConversationDetailPage extends StatefulWidget {
  static const String ROUTE_NAME = '/ConversationDetailPage';

  final ValueChanged<Conversation> setConversation;
  final RouteArgument routeArgument;

  ConversationDetailPage({
    this.setConversation,
    this.routeArgument,
  });

  @override
  _ConversationDetailPageState createState() => _ConversationDetailPageState();
}

class _ConversationDetailPageState extends State<ConversationDetailPage> {
  ConversationDetailLogic controller = Get.put(ConversationDetailLogic());
  ConversationDetailState state = Get.find<ConversationDetailLogic>().state;

  final _myListKey = GlobalKey<AnimatedListState>();
  final mentionKey = GlobalKey<FlutterMentionsState>();
  MediaActionSheetWidget _mediaActionSheetWidget;

  Conversation get conversation => widget.routeArgument.param;

  @override
  void initState() {
    logger.d(
        "ConversationDetailPage - initState - conversation: ${conversation.toJson()}");
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.initialize(conversation);
      state.buildContext = context;
    });
    super.initState();
  }

  @override
  void dispose() {
    mentionKey.currentState?.controller?.dispose();
    Get.delete<ConversationDetailLogic>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _mediaActionSheetWidget = MediaActionSheetWidget(context: context);
    return GestureDetector(
      onTap: _hideKeyboard,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size(Get.width, 52),
          child: Obx(() {
            return ConversationAppBar(
              conversation: state.conversation.value,
              avatar: controller.getConversationAvatar(),
              onBackPressed: Navigator.of(context).pop,
              onTitlePressed: () {
                NavigatorUtils.navigateToSettingChatGroupWidget(
                  ChatGroupParams(
                      uuid: conversation.uuid,
                      onTapChangeBackground: _onTapChangeBackground),
                );
              },
              backgroundColor: Theme.of(Get.context).primaryColor,
              onSettingPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => GroupEditWidget(
                          conversation: state.conversation.value,
                          callback: () {
                            // controller.listenForChats(state.conversation.value);
                          },
                        )));
              },
              onCallPressed: controller.makeCall,
              onMenuPressed: /*_showChatMenu*/ () {
                NavigatorUtils.navigateToSettingChatGroupWidget(
                  ChatGroupParams(
                      uuid: conversation.uuid,
                      onTapChangeBackground: _onTapChangeBackground),
                );
              },
              onCallVideoPressed: controller.makeVideoCall,
            );
          }),
        ),
        body: SafeArea(
          bottom: true,
          top: false,
          child: _buildBodyWidget(),
        ),
      ),
    );
  }

  Widget _buildBodyWidget() {
    return Stack(
      children: [
        Positioned.fill(
          child: _buildBackground(),
        ),
        Positioned.fill(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Obx(() {
                return Visibility(
                  visible: state.hasPinMessageRx.value,
                  child: _buildPinHeader(),
                );
              }),
              Expanded(
                child: Obx(_buildChatList),
              ),
              Container(
                child: Obx(() {
                  return MessageInputWidget(
                    onMediaPressed: _addMedia,
                    onSendPressed: _onSendMessage,
                    onCancelReplyingPressed: controller.cancelReplyMessage,
                    replyingMessage: state.replyingMessage.value,
                    showSticker: state.isShowStickerInput.value,
                    onShowStickerPressed: controller.showStickerInput,
                    onCancelStickerPressed: controller.hideStickerInput,
                    onStickerPressed: _onSendSticker,
                    showGif: state.isShowGifInput.value,
                    onShowGifPressed: controller.showGifInput,
                    onCancelGifPressed: controller.hideGifInput,
                    onGifPressed: _onSendGif,
                    conversation: state.conversation.value,
                    mentionsKey: mentionKey,
                    onShowSendPressed: controller.showSendInput,
                    onCancelSendPressed: controller.hideSendInput,
                    showSend: state.isShowSendInput.value,
                    onMorePressed: _onMoreMessage,
                    onVoicePressed: _onVoicePressed,
                    showVoiceRecord: state.isShowVoiceRecord.value,
                    onSendVoice: (filePath) {
                      controller.sendVoice(filePath);
                    },
                    currentMessageTextStyle:
                        state.currentMessageTextStyleEntity.value,
                  );
                }),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildPinHeader() {
    return Column(
      children: [
        Container(
          height: 0.5,
          color: AppColors.grayLight,
        ),
        Container(
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            boxShadow: [
              BoxShadow(
                  color: Theme.of(context).hintColor.withOpacity(0.10),
                  offset: Offset(0.0, 0.75),
                  blurRadius: 10)
            ],
          ),
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 12),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset(AppImages.icGroupNormalPin, width: 15, height: 15),
              SizedBox(width: 16),
              Expanded(
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
                            style: TextStyle(
                                fontFamily: "Quicksand",
                                fontSize: 14,
                                fontWeight: FontWeight.bold,
                                color: AppColors.black),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                          SizedBox(height: 5),
                          Text(
                            "Ghim bởi Nguyễn Duy Tấn Phát",
                            style: TextStyle(
                                fontFamily: "Quicksand",
                                fontSize: 10,
                                fontWeight: FontWeight.normal,
                                color: AppColors.black),
                          ),
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        _onMorePinMessage();
                      },
                      child: Container(
                        padding: EdgeInsets.all(10),
                        child: Image.asset(
                          AppImages.icGroupDropdown,
                          width: 30,
                          height: 20,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildChatList() {
    if (state.messages.value.isNotEmpty) {
      return Obx(
        () => SmartRefresher(
          controller: state.refreshController,
          onLoading: () => controller.fetchMessages(),
          reverse: true,
          enablePullUp: state.canLoadmore.value,
          enablePullDown: false,
          child: ListView.builder(
            key: _myListKey,
            physics: const AlwaysScrollableScrollPhysics(),
            padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
            itemCount: state.messages.value.length,
            primary: true,
            itemBuilder: (context, index) {
              MessageChat _currentMessage = state.messages.value[index];
              MessageChat _previousMessage = null;
              if (index < state.messages.value.length - 1) {
                _previousMessage = state.messages.value[index + 1];
              }

              return MessageItemWidget(
                chat: _currentMessage,
                previousChat: _previousMessage,
                isGroupChat: state.conversation.value.type == "GROUP",
                voiceUrlPlaying: state.voiceUrlPlaying.value,
                voiceTimeRemain: state.voiceTimeRemain.value,
                voiceProcessingState: state.voiceProcessingState.value,
                pollMessages: state.pollMessages,
                onLongPressed: () {
                  showDialog(
                    context: context,
                    barrierDismissible: true,
                    builder: (context) {
                      return WillPopScope(
                          onWillPop: () {
                            return Future.value(true);
                          },
                          child: ControlPanelPage(
                            chat: _currentMessage,
                            onTapReply: _onTapReply,
                            onTapCopy: _onTapCopy,
                            onTapPinMessage: _onTapPinMessage,
                            onTapForward: _onTapForward,
                            onTapDelete: _onTapDelete,
                            onTapEditImage: _onTapEditImage,
                            onDownload: _handleDownloadFile,
                          ));
                    },
                  );
                  //_showMessageMenu(_currentMessage);
                },
                onUserTapped: (username, userId) {
                  _handleTapOnUserTag(username, userId);
                },
                onLocationPressed: (chat) {
                  MapUtils.openMap(
                    chat.config.data.location.lat,
                    chat.config.data.location.lng,
                  );
                },
                onFilePressed: (chat) {},
                onVoicePressed: controller.voicePressed,
                onPressedCall: controller.makeVideoCall,
                onPressedCallVoice: controller.makeCall,
                onPollTapped: _onPollMessageItemPressed,
              );
            },
          ),
        ),
      );
    } else {
      return EmptyMessagesWidget();
    }
  }

  void _showChatMenu() async {
    final mutedUsers = state.conversation.value?.mutedUsers ?? [];
    final isMuted = mutedUsers.contains(currentUser.value?.id ?? "");
    final result = await Get.bottomSheet(Container(
      padding: EdgeInsets.only(top: 10, bottom: Get.bottomBarHeight),
      decoration: BoxDecoration(
          color: Theme.of(context).scaffoldBackgroundColor,
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(20),
            topLeft: Radius.circular(20),
          )),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          (state.conversation.value.idAdmin == currentUser.value.id) ||
                  (state.conversation.value.users.length <= 2)
              ? ListTile(
                  leading: Icon(
                    Icons.delete_forever_outlined,
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                  minLeadingWidth: 0,
                  title: Text(
                    S.of(context).deleteChatHistory,
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1
                        .copyWith(fontWeight: FontWeight.bold),
                  ),
                  onTap: () {
                    Get.back(result: "delete");
                  },
                )
              : Container(),
          state.conversation.value.users.length == 2
              ? ListTile(
                  leading: Icon(
                    Icons.block,
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                  minLeadingWidth: 0,
                  title: Text(
                    S.of(context).block,
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1
                        .copyWith(fontWeight: FontWeight.bold),
                  ),
                  onTap: () {
                    Get.back(result: "block");
                  },
                )
              : Container(),
          ListTile(
            leading: Icon(
                isMuted
                    ? Icons.notifications_on_outlined
                    : Icons.notifications_off_outlined,
                color: Theme.of(context).colorScheme.secondary),
            minLeadingWidth: 0,
            title: Text(
              isMuted
                  ? S.of(context).turnOnNotification
                  : S.of(context).turnOffNotification,
              style: Theme.of(context)
                  .textTheme
                  .bodyText1
                  .copyWith(fontWeight: FontWeight.bold),
            ),
            onTap: () {
              isMuted
                  ? Get.back(result: "on_notification")
                  : Get.back(result: "off_notification");
            },
          ),
        ],
      ),
    ));
    if (result is String && result == "delete") {
      controller.removeConversation(
          conversationId: state.conversation.value.id);
      Get.back(result: "deleted");
    } else if (result is String && result == "block") {
      String userId = "";
      try {
        state.conversation.value.users
            .firstWhere((element) => element.id != currentUser.value.id);
      } catch (e) {}
      await controller.doBlockUser(userId);
      Get.back(result: "blocked");
    } else if (result is String && result == "on_notification") {
      controller.turnOnNotification();
    } else if (result is String && result == "off_notification") {
      controller.turnOffNotification();
    }
  }

  void _showMessageMenu(MessageChat chat) {
    MessageBottomSheet.showChatMenu(
      chat: chat,
      onCopyPressed: () {
        Clipboard.setData(ClipboardData(text: chat.content));
        ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
          content: Text(S.of(context).copied),
        ));
      },
      onDeletePressed: () {
        controller.deleteMessage(chat);
      },
      onReplyPressed: () {
        controller.replyMessage(chat);
      },
      onDownloadPressed: () {
        _handleDownloadFile(chat);
      },
      onForwardPressed: () {
        _handleForwardMessage(chat);
      },
    );
  }

  void _onMoreMessage() {
    _showChatItemMenu();
  }

  void _onVoicePressed() async {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
    if (state.isShowVoiceRecord.value) {
      controller.hideVoiceRecord();
    } else {
      controller.showVoiceRecord();
    }
  }

  void _handleDownloadFile(MessageChat chat) async {
    try {
      String url = null;
      FileType fileType = null;

      switch (chat.type) {
        case MessageChatType.image:
          url = chat.config.data.image.media.url;
          fileType = FileType.image;
          break;
        case MessageChatType.video:
          url = chat.config.data.video.url;
          fileType = FileType.video;
          break;
        default:
          return;
      }

      if (url == null || url.isEmpty) return;

      final mineType = url.split(".").last;
      final time = DateTime.now().microsecondsSinceEpoch;
      final fileName = fileType == FileType.video ? "video" : "image";
      final result = await Get.dialog(
        DownloadDialogPage(
          fileType: fileType,
          fileUrl: url,
          fileName: "findnear_${fileName}_${time}.${mineType}",
        ),
      );
      logger.d(result);
      if (result == "success") {
        ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
          content: Text(S.of(Get.context).success),
        ));
      } else if (result == "failure") {
        ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
          content: Text(S.of(Get.context).failure),
        ));
      }
    } catch (e) {
      ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
        content: Text(S.of(Get.context).failure),
      ));
    }
  }

  void _handleForwardMessage(MessageChat chat) {
    Get.bottomSheet(
      ForwardMessagePage(
        forwardMessage: chat,
      ),
    );
  }

  void _handleTapOnUserTag(String username, String userId) {
    logger.d("Open user: ${username} - ${userId}");
    if (userId == currentUser.value?.id) {
      return;
    }
    ProfileBottomSheet.show(
        userName: username,
        onOpenProfilePressed: () {
          NavigatorUtils.navigateToProfilePage(userId);
        });
  }

  void _onPollMessageItemPressed(MessageChat chat) {
    Get.dialog(
      PollDetailDialog(
        conversation: state.conversation.value,
        messagePoll:
            state.pollMessages[chat.config.data.poll.id].config.data.poll,
        users: state.users.value,
      ),
    );
  }

  void _onMorePinMessage() {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) {
        return WillPopScope(
            onWillPop: () {
              return Future.value(true);
            },
            child: UnpinPanel(
              onTapPinMessageList: _onTapPinMessageList,
              onTapUnpin: _onTapUnpin,
            ));
      },
    );
  }

  /// control panel
  void _onTapReply(MessageChat chat) {
    controller.replyMessage(chat);
  }

  void _onTapCopy(MessageChat chat) {
    Clipboard.setData(ClipboardData(text: chat.content));
    ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
      content: Text(S.of(context).copied),
    ));
  }

  void _onTapPinMessage(MessageChat chat) {
    controller.pinMessage();
  }

  void _onTapForward(MessageChat chat) {
    _handleForwardMessage(chat);
  }

  void _onTapDelete(MessageChat chat) {
    controller.deleteMessage(chat);
  }

  void _onTapEditImage(MessageChat chat) async {
    final imageUrl = chat.config.data.image.media.url;
    if (imageUrl == null || imageUrl.isEmpty) return;

    final imagePath = await NavigatorUtils.navigateToCropImageScreen(imageUrl);
    controller.sendImage(imagePath);
  }

  /// danh sách tin ghim

  void _onTapUnpin() {
    Get.back();
    controller.unpinMessage();
  }

  void _onTapPinMessageList() {
    Get.back();
    NavigatorUtils.navigateToPinnedMessage();
  }

  Future<String> _constructUrl() async {
    final currentPosition = await Utils.determinePosition();

    return Uri(
      scheme: 'https',
      host: 'maps.googleapis.com',
      // port: 443,
      path: '/maps/api/staticmap',
      queryParameters: {
        'center': '${currentPosition.latitude},${currentPosition.longitude}',
        'zoom': '15',
        'size': '700x500',
        'maptype': 'roadmap',
        'key': "AIzaSyBtaQfYohHQE01th0fmXvKLG-DvIvcBhrI",
        'markers':
            'color:red|size:large|label:Q|${currentPosition.latitude},${currentPosition.longitude}',
        // 'markers': 'https://vcdn-thethao.vnecdn.net/2021/06/23/16216632103250-jpeg-9167-1624407937.jpg|$lat,$long',
      },
    ).toString();
  }

  // ChatItemBottomSheet
  void _showChatItemMenu() {
    Get.bottomSheet(
      ChatItemBottomSheet(
        isGroup: state.conversation.value.type == "GROUP",
        onLocationPressed: () async {
          controller.sendLocation(await _constructUrl());
        },
        onAttachFilePressed: () {
          controller.openFilePicker();
        },
        onSchedulePressed: () {
          // controller.sendInitialize();
          Fluttertoast.showToast(
              msg: "Chức năng đang phát triển",
              toastLength: Toast.LENGTH_SHORT);
        },
        onVotePressed: () {
          NavigatorUtils.navigateToCreatePollScreen(state.conversation.value);
        },
        onCardPressed: () {
          NavigatorUtils.navigateToSendNameCardScreen(state.conversation.value);
        },
        onPaintPressed: () async {
          try {
            final String imagePath =
                await NavigatorUtils.navigateToDrawImageScreen();
            if (imagePath != null && imagePath.isNotEmpty == true) {
              controller.sendImage(imagePath);
            }
          } catch (e) {
            logger.e("ConversationDetailPage - onPaintPressed - error: $e");
          }
        },
        onFontPressed: () {
          TextStyleBottomSheet.show(
            currentMessageTextStyleEntity:
                state.currentMessageTextStyleEntity.value,
            onSelected: controller.changeMessageTextStyle,
          );
        },
        onGifPressed: () {
          mentionKey.currentState.controller.text = GlobalVar.gifTag;
        },
        onStickerPressed: () {
          controller.showStickerInput();
        },
      ),
    );
  }

  void _hideKeyboard() {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
      FocusManager.instance.primaryFocus.unfocus();
    }
  }

  void _onSendMessage() {
    print(mentionKey.currentState.controller.markupText);
    Timer(Duration(milliseconds: 100), () {
      mentionKey.currentState.controller.clear();
    });
    if (mentionKey.currentState.controller.markupText != '') {
      controller.sendText(mentionKey.currentState.controller.markupText);
    }
  }

  void _addMedia() async {
    FocusScopeNode currentFocus = FocusScope.of(context);

    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
    _mediaActionSheetWidget.showActionSheet(
      "Choose media",
      [
        S.of(context).chooseImage,
        S.of(context).chooseVideo,
      ],
      (int index) async {
        if (index == 0) {
          List<Media> res = await ImagesPicker.pick(
            pickType: PickType.image,
            language: Language.System,
            quality: 0.8,
          );
          controller.sendImage(res[0].path);
        }
        if (index == 1) {
          List<Media> res = await ImagesPicker.pick(
            pickType: PickType.video,
            language: Language.System,
            quality: 0.8,
          );
          controller.sendVideo(res[0].path);
        }
      },
    );
  }

  void _onSendSticker(StickerEntity sticker) {
    controller.sendSticker(sticker.id);
  }

  void _onSendGif(GifTypes gifTypes) {
    controller.sendGif(gifTypes);
  }

  void _onTapChangeBackground(String text) {
    controller.enablePreviewMode();
    ChangeBgBottomSheet.showChangeBgMenu(
      oldBgImage: state.backgroundRx.value,
      onCameraPressed: () {
        controller.openImagePicker();
      },
      onBackgroundPressed: (String url) {
        state.previewBackgroundRx.value = url;
        Fluttertoast.showToast(
            msg: "choose background ${url}", toastLength: Toast.LENGTH_LONG);
      },
      onClosePressed: () {
        controller.disablePreviewMode();
      },
      onSavePressed: (bool enableBothTwoSide) async {
        if (state.previewBackgroundRx.value.isNotEmpty) {
          bool result = await controller.updateBackground(
              conversation: state.conversation.value,
              path: state.previewBackgroundRx.value,
              enableBothTwoSide: enableBothTwoSide);
          if (result) {
            state.backgroundRx.value = state.previewBackgroundRx.value;
            controller.disablePreviewMode();
          } else {
            controller.disablePreviewMode();
          }
        } else {
          ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
            content: Text("Bạn chưa chọn ảnh nền cho cuộc trò chuyện!"),
          ));
        }
      },
    );
  }

  Widget _buildBackground() {
    return Obx(() {
      return Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: getImage(state.isPreviewModeRx.value
                ? state.previewBackgroundRx.value
                : state.backgroundRx.value),
            fit: BoxFit.cover,
          ),
        ),
      );
    });
  }

  ImageProvider getImage(url) {
    if (url.contains("http")) {
      return NetworkImage(url);
    } else {
      return FileImage(File(url));
    }
  }
}
