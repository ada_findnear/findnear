import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:just_audio/just_audio.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../base/base_state.dart';
import '../../../models/conversation.dart';
import '../../../models/entities/message_chat/entities/message_chat.dart';
import '../../../models/entities/message_chat/enums/message_text_style_type.dart';
import '../../../models/response/user_v2_response.dart';
import '../../../utils/local_data_manager.dart';
import '../chat_group_setting/bottom_sheet/change_bg_logic.dart';

class ConversationDetailState extends BaseState {
  // conversation
  final conversation = Rxn<Conversation>();

  // messages
  final messages = Rx<List<MessageChat>>([]);
  List<MessageChat> messagesCache = [];

  // message
  final replyingMessage = Rxn<MessageChat>();

  // pin message
  final hasPinMessageRx = false.obs;

  // input
  final isShowStickerInput = false.obs;
  final isShowGifInput = false.obs;
  final isShowSendInput = false.obs;
  final isShowVoiceRecord = false.obs;
  final currentMessageTextStyleEntity =
      MessageTextStyleEntity.fromMessageTextStyleType(
    Get.find<LocalDataManager>().getMessageTextStyle(),
  ).obs;

  // loadmore
  final currentPageIndex = 0.obs;
  final canLoadmore = true.obs;
  final refreshController = RefreshController();

  // users in conversation
  final users = Rx<List<UserV2Response>>([]);

  // voice
  final voiceUrlPlaying = "".obs;
  final voiceTimeRemain = Rx<Duration>(Duration(seconds: 0));
  final voiceProcessingState = Rx<ProcessingState>(ProcessingState.idle);

  // poll
  final pollMessages = Map<String, MessageChat>();

  // background
  RxString backgroundRx = ChangeBgController.DEFAULT_BACKGROUND.obs;
  // preview mode
  RxBool isPreviewModeRx = false.obs;
  RxString previewBackgroundRx = ChangeBgController.DEFAULT_BACKGROUND.obs;

  bool get isEmptyGroupBg =>
      (conversation.value.config?.groupBackground?.isEmpty ?? true) ||
      (conversation.value.config?.groupBackground ==
              ChangeBgController.DEFAULT_BACKGROUND ??
          false);
  bool get isEmptyPrivateBg =>
      (conversation.value.config?.privateBackground?.isEmpty ?? true) ||
      (conversation.value.config?.privateBackground ==
              ChangeBgController.DEFAULT_BACKGROUND ??
          false);

  BuildContext buildContext;

}
