import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../commons/app_images.dart';
import '../../../../widgets/common.dart';
import '../../../../widgets/touchable_opacity.dart';
import 'chat_item_tile.dart';

class ChatItemBottomSheet extends StatelessWidget {
  final bool isGroup;
  final Function onLocationPressed;
  final Function onAttachFilePressed;
  final Function onSchedulePressed;
  final Function onVotePressed;
  final Function onCardPressed;
  final Function onPaintPressed;
  final Function onFontPressed;
  final Function onGifPressed;
  final Function onStickerPressed;

  const ChatItemBottomSheet({
    this.isGroup = false,
    this.onLocationPressed,
    this.onAttachFilePressed,
    this.onSchedulePressed,
    this.onVotePressed,
    this.onCardPressed,
    this.onPaintPressed,
    this.onFontPressed,
    this.onGifPressed,
    this.onStickerPressed,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final datas = _createDatas()..removeWhere((e) => !e.visible);
    return Container(
      padding: EdgeInsets.only(
        top: 10,
        bottom: 10,
        left: 4,
        right: 4,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(20),
          topLeft: Radius.circular(20),
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            children: [
              Spacer(),
              TouchableOpacity(
                onTap: () => Get.back(),
                child: AppImage(
                  AppImages.icClose,
                  width: 29,
                ),
              ),
              SizedBox(width: 10),
            ],
          ),
          GridView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 4,
              crossAxisSpacing: 4,
              mainAxisSpacing: 4,
              childAspectRatio: 68 / 60,
            ),
            itemCount: datas.length,
            itemBuilder: (context, index) {
              final data = datas[index];
              return TouchableOpacity(
                onTap: () {
                  Get.back();
                  data.onTap?.call();
                },
                child: ChatItemTile(
                  icon: data.iconPath,
                  title: data.text,
                ),
              );
            },
          ),
        ],
      ),
    );
  }

  List<_ChatItemBottomSheetEntity> _createDatas() {
    return [
      _ChatItemBottomSheetEntity(
        text: "Vị trí",
        iconPath: AppImages.icChatMap,
        visible: true,
        onTap: onLocationPressed,
      ),
      _ChatItemBottomSheetEntity(
        text: "Tài liệu",
        iconPath: AppImages.icChatDocument,
        visible: true,
        onTap: onAttachFilePressed,
      ),
      _ChatItemBottomSheetEntity(
        text: "Nhắc hẹn",
        iconPath: AppImages.icChatSchedule,
        visible: true,
        onTap: onSchedulePressed,
      ),
      _ChatItemBottomSheetEntity(
        text: "Bình chọn",
        iconPath: AppImages.icChatVote,
        visible: isGroup,
        onTap: onVotePressed,
      ),
      _ChatItemBottomSheetEntity(
        text: "Danh thiếp",
        iconPath: AppImages.icChatCard,
        visible: true,
        onTap: onCardPressed,
      ),
      _ChatItemBottomSheetEntity(
        text: "Hình vẽ",
        iconPath: AppImages.icChatPaint,
        visible: true,
        onTap: onPaintPressed,
      ),
      _ChatItemBottomSheetEntity(
        text: "Kiểu chữ",
        iconPath: AppImages.icChatFont,
        visible: true,
        onTap: onFontPressed,
      ),
      _ChatItemBottomSheetEntity(
        text: "@GIF",
        iconPath: AppImages.icChatGif,
        visible: true,
        onTap: onGifPressed,
      ),
      _ChatItemBottomSheetEntity(
        text: "@STICKER",
        iconPath: AppImages.icChatSticker,
        visible: true,
        onTap: onStickerPressed,
      ),
    ];
  }
}

class _ChatItemBottomSheetEntity {
  String text;
  String iconPath;
  bool visible;
  Function onTap;

  _ChatItemBottomSheetEntity({
    @required this.text,
    @required this.iconPath,
    @required this.visible,
    @required this.onTap,
  });
}
