import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../generated/l10n.dart';
import '../../../../models/entities/message_chat/entities/message_chat.dart';
import '../../../../models/entities/message_chat/enums/message_chat_type.dart';
import '../../../../repository/user_repository.dart';
import 'menu_list_tile.dart';

class MessageBottomSheet {
  static void showChatMenu({
    MessageChat chat,
    VoidCallback onReplyPressed,
    VoidCallback onDeletePressed,
    VoidCallback onCopyPressed,
    VoidCallback onDownloadPressed,
    VoidCallback onForwardPressed,
  }) async {
    final currentUserId = currentUser.value?.id ?? "";
    final result = await Get.bottomSheet(Container(
      padding: EdgeInsets.only(top: 10, bottom: Get.bottomBarHeight),
      decoration: BoxDecoration(
          color: Theme.of(Get.context).scaffoldBackgroundColor,
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(20),
            topLeft: Radius.circular(20),
          )),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: []
          ..addAll([
            MenuListTile(
              icon: Icons.reply,
              title: S.of(Get.context).reply,
              onPressed: () {
                Get.back(result: "reply");
              },
            ),
            MenuListTile(
              icon: Icons.copy_rounded,
              title: S.of(Get.context).copy,
              onPressed: () {
                Get.back(result: "copy");
              },
            ),
            MenuListTile(
              icon: Icons.forward_rounded,
              title: S.of(Get.context).forward,
              onPressed: () {
                Get.back(result: "forward");
              },
            ),
          ])
          ..addIf(
              chat.user.id == currentUserId,
              MenuListTile(
                icon: Icons.delete_forever_rounded,
                title: S.of(Get.context).delete,
                onPressed: () {
                  Get.back(result: "delete");
                },
              ))
          ..addIf(
              chat.type == MessageChatType.image ||
                  chat.type == MessageChatType.video,
              MenuListTile(
                icon: Icons.save_alt_rounded,
                title: S.of(Get.context).download,
                onPressed: () {
                  Get.back(result: "download");
                },
              )),
      ),
    ));
    if (result is String) {
      if (result == "reply") {
        onReplyPressed?.call();
      } else if (result == "copy") {
        onCopyPressed?.call();
      } else if (result == "forward") {
        onForwardPressed?.call();
      } else if (result == "delete") {
        onDeletePressed?.call();
      } else if (result == "download") {
        onDownloadPressed?.call();
      }
    }
  }
}
