import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../commons/app_colors.dart';
import '../../../../commons/app_images.dart';
import '../../../../models/entities/message_chat/enums/message_text_style_type.dart';
import '../../../../widgets/common.dart';
import '../../../../widgets/touchable_opacity.dart';

class TextStyleBottomSheet {
  static void show({
    MessageTextStyleEntity currentMessageTextStyleEntity,
    Function(MessageTextStyleEntity) onSelected,
  }) async {
    final datasource = MessageTextStyleType.values
        .map((e) => MessageTextStyleEntity.fromMessageTextStyleType(e))
        .toList();

    final result = await Get.bottomSheet(Container(
      padding: EdgeInsets.only(
        top: 24,
        left: 12,
        right: 12,
        bottom: 36,
      ),
      decoration: BoxDecoration(
          color: Theme.of(Get.context).scaffoldBackgroundColor,
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(20),
            topLeft: Radius.circular(20),
          )),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            height: 28,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Text(
                      "Chọn kiểu chữ",
                      style: TextStyle(
                        color: Color(0xFF333333),
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                TouchableOpacity(
                  onTap: () => Get.back(),
                  child: AppImage(
                    AppImages.icClose,
                    width: 26,
                    height: 26,
                    color: AppColors.gray,
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: 16,
          ),
          GridView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 5,
                crossAxisSpacing: 6,
                mainAxisSpacing: 12,
                childAspectRatio: 66 / 35,
              ),
              itemCount: datasource.length,
              itemBuilder: (_, index) {
                final data = datasource[index];
                final isSelected = data.id == currentMessageTextStyleEntity.id;
                return GestureDetector(
                  onTap: () {
                    Get.back(result: data);
                  },
                  child: Container(
                    height: 36,
                    decoration: BoxDecoration(
                        border: Border.all(
                          color: isSelected
                              ? Theme.of(Get.context).colorScheme.secondary
                              : Color(0xFFCCCCCC),
                        ),
                        borderRadius: BorderRadius.all(
                          Radius.circular(6),
                        )),
                    child: Center(
                      child: Text(
                        "Hello",
                        style: TextStyle(
                          fontFamily: data.font,
                          color: data.color,
                          fontWeight: data.weight,
                        ),
                      ),
                    ),
                  ),
                );
              }),
        ],
      ),
    ));
    if (result is MessageTextStyleEntity) {
      onSelected?.call(result);
    }
  }
}
