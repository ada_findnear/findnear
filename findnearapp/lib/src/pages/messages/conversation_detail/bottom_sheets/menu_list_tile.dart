import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MenuListTile extends ListTile {
  MenuListTile({
    String title,
    IconData icon,
    VoidCallback onPressed,
  }) : super(
          leading: Icon(
            icon,
            color: Theme.of(Get.context).accentColor,
          ),
          minLeadingWidth: 0,
          title: Text(
            title ?? "",
            style: Theme.of(Get.context).textTheme.bodyText1.copyWith(fontWeight: FontWeight.bold),
          ),
          onTap: onPressed,
        );
}
