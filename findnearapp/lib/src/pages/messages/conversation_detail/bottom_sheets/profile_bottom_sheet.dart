import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/pages/messages/conversation_detail/bottom_sheets/menu_list_tile.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProfileBottomSheet {
  static void show({
    String userName,
    VoidCallback onOpenProfilePressed,
  }) async {
    final result = await Get.bottomSheet(Container(
      padding: EdgeInsets.only(top: 20, bottom: Get.bottomBarHeight),
      decoration: BoxDecoration(
          color: Theme.of(Get.context).scaffoldBackgroundColor,
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(20),
            topLeft: Radius.circular(20),
          )),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(userName, style: TextStyle(color: Colors.red, fontSize: 18, fontWeight: FontWeight.bold)),
          MenuListTile(
            icon: Icons.person_outline_rounded,
            title: S.of(Get.context).openProfile,
            onPressed: () {
              Get.back(result: "go_profile");
            },
          ),
        ],
      ),
    ));
    if (result is String) {
      if (result == "go_profile") {
        onOpenProfilePressed?.call();
      }
    }
  }
}
