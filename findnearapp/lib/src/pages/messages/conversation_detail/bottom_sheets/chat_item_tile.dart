import '../../../../commons/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChatItemTile extends StatelessWidget {
  final String title;
  final String icon;

  ChatItemTile({
    this.title,
    this.icon,
    Key key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(icon, width: 49, height: 49),
        SizedBox(
          height: 2,
        ),
        Text(
          title,
          maxLines: 2,
          style: TextStyle(
              fontFamily: "Quicksand",
              fontSize: 12,
              fontWeight: FontWeight.normal,
              color: AppColors.textBlack),
        ),
      ],
    );
  }
}
