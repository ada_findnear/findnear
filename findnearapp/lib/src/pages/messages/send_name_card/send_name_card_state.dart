import 'package:findnear/src/models/conversation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../base/base_state.dart';
import '../../../models/user.dart';

class SendNameCardState extends BaseState {
  Conversation conversation;

  final selectedFriends = Rx<List<User>>([]);
  final friends = Rx<List<User>>([]);

  // loadmore
  final canLoadmore = true.obs;
  final refreshController = RefreshController();

  // search
  var textSearchController = TextEditingController();
  final keywordSearch = "".obs;
}
