import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:getwidget/getwidget.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../../generated/l10n.dart';
import '../../../base/base_widget_state.dart';
import '../../../commons/app_colors.dart';
import '../../../commons/app_images.dart';
import '../../../models/route_argument.dart';
import '../../../models/user.dart';
import 'send_name_card_controller.dart';
import 'send_name_card_state.dart';

class SendNameCardPage extends StatefulWidget {
  static const String ROUTE_NAME = '/send_name_card';
  final RouteArgument routeArgument;

  const SendNameCardPage({Key key, this.routeArgument}) : super(key: key);

  @override
  _SendNameCardPageState createState() => _SendNameCardPageState();
}

class _SendNameCardPageState extends BaseWidgetState<SendNameCardPage,
    SendNameCardController, SendNameCardState> {
  @override
  void initState() {
    super.initState();
    state.conversation = widget.routeArgument.param;
  }

  @override
  void onReady() {
    super.onReady();
    state.isLoadingRx.value = true;
    controller.fetchFriends();
  }

  @override
  void dispose() {
    Get.delete<SendNameCardController>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        leading: IconButton(
          icon: Image.asset(
            AppImages.icBackDark,
            color: Theme.of(context).hintColor,
          ),
          onPressed: () {
            Navigator.of(context).pop(false);
          },
        ),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Gửi danh thiếp",
              style: TextStyle(
                fontFamily: 'Quicksand',
                fontSize: 16,
                color: AppColors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
            Obx(
              () => Text(
                "Đã chọn: ${state.selectedFriends.value.length}/${state.friends.value.length}",
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 12,
                  color: AppColors.black,
                  fontWeight: FontWeight.normal,
                ),
              ),
            ),
          ],
        ),
        actions: <Widget>[
          Obx(() {
            if (state.selectedFriends.value.isNotEmpty) {
              return GestureDetector(
                onTap: controller.sendFriendsNameCard,
                child: Container(
                  width: 72,
                  height: 28,
                  margin: EdgeInsets.only(right: 12),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(14.0),
                          color: const Color(0xffFF0000),
                        ),
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 7),
                        child: Center(
                          child: Text(
                            "Gửi",
                            style: TextStyle(
                              fontFamily: 'Quicksand',
                              fontSize: 12,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            } else {
              return SizedBox();
            }
          })
        ],
      ),
      body: SafeArea(
        bottom: true,
        top: false,
        child: SizedBox.expand(
          child: Container(
            child: Column(
              children: <Widget>[
                SizedBox(height: 20),
                _buildSearch(),
                SizedBox(height: 20),
                Obx(
                  () => Expanded(
                    child: state.isLoadingRx.value
                        ? Center(
                            child: CircularProgressIndicator(
                              color: Theme.of(context).colorScheme.secondary,
                            ),
                          )
                        : _buildListFriend(),
                  ),
                ),
                _buildSelectedFriends(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildSearch() {
    return Container(
      height: 40,
      margin: EdgeInsets.only(bottom: 2, left: 10, right: 10),
      decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border.all(
            color: Color(0xffCCCCCC),
          ),
          borderRadius: BorderRadius.circular(20)),
      child: TextField(
        controller: state.textSearchController,
        textInputAction: TextInputAction.search,
        style: TextStyle(
          fontFamily: 'Quicksand',
          fontSize: 14,
          fontWeight: FontWeight.normal,
          color: Color(0xff333333),
        ),
        decoration: new InputDecoration(
          border: InputBorder.none,
          focusedBorder: InputBorder.none,
          enabledBorder: InputBorder.none,
          errorBorder: InputBorder.none,
          disabledBorder: InputBorder.none,
          hintText: S.of(context).searchForFriends,
          prefixIcon: Icon(Icons.search, color: Theme.of(context).focusColor),
          hintStyle: TextStyle(
            fontFamily: 'Quicksand',
            fontSize: 14,
            fontWeight: FontWeight.normal,
            color: Color(0xff333333),
          ),
        ),
        onChanged: (value) {
          state.keywordSearch.value = value;
        },
      ),
    );
  }

  Widget _buildListFriend() {
    return Obx(
      () => state.friends.value.isNotEmpty
          ? SmartRefresher(
              controller: state.refreshController,
              onLoading: () => controller.fetchFriends(
                keyword: state.textSearchController.text,
              ),
              enablePullUp: state.canLoadmore.value,
              enablePullDown: false,
              child: ListView.separated(
                physics: const AlwaysScrollableScrollPhysics(),
                padding: EdgeInsets.symmetric(horizontal: 15),
                itemCount: state.friends.value.length,
                primary: true,
                itemBuilder: (context, index) {
                  final user = state.friends.value[index];
                  return Row(
                    children: [
                      _buildAvatarWidget(
                        user: user,
                        size: 41,
                      ),
                      SizedBox(width: 16),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              user.name,
                              style: TextStyle(
                                fontFamily: 'Quicksand',
                                fontSize: 14,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(height: 2),
                            Text(
                              "ID ${user.code}",
                              style: TextStyle(
                                fontFamily: 'Quicksand',
                                fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: 16),
                      Obx(
                        () => GFCheckbox(
                          size: 18,
                          value: state.selectedFriends.value
                              .where(
                                (element) => element.id == user.id,
                              )
                              .isNotEmpty,
                          type: GFCheckboxType.square,
                          inactiveBgColor: Colors.transparent,
                          inactiveBorderColor: Colors.transparent,
                          activeBorderColor: Colors.transparent,
                          activeBgColor: Colors.transparent,
                          activeIcon: Image.asset(AppImages.icCheckedBox),
                          inactiveIcon: Image.asset(AppImages.icUncheckedBox),
                          onChanged: (value) {
                            controller.selectedFriend(index, value);
                          },
                        ),
                      ),
                    ],
                  );
                },
                separatorBuilder: (context, index) => SizedBox(height: 12),
              ),
            )
          : Center(
              child: Text(
                state.textSearchController.text.isEmpty
                    ? "Không có danh sách bạn bè"
                    : "Không tìm thấy kết quả",
              ),
            ),
    );
  }

  Widget _buildAvatarWidget({
    User user,
    double size,
  }) {
    return SizedBox(
      width: size,
      height: size,
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(60)),
        child: CircleAvatar(
          foregroundImage: user.getAvatarImage(),
        ),
      ),
    );
  }

  Widget _buildSelectedFriends() {
    return Obx(
      () => state.selectedFriends.value.isNotEmpty
          ? Container(
              height: 52,
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    blurRadius: 6,
                    offset: Offset(0, 0),
                  )
                ],
              ),
              child: ListView.separated(
                scrollDirection: Axis.horizontal,
                padding: EdgeInsets.only(left: 20),
                itemCount: state.selectedFriends.value.length,
                itemBuilder: (context, index) {
                  final user = state.selectedFriends.value[index];
                  return GestureDetector(
                    onTap: () => {
                      controller.removeSelectedFriend(user),
                    },
                    child: Stack(
                      children: [
                        Center(
                          child: _buildAvatarWidget(
                            user: user,
                            size: 34,
                          ),
                        ),
                        Positioned(
                          right: 0,
                          top: 6,
                          child: Container(
                              width: 12,
                              height: 12,
                              decoration: BoxDecoration(
                                color: Colors.grey,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(100)),
                              ),
                              child: Icon(
                                Icons.close,
                                color: Colors.white,
                                size: 12,
                              )),
                        )
                      ],
                    ),
                  );
                },
                separatorBuilder: (context, index) => SizedBox(width: 12),
              ),
            )
          : Container(),
    );
  }

  @override
  SendNameCardController createController() =>
      Get.put(SendNameCardController());
}
