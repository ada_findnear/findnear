import 'package:get/get.dart';

import '../../../base/base_controller.dart';
import '../../../models/entities/message_chat/entities/message_name_card.dart';
import '../../../models/user.dart';
import '../../../repository/message_repository.dart';
import '../../../repository/user_v2_repository.dart';
import 'send_name_card_state.dart';

class SendNameCardController extends BaseController<SendNameCardState> {
  final _userV2Repository = Get.find<UserV2Repository>();
  final _messageRepository = Get.find<MessageRepository>();

  Worker _keywordSearchDebounce;

  @override
  void onInit() {
    super.onInit();
    _keywordSearchDebounce = debounce(
      state.keywordSearch,
      (value) => fetchFriends(keyword: value, isReload: true),
      time: Duration(milliseconds: 500),
    );
  }

  @override
  void onClose() {
    _keywordSearchDebounce.dispose();
    super.onClose();
  }

  Future<void> fetchFriends({
    String keyword = "",
    bool isReload = false,
  }) async {
    var offset = state.friends.value.length;

    if (isReload) {
      state.isLoadingRx.value = true;
      offset = 0;
      state.canLoadmore.value = true;
      state.friends.value = [];
    }

    if (state.canLoadmore.value != true) return;
    final response = await _userV2Repository.fetchFriends(
      keyword: keyword,
      currentOffset: offset,
    );

    if (response.data.isNotEmpty == true) {
      state.friends.value = state.friends.value..addAll(response.data);
      state.friends.refresh();
    } else {
      state.canLoadmore.value = false;
    }
    state.isLoadingRx.value = false;
    state.refreshController.loadComplete();
  }

  void selectedFriend(int index, bool isSelected) {
    final friend = state.friends.value[index];
    if (isSelected) {
      state.selectedFriends.value.add(friend);
    } else {
      state.selectedFriends.value
          .removeWhere((element) => element.id == friend.id);
    }
    state.selectedFriends.refresh();
  }

  void removeSelectedFriend(User user) {
    state.selectedFriends.value.removeWhere((element) => element.id == user.id);
    state.selectedFriends.refresh();
  }

  void sendFriendsNameCard() {
    if (state.conversation == null || state.selectedFriends.value.isEmpty)
      return;

    for (var friend in state.selectedFriends.value) {
      final messageNameCard = MessageNameCard(
        userId: friend.id,
        userCode: friend.code,
        avatarUrl: friend.image.icon,
        name: friend.name,
      );
      _messageRepository.sendNameCard(messageNameCard, state.conversation);
    }
    Get.back();
  }

  @override
  SendNameCardState createState() => SendNameCardState();
}
