import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../controllers/profile_controller.dart';
import '../elements/AddFriendWidget.dart';
import '../elements/AlbumItemWidget.dart';
import '../elements/DrawerWidget.dart';
import '../elements/MediaActionSheetWidget.dart';
import '../elements/NoDataPostsWidget.dart';
import '../elements/PermissionDeniedWidget.dart';
import '../elements/PostsLoaderWidget.dart';
import '../elements/ProfileAvatarWidget2.dart';
import '../elements/ProfileWriteTopicWidget.dart';
import '../elements/TopicItemWidget.dart';
import '../models/post.dart';
import '../models/route_argument.dart';
import '../models/user.dart';
import '../repository/user_repository.dart';
import 'detail_topic.dart';
import 'preview_media.dart';
import 'profile_list_photo.dart';
import 'profile_list_video.dart';
import 'write_topic.dart';

class ProfileWidget extends StatefulWidget {
  final RouteArgument routeArgument;
  final GlobalKey<ScaffoldState> parentScaffoldKey;
  final bool showDrawer;

  ProfileWidget(
      {Key key,
      this.parentScaffoldKey,
      this.routeArgument,
      this.showDrawer = false})
      : super(key: key);

  @override
  _ProfileWidgetState createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends StateMVC<ProfileWidget> {
  ProfileController _con;
  MediaActionSheetWidget _mediaActionSheetWidget;
  User dataUser;
  bool isMyProfile;
  int userId;
  bool showBackgroundHeader = false;

  _ProfileWidgetState() : super(ProfileController()) {
    _con = controller;
  }

  @override
  initState() {
    super.initState();
    if (widget.routeArgument != null) {
      _con.listenForUser(widget.routeArgument.param, false);
      _con.listenForPosts(widget.routeArgument.param);
      userId = widget.routeArgument.param;
    } else if (currentUser.value?.apiToken != null) {
      _con.listenForUser(int.parse(currentUser.value?.id), true);
      _con.listenForPosts(int.parse(currentUser.value?.id));
      userId = int.parse(currentUser.value?.id);
    }
  }

  @override
  Widget build(BuildContext context) {
    _mediaActionSheetWidget = MediaActionSheetWidget(context: context);
    dataUser = widget.routeArgument == null ? currentUser.value : _con.userData;
    isMyProfile = widget.routeArgument == null ? true : false;

    return Scaffold(
      key: _con.scaffoldKey,
      drawer: DrawerWidget(),
      extendBodyBehindAppBar: currentUser.value?.apiToken == null ? false : true,
      appBar: AppBar(
        leading: new IconButton(
            icon: new Icon(widget.showDrawer ? Icons.sort : Icons.arrow_back,
                color: currentUser.value?.apiToken != null
                    ? Theme.of(context).primaryColor
                    : Theme.of(context).hintColor),
            onPressed: () {
              widget.showDrawer
                  ? widget.parentScaffoldKey.currentState.openDrawer()
                  : Navigator.of(context)
                      .pushReplacementNamed('/Pages', arguments: 2);
            }),
        automaticallyImplyLeading: false,
        backgroundColor: showBackgroundHeader
            ? Theme.of(context).accentColor
            : Colors.transparent.withAlpha(0),
        elevation: 0,
        centerTitle: true,
        title: currentUser.value?.apiToken == null
            ? Text(
                S.of(context).profile,
                overflow: TextOverflow.fade,
                maxLines: 1,
                style: Theme.of(context)
                    .textTheme
                    .headline6
                    .merge(TextStyle(letterSpacing: 1.3)),
              )
            : SizedBox(),
        actions: <Widget>[
          if (currentUser.value?.apiToken != null)
            if (isMyProfile)
              MaterialButton(
                elevation: 0,
                onPressed: () {
                  Navigator.of(context).pushNamed('/Settings');
                },
                child: Icon(
                  Icons.more_horiz,
                  color: Theme.of(context).primaryColor,
                  size: 28,
                ),
              )
            else
              SizedBox()
        ],
      ),
      body: currentUser.value?.apiToken == null
          ? PermissionDeniedWidget()
          : NotificationListener<ScrollNotification>(
              onNotification: (scrollNotification) {
                if (scrollNotification is ScrollEndNotification) {
                  setState(() {
                    showBackgroundHeader =
                        scrollNotification.metrics.pixels > 200;
                  });
                  if (scrollNotification.metrics.maxScrollExtent ==
                          scrollNotification.metrics.pixels &&
                      _con.canLoadMore &&
                      !_con.isLoading) {
                    load();
                  }
                }
                return true;
              },
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ProfileAvatarWidget2(
                      user: dataUser,
                      onEditBioSelected: isMyProfile
                          ? () {
                              _editBio();
                            }
                          : null,
                      onEditAvatarSelected: () {
                        isMyProfile ? _editAvatar() : _previewAvatar();
                      },
                      onEditCoverSelected: () {
                        isMyProfile ? _editCover() : _previewCover();
                      },
                    ),
                    _mediaContainer(),
                    if (isMyProfile)
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: _addTopicContainer(),
                      ),
                    _con.isLoadingPost
                        ? PostsLoaderWidget(type: "post")
                        : (_con.Posts.length == 0
                            ? NoDataPostsWidget(isMyProfile: this.isMyProfile)
                            : _timelineContainer()),
                    !_con.canLoadMore
                        ? SizedBox()
                        : Container(
                            padding: EdgeInsets.only(top: 20, bottom: 10),
                            child: Image.asset(
                              'assets/img/loading_load_more.gif',
                              height: 30,
                              width: 30,
                            ),
                          ),
                  ],
                ),
              ),
            ),
      floatingActionButton: isMyProfile == false
          ? new AddFriendWidget(
              userData: dataUser,
              onPressed: () {
                if (!dataUser.isFriend) {
                  if (dataUser.has_friend_request_from) {
                    _mediaActionSheetWidget.showActionSheet(
                        S.of(context).friendRequest,
                        [S.of(context).confirm, S.of(context).deny],
                        (int index) {
                      if (index == 0) {
                        _con.addFriend(dataUser.id, "confirm");
                      }
                      if (index == 1) {
                        _con.addFriend(dataUser.id, "deny");
                      }
                    });
                  } else if (dataUser.has_sent_friend_request_to) {
                    _con.addFriend(dataUser.id, "cancel");
                  } else {
                    _con.addFriend(dataUser.id, "add");
                  }
                } else {
                  _mediaActionSheetWidget.showActionSheet(
                      S.of(context).friend, [S.of(context).unfriend],
                      (int index) {
                    if (index == 0) {
                      showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                                title: Text(S.of(context).confirm),
                                content:
                                    Text(S.of(context).wantToDeleteThisFriend),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () =>
                                        Navigator.pop(context, 'Cancel'),
                                    child: Text(S.of(context).cancel),
                                  ),
                                  TextButton(
                                    onPressed: () => {
                                      Navigator.pop(context, 'OK'),
                                      _con.addFriend(dataUser.id, "delete")
                                    },
                                    child: Text(S.of(context).ok),
                                  ),
                                ],
                              ));
                    }
                  });
                }
              },
              iconColor: Theme.of(context).primaryColor,
              labelColor: Theme.of(context).hintColor)
          : SizedBox(),
    );
  }

  Widget _mediaContainer() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20.0),
      height: 90.0,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: [
          AlbumItemWidget(
            icon: Icons.photo,
            title: S.of(context).myPhoto,
            desc: S.of(context).seeAllPhoto,
            onSelected: () {
              _openMyPhotos(widget.routeArgument);
            },
          ),
          AlbumItemWidget(
            icon: Icons.videocam,
            title: S.of(context).myVideo,
            desc: S.of(context).seeAllVideo,
            onSelected: () {
              _openMyVideos(widget.routeArgument);
            },
          )
        ],
      ),
    );
  }

  Widget _addTopicContainer() {
    return GestureDetector(
      onTap: () {
        _writeTopic();
      },
      child: ProfileWriteTopicWidget(),
    );
  }

  Widget _timelineContainer() {
    return ListView.separated(
      padding: EdgeInsets.fromLTRB(15, 10, 15, 0),
      itemCount: _con.Posts.length,
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          onTap: () {
            _openDetailTopic(
                post: _con.Posts.elementAt(index),
                profileController: _con,
                userId: userId,
                index: index);
          },
          child: TopicItemWidget(
            onPreviewPhoto: (url) {
              _previewTopicPhoto(url);
            },
            onOpenSetting: () {
              _openTopicSetting(_con.Posts.elementAt(index));
            },
            openDetailTopic: () {
              _openDetailTopic(
                  post: _con.Posts.elementAt(index),
                  profileController: _con,
                  userId: userId,
                  index: index);
            },
            post: _con.Posts.elementAt(index),
            toggleLikePost: (postID) {
              _con.toggleLikePost(postID);
            },
            isMyProfile: isMyProfile,
          ),
        );
      },
      physics: const NeverScrollableScrollPhysics(),
      separatorBuilder: (BuildContext context, int index) => const Divider(),
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
    );
  }

  void load() async {
    _con.isLoading = true;
    if (widget.routeArgument != null) {
      await _con.listenForPosts(widget.routeArgument.param);
    } else {
      await _con.listenForPosts(int.parse(currentUser.value?.id));
    }
  }

  void _editAvatar() async {
    _mediaActionSheetWidget.showActionSheet(S.of(context).avatar, [
      S.of(context).viewProfilePicture,
      S.of(context).takePhoto,
      S.of(context).chooseFromGrallery
    ], (int index) {
      if (index == 0) {
        _previewAvatar();
      }
      if (index == 1) {
        _openCamera((pickedFile) {
          _con.update(
              user: dataUser, pathImage: pickedFile.path, imageType: 'avatar');
        });
      }
      if (index == 2) {
        _openGrallery((pickedFile) {
          _con.update(
              user: dataUser, pathImage: pickedFile.path, imageType: 'avatar');
        });
      }
    });
  }

  void _editBio() {
    Navigator.of(context)
        .push(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) => WriteTopicWidget(
          isWriteTopic: false,
          updateUser: () {
            _con.update(user: dataUser);
          },
        ),
      ),
    )
        .then((value) {
      // _con.update(user: dataUser);
    });
  }

  void _writeTopic() {
    Navigator.of(context)
        .push(
          CupertinoPageRoute(
            fullscreenDialog: true,
            builder: (context) => WriteTopicWidget(
              isWriteTopic: true,
              handleSubmit: (post, pathImages) {
                _con.createPost(post, pathImages);
              },
            ),
          ),
        )
        .then((value) {});
  }

  void _editTopic(Post post) {
    Navigator.of(context).push(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) => WriteTopicWidget(
            isWriteTopic: true,
            handleSubmit: (post, pathImages) {
              _con.updatePost(post);
            },
            currentPost: post),
      ),
    );
  }

  void _editCover() async {
    await _mediaActionSheetWidget.showActionSheet(S.of(context).cover, [
      S.of(context).viewFullCover,
      S.of(context).takePhoto,
      S.of(context).chooseFromGrallery
    ], (int index) {
      if (index == 0) {
        _previewCover();
      }
      if (index == 1) {
        _openCamera((pickedFile) {
          _con.update(
              user: dataUser, pathImage: pickedFile.path, imageType: 'cover');
        });
      }
      if (index == 2) {
        _openGrallery((pickedFile) {
          _con.update(
              user: dataUser, pathImage: pickedFile.path, imageType: 'cover');
        });
      }
    });
  }

  void _openTopicSetting(Post post) async {
    await _mediaActionSheetWidget.showActionSheet("Options", [
      S.of(context).privacy,
      S.of(context).editCaption,
      S.of(context).deleteActivity
    ], (int index) {
      if (index == 0) {
        _mediaActionSheetWidget.showActionSheet(S.of(context).privacy, [
          S.of(context).viewerPrivate,
          S.of(context).viewerOnlyFriend,
          S.of(context).viewerPublic
        ], (int index) {
          _con.updatePost(new Post.fromJSON(
              {'id': post.id, 'content': post.content, 'viewer': index}));
        });
      }
      if (index == 1) {
        _editTopic(post);
      }
      if (index == 2) {
        showDialog<String>(
            context: context,
            builder: (BuildContext context) => AlertDialog(
                  title: Text(S.of(context).confirm),
                  content: Text(S.of(context).confirmRemovePost),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () => Navigator.pop(context, 'Cancel'),
                      child: Text(S.of(context).cancel),
                    ),
                    TextButton(
                      onPressed: () => _submitRemovePost(post),
                      child: Text(S.of(context).ok),
                    ),
                  ],
                ));
      }
    });
  }

  void _submitRemovePost(post) {
    Navigator.pop(context, 'OK');
    _con.removePost(post);
  }

  void _openGrallery(OnPickImageCallback onPickImageCallback) {
    _mediaActionSheetWidget.pickImageDialog(ImageSource.gallery,
        (PickedFile pickedFile) {
      // setState(() {
      onPickImageCallback(pickedFile);
      // });
    });
  }

  void _openCamera(OnPickImageCallback onPickImageCallback) {
    _mediaActionSheetWidget.pickImageDialog(ImageSource.camera,
        (PickedFile pickedFile) {
      setState(() {
        onPickImageCallback(pickedFile);
      });
    });
  }

  void _previewTopicPhoto(url) {
    String path = url;
    print(url);
    Navigator.of(context).push(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) => PreviewMediaWidget(photoPath: path),
      ),
    );
  }

  void _previewAvatar() {
    Navigator.of(context).push(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) => PreviewMediaWidget(photoPath: dataUser.image.url),
      ),
    );
  }

  void _previewCover() {
    Navigator.of(context).push(CupertinoPageRoute(
      fullscreenDialog: true,
      builder: (context) =>
          PreviewMediaWidget(photoPath: dataUser.imageCover.url),
    ));
  }

  void _openMyPhotos(routeArgument) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) =>
            ProfileListPhotoWidget()));
  }

  void _openMyVideos(routeArgument) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) =>
            ProfileListVideoWidget()));
  }

  void _openDetailTopic({post, profileController, userId, index}) async {
    final result = await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext context) => DetailTopicWidget(
          post: post,
        ),
      ),
    );

    if (result != null) {
      await _con.listenForPost(post.id, index);
    }
  }

  @override
  bool get wantKeepAlive => true;
}
