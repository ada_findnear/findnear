import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/HomeSliderWidget.dart';
import 'package:findnear/src/elements/HotPlaceWidget.dart';
import 'package:findnear/src/elements/LivestreamItemWidget.dart';
import 'package:findnear/src/elements/LivestreamListWidget.dart';
import 'package:findnear/src/elements/SearchBarWidget.dart';
import 'package:findnear/src/elements/ServiceListWidget.dart';
import 'package:findnear/src/elements/RecommendListWidget.dart';
import 'package:findnear/src/elements/ShopItemWidget.dart';
import 'package:findnear/src/elements/TodaySuggestWidget.dart';
import 'package:findnear/src/models/field.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/models/shop.dart';
import 'package:findnear/src/pages/list_livestream/presenter/livestream_item_presenter.dart';
import 'package:findnear/src/pages/list_shop/view.dart';
import 'package:findnear/src/pages/notification/notification_list/notification_list_view.dart';
import 'package:findnear/src/pages/tab_map/widgets/notification_button.dart';
import 'package:findnear/src/repository/find_near_repository.dart';
import 'package:findnear/src/repository/livestream_repository.dart';
import 'package:findnear/src/repository/shop_repository.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/dialog/expired_account_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../settings/global_variable.dart' as GlobalVar;
import 'find_near_controller.dart';

class FindNearPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  FindNearPage({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _FindNearPageState createState() => _FindNearPageState();
}

class _FindNearPageState extends State<FindNearPage>
    with AutomaticKeepAliveClientMixin, TickerProviderStateMixin {
  FindNearController _controller;
  AnimationController _animController;
  Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    _controller = Get.put<FindNearController>(FindNearController(
        Get.find<FindNearRepository>(), Get.find<ShopsRepository>(), Get.find<LiveStreamRepository>()));
    _controller.getHasShop();
    _animController = AnimationController(
      duration: const Duration(milliseconds: 2000),
      vsync: this,
    )..repeat(reverse: true);
    _animation = CurvedAnimation(
      parent: _animController,
      curve: Curves.fastOutSlowIn,
    );
  }

  @override
  void dispose() {
    _animController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: _getAppBar(),
      body: _getBody(),
      floatingActionButton:
          ScaleTransition(scale: _animation, child: _registerButton()),
    );
  }

  AppBar _getAppBar() {
    return AppBar(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      centerTitle: true,
      leadingWidth: 52.w,
      leading: new IconButton(
        icon: SvgPicture.asset(
          AppImages.icHamburger,
          color: Theme.of(context).accentColor,
        ),
        onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
      ),
      titleSpacing: 0,
      title: SearchBarWidget(),
      actions: [
        Obx(() {
          return NotificationButton(
            onPressed: _showNotification,
            badgeCounter:
                _controller.notificationService.unreadNotificationCounter.value,
          );
        }),
      ],
    );
  }

  void _showNotification() {
    Get.to(() => NotificationListPage());
  }

  Widget _registerButton() {
    return Obx(() {
      var hasShop = _controller.hasShop.value;
      var canCreateShop = currentUser.value?.canCreateShop ?? false;
      if (_controller.isGettingShopInfo.value) {
        return SizedBox(width: 0, height: 0);
      } else {
        if (hasShop && canCreateShop) {
          return IconButton(
            iconSize: 72.w,
            icon: SvgPicture.asset(AppImages.icMyStore),
            onPressed: () {
              openShop();
            },
          );
        } else {
          return IconButton(
            iconSize: 72.w,
            icon: SvgPicture.asset(AppImages.icAddStore),
            onPressed: () {
              if (canCreateShop) {
                openRegisterShop();
              } else {
                showDialog(
                  context: context,
                  barrierDismissible: true,
                  builder: (BuildContext context) => contactUsDialog(),
                );
              }
            },
          );
        }
      }
    });
  }

  Widget _getBody() {
    var recommendForYou = S.of(context).recommend_for_you;
    var serviceNearMe = S.of(context).service_near_me;
    var findnearLive = "Findnear Live";
    return RefreshIndicator(
      onRefresh: _controller.refreshData,
      child: ListView(
        children: [
          Obx(() => HomeSliderWidget(slides: _controller.slidesListRx.value)),
          Obx(() =>
              HotPlaceWidget(_controller.fieldListRx.value, onNearMeItemClick)),
          LivestreamListWidget(
              findnearLive,
              onClickLivestreamLoadMore,
              onLivestreamItemClick,
          ),
          ServiceListWidget(serviceNearMe, onClickNearMeLoadMore, onItemClick),
          // ServiceListWidget2(
          //     recommendForYou, onClickSuggestLoadMore, onItemClick, (id) {}),
          RecommendListWidget(
              recommendForYou,
              onClickSuggestLoadMore,
              onItemClick, (int id){
                /// todo what are u doing here?
              },
          ),
          TodaySuggestWidget(
            slides: _controller.todaySuggestListRx.value,
            title: S.of(context).todaySuggestTitle,
            onClickReload: () {
              /// todo do something here
            },
          ),
          SizedBox(
            height: 76.w,
          )
        ],
      ),
    );
  }

  void onNearMeItemClick(Field field) {
    Navigator.of(context).pushNamed(ListShopPage.ROUTE_NAME,
        arguments: RouteArgument(param: {
          'type': ListShopType.hot,
          'lat': GlobalVar.current_location.latitude,
          'long': GlobalVar.current_location.longitude,
          'fieldID': field.id == null ? "" : field.id.toString(),
          'title': field.name, //todo
        }));
  }

  void onItemClick(ShopItemPresenter shop) {
    Shop _myshop = _controller.getMyShop();
    int shopId = shop.id;
    if (_myshop != null && _myshop.id == shop.id) {
      shopId = _myshop.id;
    }
    NavigatorUtils.toShopDetailPage(shopId: shopId);
  }

  void onLivestreamItemClick(LivestreamItemPresenter liveInfo) async{
    await _controller.getTokenAudience(liveInfo.userId);
  }

  void onClickNearMeLoadMore(String title) {
    Navigator.of(context).pushNamed(ListShopPage.ROUTE_NAME,
        arguments: RouteArgument(param: {
          'type': ListShopType.nearMe,
          'lat': GlobalVar.current_location.latitude, //todo
          'long': GlobalVar.current_location.longitude, //todo
        }));
  }

  void onClickSuggestLoadMore(String title) {
    Navigator.of(context).pushNamed(ListShopPage.ROUTE_NAME,
        arguments: RouteArgument(param: {
          'type': ListShopType.recommended,
        }));
  }

  void onClickLivestreamLoadMore(String title) {
    NavigatorUtils.navigateToListLivestreamPage();
  }

  void openRegisterShop() async {
    NavigatorUtils.toRegisterShopPage();
    _controller.getHasShop();
  }

  void openShop() {
    // Get.toNamed(ShopDetailPage.ROUTE_NAME,
    //     arguments: RouteArgument(param: _controller.getMyShop()));
    NavigatorUtils.toShopDetailPage(shopId: _controller.getMyShop().id);
  }

  @override
  bool get wantKeepAlive => true;
}
