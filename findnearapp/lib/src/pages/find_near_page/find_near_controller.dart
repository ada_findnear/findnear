import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/models/entities/live_token_entity.dart';
import 'package:findnear/src/models/entities/livestream_info_entity.dart';
import 'package:findnear/src/models/category.dart';
import 'package:findnear/src/models/enums/pop_enums.dart';
import 'package:findnear/src/models/field.dart';
import 'package:findnear/src/models/my_shop.dart';
import 'package:findnear/src/models/response/live_token_reponse.dart';
import 'package:findnear/src/models/recommend_type.dart';
import 'package:findnear/src/models/response/field_response.dart';
import 'package:findnear/src/models/response/my_shop_response.dart';
import 'package:findnear/src/models/shop.dart';
import 'package:findnear/src/models/slide.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/repository/find_near_repository.dart';
import 'package:findnear/src/repository/livestream_repository.dart';
import 'package:findnear/src/repository/shop_repository.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/services/cache_service.dart';
import 'package:findnear/src/services/notification_service.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

import '../../settings/global_variable.dart' as GlobalVar;
import '../../utils/utils.dart';
import 'package:rxdart/rxdart.dart' as RxDart;

class FindNearController extends GetxController {
  RxList<Slide> slidesListRx = <Slide>[].obs;
  RxList<Slide> todaySuggestListRx = <Slide>[].obs;
  Rxn<LiveTokenEntity> audienceToken = Rxn<LiveTokenEntity>();
  Rx<MyShopResponse> myShopResponseRx = MyShopResponse().obs;
  RxBool hasShop = false.obs;
  RxBool isGettingShopInfo = true.obs;
  RxList<Field> fieldListRx = <Field>[].obs;
  RxList<Shop> nearMeShopListRx = <Shop>[].obs;
  RxList<Shop> recommendedShopListRx = <Shop>[].obs;
  RxList<LiveStreamInfoEntity> livestreamListRx = <LiveStreamInfoEntity>[].obs;
  RxList<RecommendType> recommendTypes = <RecommendType>[].obs;
  RxList<Category> recommendedCategoryListRx = <Category>[].obs;
  FindNearRepository findNearRepository;
  ShopsRepository shopRepository;
  LiveStreamRepository livestreamRepository;
  User _currentUserLogin;
  Rx<Category> currentCategory = Category().obs;

  Rx<RecommendType> currentRecommend = RecommendType().obs;

  final NotificationService notificationService =
      Get.find<NotificationService>();
  final LocalDataManager localDataManager = Get.find();

  FindNearController(
      this.findNearRepository, this.shopRepository, this.livestreamRepository);

  void init() async {
    this._currentUserLogin = currentUser.value;
    refreshData();
  }

  Future<void> refreshData() async {
    Position current_position;
    current_position = await Utils.determinePosition();
    if (current_position != null) {
      GlobalVar.current_location = current_position;
    }

    await getForSlides();
    await getFields();
    await getNearMeShop();
    await getRecommendedShop();
    await getHasShop();
    await getForTodaySuggest();
    await getLivestreamShop();
    await getRecommend();
  }

  Future<void> getForSlides() async {
    final Stream<Slide> stream = await findNearRepository.getSlides();
    stream.listen((Slide _slide) {
      if (slidesListRx.indexOf(_slide) == -1) slidesListRx.add(_slide);
    }, onError: (a) {
      print(a);
    }, onDone: () {});
  }

  Future<void> getFields() async {
    final cacheService = Get.find<CacheService>();
    final response = await cacheService.shopFields.value;
    if (response != null) {
      fieldListRx.value = response;
    }
  }

  Future<void> getRecommend() async {
    final response = await shopRepository.getRecommendType();
    recommendTypes.value = response.data;
    if (recommendTypes.isNotEmpty) {
      currentRecommend.value = recommendTypes.first;
    }
  }

  Future<void> getHasShop() async {
    isGettingShopInfo.value = true;
    MyShopResponse myShopResponse = await findNearRepository.getHasShop();
    if (myShopResponse != null) {
      myShopResponseRx.value = myShopResponse;
      hasShop.value = true;
    } else {
      hasShop.value = false;
    }
    isGettingShopInfo.value = false;
  }

  Future<void> getNearMeShop() async {
    final response = await shopRepository.getNearShops(
        long: GlobalVar.current_location.longitude ?? 21.029832,
        lat: GlobalVar.current_location.latitude ?? 105.852605);
    nearMeShopListRx.clear();
    nearMeShopListRx.addAll(response?.data?.data);
  }

  Future<void> getRecommendedShop() async {
    final response = await shopRepository.getRecommendedShops(
        recommendTypeId: currentRecommend.value.id);
    recommendedShopListRx.clear();
    recommendedCategoryListRx.clear();
    recommendedShopListRx.addAll(response?.data?.data);
    recommendedCategoryListRx.addAll(response?.data?.categories ?? []);
  }

  Future<void> getLivestreamShop() async {
    final response = await livestreamRepository.getListLiveStream();
    livestreamListRx.value = []..addAll(response?.data ?? []);
  }

  bool isLogin() {
    return _currentUserLogin.apiToken != null;
  }

  Shop getMyShop() {
    MyShop _myShop = localDataManager.getMyShop();
    if (_myShop == null) {
      return null;
    }
    Shop shop = Shop();
    shop.id = _myShop.id;
    shop.title = _myShop.title;
    shop.street = _myShop.street;
    shop.media = _myShop.media;
    shop.field_id = _myShop.field_id;
    shop.district = _myShop.district;
    shop.ward = _myShop.ward;
    shop.city = _myShop.city;
    shop.description = _myShop.description;
    shop.media = _myShop.media;
    shop.vote = double.parse(_myShop.vote);
    return shop;
  }

  Future<void> getForTodaySuggest() async {
    final Stream<Slide> stream = await findNearRepository.getTodaySuggests();
    stream.listen((Slide _slide) {
      if (todaySuggestListRx.indexOf(_slide) == -1)
        todaySuggestListRx.add(_slide);
    }, onError: (a) {
      print(a);
    }, onDone: () {});
  }

  void getTokenAudience(int broadcasterId) async {
    DialogHelper.showLoaderDialog();
    RxDart.Rx.zip2(
        Stream.fromFuture(livestreamRepository.getTokenAudience(broadcasterId)),
        Stream.fromFuture(livestreamRepository.getRTMToken()),
        (broadcastToken, rtmToken) => [broadcastToken, rtmToken]).first.then(
      (value) {
        LiveTokenResponse token = value[0];
        LiveTokenResponse rtmToken = value[1];
        if (token.success && rtmToken.success) {
          audienceToken.value = token.data;
          NavigatorUtils.navigateToAudienceLivestreamWidget(
              token.data, broadcasterId, rtmToken.data, PopTarget.market.index);
        } else {
          AppSnackbar.showInfo(message: "Vào xem không thành công!");
        }
      },
    );
    Get.back();
  }

  void setCurrentCategory(RecommendType recommend) {
    currentRecommend.value = recommend;
    getRecommendedShop();
  }
}
