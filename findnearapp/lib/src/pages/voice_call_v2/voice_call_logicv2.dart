import 'dart:async';
import 'dart:io';

import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/global/global_data.dart';
import 'package:findnear/src/global/global_event.dart';
import 'package:findnear/src/models/entities/message_chat/entities/message_call.dart';
import 'package:findnear/src/models/entities/message_chat/enums/message_call_type.dart';
import 'package:findnear/src/models/search_user_entity.dart';
import 'package:findnear/src/pages/videocall/callsignaling.dart';
import 'package:findnear/src/pages/voice_call_v2/voice_call_statev2.dart';
import 'package:findnear/src/repository/market_repository.dart';
import 'package:findnear/src/repository/message_repository.dart';
import 'package:findnear/src/repository/notification_v2_repository.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/repository/user_v2_repository.dart';
import 'package:findnear/src/services/audio_app_services.dart';
import 'package:findnear/src/utils/listener_event_callkit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';
import 'package:get/get.dart';

class VoiceCallController extends BaseController<VoiceCallState> {
  CallSignaling signaling = Get.find<CallSignaling>();
  AudioAppService audioAppService = Get.find<AudioAppService>();
  MessageRepository messageRepository = Get.find<MessageRepository>();
  UserV2Repository userV2Repository = Get.find<UserV2Repository>();
  NotificationV2Repository notificationV2Repository =
      Get.find<NotificationV2Repository>();
  final eventCallKit = ListenerEventCallKit();

  @override
  VoiceCallState createState() => VoiceCallState();
  StreamSubscription _endCallSubscription;
  StreamSubscription _onCandidateCallSubscription;
  StreamSubscription _answerCallSubscription;
  StreamSubscription _readyCallSubscription;

  void initialize() async {
    try {
      await resetAudioCache();
      _listenEndCallFromSocket();
      _listenCandidateCallFromSocket();
      _listenAnswerCallFromSocket();
      _listenReadyCallFromSocket();
    } catch (e) {}
  }

  void connectWebrtc() async {
    await signaling.connect();
    await signaling.initRenderers();
  }

  @override
  void onClose() async {
    closeWebrtc();
    await resetAudioCache();
    closeSubscription();
    super.onClose();
  }

  void closeWebrtc() async {
    // dispose webrtc
    await signaling.close();
    await signaling.disposeRenderers();
  }

  void acceptCallFromCallKit() async {
    //FlutterCallkitIncoming.endAllCalls();
    state.isAnswer.value = false;
    await connectWebrtc();
    signaling?.userReceiverCall = currentUser.value.code;
    signaling?.userMakeCall = state.offerCallEntity.userMakeCall;
    signaling?.isReceiver = true;
    await signaling?.hanlderOffer(state.offerCallEntity);
    await signaling?.createAnswer();
    await resetAudioCache();
    state.isAnswer.value = true;
    state.isMic.value = true;
    state.isSpeaker.value = false;
    signaling.enableMic();
    signaling.disableSpeaker();
    startTimer();
  }

  void createCall() async {
    signaling?.isMakeCall = true;
    signaling?.userMakeCall = currentUser.value.code;
    signaling?.connectedUser = currentUser.value.code;
    signaling?.userReceiverCall = state.targetUser.code;
    await signaling?.invite(CallData.audio);
    // bắn socket update trạng thái đang trong cuộc gọi
    eventCallKit.updateUserStatus(1, currentUser.value.code, true);
  }

  void closeSubscription() {
    _endCallSubscription?.cancel();
    _onCandidateCallSubscription?.cancel();
    _answerCallSubscription?.cancel();
  }

  void searchUserFromCode(String code) async {
    state.isLoading = true;
    List<SearchUserEntity> listUser = <SearchUserEntity>[];

    final Stream<SearchUserEntity> stream = await getUserFromCode(code: code);
    stream.listen(
      (SearchUserEntity _market) {
        listUser.add(_market);
      },
      onError: (a) {
        state.isLoading = false;
      },
      onDone: () {
        listUser.forEach((element) {
          state.userName.value = element.name;
          state.userAvatar.value = element.media[0].thumb;
        });

        state.isLoading = false;
      },
    );
  }

  void getUserStatus(String code) async {
    state.statusUserCall = await userV2Repository.getUserStatus(code);
  }

  void startTimer() {
    state.callDuration.value = 0;
    state.timer?.cancel();
    state.timer = Timer.periodic(const Duration(seconds: 1), (Timer t) {
      state.callDuration.value += 1;
    });
  }

  void _listenEndCallFromSocket() async {
    await resetAudioCache();
    _endCallSubscription?.cancel();
    _endCallSubscription =
        GlobalEvent.instance.onEndVideoCall.stream.listen((data) async {
      state.isTimeOut.value = false;
      if (signaling.isMakeCall) {
        if (state.isAnswer.value) {
          // khi đã có sự kiện trả lời -> truyền time cuộc gọi
          sendMessage(state.callDuration.value, MessageCallStateType.accept);
          state.isAnswer.value = false;
          state.callStatus.value = "Kết thúc";
        } else {
          // khi chưa có sự kiện trả lời
          sendMessage(0, MessageCallStateType.cancel);
          state.callStatus.value = "Người nhận từ chối cuộc gọi.";
        }

        signaling.disableMic();
        signaling.disableSpeaker();
        closeSubscription();
        await signaling.handleBye();
        if(Platform.isIOS){
          GlobalData.instance.offer = null;
          FlutterCallkitIncoming.endAllCalls();
        }
        GlobalEvent.instance.onCloseOverlayCall.add(true);
      } else {
        state.isAnswer.value = false;
        state.callStatus.value = "Kết thúc";
        signaling.disableMic();
        signaling.disableSpeaker();
        closeSubscription();
        await signaling.handleBye();
        if(Platform.isIOS){
          GlobalData.instance.offer = null;
          FlutterCallkitIncoming.endAllCalls();
        }
        GlobalEvent.instance.onCloseOverlayCall.add(true);
      }
    });
  }

  void _listenCandidateCallFromSocket() {
    _onCandidateCallSubscription?.cancel();
    _onCandidateCallSubscription =
        GlobalEvent.instance.onCandidateCall.stream.listen((data) async {
      await signaling.handleCandidate(data);
    });
  }

  void _listenAnswerCallFromSocket() {
    _answerCallSubscription?.cancel();
    _answerCallSubscription =
        GlobalEvent.instance.onAnswerCall.stream.listen((data) async {
      await resetAudioCache();
      await signaling?.handleAnswer(data);
      state.isAnswer.value = true;
      state.isMic.value = true;
      state.isSpeaker.value = false;
      signaling.enableMic();
      signaling.disableSpeaker();
      startTimer();
    });
  }

  void _listenReadyCallFromSocket() {
    _readyCallSubscription?.cancel();
    _readyCallSubscription =
        GlobalEvent.instance.onReadyCall.stream.listen((data) async {
      try {
        final userMakeCall = data.userMakeCall;
        if (userMakeCall == currentUser.value.code) {
          if (state.isMakeCall.value == true) {
            if (state.statusUserCall.isCalling == true) {
              state.callStatus.value = "Người dùng bận";
              eventCallKit.updateUserStatus(1, currentUser.value.code, false);
              GlobalEvent.instance.onCloseOverlayCall.add(true);
            } else {
              state.callStatus.value = "Đang đổ chuông";
              if (GlobalData.instance.isCallShowing == true) {
                await createCall();
              }
            }
          }
        }
      } catch (e) {
        print("NamNH - _listenReadyCallFromSocket - error: $e");
      }
    });
  }

  void endCall() async {
    try {
      GlobalData.instance.isCallShowing = false;
      await resetAudioCache();
      state.isTimeOut.value = false;

      if(state.statusUserCall.devicePlatform == "IOS"){
        Get.find<MessageRepository>().sendMakeCallNotification(
          state.targetUser.code,
          "1e74092d-61a4-4e09-ad41-1adfa0609c5e",
          "END_CALL",
        );
      }

      if (signaling.isMakeCall) {
        if (state.isAnswer.value) {
          sendMessage(state.callDuration.value, MessageCallStateType.accept);
        } else {
          sendMessage(0, MessageCallStateType.cancel);
        }
      }
      eventCallKit.updateUserStatus(1, currentUser.value.code, false);
      await signaling?.bye();
      state.callStatus.value = "Kết thúc";
      signaling.disableMic();
      signaling.disableSpeaker();
      state.isAnswer.value = false;

      if(Platform.isIOS){
        GlobalData.instance.offer = null;
        FlutterCallkitIncoming.endAllCalls();
      }

    } catch (e) {
      print("TuanLA - $e");
    }
    GlobalEvent.instance.onCloseOverlayCall.add(true);
  }

  void delayCall(BuildContext context) {
    Future.delayed(Duration(seconds: 60)).then((value) async {
      if (state.isTimeOut.value && state.isAnswer.value == false) {
        // khi hết thời gian chờ nhận cuộc gọi
        sendMessage(0, MessageCallStateType.cancel);
        if(state.statusUserCall.devicePlatform == "IOS"){
          Get.find<MessageRepository>().sendMakeCallNotification(
            state.targetUser.code,
            "1e74092d-61a4-4e09-ad41-1adfa0609c5e",
            "END_CALL",
          );
        }
        eventCallKit.updateUserStatus(1, currentUser.value.code, false);
        await resetAudioCache();
        await signaling.bye();
        signaling.disableMic();
        signaling.disableSpeaker();
        state.callStatus.value = "Người nhận không bắt máy";
        GlobalEvent.instance.onCloseOverlayCall.add(true);
      }
    });
  }

  void sendMessage(int time, MessageCallStateType type) {
    messageRepository.sendCallMessage(
      MessageCall(
          messageCallType: MessageCallType.audio,
          messageCallStateType: type,
          duration: Duration(seconds: time)),
      state.conversation.value,
    );
  }

  void resetAudioCache() async {
    try {
      await audioAppService.audioCacheCallOut.clearAll();
      await audioAppService.advancedPlayerCallOut.stop();
    } catch (e) {
      print("TuanLA - resetAudioCache - $e");
    }
  }

  void loadAudio() async {
    try {
      audioAppService.loadAudioCallOut();
    } catch (e) {
      print("TuanLA -loadAudio - $e");
    }
  }
}
