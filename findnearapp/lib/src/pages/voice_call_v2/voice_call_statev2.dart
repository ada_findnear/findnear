import 'dart:async';

import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/entities/offer_call_entity.dart';
import 'package:findnear/src/models/entities/state_usercall.dart';
import 'package:get/get.dart';

class VoiceCallState extends BaseState{
  RxBool isMic = false.obs;
  RxBool isSpeaker = false.obs;
  RxBool isAnswer = false.obs; // Có cuộc gọi đến
  RxBool isMakeCall = false.obs; // người gọi

  final conversation = Rxn<Conversation>();
  RxBool isTimeOut = false.obs;
  RxString callStatus = "".obs;

  RxString userName = "".obs;
  RxString userAvatar = "".obs;
  RxBool isFromCallkit = false.obs;

  Timer timer;
  final callDuration = 0.obs;

  ShortUserEntity targetUser;
  OfferCallEntity offerCallEntity;
  StateUserCall statusUserCall;

  VideoCallState() {}

}