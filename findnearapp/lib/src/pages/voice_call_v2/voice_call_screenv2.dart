import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/extensions/duration_ext.dart';
import 'package:findnear/src/global/global_data.dart';
import 'package:findnear/src/global/global_event.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/pages/voice_call/widgets/sprite_painter.dart';
import 'package:findnear/src/pages/voice_call_v2/voice_call_logicv2.dart';
import 'package:findnear/src/pages/voice_call_v2/voice_call_statev2.dart';
import 'package:findnear/src/repository/message_repository.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/listener_event_callkit.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:tap_debouncer/tap_debouncer.dart';

class VoiceCallPage extends StatefulWidget {
  static const String ROUTE_NAME = '/VoiceCall';
  final RouteArgument routeArgument;
  final Function endCallCallback;

  VoiceCallPage({Key key, this.routeArgument, this.endCallCallback})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _VoiceCallPageState();
  }
}

class _VoiceCallPageState
    extends BaseWidgetState<VoiceCallPage, VoiceCallController, VoiceCallState>
    with SingleTickerProviderStateMixin {
  Function get callback => widget.routeArgument.param["callback"];
  final eventCallKit = ListenerEventCallKit();

  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      _controller = AnimationController(vsync: this);
      state.callStatus.value = "";
      state.userName.value = "";
      state.userAvatar.value = "";
      state.isAnswer.value = false;
      GlobalData.instance.isCallShowing = true;

      _startAnimation();
      state.targetUser = widget.routeArgument.param['targetUser'];
      state.isMakeCall.value = widget.routeArgument.param['isMakeCall'];
      state.isAnswer.value = widget.routeArgument.param['isAnswer'];
      state.conversation.value = widget.routeArgument.param['conversation'];
      state.isFromCallkit.value = widget.routeArgument.param['callkit'];

      controller.initialize();
      await controller.searchUserFromCode(state.targetUser.code);
      await controller.getUserStatus(state.targetUser.code);

      if (state.isFromCallkit.value == true) {
        state.offerCallEntity = widget.routeArgument.param['offerCallEntity'];
        controller.acceptCallFromCallKit();
      } else {
        state.callStatus.value = "Đang nối máy";
        if (state.isMakeCall.value == true) {
          await controller.connectWebrtc();
          await controller.loadAudio();
          state.isMic.value = true;
          state.isSpeaker.value = false;
          controller.signaling.enableMic();
          controller.signaling.disableSpeaker();

          if (!state.statusUserCall.status) {
            // user không online ==> kill app . send message thông báo có cuộc gọi
            Get.find<MessageRepository>().sendMakeCallNotification(
              state.targetUser.code,
              "1e74092d-61a4-4e09-ad41-1adfa0609c5e",
              "VOICE_CALL",
            );
            eventCallKit.updateUserStatus(1, currentUser.value.code, true);
          } else {
            // user online
            await Future.delayed(Duration(seconds: 1));
            if (state.statusUserCall.isCalling == true) {
              state.callStatus.value = "Người dùng bận";
              await Future.delayed(Duration(seconds: 1));
              widget.endCallCallback();
            } else {
              if (state.statusUserCall.devicePlatform == "IOS") {
                Get.find<MessageRepository>().sendMakeCallNotification(
                  state.targetUser.code,
                  "1e74092d-61a4-4e09-ad41-1adfa0609c5e",
                  "VOICE_CALL",
                );
              }
              state.callStatus.value = "Đang đổ chuông";
              await controller.createCall();
            }
          }
        }

        if (state.isAnswer.value) {
          controller.resetAudioCache();
          state.isMic.value = true;
          state.isSpeaker.value = false;
          controller.signaling.enableMic();
          controller.signaling.disableSpeaker();

          controller.startTimer();
        }
        // setup time delay
        state.isTimeOut.value = true;
        controller.delayCall(context);
      }

      GlobalEvent.instance.onCloseOverlayCall.stream.listen((data) async {
        if (data == true) {
          widget.endCallCallback();
        }
      });
    });
  }

  @override
  void deactivate() {
    super.deactivate();
  }

  @override
  void dispose() {
    GlobalData.instance.isCallShowing = false;
    controller.resetAudioCache();
    controller.signaling.disableMic();
    controller.signaling.disableSpeaker();
    state.timer.cancel();
    _controller.dispose();
    Get.delete<VoiceCallController>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: _buildLayoutCall(),
    );
  }

  Widget _buildLayoutCall() {
    return Scaffold(
        backgroundColor: Color(0xffFFB5B5),
        body: Stack(
          children: [
            Obx(() {
              return Container(
                child: _buildLayoutWaiting(),
              );
            }),

            Obx(() {
              return Positioned(
                left: 15,
                right: 15,
                top: ScreenUtil().statusBarHeight + 17,
                child: _buildLayoutTop(),
              );
            }),
            Obx(() {
              return Positioned(
                bottom: 55,
                left: 0,
                right: 0,
                child: _buildLayoutBottom(),
              );
            }),
          ],
        ));
  }

  Widget _buildLayoutWaiting() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: MediaQuery
              .of(context)
              .padding
              .top + 66),
          Container(
            width: 270,
            height: 270,
            child: Stack(
              alignment: Alignment.center,
              children: [
                CustomPaint(
                  painter: SpritePainter(_controller),
                  child: SizedBox(
                    width: 270.0,
                    height: 270.0,
                  ),
                ),
                Positioned(
                  child: Container(
                    width: 80,
                    height: 80,
                    child: CircleAvatar(
                      backgroundImage: NetworkImage(state.userAvatar.value ??
                          "${GlobalConfiguration().getValue(
                              'base_url')}images/image_default.png"),
                      radius: 80 / 2,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(80 / 2),
                      border: Border.all(color: Colors.white, width: 2),
                    ),
                  ),
                )
              ],
            ),
          ),
          state.isAnswer.value
              ? Center(
            child: Text(
              Duration(seconds: state.callDuration.value)
                  .toFormatString(separatorSymbol: ":"),
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w500,
                color: AppColors.white,
              ),
              textAlign: TextAlign.center,
            ),
          )
              : Text(
            state.callStatus.value,
            style: TextStyle(
                fontSize: 20,
                color: Colors.white,
                fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }

  void _startAnimation() {
    _controller.stop();
    _controller.reset();
    _controller.repeat(
      period: Duration(seconds: 1),
    );
  }

  Widget _buildLayoutTop() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: 47,
            ),
            Expanded(
              child: Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: 37,
                  padding: EdgeInsets.only(top: 0),
                  child: Text(
                    state.userName.value ?? "",
                    style: TextStyle(
                      overflow: TextOverflow.ellipsis,
                      fontSize: 18,
                      fontWeight: FontWeight.w800,
                      fontFamily: 'Quicksand',
                      color: Colors.white,
                    ),
                    maxLines: 1,
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            IconButton(
              iconSize: 37,
              padding: EdgeInsets.all(0),
              onPressed: null,
              icon: SvgPicture.asset(AppImages.icCallPip),
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildLayoutBottom() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Material(
          type: MaterialType.transparency,
          child: Ink(
            decoration: BoxDecoration(
              border: Border.all(
                  color: Colors.white, width: 1.0),
              shape: BoxShape.circle,
            ),
            child: InkWell(
              borderRadius: BorderRadius.circular(1000.0),
              child: Padding(
                padding: EdgeInsets.all(5.0),
                child: Icon(
                  state.isSpeaker.value ? Icons.volume_up: Icons.volume_off,
                  size: 37.0,
                  color: Colors.white,
                ),
              ),
              onTap: ()=> disableSpeaker(),
            ),
          ),
        ),
        SizedBox(
          width: 37,
        ),
        TapDebouncer(
          onTap: () async {
            await controller.endCall();
            await Future<void>.delayed(const Duration(milliseconds: 2000));
          }, // your tap handler moved here
          builder: (BuildContext context, TapDebouncerFunc onTap) {
            return IconButton(
              iconSize: 48,
              padding: EdgeInsets.all(0),
              onPressed: onTap,
              icon: SvgPicture.asset(AppImages.icCallVideoEnd),
            );
          },
        ),
        SizedBox(
          width: 37,
        ),
        Material(
          type: MaterialType.transparency,
          child: Ink(
            decoration: BoxDecoration(
              border: Border.all(
                  color: Colors.white, width: 1.0),
              shape: BoxShape.circle,
            ),
            child: InkWell(
              borderRadius: BorderRadius.circular(1000.0),
              child: Padding(
                padding: EdgeInsets.all(5.0),
                child: Icon(
                  state.isMic.value ? Icons.mic: Icons.mic_off,
                  size: 37.0,
                  color: Colors.white,
                ),
              ),
              onTap: ()=> disableMic(),
            ),
          ),
        ),
      ],
    );
  }

  void buildTimer() {}

  void disableMic() {
    if (state.isMic.value == false) {
      state.isMic.value = true;
      controller.signaling?.enableMic();
    } else {
      state.isMic.value = false;
      controller.signaling?.disableMic();
    }
  }

  void disableSpeaker() {
    if (state.isSpeaker.value == false) {
      state.isSpeaker.value = true;
      controller.signaling?.enableSpeaker();
    } else {
      state.isSpeaker.value = false;
      controller.signaling?.disableSpeaker();
    }
  }

  Widget _action({bool isActive, Function onTap, IconData icon}) {
    return Material(
      type: MaterialType.transparency,
      //Makes it usable on any background color, thanks @IanSmith
      child: Ink(
        decoration: BoxDecoration(
          border: Border.all(
              color: isActive ? Colors.white : Colors.black38, width: 2.0),
          shape: BoxShape.circle,
        ),
        child: InkWell(
          borderRadius: BorderRadius.circular(1000.0),
          child: Padding(
            padding: EdgeInsets.all(10.0),
            child: Icon(
              icon,
              size: 37.0,
              color: isActive ? Colors.white : Colors.black38,
            ),
          ),
          onTap: onTap,
        ),
      ),
    );
  }

  @override
  VoiceCallController createController() => Get.put(VoiceCallController());
}
