import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/entities/offer_call_entity.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class VoiceCallComingState extends BaseState{
  RxString userName = "".obs;
  RxString userAvatar = "".obs;
  RxString callStatus = "".obs;
  RxString errorStringPermission = "".obs;
  RxBool checkPermission = false.obs;
  OfferCallEntity targetUser;
  BuildContext buildContext;

  VideoCallComingState(){}
}