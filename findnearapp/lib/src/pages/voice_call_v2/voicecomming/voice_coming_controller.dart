import 'dart:async';

import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/global/global_event.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/search_user_entity.dart';
import 'package:findnear/src/pages/videocall/callsignaling.dart';
import 'package:findnear/src/pages/voice_call_v2/voicecomming/voice_comming_state.dart';
import 'package:findnear/src/repository/market_repository.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/services/audio_app_services.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';

class VoiceCallComingController extends BaseController<VoiceCallComingState> {
  CallSignaling signaling = Get.find<CallSignaling>();
  AudioAppService audioAppService = Get.find<AudioAppService>();

  StreamSubscription _endCallSubscription;
  StreamSubscription _onCandidateCallSubscription;

  @override
  VoiceCallComingState createState() {
    return VoiceCallComingState();
  }

  void initialize() async {
    try {
      resetAudioCache();
      //connect webrtc
      await connectWebrtc();
      // connect listen socket
      _listenEndCallFromSocket();
      _listenCandidateCallFromSocket();
    } catch (e) {}
  }

  void connectWebrtc() async{
    await signaling.connect();
    await signaling.initRenderers();
    signaling?.userReceiverCall = currentUser.value.code;
    signaling?.userMakeCall = state.targetUser.userMakeCall;
    signaling?.isReceiver = true;

    await signaling?.hanlderOffer(state.targetUser);
  }

  @override
  void onClose() async {
    await resetAudioCache();
    _endCallSubscription?.cancel();
    _onCandidateCallSubscription?.cancel();
    super.onClose();
  }

  void closeWebrtc()async {
    // dispose webrtc
    await signaling.close();
    await signaling.disposeRenderers();
  }

  void searchUserFromCode(String code) async {
    state.isLoading = true;
    List<SearchUserEntity> listUser = <SearchUserEntity>[];

    final Stream<SearchUserEntity> stream = await getUserFromCode(code: code);
    stream.listen(
      (SearchUserEntity _market) {
        listUser.add(_market);
      },
      onError: (a) {
        state.isLoading = false;
      },
      onDone: () {
        listUser.forEach((element) {
          state.userName.value = element.name;
          state.userAvatar.value = element.media[0].thumb;
        });

        state.isLoading = false;
      },
    );
  }

  void _listenEndCallFromSocket() {
    resetAudioCache();
    _endCallSubscription?.cancel();
    _endCallSubscription =
        GlobalEvent.instance.onEndVideoCall.stream.listen((data) async {
      if (signaling.isReceiver) {
        signaling.disableMic();
        signaling.disableSpeaker();
        _endCallSubscription?.cancel();
        await signaling.handleBye();
        state.callStatus.value = "Kết thúc";
        GlobalEvent.instance.onCloseOverlayCall.add(true);
      }
    });
  }

  void _listenCandidateCallFromSocket() {
    _onCandidateCallSubscription?.cancel();
    _onCandidateCallSubscription =
        GlobalEvent.instance.onCandidateCall.stream.listen((data) async {
      await signaling.handleCandidate(data);
    });
  }

  void acceptCall(String code) async {
    await resetAudioCache();
    await signaling?.createAnswer();
    NavigatorUtils.navigateToVoiceCallWidget(state.buildContext,
        ShortUserEntity(
            code: code,
            id: "",
            avatar: state.userAvatar.value),false,
        true,);
  }

  void checkPermission() async {
    // TODO xin cấp quyền
    Map<Permission, PermissionStatus> statuses = await [
      Permission.microphone,
      Permission.camera,
    ].request();

    if (statuses[Permission.microphone].isGranted &&
        statuses[Permission.camera].isGranted) {
      state.checkPermission.value = true;
    } else {
      state.checkPermission.value = false;
      if (statuses[Permission.microphone].isDenied ||
          statuses[Permission.microphone].isPermanentlyDenied) {
        state.errorStringPermission.value = "Bạn cần phải cấp quyền micro để thực hiện chức năng này";
      } else if (statuses[Permission.camera].isDenied ||
          statuses[Permission.camera].isPermanentlyDenied) {
        state.errorStringPermission.value = "Bạn cần phải cấp quyền máy ảnh để thực hiện chức năng này";
      } else if (statuses[Permission.microphone].isDenied &&
          statuses[Permission.camera].isDenied) {
        state.errorStringPermission.value =
        "Bạn cần phải cấp quyền micro và máy ảnh để thực hiện chức năng này";
      }
    }
  }

  void resetAudioCache() async {
    try{
      print("TuanLA - resetAudioCache - 1");
      await audioAppService.audioCacheCallComing.clearAll();
      print("TuanLA - resetAudioCache - 2");
      await audioAppService.advancedPlayerCallComing.stop();
    }catch(e){
      print("TuanLA - resetAudioCache - $e");
    }
  }

  void loadAudio() {
    try{
      audioAppService.loadAudioCall();
    }catch(e){
      print("TuanLA - loadAudio - $e");
    }
  }
}
