import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/settings/global_variable.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:url_launcher/url_launcher.dart';

class TermWidget extends StatelessWidget {
  final bool isAgreed;
  final ValueChanged<bool> onChanged;

  const TermWidget({
    Key key,
    this.isAgreed = false,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: SizedBox(
              width: 20,
              height: 20,
              child: Checkbox(
                value: isAgreed,
                onChanged: onChanged,
              ),
            ),
          ),
          SizedBox(width: 10),
          Expanded(
            child: Container(
              child: Text.rich(
                TextSpan(
                  children: [
                    TextSpan(
                        text: S.of(Get.context).term_and_policy_des1,
                        style: TextStyle(
                            fontFamily: "Quicksand",
                            fontSize: 14,
                            color: AppColors.black,
                            fontWeight: FontWeight.normal)),
                    TextSpan(
                      recognizer: new TapGestureRecognizer()
                        ..onTap = () async {
                          final privacyPolicyUrl =
                              "${GlobalConfiguration().getValue(privacyPolicy)}";
                          if (await canLaunch(privacyPolicyUrl)) {
                            await launch(privacyPolicyUrl);
                          }
                        },
                      text: S.of(Get.context).term_and_policy,
                      style: TextStyle(
                          fontFamily: "Quicksand",
                          fontSize: 14,
                          color: Colors.blue,
                          fontWeight: FontWeight.normal),
                    ),
                    TextSpan(
                        text: S.of(Get.context).term_and_policy_des2,
                        style: TextStyle(
                            fontFamily: "Quicksand",
                            fontSize: 14,
                            color: AppColors.black,
                            fontWeight: FontWeight.normal)),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
