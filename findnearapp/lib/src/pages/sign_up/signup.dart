import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/pages/sign_up/widgets/term_widget.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:velocity_x/velocity_x.dart';

import '../../../generated/l10n.dart';
import '../../controllers/user_controller.dart';
import '../../helpers/helper.dart';

class SignUpWidget extends StatefulWidget {
  static const String ROUTE_NAME = '/SignUp';

  @override
  _SignUpWidgetState createState() => _SignUpWidgetState();
}

class _SignUpWidgetState extends StateMVC<SignUpWidget> {
  UserController _con;

  _SignUpWidgetState() : super(UserController()) {
    _con = controller;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: Helper.of(context).onWillPop,
      child: Scaffold(
        key: _con.scaffoldKey,
         resizeToAvoidBottomInset: false,
        body: Stack(
          alignment: AlignmentDirectional.topCenter,
          children: <Widget>[
            Image.asset(
              AppImages.icLoginBg,
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.cover,
            ),
            VxBox(child: _itemNewRegister())
                .padding(EdgeInsets.only(
                    top: Get.mediaQuery.viewPadding.top,
                    bottom: Get.mediaQuery.viewPadding.bottom))
                .height(Get.height)
                .padding(EdgeInsets.only(
                    bottom: Get.context.mediaQueryViewInsets.bottom))
                .make()
                .onTap(() => FocusScope.of(context).unfocus()),
            Positioned(
              top: 60,
              left: 5,
              child: GestureDetector(
                onTap: () {
                  NavigatorUtils.navigateToPagesWidget();
                },
                child: Image.asset(
                  AppImages.icBackLight,
                  width: 40,
                  height: 40,
                  color: AppColors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _itemNewRegister() => SingleChildScrollView(
        child: Column(children: [
          _itemTopRegister(),
          Container(
              height: MediaQuery.of(context).size.height * 2 / 3,
              child: _itemBodyRegister()),
        ]),
      );

  Widget _itemTopRegister() => ZStack([
        AppImage(
          AppImages.icMarkLogin,
          fit: BoxFit.cover,
          width: Get.width,
        ),
        Align(
            child: AppImage(
              AppImages.icLogin,
              width: Get.width * 0.35,
            ),
            alignment: Alignment.center),
        S
            .of(Get.context)
            .register_acc
            .toUpperCase()
            .text
            .color(Vx.white)
            .size(18)
            .bold
            .make()
            .box
            .alignBottomCenter
            .make()
            .marginOnly(bottom: 8)
      ]).box.height(Get.height * 0.35).make();

  Widget _itemBodyRegister() {
    return Container(
      decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20)),
          boxShadow: [
            BoxShadow(
              blurRadius: 50,
              color: Theme.of(context).hintColor.withOpacity(0.2),
            )
          ]),
      padding: EdgeInsets.symmetric(horizontal: 32),
      width: Get.mediaQuery.size.width,
      child: Form(
        key: _con.loginFormKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 16),
            Text(
              S.of(Get.context).dealerCode,
              style: TextStyle(
                  fontFamily: "Quicksand",
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: AppColors.textBlack),
            ),
            SizedBox(height: 6),
            TextFormField(
              keyboardType: TextInputType.text,
              inputFormatters: [
                LengthLimitingTextInputFormatter(20),
              ],
              maxLength: 20,
              onSaved: (input) => _con.user.dealerCode = input,
              decoration: InputDecoration(
                labelText: S.of(context).register_acc_des1,
                labelStyle: TextStyle(
                    fontFamily: "Quicksand",
                    color: AppColors.gray.withOpacity(0.5)),
                contentPadding: EdgeInsets.all(12),
                hintText: 'AC1010101',
                hintStyle: TextStyle(
                    color: Theme.of(context).focusColor.withOpacity(0.7)),
                border: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.gray.withOpacity(0.5))),
                focusedBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.gray.withOpacity(0.5))),
                enabledBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.gray.withOpacity(0.5))),
              ),
            ),
            SizedBox(height: 26),
            Text(
              S.of(Get.context).phoneNumber,
              style: TextStyle(
                  fontFamily: "Quicksand",
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: AppColors.textBlack),
            ),
            SizedBox(height: 6),
            TextFormField(
              keyboardType: TextInputType.phone,
              onSaved: (input) => _con.user.phone = input,
              validator: (input) {
                print(input.startsWith('\+'));
                return !input.startsWith('\+') && !input.startsWith('0')
                    ? "Should be valid mobile number with country code"
                    : null;
              },
              decoration: InputDecoration(
                labelText: S.of(context).register_acc_des2,
                labelStyle: TextStyle(
                    fontFamily: "Quicksand",
                    color: AppColors.gray.withOpacity(0.5)),
                contentPadding: EdgeInsets.all(12),
                hintText: '0963125582',
                hintStyle: TextStyle(
                    color: Theme.of(context).focusColor.withOpacity(0.7)),
                border: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.gray.withOpacity(0.5))),
                focusedBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.gray.withOpacity(0.5))),
                enabledBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.gray.withOpacity(0.5))),
              ),
            ),
            SizedBox(height: 20),
            VxBox(
                    child: S
                        .of(Get.context)
                        .register_acc
                        .text
                        .size(16)
                        .bold
                        .color(AppColors.white)
                        .make())
                .roundedLg
                .alignCenter
                .color(AppColors.red)
                .width(Get.width)
                .padding(EdgeInsets.symmetric(vertical: 10))
                .make()
                .onTap(
              () {
                if (_con.isTermAgreed != true) {
                  AppSnackbar.showError(
                      title: S.of(context).error,
                      message: S.of(context).mustAcceptTermsAndCondition);
                  return;
                }

                if (_con.loginFormKey.currentState.validate()) {
                  _con.loginFormKey.currentState.save();
                  _con.register();
                }
              },
            ),
            SizedBox(height: 30),
            TermWidget(
              isAgreed: _con.isTermAgreed,
              onChanged: (value) {
                setState(() {
                  _con.isTermAgreed = value;
                });
              },
            ),
            Spacer(),
            Container(
              margin: EdgeInsets.only(bottom: 60),
              child: Center(
                child: Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                          text: S.of(Get.context).register_acc_des3,
                          style: TextStyle(
                              fontFamily: "Quicksand",
                              fontSize: 16,
                              color: AppColors.black,
                              fontWeight: FontWeight.bold)),
                      TextSpan(
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () {
                            Navigator.of(context).pushNamed('/Login');
                          },
                        text: S.of(Get.context).register_acc_des4,
                        style: TextStyle(
                            fontFamily: "Quicksand",
                            fontSize: 16,
                            color: AppColors.red,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
