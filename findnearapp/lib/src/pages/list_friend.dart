import 'package:findnear/src/controllers/friends_controller.dart';
import 'package:findnear/src/elements/FriendItemWidget.dart';
import 'package:findnear/src/helpers/loadmore_text_builder.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loadmore/loadmore.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import 'profile_user/profile_user_view.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ListFriendWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  ListFriendWidget({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _ListFriendWidgetState createState() => _ListFriendWidgetState();
}

class _ListFriendWidgetState extends StateMVC<ListFriendWidget> {
  String keyword = '';
  FriendsController _con;
  Widget currentPage;
  var _textController = TextEditingController();

  _ListFriendWidgetState() : super(FriendsController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios_outlined,
                color: Theme.of(context).hintColor),
            onPressed: () {
              Navigator.of(context).pop(false);
            },
          ),
          centerTitle: true,
          title: Text(S.of(context).friendList),
        ),
        body: currentPage ??
            SizedBox.expand(
              child: Container(
                child: Column(
                  children: <Widget>[
                    // _addNameAndImage(),
                    editTextSearch(),
                    Stack(
                      children: [
                        SingleChildScrollView(
                          padding: EdgeInsets.only(left: 0, right: 0),
                          child: Column(
                        children: [
                          getListFriend(),
                        ],
                          ),
                        ),
                        Visibility(
                          visible: !(_con.friends.length > 0),
                          child: Container(
                            height: MediaQuery.of(context).size.height-300,
                            child: Center(
                              child: Text(S.of(context).no_friends,
                                  style: TextStyle(
                                    color: Color(0xFF333333),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    fontFamily: 'Quicksand',
                                  )),
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ));
  }

  Widget editTextSearch() {
    return Container(
      padding:
          EdgeInsets.only(bottom: 20.w, left: 20.w, right: 20.w, top: 20.w),
      child: TextField(
        style: TextStyle(
          height: 1.0,
        ),
        controller: _textController,
        decoration: InputDecoration(
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            borderSide:
                new BorderSide(color: Theme.of(context).primaryColor, width: 0),
          ),
          enabledBorder: const OutlineInputBorder(
            // width: 0.0 produces a thin "hairline" border
            borderRadius: BorderRadius.all(Radius.circular(20)),
            borderSide: const BorderSide(color: Colors.black12, width: 1.0),
          ),
          border: OutlineInputBorder(),
          contentPadding: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
          hintText: S.of(context).searchName,
          hintStyle:
              TextStyle(fontSize: 16.sp, color: Theme.of(context).hintColor),
          fillColor: Theme.of(context).primaryColor,
          filled: true,
          prefixIcon: Icon(Icons.search),
        ),
        onChanged: (value) async {
          keyword = value != null ? value : '';
          await _con.setState(() {
            _con.currentOffset = 0;
            _con.canLoadMore = true;
          });
          await _con.listenForFriend(keyword: value, isClearData: true);
        },
      ),
    );
  }

  Widget getListFriend() {
    return SingleChildScrollView(
      child: LoadMore(
        isFinish: !_con.canLoadMore,
        child: ListView.separated(
          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
          itemCount: _con.friends.length,
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
                onTap: () {
                  NavigatorUtils.navigateToProfilePage(_con.friends.elementAt(index).id);
                },
                child: FriendItemWidget(
                    user: _con.friends.elementAt(index),
                    isListFriend: true,
                    onShowDialogUnfriend: (id) => {_showDialogUnFriend(id)},
                    onOpenChatWithFriend: (friend) =>
                        {_openChatWithFriend(friend)}));
          },
          physics: const NeverScrollableScrollPhysics(),
          separatorBuilder: (BuildContext context, int index) => Container(
            padding: EdgeInsets.only(left: 90.w),
            height: 3.w,
            child: Divider(color: Colors.grey),
          ),
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
        ),
        onLoadMore: _loadMore,
        whenEmptyLoad: false,
        delegate: DefaultLoadMoreDelegate(),
        textBuilder: CustomLoadMoreTextBuilder.getText,
      ),
    );
  }

  Future<bool> _loadMore() async {
    await Future.delayed(Duration(seconds: 2, milliseconds: 1000));
    await _con.listenForFriend(keyword: keyword);
    return true;
  }

  void _showDialogUnFriend(String id) async {
    showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
              title: Text(S.of(context).confirm),
              content: Text(S.of(context).unfriendConfirm),
              actions: <Widget>[
                TextButton(
                  onPressed: () => Navigator.pop(context, 'Cancel'),
                  child: Text(S.of(context).cancel),
                ),
                TextButton(
                  onPressed: () => {_submitUnfriend(id)},
                  child: Text(S.of(context).ok),
                ),
              ],
            ));
  }

  void _submitUnfriend(String id) {
    _con.unfriend(id, "delete");
    Navigator.pop(context, 'OK');
  }

  void _openChatWithFriend(friend) {
    NavigatorUtils.navigateToProfilePage(friend.id);
  }
}
