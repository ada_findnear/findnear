import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:get/get.dart';

import '../../generated/l10n.dart';

import '../elements/FriendItemWidget.dart';
import '../models/route_argument.dart';
import '../repository/user_repository.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../controllers/chat_controller.dart';
import '../models/conversation.dart';
import 'profile_user/profile_user_view.dart';

class GroupMembersWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;
  final Conversation conversation;

  GroupMembersWidget({Key key, this.parentScaffoldKey, this.conversation}) : super(key: key);

  @override
  _GroupMembersWidgetState createState() => _GroupMembersWidgetState();
}

class _GroupMembersWidgetState extends StateMVC<GroupMembersWidget> {
  ChatController _con;

  _GroupMembersWidgetState() : super(ChatController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
  }

  Widget conversationsList() {
    var users = widget.conversation.users.where((element) => widget.conversation.visibleToUsers.contains(element.id));
    return ListView.separated(
        itemCount: users.length,
        separatorBuilder: (context, index) {
          return SizedBox(height: 7);
        },
        shrinkWrap: true,
        primary: false,
        itemBuilder: (context, index) {
          return GestureDetector(
              onTap: () {
                print(users.elementAt(index).market);
                NavigatorUtils.navigateToProfilePage(users.elementAt(index).id);
              },
              child: FriendItemWidget(
                  user: users.elementAt(index),
                  isListFriend: true,
                  onRemoveUserInGroupChat: users.elementAt(index).id != currentUser.value?.id && widget.conversation.idAdmin == currentUser.value?.id
                      ? () {
                          removeUser(index);
                        }
                      : null
              )
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios_outlined, color: Theme.of(context).hintColor),
            onPressed: () {
              Navigator.of(context).pop(false);
            },
          ),
          centerTitle: true,
          title: Text(S.of(context).groupMembers)
      ),
      body: ListView(
        primary: false,
        children: <Widget>[
          conversationsList(),
        ],
      ),
    );
  }

  void removeUser(index) {
    showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: Text(S.of(context).confirm),
          content: Text(S.of(context).confirmRemoveChatGroupMember),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, 'Cancel'),
              child: Text(S.of(context).cancel),
            ),
            TextButton(
              onPressed: () => {
                Navigator.pop(context, 'OK'),
                _con.removeUser(conversation: widget.conversation, userID: widget.conversation.users.elementAt(index).id)
              },
              child: Text(S.of(context).ok),
            ),
          ],
        )
    );
  }
}
