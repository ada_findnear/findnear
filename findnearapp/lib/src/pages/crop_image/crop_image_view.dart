import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

import '../../../generated/l10n.dart';
import '../../base/base_widget_state.dart';
import '../../models/route_argument.dart';
import 'crop_image_controller.dart';
import 'crop_image_state.dart';

class CropImagePage extends StatefulWidget {
  static const String ROUTE_NAME = '/CropImage';
  final RouteArgument routeArgument;

  const CropImagePage({
    Key key,
    this.routeArgument,
  }) : super(key: key);

  @override
  _CropImagePageState createState() => _CropImagePageState();
}

class _CropImagePageState extends BaseWidgetState<CropImagePage,
    CropImageController, CropImageState> {
  double _ratioCrop = null;
  @override
  void initState() {
    super.initState();
    controller.setImageUrl(widget.routeArgument.param);
  }

  @override
  void dispose() {
    Get.delete<CropImageController>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.light,
      child: Scaffold(
        backgroundColor: Colors.black,
        body: SafeArea(
          child: Column(
            children: [
              _buildHeaderWidget(context),
              _buildImageWidget(),
              _buildFooterWidget(),
            ],
          ),
        ),
      ),
    );
  }

  Container _buildHeaderWidget(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 40,
      padding: EdgeInsets.symmetric(horizontal: 4),
      child: Material(
        color: Colors.black,
        child: Align(
          alignment: Alignment.centerRight,
          child: IconButton(
            onPressed: Get.back,
            splashColor: Colors.red,
            icon: Icon(
              Icons.close_rounded,
              color: Theme.of(context).primaryColor,
            ),
          ),
        ),
      ),
    );
  }

  Expanded _buildImageWidget() {
    return Expanded(
      child: Container(
        color: Colors.black,
        child: Center(
          child: ExtendedImage.network(
            controller.state.imageUrl,
            fit: BoxFit.contain,
            mode: ExtendedImageMode.editor,
            extendedImageEditorKey: controller.state.editorKey,
            initEditorConfigHandler: (_) => EditorConfig(
              maxScale: 8.0,
              cropRectPadding: EdgeInsets.all(20.0),
              hitTestSize: 20.0,
              cropAspectRatio: _ratioCrop,
            ),
          ),
        ),
      ),
    );
  }

  Container _buildFooterWidget() {
    return Container(
      width: double.infinity,
      height: 60,
      padding: EdgeInsets.symmetric(horizontal: 12),
      child: Material(
        color: Colors.black,
        child: Row(
          children: [
            SizedBox(
              width: 40,
            ),
            Expanded(
              child: Center(
                child: Wrap(
                  children: [
                    IconButton(
                      onPressed: controller.rotate,
                      splashColor: Colors.red,
                      icon: Icon(
                        Icons.rotate_left_outlined,
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    IconButton(
                      onPressed: () {
                        setState(() {
                          _ratioCrop == null
                              ? _ratioCrop = CropAspectRatios.ratio1_1
                              : _ratioCrop = null;
                        });
                      },
                      splashColor: Colors.red,
                      icon: Icon(
                        Icons.crop,
                        color: _ratioCrop == null
                            ? Theme.of(context).primaryColor
                            : Theme.of(context).colorScheme.secondary,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            ElevatedButton(
              onPressed: controller.send,
              child: Text(S.of(context).send),
              style: ElevatedButton.styleFrom(
                  shape: StadiumBorder(),
                  primary: Theme.of(context).colorScheme.secondary),
            ),
          ],
        ),
      ),
    );
  }

  @override
  CropImageController createController() => CropImageController();
}
