import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';

import '../../base/base_state.dart';

class CropImageState extends BaseState {
  final GlobalKey<ExtendedImageEditorState> editorKey =
      GlobalKey<ExtendedImageEditorState>();

  String imageUrl = "";
  bool isCropping = false;
}
