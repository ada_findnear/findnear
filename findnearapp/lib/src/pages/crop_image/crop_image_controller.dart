import 'dart:typed_data';

import 'package:extended_image/extended_image.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../base/base_controller.dart';
import '../../helpers/crop_editor_helper.dart';
import '../../utils/logger.dart';
import 'crop_image_state.dart';

class CropImageController extends BaseController<CropImageState> {
  @override
  CropImageState createState() => CropImageState();

  void setImageUrl(String imageUrl) {
    state.imageUrl = imageUrl ?? state.imageUrl;
  }

  void rotate() {
    state.editorKey.currentState.rotate(right: false);
  }

  void send() async {
    _cropImage();
  }

  void _cropImage() async {
    if (state.isCropping) return;

    try {
      state.isCropping = true;

      Uint8List fileData;

      fileData = await cropImageDataWithNativeLibrary(
        state: state.editorKey.currentState,
      );

      final directory = await getTemporaryDirectory();
      final file = File(directory.path + "/findnear/crop.jpg");

      if (!file.parent.existsSync()) {
        file.parent.createSync();
      }
      
      var cropFile = await file.writeAsBytes(fileData);

      logger.d("CropImageController - imagePath: ${cropFile.path}");
      Get.back(result: file.path);
    } catch (e, stack) {
      logger.e("CropImageController - save failed: $e\n $stack");
    }

    state.isCropping = false;
  }
}
