import 'package:flutter_google_places/flutter_google_places.dart';

import '../controllers/position_picker_controller.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:google_maps_webservice/places.dart';
import '../../generated/l10n.dart';
import '../elements/CircularLoadingWidget.dart';
import '../models/route_argument.dart';
import '../repository/settings_repository.dart' as settingRepo;

class PositionPickerWidget extends StatefulWidget {
  static const String ROUTE_NAME = '/PositionPicker';
  final RouteArgument routeArgument;
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  PositionPickerWidget({Key key, this.routeArgument, this.parentScaffoldKey}) : super(key: key);

  @override
  _PositionPickerWidgetState createState() => _PositionPickerWidgetState();
}

class _PositionPickerWidgetState extends StateMVC<PositionPickerWidget> {
  PositionPickerController _con;

  _PositionPickerWidgetState() : super(PositionPickerController()) {
    _con = controller;
  }

  @override
  void initState() {
    LatLng initPosition = widget.routeArgument?.param as LatLng;
    _con.setInitPosition(initPosition);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          title: Text(
            S.of(context).maps_explorer,
            style: Theme.of(context).textTheme.headline6.merge(TextStyle(letterSpacing: 1.3)),
          ),
        ),
        body: Stack(
          alignment: AlignmentDirectional.topStart,
          children: [
            Stack(
              alignment: AlignmentDirectional.bottomStart,
              children: <Widget>[
                _con.cameraPosition == null
                    ? CircularLoadingWidget(height: 0)
                    : GoogleMap(
                  mapToolbarEnabled: false,
                  mapType: MapType.normal,
                  initialCameraPosition: _con.cameraPosition,
                  markers: _con.markers,
                  onMapCreated: (GoogleMapController controller) {
                    _con.mapController.complete(controller);
                  },
                  onCameraMove: (CameraPosition cameraPosition) {
                    _con.cameraPosition = cameraPosition;
                  },
                  onCameraIdle: () {
                    _con.onCameraIdle();
                  },
                ),
                Container(
                  alignment: Alignment.bottomCenter,
                  padding: EdgeInsets.only(bottom: 50),
                  child: Container(
                    padding: EdgeInsets.all(10),
                    child: MaterialButton(
                      minWidth: 200,
                      elevation: 10,
                      onPressed: () {
                        Navigator.pop(context, _con.cameraPosition);
                      },
                      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 14),
                      color: Theme.of(context).accentColor,
                      shape: StadiumBorder(),
                      child: Text('OK', style: Theme.of(context).textTheme.bodyText1.copyWith(color: Colors.white)),
                    ),
                  ),
                ),
              ],
            ),
            Container(
                alignment: _con.searchPosition == null ? Alignment.topRight : Alignment.topCenter,
                height: 60,
                decoration: BoxDecoration(
                  boxShadow: _con.searchPosition != null ? [
                    BoxShadow(color: Theme.of(context).focusColor.withOpacity(0.3), blurRadius: 15, offset: Offset(0, 5)),
                  ] : null,
                ),
                child: Padding(
                    padding: EdgeInsets.fromLTRB(16, 16, 16, 0),
                    child: GestureDetector(
                      onTap: () async {
                        var language = await settingRepo.getDefaultLanguage(settingRepo.setting.value.mobileLanguage.value.languageCode);
                        Prediction p = await PlacesAutocomplete.show(
                            context: context,
                            apiKey: settingRepo.setting.value.googleMapsKey,
                            language: language,
                            location: Location.fromJson({
                              'lat': _con.cameraPosition?.target?.latitude != null ? _con.cameraPosition?.target?.latitude : 21.029832,
                              'lng': _con.cameraPosition?.target?.longitude != null ? _con.cameraPosition?.target?.longitude : 105.852605
                            }),
                            radius: 500,
                            mode: Mode.overlay,
                            startText: _con.searchPosition != null ? _con.searchPosition.description : '',
                            hint: S.of(context).search
                        );
                        _con.displayPrediction(p);
                      },
                      child: _con.searchPosition == null ?
                      Container(
                        padding: EdgeInsets.all(10),
                        child: Icon(Icons.search_outlined, color: Theme.of(context).primaryColor),
                        decoration: BoxDecoration(
                            color: Theme.of(context).accentColor,
                            borderRadius: BorderRadius.all(Radius.circular(25)),
                            boxShadow: [
                              BoxShadow(color: Theme.of(context).focusColor.withOpacity(0.8), blurRadius: 15, offset: Offset(0, 5)),
                            ]
                        ),
                      ) :
                      Container(
                        decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.all(Radius.circular(25)),
                          boxShadow: [
                            BoxShadow(color: Theme.of(context).focusColor.withOpacity(0.1), blurRadius: 15, offset: Offset(0, 5)),
                          ],
                        ),
                        child: Row(
                          children: [
                            SizedBox(
                              width: 10,
                            ),
                            Icon(Icons.location_on_sharp, color: Theme.of(context).accentColor),
                            SizedBox(
                              width: 20,
                            ),
                            Expanded(
                              child: Text(_con.searchPosition != null ? _con.searchPosition.description.split(', ')[0] : S.of(context).search, style: TextStyle(
                                  fontSize: 15.0,
                                  color: _con.searchPosition != null ? Theme.of(context).hintColor : Theme.of(context).focusColor
                              ),),
                            ),
                            VerticalDivider(),
                            if (_con.searchPosition != null)
                              IconButton(
                                icon: Icon(Icons.close, color: Theme.of(context).focusColor),
                                onPressed: () {
                                  setState(() {
                                    _con.searchPosition = null;
                                  });
                                },
                              ),
                            SizedBox(
                              width: 10,
                            ),
                          ],
                        ),
                      ),
                    )
                )
            ),
          ],
        )
    );
  }
}
