import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/my_blocking/widgeds/blocking_user_widget.dart';
import 'package:findnear/src/pages/profile_user/profile_user_view.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'my_blocking_logic.dart';
import 'my_blocking_state.dart';
import 'widgeds/user_loading_widget.dart';

class MyBlockingPage extends StatefulWidget {
  @override
  _MyBlockingPageState createState() => _MyBlockingPageState();
}

class _MyBlockingPageState extends State<MyBlockingPage> {
  final MyBlockingLogic logic = Get.put(MyBlockingLogic());
  final MyBlockingState state = Get.find<MyBlockingLogic>().state;

  @override
  void initState() {
    super.initState();
    logic.getMyBlocking();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: _buildBody(),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Theme.of(context).brightness == Brightness.dark
              ? Image.asset(
                  AppImages.icBackDark,
                )
              : Image.asset(
                  AppImages.icBackLight,
                )),
      title: Text(S.of(context).myBlocking),
    );
  }

  Widget _buildBody() {
    return Obx(() {
      if (state.loadDataStatus.value == LoadStatus.loading) {
        return UsersLoadingWidget();
      } else if (state.loadDataStatus.value == LoadStatus.success) {
        return state.users.length > 0
            ? ListView.builder(
                itemCount: state.users.length,
                itemBuilder: (context, index) {
                  return BlockingUserWidget(
                    user: state.users[index],
                    onPressed: () {
                      _openProfile(state.users[index]);
                    },
                    onMenuPressed: () {
                      _showUserMenu(state.users[index]);
                    },
                  );
                })
            : Center(
                child: Text(
                  S.of(context).no_friends_block,
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      fontFamily: 'Quicksand'),
                ),
              );
      } else {
        return Container();
      }
    });
  }

  void _openProfile(User user) {
    NavigatorUtils.navigateToProfilePage(user.id);
  }

  @override
  void dispose() {
    Get.delete<MyBlockingLogic>();
    super.dispose();
  }

  void _showUserMenu(User user) {
    Get.bottomSheet(Container(
      padding: EdgeInsets.only(top: 10),
      decoration: BoxDecoration(
          color: Theme.of(context).scaffoldBackgroundColor,
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(20),
            topLeft: Radius.circular(20),
          )),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListTile(
            leading: Icon(
              Icons.block_rounded,
              color: Theme.of(context).accentColor,
            ),
            minLeadingWidth: 0,
            title: Text(
              S.of(context).unblock,
              style: Theme.of(context).textTheme.bodyText1.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
            ),
            onTap: () {
              logic.unblockUser(user);
              Get.back();
            },
          )
        ],
      ),
    ));
  }
}
