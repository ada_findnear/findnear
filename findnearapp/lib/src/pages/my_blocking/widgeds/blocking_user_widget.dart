import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/market.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/repository/settings_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '/../../../generated/l10n.dart';

class BlockingUserWidget extends StatelessWidget {
  final User user;
  final VoidCallback onPressed;
  final VoidCallback onMenuPressed;

  BlockingUserWidget({
    Key key,
    this.user,
    this.onPressed,
    this.onMenuPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              SizedBox(width: 27),
              Container(
                width: 40,
                height: 40,
                child: CircleAvatar(
                  backgroundImage: NetworkImage(user.image?.thumb ?? ""),
                  radius: 45 / 2,
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(45 / 2),
                ),
              ),
              SizedBox(width: 10),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      user?.name ?? "",
                      overflow: TextOverflow.fade,
                      softWrap: false,
                      style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 14, fontWeight: FontWeight.bold),
                    ),
                    // SizedBox(height: 7),
                    // Row(
                    //   children: [
                    //     Expanded(
                    //       child: Container(
                    //         child: Text(
                    //           user.address ?? "",
                    //           maxLines: 1,
                    //           style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 12),
                    //         ),
                    //       ),
                    //     ),
                    //     SizedBox(width: 4),
                    //   ],
                    // ),
                  ],
                ),
              ),
              IconButton(
                onPressed: onMenuPressed,
                icon: Container(
                  width: 40,
                  height: double.infinity,
                  child: Center(
                    child: SvgPicture.asset(
                      "assets/svg/ic_menu.svg",
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                ),
              )
            ],
          ),
          SizedBox(height: 10),
          Divider(
            height: 1,
            color: Colors.grey,
            indent: 27,
          ),
        ],
      ),
    );
  }
}
