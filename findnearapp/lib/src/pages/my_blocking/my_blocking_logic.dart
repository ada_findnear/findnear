import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/user.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../repository/user_repository.dart' as repository;

import 'my_blocking_state.dart';

class MyBlockingLogic extends GetxController {
  final state = MyBlockingState();

  void getMyBlocking() async {
    state.loadDataStatus.value = LoadStatus.loading;
    final Stream<User> stream = await repository.getBlockingUsers();
    stream.listen(
      (User User) {
        state.users.add(User);
      },
      onError: (a) {
        state.loadDataStatus.value = LoadStatus.failure;
      },
      onDone: () {
        state.loadDataStatus.value = LoadStatus.success;
      },
    );
  }

  void unblockUser(User user) async {
    try {
      final userID = user.id;
      final User data = await repository.unblockUser(int.tryParse(userID));
      if (data != null) {
        state.users.removeWhere((element) {
          return element.id == data.id;
        });
        state.users.refresh();
        ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
          content: Text(S.of(Get.context).unblockUserSuccessfully),
        ));
      } else {
        ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
          content: Text(S.of(Get.context).unblockUserFailure),
        ));
      }
    } catch (e) {
      ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
        content: Text(S.of(Get.context).unblockUserFailure),
      ));
    }
  }
}
