import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/user.dart';
import 'package:get/get.dart';

class MyBlockingState {
  Rx<LoadStatus> loadDataStatus = LoadStatus.initial.obs;
  RxList<User> users = <User>[].obs;

  MyBlockingState() {
    ///Initialize variables
  }
}
