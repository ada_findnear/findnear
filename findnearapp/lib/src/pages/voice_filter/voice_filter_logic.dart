import 'package:get/get.dart';

import 'voice_filter_state.dart';

class VoiceFilterLogic extends GetxController {
  final state = VoiceFilterState();

  void setWords(String words) {
    state.words.value = words ?? "";
  }
}
