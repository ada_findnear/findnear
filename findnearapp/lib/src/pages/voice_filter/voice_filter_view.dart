import 'package:findnear/src/services/speech_to_text_service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'voice_filter_logic.dart';
import 'voice_filter_state.dart';
import 'package:speech_to_text/speech_recognition_result.dart';
import 'package:speech_to_text/speech_to_text.dart';

import 'widgets/ripple_animation_widget.dart';

class VoiceFilterPage extends StatefulWidget {
  @override
  _VoiceFilterPageState createState() => _VoiceFilterPageState();
}

class _VoiceFilterPageState extends State<VoiceFilterPage> {
  final VoiceFilterLogic logic = Get.put(VoiceFilterLogic());
  final VoiceFilterState state = Get.find<VoiceFilterLogic>().state;
  final SpeechToTextService sttService = Get.find<SpeechToTextService>();

  final bool didHaveResult = false;

  @override
  void initState() {
    super.initState();
    sttService.startListening(onResult: _resultListener);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(),
          Text(
            "Xin chào, FindNear đang nghe bạn…..",
            style: Theme.of(context).textTheme.bodyText2,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Center(
              child: Obx(() {
                return Text(
                  state.words.value,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline3,
                );
              }),
            ),
          ),
          Container(
            height: Get.width / 2 + 60,
            child: Stack(
              children: [
                Positioned(
                  right: 0,
                  left: 0,
                  bottom: -Get.width / 2 + 60,
                  child: Container(
                    width: Get.width,
                    height: Get.width,
                    child: Center(
                      child: Obx(() {
                        if (sttService.isListening.value) {
                          return RipplesAnimation(
                            color: Color(0xFFFFB5B5).withOpacity(0.3),
                            child: GestureDetector(
                              onTap: () {
                                Get.back();
                              },
                              child: Container(
                                width: 68,
                                height: 68,
                                child: Icon(
                                  Icons.close_rounded,
                                  color: Colors.white,
                                  size: 32,
                                ),
                                decoration: BoxDecoration(
                                  color: Colors.red,
                                  borderRadius: BorderRadius.all(Radius.circular(68 / 2)),
                                ),
                              ),
                            ),
                          );
                        } else {
                          return GestureDetector(
                            onTap: () {
                              sttService.startListening(onResult: _resultListener);
                            },
                            child: Container(
                              width: 68,
                              height: 68,
                              child: Icon(
                                Icons.mic_rounded,
                                color: Colors.white,
                                size: 32,
                              ),
                              decoration: BoxDecoration(
                                color: Colors.red,
                                borderRadius: BorderRadius.all(Radius.circular(68 / 2)),
                              ),
                            ),
                          );
                        }
                      }),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _resultListener(SpeechRecognitionResult result) async {
    if (didHaveResult) {
      return;
    }
    if (!result.finalResult && (result.recognizedWords ?? "").isNotEmpty) {
      logic.setWords(result.recognizedWords);
      return;
    }
    if (result.finalResult && (result.recognizedWords ?? "").isNotEmpty) {
      didHaveResult == true;
      sttService.stopListening();
      logic.setWords(result.recognizedWords);
      await Future.delayed(Duration(milliseconds: 400));
      Get.back(result: result.recognizedWords);
    }
  }

  @override
  void dispose() {
    sttService.stopListening();
    Get.delete<VoiceFilterLogic>();
    super.dispose();
  }
}
