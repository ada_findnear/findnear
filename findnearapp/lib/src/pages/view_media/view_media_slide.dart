import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/elements/CircularAvatarWidget.dart';
import 'package:findnear/src/helpers/date_utils.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/bottom_sheet_option.dart';
import 'package:findnear/src/models/media.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/pages/create_post/create_post.controller.dart';
import 'package:findnear/src/pages/create_post/create_post.screen.dart';
import 'package:findnear/src/pages/messages/conversation_detail/download_dialog/download_dialog/download_dialog_view.dart';
import 'package:findnear/src/pages/preview_video.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:findnear/src/utils/media/media_utils.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:photo_view/photo_view.dart';
import 'package:readmore/readmore.dart';

class ViewMediaSlide extends StatefulWidget {
  final Post _post;
  final int _clickedIndex;

  static void show(BuildContext context, Post post, int clickedIndex) {
    showGeneralDialog(
      barrierLabel: "Label",
      barrierDismissible: false,
      barrierColor: Colors.black.withOpacity(0.1),
      context: context,
      pageBuilder: (context, anim1, anim2) {
        return ViewMediaSlide(post, clickedIndex);
      },
    );
  }

  ViewMediaSlide(this._post, this._clickedIndex);

  @override
  State<StatefulWidget> createState() {
    return _ViewMediaSlideState();
  }
}

class _ViewMediaSlideState extends State<ViewMediaSlide> {
  PageController controller;

  @override
  void initState() {
    controller = PageController(initialPage: widget._clickedIndex);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      direction: DismissDirection.vertical,
      key: const Key('key'),
      onDismissed: (_) => Navigator.of(context).pop(),
      child: Scaffold(
        backgroundColor: Color(0xFF252525),
        body: SafeArea(
            child: Stack(
          children: [
            PageView.builder(
                controller: controller,
                itemBuilder: (context, index) {
                  var media = widget._post.medias[index];
                  return GestureDetector(
                    onLongPress: () async => await onLongPress(media),
                    child: media.isPhoto
                        ? CachedNetworkImage(
                            imageBuilder: (context, imageProvider) =>
                                PhotoView(imageProvider: imageProvider),
                            errorWidget: (context, s, i) => Container(
                                  width: double.infinity,
                                  alignment: Alignment.center,
                                  height: 160.w,
                                  child: Text(
                                    S.of(context).load_image_failed_message,
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                            placeholder: (context, url) => Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    CachedNetworkImage(
                                      imageUrl: media.thumb,
                                      errorWidget: (c, u, i) => Container(),
                                    ),
                                    Container(
                                        width: 24.w,
                                        height: 24.w,
                                        child: CircularProgressIndicator())
                                  ],
                                ),
                            imageUrl: widget._post.medias[index].url)
                        : PreviewVideoWidget(
                            path: media.url,
                            closable: false,
                            thumbUrl: media.thumb),
                  );
                },
                itemCount: widget._post.medias.length),
            Positioned(
              top: MediaQuery.of(context).viewPadding.top,
              left: 0,
              child: Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: () {
                        Get.back();
                      },
                      child: Text(
                        S.of(context).close_popup_button,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontFamily: 'Quicksand'),
                      ),
                    ),
                    Builder(
                      builder: (context) {
                        final dateTime =
                            DateTime.tryParse(widget._post.created_at ?? "");
                        return Text(
                          dateTime != null
                              ? AppDateFormatUtils
                                  .getVerboseDateTimeRepresentation(dateTime)
                              : "",
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1
                              .merge(TextStyle(
                                color: AppColors.white,
                                fontSize: 14,
                              )),
                        );
                      },
                    ),
                    InkWell(
                      onTap: () {
                        if (!isLoggedIn) {
                          NavigatorUtils.toLogin();
                          return;
                        }
                        onMoreMenuPress();
                      },
                      child: Icon(
                        Icons.more_horiz,
                        color: AppColors.white,
                        size: 30,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 157,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Colors.white.withOpacity(0.2),
                        Colors.black.withOpacity(0.2),
                        Colors.black.withOpacity(0.3)
                      ]),
                ),
                padding:
                    EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
                child: Column(
                  children: [
                    Container(
                      height: 52,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          CircularAvatarWidget(
                            user: widget._post?.creator,
                            size: 42,
                            hasBorder: widget._post?.creator?.isOnline,
                            hasLiveIndicator:
                                widget._post?.creator?.isLiveStreaming,
                          ),
                          const SizedBox(width: 9),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Text(
                                  Helper.limitString(
                                      widget._post?.creator?.name ?? "",
                                      limit: 20),
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline6
                                      .merge(TextStyle(
                                        color: AppColors.white,
                                        fontSize: 14,
                                        fontFamily: 'Quicksand',
                                      )),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                SizedBox(
                                  height: 6,
                                ),
                                Text(
                                  "ID: ${widget._post?.creator?.code ?? ""}",
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1
                                      .merge(TextStyle(
                                        color: AppColors.red400,
                                        fontSize: 12,
                                        fontFamily: 'Quicksand',
                                      )),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(width: 9),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Container(
                          child: SingleChildScrollView(
                            physics: BouncingScrollPhysics(),
                            child: Text(
                              widget._post?.content ?? "",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        )),
      ),
    );
  }

  void onLongPress(Media media) async {
    final indexTapped = await DialogHelper.showBottomSheetOptions(
      context: Get.context,
      options: [
        BottomSheetOption(icon: Icons.download, text: S.current.downloadMedia)
      ],
    );
    switch (indexTapped) {
      case 0:
        DialogHelper.showLoadingDialog();
        final result = await MediaUtils.saveMedia(media);
        Get.back();
        if (result) {
          AppSnackbar.showInfo(message: S.current.mediaSaved);
        }
        break;
    }
  }

  void onMoreMenuPress() async {
    onMyPostMenuPressed();
  }

  void onMyPostMenuPressed() async {
    final indexTapped = await DialogHelper.showBottomSheetOptions(
      context: context,
      options: [
        BottomSheetOption(
          icon: Icons.file_download,
          text: S.of(context).label_download,
        ),
        BottomSheetOption(
          iconPath: AppImages.icSharePost,
          text: S.of(context).label_share,
        ),
        BottomSheetOption(
          iconPath: AppImages.icWrite,
          text: S.of(context).label_post,
        ),
      ],
    );
    switch (indexTapped) {
      case 0:
        handleDownloadFile();
        break;
      case 1:
        break;
      case 2:
        //tạo bài post
        goToCreatePost();
        break;
    }
  }

  void goToCreatePost() async {
    DialogHelper.showAlertDialog(
      context: context,
      description: "Chức năng đang trong quá trình hoàn thiện",
      title: S.current.notifications,
      okText: 'OK',
      okFunction: () {
      },
    );
    // try {
    //   int index = widget._clickedIndex;
    //   Post post = widget._post;
    //   NavigatorUtils.navigateToCreatePostV2(user: currentUser.value,medias:[post.medias[index].toMediaPicker()]);
    // } catch (e) {
    //   print("TuanLA - $e");
    // }
  }

  void handleDownloadFile() async {
    try {
      int index = widget._clickedIndex;
      Post post = widget._post;
      final time = DateTime.now().microsecondsSinceEpoch;

      var mineType = "";
      var url = "";
      var fileType =
          post.medias[index].isVideo ? FileType.video : FileType.image;
      if (post.medias[index].isVideo) {
        url = post.medias[index].url;
        mineType = post.medias[index].url.split(".").last;
      } else {
        url = post.medias[index].thumb;
        mineType = post.medias[index].thumb.split(".").last;
      }

      final fileName = fileType == FileType.video ? "video" : "image";

      final result = await Get.dialog(
        DownloadDialogPage(
          fileType: fileType,
          fileUrl: url,
          fileName: "findnear_${fileName}_${time}.${mineType}",
        ),
      );
      logger.d(result);
      if (result == "success") {
        ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
          content: Text(S.of(Get.context).success),
        ));
      } else if (result == "failure") {
        ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
          content: Text(S.of(Get.context).failure),
        ));
      }
    } catch (e) {
      ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
        content: Text(S.of(Get.context).failure),
      ));
    }
  }
}
