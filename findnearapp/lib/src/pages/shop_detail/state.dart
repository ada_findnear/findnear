import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/category.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/order_history.dart';
import 'package:findnear/src/models/response/product_response.dart';
import 'package:findnear/src/models/shop.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../models/user.dart' as userModel;
class ShopDetailState extends BaseState {
  final _isLoading = true.obs;
  final _products = List<Product>.empty().obs;
  final _categories = List<Category>.empty().obs;
  final _histories = List<OrderHistory>.empty().obs;
  final _isShowBottomBar = false.obs;
  Rx<Shop> _shop = Shop().obs;

  get isLoading => _isLoading.value;

  set isLoading(value) {
    _isLoading.value = value;
  }

  bool get isShowBottomBar => _isShowBottomBar.value;
  set isShowBottomBar(bool value) {
    _isShowBottomBar.value = value;
  }

  List<Product> get products => _products.toList();
  List<Category> get categories => _categories.toList();
  List<OrderHistory> get orderHistories => _histories.toList();

  set products(List<Product> value) {
    _products.value = value;
  }

  set categories(List<Category> value) {
    _categories.value = value ?? [];
    _categories.insert(0, Category(name: "Tất cả sản phẩm", id: "0"));
  }

  set orderHistories(List<OrderHistory> value) {
    _histories.value = value;
  }

  Shop get shop => _shop.value;

  set shop(Shop obj) {
    _shop.value = obj;
  }

  Conversation conversation;
  Rx<userModel.User> userOwner = userModel.User.fromJSON({}).obs;
  RxString address = "".obs;
}
