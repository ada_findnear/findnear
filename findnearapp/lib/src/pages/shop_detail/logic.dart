import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/base/base_refresh_load_more.dart';
import 'package:findnear/src/database/conversations_dao.dart';
import 'package:findnear/src/global/global_event.dart';
import 'package:findnear/src/models/category.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/communication/entities/group_config_entity.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/entities/communication/response/conversation_entity.dart';
import 'package:findnear/src/models/entities/place_entity.dart';
import 'package:findnear/src/models/order_history.dart';
import 'package:findnear/src/models/response/order_history_response.dart';
import 'package:findnear/src/models/response/rating_response.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/bottom_sheet/change_bg_logic.dart';
import 'package:findnear/src/repository/communication_repository.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import '../../models/response/category_response.dart';
import 'package:findnear/src/models/response/product_response.dart';
import 'package:findnear/src/models/shop.dart';
import 'package:findnear/src/repository/shop_repository.dart';
import 'package:get/get.dart';
import 'state.dart';
import '../../settings/global_variable.dart' as GlobalVar;
import '../../models/user.dart' as userModel;
import '../../repository/user_repository.dart' as repository;
import 'package:dio/dio.dart' as dio;
import 'package:findnear/src/repository/user_repository.dart';

class ShopDetailLogic extends BaseController<ShopDetailState>
    with RefreshLoadMoreControllerMixin {
  final shopRepository = Get.find<ShopsRepository>();
  final _categories = List<Category>.empty().obs;
  CommunicationRepository _socketRepository =
      CommunicationApi(isSocketApi: true);
  final _conversationsDAO = Get.find<ConversationsDAO>();
  CommunicationRepository communicationRepository =
      Get.find<CommunicationRepository>();

  ShopDetailLogic();

  void fetchInitialData(int shopId) async {
    try {
      getShopDetail(shopId: shopId);
      getCategories(shopId: shopId);
    } catch (e) {
      print("TuanLA - $e");
    }
  }

  Future<ProductsResponse> getList() async {
    // return await shopRepository.getProducts(
    //     shopId: state.shop.id, page: currentPage++);
    return await shopRepository.getProducts(
        shopId: state.shop.id, page: 1, perPage: 100);
  }

  Future<CategoryResponse> getCategories({int shopId}) async {
    final res = await shopRepository.getCategories(
      shopId: shopId ?? state.shop.id,
    );
    categories = res.data ?? List<Category>.empty();
    return res;
  }

  Future<OrderHistoryResponse> getOrderHistory() async {
    final res = await shopRepository.getOrderHistory(
      state.shop.id,
    );
    state.orderHistories = res.data ?? List<OrderHistory>.empty();
    return res;
  }

  List<Category> get categories => _categories;

  set categories(List<Category> value) {
    _categories.value = value ?? [];
    state.categories = value;
  }

  void getCategory() {
    print(_categories);
  }

  @override
  ShopDetailState createState() => ShopDetailState();

  @override
  Future<bool> loadList() async {
    if (!canLoadMore) return true;
    try {
      if (currentPage == 1) state.isLoading = true;
      ProductsResponse response = await getList();
      if (response?.data?.data?.isNotEmpty == true) {
        canLoadMore = true;
        state.products = List<Product>.from(currentPage == 1
            ? List<Shop>.empty()
            : (state.products ?? List<Shop>.empty()))
          ..addAll(response?.data?.data ?? List.empty());
      } else {
        state.products = List<Product>.from(currentPage == 1
            ? List<Shop>.empty()
            : (state.products ?? List<Shop>.empty()))
          ..addAll(response?.data?.data ?? List.empty());
        canLoadMore = false;
      }
      state.isLoading = false;
      return true;
    } catch (e) {
      state.error = e;
      state.isLoading = false;
      return false;
    }
  }

  void getShopDetail({int shopId}) async {
    state.isLoading = true;
    await shopRepository
        .getShopDetail(
            shopId: shopId,
            long: GlobalVar.current_location.longitude,
            lat: GlobalVar.current_location.latitude)
        .then((response) async {
      state.shop = response?.data ?? Shop();
      await loadList();
      await getOrderHistory();
      await searchAddressFromLatLng(LatLng(double.parse(state.shop.latitude),
          double.parse(state.shop.longitude)));
      // await getCategories();
      state.isLoading = false;
    });
  }

  ///Navigate
  void openChatRoom() async {
    try {
      if (!isLoggedIn) {
        NavigatorUtils.toLogin();
        return;
      }else{
        DialogHelper.showLoadingDialog();
        await listenForUser();
        await getConversation();
        Conversation _conversation;
        if (state.conversation != null) {
          _conversation = state.conversation;
        } else {
          ConversationEntity entity = await createConversation();
          _conversation = entity.toConversation;
        }
        Get.back();
        NavigatorUtils.navigateToConversationDetailPage(_conversation);
      };
    } catch (e) {
      Get.back();
      print("TuanLA - $e");
    }
  }

  Future<ConversationEntity> createConversation() async {
    List<User> users = [];
    List<ShortUserEntity> shortUsers = [];

    User currentUser = Get.find<LocalDataManager>().currentUser;
    Conversation _conversation = new Conversation(users);

    _conversation.id = UniqueKey().toString();
    _conversation.lastMessageTime =
        DateTime.now().toUtc().millisecondsSinceEpoch;
    _conversation.name = state.userOwner.value.name;
    _conversation.readByUsers = [currentUser?.id ?? ""];
    _conversation.idAdmin = currentUser?.id ?? "";
    if (_conversation.visibleToUsers == null) {
      _conversation.visibleToUsers = [];
    }

    /// thêm short user bao gồm bản thân
    _conversation.users.add(currentUser);
    _conversation.visibleToUsers.add(currentUser.id);
    shortUsers.add(ShortUserEntity.fromUser(currentUser));

    /// thêm short user bao gồm user trong profile
    _conversation.users.add(state.userOwner.value);
    _conversation.visibleToUsers.add(state.userOwner.value.id);
    shortUsers.add(ShortUserEntity.fromUser(state.userOwner.value));

    _conversation.visibleToUsers.sort((a, b) => a.compareTo(b));
    _conversation.type = "PEEK";
    String currentUserId = currentUser.id;
    User user = users.firstWhere((element) => element.id != currentUserId);
    String imgUrl = user.image.thumb ??
        "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
    _conversation.groupAvatar = imgUrl;
    _conversation.config = GroupConfigEntity(
      groupAvatar: imgUrl,
      shortUsers: shortUsers,
      groupBackground: ChangeBgController.DEFAULT_BACKGROUND,
      privateBackground: ChangeBgController.DEFAULT_BACKGROUND,
    );

    ConversationEntity result =
        await _socketRepository.createConversations(_conversation);

    /// save vào local database
    await _conversationsDAO.put(result.toConversation);

    /// save lại vào conversation hiện tại
    state.conversation = result.toConversation;

    /// notify đến danh sách conversation cập nhật lại
    GlobalEvent.instance.onCreateConversationResponse.add(result);
    return result;
  }

  void getConversation() async {
    final userId = state.shop.user_id;
    if (userId == null) {
      return;
    }
    // state.conversation = await _conversationsDAO.getAllWithUserId(userId.toString());
    User _user = Get.find<LocalDataManager>().currentUser;
    List<ConversationEntity> result =
        await communicationRepository.findConversationByReceiver(
            receiver: state.userOwner.value.code, sender: _user.code);
    if (result.isNotEmpty && result.length > 0) {
      state.conversation = result[0].toConversation;
    }
  }

  void listenForUser() async {
    final userModel.User data = await repository.getUser(state.shop.user_id);
    state.userOwner.value = data;
  }

  void searchAddressFromLatLng(LatLng latlng) async {
    dio.Response response;
    var dioClient = dio.Dio(); // with default Options
    dioClient.options.baseUrl = 'https://maps.googleapis.com';
    dioClient.options.connectTimeout = 30000; //30s
    dioClient.options.receiveTimeout = 60000; //60s
    try {
      response =
          await dioClient.get('/maps/api/geocode/json', queryParameters: {
        "latlng": "${latlng.latitude},${latlng.longitude}",
        "key": "AIzaSyBtaQfYohHQE01th0fmXvKLG-DvIvcBhrI",
      });
      final data = response.data as Map<String, dynamic>;
      final placeList = AddressList.fromJson(data);
      var placeResult = placeList?.results ?? [];
      state.address.value = placeResult[0].formattedAddress;
    } catch (e, s) {
      print("TuanLA - $e");
    }
  }
}
