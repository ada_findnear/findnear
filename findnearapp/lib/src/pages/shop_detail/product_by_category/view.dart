import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/pages/product_detail/view.dart';
import 'package:findnear/src/pages/shop_detail/product_by_category/logic.dart';
import 'package:findnear/src/pages/shop_detail/product_by_category/state.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class ProductByCategoryView extends StatefulWidget {
  static const String ROUTE_NAME = 'ProductByCategoryView';
  final RouteArgument routeArgument;

  const ProductByCategoryView({Key key, this.routeArgument}) : super(key: key);

  @override
  _ProductByCategoryViewState createState() => _ProductByCategoryViewState();
}

class _ProductByCategoryViewState extends BaseWidgetState<ProductByCategoryView,
    ProductByCategoryLogic, ProductByCategoryState> {
  int get categoryId => widget.routeArgument.param['categoryId'];
  int get shopId => widget.routeArgument.param['shopId'];
  String get title => widget.routeArgument.param['title'];

  @override
  void initState() {
    controller.setCategoryId(categoryId);
    controller.setShopId(shopId);
    controller.loadList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.keyboard_arrow_left),
          onPressed: () {
            Get.back();
          },
        ),
        title: Text(
          title,
          style: TextStyle(color: AppColors.textBlack),
        ),
        automaticallyImplyLeading: false,
      ),
      body: Obx(() => Container(
            child: _buildListProduct(),
          )),
    );
  }

  Widget _buildListProduct() {
    state.isLoading;
    if (state.isLoading) {
      return Container(
        margin: EdgeInsets.only(top: 20),
        alignment: Alignment.center,
        child: LoadingView(),
      );
    }
    if (state.products.isEmpty && !state.isLoading) {
      return Container(
        margin: EdgeInsets.only(top: 20),
        child: Center(
            child: Text("Cửa hàng chưa có món",
                style: TextStyle(
                    color: AppColors.textBlack, fontWeight: FontWeight.bold))),
      );
    }
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(40.0),
        // color: const Color(0xffffffff),
      ),
      padding: EdgeInsets.all(10.0),
      child: GridView.builder(
        primary: false,
        shrinkWrap: true,
        padding: EdgeInsets.only(top: 10),
        itemBuilder: _buildProductItem,
        // separatorBuilder: (_, index) => index >0 ? Divider(thickness: 1) : SizedBox(),
        itemCount: state.products.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 10,
            childAspectRatio: 7 / 9,
            mainAxisSpacing: 10),
      ),
    );
  }

  Widget _buildProductItem(BuildContext context, int index) {
    var formatter = NumberFormat('#,###,000');
    final product = state.products[index];
    return GestureDetector(
      onTap: () async {
        await Get.toNamed(ProductDetailPage.ROUTE_NAME,
            arguments: RouteArgument(
                param: {'product': product, 'isRouteFromShop': true}));
        controller.loadList();
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: AppColors.white,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AspectRatio(
              aspectRatio: 1 / 1,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  image: DecorationImage(
                    image: CachedNetworkImageProvider(product.firstImage),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            SizedBox(height: 5),
            Text(
              product.name ?? '',
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontSize: 14,
                color: const Color(0xff333333),
                fontWeight: FontWeight.w700,
              ),
            ),
            SizedBox(height: 3),
            Text(
              '${product.price != null ? formatter.format(product.price) : 0.0}đ',
              style: TextStyle(
                fontSize: 14,
                color: const Color(0xffff0000),
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  createController() => ProductByCategoryLogic();
}
