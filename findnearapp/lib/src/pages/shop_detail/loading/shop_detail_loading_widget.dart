import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/widgets/shimmer/app_shimmer.dart';
import 'package:flutter/material.dart';

class ShopDetailWidget extends StatelessWidget {
  const ShopDetailWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: key,
      backgroundColor: AppColors.white,
      extendBodyBehindAppBar: true,
      body: Stack(
        fit: StackFit.expand,
        children: [
          Positioned(
            top: 0,
            child: AppShimmer(
                width: MediaQuery.of(context).size.width,
                height: 200,
                cornerRadius: 10),
          ),
          Positioned(
            top: 0,
            left: 12,
            right: 12,
            bottom: 0,
            child: SingleChildScrollView(
              physics: ClampingScrollPhysics(),
              child: Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width - 32,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(16.0),
                      color: AppColors.white,
                      boxShadow: [
                        BoxShadow(
                          color: const Color(0x29000000),
                          offset: Offset(0, 0),
                          blurRadius: 6,
                        ),
                      ],
                    ),
                    margin: EdgeInsets.only(top: 160),
                    height: 180,
                    padding: EdgeInsets.all(16),
                    child: Column(
                      children: [
                        AppShimmer(
                            width: MediaQuery.of(context).size.width / 2,
                            height: 20,
                            cornerRadius: 10),
                        SizedBox(height: 5),
                        AppShimmer(
                            width: MediaQuery.of(context).size.width / 3,
                            height: 15,
                            cornerRadius: 10),
                        SizedBox(height: 5),
                        AppShimmer(
                            width: MediaQuery.of(context).size.width / 2,
                            height: 15,
                            cornerRadius: 10),
                        SizedBox(height: 5),
                        AppShimmer(
                            width: MediaQuery.of(context).size.width / 4,
                            height: 15,
                            cornerRadius: 10),
                        SizedBox(height: 35),
                        AppShimmer(
                            width: MediaQuery.of(context).size.width / 2,
                            height: 33,
                            cornerRadius: 20),
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(top: 20),
                          child: AppShimmer(
                              width: MediaQuery.of(context).size.width/ 2 - 5,
                              height: MediaQuery.of(context).size.width/ 2 - 5,
                              cornerRadius: 10),
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(top: 20),
                          child: AppShimmer(
                              width: MediaQuery.of(context).size.width/ 2 - 5 ,
                              height: MediaQuery.of(context).size.width/ 2 - 5,
                              cornerRadius: 10),
                        ),
                      ),

                    ],
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(top: 20),
                          child: AppShimmer(
                              width: MediaQuery.of(context).size.width/ 2 - 5,
                              height: MediaQuery.of(context).size.width/ 2 - 5,
                              cornerRadius: 10),
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(top: 20),
                          child: AppShimmer(
                              width: MediaQuery.of(context).size.width/ 2 - 5 ,
                              height: MediaQuery.of(context).size.width/ 2 - 5,
                              cornerRadius: 10),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
