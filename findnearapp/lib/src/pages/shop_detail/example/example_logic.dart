import 'package:get/get.dart';

import 'example_state.dart';

class ExampleLogic extends GetxController {
  final ExampleState state = ExampleState();
}
