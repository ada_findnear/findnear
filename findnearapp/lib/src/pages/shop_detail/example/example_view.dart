import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'example_logic.dart';

class ExamplePage extends StatelessWidget {
  final logic = Get.put(ExampleLogic());
  final state = Get.find<ExampleLogic>().state;

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
