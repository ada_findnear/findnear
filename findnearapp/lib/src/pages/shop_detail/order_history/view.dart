import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/models/order_history.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

typedef OnTapRatingOrder(OrderHistory orderHistory);

class OrderHistoryView extends StatelessWidget {
  final List<OrderHistory> histories;
  final OnTapRatingOrder onTap;
  const OrderHistoryView({Key key, this.histories, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.white,
      child: histories.isEmpty
          ? Center(
              child: Text(
              'Chưa có đơn hàng để đánh giá',
              style: TextStyle(color: AppColors.textBlack),
            ))
          : ListView.separated(
              // shrinkWrap: true,
              itemBuilder: (c, index) {
                return TextButton(
                  onPressed: () {
                    onTap(histories[index]);
                  },
                  child: _orderBuild(
                    histories[index],
                  ),
                );
              },
              separatorBuilder: (c, index) {
                return SizedBox(height: 0.0);
              },
              itemCount: histories.length,
            ),
    );
  }

  Widget _orderBuild(OrderHistory order) {
    var formatter = NumberFormat('#,###,000');
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 10.0),
      child: Row(
        children: [
          Container(
            width: 56.0,
            height: 56.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              image: DecorationImage(
                image: CachedNetworkImageProvider(
                    order?.products?.first?.image?.url ?? ""),
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(
            width: 10.0,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  order?.products?.first?.name ?? "",
                  style: TextStyle(fontSize: 14, color: AppColors.textBlack),
                ),
                SizedBox(
                  height: 8.0,
                ),
                Row(
                  children: [
                    Expanded(
                        child: Text(
                            "${formatter.format(order?.total ?? 0.0) ?? ""}đ")),
                    Expanded(child: Text('Số lượng: ${order?.numberItem}')),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
