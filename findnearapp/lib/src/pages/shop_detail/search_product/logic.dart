import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/base/base_refresh_load_more.dart';
import 'package:findnear/src/models/response/product_response.dart';
import 'package:findnear/src/models/shop.dart';
import 'package:findnear/src/pages/shop_detail/product_by_category/state.dart';
import 'package:findnear/src/pages/shop_detail/search_product/state.dart';
import 'package:findnear/src/repository/shop_repository.dart';
import 'package:get/get.dart';

class ProductByKeywordLogic extends BaseController<ProductByKeywordState>
    with RefreshLoadMoreControllerMixin {
  final shopRepository = Get.find<ShopsRepository>();

  Future<ProductsResponse> getList() async {
    // return await shopRepository.getProducts(
    //     shopId: state.shop.id, page: currentPage++);
    return await shopRepository.getProducts(
        shopId: state.shopId, page: 1, perPage: 100, keyword: state.keyword);
  }

  @override
  ProductByKeywordState createState() => ProductByKeywordState();

  @override
  Future<bool> loadList() async {
    if (!canLoadMore) return true;
    try {
      if (currentPage == 1) state.isLoading = true;
      ProductsResponse response = await getList();
      if (response?.data?.data?.isNotEmpty == true) {
        canLoadMore = true;
        state.products = List<Product>.from(currentPage == 1
            ? List<Shop>.empty()
            : (state.products ?? List<Shop>.empty()))
          ..addAll(response?.data?.data ?? List.empty());
      } else {
        canLoadMore = false;
      }
      state.isLoading = false;
      return true;
    } catch (e) {
      state.error = e;
      state.isLoading = false;
      return false;
    }
  }

  void setShopId(int id) {
    state.shopId = id;
  }

  void setKeyWord(String keyword) {
    state.isLoading = true;
    state.keyword = keyword;
    state.isLoading = false;
    loadList();
  }
}
