import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/response/product_response.dart';
import 'package:findnear/src/models/shop.dart';
import 'package:get/get.dart';

class ProductByKeywordState extends BaseState {
  final _isLoading = false.obs;
  final _products = List<Product>.empty().obs;
  final _shopId = 0.obs;
  Rx<String> _keyword = ''.obs;
  Rx<Shop> _shop = Shop().obs;

  get isLoading => _isLoading.value;

  set isLoading(value) {
    _isLoading.value = value;
  }

  List<Product> get products => _products.toList();

  set products(List<Product> value) {
    _products.value = value;
  }

  Shop get shop => _shop.value;

  set shop(Shop obj) {
    _shop.value = obj;
  }

  int get shopId => _shopId.value;

  set shopId(int id) {
    _shopId.value = id;
  }

  String get keyword => _keyword.value;

  set keyword(String keyWord) {
    _keyword.value = keyWord;
  }
}
