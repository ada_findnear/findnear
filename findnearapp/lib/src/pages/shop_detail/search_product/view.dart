import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/pages/product_detail/view.dart';
import 'package:findnear/src/pages/shop_detail/search_product/logic.dart';
import 'package:findnear/src/pages/shop_detail/search_product/state.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../../main.dart';

class SearchProductOfShopView extends StatefulWidget {
  final int shopId;
  const SearchProductOfShopView({Key key, this.shopId}) : super(key: key);

  @override
  _SearchProductOfShopViewState createState() =>
      _SearchProductOfShopViewState();
}

class _SearchProductOfShopViewState extends BaseWidgetState<
    SearchProductOfShopView, ProductByKeywordLogic, ProductByKeywordState> {
  int get shopId => widget.shopId;
  @override
  void initState() {
    controller.setShopId(shopId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: "SearchField",
      child: Scaffold(
          appBar: AppBar(
            automaticallyImplyLeading: false,
            leading: GestureDetector(
              child: Icon(Icons.keyboard_arrow_left),
              onTap: () {
                Get.back();
              },
            ),
            elevation: 0,
            leadingWidth: 32,
            title: Container(
              height: 40,
              child: TextFormField(
                decoration: InputDecoration(
                  prefixIcon:
                      Icon(Icons.search, color: Theme.of(context).accentColor),
                  border: OutlineInputBorder(
                      borderSide: BorderSide(
                          color:
                              Theme.of(context).focusColor.withOpacity(0.1))),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color:
                              Theme.of(context).focusColor.withOpacity(0.3))),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color:
                              Theme.of(context).focusColor.withOpacity(0.1))),
                  hintText: S.of(context).search_products,
                  hintStyle: TextStyle(color: AppColors.gray, fontSize: 14),
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 8.0, horizontal: 10.0),
                ),
                onFieldSubmitted: (text) async {
                  controller.setKeyWord(text);
                },
              ),
            ),
          ),
          body: Obx(() => _buildListProduct())),
    );
  }

  Widget _buildListProduct() {
    if (state.isLoading) {
      return Container(
        margin: EdgeInsets.only(top: 20),
        alignment: Alignment.center,
        child: LoadingView(),
      );
    }
    if (state.products.isEmpty && !state.isLoading) {
      return Container(
        margin: EdgeInsets.only(top: 20),
        child: Align(
            alignment: Alignment.topCenter,
            child: Text(S.of(context).shop_no_product)),
      );
    }
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(40.0),
        // color: const Color(0xffffffff),
      ),
      padding: EdgeInsets.all(10.0),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: ListTile(
                dense: true,
                contentPadding: EdgeInsets.symmetric(vertical: 0),
                title: Text(
                  S.of(context).products_results,
                  style: Theme.of(context).textTheme.subtitle1,
                ),
              ),
            ),
            GridView.builder(
              primary: false,
              shrinkWrap: true,
              padding: EdgeInsets.only(top: 10),
              itemBuilder: _buildProductItem,
              // separatorBuilder: (_, index) => index >0 ? Divider(thickness: 1) : SizedBox(),
              itemCount: state.products.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 10,
                  childAspectRatio: 7 / 9,
                  mainAxisSpacing: 10),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildProductItem(BuildContext context, int index) {
    var formatter = NumberFormat('#,###,000');
    final product = state.products[index];
    return GestureDetector(
      onTap: () async {
        await Get.toNamed(ProductDetailPage.ROUTE_NAME,
            arguments: RouteArgument(
                param: {'product': product, 'isRouteFromShop': true}));
        controller.loadList();
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: AppColors.white,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AspectRatio(
              aspectRatio: 1 / 1,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  image: DecorationImage(
                    image: CachedNetworkImageProvider(product.firstImage),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            SizedBox(height: 5),
            Text(
              product.name ?? '',
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontSize: 14,
                color: const Color(0xff333333),
                fontWeight: FontWeight.w700,
              ),
            ),
            SizedBox(height: 3),
            Text(
              '${product.price != null ? formatter.format(product.price) : 0.0}đ',
              style: TextStyle(
                fontSize: 14,
                color: const Color(0xffff0000),
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  ProductByKeywordLogic createController() => ProductByKeywordLogic();
}
