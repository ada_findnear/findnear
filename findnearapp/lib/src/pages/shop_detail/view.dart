import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/main.dart';
import 'package:findnear/src/base/base_refresh_load_more.dart';
import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_bottom_sheets.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/helpers/app_config.dart';
import 'package:findnear/src/models/product.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/models/shop.dart';
import 'package:findnear/src/pages/product/presentation/create_product/view.dart';
import 'package:findnear/src/pages/product_detail/view.dart';
import 'package:findnear/src/pages/profile_user/profile_user_logic.dart';
import 'package:findnear/src/pages/profile_user/profile_user_state.dart';
import 'package:findnear/src/pages/profile_user/profile_user_view.dart';
import 'package:findnear/src/pages/rating/rating_order/view.dart';
import 'package:findnear/src/pages/rating/rating_page.dart';
import 'package:findnear/src/pages/register_shop_page/register_shop_page.dart';
import 'package:findnear/src/pages/shop_detail/order_history/view.dart';
import 'package:findnear/src/pages/shop_detail/search_product/view.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/services/setting_service.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import 'loading/shop_detail_loading_widget.dart';
import 'logic.dart';
import 'state.dart';

class ShopDetailPage extends StatefulWidget {
  static const String ROUTE_NAME = 'ShopDetailPage';
  final RouteArgument routeArgument;

  ShopDetailPage({this.routeArgument});

  @override
  _ShopDetailPageState createState() => _ShopDetailPageState();
}

class _ShopDetailPageState
    extends BaseWidgetState<ShopDetailPage, ShopDetailLogic, ShopDetailState>
    with RefreshLoadMoreWidgetMixin {
  @override
  ShopDetailLogic createController() => ShopDetailLogic();

  int get shopId => widget.routeArgument.param;
  Key key = UniqueKey();

  ScrollController _scrollAController = ScrollController();

  PersistentBottomSheetController _presistentController;
  final ProfileUserLogic logic = Get.put(ProfileUserLogic());
  final ProfileUserState stateProfile = Get.find<ProfileUserLogic>().state;

  @override
  void initState() {
    super.initState();
    controller.fetchInitialData(shopId);

    _scrollAController.addListener(() {
      if (_scrollAController.offset >=
              _scrollAController.position.maxScrollExtent &&
          !_scrollAController.position.outOfRange) {
        // controller.loadList();
      }
      if (_scrollAController.offset <=
              _scrollAController.position.minScrollExtent &&
          !_scrollAController.position.outOfRange) {
        // scroll to bottom
      }
    });
  }

  void openRegisterShop() async {
    final shop = await Get.toNamed(RegisterShopPage.ROUTE_NAME);
    if (shop != null) {
      controller.getShopDetail(shopId: shop.id);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: key,
      backgroundColor: AppColors.white,
      appBar: AppBar(
          iconTheme: IconThemeData(color: AppColors.white),
          backgroundColor: AppColors.black.withOpacity(0.2),
          elevation: 0,
          actions: [
            _buildSearchField(),
            Obx(() {
              return Visibility(
                visible: !state.isLoading &&
                        Get.find<LocalDataManager>().getMyShop()?.id ==
                            state.shop?.id ??
                    false,
                child: InkWell(
                  onTap: () async {
                    openRegisterShop();
                  },
                  child: Center(
                    child: Container(
                      width: 18.0,
                      height: 18.0,
                      margin: EdgeInsets.only(right: 12, left: 0.0),
                      child: SvgPicture.asset(AppImages.icEdit),
                    ),
                  ),
                ),
              );
            }),
            // Search
          ]),
      extendBodyBehindAppBar: true,
      body: Obx(() {
        if (state.isLoading) {
          return ShopDetailWidget();
        }

        String imageUrl = '';
        if (state.shop.media != null &&
            state.shop.media.length > 0 &&
            state.shop.media[0] != null) {
          imageUrl = state.shop.media[0].url;
        } else
          imageUrl = state.shop.img_url;
        return GestureDetector(
          onTap: () {
            if (_presistentController != null) {
              _presistentController.close();
              _presistentController = null;
            }
          },
          child: Stack(
            fit: StackFit.expand,
            children: [
              Positioned(
                top: 0,
                child: (imageUrl?.isNotEmpty)
                    ? Container(
                        width: MediaQuery.of(context).size.width,
                        height: 200.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          image: DecorationImage(
                            image: CachedNetworkImageProvider(imageUrl),
                            fit: BoxFit.cover,
                          ),
                        ),
                      )
                    : Container(
                        width: MediaQuery.of(context).size.width,
                        height: 200.0,
                        color: Color(0xFFFFF4F4),
                      ),
              ),
              Positioned(
                top: 0,
                left: 12,
                right: 12,
                bottom: 0,
                child: RefreshIndicator(
                  onRefresh: () async {
                    controller.refreshList();
                    refreshController.refreshCompleted();
                  },
                  child: SingleChildScrollView(
                    controller: _scrollAController,
                    physics: ClampingScrollPhysics(),
                    child: Column(
                      children: [
                        _buildShopProfile(),
                        Obx(() => _buildListProduct()),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      }),
      floatingActionButton: Obx(() {
        return Visibility(
          visible: !state.isLoading &&
                  Get.find<LocalDataManager>().getMyShop()?.id ==
                      state.shop?.id ??
              false,
          child: InkWell(
            onTap: () async {
              if (_presistentController != null) {
                _presistentController.close();
                _presistentController = null;
                state.isShowBottomBar = false;
              }
              WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
                final product = await Get.toNamed(CreateProductPage.ROUTE_NAME,
                    arguments:
                        RouteArgument(param: {'shopId': state.shop?.id}));
                final isMyShop = Get.find<LocalDataManager>().getMyShop()?.id ==
                    state.shop?.id;
                if (product != null && isMyShop == true) {
                  controller.canLoadMore = true;
                  controller.loadList();
                }
                controller.getCategories(shopId: state.shop?.id);
              });
            },
            child: Container(
              width: 45.0,
              height: 45.0,
              padding: EdgeInsets.all(10),
              child: SvgPicture.asset(
                'assets/svg/ic_add.svg',
                allowDrawingOutsideViewBox: true,
              ),
              decoration: BoxDecoration(
                borderRadius:
                    BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                color: const Color(0xffff0000),
                boxShadow: [
                  BoxShadow(
                    color: const Color(0x69ff0000),
                    offset: Offset(0, 0),
                    blurRadius: 10,
                  ),
                ],
              ),
            ),
          ),
        );
      }),
      bottomNavigationBar: Container(
        height: 60,
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        child: _buildBottomInfoBar(),
      ),
    );
  }

  Widget _buildListProduct() {
    state.isLoading;
    if (state.isLoading) {
      return Container(
        margin: EdgeInsets.only(top: 20),
        child: LoadingView(),
      );
    }
    if (state.products.isEmpty && !state.isLoading) {
      return Container(
        margin: EdgeInsets.only(top: 20),
        child: Text("Cửa hàng chưa có món"),
      );
    }
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(40.0),
        // color: const Color(0xffffffff),
      ),
      child: GridView.builder(
        primary: false,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        padding: EdgeInsets.only(top: 10),
        itemBuilder: _buildProductItem,
        // separatorBuilder: (_, index) => index >0 ? Divider(thickness: 1) : SizedBox(),
        itemCount: state.products.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 10,
            childAspectRatio: 7 / 9,
            mainAxisSpacing: 10),
      ),
    );
  }

  Container _buildShopProfile() {
    return Container(
      margin: EdgeInsets.only(top: 160),
      padding: EdgeInsets.all(16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Wrap(
            spacing: 8.0,
            runAlignment: WrapAlignment.center,
            alignment: WrapAlignment.center,
            crossAxisAlignment: WrapCrossAlignment.center,
            children: [
              Text(
                state.shop.title ?? "",
                style: TextStyle(
                  fontSize: 16,
                  color: const Color(0xff333333),
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.center,
              ),
              if (state?.shop?.isMonopoly ?? false)
                SvgPicture.asset(
                  AppImages.icFn,
                )
            ],
          ),
          SizedBox(height: 7),
          Text(
            "ID: ${state?.shop?.user_code ?? ""}",
            style: TextStyle(
              fontSize: 14,
              color: AppColors.red500,
            ),
          ),
          SizedBox(height: 7),
          Text(
           state.address.value,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 14,
              color: const Color(0xff333333),
            ),
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (state.shop?.rating != null && state.shop?.rating != 0)
                Obx(
                  () => Visibility(
                    child: TextButton.icon(
                      onPressed: () {
                        Get.toNamed(RatingPage.ROUTE_NAME,
                            arguments:
                                RouteArgument(param: {'shop': state.shop}));
                      },
                      icon: SvgPicture.asset(AppImages.icStarOutline,
                          width: 14.0, height: 14.0, color: AppColors.yellow),
                      label: Text(
                        '${state.shop?.rating}',
                        style: TextStyle(
                            color: AppColors.textBlack,
                            fontWeight: FontWeight.normal,
                            fontSize: 14.0),
                      ),
                    ),
                  ),
                ),
              if (state.shop.range != null)
                Text(
                  '${(state.shop.range / 1000)?.toStringAsFixed(1)} km',
                  style: TextStyle(
                    fontSize: 14,
                    color: const Color(0xff707070),
                  ),
                ),
            ],
          ),
          Divider(thickness: 1),
          SizedBox(height: 16),
          if (Get.find<LocalDataManager>().getMyShop()?.id != state.shop?.id)
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                InkWell(
                  onTap: () {
                    if (!isLoggedIn) {
                      NavigatorUtils.toLogin();
                      return;
                    }
                    Get.find<SettingService>().mapDisplayDatingDirection.value =
                        false;
                    NavigatorUtils.navigateToProfilePage(
                        state.shop.user_id.toString());
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 25, vertical: 7),
                    child: Text(
                      'Liên hệ để mua',
                      style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 14,
                        color: const Color(0xffffffff),
                      ),
                      textHeightBehavior:
                          TextHeightBehavior(applyHeightToFirstAscent: false),
                      textAlign: TextAlign.left,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(16.0),
                      color: const Color(0xffff0000),
                      border: Border.all(
                          width: 1.0, color: const Color(0xffff0000)),
                    ),
                  ),
                ),
                SizedBox(
                  width: 10.0,
                ),
                InkWell(
                  onTap: () {
                    if (!isLoggedIn) {
                      NavigatorUtils.toLogin();
                      return;
                    }
                    Get.bottomSheet(OrderHistoryView(
                      histories: state?.orderHistories,
                      onTap: (orderHistory) {
                        print(orderHistory.toJson());
                        Get.to(() => RatingOrderView(
                              argument: RouteArgument(
                                  param: {'orderHistory': orderHistory}),
                            ));
                      },
                    ));
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 25, vertical: 7),
                    child: Text(
                      'Viết đánh giá',
                      style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 14,
                        color: AppColors.textBlack,
                      ),
                      textHeightBehavior:
                          TextHeightBehavior(applyHeightToFirstAscent: false),
                      textAlign: TextAlign.left,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(16.0),
                      color: AppColors.term_yellow,
                      border: Border.all(width: 1.0, color: AppColors.yellow),
                    ),
                  ),
                ),
              ],
            ),
        ],
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20.0),
        color: const Color(0xffffffff),
        boxShadow: [
          BoxShadow(
            color: const Color(0x29000000),
            offset: Offset(0, 0),
            blurRadius: 6,
          ),
        ],
      ),
    );
  }

  Widget _buildSearchField() {
    return Hero(
      tag: "SearchField",
      child: Material(
        color: AppColors.transparent,
        child: GestureDetector(
          onTap: () => Get.to(SearchProductOfShopView(
            shopId: state.shop.id,
          )),
          child: Container(
            margin: EdgeInsets.all(16.0),
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            decoration: BoxDecoration(
                border: Border.all(color: AppColors.white),
                borderRadius: BorderRadius.circular(20.0)),
            child: Row(
              children: [
                Text(
                  "Tìm kiếm",
                  style: TextStyle(
                    fontSize: 12,
                    color: AppColors.white,
                  ),
                ),
                SizedBox(
                  width: 8.0,
                ),
                Icon(
                  Icons.search,
                  size: 18,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    // Get.delete<ShopDetailLogic>();
    super.dispose();
  }

  Widget _buildProductItem(BuildContext context, int index) {
    var formatter = NumberFormat('#,###,000');
    final product = state.products[index];
    return GestureDetector(
      onTap: () async {
        await Get.toNamed(ProductDetailPage.ROUTE_NAME,
            arguments: RouteArgument(
                param: {'product': product, 'isRouteFromShop': true}));

        controller.canLoadMore = true;
        controller.loadList();
        controller.getCategories(shopId: state.shop?.id);
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: AppColors.white,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AspectRatio(
              aspectRatio: 1 / 1,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  image: DecorationImage(
                    image: CachedNetworkImageProvider(product.firstImage),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            SizedBox(height: 5),
            Text(
              product.name ?? '',
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontSize: 14,
                color: const Color(0xff333333),
                fontWeight: FontWeight.w700,
              ),
            ),
            SizedBox(height: 3),
            Text(
              '${product.price != null ? formatter.format(product.price) : 0.0}đ',
              style: TextStyle(
                fontSize: 14,
                color: const Color(0xffff0000),
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBottomInfoBar() {
    return Builder(builder: (c) {
      return Obx(() {
        return Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // InfoShop
              Expanded(
                child: Row(
                  children: [
                    // Circle image
                    CircleAvatar(
                      radius: 30.0,
                      backgroundImage: NetworkImage(state.shop.img_url ?? ""),
                      backgroundColor: Colors.transparent,
                    ),
                    // Name and rating
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            state.shop.title ?? "",
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontWeight: FontWeight.w700, fontSize: 12),
                          ),
                          (state.shop?.rating != null &&
                                  state.shop?.rating != 0)
                              ? Container(
                                  height: 20,
                                  padding: EdgeInsets.zero,
                                  child: InkWell(
                                    onTap: () {},
                                    child: Row(
                                      children: [
                                        SvgPicture.asset(AppImages.icStar,
                                            width: 14, height: 14),
                                        SizedBox(
                                          width: 4.0,
                                        ),
                                        Text(
                                          '${state.shop?.rating}',
                                          style: TextStyle(
                                              color: AppColors.textBlack,
                                              fontWeight: FontWeight.normal,
                                              fontSize: 12.0),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              : Text(S.of(context).no_rating,
                                  style: TextStyle(
                                      fontSize: 10,
                                      color: AppColors.textBlack)),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              _verticalSeparated(),
              Obx(
                () => _actionButton(
                  AppImages.icCategory,
                  S.of(context).category,
                  color: state.isShowBottomBar
                      ? AppColors.red
                      : AppColors.textBlack,
                  colorIcon:
                      state.isShowBottomBar ? AppColors.red : AppColors.red500,
                  fontWeight:
                      state.isShowBottomBar ? FontWeight.bold : FontWeight.w500,
                  onPressed: () {
                    if (_presistentController != null) {
                      _presistentController.close();
                      _presistentController = null;
                      state.isShowBottomBar = false;
                    } else {
                      _presistentController = showBottomSheet(
                        context: c,
                        constraints: BoxConstraints.loose(Size(
                            MediaQuery.of(context).size.width,
                            MediaQuery.of(context).size.height * 0.4)),
                        builder: (_) {
                          if (controller.categories.isEmpty) {
                            return Container(
                              color: AppColors.white,
                              width: MediaQuery.of(context).size.width,
                              child: Center(
                                  child: Text(S.of(context).shop_no_category)),
                            );
                          }
                          return Obx(
                            () => Container(
                              color: AppColors.white,
                              child: ListView.separated(
                                // shrinkWrap: true,
                                itemBuilder: (c, index) {
                                  return TextButton(
                                    onPressed: () {
                                      NavigatorUtils.toSearchProductByCategory(
                                          state.shop.id,
                                          int.parse(
                                              controller.categories[index]?.id),
                                          controller.categories[index]?.name ??
                                              '');
                                    },
                                    style: TextButton.styleFrom(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 16.0)),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Text(
                                            controller.categories[index].name,
                                            style: TextStyle(
                                                color: AppColors.textBlack),
                                          ),
                                        ),
                                        Icon(Icons.keyboard_arrow_right,
                                            color: AppColors.textBlack)
                                      ],
                                    ),
                                  );
                                },
                                separatorBuilder: (c, index) {
                                  return Divider(
                                      thickness: 2.0,
                                      height: 0.0,
                                      color: AppColors.divider);
                                },
                                itemCount: controller.categories.length,
                              ),
                            ),
                          );
                        },
                      );
                      state.isShowBottomBar = true;
                    }
                  },
                ),
              ),
              Get.find<LocalDataManager>().getMyShop()?.id == state.shop?.id ??
                      false
                  ? SizedBox.shrink()
                  : _verticalSeparated(),
              Get.find<LocalDataManager>().getMyShop()?.id == state.shop?.id ??
                      false
                  ? SizedBox.shrink()
                  : _actionButton(
                      AppImages.icPhone, S.of(context).call_category,
                      onPressed: () {
                      if (!isLoggedIn) {
                        NavigatorUtils.toLogin();
                        return;
                      }

                      /// TODO: Make call to shop
                      // logic.makeCall();
                      Get.find<SettingService>()
                          .mapDisplayDatingDirection
                          .value = false;
                      NavigatorUtils.navigateToProfilePage(
                          state.shop.id.toString());
                    }),
              Get.find<LocalDataManager>().getMyShop()?.id == state.shop?.id ??
                      false
                  ? SizedBox.shrink()
                  : _verticalSeparated(),
              Get.find<LocalDataManager>().getMyShop()?.id == state.shop?.id ??
                      false
                  ? SizedBox.shrink()
                  : _actionButton(
                      AppImages.icChatCategory, S.of(context).messages,
                      onPressed: () {
                      // open chat room
                      controller.openChatRoom();
                    }),
            ],
          ),
        );
      });
    });
  }

  Widget _verticalSeparated() {
    return Container(
      height: 32,
      width: 1,
      margin: EdgeInsets.symmetric(horizontal: 4.0),
      color: AppColors.grayDark,
    );
  }

  Widget _actionButton(String icon, String title,
      {Function onPressed,
      Color color,
      Color colorIcon,
      FontWeight fontWeight = FontWeight.normal}) {
    return Container(
      // height: 32,
      margin: EdgeInsets.symmetric(horizontal: 12.0),
      child: InkWell(
        onTap: onPressed,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset(icon, width: 14, height: 14, color: colorIcon),
            Text(
              title,
              style: TextStyle(
                  fontSize: 10,
                  color: color,
                  fontWeight: fontWeight,
                  height: 2),
            ),
          ],
        ),
      ),
    );
  }
}
