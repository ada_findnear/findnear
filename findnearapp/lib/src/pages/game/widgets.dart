import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';

Widget buildGoldContainer(
    {@required Widget child, @required Widget title, double marginTop = 20}) {
  return Stack(
    children: [
      Container(
        margin: EdgeInsets.only(left: 20, right: 20, top: marginTop),
        child: Container(
          padding: EdgeInsets.only(top: 30, bottom: 20, left: 10, right: 10),
          child: child,
          margin: EdgeInsets.all(3),
          width: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            color: const Color(0xffffffff),
          ),
        ),
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          gradient: LinearGradient(
            begin: Alignment(0.0, -1.0),
            end: Alignment(0.0, 1.0),
            colors: [
              const Color(0xfff7aa00),
              const Color(0xfff9db00),
              const Color(0xffed9700),
              const Color(0xfff7aa00)
            ],
            stops: [0.0, 0.352, 0.727, 1.0],
          ),
        ),
      ),
      Align(child: title),
    ],
  );
}

Widget buildCoinTotal({int point}) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      AppImage('assets/svg/ic_dollar.svg'),
      SizedBox(width: 3),
      Text.rich(
        TextSpan(
          style: TextStyle(
            fontFamily: 'Quicksand',
            fontSize: 16,
            color: const Color(0xfff5ff00),
          ),
          children: [
            TextSpan(
              text: point?.toString() ?? currentUser.value?.currentPoint?.toString() ?? '0',
              style: TextStyle(
                fontWeight: FontWeight.w700,
              ),
            ),
            TextSpan(
              text: ' Find Xu',
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
      ),
    ],
  );
}