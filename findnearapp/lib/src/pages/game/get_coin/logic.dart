import 'package:findnear/src/models/game.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/repository/game_repository.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:rxdart/rxdart.dart' as RxDart;

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import 'state.dart';

class GetCoinLogic extends GetxController {
  final state = GetCoinState();
  final GameRepository gameRepository = Get.find();
  LocalDataManager localDataManager = Get.find();

  final timeRange = [
    TimeOfDay(hour: 8, minute: 0),
    TimeOfDay(hour: 10, minute: 0),
    TimeOfDay(hour: 12, minute: 0),
    TimeOfDay(hour: 14, minute: 0),
    TimeOfDay(hour: 16, minute: 0),
    TimeOfDay(hour: 18, minute: 0),
    TimeOfDay(hour: 20, minute: 0),
  ];

  List<String> get timeRangeText => timeRange
      .map((e) =>
          '${NumberFormat('00').format(e.hour)}:${NumberFormat('00').format(e.minute)}')
      .toList();

  //start from 1
  int get currentTimeIndex {
    final now = TimeOfDay.now();
    // final now = TimeOfDay(hour: 8,minute: 30); testing
    if (now.compareTo(timeRange[timeRange.length - 1]) >= 0) {
      return timeRange.length;
    }
    if (now.compareTo(timeRange[0]) < 0) {
      return -1;
    }
    for (int i = 0; i < timeRange.length; i++) {
      if (now.compareTo(timeRange[i]) <= 0) {
        return i;
      }
    }
    return -1;
  }

  bool hasPlayed(int index) =>
      state.playedPhases.any((element) => element == index + 1);

  @override
  void onReady() {
    super.onReady();
  }

  void getCoin() async {
    if (state.isGettingCoin || !canGetCoin || state.isLoading.value) return;
    state.isGettingCoin = true;
    try {
      final user = await gameRepository.playGame(
          id: 1, point: 100, phase: currentTimeIndex);
      state.user.value = user;
      localDataManager.setCurrentUser(user);
      state.canGetCoin.value = canGetCoin;
      _calculateFromHistories(await gameRepository.getGameHistories('1'));
      state.isGettingCoin = false;
    } catch (e) {
      state.isGettingCoin = false;
    }
    getLeaderBoard();
  }

  void getLeaderBoard() async {
    state.isLoading.value = true;
    final leaderUsers = await gameRepository.getLeaderBoard(limit: 10);
    state.leaderUsers.value = leaderUsers ?? List<User>.empty();
    state.isLoading.value = false;
  }

  void playGame(Game game) async {
    gameRepository.playGame(id: game.id, point: 5).then((value) => () {
          state.user.value = state.user.value;
          localDataManager.setCurrentUser(value);
        });
  }

  void winGame(Game game, int earnPoint) async {
    state.isLoading.value = true;
    await gameRepository
        .earnPoint(gameId: game.id, point: earnPoint)
        .then((value) => () {
              state.user.value = value;
              state.isLoading.value = false;
              localDataManager.setCurrentUser(value);
            }());
  }

  void getGameList() async {
    state.user.value = Get.find<LocalDataManager>().currentUser;
    try {
      state.isLoading.value = true;
      final gameData = await RxDart.Rx.zip2(
          Stream.fromFuture(gameRepository.getGameList()),
          Stream.fromFuture(gameRepository.getGameHistories('1')),
          (List<Game> games, List<GameHistory> histories) =>
              [games, histories]).first;
      state.games.value = gameData[0] ?? List<Game>.empty();
      List<GameHistory> histories = gameData[1];
      _calculateFromHistories(histories);
      state.isLoading.value = false;
    } catch (e, stackTrace) {
      state.isLoading.value = false;
      prettyLog.e(e, "====", stackTrace);
    }
  }

  void _calculateFromHistories(List<GameHistory> histories) {
    state.gameHistories.value = histories ?? List<GameHistory>.empty();
    state.currentTimeIndex.value = currentTimeIndex;
    state.canGetCoin.value = canGetCoin;
    print(
        'calculateFromHistories: $currentTimeIndex - ${state.playedPhases} $canGetCoin');
  }

  bool get canGetCoin =>
      !hasPlayed(state.currentTimeIndex.value - 1) &&
      state.currentTimeIndex != -1;

  void enablePush(bool isEnable) {
    print('enablePush" $isEnable');
    final newUser = state.user.value.copyWith(gameCanNotification: isEnable);
    state.user.value = newUser;
    localDataManager.setCurrentUser(newUser);
    gameRepository.enablePushNotification(isEnable).then((_newUser) {
      state.user.value = _newUser;
      localDataManager.setCurrentUser(_newUser);
    }).catchError((e) {
      final revertUser = newUser.copyWith(gameCanNotification: !isEnable);
      state.user.value = revertUser;
      localDataManager.setCurrentUser(revertUser);
    });
  }

  void refreshData() {
    getGameList();
    getLeaderBoard();
  }
}

extension TimeOfDayExtension on TimeOfDay {
  int compareTo(TimeOfDay other) {
    if (this.hour < other.hour) return -1;
    if (this.hour > other.hour) return 1;
    if (this.minute < other.minute) return -1;
    if (this.minute > other.minute) return 1;
    return 0;
  }
}
