import 'package:findnear/src/models/game.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';
class GetCoinState {
  GetCoinState() {}
  final RxInt currentTimeIndex = (-1).obs; // calculate from local time
  final _isGettingCoinRx = false.obs;
  final leaderUsers = List<User>.empty().obs;
  final isLoading = false.obs;
  final games = List<Game>.empty().obs;
  final canGetCoin = false.obs;
  final user = Get.find<LocalDataManager>().currentUser.obs;
  final gameHistories = List<GameHistory>.empty().obs;

  bool get isGettingCoin => _isGettingCoinRx.value;

  set isGettingCoin(bool value) => _isGettingCoinRx.value = value;

  List<int> get playedPhases =>
      gameHistories?.map((e) => e.phase)?.toList() ?? [];
}
