import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/commons/app_bottom_sheets.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/game.dart';
import 'package:findnear/src/models/media.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/game/wheel-character-collect/view.dart';
import 'package:findnear/src/pages/game/wheel/view.dart';
import 'package:findnear/src/pages/game/widgets.dart';
import 'package:findnear/src/pages/game/lottery/view.dart';
import 'package:findnear/src/settings/global_variable.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:timelines/timelines.dart' as Timeline;
import 'package:url_launcher/url_launcher.dart';

import 'logic.dart';
import 'state.dart';

class GetCoinPage extends StatelessWidget {
  static const String ROUTE_NAME = '/GetCoinPage';
  final GetCoinLogic logic = Get.put(GetCoinLogic());
  final GetCoinState state = Get.find<GetCoinLogic>().state;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<GetCoinLogic>(
      didChangeDependencies: (_) {
        logic.getGameList();
        logic.getLeaderBoard();
      },
      dispose: (_) => Get.delete<GetCoinLogic>(),
      builder: (logic) {
        return Scaffold(
          extendBodyBehindAppBar: true,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            toolbarHeight: 40,
            centerTitle: true,
            brightness: Brightness.dark,
            iconTheme: IconThemeData(color: AppColors.white),
            actions: [
              _buildNotificationButton(),
            ],
            title: Text(
              'Check-in nhận Find Xu ',
              style: TextStyle(
                fontFamily: 'Quicksand',
                fontSize: 16,
                color: const Color(0xffffffff),
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          body: Column(
            children: [
              Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(height: ScreenUtil().statusBarHeight),
                    SizedBox(height: 40),
                    Obx(() => buildCoinTotal(point: state.user.value?.currentPoint ?? 0)),
                    SizedBox(height: 12),
                    _buildButtonGetCoin(),
                    SizedBox(height: 12),
                    _buildRangeDate(context),
                    _buildFindXuDetailRule(context),
                  ],
                ),
                decoration: BoxDecoration(
                    image: DecorationImage(
                  image: AssetImage('assets/img/banner_get_coin.png'),
                  fit: BoxFit.fill,
                )),
              ),
              Expanded(
                  child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.only(left: 12, top: 17),
                      child: Text(
                        'Event nổi bật',
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          fontSize: 16,
                          color: const Color(0xff333333),
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    AspectRatio(
                      aspectRatio: 350 / 120,
                      child: Container(
                        margin: EdgeInsets.all(12),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          image: DecorationImage(
                            image: const AssetImage(
                                // 'assets/img/banner_get_coin.png'
                              AppImages.icBannerIphoneEvent
                            ),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    _buildGameList(),
                    SizedBox(height: 12),
                    _buildLeadBoard(),
                  ],
                ),
              ))
            ],
          ),
        );
      },
    );
  }

  Widget _buildNotificationButton() {
    return Builder(
      builder: (context) => GestureDetector(
        child: Container(
          child: Obx(() {
            return AppImage(state.user.value?.gameCanNotification == true
                ? 'assets/svg/ic_notification_checked.svg'
                : 'assets/svg/ic_notification_white.svg');
          }),
          padding: EdgeInsets.symmetric(horizontal: 16),
        ),
        onTap: () {
          showAppBottomSheet(
            context: context,
            child: Obx(() {
              return NotificationSettingPopup(
                isEnable: state.user.value.gameCanNotification,
                onChanged: (isEnable) {
                  logic.enablePush(isEnable);
                },
              );
            }),
          );
        },
      ),
    );
  }

  Widget _buildFindXuDetailRule(BuildContext context) => InkWell(
        onTap: () async {
          final findXuRulesUrl =
              "${GlobalConfiguration().getValue(findXuRules)}";
          await launch(findXuRulesUrl);
        },
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.all(16),
          child: Text(
            S.of(context).findXuDetailRules,
            style: Theme.of(context)
                .textTheme
                .bodyText1
                .copyWith(color: Colors.white,
                          decoration: TextDecoration.underline,
                          fontWeight:FontWeight.bold,
                          fontSize: 18),
          ),
        ),
      );

  Widget _buildGameList() {
    return Obx(() {
      final games = List<Game>.from(state.games.toList(), growable: true);
      games.removeWhere((element) => element.id == 1); //not list get coin game
      if (state.isLoading.value && state.games.isBlank) {
        return Container(
          margin: EdgeInsets.only(top: 40),
          alignment: Alignment.center,
          child: LoadingView(),
        );
      }
      if (games.isEmpty) {
        return Container();
      }
      return Container(
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: games.toList().map((e) => _buildGameItem(e)).toList(),
          ),
        ),
      );
    });
  }

  Widget _buildLeadBoard() {
    return Obx(() {
      if (state.isLoading.value && state.user.isBlank)
        return Container(
          alignment: Alignment.center,
          margin: EdgeInsets.only(top: 60),
          child: LoadingView(),
        );
      return Stack(
        children: [
          Container(
            margin: EdgeInsets.only(top: 30),
            child: Column(
              children: [
                SizedBox(height: 80),
                ..._buildLeaderBoardItems(),
              ],
            ),
            decoration: BoxDecoration(
                image: DecorationImage(
              image: AssetImage('assets/img/bg_coin_ranking.png'),
              fit: BoxFit.fill,
            )),
          ),
          Align(
            child: AppImage('assets/img/logo_coin_ranking.png'),
          ),
        ],
      );
    });
  }

  Widget _buildUserItem({
    int index,
    User user,
  }) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Row(
        children: [
          Text(
            (index + 1).toString(),
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 16,
              color: const Color(0xffffffff),
              fontWeight: FontWeight.w700,
            ),
          ),
          SizedBox(width: 10),
          Container(
            width: 45,
            height: 45,
            child: Stack(
              children: [
                Container(
                  child: Container(
                    margin: EdgeInsets.all(2),
                    decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                      image: DecorationImage(
                        image:
                            CachedNetworkImageProvider(user.image?.thumb ?? ''),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    borderRadius:
                        BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                    gradient: LinearGradient(
                      begin: Alignment(0.0, -1.0),
                      end: Alignment(0.0, 1.0),
                      colors: [
                        const Color(0xffffffff),
                        const Color(0xffffa800)
                      ],
                      stops: [0.0, 1.0],
                    ),
                  ),
                ),
                if (index == 0)
                  Align(
                    child: AppImage('assets/svg/ic_badge_gold.svg'),
                    alignment: Alignment.bottomRight,
                  ),
                if (index == 1)
                  Align(
                    child: AppImage('assets/svg/ic_badge_silver.svg'),
                    alignment: Alignment.bottomRight,
                  ),
                if (index == 2)
                  Align(
                    child: AppImage('assets/svg/ic_badge_bronze.svg'),
                    alignment: Alignment.bottomRight,
                  ),
              ],
            ),
          ),
          SizedBox(width: 10),
          Expanded(
            child: Text(
              user.name,
              style: TextStyle(
                fontFamily: 'Quicksand',
                fontSize: 16,
                color: const Color(0xffffffff),
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          AppImage('assets/svg/ic_coin_small.svg'),
          SizedBox(width: 5),
          Text(
            user.currentPoint.toString(),
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 16,
              color: const Color(0xffffffff),
              fontWeight: FontWeight.w700,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildGameItem(Game game) {
    return GestureDetector(
      onTap: () async {
        if (game.code == 'RDNUM') {
          await Get.toNamed(LotteryPage.ROUTE_NAME,
              arguments: RouteArgument(param: game));
        } else if(game.code =='LUCKW'){
          await Get.toNamed(WheelPage.ROUTE_NAME);
        } else if(game.code =='FNW'){
          await Get.toNamed(WheelCharacterCollectPage.ROUTE_NAME);
        }
        logic.refreshData();
      },
      child: Container(
        width: 84,
        margin: EdgeInsets.only(right: 5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              width: 65.0,
              height: 65.0,
              decoration: BoxDecoration(
                borderRadius:
                    BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                image: DecorationImage(
                  image: CachedNetworkImageProvider(game.firstThumb),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(height: 3),
            Text(
              game.name,
              style: TextStyle(
                fontFamily: 'Quicksand',
                fontSize: 14,
                color: const Color(0xff333333),
                fontWeight: FontWeight.w500,
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildButtonGetCoin() {
    return GestureDetector(
      onTap: () => logic.getCoin(),
      child: Obx(() {
        //print('state.canGetCoin.value ${state.canGetCoin.value}');
        return Container(
          child: Container(
            margin: EdgeInsets.all(2),
            padding: EdgeInsets.symmetric(horizontal: 29, vertical: 7),
            child: state.isLoading.value
                ? LoadingView()
                : Text(
                    state.canGetCoin.value
                        ? 'Nhận Find Xu'
                        : 'Quay lại vào giờ tiếp theo nhé',
                    style: TextStyle(
                      fontFamily: 'Quicksand',
                      fontSize: 16,
                      color: const Color(0xffffffff),
                      fontWeight: FontWeight.w700,
                    ),
                  ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(18.0),
              color: const Color(0xffffa800),
            ),
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(18.0),
            gradient: LinearGradient(
              begin: Alignment(-0.87, -0.88),
              end: Alignment(1.06, 1.3),
              colors: [
                const Color(0xffffd076),
                const Color(0xffffb82f),
                const Color(0xffffa800),
                const Color(0xfffff500),
                const Color(0xffffa800),
                const Color(0xff93ff00)
              ],
              stops: [0.0, 0.261, 0.374, 0.663, 0.88, 1.0],
            ),
          ),
        );
      }),
    );
  }

  Widget _buildRangeDate(BuildContext context) {
    return Obx(() {
      final processingIndex = state.currentTimeIndex.value - 1;
      final isGettingCoin = state.isGettingCoin;
      return Container(
        height: 30.h,
        child: Timeline.Timeline.tileBuilder(
          theme: Timeline.TimelineThemeData(
            direction: Axis.horizontal,
            connectorTheme: Timeline.ConnectorThemeData(
              color: Color(0xFFFFD584),
              //space: 20.0,
              thickness: 3.0,
            ),
          ),
          builder: Timeline.TimelineTileBuilder.connected(
            connectionDirection: Timeline.ConnectionDirection.before,
            itemExtentBuilder: (_, __) =>
                MediaQuery.of(context).size.width / logic.timeRange.length,
            oppositeContentsBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Text(
                  logic.timeRangeText[index],
                  style: TextStyle(
                      color: AppColors.white, fontWeight: FontWeight.w500),
                ),
              );
            },
            indicatorBuilder: (_, index) {
              var child;
              if (index == processingIndex) {
                if (isGettingCoin) {
                  child = Timeline.DotIndicator(
                    size: 26.0,
                    color: Color(0xFFFFD584),
                    child: CircularProgressIndicator(
                      strokeWidth: 3.0,
                      valueColor: AlwaysStoppedAnimation(Colors.white),
                    ),
                  );
                } else if (logic.hasPlayed(index)) {
                  child = AppImage('assets/svg/ic_coin_checked.svg');
                } else {
                  child = GestureDetector(
                    child: AppImage('assets/svg/ic_coin_100_border.svg'),
                    onTap: () => logic.getCoin(),
                  );
                }
              } else if (logic.hasPlayed(index)) {
                child = AppImage('assets/svg/ic_coin_checked.svg');
              } else {
                child = AppImage('assets/svg/ic_coin_100.svg');
              }

              if (index <= processingIndex) {
                return child;
              }
              return AppImage('assets/svg/ic_coin_100.svg');
            },
            connectorBuilder: (_, index, type) {
              if (index > 0) {
                return _buildConnector(index, type);
              }
              return _buildConnector(index, type);
            },
            itemCount: logic.timeRange.length,
          ),
        ),
      );
    });
  }

  Widget _buildConnector(int index, Timeline.ConnectorType type) {
    return Timeline.SolidLineConnector(color: Color(0xFFFFD584));
  }

  List<Widget> _buildLeaderBoardItems() {
    List<Widget> widgets = List<Widget>.empty(growable: true);
    int index = 0;
    state.leaderUsers.forEach((user) {
      widgets.add(_buildUserItem(index: index, user: user));
      index++;
    });

    return widgets;
  }
}

class NotificationSettingPopup extends StatelessWidget {
  final bool isEnable;
  final ValueChanged<bool> onChanged;

  NotificationSettingPopup({Key key, this.isEnable, this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: Text(
                S.of(context).setting,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 18,
                  color: const Color(0xff333333),
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
            InkWell(
                child: AppImage('assets/img/ic_close.png'),
                onTap: () => Get.back()),
          ],
        ),
        Row(
          children: [
            AppImage('assets/img/ic_menu_notifications.png'),
            SizedBox(width: 8),
            Expanded(
              child: Text(
                S.of(context).receive_push_notification,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 16,
                  color: const Color(0xff333333),
                ),
              ),
            ),
            Switch(
              value: isEnable,
              onChanged: onChanged,
            )
          ],
        )
      ],
    );
  }
}
