import 'dart:math';

import 'package:findnear/src/repository/game_repository.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';

import 'state.dart';

class WheelLogic extends GetxController {
  final state = WheelState();
  GameRepository gameRepository = Get.find();

  final List<int> coinRange = [0, 40, 0, 60, 0, 80, 0, 100, 0, 500];
  int currentIndex = 0;

  static const int pointToPlay = 20;

  get canPlay => state.currentPoint.value >= pointToPlay;

  int random() {
    final randomNumber = Random().nextInt(100);
    if (randomNumber >= 0 && randomNumber < 83) {
      //82%
      currentIndex = 0; //0
    } else if (randomNumber >= 83 && randomNumber < 93) {
      //10%
      currentIndex = 1; //40
    } else if (randomNumber >= 93 && randomNumber < 98) {
      //5%
      currentIndex = 3; //60
    } else if (randomNumber == 98) {
      //1%
      currentIndex = 5; //80
    } else if (randomNumber == 99) {
      //1%
      currentIndex = 7; //100
    } else if (randomNumber == 100) {
      //1%
      currentIndex = 9; //500
    }
    return currentIndex;
  }

  void playGame() async {
    try {
      if (state.currentPoint < pointToPlay) return;
      state.isLoading.value = true;
      final user = await gameRepository.playGame(id: 4, point: pointToPlay);
      state.currentPoint.value = user.currentPoint;
      await Get.find<LocalDataManager>().setCurrentUser(user);
      state.isLoading.value = true;
    } catch (e) {
      state.isLoading.value = false;
    }
  }

  int get gottenCoin => coinRange[currentIndex];

  Future<bool> sendPoint() async {
    if (gottenCoin == 0) return false;
    state.isLoading.value = true;
    try {
      final user = await gameRepository.earnPoint(
        gameId: 4,
        point: gottenCoin,
        description: 'Win game Lucky Wheel',
      );
      state.currentPoint.value = user.currentPoint;
      await Get.find<LocalDataManager>().setCurrentUser(user);
      state.isLoading.value = false;
      return true;
    } catch (e) {
      state.isLoading.value = false;
      return false;
    }
  }
}
