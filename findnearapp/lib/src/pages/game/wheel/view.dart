import 'dart:async';

import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/settings/global_variable.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:url_launcher/url_launcher.dart';

import '../widgets.dart';
import 'logic.dart';
import 'state.dart';
import 'package:flutter_fortune_wheel/flutter_fortune_wheel.dart';

class WheelPage extends StatefulWidget {
  static const String ROUTE_NAME = '/WheelPage';

  @override
  _WheelPageState createState() => _WheelPageState();
}

class _WheelPageState extends State<WheelPage> {
  final WheelLogic logic = Get.put(WheelLogic());
  final WheelState state = Get.find<WheelLogic>().state;
  StreamController<int> controller = StreamController<int>.broadcast();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        brightness: Brightness.dark,
        iconTheme: IconThemeData(color: AppColors.white),
        title: Text(
          'Vòng quay may mắn',
          style: TextStyle(
            fontFamily: 'Quicksand',
            fontSize: 16,
            color: const Color(0xffffffff),
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      body: Container(
          padding: EdgeInsets.all(20),
          height: double.infinity,
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 30 + ScreenUtil().statusBarHeight),
                Align(
                  alignment: Alignment.centerRight,
                  child: Obx(() {
                    return buildCoinTotal(point: state.currentPoint.value);
                  }),
                ),
                SizedBox(height: 20),
                AspectRatio(
                  aspectRatio: 1,
                  child: Opacity(
                    opacity: 0.7,
                    child: FortuneWheel(
                      selected: controller.stream,
                      items: _buildPrizeItems(),
                      animateFirst: false,
                      onFling: () {
                        if(logic.canPlay){
                          controller.add(logic.random());
                          logic.playGame();
                        }else{
                          AppSnackbar.showWarning(
                              message:
                              'Bạn không đủ xu để quay');
                        }
                      },
                      onAnimationEnd: () async {
                        final isSuccess = await logic.sendPoint();
                        if (isSuccess) {
                          AppSnackbar.showInfo(
                              message:
                                  'Bạn đã kiếm thêm ${logic.gottenCoin} xu');
                        }
                      },
                    ),
                  ),
                ),
                SizedBox(height: 20),
                _buildRuleBoard(),
              ],
            ),
          ),
          width: double.infinity,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment(0.0, -1.0),
              end: Alignment(0.0, 1.0),
              colors: [
                const Color(0xffff0000),
                const Color(0xff800000),
                const Color(0xffd50000)
              ],
              stops: [0.0, 0.468, 1.0],
            ),
          )),
    );
  }

  Widget _buildRuleBoard() {
    return Opacity(
      opacity: 0.7,
      child: buildGoldContainer(
          title: AppImage('assets/svg/img_rule.svg'),
          child: Column(
            children: [
              Text.rich(
                TextSpan(
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 14,
                    color: const Color(0xff333333),
                  ),
                  children: [
                    TextSpan(
                      text: '1/ Đối tượng tham gia\n',
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    TextSpan(
                      text:
                          'Tất cả người dùng có tài khoản tại ứng dụng Findnear đủ 18 tuổi trở lên.\n',
                    ),
                    TextSpan(
                      text: '2/ Cách thức tham dự:\n',
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    TextSpan(
                      text:
                          '- Cần 20 xu cho mỗi lượt chơi\n- Không giới hạn số lượt chơi\n',
                    ),
                    TextSpan(
                      text: '3/ Thông tin giải thưởng\n',
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    TextSpan(
                      text: '- Xu quay trúng sẽ được cộng vào tài khoản',
                    ),
                  ],
                ),
                textAlign: TextAlign.left,
              ),
              _buildDetailRule(),
            ],
          )),
    );
  }

  Widget _buildDetailRule() => GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () async {
          final luckyWheelRulesUrl =
              "${GlobalConfiguration().getValue(luckyWheelRules)}";
          if (await canLaunch(luckyWheelRulesUrl)) {
            await launch(luckyWheelRulesUrl);
          }
        },
        child: Container(
          child: Text(
            S.of(context).detailedRules,
            style: Theme.of(context)
                .textTheme
                .bodyText1
                .copyWith(color: Colors.blue),
          ),
        ),
      );

  @override
  void dispose() {
    Get.delete<WheelLogic>();
    super.dispose();
  }

  List<FortuneItem> _buildPrizeItems() {
    int index = -1;
    return logic.coinRange.map((coin) {
      index++;
      return FortuneItem(
        child: Text(coin == 0 ? 'Chúc bạn may mắn' : coin.toString(),
            style: TextStyle(
              color: AppColors.black,
              fontSize: coin == 0 ? 14 : 25,
            )),
        style: FortuneItemStyle(
            color: index % 2 == 0 ? AppColors.white : AppColors.yellow,
            // <-- custom circle slice fill color
            borderWidth: 3,
            borderColor:
                AppColors.yellow // <-- custom circle slice stroke width
            ),
      );
    }).toList();
  }
}
