import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';

class WheelState {
  WheelState() {
    ///Initialize variables
  }
  final currentPoint = Get.find<LocalDataManager>().currentUser.currentPoint.obs;
  final isLoading = false.obs;
}
