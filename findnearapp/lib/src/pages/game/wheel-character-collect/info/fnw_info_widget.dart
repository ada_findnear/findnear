import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';

class FNDInfoWidget extends StatefulWidget {
  String text;
  FNDInfoWidget({this.text});

  @override
  State<FNDInfoWidget> createState() => _FNDInfoWidgetState();
}

class _FNDInfoWidgetState extends State<FNDInfoWidget> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Align(
          alignment: Alignment.center,
          child: Container(
            color: AppColors.transparent,
            height: 319,
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    width: 318,
                    height: 300,
                    padding:
                        EdgeInsets.only(top: 40, bottom: 20, left: 5, right: 5),
                    margin: EdgeInsets.fromLTRB(30, 25, 30, 50),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      border: Border.all(color: Color(0xffFFA800), width: 10),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.topCenter,
                  child: Image.asset(AppImages.icNdInfoBanner, height: 58.96),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
                    child: GestureDetector(
                      onTap: () {
                        Get.back();
                      },
                      child: Image.asset(AppImages.icNdClose),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    width: 318,
                    margin: EdgeInsets.fromLTRB(30, 45, 30, 50),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage(AppImages.icNdInfoBottom),
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    width: 318,
                    height: 300,
                    padding:
                    EdgeInsets.only(bottom: 20, left: 5, right: 5),
                    margin: EdgeInsets.fromLTRB(30, 25, 30, 50),
                    child: _buildBodyWidget(),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  bool isLoading = true;

  Widget _buildBodyWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Bạn đã quay trúng",
          style: TextStyle(
            fontFamily: 'Quicksand',
            fontSize: 16,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        SizedBox(width: 15),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Visibility(
                visible: widget.text == "2000" ? true: false,
                child: Image.asset(AppImages.icNdCoin, width: 21.15, height: 20.23)),
            SizedBox(width: widget.text == "2000" ? 5: 0),
            Text(
              widget.text ?? "",
              style: TextStyle(
                fontFamily: 'Quicksand',
                fontSize: 30,
                fontWeight: FontWeight.bold,
                color: Color(0xffCC0000),
              ),
            ),
          ],
        ),
        SizedBox(height: 10),
        GestureDetector(
          onTap: (){
            Get.back();
          },
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(30)),
              color: Color(0xffFFA900),
              border: Border.all(color: AppColors.white, width: 2),
            ),
            child: Text(
              "CHƠI LẠI",
              style: TextStyle(
                fontFamily: 'Quicksand',
                fontSize: 16,
                fontWeight: FontWeight.bold,
                color: AppColors.white,
              ),
            ),
          ),
        )
      ],
    );
  }
}
