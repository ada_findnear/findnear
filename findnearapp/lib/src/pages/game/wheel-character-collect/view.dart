import 'dart:async';

import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/pages/game/wheel-character-collect/result/fnw_result_widget.dart';
import 'package:findnear/src/pages/game/wheel-character-collect/terms/fnw_terms_widget.dart';
import 'package:findnear/src/settings/global_variable.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import '../widgets.dart';
import 'info/fnw_info_widget.dart';
import 'logic.dart';
import 'state.dart';
import 'package:flutter_fortune_wheel/flutter_fortune_wheel.dart';

class WheelCharacterCollectPage extends StatefulWidget {
  static const String ROUTE_NAME = '/WheelCharacterCollect';

  @override
  _WheelCharacterCollectState createState() => _WheelCharacterCollectState();
}

class _WheelCharacterCollectState extends State<WheelCharacterCollectPage> {
  final WheelCharacterCollectLogic logic =
      Get.put(WheelCharacterCollectLogic());
  final WheelCharacterCollectState state =
      Get.find<WheelCharacterCollectLogic>().state;
  StreamController<int> controller = StreamController<int>.broadcast();
  final formatCurrency = new NumberFormat("#,###");

  @override
  void initState() {
    super.initState();
    logic.getHistory();
  }

  Widget _buildAppbar() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 17),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          GestureDetector(
            onTap: () {
              Get.back();
            },
            child: Container(
              color: AppColors.transparent,
                padding: EdgeInsets.only(right: 10),
                child: Image.asset(AppImages.icBackDark)),
          ),
          SizedBox(width: 7),
          Container(
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.3),
              borderRadius: BorderRadius.all(Radius.circular(20)),
              border: Border.all(color: AppColors.white, width: 2),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image.asset(AppImages.icNdCoin, width: 19.15, height: 18.23),
                SizedBox(width: 2),
                Obx(() {
                  return Text(
                    "${formatCurrency.format(state.currentPoint.value)}",
                    style: TextStyle(
                      fontFamily: 'Quicksand',
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: Color(0xffCC0000),
                    ),
                  );
                }),
              ],
            ),
          ),
          Spacer(),
          GestureDetector(
            onTap: () {
              showDialog(
                context: context,
                barrierDismissible: true,
                builder: (BuildContext context) => termDialog(),
              );
            },
            child: Image.asset(AppImages.icNdNote),
          ),
          SizedBox(width: 10),
          GestureDetector(
            onTap: () {
              showDialog(
                context: context,
                barrierDismissible: true,
                builder: (BuildContext context) => resultDialog(),
              );
            },
            child: Image.asset(AppImages.icNdHistory),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      body: Container(
          padding: EdgeInsets.symmetric(vertical: 20),
          child: Stack(
            children: [
              Positioned.fill(
                child: Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: _buildAppbar()),
                ),
              ),
              Positioned.fill(
                child: Align(
                  alignment: Alignment.center,
                  child: Container(
                    width: 300,
                    padding: EdgeInsets.only(bottom: 26, left: 9),
                    child: Image.asset(AppImages.icNdBannerLarge),
                  ),
                ),
              ),
              Positioned.fill(
                child: Align(
                  alignment: Alignment.center,
                  child: Container(
                    width: 218,
                    height: 218,
                    child: RotatedBox(
                      quarterTurns: 1,
                      child: FortuneWheel(
                        indicators: <FortuneIndicator>[
                          FortuneIndicator(
                            alignment: Alignment.topCenter,
                            // <-- changing the position of the indicator
                            child: Container(
                              width: 15,
                              height: 15,
                              child: TriangleIndicator(
                                color: Color(
                                    0xff903F2E), // <-- changing the color of the indicator
                              ),
                            ),
                          ),
                        ],
                        selected: controller.stream,
                        items: _buildPrizeItems(),
                        animateFirst: false,
                        onFling: _handleRollAction,
                        onAnimationEnd: () async {
                          _handleAnimationEnd();
                          if(logic.gottenCoinStr == "0")
                            showDialog(
                              context: context,
                              barrierDismissible: true,
                              builder: (BuildContext context) => infoDialog('Chúc bạn may mắn lần sau'),
                            );
                          else if(logic.gottenCoinStr == "2000")
                            showDialog(
                              context: context,
                              barrierDismissible: true,
                              builder: (BuildContext context) => infoDialog('2000'),
                            );
                          else if(logic.gottenCoinStr == "BANNER")
                            showDialog(
                              context: context,
                              barrierDismissible: true,
                              builder: (BuildContext context) => infoDialog('BANNER'),
                            );
                          else if(logic.gottenCoinStr == "ID")
                            showDialog(
                              context: context,
                              barrierDismissible: true,
                              builder: (BuildContext context) => infoDialog('ID Gian hàng'),
                            );
                          else
                            showDialog(
                              context: context,
                              barrierDismissible: true,
                              builder: (BuildContext context) => infoDialog("${logic.gottenCoinStr}"),
                            );
                        },
                      ),
                    ),
                  ),
                ),
              ),
              Positioned.fill(
                child: Align(
                  alignment: Alignment.center,
                  child: GestureDetector(
                      onTap: _handleRollAction, child: _buildRollButton()),
                ),
              ),
              Positioned.fill(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: _buildBoard(),
                ),
              ),
            ],
          ),
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(AppImages.icNdBg),
              fit: BoxFit.fill,
            ),
          )),
    );
  }

  @override
  void dispose() {
    Get.delete<WheelCharacterCollectLogic>();
    super.dispose();
  }

  List<FortuneItem> _buildPrizeItems() {
    int index = -1;
    return logic.coinRange.map((coin) {
      index++;
      return FortuneItem(
        child: getItem(coin),
        style: FortuneItemStyle(
            color: index % 2 == 0 ? Color(0xffFFEAEA) : Color(0xffFBC4AE),
            // <-- custom circle slice fill color
            borderWidth: 0,
            borderColor:
                Color(0xffCC0000) // <-- custom circle slice stroke width
            ),
      );
    }).toList();
  }

  Widget _buildTextItem(String text) {
    return Container(
      margin: EdgeInsets.only(left: 40),
      child: Text(
        text,
        style: TextStyle(
            color: Color(0xffCC0000),
            fontSize: 20,
            fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget getItem(int coin) {
    switch (coin) {
      case 0:
        return _buildTextItem("A");
        break;
      case 1:
        return _buildTextItem("R");
        break;
      case 2:
        return Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(width: 46),
            Image.asset(AppImages.icNdCoin, width: 16.15, height: 14.23),
            SizedBox(width: 2),
            Text(
              "2000",
              style: TextStyle(
                fontFamily: 'Quicksand',
                fontSize: 12,
                fontWeight: FontWeight.bold,
                color: Color(0xffCC0000),
              ),
            ),
          ],
        );
        break;
      case 3:
        return _buildTextItem("F");
        break;
      case 4:
        return _buildTextItem("I");
        break;
      case 5:
        return Container(
          margin: EdgeInsets.only(left: 60),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              AppImage(AppImages.icNdSound, width: 16.15, height: 14.23),
              SizedBox(width: 2),
              Text(
                "Banner",
                style: TextStyle(
                  fontSize: 10,
                  fontWeight: FontWeight.bold,
                  color: Color(0xffCC0000),
                ),
              ),
            ],
          ),
        );
        break;
      case 6:
        return _buildTextItem("N");
        break;
      case 7:
        return _buildTextItem("D");
        break;
      case 8:
        return Container(
          margin: EdgeInsets.only(left: 45),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              AppImage(AppImages.icNdStore, width: 16.15, height: 14.23),
              SizedBox(width: 3),
              Text(
                "ID Gian hàng",
                style: TextStyle(
                  fontSize: 9,
                  fontWeight: FontWeight.bold,
                  color: Color(0xffCC0000),
                ),
              ),
              SizedBox(width: 2),
              Text(
                "(300.000đ)",
                style: TextStyle(
                  fontSize: 8,
                  fontWeight: FontWeight.normal,
                  color: Color(0xffCC0000),
                ),
              ),
            ],
          ),
        );
        break;
      case 9:
        return _buildTextItem("E");
        break;
    }
  }

  Widget _buildRollButton() {
    return Container(
      width: 90,
      height: 90,
      padding: EdgeInsets.all(5),
      decoration: new BoxDecoration(
        gradient: RadialGradient(
          colors: [
            const Color(0xffFFA800),
            const Color(0xffFF0000),
          ],
        ),
        border: Border.all(color: Color(0xffFFD72E), width: 4),
        shape: BoxShape.circle,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(AppImages.icNdRoll),
          SizedBox(height: 5),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(AppImages.icNdCoin, width: 19.15, height: 18.23),
              SizedBox(width: 2),
              Text(
                "100",
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _buildBoard() {
    return Container(
      height: 120.42,
      margin: EdgeInsets.symmetric(horizontal: 20),
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Text(
            "Bộ sưu tập của bạn",
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Color(0xffCC0000),
            ),
          ),
          SizedBox(height: 5),
          Container(
            height: 76.91,
            padding: EdgeInsets.only(bottom: 15, top: 10),
            decoration: BoxDecoration(
              color: Color(0xffFFBC39),
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            child: Row(
              children: [
                SizedBox(width: 8),
                Expanded(
                  flex: 1,
                  child: Obx(() {
                    if (state.loadBoardStatus == LoadStatus.success)
                      return _buildItem(
                          text: state.FCharacterRx.value.count > 0 ? "F" : "",
                          count: state.FCharacterRx.value.count ?? 0);
                    return _buildEmptyItem();
                  }),
                ),
                SizedBox(width: 3),
                Expanded(
                  flex: 1,
                  child: Obx(() {
                    if (state.loadBoardStatus == LoadStatus.success)
                      return _buildItem(
                          text: state.ICharacterRx.value.count > 0 ? "I" : "",
                          count: state.ICharacterRx.value.count ?? 0);
                    return _buildEmptyItem();
                  }),
                ),
                SizedBox(width: 3),
                Expanded(
                  flex: 1,
                  child: Obx(() {
                    if (state.loadBoardStatus == LoadStatus.success)
                      return _buildItem(
                          text: state.NCharacterRx.value.count > 0 ? "N" : "",
                          count: state.NCharacterRx.value.count ?? 0);
                    return _buildEmptyItem();
                  }),
                ),
                SizedBox(width: 3),
                Expanded(
                  flex: 1,
                  child: Obx(() {
                    if (state.loadBoardStatus == LoadStatus.success)
                      return _buildItem(
                          text: state.DCharacterRx.value.count > 0 ? "D" : "",
                          count: state.DCharacterRx.value.count ?? 0);
                    return _buildEmptyItem();
                  }),
                ),
                SizedBox(width: 3),
                Expanded(
                  flex: 1,
                  child: Obx(() {
                    if (state.loadBoardStatus == LoadStatus.success)
                      return _buildItem(
                          text: state.NCharacterRx.value.count > 0 ? "N" : "",
                          count: state.NCharacterRx.value.count ?? 0);
                    return _buildEmptyItem();
                  }),
                ),
                SizedBox(width: 3),
                Expanded(
                  flex: 1,
                  child: Obx(() {
                    if (state.loadBoardStatus == LoadStatus.success)
                      return _buildItem(
                          text: state.ECharacterRx.value.count > 0 ? "E" : "",
                          count: state.ECharacterRx.value.count ?? 0);
                    return _buildEmptyItem();
                  }),
                ),
                SizedBox(width: 3),
                Expanded(
                  flex: 1,
                  child: Obx(() {
                    if (state.loadBoardStatus == LoadStatus.success)
                      return _buildItem(
                          text: state.ACharacterRx.value.count > 0 ? "A" : "",
                          count: state.ACharacterRx.value.count ?? 0);
                    return _buildEmptyItem();
                  }),
                ),
                SizedBox(width: 3),
                Expanded(
                  flex: 1,
                  child: Obx(() {
                    if (state.loadBoardStatus == LoadStatus.success)
                      return _buildItem(
                          text: state.RCharacterRx.value.count > 0 ? "R" : "",
                          count: state.RCharacterRx.value.count ?? 0);
                    return _buildEmptyItem();
                  }),
                ),
                SizedBox(width: 8),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildItem({String text, int count}) {
    return Stack(
      children: [
        Container(
          color: Color(0xffFFBC39),
          padding: EdgeInsets.only(top: 8, right: 8),
          child: Container(
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(5)),
            ),
            child: Center(
              child: Text(
                text,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  color: Color(0xffCC0000),
                ),
              ),
            ),
          ),
        ),
        Visibility(
          visible: count > 0,
          child: Positioned(
            child: Align(
              alignment: Alignment.topRight,
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 2, horizontal: 5),
                decoration: BoxDecoration(
                  color: Color(0xffCC0000),
                  borderRadius: BorderRadius.all(Radius.circular(25)),
                ),
                child: Text(
                  "$count",
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildEmptyItem() {
    return Container(
      color: Color(0xffFFBC39),
      padding: EdgeInsets.only(top: 8, right: 8),
      child: Container(
        padding: EdgeInsets.all(5),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(5)),
        ),
        child: Center(
          child: Text(
            "",
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 24,
              fontWeight: FontWeight.bold,
              color: Color(0xffCC0000),
            ),
          ),
        ),
      ),
    );
  }

  void _handleRollAction() {
    if (logic.canPlay) {
      controller.add(logic.random());
      logic.playGame();
    } else {
      AppSnackbar.showWarning(message: 'Bạn không đủ xu để quay');
    }
  }

  void _handleAnimationEnd() {
    logic.updateBoard();
  }

  Dialog termDialog() {
    return Dialog(
      backgroundColor: Colors.transparent,
      insetPadding: EdgeInsets.zero,
      child: FNDTermWidget(),
    );
  }

  Dialog resultDialog() {
    return Dialog(
      backgroundColor: Colors.transparent,
      insetPadding: EdgeInsets.zero,
      child: FNDResultWidget(),
    );
  }

  Dialog infoDialog(String text) {
    return Dialog(
      backgroundColor: Colors.transparent,
      insetPadding: EdgeInsets.zero,
      child: FNDInfoWidget(text: text),
    );
  }
}
