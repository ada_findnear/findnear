import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/CircularAvatarWidget.dart';
import 'package:findnear/src/models/enums/finear_wheel_price.dart';
import 'package:findnear/src/models/game.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class FNWResultItem extends StatefulWidget {
  GameHistory game;

  FNWResultItem({this.game});

  @override
  State<FNWResultItem> createState() => _FNWResultItemState();
}

class _FNWResultItemState extends State<FNWResultItem> {
  User user;

  @override
  void initState() {
    super.initState();
    user = Get.find<LocalDataManager>().currentUser;
  }

  @override
  Widget build(BuildContext context) {
    FindnearWheelPrice price =
        FindnearWheelPriceExtension.fromText(widget.game.fndData);
    return Container(
      height: 40,
      child: Center(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _buildAvatar(),
            SizedBox(width: 12),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Container(
                    child: Center(
                      child: Text(price.description,
                          overflow: TextOverflow.fade,
                          softWrap: false,
                          style: TextStyle(
                            fontFamily: "Quicksand",
                            fontSize: 14,
                            color: AppColors.black,
                            fontWeight: FontWeight.bold,
                          )),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    child: Center(
                      child: Text(widget.game.updated_at,
                          overflow: TextOverflow.fade,
                          softWrap: false,
                          style: TextStyle(
                            fontFamily: "Quicksand",
                            fontSize: 12,
                            color: AppColors.black,
                            fontWeight: FontWeight.normal,
                          )),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildAvatar() {
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(20)),
      child: Image.asset(AppImages.icNdFinearWheel, width: 42, height: 42),
    );
  }
}
