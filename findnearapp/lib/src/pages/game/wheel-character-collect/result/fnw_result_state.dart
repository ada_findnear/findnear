import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/game.dart';
import 'package:findnear/src/pages/game/wheel-character-collect/presenter/character_presenter.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';

class FNDResultState {
  FNDResultState() {
    ///Initialize variables
  }
  Rx<LoadStatus> loadHistoryStatusRx = LoadStatus.initial.obs;
  final gameHistory = List<GameHistory>.empty().obs;

}
