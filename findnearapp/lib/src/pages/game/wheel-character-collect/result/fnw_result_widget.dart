import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/game.dart';
import 'package:findnear/src/pages/game/wheel-character-collect/result/fnw_result_logic.dart';
import 'package:findnear/src/pages/game/wheel-character-collect/result/item/fnw_result_item.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../main.dart';
import 'fnw_result_state.dart';

class FNDResultWidget extends StatefulWidget {
  FNDResultWidget();

  @override
  State<FNDResultWidget> createState() => _FNDResultWidgetState();
}

class _FNDResultWidgetState extends State<FNDResultWidget> {
  final FNDResultLogic logic = Get.put(FNDResultLogic());
  final FNDResultState state = Get.find<FNDResultLogic>().state;

  @override
  void initState() {
    super.initState();
    logic.getHistory();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 300,
              decoration: BoxDecoration(
                  image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage(AppImages.icNdTermBottom),
              )),
            ),
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            color: AppColors.transparent,
            height: 489,
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    width: 318,
                    height: 479,
                    padding:
                        EdgeInsets.only(top: 40, bottom: 20, left: 5, right: 5),
                    margin: EdgeInsets.fromLTRB(30, 25, 30, 50),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      border: Border.all(color: Color(0xffFFA800), width: 10),
                    ),
                    child: _buildBodyWidget(),
                  ),
                ),
                Align(
                  alignment: Alignment.topCenter,
                  child: Image.asset(AppImages.icNdResultBanner, height: 58.96),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
                    child: GestureDetector(
                      onTap: () {
                        Get.back();
                      },
                      child: Image.asset(AppImages.icNdClose),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildBodyWidget() {
    return Obx(() {
      if (state.loadHistoryStatusRx.value == LoadStatus.success)
        return ListView.separated(
          scrollDirection: Axis.vertical,
          itemCount: state.gameHistory.length,
          separatorBuilder: (BuildContext context, int index) =>
              SizedBox(height: 10),
          itemBuilder: (_, int index) => GestureDetector(
            onTap: (){
              GameHistory game = state.gameHistory[index];
              if(game.fndData == "ID" || game.fndData == "BANNER")
                showDialog(
                  context: context,
                  barrierDismissible: true,
                  builder: (BuildContext context) => infoDialog('${game.fndData}'),
                );
            },
            child: FNWResultItem(
              game: state.gameHistory[index],
            ),
          ),
        );
      return Center(
        child: CircularProgressIndicator(),
      );
    });
  }

  Dialog infoDialog(String text) {
    String description = "";
    if(text == "ID")
      description = S.of(context).game_description_1("ID gian hàng", "300.000đ");
    else if(text == "BANNER")
      description = S.of(context).game_description_1("Banner quảng cáo", "200.000đ");
    return Dialog(
      backgroundColor: Colors.transparent,
      insetPadding: EdgeInsets.zero,
      child: Column(
        children: [
          Spacer(),
          Stack(
            children: [
              Container(
                padding: EdgeInsets.fromLTRB(20, 40, 20, 20),
                margin: EdgeInsets.fromLTRB(30, 25, 30, 0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      description,
                      style: TextStyle(
                          fontSize: 14,
                          color: AppColors.black,
                          fontWeight: FontWeight.w300,
                          height: 1.4),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "Bạn vui lòng liên hệ theo:",
                      style: TextStyle(
                          fontSize: 14,
                          color: AppColors.black,
                          fontWeight: FontWeight.w300,
                          height: 1.4),
                    ),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text:
                            S.of(context).game_description_2,
                            style: TextStyle(
                                fontFamily: 'Quicksand',
                                fontSize: 14,
                                color: AppColors.black,
                                fontWeight: FontWeight.w300,
                                height: 1.4),
                          ),
                          TextSpan(
                            text:
                            ' 0963125582',
                            style: TextStyle(
                                fontFamily: 'Quicksand',
                                fontSize: 14,
                                color: AppColors.red,
                                fontWeight: FontWeight.bold,
                                height: 1.4),
                          )
                        ],
                      ),
                    ),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text:
                            S.of(context).game_description_3,
                            style: TextStyle(
                                fontFamily: 'Quicksand',
                                fontSize: 14,
                                color: AppColors.black,
                                fontWeight: FontWeight.w300,
                                height: 1.4),
                          ),
                          TextSpan(
                            text:
                            ' 22222222',
                            style: TextStyle(
                                fontFamily: 'Quicksand',
                                fontSize: 14,
                                color: AppColors.red,
                                fontWeight: FontWeight.bold,
                                height: 1.4),
                          )
                        ],
                      ),
                    ),
                    Text(
                      "để được hướng dẫn chi tiết.",
                      style: TextStyle(
                          fontSize: 14,
                          color: AppColors.black,
                          fontWeight: FontWeight.w300,
                          height: 1.4),
                    ),
                  ],
                ),
              ),
              Positioned.fill(
                top: 0,
                child: Align(
                  alignment: Alignment.topCenter,
                  child: AppImage(AppImages.icMail),
                ),
              ),
              Positioned(
                top: 30,
                right: 30,
                child: InkWell(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AppImage('assets/img/ic_close.png'),
                  ),
                  onTap: () => Get.back(),
                ),
              )
            ],
          ),
          Spacer(),
        ],
      ),
    );
  }
}
