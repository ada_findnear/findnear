import 'dart:math';

import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/game.dart';
import 'package:findnear/src/repository/game_repository.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';

import 'fnw_result_state.dart';

class FNDResultLogic extends GetxController {
  final state = FNDResultState();
  GameRepository gameRepository = Get.find();

  void getHistory() async {
    state.loadHistoryStatusRx.value = LoadStatus.loading;
    try {
      state.gameHistory.value =
          (await gameRepository.getFNWGameHistories('6')) ??
              List<GameHistory>.empty();
      state.loadHistoryStatusRx.value = LoadStatus.success;
    } catch (e) {
      state.loadHistoryStatusRx.value = LoadStatus.failure;
    }
  }
}
