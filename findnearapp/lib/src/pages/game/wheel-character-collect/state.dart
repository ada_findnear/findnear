import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/game.dart';
import 'package:findnear/src/pages/game/wheel-character-collect/presenter/character_presenter.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';

class WheelCharacterCollectState {
  WheelCharacterCollectState() {
    ///Initialize variables
  }
  final currentPoint = Get.find<LocalDataManager>().currentUser.currentPoint.obs;
  final isLoading = false.obs;
  Rx<CharacterPresenter> FCharacterRx = CharacterPresenter(character: "F", count: 0).obs;
  Rx<CharacterPresenter> ICharacterRx = CharacterPresenter(character: "I", count: 0).obs;
  Rx<CharacterPresenter> NCharacterRx = CharacterPresenter(character: "N", count: 0).obs;
  Rx<CharacterPresenter> DCharacterRx = CharacterPresenter(character: "D", count: 0).obs;
  Rx<CharacterPresenter> ECharacterRx = CharacterPresenter(character: "E", count: 0).obs;
  Rx<CharacterPresenter> ACharacterRx = CharacterPresenter(character: "A", count: 0).obs;
  Rx<CharacterPresenter> RCharacterRx = CharacterPresenter(character: "R", count: 0).obs;
  Rx<LoadStatus> loadBoardStatus = LoadStatus.initial.obs;
  final gameHistory = List<GameHistory>.empty().obs;

  Rx<CharacterPresenter> getCharacterRxFromText(String text) {
    if(text == "F") return FCharacterRx;
    else if(text == "I") return ICharacterRx;
    else if(text == "N") return NCharacterRx;
    else if(text == "D") return DCharacterRx;
    else if(text == "E") return ECharacterRx;
    else if(text == "A") return ACharacterRx;
    else if(text == "R") return RCharacterRx;
    return null;
  }
}
