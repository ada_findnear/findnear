import 'dart:math';

import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/game.dart';
import 'package:findnear/src/repository/game_repository.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';

import 'state.dart';

class WheelCharacterCollectLogic extends GetxController {
  final state = WheelCharacterCollectState();
  GameRepository gameRepository = Get.find();

  final List<int> coinRange = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
  int currentIndex = 0;

  static const int pointToPlay = 100;

  get canPlay => state.currentPoint.value >= pointToPlay;

  int random() {
    final randomNumber = Random().nextInt(1000);
    if (randomNumber >= 469 && randomNumber < 569) {
      //10%
      currentIndex = 1; // R
    } else if (randomNumber >= 569 && randomNumber < 579) {
      //1%
      currentIndex = 2; // 2000 xu
    } else if (randomNumber >= 579 && randomNumber < 679) {
      //10%
      currentIndex = 3; // F
    } else if (randomNumber >= 679 && randomNumber < 779) {
      //10%
      currentIndex = 4; // I
    } else if (randomNumber == 779) {
      //0.1%
      currentIndex = 5; // Banner
    } else if (randomNumber >= 780 && randomNumber < 880) {
      //10%
      currentIndex = 6; // N
    } else if (randomNumber >= 880 && randomNumber < 980) {
      //10%
      currentIndex = 7; // D
    } else if (randomNumber >= 980 && randomNumber < 1000) {
      //2%
      currentIndex = 8; // ID Gian hang
    } else if (randomNumber > 1001) {
      //0%
      currentIndex = 9; // E
    } else {
      //47.9%
      currentIndex = 0; // A
    }
    return currentIndex;
  }


  void playGame() async {
    try {
      if (state.currentPoint < pointToPlay) return;
      state.isLoading.value = true;
      final user = await gameRepository.playFindnearWheelGame(id: 6, point: pointToPlay, data: [gottenCoinStr]);
      state.currentPoint.value = user.currentPoint;
      await Get.find<LocalDataManager>().setCurrentUser(user);
      state.isLoading.value = true;
    } catch (e) {
      state.isLoading.value = false;
    }
  }

  int get gottenCoin => coinRange[currentIndex];

  String get gottenCoinStr {
    switch(currentIndex){
      case 0:
        return "A";
      case 1:
        return "R";
      case 2:
        return "2000";
      case 3:
        return "F";
      case 4:
        return "I";
      case 5:
        return "BANNER";
      case 6:
        return "N";
      case 7:
        return "D";
      case 8:
        return "ID";
      case 9:
        return "E";
    }
  }

  Future<bool> sendPoint() async {
    if (gottenCoin == 0) return false;
    state.isLoading.value = true;
    try {
      final user = await gameRepository.earnPoint(
        gameId: 4,
        point: gottenCoin,
        description: 'Win game Lucky Wheel',
      );
      state.currentPoint.value = user.currentPoint;
      await Get.find<LocalDataManager>().setCurrentUser(user);
      state.isLoading.value = false;
      return true;
    } catch (e) {
      state.isLoading.value = false;
      return false;
    }
  }

  void updateBoard(){
    state.loadBoardStatus.value = LoadStatus.loading;
    switch (currentIndex) {
      case 0:
        state.ACharacterRx.value.count++;
        break;
      case 1:
        state.RCharacterRx.value.count++;
        break;
      case 3:
        state.FCharacterRx.value.count++;
        break;
      case 4:
        state.ICharacterRx.value.count++;
        break;
      case 6:
        state.NCharacterRx.value.count++;
        break;
      case 7:
        state.DCharacterRx.value.count++;
        break;
      case 9:
        state.ECharacterRx.value.count++;
        break;
    }
    state.loadBoardStatus.value = LoadStatus.success;
  }

  void getHistory() async {
    state.isLoading.value = true;
    int countF = 0;
    int countI = 0;
    int countN = 0;
    int countD = 0;
    int countE = 0;
    int countA = 0;
    int countR = 0;
    try {
      state.gameHistory.value =
          (await gameRepository.getFNWGameHistories('6')) ??
              List<GameHistory>.empty();
      if(state.gameHistory.isNotEmpty){
        state.loadBoardStatus.value = LoadStatus.loading;
        for(int i=0; i< state.gameHistory.length; i++){
          GameHistory entity = state.gameHistory[i];
          if(entity.fndData == "F") countF++;
          else if(entity.fndData == "I") countI++;
          else if(entity.fndData == "N") countN++;
          else if(entity.fndData == "D") countD++;
          else if(entity.fndData == "E") countE++;
          else if(entity.fndData == "A") countA++;
          else if(entity.fndData == "R") countR++;
        }
        state.FCharacterRx.value.count = countF;
        state.ICharacterRx.value.count = countI;
        state.NCharacterRx.value.count = countN;
        state.DCharacterRx.value.count = countD;
        state.ECharacterRx.value.count = countE;
        state.ACharacterRx.value.count = countA;
        state.RCharacterRx.value.count = countR;
        state.loadBoardStatus.value = LoadStatus.success;
      }
    } catch (e) {
      state.isLoading.value = false;
    }
    state.isLoading.value = false;
  }
}
