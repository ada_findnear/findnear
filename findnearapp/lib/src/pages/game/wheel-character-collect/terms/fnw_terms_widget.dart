import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';

class FNDTermWidget extends StatefulWidget {
  @override
  State<FNDTermWidget> createState() => _FNDTermWidgetState();
}

class _FNDTermWidgetState extends State<FNDTermWidget> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 300,
              decoration: BoxDecoration(
                  image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage(AppImages.icNdTermBottom),
              )),
            ),
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            color: AppColors.transparent,
            height: 489,
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    width: 318,
                    height: 479,
                    padding:
                        EdgeInsets.only(top: 40, bottom: 20, left: 5, right: 5),
                    margin: EdgeInsets.fromLTRB(30, 25, 30, 50),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      border: Border.all(color: Color(0xffFFA800), width: 10),
                    ),
                    child: _buildBodyWidget(),
                  ),
                ),
                Align(
                  alignment: Alignment.topCenter,
                  child: Image.asset(AppImages.icNdTermBanner, height: 58.96),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
                    child: GestureDetector(
                      onTap: () {
                        Get.back();
                      },
                      child: Image.asset(AppImages.icNdClose),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  bool isLoading = true;

  Widget _buildBodyWidget() {
    return Stack(
      children: [
        WebView(
          initialUrl: 'https://findnear.vn/thelefindnearwheel.html',
          onPageFinished: (finish) {
            setState(() {
              isLoading = false;
            });
          },
        ),
        isLoading ? Center( child: CircularProgressIndicator(),)
            : Stack(),
      ],
    );
  }

  Widget _buildDateItem(String text) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 6, horizontal: 8),
      decoration: BoxDecoration(
        color: AppColors.grayDark,
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      child: Text(
        text,
        style: TextStyle(
          fontFamily: 'Quicksand',
          fontSize: 12,
          fontWeight: FontWeight.bold,
          color: AppColors.gray,
        ),
      ),
    );
  }
}
