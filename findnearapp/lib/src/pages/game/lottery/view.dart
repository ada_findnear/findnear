import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/main.dart';
import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/pages/game/widgets.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/settings/global_variable.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:findnear/src/widgets/pin_put/pin_put.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import 'controller.dart';
import 'popup.dart';
import 'state.dart';

class LotteryPage extends StatefulWidget {
  static const String ROUTE_NAME = '/LotteryPage';
  final RouteArgument routeArgument;

  LotteryPage({this.routeArgument});

  @override
  _LotteryPageState createState() => _LotteryPageState();
}

class _LotteryPageState extends BaseWidgetState<LotteryPage, LotteryController, LotteryState>
    with TickerProviderStateMixin {

  final TextEditingController _pinPutController = TextEditingController();
  final FocusNode _pinPutFocusNode = FocusNode();

  TextEditingController _petInputController = TextEditingController();

  @override
  LotteryController createController() => Get.find();

  BoxDecoration get _pinPutDecoration {
    return BoxDecoration(
      border: Border.all(color: Color(0xffC83E37)),
      borderRadius: BorderRadius.circular(4.0),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        brightness: Brightness.dark,
        iconTheme: IconThemeData(color: AppColors.white),
      ),
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment(0.0, -1.0),
            end: Alignment(0.0, 1.0),
            colors: [
              const Color(0xffff0000),
              const Color(0xff800000),
              const Color(0xffd50000)
            ],
            stops: [0.0, 0.468, 1.0],
          ),
        ),
        child: Obx(() {
         // print('hasPlayed: ${state.hasPlayed} - ${state.currentTimeIndex} ');
          if (state.hasPlayed) {
            _pinPutController.text = state.myTextNumbers;
          }
          String phase = "";
          if (controller.canPlay) {
            phase =
                '${controller.hourWillPlayText} ngày ${DateFormat('dd/MM/yyyy').format(DateTime.now())}';
          }
          return SingleChildScrollView(
            child: Column(
              children: [
                AppImage('assets/img/banner_lottery_home.png'),
                if (controller.canPlay)
                  _buildContainerWithHeader(
                      isLoading: state.isLoading,
                      header: Column(
                        children: [
                          Text(
                            'Phiên quay $phase',
                            style: TextStyle(
                              fontFamily: 'Roboto',
                              fontSize: 16,
                              color: const Color(0xffff0000),
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          SizedBox(height: 5),
                          // Row(
                          //   mainAxisAlignment: MainAxisAlignment.center,
                          //   children: [
                          //     _buildTimeNumber(number: '04'),
                          //     _buildTimeSeparate(),
                          //     _buildTimeNumber(number: '35'),
                          //     _buildTimeSeparate(),
                          //     _buildTimeNumber(number: '22'),
                          //   ],
                          // )
                        ],
                      ),
                      children: [
                        SizedBox(height: 14),
                        Text(
                          !state.hasPlayed
                              ? 'Chọn 3 cặp số của bạn'
                              : 'Cặp số bạn đã chọn',
                          style: TextStyle(
                            fontFamily: 'Quicksand',
                            fontSize: 16,
                            color: const Color(0xff333333),
                          ),
                        ),
                        SizedBox(height: 12),
                        Container(
                          color: Colors.white,
                          //margin: const EdgeInsets.all(20.0),
                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                          child: _buildPinPut(),
                        ),
                        SizedBox(height: 18),
                        if (!state.hasPlayed) _randomButton(),
                        if (!state.hasPlayed) SizedBox(height: 7),
                        if (!state.hasPlayed)
                          _buildButtonConfirm()
                        else
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 20),
                            child: Text(
                              'Đón xem kết quả phiên quay vào lúc $phase',
                              style: TextStyle(
                                fontFamily: 'Quicksand',
                                fontSize: 14,
                                color: const Color(0xffff0000),
                                fontWeight: FontWeight.w700,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        SizedBox(height: 14),
                      ])
                else
                  Container(
                    margin: EdgeInsets.only(top: 40),
                    alignment: Alignment.center,
                    child: Text(
                      'Quay lại vào hôm sau nhé!',
                      style: TextStyle(color: AppColors.white),
                    ),
                  ),
                SizedBox(height: 40),
                if (!state.isLoading &&
                    state.gameByGroup?.keys?.isNotEmpty == true)
                  buildGoldContainer(
                      marginTop: 100,
                      child: Column(
                        children: [
                          TabBar(
                            controller: TabController(
                                initialIndex: state.resultTabIndex.value,
                                length: state.gameByGroup.keys.length,
                                vsync: this),
                            tabs: _buildDateTab(),
                            indicatorColor: AppColors.transparent,
                            labelPadding: EdgeInsets.zero,
                            onTap: (index) => controller.changeDayTab(index),
                          ),
                          SizedBox(height: 10),
                          Container(
                            color: AppColors.divider,
                            padding: EdgeInsets.symmetric(
                                horizontal: 0.5, vertical: 0.5),
                            child: Column(
                              children: [
                                _buildTextRow(texts: [
                                  'Phiên',
                                  'Giải thưởng',
                                  'Số trúng',
                                  'Số của bạn'
                                ]),
                                ...state.gameHistoryByDay
                                    .map((element) => _buildTextRow(
                                          texts: [
                                            controller
                                                .phaseTimeText(element.phase)
                                                .toString(),
                                            '${element.winPoint} xu',
                                            element.wonNumber == null
                                                ? '-'
                                                : NumberFormat('00')
                                                    .format(element.wonNumber),
                                            element.lotteryDataString,
                                          ],
                                          redText: element.wonNumber == null
                                              ? '-'
                                              : NumberFormat('00')
                                                  .format(element.wonNumber),
                                        ))
                              ],
                            ),
                          ),
                        ],
                      ),
                      title: AppImage('assets/img/img_lottery_result.png')),
                SizedBox(height: 14),
                if (state.winners.isNotEmpty) _buildWinnerList(),
                SizedBox(height: 20),
                _buildRuleBoard(),
                SizedBox(height: 20),
                _buildRuleGame(),
                SizedBox(height: 60),
              ],
            ),
          );
        }),
      ),
    );
  }

  Widget _buildTextRow({List<String> texts, String redText}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        _buildText(flex: 2, text: texts[0], redText: redText),
        _buildText(flex: 3, text: texts[1], redText: redText),
        _buildText(flex: 3, text: texts[2], redText: redText),
        _buildText(flex: 3, text: texts[3], redText: redText),
      ],
    );
  }

  Expanded _buildText({String text, int flex, String redText}) {
    return Expanded(
      flex: 2,
      child: Container(
        margin: EdgeInsets.only(top: 1, left: 0.5, right: 0.5, bottom: 0.5),
        padding: EdgeInsets.symmetric(vertical: 10),
        color: AppColors.white,
        child: Text(
          text ?? '-',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'Quicksand',
            fontSize: 12,
            color: text == redText ? AppColors.red : Color(0xff333333),
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
    );
  }

  List<Widget> _buildDateTab() {
    List<Widget> tabs = List<Widget>.empty(growable: true);
    for (int i = 0; i < state.gameByGroup.keys.length; i++) {
      tabs.add(Container(
        padding: EdgeInsets.symmetric(horizontal: 8, vertical: 7),
        child: Text(
          DateFormat('dd/MM').format(state.gameByGroup.keys.toList()[i]),
          style: TextStyle(
            fontFamily: 'Quicksand',
            fontSize: 12,
            color: i == state.resultTabIndex.value
                ? Color(0xffff0000)
                : Color(0xff707070),
            fontWeight: FontWeight.w700,
          ),
          maxLines: 1,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(14.0),
          color: i == state.resultTabIndex.value
              ? Color(0xffffb5b5)
              : Color(0xffDDDDDD),
        ),
      ));
    }
    return tabs;
  }

  Widget _buildWinnerList() {
    return _buildContainerWithHeader(
        height: 240.h,
        header: Text(
          'Danh sách trúng thưởng (${state.winners.length})',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'Quicksand',
            fontSize: 16,
            color: const Color(0xffff0000),
            fontWeight: FontWeight.w700,
          ),
        ),
        children: [
          Expanded(
            child: state.winners.isEmpty
                ? Center(
                    child: Text(
                      'Chưa có danh sách trúng thưởng',
                      style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 14,
                        color: const Color(0xff707070),
                      ),
                    ),
                  )
                : Container(
                    height: double.infinity,
                    alignment: Alignment.topCenter,
                    child: ListView.builder(
                      itemBuilder: _buildUserItem,
                      itemCount: state.winners.length,
                      primary: false,
                      padding: EdgeInsets.zero,
                    ),
                  ),
          ),
        ]);
  }

  Widget _buildUserItem(
    BuildContext context,
    int index,
  ) {
    final item = state.winners[index];
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Row(
        children: [
          Text(
            (index + 1).toString(),
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 12,
              color: const Color(0xff333333),
            ),
          ),
          SizedBox(width: 10),
          Container(
            width: 24,
            height: 24,
            margin: EdgeInsets.all(2),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
              image: DecorationImage(
                image: CachedNetworkImageProvider(item.user.image?.thumb ?? ''),
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(width: 10),
          Expanded(
            child: Text(
              item.user.name,
              style: TextStyle(
                fontFamily: 'Quicksand',
                fontSize: 12,
                color: const Color(0xff333333),
              ),
            ),
          ),
          AppImage('assets/svg/ic_coin_small.svg'),
          SizedBox(width: 16),
          Text(
            item.win_point.toString(),
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 12,
              fontWeight: FontWeight.bold,
              color: const Color(0xff333333),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildPinPut() {
    return PinPut(
      fieldsCount: 6,
      focusNode: _pinPutFocusNode,
      enabled: state.myTextNumbers.isEmpty,
      eachFieldPadding: EdgeInsets.symmetric(horizontal: 0),
      pinAnimationType: PinAnimationType.fade,
      preFilledWidget: Text(
        '-',
        style: TextStyle(
          fontFamily: 'Roboto',
          fontSize: 24,
          color: const Color(0xff8e0000),
          fontWeight: FontWeight.w700,
        ),
      ),
      separator: SizedBox(width: 20),
      separatorPositions: [2, 4],
      textStyle: TextStyle(
        fontFamily: 'Roboto',
        fontSize: 24,
        color: const Color(0xff8e0000),
        fontWeight: FontWeight.w700,
      ),
      controller: _pinPutController,
      submittedFieldDecoration: _pinPutDecoration,
      disabledDecoration: _pinPutDecoration,
      selectedFieldDecoration: _pinPutDecoration.copyWith(
        border: Border.all(width: 2, color: Color(0xffC83E37)),
      ),
      followingFieldDecoration: _pinPutDecoration,
      onChanged: (_) => controller.calculateCurrentPhase(),
    );
  }

  Widget _buildRuleGame() {
    return buildGoldContainer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              _buildGuideItem(
                  index: 0,
                  text:
                      'Chọn cặp số yêu thích của bạn trước mỗi khung giờ quay số'),
              _buildGuideItem(
                  index: 1, text: 'Chọn xác nhận số và nhập giá trị Xu cược'),
            ],
          ),
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              _buildGuideItem(
                  index: 2,
                  text:
                      'Theo dõi kết quả được công bố vào mỗi khung giờ quay số'),
              _buildGuideItem(
                  index: 3,
                  text:
                      'Hệ thống sẽ gửi tiền thưởng vào tài khoản FindNear của bạn'),
            ],
          ),
        ],
      ),
      title: AppImage('assets/svg/img_how_to_lay.svg'),
    );
  }

  Widget _buildContainerWithHeader(
      {Widget header,
      List<Widget> children,
      double height,
      bool isLoading = false}) {
    return Container(
      width: double.infinity,
      height: height,
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 9),
            width: double.infinity,
            child: header,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                topRight: Radius.circular(10.0),
              ),
              color: const Color(0xffffb5b5),
            ),
          ),
          if (isLoading)
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(top: 30, bottom: 30),
              child: LoadingView(),
            )
          else
            ...children,
        ],
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: const Color(0xffffffff),
      ),
    );
  }

  Widget _buildButtonConfirm() {
    return GestureDetector(
      onTap: () async {
        if (_pinPutController.text.length < 6) {
          AppSnackbar.showError(message: 'Vui lòng chọn đủ 3 cặp số');
        } else if (currentUser.value?.currentPoint <= 0) {
          AppSnackbar.showError(message: 'Bạn không có đủ xu để chơi');
        } else {
          controller.calculateCurrentPhase();
          final List<int> numbers =
              controller.getSelectedNumbers(_pinPutController.text);
          final result = await showDialog(
              context: context,
              builder: (context) => BetConfirmPopup(
                    inputController: _petInputController,
                    selectedNumbers: numbers,
                  ));
          print('result = $result');
          if (result == true) {
            _playGame();
            _pinPutController.clear();
          }
        }
      },
      child: Obx(() {
        return Container(
          margin: EdgeInsets.symmetric(horizontal: 30),
          padding: EdgeInsets.symmetric(horizontal: 30, vertical: 9),
          width: double.infinity,
          alignment: Alignment.center,
          child: state.isSubmiting.value
              ? LoadingView()
              : Text(
                  'Xác nhận chọn số',
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 15,
                    color: const Color(0xffffffff),
                    fontWeight: FontWeight.w700,
                  ),
                ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4.0),
            color: const Color(0xffff0000),
          ),
        );
      }),
    );
  }

  Future<void> _playGame() async {
    // DialogHelper.showLoaderDialog();
    final played = await controller.playGame(
        controller.getSelectedNumbers(_pinPutController.text),
        _petInputController.text);
    // Get.back();
    if (played) {
      AppSnackbar.showInfo(
          message:
              'Chọn cặp số và cược thành công.\nBạn nhớ theo dõi phiên quay sắp tới để\nxem kết quả trúng giải nhé');
    }
  }

  Widget _randomButton() {
    return GestureDetector(
      onTap: () {
        controller.calculateCurrentPhase();
        _pinPutController.text = controller.randomNumbers();
      },
      child: Container(
        width: double.infinity,
        margin: EdgeInsets.symmetric(horizontal: 30),
        padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
        alignment: Alignment.center,
        child: Row(
          children: [
            AppImage('assets/svg/ic_dice.svg'),
            SizedBox(width: 3),
            Text(
              'Chọn ngẫu nhiên',
              style: TextStyle(
                fontFamily: 'Quicksand',
                fontSize: 15,
                color: const Color(0xff333333),
              ),
            ),
          ],
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4.0),
          color: const Color(0xffffffff),
          border: Border.all(width: 1.0, color: const Color(0xffc83e37)),
        ),
      ),
    );
  }

  Widget _buildGuideItem({int index, String text}) {
    return Container(
      width: 137.w,
      height: 95.h,
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(top: 10),
            padding: EdgeInsets.only(left: 10, right: 10, top: 30),
            height: double.infinity,
            child: Text(
              text,
              style: TextStyle(
                fontFamily: 'Quicksand',
                fontSize: 11.sp,
                color: const Color(0xff333333),
              ),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              gradient: LinearGradient(
                begin: Alignment(0.0, -1.0),
                end: Alignment(0.0, 1.0),
                colors: [const Color(0xffffe59a), const Color(0xffffffd5)],
                stops: [0.0, 1.0],
              ),
              boxShadow: [
                BoxShadow(
                  color: const Color(0x33818181),
                  offset: Offset(0, 3),
                  blurRadius: 3,
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: Text(
              'Bước ${index + 1}',
              style: TextStyle(
                fontFamily: 'Quicksand',
                fontSize: 16,
                color: const Color(0xffffffff),
                fontWeight: FontWeight.w700,
              ),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                topRight: Radius.circular(10.0),
                bottomRight: Radius.circular(10.0),
              ),
              color: const Color(0xffff0000),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildRuleBoard() {
    return buildGoldContainer(
        title: AppImage('assets/svg/img_rule.svg'),
        child: Column(
          children: [
            Text.rich(
              TextSpan(
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 14,
                  color: const Color(0xff333333),
                ),
                children: [
                  TextSpan(
                    text: '1/ Đối tượng tham gia\n',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  TextSpan(
                    text:
                        'Tất cả người dùng có tài khoản tại ứng dụng FindNear đủ 18 tuổi trở lên.\n',
                  ),
                  TextSpan(
                    text: '2/ Cách thức tham dự:\n',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  TextSpan(
                    text:
                        '- Mỗi ngày có 5 phiên quay số lúc 09h00, 11h00, 15h00, 18h00, 21h00. \n- Người chơi chọn cho mình 3 cặp số yêu thích gồm 2 chữ số có giá trị từ 00 ->99 hoặc chọn số ngẫu nhiên từ hệ thống. \n- Khi đã chọn được số ưng ý, chọn xác nhận để thực hiện cược số.\n- Mỗi lượt khung giờ người chơi có 1 lượt cược số. Người chơi có thể cược tối thiểu 50 xu, tối đa 5000 xu.\n- Cặp số may mắn được sổ vào mỗi khung giờ trùng với 1 trong 3 cặp số bạn chọn sẽ nhận được tiền thưởng.\n',
                  ),
                  TextSpan(
                    text: '3/ Thông tin giải thưởng\n',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  TextSpan(
                    text:
                        '- Giá trị tiền thưởng bằng giá trị số tiền cược X 5 lần. \nVd: bạn cược 100 xu, giá trị tiền thưởng sẽ là 100x5 = 500 xu',
                  ),
                ],
              ),
              textHeightBehavior:
                  TextHeightBehavior(applyHeightToFirstAscent: false),
              textAlign: TextAlign.left,
            ),
            _buildDetailRule(),
          ],
        ));
  }

  Widget _buildDetailRule() => GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () async {
          final lotteryRulesUrl =
              "${GlobalConfiguration().getValue(lotteryRules)}";
          if (await canLaunch(lotteryRulesUrl)) {
            await launch(lotteryRulesUrl);
          }
        },
        child: Container(
          child: Text(
            S.of(context).detailedRules,
            style: Theme.of(context)
                .textTheme
                .bodyText1
                .copyWith(color: Colors.blue),
          ),
        ),
      );

  Widget _buildTimeSeparate() {
    return Container(
      width: 16,
      alignment: Alignment.center,
      child: Text(
        ':',
        style: TextStyle(
          fontFamily: 'Roboto',
          fontSize: 16,
          color: const Color(0xff333333),
          fontWeight: FontWeight.w700,
        ),
      ),
    );
  }
}
