import 'package:collection/collection.dart';
import 'package:findnear/src/base/base_state.dart';
import 'package:findnear/src/models/game.dart';
import 'package:findnear/src/models/lotte_game_result.dart';
import 'package:findnear/src/utils/logger.dart';

import 'package:get/get.dart';
import 'package:findnear/src/helpers/date_utils.dart';

class LotteryState extends BaseState{
  final isSubmiting = false.obs;
  final currentTimeIndex = (-1).obs;
  final gameHistory = List<GameHistory>.empty().obs;
  final resultTabIndex = 0.obs;
  final winners = List<LotteGameResult>.empty().obs;

  List<String> get myLatestNumbers {
    //print('myLatestNumbers $latestPhase - ${gameHistory?.last?.lotteryData}');
    if (latestPhase == -1) {
      return List<String>.empty();
    }
    try {
      return gameHistory.last.lotteryData;
    } catch (e, s) {
      error = e;
      prettyLog.e(e, s);
      return List<String>.empty();
    }
  }

  Map<DateTime, List<GameHistory>> get gameByGroup =>
      groupBy(gameHistory, (game) => game.createAtDay);

  List<GameHistory> get gameHistoryByDay {
    try {
      return gameByGroup[gameByGroup.keys.toList()[resultTabIndex.value]];
    } catch (e, s) {
      error = e;
      prettyLog.e(e, s);
      return List<GameHistory>.empty();
    }
  }

  // Map<DateTime, List<GameHistory>> get gameByGroup => {
  //       DateTime(2021, 8, 8): gameHistory,
  //       DateTime(2021, 8, 9): gameHistory,
  //       DateTime(2021, 8, 10): gameHistory,
  //       DateTime(2021, 8, 11): gameHistory,
  //       DateTime(2021, 8, 12): gameHistory,
  //     };

  String get myTextNumbers => myLatestNumbers.isEmpty ? "" : myLatestNumbers.join();

  int get latestPhase {
    if(gameHistory.isEmpty) return -1;

    if(gameHistory.last.createAtDate.toLocal().isSameDay(DateTime.now())){
      return gameHistory.last.phase;
    }
    return -1;
  }

  bool get hasPlayed =>
      latestPhase == currentTimeIndex.value && latestPhase != -1;
}
