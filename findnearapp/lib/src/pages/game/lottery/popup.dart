import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../commons/app_colors.dart';
import '../../../widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class BetConfirmPopup extends StatelessWidget {
  final TextEditingController inputController;
  final List<int>  selectedNumbers;

  BetConfirmPopup({Key key, this.inputController, this.selectedNumbers}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: AppColors.transparent,
      insetPadding: EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        height: 310.h,
        width: double.infinity,
        child: Stack(
          children: [
            Container(
              margin:
                  EdgeInsets.only(top: 30.h, bottom: 15.h, left: 5, right: 5),
              width: double.infinity,
              child: Stack(
                children: [
                  Positioned(
                    child: AppImage(
                      'assets/svg/img_flash.svg',
                      fit: BoxFit.fill,
                    ),
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                  ),
                  Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(height: 30.h),
                        Text(
                          '3 cặp số bạn đã chọn',
                          style: TextStyle(
                            fontFamily: 'Quicksand',
                            fontSize: 16,
                            color: const Color(0xffffffff),
                          ),
                        ),
                        SizedBox(height: 8.h),
                        Container(
                          padding: EdgeInsets.all(8.w),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: selectedNumbers.map((e) => _buildNumber(e)).toList(),
                          ),
                          width: 158.w,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5.0),
                            color: const Color(0xffff5858),
                            border: Border.all(
                                width: 1.0, color: const Color(0xffffffff)),
                          ),
                        ),
                        SizedBox(height: 12.h),
                        Text(
                          'Nhập số tiền muốn cược',
                          style: TextStyle(
                            fontFamily: 'Quicksand',
                            fontSize: 16,
                            color: const Color(0xffffffff),
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        SizedBox(height: 8.h),
                        Container(
                          width: 162.w,
                          //height: 42.h,
                          alignment: Alignment.center,
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: Row(
                            children: [
                              AppImage(
                                'assets/svg/ic_coin_small.svg'),
                              Expanded(
                                child: TextField(
                                  controller: inputController,
                                  maxLines: 1,
                                  style: TextStyle(
                                    fontFamily: 'Roboto',
                                    fontSize: 24,
                                    color: const Color(0xffffffff),
                                    fontWeight: FontWeight.w700,
                                  ),
                                  inputFormatters: [
                                    FilteringTextInputFormatter.digitsOnly
                                  ],
                                  textAlign: TextAlign.center,
                                  decoration: InputDecoration(
                                    isDense: true,
                                    contentPadding: EdgeInsets.symmetric(vertical: 4),
                                    focusedBorder: InputBorder.none,
                                    enabledBorder: InputBorder.none,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5.0),
                            color: const Color(0xff8e0700),
                            border: Border.all(
                                width: 1.0, color: const Color(0xffffffff)),
                          ),
                        ),
                        SizedBox(height: 8.h),
                        Text(
                          '***Tối thiểu 50Xu, tối đa 5000Xu',
                          style: TextStyle(
                            fontFamily: 'Quicksand',
                            fontSize: 12,
                            color: const Color(0xffffc24b),
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                gradient: LinearGradient(
                  begin: Alignment(0.0, -1.0),
                  end: Alignment(0.0, 1.0),
                  colors: [const Color(0xffff0c00), const Color(0xffbe0000)],
                  stops: [0.0, 1.0],
                ),
              ),
            ),
            Positioned(
              child: Center(child: AppImage('assets/img/img_your_numbers.png')),
              right: 0,
              left: 0,
            ),
            _buildConfirmButton(),
            GestureDetector(
              onTap: () => Get.back(),
              child: Container(
                margin: EdgeInsets.only(top: 18.h),
                alignment: Alignment.topRight,
                child: AppImage('assets/svg/btn_close.svg'),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildConfirmButton() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: GestureDetector(
        onTap: () {
          final coin = int.tryParse(inputController.text) ?? 0;
          if(coin< 50 || coin >5000){
            AppSnackbar.showError(message: 'Tối thiểu 50Xu, tối đa 5000Xu');
          }else{
            Get.back(result: true);
          }

        },

        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 25, vertical: 6),
          child: Text(
            'Xác nhận cược',
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 16,
              color: const Color(0xff8e0000),
              fontWeight: FontWeight.w700,
            ),
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20.0),
            gradient: LinearGradient(
              begin: Alignment(0.0, -1.11),
              end: Alignment(0.0, 1.81),
              colors: [
                const Color(0xfff7aa00),
                const Color(0xfff9db00),
                const Color(0xffed9700),
                const Color(0xfff7aa00)
              ],
              stops: [0.0, 0.352, 0.727, 1.0],
            ),
            boxShadow: [
              BoxShadow(
                color: const Color(0x29000000),
                offset: Offset(0, 3),
                blurRadius: 6,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildNumber(int number) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5.0),
        color: const Color(0xffffffff),
      ),
      child: Text(
        NumberFormat('00').format(number),
        style: TextStyle(
          fontFamily: 'Roboto',
          fontSize: 24.sp,
          color: const Color(0xff8e0000),
          fontWeight: FontWeight.w700,
        ),
      ),
      padding: EdgeInsets.symmetric(horizontal: 3, vertical: 9),
    );
  }
}
