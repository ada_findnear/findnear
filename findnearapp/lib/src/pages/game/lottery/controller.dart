import 'dart:math';

import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/models/lotte_game_result.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../models/game.dart';

import '../../../repository/game_repository.dart';

import 'state.dart';

class LotteryController extends BaseController<LotteryState> {
  GameRepository gameRepository;

  LotteryController(this.gameRepository);

  @override
  createState() => LotteryState();

  int coin = 0;

  final timeRange = [
    TimeOfDay(hour: 9, minute: 0),
    TimeOfDay(hour: 11, minute: 0),
    TimeOfDay(hour: 15, minute: 0),
    TimeOfDay(hour: 18, minute: 0),
    TimeOfDay(hour: 21, minute: 0),
  ];

  //start at 0
  int get currentTimeIndex {
    final now = TimeOfDay.now();
    //final now = TimeOfDay(hour: 20,minute: 0);
    for (int i = 0; i < timeRange.length; i++) {
      if (now.compareTo(timeRange[i]) < 0) {
        return i;
      }
    }
    return -1;
  }

  TimeOfDay phaseTime(int index) =>
      index >= timeRange.length || index < 0 ? null : timeRange[index];

  String phaseTimeText(int index) => phaseTime(index)?.formatString ?? "";

  @override
  void onInit() {
    calculateCurrentPhase();
    getHistory();
    getWinners();
    super.onInit();
  }

  void calculateCurrentPhase() {
    state.currentTimeIndex.value = currentTimeIndex;
    print('currentTimeIndex: $currentTimeIndex');
  }

  bool get canPlay => state.currentTimeIndex.value != -1;

  TimeOfDay get hourWillPlay => timeRange[state.currentTimeIndex.value];

  String get hourWillPlayText =>
      '${NumberFormat('00').format(hourWillPlay.hour)}h${NumberFormat('00').format(hourWillPlay.minute)}';

  String randomNumbers() {
    String first = NumberFormat('00').format(Random().nextInt(99));
    String second = NumberFormat('00').format(Random().nextInt(99));
    String third = NumberFormat('00').format(Random().nextInt(99));
    return first + second + third;
  }

  Future<bool> playGame(List<int> data, String coin) async {
    if (state.isSubmiting.value || state.isLoading) return false;
    if (int.parse(coin) > currentUser.value?.currentPoint) {
      AppSnackbar.showError(message: 'Bạn không có đủ xu để chơi');
      return false;
    }
    state.isSubmiting.value = true;

    try {
      final user = await gameRepository.playGame(
        id: 2,
        point: int.parse(coin),
        phase: currentTimeIndex,
        data: data,
      );
      final newUser = currentUser.value?.copyWith(
          currentPoint: user.currentPoint, totalPoint: user.totalPoint);
      currentUser.value = newUser;
      await getHistory();
      state.isSubmiting.value = false;
      return false;
    } catch (e) {
      state.error = e;
      state.isSubmiting.value = false;
      return false;
    }
  }

  List<int> getSelectedNumbers(String text) {
    prettyLog.d('getSelectedNumbers $text');
    return <int>[
      int.parse(text[0] + text[1]),
      int.parse(text[2] + text[3]),
      int.parse(text[4] + text[5])
    ];
  }

  void getHistory() async {
    state.isLoading = true;
    try {
      final now = DateTime.now();
      DateTime startDate = DateTime(now.year, now.month, now.day, 7, 0).toUtc();
      startDate = startDate.subtract(Duration(days: 7));
      state.gameHistory.value =
          (await gameRepository.getGameHistories('2', startDate: startDate)) ??
              List<GameHistory>.empty();
      state.resultTabIndex.value = state.gameByGroup.length - 1;
    } catch (e) {
      state.isLoading = false;
    }
    state.isLoading = false;
  }

  void changeDayTab(int index) {
    state.resultTabIndex.value = index;
  }

  void getWinners() {
    state.isLoading = true;

    gameRepository.getLotteryWinners().then(
      (value) {
        state.winners.value = value ?? List<LotteGameResult>.empty();
        state.isLoading = false;
      },
    ).catchError((error) {
      state.isLoading = false;
      state.error = error;
    });
  }
}

extension TimeOfDayExtension on TimeOfDay {
  int compareTo(TimeOfDay other) {
    if (this.hour < other.hour) return -1;
    if (this.hour > other.hour) return 1;
    if (this.minute < other.minute) return -1;
    if (this.minute > other.minute) return 1;
    return 0;
  }

  String get formatString =>
      '${NumberFormat('00').format(hour)}:${NumberFormat('00').format(minute)}';
}
