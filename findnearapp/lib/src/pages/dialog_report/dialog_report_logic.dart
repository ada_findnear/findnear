import 'package:dio/dio.dart';
import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/repository/newsfeed_repository.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:get/get.dart';

import 'dialog_report_state.dart';

class DialogReportLogic extends GetxController {
  Post post;
  final NewsfeedRepository newsfeedRepository = Get.find();
  final DialogReportState state = DialogReportState();

  DialogReportLogic();

  void updateReportDescription(String value) {
    state.description.value = value;
  }

  void sendReport() async {
    if (post?.id == null) return;
    try {
      final result = await newsfeedRepository.sendReport(
        postId: post.id,
        content: state.description.value,
      );
      state.isReportSent.value = result == true;
    } catch (e) {
      state.isReportSent.value = false;
      prettyLog.e(e);
      AppSnackbar.showError(
          message: (e is DioError)
              ? e.message
              : S.of(Get.context).verify_your_internet_connection);
    }
  }
}
