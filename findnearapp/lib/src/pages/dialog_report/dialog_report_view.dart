import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/pages/dialog_report/dialog_report_logic.dart';
import 'package:findnear/src/pages/dialog_report/dialog_report_state.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DialogReportWidget extends StatefulWidget {
  final Post post;

  const DialogReportWidget({Key key, @required this.post}) : super(key: key);

  @override
  _DialogReportWidgetState createState() => _DialogReportWidgetState();
}

class _DialogReportWidgetState extends State<DialogReportWidget> {
  final DialogReportLogic logic =
      Get.put<DialogReportLogic>(DialogReportLogic());
  final DialogReportState state = Get.find<DialogReportLogic>().state;

  @override
  void initState() {
    super.initState();
    logic.post = widget.post;
    state.isReportSent.stream.listen(isReportSentListener);
  }

  void isReportSentListener(bool isSent) async {
    if (isSent) {
      FocusScope.of(context).unfocus();
      // DialogHelper.showToast(
      //   message: S.of(context).reportThisPostSuccess,
      // );
      Get.back();
      Get.snackbar(
        "Thông báo",
        S.of(context).reportThisPostSuccess,
        backgroundColor: Colors.green,
        colorText: Colors.white,
      );
      await Future.delayed(Duration(seconds: 2));
      Get.back();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: const EdgeInsets.symmetric(horizontal: 16),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15))),
      child: Obx(() {
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 22),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(height: 16),
              Text(
                S.of(context).report,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 18,
                  color: const Color(0xff333333),
                  fontWeight: FontWeight.w700,
                ),
              ),
              SizedBox(height: 16),
              Text(
                S.of(context).reportDetail,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: 14,
                  color: const Color(0xff333333),
                ),
              ),
              SizedBox(height: 5),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: TextFormField(
                      onChanged: (value) =>
                          logic.updateReportDescription(value),
                      validator: (input) => state.description.isEmpty
                          ? S.of(context).reportReasonShouldntBeEmpty
                          : null,
                      textCapitalization: TextCapitalization.sentences,
                      textInputAction: TextInputAction.newline,
                      minLines: 1,
                      maxLines: 3,
                      decoration: InputDecoration(
                          contentPadding: const EdgeInsets.all(16),
                          isDense: true,
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: AppColors.gray, width: 0.5)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: AppColors.gray, width: 0.5)),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: AppColors.gray, width: 0.5)),
                          hintStyle: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 17,
                              color: AppColors.grayLight)),
                      keyboardType: TextInputType.multiline,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                    ),
                  )
                ],
              ),
              const SizedBox(height: 8),
              ElevatedButton(
                onPressed: () async {
                  prettyLog.d("TODO: SEND REPORT");
                  if (state.description.isNotEmpty) {
                    logic.sendReport();
                  } else {}
                },
                child: Text(
                  S.current.send,
                  style: const TextStyle(
                    fontSize: 14,
                    color: const Color(0xffffffff),
                    fontWeight: FontWeight.w700,
                  ),
                ),
                style: ElevatedButton.styleFrom(
                  shape: StadiumBorder(),
                  primary: state.description.isNotEmpty
                      ? AppColors.red
                      : AppColors.gray,
                  elevation: 0,
                ),
              ),
              const SizedBox(height: 16),
            ],
          ),
          width: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20.0),
            color: const Color(0xffffffff),
            boxShadow: [
              BoxShadow(
                color: const Color(0x29000000),
                offset: Offset(0, 0),
                blurRadius: 6,
              ),
            ],
          ),
        );
      }),
    );
  }

  @override
  void dispose() {
    super.dispose();
    Get.delete<DialogReportLogic>();
  }
}
