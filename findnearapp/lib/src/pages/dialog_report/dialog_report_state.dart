import '../../models/post.dart';
import 'package:flutter/material.dart';
import 'package:get/get_rx/get_rx.dart';

class DialogReportState {
  RxString description = "".obs;
  RxBool isReportSent = false.obs;

  DialogReportState();
}
