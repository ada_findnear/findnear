import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'notification_detail_logic.dart';
import 'notification_detail_state.dart';

class NotificationDetailPage extends StatefulWidget {
  @override
  _NotificationDetailPageState createState() => _NotificationDetailPageState();
}

class _NotificationDetailPageState extends State<NotificationDetailPage> {
  final NotificationDetailLogic logic = Get.put(NotificationDetailLogic());
  final NotificationDetailState state = Get.find<NotificationDetailLogic>().state;

  @override
    Widget build(BuildContext context) {
      return Container();
    }

  @override
  void dispose() {
    Get.delete<NotificationDetailLogic>();
    super.dispose();
  }
}