import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../models/notification.dart' as model;

class NotificationListState {
  RxList<model.Notification> notifications = <model.Notification>[].obs;
  RxInt currentOffset = 0.obs;
  Rx<bool> isLoading = false.obs;
  Rx<bool> isLoadingMore = false.obs;
  RxBool canLoadMore = true.obs;
  Rx<RefresherAction> refreshControllerAction = RefresherAction.idle.obs;
  RxInt unReadNotificationsCount = 0.obs;
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  NotificationListState() {
    ///Initialize variables
  }
}

enum RefresherAction {
  idle, refreshCompleted, refreshFailed, loadComplete, loadFailed
}
