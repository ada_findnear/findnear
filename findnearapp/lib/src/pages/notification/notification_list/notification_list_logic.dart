import 'package:findnear/main.dart';
import 'package:findnear/src/repository/notification_repository.dart';
import 'package:findnear/src/repository/friends_repository.dart' as friendRepo;
import 'package:findnear/src/repository/refactored_notification_repository.dart';
import 'package:findnear/src/services/notification_service.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'notification_list_state.dart';
import '../../../models/notification.dart' as model;

class NotificationListLogic extends GetxController {
  final state = NotificationListState();
  final NewNotificationRepository notificationRepository = Get.find();
  final NotificationService notificationService =
      Get.find<NotificationService>();

  NotificationListLogic();

  void _resetData() {
    state.notifications.clear();
    state.canLoadMore.value == true;
    state.currentOffset.value = 0;
  }

  void initialize() {
    _resetData();
    listenForNotifications();
    markAllAsRead();
  }

  void listenForNotifications({String message, bool isRefresh = false}) async {
    state.refreshControllerAction.value = RefresherAction.idle;
    state.isLoading.value = true;
    if (state.currentOffset.value != 0 && !isRefresh)
      state.isLoadingMore.value = true;
    try {
      final notifications = await notificationRepository.getMyNotifications(
          offset: isRefresh ? 0 : state.currentOffset.value, limit: 15);
      if (isRefresh) {
        state.isLoadingMore.value = false;
        _resetData();
        state.refreshControllerAction.value = RefresherAction.refreshCompleted;
      } else {
        state.refreshControllerAction.value = RefresherAction.loadComplete;
      }
      if (notifications.length > 0) {
        state.notifications.addAll(notifications);
      } else {
        state.canLoadMore.value = false;
      }
      state.currentOffset.value = state.notifications.length;
      state.isLoading.value = false;
      if (message != null) {
        ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    } catch (e) {
      prettyLog.e(e);
      state.isLoading.value = false;
      if (isRefresh) {
        state.refreshControllerAction.value = RefresherAction.refreshFailed;
      } else {
        state.refreshControllerAction.value = RefresherAction.loadFailed;
      }
      ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
        content: Text(S.of(Get.context).verify_your_internet_connection),
      ));
    }
  }

  Future<void> refreshNotifications() async {
    listenForNotifications(message: S.of(Get.context).notifications_refreshed_successfuly, isRefresh: true);
  }

  void doMarkAsReadNotifications(model.Notification _notification) async {
    markAsReadNotifications(_notification).then((value) {
      state.unReadNotificationsCount.value = state.unReadNotificationsCount.value - 1;
      _notification.read = !_notification.read;
      state.notifications.refresh();
      // ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
      //   content: Text(S.of(Get.context).thisNotificationHasMarkedAsRead),
      // ));
    });
  }

  void doMarkAsUnReadNotifications(model.Notification _notification) {
    markAsReadNotifications(_notification).then((value) {
      state.unReadNotificationsCount.value = state.unReadNotificationsCount.value + 1;
      _notification.read = !_notification.read;
      state.notifications.refresh();
      // ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
      //   content: Text(S.of(Get.context).thisNotificationHasMarkedAsUnread),
      // ));
    });
  }

  void doRemoveNotification(model.Notification _notification) async {
    removeNotification(_notification).then((value) {
      if (!_notification.read) {
        state.unReadNotificationsCount.value -= 1;
      }
      state.notifications.remove(_notification);
      ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
        content: Text(S.of(Get.context).notificationWasRemoved),
      ));
    });
  }

  void markAllAsRead() async {
    await clearNotificationsCount();
    notificationService.unreadNotificationCounter.value = 0;
    /*state.notifications.forEach((element) {
      element.read = true;
    });
    state.notifications.refresh();*/
  }

  void handleFriendRequest({int index, bool isAccept}) async {
    final notification = state.notifications.elementAt(index);
    final idUserTarget = notification.data['send_user_id'].toString();
    final type = isAccept ? "confirm" : "deny";
    try {
      await friendRepo.addFriend(idUserTarget, type);
      await refreshNotifications();
    } catch (e) {
      logger.e(e);
    }
  }
}
