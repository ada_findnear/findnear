import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/CircularAvatarWidget.dart';
import 'package:findnear/src/models/notification.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:findnear/src/helpers/date_utils.dart';

import '../../../../helpers/swipe_widget.dart';
import '../../../../models/notification.dart' as model;

typedef void FriendRequestCallback(bool);

class NotificationItemWidget extends StatelessWidget {
  final model.Notification notification;
  final VoidCallback onMarkAsRead;
  final VoidCallback onMarkAsUnRead;
  final VoidCallback onRemoved;
  final VoidCallback onTap;
  final FriendRequestCallback onFriendRequestAcceptOrDecline;

  NotificationItemWidget({
    Key key,
    this.notification,
    this.onMarkAsRead,
    this.onMarkAsUnRead,
    this.onRemoved,
    this.onTap,
    this.onFriendRequestAcceptOrDecline,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isDark = Theme.of(context).brightness == Brightness.dark;
    final isUnread = !notification.read;
    final desColor = isUnread
        ? Colors.black
        : isDark
            ? Colors.white
            : Colors.black;
    final unReadBackgroundColor =
        isDark ? Color(0xFFFFF4F4) : Color(0xFFFFF4F4);
    final bool isFriendRequestNotification =
        (notification.type == model.NotificationType.friendRequest.rawString);

    return GestureDetector(
      onTap: onTap,
      child: Container(
        child: OnSlide(
            backgroundColor: notification.read
                ? Theme.of(context).scaffoldBackgroundColor
                : unReadBackgroundColor,
            items: <ActionItems>[
              ActionItems(
                  icon: notification.read
                      ? new Icon(
                          Icons.panorama_fish_eye,
                          color: Theme.of(context).accentColor,
                        )
                      : new Icon(
                          Icons.brightness_1,
                          color: Theme.of(context).accentColor,
                        ),
                  onPress: () {
                    if (notification.read) {
                      onMarkAsUnRead();
                    } else {
                      onMarkAsRead();
                    }
                  },
                  backgroudColor: Theme.of(context).scaffoldBackgroundColor),
              ActionItems(
                  icon: Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: new Icon(Icons.delete_outline,
                        color: Theme.of(context).accentColor),
                  ),
                  onPress: () {
                    onRemoved();
                  },
                  backgroudColor: Theme.of(context).scaffoldBackgroundColor),
            ],
            child: Container(
              margin: const EdgeInsets.symmetric(vertical: 10),
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  if (isFriendRequestNotification) ...[
                    Text(
                      S.current.friendRequest,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                        color: AppColors.textBlack,
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(height: 8),
                  ],
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      CircularAvatarWidget(
                        imageUrl: notification.avatarPath,
                        size: 38,
                      ),
                      const SizedBox(width: 12),
                      Flexible(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            _buildTextContent(context),
                            SizedBox(height: 4),
                            IgnorePointer(
                              child: Row(
                                children: [
                                  _buildIcon(),
                                  SizedBox(width: 5.6),
                                  Text(
                                    AppDateFormatUtils
                                        .getVerboseDateTimeRepresentation(
                                            notification.createdAt),
                                    style: TextStyle(
                                      color: desColor.withOpacity(0.6),
                                      fontSize: 12,
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )),
      ),
    );
  }

  Widget _buildIcon() {
    if (notification.getNotificationType == null) return Container();
    switch (notification.getNotificationType) {
      case NotificationType.friendRequest:
        return AppImage(AppImages.icAddPerson,
            width: 15.47, height: 12.42, color: AppColors.red);
        break;
      case NotificationType.friendRequestAccepted:
        return AppImage(AppImages.icFriendRequestAccepted, color: AppColors.green2);
        break;
      case NotificationType.likePost:
      case NotificationType.likeComment:
        return AppImage(AppImages.icLike, color: AppColors.red);
        break;
      case NotificationType.commentPost:
        return AppImage(AppImages.icMessage, color: AppColors.yellow);
        break;
      case NotificationType.statusChangedOrder:
        return AppImage(AppImages.icMessage, color: AppColors.yellow);
        break;
      default:
        return AppImage(AppImages.icMessage, color: AppColors.yellow);
        break;
    }
  }

  Widget _buildTextContent(BuildContext context) {
    final username = notification?.data['user_name'] != null
        ? notification.data['user_name']
        : "";
    String contentText = "";
    switch (notification.getNotificationType) {
      case NotificationType.friendRequestAccepted:
        contentText = S.current.acceptedYourFriendRequest;
        break;
      case NotificationType.likePost:
        contentText = S.current.likedYourPost;
        break;
      case NotificationType.likeComment:
        contentText = S.of(context).likedYourComment;
        break;
      case NotificationType.commentPost:
        contentText = S.current.commentedYourPost;
        break;
      case NotificationType.statusChangedOrder:
        contentText = S.current.order_status_changed;
        break;
      case NotificationType.userCreateMoment:
        contentText = S.of(context).hasAddedANewMoment;
        break;
      case NotificationType.newOrder:
        contentText = S.of(context).new_order_from_client;
        break;
      case NotificationType.friendPost:
        contentText = S.of(context).friendPostNewStatus;
        break;
      case NotificationType.winGame:
        //TODO support multi-language?
        final gameName = notification?.data['game_name'] ?? '';
        contentText = ' đã chiến thắng trò chơi $gameName';
        break;
      case NotificationType.friendRequest:
        return Row(
          children: [
            Expanded(
              child: RichText(
                text: TextSpan(
                  text: username,
                  style: Theme.of(context).textTheme.bodyText1.copyWith(
                        color: AppColors.textBlack,
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                      ),
                ),
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
                textAlign: TextAlign.justify,
              ),
            ),
            InkWell(
              onTap: () {
                onFriendRequestAcceptOrDecline?.call(true);
              },
              child: Container(
                padding: const EdgeInsets.symmetric(vertical: 3, horizontal: 6),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(13),
                  color: AppColors.red,
                ),
                child: Center(
                  child: Text(
                    S.current.accept,
                    style: Theme.of(context).textTheme.bodyText1.copyWith(
                          color: AppColors.white,
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                        ),
                  ),
                ),
              ),
            ),
            const SizedBox(width: 6),
            InkWell(
              onTap: () {
                onFriendRequestAcceptOrDecline?.call(false);
              },
              child: Container(
                padding: const EdgeInsets.symmetric(vertical: 3, horizontal: 6),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(13),
                  border: Border.all(color: AppColors.grayBorder),
                  color: AppColors.white,
                ),
                child: Center(
                  child: Text(
                    S.current.decline,
                    style: Theme.of(context).textTheme.bodyText1.copyWith(
                      color: AppColors.textBlack,
                      fontSize: 14,
                    ),
                  ),
                ),
              ),
            )
          ],
        );
        break;
    }
    return IgnorePointer(
      ignoring: true,
      child: RichText(
        text: TextSpan(
          text: username,
          style: Theme.of(context).textTheme.bodyText1.copyWith(
                color: AppColors.textBlack,
                fontSize: 14,
                fontWeight: FontWeight.bold,
              ),
          children: [
            TextSpan(
              text: contentText,
              style: Theme.of(context).textTheme.bodyText1.copyWith(
                    color: AppColors.textBlack,
                    fontSize: 14,
                  ),
            ),
          ],
        ),
        overflow: TextOverflow.ellipsis,
        maxLines: 2,
        textAlign: TextAlign.justify,
      ),
    );
  }
}
