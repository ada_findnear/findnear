import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/controllers/page_controller.dart';
import 'package:findnear/src/elements/EmptyNotificationsWidget.dart';
import 'package:findnear/src/elements/PermissionDeniedWidget.dart';
import 'package:findnear/src/models/notification.dart' as model;
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/pages/newsfeed/details/newsfeed_post_detail_view.dart';
import 'package:findnear/src/pages/profile_user/profile_user_view.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'notification_list_logic.dart';
import 'notification_list_state.dart';
import 'widgets/notification_item_widget.dart';

class NotificationListPage extends StatefulWidget {
  @override
  _NotificationListPageState createState() => _NotificationListPageState();
}

class _NotificationListPageState extends State<NotificationListPage> {
  final NotificationListLogic logic = Get.put(NotificationListLogic());
  final NotificationListState state = Get.find<NotificationListLogic>().state;
  final PagesController _pageController = Get.find();
  final refreshController = RefreshController();

  @override
  void initState() {
    super.initState();
    logic.initialize();
    state.refreshControllerAction.stream.listen(onNewRefresherAction);
  }

  void onNewRefresherAction(RefresherAction action) {
    switch (action) {
      case RefresherAction.idle:
        break;
      case RefresherAction.refreshCompleted:
        refreshController.refreshCompleted();
        break;
      case RefresherAction.refreshFailed:
        refreshController.refreshCompleted();
        break;
      case RefresherAction.loadComplete:
        refreshController.loadComplete();
        break;
      case RefresherAction.loadFailed:
        refreshController.loadFailed();
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final isDark = Theme.of(context).brightness == Brightness.dark;
    return Scaffold(
      key: state.scaffoldKey,
      appBar: AppBar(
        leading: new IconButton(
          icon: Image.asset(isDark ? AppImages.icBackDark : AppImages.icBackLight),
          onPressed: () {
            Get.back();
          },
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        centerTitle: false,
        leadingWidth: 40,
        titleSpacing: 0,
        elevation: 0,
        title: Text(
          S.of(context).notifications,
          style: Theme.of(context).textTheme.bodyText1.copyWith(fontWeight: FontWeight.bold),
        ),
        actions: [
          /*GestureDetector(
            onTap: logic.markAllAsRead,
            child: Container(
              padding: EdgeInsets.all(12),
              child: Center(
                child: Text(
                  S.of(context).maskAllAsRead,
                  style: TextStyle(color: Theme.of(context).accentColor),
                ),
              ),
            ),
          )*/
        ],
      ),
      body: _buildBodyWidget(),
    );
  }

  Widget _buildBodyWidget() {
    return currentUser.value?.apiToken == null
        ? PermissionDeniedWidget()
        : Obx(() {
            return SmartRefresher(
              controller: refreshController,
              enablePullUp: state.canLoadMore.value,
              onRefresh: logic.refreshNotifications,
              onLoading: () => logic.listenForNotifications(),
              child: state.notifications.isEmpty
                  ? EmptyNotificationsWidget(
                      onExplorePressed: () {
                        Navigator.pop(context);
                      },
                    )
                  : ListView(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      children: <Widget>[
                        /*Padding(
                          padding: const EdgeInsets.only(left: 20, right: 10),
                          child: ListTile(
                            contentPadding: EdgeInsets.symmetric(vertical: 0),
                            leading: Icon(
                              Icons.notifications_outlined,
                              color: Theme.of(context).hintColor.withOpacity(0.6),
                            ),
                            // title: Text(
                            //   S.of(context).notifications,
                            //   maxLines: 1,
                            //   overflow: TextOverflow.ellipsis,
                            //   style: Theme.of(context).textTheme.headline4,
                            // ),
                            minLeadingWidth: 28,
                            subtitle: Text(
                              S.of(context).swipeLeftTheNotificationToDeleteOrReadUnreadIt,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context).textTheme.caption,
                            ),
                          ),
                        ),*/
                        ListView.separated(
                          padding: const EdgeInsets.only(bottom: 15),
                          scrollDirection: Axis.vertical,
                          shrinkWrap: true,
                          primary: false,
                          itemCount: state.notifications.length,
                          separatorBuilder: (context, index) {
                            return Divider(height: 1, color: Colors.white);
                          },
                          itemBuilder: (context, index) {
                            final notification =
                                state.notifications.elementAt(index);
                            return NotificationItemWidget(
                              notification: notification,
                              onTap: () {
                                handleTapNotification(notification);
                              },
                              onMarkAsRead: () {
                                logic.doMarkAsReadNotifications(notification);
                              },
                              onMarkAsUnRead: () {
                                logic.doMarkAsUnReadNotifications(notification);
                              },
                              onRemoved: () {
                                logic.doRemoveNotification(notification);
                              },
                              onFriendRequestAcceptOrDecline: (isAccepted) {
                                logic.handleFriendRequest(
                                    index: index, isAccept: isAccepted);
                              },
                            );
                          },
                        ),
                      ],
                    ),
            );
          });
  }

  void handleTapNotification(model.Notification notification) {
    final type = notification.getNotificationType;
    if(notification.read == false)
      logic.doMarkAsReadNotifications(notification);
    switch (type) {
      case model.NotificationType.friendRequest:
        //Request sent by other people
        final userId = notification.data['send_user_id'].toString();
        NavigatorUtils.navigateToProfilePage(userId);
        break;
      case model.NotificationType.friendRequestAccepted:
      //Request sent by me
        final userId = notification.data['receive_user_id'].toString();
        NavigatorUtils.navigateToProfilePage(userId);
        break;
      case model.NotificationType.likePost:
      case model.NotificationType.likeComment:
      case model.NotificationType.commentPost:
      case model.NotificationType.friendPost:
        final postId = notification.data['post_id'];
        if (postId != null) {
          Get.to(() => NewsfeedPostDetailsPage(
                post: Post(id: postId.toString()),
                isPostContainsOnlyId: true,
              ));
        }
        break;
      case model.NotificationType.userCreateMoment:
        navigator.pop();
        _pageController?.currentTab?.value = 0;
        break;
      default:
        break;
    }
  }

  @override
  void dispose() {
    Get.delete<NotificationListLogic>();
    super.dispose();
  }
}
