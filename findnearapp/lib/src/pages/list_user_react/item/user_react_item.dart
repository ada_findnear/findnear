import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/elements/CircularAvatarWidget.dart';
import 'package:findnear/src/models/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class UserReactItem extends StatelessWidget{
  User user;
  UserReactItem({this.user});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        _buildAvatar(),
        SizedBox(width: 12),
        Expanded(
          child: Text(
            user.name,
            overflow: TextOverflow.fade,
            softWrap: false,
            style: TextStyle(
              fontFamily: "Quicksand",
              fontSize: 14,
              color: AppColors.black,
              fontWeight: FontWeight.bold,
            )
          ),
        ),
      ],
    );
  }

  Widget _buildAvatar(){
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(20)),
      child: CircularAvatarWidget(
        user: user,
        size: 42.w,
        hasBorder: true,
        hasLiveIndicator: user.isLiveStreaming,
      ),
    );
  }

}