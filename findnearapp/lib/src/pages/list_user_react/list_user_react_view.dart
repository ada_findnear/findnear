import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/models/user.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'item/user_react_item.dart';
import 'list_user_react_logic.dart';
import 'list_user_react_state.dart';

class ListUserReactPage extends StatefulWidget {
  static const String ROUTE_NAME = '/ListUserReactPage';
  final RouteArgument routeArgument;

  ListUserReactPage({this.routeArgument});

  @override
  State<ListUserReactPage> createState() => _ListUserReactPageState();
}

class _ListUserReactPageState extends BaseWidgetState<ListUserReactPage, ListUserReactLogic, ListUserReactState> {

  List<User> get likers => widget.routeArgument.param;

  @override
  void initState() {
    super.initState();
  }

  @override
  ListUserReactLogic createController() => ListUserReactLogic();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.white,
        title: Text(
          S.of(Get.context).list_user_react,
          style: TextStyle(
            fontFamily: 'Quicksand',
            fontSize: 16,
            color: AppColors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
        child: ListView.separated(
          scrollDirection: Axis.vertical,
          itemCount: likers.length,
          separatorBuilder: (BuildContext context, int index) =>
              SizedBox(height: 10),
          itemBuilder: (_, int index) => UserReactItem(
            user: likers[index],
          ),
        ),
      ),
    );
  }

}
