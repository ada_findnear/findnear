import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/base/base_state.dart';
import 'package:get/get.dart';

import 'list_user_react_state.dart';

class ListUserReactLogic extends BaseController<ListUserReactState> {

  @override
  ListUserReactState createState() => ListUserReactState();
}
