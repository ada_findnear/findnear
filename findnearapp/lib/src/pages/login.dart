import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/pages/contact_us.dart';
import 'package:findnear/src/pages/forgot_password/forgot_password_screen.dart';
import 'package:findnear/src/pages/sign_up/signup.dart';
import 'package:findnear/src/settings/global_variable.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:velocity_x/velocity_x.dart';

import '../../generated/l10n.dart';
import '../controllers/user_controller.dart';
import '../helpers/helper.dart';
import '../repository/user_repository.dart' as userRepo;

class LoginWidget extends StatefulWidget {
  static const String ROUTE_NAME = '/Login';

  @override
  _LoginWidgetState createState() => _LoginWidgetState();
}

class _LoginWidgetState extends StateMVC<LoginWidget> {
  UserController _con;

  _LoginWidgetState() : super(UserController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    if (userRepo.currentUser.value?.apiToken != null) {
      navigator.pushReplacementNamed('/Pages', arguments: 2);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: Helper.of(context).onWillPop,
      child: Scaffold(
          key: _con.scaffoldKey,
          resizeToAvoidBottomInset: false,
          body: Stack(
            children: [
              Image.asset(
                AppImages.icLoginBg,
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                fit: BoxFit.cover,
              ),
              VxBox(child: _itemNewLogin())
                  .padding(EdgeInsets.only(
                      top: Get.mediaQuery.viewPadding.top,
                      bottom: Get.mediaQuery.viewPadding.bottom))
                  // .color(AppColors.green)
                  .height(Get.height)
                  .padding(EdgeInsets.only(
                      bottom: Get.context.mediaQueryViewInsets.bottom))
                  .make()
                  .onTap(() => FocusScope.of(context).unfocus()),
              Positioned(
                top: 60,
                child: GestureDetector(
                  onTap: (){
                    NavigatorUtils.navigateToPagesWidget();
                  },
                  child: Image.asset(
                    AppImages.icBackLight,
                    width: 40,
                    height: 40,
                    color: AppColors.white,
                  ),
                ),
              ),
            ],
          )),
      // body: _itemOldLogin()),
    );
  }

  Widget _itemNewLogin() => VStack([
        _itemTopLogin(),
        _itemBodyLogin(),
      ]).scrollVertical(physics: BouncingScrollPhysics());

  Widget _itemTopLogin() => ZStack([
        AppImage(
          AppImages.icMarkLogin,
          fit: BoxFit.cover,
          width: Get.width,
        ),
        Align(
            child: AppImage(
              AppImages.icLogin,
              width: Get.width * 0.35,
            ),
            alignment: Alignment.center),
        S
            .of(Get.context)
            .login_acc
            .toUpperCase()
            .text
            .color(Vx.white)
            .size(18)
            .bold
            .make()
            .box
            .alignBottomCenter
            .make()
            .marginOnly(bottom: 8)
      ]).box.height(Get.height * 0.35).make();

  Widget _itemBodyLogin() => VxBox(
              child: VStack([
        16.heightBox,
        Form(
            key: _con.loginFormKey,
            child: VStack([
              _itemInput(
                  title: S.of(Get.context).login_code,
                  icon: AppImages.icLoginUser,
                  validator: (input) => input.length < 6
                      ? S.of(context).should_be_more_than_6_letters
                      : null,
                  onChange: (input) => _con.user.code = input),
              16.heightBox,
              _itemInput(
                  title: S.of(Get.context).password,
                  icon: AppImages.icLoginPass,
                  hasPassword: true,
                  onChange: (input) => _con.user.password = input,
                  validator: (input) => input.length < 3
                      ? S.of(context).should_be_more_than_3_characters
                      : null),
            ])),
        16.heightBox,
        _itemButtonLogin(),
        _itemForgotPass(),
        _itemNoAccount(),
        _itemSkipLogin(),
        // _itemPolicy(),
      ]))
          .withDecoration(BoxDecoration(
              color: Vx.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(24), topRight: Radius.circular(24))))
          .width(Get.width)
          .height(Get.height * 0.65)
          .padding(
              EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom))
          .make();

  Widget _itemInput(
          {String title,
          String icon,
          bool hasPassword = false,
          Function(String) validator,
          Function(String) onChange}) =>
      VStack([
        title.text
            .color(AppColors.textBlack)
            .size(14)
            .bold
            .make()
            .paddingAll(4),
        HStack([
          TextFormField(
            onSaved: onChange,
            scrollPhysics: BouncingScrollPhysics(),
            validator: validator,
            initialValue:
                hasPassword ? '' : Get.find<LocalDataManager>().getLoginId,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.all(11),
                prefixIcon: Image.asset(icon,
                    width: 25, height: 25, color: AppColors.gray),
                suffixIcon: hasPassword
                    ? AppImage(
                        _con.hidePassword
                            ? AppImages.icEnViewer
                            : AppImages.icViewer,
                        height: 15,
                        width: 25,
                        fit: BoxFit.fill,
                      ).marginOnly(right: 11).onTap(() {
                        setState(() {
                          _con.hidePassword = !_con.hidePassword;
                        });
                      })
                    : 0.widthBox,
                suffixIconConstraints:
                    BoxConstraints(maxWidth: 50, maxHeight: 15),
                hintText: hasPassword ? '••••••••••••' : '12345678',
                isDense: true,
                border: OutlineInputBorder(
                    borderSide: BorderSide(color: AppColors.gray, width: 0.5)),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: AppColors.gray, width: 0.5)),
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: AppColors.gray, width: 0.5)),
                hintStyle: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 17,
                    color: AppColors.grayLight)),
            obscureText: hasPassword ? _con.hidePassword : false,
            keyboardType:
                hasPassword ? TextInputType.text : TextInputType.number,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
            onChanged: onChange,
          ).expand(),
        ]).paddingAll(4)
      ]).px32();

  Widget _itemButtonLogin() => VxBox(
          child: S
              .of(Get.context)
              .login
              .text
              .size(16)
              .bold
              .color(AppColors.white)
              .make())
      .roundedLg
      .alignCenter
      .color(AppColors.red)
      .width(Get.width)
      .padding(EdgeInsets.symmetric(vertical: 10))
      .margin(EdgeInsets.symmetric(horizontal: 32))
      .make()
      .onTap(() => _con.login());

  Widget _itemForgotPass() => S
      .of(Get.context)
      .i_forgot_password
      .text
      .bold
      .underline
      .size(15)
      .color(AppColors.red)
      .make()
      .box
      .alignCenter
      .make()
      .onTap(() => Get.toNamed(ForgotPasswordScreen.ROUTE_NAME))
      .p(16);

  Widget _itemNoAccount() => VxRichText(S.of(context).no_account)
      .withTextSpanChildren([
        VelocityXTextSpan(S.of(context).register)
            .bold
            .textStyle(TextStyle(fontSize: 15, color: AppColors.red))
            .underline
            .make()
      ])
      .textStyle(TextStyle(
          fontSize: 15,
          color: AppColors.textBlack,
          fontWeight: FontWeight.bold))
      .make()
      .box
      .alignBottomCenter
      .make()
      .onTap(() => Navigator.of(context).pushNamed(SignUpWidget.ROUTE_NAME))
      .expand();

  Widget _itemSkipLogin() => VxBox(
          child: S
              .of(context)
              .skip_login
              .text
              .bold
              .color(AppColors.textBlack)
              .size(14)
              .make())
      .margin(EdgeInsets.symmetric(vertical: 16))
      .border(color: AppColors.red)
      .color(AppColors.red200)
      .roundedLg
      .padding(EdgeInsets.symmetric(vertical: 8, horizontal: 20))
      .makeCentered()
      .onTap(() => NavigatorUtils.navigateToPagesWidget());

  Widget _itemPolicy() => Padding(
        padding: const EdgeInsets.only(left: 16, top: 16, right: 16),
        child: RichText(
          text: TextSpan(
            style: Theme.of(context)
                .textTheme
                .bodyText1
                .copyWith(color: AppColors.textBlack),
            text: S.of(context).termsAgreement1,
            children: [
              TextSpan(
                text: S.of(context).termsAndConditions,
                style: Theme.of(context)
                    .textTheme
                    .bodyText1
                    .copyWith(color: Colors.blue),
                recognizer: TapGestureRecognizer()
                  ..onTap = () async {
                    final privacyPolicyUrl =
                        "${GlobalConfiguration().getValue(privacyPolicy)}";
                    if (await canLaunch(privacyPolicyUrl)) {
                      await launch(privacyPolicyUrl);
                    }
                  },
              ),
              TextSpan(text: S.of(context).termsAgreement2),
            ],
          ),
        ),
      );

//   Widget _itemOldLogin() {
//     return Stack(
//       alignment: AlignmentDirectional.topCenter,
//       children: <Widget>[
//         Positioned(
//           top: 0,
//           child: Container(
//             width: config.App(context).appWidth(100),
//             height: config.App(context).appHeight(37),
//             decoration: BoxDecoration(color: Theme.of(context).accentColor),
//           ),
//         ),
//         Positioned(
//           top: config.App(context).appHeight(37) - 120,
//           child: Container(
//             width: config.App(context).appWidth(84),
//             height: config.App(context).appHeight(37),
//             child: Text(
//               S.of(context).lets_start_with_login,
//               style: Theme.of(context)
//                   .textTheme
//                   .headline2
//                   .merge(TextStyle(color: Theme.of(context).primaryColor)),
//             ),
//           ),
//         ),
//         Positioned(
//           top: config.App(context).appHeight(37) - 50,
//           child: Container(
//             decoration: BoxDecoration(
//                 color: Theme.of(context).primaryColor,
//                 borderRadius: BorderRadius.all(Radius.circular(10)),
//                 boxShadow: [
//                   BoxShadow(
//                     blurRadius: 50,
//                     color: Theme.of(context).hintColor.withOpacity(0.2),
//                   )
//                 ]),
//             margin: EdgeInsets.symmetric(
//               horizontal: 20,
//             ),
//             padding: EdgeInsets.only(top: 50, right: 27, left: 27, bottom: 20),
//             width: config.App(context).appWidth(88),
// //              height: config.App(context).appHeight(55),
//             child: Form(
//               key: _con.loginFormKey,
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.stretch,
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: <Widget>[
//                   TextFormField(
//                     keyboardType: TextInputType.number,
//                     onSaved: (input) => _con.user.code = input,
//                     validator: (input) => input.length < 6
//                         ? S.of(context).should_be_more_than_6_letters
//                         : null,
//                     decoration: InputDecoration(
//                       labelText: S.of(context).code,
//                       labelStyle:
//                           TextStyle(color: Theme.of(context).accentColor),
//                       contentPadding: EdgeInsets.all(12),
//                       hintText: '12345678',
//                       hintStyle: TextStyle(
//                           color: Theme.of(context).focusColor.withOpacity(0.7)),
//                       prefixIcon: Icon(Icons.confirmation_number_outlined,
//                           color: Theme.of(context).accentColor),
//                       border: OutlineInputBorder(
//                           borderSide: BorderSide(
//                               color: Theme.of(context)
//                                   .focusColor
//                                   .withOpacity(0.2))),
//                       focusedBorder: OutlineInputBorder(
//                           borderSide: BorderSide(
//                               color: Theme.of(context)
//                                   .focusColor
//                                   .withOpacity(0.5))),
//                       enabledBorder: OutlineInputBorder(
//                           borderSide: BorderSide(
//                               color: Theme.of(context)
//                                   .focusColor
//                                   .withOpacity(0.2))),
//                     ),
//                   ),
//                   SizedBox(height: 30),
//                   TextFormField(
//                     keyboardType: TextInputType.text,
//                     onSaved: (input) => _con.user.password = input,
//                     validator: (input) => input.length < 3
//                         ? S.of(context).should_be_more_than_3_characters
//                         : null,
//                     obscureText: _con.hidePassword,
//                     decoration: InputDecoration(
//                       labelText: S.of(context).password,
//                       labelStyle:
//                           TextStyle(color: Theme.of(context).accentColor),
//                       contentPadding: EdgeInsets.all(12),
//                       hintText: '••••••••••••',
//                       hintStyle: TextStyle(
//                           color: Theme.of(context).focusColor.withOpacity(0.7)),
//                       prefixIcon: Icon(Icons.lock_outline,
//                           color: Theme.of(context).accentColor),
//                       suffixIcon: IconButton(
//                         onPressed: () {
//                           setState(() {
//                             _con.hidePassword = !_con.hidePassword;
//                           });
//                         },
//                         color: Theme.of(context).focusColor,
//                         icon: Icon(_con.hidePassword
//                             ? Icons.visibility_outlined
//                             : Icons.visibility_off_outlined),
//                       ),
//                       border: OutlineInputBorder(
//                           borderSide: BorderSide(
//                               color: Theme.of(context)
//                                   .focusColor
//                                   .withOpacity(0.2))),
//                       focusedBorder: OutlineInputBorder(
//                           borderSide: BorderSide(
//                               color: Theme.of(context)
//                                   .focusColor
//                                   .withOpacity(0.5))),
//                       enabledBorder: OutlineInputBorder(
//                           borderSide: BorderSide(
//                               color: Theme.of(context)
//                                   .focusColor
//                                   .withOpacity(0.2))),
//                     ),
//                   ),
//                   SizedBox(height: 30),
//                   BlockButtonWidget(
//                     text: Text(
//                       S.of(context).login,
//                       style: TextStyle(color: Theme.of(context).primaryColor),
//                     ),
//                     color: Theme.of(context).accentColor,
//                     onPressed: () {
//                       _con.login();
//                     },
//                   ),
//                   SizedBox(height: 15),
//                   MaterialButton(
//                     elevation: 0,
//                     onPressed: () {
//                       Navigator.of(context)
//                           .pushReplacementNamed('/Pages', arguments: 2);
//                     },
//                     shape: StadiumBorder(),
//                     textColor: Theme.of(context).hintColor,
//                     child: Text(S.of(context).skip),
//                     padding: EdgeInsets.symmetric(horizontal: 30, vertical: 14),
//                   ),
// //                      SizedBox(height: 10),
//                 ],
//               ),
//             ),
//           ),
//         ),
//         Positioned(
//           bottom: 10,
//           child: Column(
//             children: <Widget>[
//               MaterialButton(
//                 elevation: 0,
//                 onPressed: () {
//                   Navigator.of(context).pushReplacementNamed('/ForgetPassword');
//                 },
//                 textColor: Theme.of(context).hintColor,
//                 child: Text(S.of(context).i_forgot_password),
//               ),
//               MaterialButton(
//                 elevation: 0,
//                 onPressed: () {
//                   Navigator.of(context).pushReplacementNamed('/SignUp');
//                 },
//                 textColor: Theme.of(context).hintColor,
//                 child: Text(S.of(context).i_dont_have_an_account),
//               ),
//             ],
//           ),
//         )
//       ],
//     );
//   }
}
