import 'dart:async';

import 'package:findnear/src/helpers/maps_util.dart';
import 'package:findnear/src/models/address.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/market.dart';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';

class TabMapState {
  Rxn<Market> currentMarket = Rxn<Market>();

  final loadTopMarkets = LoadStatus.initial.obs;
  final topMarkets = <Market>[].obs;
  final allMarkers = <Marker>[].obs;
  final isMapLoading = true.obs;
  final RxBool isDatingLoadingFirstTime = false.obs;
  final RxBool isJobLoadingFirstTime = false.obs;
  final RxBool isShowMarkerSearch = false.obs;

  Rxn<Address> currentAddress = Rxn<Address>();

  Rxn<CameraPosition> cameraPosition = Rxn<CameraPosition>();
  Rxn<LatLngBounds> latLngBounds = Rxn<LatLngBounds>();

  MapsUtil mapsUtil = new MapsUtil();

  Completer<GoogleMapController> mapController;
  GlobalKey<ScaffoldState> scaffoldKey;
  var bottomSheetController;

  PolylinePoints polylinePoints;
  List<LatLng> polylineCoordinates = [];

  RxMap<PolylineId, Polyline> polyLines = <PolylineId, Polyline>{}.obs;
  Rxn<Prediction> searchPosition = Rxn<Prediction>();

  ///Save last search info: myLocation, area, radius
  Address lastSearchingArea;
  Address lastSearchingMyPosition;
  double lastSearchingRadius = 0;

  TabMapState() {
    ///Initialize variables
  }
}
