import 'package:findnear/src/commons/app_configs.dart';
import 'package:findnear/src/elements/CardUserBottomSheetWidget.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/address.dart';
import 'package:findnear/src/models/entities/place_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/market.dart';
import 'package:findnear/src/repository/refactored_market_repository.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/services/setting_service.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';
import '../../repository/market_repository.dart';
import '../../repository/settings_repository.dart' as sett;
import '../../repository/settings_repository.dart' as settingRepo;

import 'tab_map_state.dart';

class TabMapLogic extends GetxController {
  final state = TabMapState();
  final settingService = Get.find<SettingService>();
  final marketRepository = Get.find<RefactoredMarketRepository>();

  TabMapLogic() {
    settingService.displayMapInCareerRx.listen((value) async {
      if (state.topMarkets.isNotEmpty) {
        state.allMarkers.clear();
        state.topMarkets.forEach((element) {
          Helper.getMarker(
              element, onPressed: () {
                onClickMarker(element);
              },
              displayCareer: settingService.displayMapInCareer,
              isDatingMarker: settingService.mapDisplayDating.value,
          )
              .then((marker) {
            marker.forEach((element) {
              state.allMarkers.add(element);
            });
          });
        });
      }
    });
  }

  @override
  void onInit() {
    Future.delayed(Duration(milliseconds: 800)).then((value) {
      state.isMapLoading.value = false;
    });
    super.onInit();
  }

  void onClickMarker(Market market) {
    state.bottomSheetController =
        state.scaffoldKey.currentState.showBottomSheet(
      (context) => CardUserBottomSheetWidget(scaffoldKey: null, market: market),
      shape: RoundedRectangleBorder(
        borderRadius: new BorderRadius.only(
            topLeft: Radius.circular(10), topRight: Radius.circular(10)),
      ),
    );
  }

  void getCurrentLocation() async {
    try {
      state.currentAddress.value = sett.deliveryAddress.value;
      if (state.currentAddress.value.isUnknown()) {
        state.cameraPosition.value = CameraPosition(
          target: LatLng(21.029832, 105.852605),
          zoom: AppConfigs.defaultMapZoom,
        );
      } else {
        state.cameraPosition.value = CameraPosition(
          target: LatLng(state.currentAddress.value.latitude,
              state.currentAddress.value.longitude),
          zoom: AppConfigs.defaultMapZoom,
        );
      }
      if (!state.currentAddress.value.isUnknown()) {
        Helper.getMyPositionMarker(state.currentAddress.value.latitude,
                state.currentAddress.value.longitude)
            .then((marker) {
          state.allMarkers.add(marker);
        });
      }
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        print('Permission denied');
      }
    }
  }

  void getMarketLocation() async {
    try {
      state.currentAddress.value = await sett.getCurrentLocation();
      state.cameraPosition.value = CameraPosition(
        target: LatLng(double.parse(state.currentMarket.value.latitude),
            double.parse(state.currentMarket.value.longitude)),
        zoom: AppConfigs.defaultMapZoom,
      );
      Helper.getMyPositionMarker(state.currentAddress.value.latitude,
              state.currentAddress.value.longitude)
          .then((marker) {
        state.allMarkers.add(marker);
      });
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        print('Permission denied');
      }
    }
  }

  Future<void> goCurrentLocation() async {
    final GoogleMapController controller = await state.mapController.future;

    sett.setCurrentLocation().then((_currentAddress) {
      sett.deliveryAddress.value = _currentAddress;
      state.currentAddress.value = _currentAddress;
      controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(_currentAddress.latitude, _currentAddress.longitude),
        zoom: AppConfigs.defaultMapZoom,
      )));
    });
  }

  Future<Null> displayPrediction(Prediction p) async {
    final GoogleMapController controller = await state.mapController.future;

    if (p != null) {
      state.searchPosition.value = p;
      PlacesDetailsResponse detail = await GoogleMapsPlaces(
              apiKey: settingRepo.setting.value.googleMapsKey)
          .getDetailsByPlaceId(p.placeId);

      double lat = detail.result.geometry.location.lat;
      double lng = detail.result.geometry.location.lng;

      controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(lat, lng),
        zoom: 14.4746,
      )));
    }
  }

  void startLoadingFindLove() {
    state.isDatingLoadingFirstTime.value = true;
    getMarketOfAreaV2();
  }

  void startLoadingFindJob() {
    state.isJobLoadingFirstTime.value = true;
    getMarketOfAreaV2();
  }

  void enableMapDating(bool value) async {
    settingService.isDatingOnMapActive.value = value;
    Market market = currentUser.value?.market;
    market.isOpenForDating = value;
    try {
      await marketRepository.updateIsOpenForDating(value);
      getUser(int.parse(currentUser.value?.id ?? "0"));
    } catch (e) {
      prettyLog.e(e);
      settingService.isDatingOnMapActive.value = !value;
    }
  }

  void getMarketOfAreaV2() async {
    if (state.latLngBounds.value != null) {
      state.loadTopMarkets.value = LoadStatus.loading;

      final currentMapDisplayDating = settingService.mapDisplayDating.value;
      final currentLatLngBounds = state.latLngBounds.value;

      var markets;
      if (currentMapDisplayDating) {
        markets =
            await marketRepository.getDatingListNearby(currentLatLngBounds);
        state.topMarkets.value = markets;
      } else {
        markets = await marketRepository.getNearMarketsV2(currentLatLngBounds);
        state.topMarkets.value = markets;
      }

      if (markets.length > 0) {
        final numberMarkerShowEachTime = 5;
        final List<List<Marker>> markerResult = [];
        var countGetMarkerFinished = 0;

        void handleGetMarker() {
          // skip updating old markers if mapDisplayDating or latLngBounds has changed
          if (currentMapDisplayDating !=
                  settingService.mapDisplayDating.value ||
              currentLatLngBounds != state.latLngBounds.value) {
            return;
          }

          if (countGetMarkerFinished % numberMarkerShowEachTime == 0 ||
              countGetMarkerFinished == markets.length) {
            final markers =
                markerResult.reduce((value, element) => [...value, ...element]);
            state.allMarkers.value = markers;
          }
        }

        markets.forEach((element) {
          Helper.getMarker(
            element,
            onPressed: () {
              onClickMarker(element);
            },
            displayCareer: settingService.displayMapInCareer,
            isDatingMarker: currentMapDisplayDating == true && (currentUser.value?.market?.isOpenForDating ?? false) == true,
          ).then((marker) {
            markerResult.add(marker);
          }).whenComplete(() {
            countGetMarkerFinished += 1;
            handleGetMarker();
          });
        });
      } else {
        state.allMarkers.value = <Marker>[];
      }

      state.loadTopMarkets.value = LoadStatus.success;
      state.isDatingLoadingFirstTime.value = false;
      state.isJobLoadingFirstTime.value = false;
    }
  }

  void getDirectionSteps() async {
    state.polylinePoints = PolylinePoints();

    var _currentAddress = await sett.setCurrentLocation();
    sett.deliveryAddress.value = _currentAddress;
    state.currentAddress.value = _currentAddress;

    PolylineResult result =
        await state.polylinePoints.getRouteBetweenCoordinates(
      settingRepo.setting.value.googleMapsKey,
      PointLatLng(_currentAddress.latitude, _currentAddress.longitude),
      PointLatLng(double.parse(state.currentMarket.value.latitude),
          double.parse(state.currentMarket.value.longitude)),
    );

    state.polylineCoordinates = [];
    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        state.polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }

    PolylineId id = PolylineId('poly');

    Polyline polyline = Polyline(
      polylineId: id,
      color: Colors.blueAccent,
      points: state.polylineCoordinates,
      width: 5,
    );
    state.polyLines[id] = polyline;
  }

  void addMarkerFromSearch(PlaceEntity entity){
    final List<Marker> markerResult = [];
    var marker =  Marker(
      // This marker id can be anything that uniquely identifies each marker.
      markerId: MarkerId(entity.formattedAddress),
      position: LatLng(entity.geometry.location.lat, entity.geometry.location.lng),
      infoWindow: InfoWindow(
        title: entity.formattedAddress,
      ),

      icon: BitmapDescriptor.defaultMarker,
    );
    markerResult.add(marker);
    state.allMarkers.value = markerResult;
  }
}
