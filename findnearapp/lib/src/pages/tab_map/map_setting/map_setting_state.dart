import 'package:findnear/src/repository/user_repository.dart';
import 'package:get/get.dart';

class MapSettingState {
  RxBool mapVisible;
  RxBool positionFixed;
  RxBool isDatingOnMapActive;
  RxString address;

  MapSettingState() {
    mapVisible = currentUser.value.market.visible.obs;
    positionFixed = currentUser.value.market.fixed_position.obs;
    isDatingOnMapActive = currentUser.value.market.isOpenForDating.obs;
    address = currentUser.value.market.address.obs;
  }
}

