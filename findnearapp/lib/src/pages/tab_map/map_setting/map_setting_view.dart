import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'map_setting_logic.dart';
import 'map_setting_state.dart';

class MapSettingPage extends StatefulWidget {
  @override
  _MapSettingPageState createState() => _MapSettingPageState();
}

class _MapSettingPageState extends State<MapSettingPage> {
  final MapSettingLogic logic = Get.put(MapSettingLogic());
  final MapSettingState state = Get.find<MapSettingLogic>().state;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
        boxShadow: [BoxShadow(color: Colors.black.withOpacity(0.16), blurRadius: 6)],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 8),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(width: 15),
                Expanded(child: Text(S.of(context).settings, style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 18, fontWeight: FontWeight.bold))),
                IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: Image.asset(AppImages.icClose),
                ),
              ],
            ),
          ),
          Obx(() {
            return ListTile(
              contentPadding: EdgeInsets.only(left: 15, right: 0, top: 4, bottom: 4),
              onTap: () {
                if (state.positionFixed.value) {
                  logic.togglePositionFixed(false);
                } else {
                  logic.togglePositionFixed(true);
                }
              },
              dense: true,
              leading: Theme.of(context).brightness == Brightness.dark
                  ? Image.asset(
                AppImages.icGpsDark,
                width: 20,
                fit: BoxFit.contain,
              )
                  : Image.asset(
                AppImages.icGpsLight,
                width: 20,
                fit: BoxFit.contain,
              ),
              minLeadingWidth: 10,
              title: Text(
                S.of(context).positionFixed,
                style: Theme.of(context).textTheme.bodyText2.copyWith(fontSize: 16),
              ),
              subtitle: state.address?.value != null
                  ? Text(
                state.address?.value ?? "",
                style: Theme.of(context).textTheme.bodyText2.copyWith(fontSize: 12),
              )
                  : null,
              trailing: Switch(
                onChanged: (value) {
                  logic.togglePositionFixed(value);
                },
                value: state.positionFixed.value,
              ),
            );
          }),
          Divider(indent: 15, endIndent: 15),
          Obx(() {
            return ListTile(
              contentPadding: EdgeInsets.only(left: 15, right: 0, top: 4, bottom: 4),
              onTap: () {
                if (state.mapVisible.value) {
                  logic.toggleMapVisible(false);
                } else {
                  logic.toggleMapVisible(true);
                }
              },
              dense: true,
              minLeadingWidth: 10,
              leading: Theme.of(context).brightness == Brightness.dark
                  ? Image.asset(
                AppImages.icVisibleDark,
                width: 20,
                fit: BoxFit.contain,
              )
                  : Image.asset(
                AppImages.icVisibleLight,
                width: 20,
                fit: BoxFit.contain,
              ),
              title: Text(
                S.of(context).visibleOnMap,
                style: Theme.of(context).textTheme.bodyText2.copyWith(fontSize: 16),
              ),
              // subtitle: Text(
              //   state.mapVisible.value ? "Bật" : "Tắt",
              //   style: Theme.of(context).textTheme.bodyText2.copyWith(fontSize: 12),
              // ),
              trailing: Switch(
                onChanged: (value) {
                  logic.toggleMapVisible(value);
                },
                value: state.mapVisible.value,
              ),
            );

          }),
          Divider(indent: 15, endIndent: 15),
          Obx(() {
            return ListTile(
              contentPadding: EdgeInsets.only(left: 15, right: 0, top: 4, bottom: 4),
              onTap: () {

                if (state.isDatingOnMapActive.value) {
                  logic.toggleIsOpenForDating(false);
                } else {
                  logic.toggleIsOpenForDating(true);
                }
              },
              dense: true,
              minLeadingWidth: 10,
              leading: AppImage(AppImages.icDate, width: 20),
              title: Text(
                S.of(context).showDatingModeOnMap,
                style: Theme.of(context).textTheme.bodyText2.copyWith(fontSize: 16),
              ),
              trailing: Switch(
                onChanged: (value) {
                  logic.toggleIsOpenForDating(value);
                },
                value: state.isDatingOnMapActive.value,
              ),
            );
          }),
          SizedBox(height: Get.bottomBarHeight),
        ],
      ),
    );
  }

  @override
  void dispose() {
    Get.delete<MapSettingLogic>();
    super.dispose();
  }
}
