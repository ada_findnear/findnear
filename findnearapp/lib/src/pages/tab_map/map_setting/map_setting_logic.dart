import 'package:findnear/main.dart';
import 'package:findnear/src/helpers/maps_util.dart';
import 'package:findnear/src/models/market.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/pages/position_picker.dart';
import 'package:findnear/src/pages/tab_map/tab_map_logic.dart';
import 'package:findnear/src/pages/tab_map/tab_map_state.dart';
import 'package:findnear/src/repository/refactored_market_repository.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/services/setting_service.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../repository/settings_repository.dart' as settingRepository;

import 'map_setting_state.dart';

class MapSettingLogic extends GetxController {
  final state = MapSettingState();
  final marketRepository = Get.find<RefactoredMarketRepository>();
  MapsUtil mapsUtil = new MapsUtil();
  final settingService = Get.find<SettingService>();

  void togglePositionFixed(bool value) async {
    state.positionFixed.value = value;
    Market market = currentUser.value?.market;
    if (value) {
      CameraPosition cameraPosition = await Navigator.of(Get.context).pushNamed(PositionPickerWidget.ROUTE_NAME) as CameraPosition;
      if (cameraPosition != null) {
        String addressResult = await mapsUtil.getAddressName(LatLng(cameraPosition.target.latitude, cameraPosition.target.longitude), settingRepository.setting.value.googleMapsKey);

        market.latitude = cameraPosition.target.latitude.toString();
        market.longitude = cameraPosition.target.longitude.toString();
        market.fixed_position = true;
        market.address = addressResult;
        updateMarket(market);
        state.address.value = addressResult;
      } else {
        state.positionFixed.value = false;
      }
    } else {
      market.fixed_position = false;
      updateMarket(market);
    }
  }

  void updateFixedAddress() async {
    CameraPosition cameraPosition = await Navigator.of(Get.context).pushNamed(PositionPickerWidget.ROUTE_NAME, arguments: RouteArgument(param: LatLng(double.parse(currentUser.value.market?.latitude), double.parse(currentUser.value.market?.longitude)))) as CameraPosition;
    if (cameraPosition != null) {
      String addressResult = await mapsUtil.getAddressName(LatLng(cameraPosition.target.latitude, cameraPosition.target.longitude), settingRepository.setting.value.googleMapsKey);

      Market market = currentUser.value?.market;
      market.latitude = cameraPosition.target.latitude.toString();
      market.longitude = cameraPosition.target.longitude.toString();
      market.fixed_position = true;
      market.address = addressResult;

      updateMarket(market);
      state.address.value = addressResult;
    }
  }

  void updateMarket(Market market) async {
    marketRepository.updateMarkets(market).then((value) {
      getUser(int.parse(currentUser.value?.id ?? "0"));
      //Todo
      // ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
      //   content: Text(S.of(Get.context).profile_settings_updated_successfully),
      // ));
    });
  }

  void toggleMapVisible(bool value) {
    Market market = currentUser.value?.market;
    market.visible = value;
    Get.find<LocalDataManager>().setIsVisibleOnMap(value);
    updateMarket(market);
    state.mapVisible.value = value;
  }

  void toggleIsOpenForDating(bool value) async {
    state.isDatingOnMapActive.value = value;
    settingService.isDatingOnMapActive.value = value;

    Market market = currentUser.value?.market;
    market.isOpenForDating = value;
    try {
      await marketRepository.updateIsOpenForDating(value);
      getUser(int.parse(currentUser.value?.id ?? "0"));
    } catch (e) {
      prettyLog.e(e);
      state.isDatingOnMapActive.value = !value;
      settingService.isDatingOnMapActive.value = !value;
    }
  }
}

