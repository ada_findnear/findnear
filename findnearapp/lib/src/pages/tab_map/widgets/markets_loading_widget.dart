import 'package:findnear/src/widgets/shimmer/app_shimmer.dart';
import 'package:flutter/material.dart';

class MarketsLoadingWidget extends StatelessWidget {
  MarketsLoadingWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 55,
      child: ListView.separated(
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.symmetric(horizontal: 12),
        physics: NeverScrollableScrollPhysics(),
        itemCount: 5,
        itemBuilder: (context, index) {
          return MarketLoadingWidget();
        },
        separatorBuilder: (context, index) {
          return SizedBox(width: 8);
        },
      ),
    );
  }
}

class MarketLoadingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 210,
      height: double.infinity,
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          BoxShadow(color: Theme.of(context).focusColor.withOpacity(0.1), blurRadius: 15, offset: Offset(0, 5)),
        ],
      ),
      child: Row(
        children: [
          SizedBox(width: 4),
          AppShimmer(width: 45, height: 45, cornerRadius: 22.5),
          SizedBox(width: 7),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              AppShimmer(width: 116, height: 12, cornerRadius: 12 / 2),
              SizedBox(height: 4),
              Row(
                children: [
                  AppShimmer(width: 67, height: 12, cornerRadius: 12 / 2),
                  SizedBox(width: 7),
                  AppShimmer(width: 74, height: 18, cornerRadius: 18 / 2),
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
