import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/commons/app_shadows.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../../main.dart';

class SearchBarWidget extends StatelessWidget {
  final String keyword;
  final VoidCallback onPressed;
  final VoidCallback onClear;
  final VoidCallback onVoiceSearch;

  const SearchBarWidget({
    Key key,
    this.keyword,
    this.onPressed,
    this.onClear,
    this.onVoiceSearch,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        height: 40,
        width: double.infinity,
        child: Row(
          children: [
            IconButton(
              padding: EdgeInsets.zero,
              onPressed: null,
              icon: Icon(
                Icons.search_rounded,
                color: Theme.of(context).iconTheme.color,
                size: 20,
              ),
            ),
            Expanded(
              child: Text(
                (keyword ?? "").isNotEmpty ? keyword : S.of(context).search,
                style: TextStyle(fontSize: 15.0, color: (keyword ?? "").isNotEmpty ? Theme.of(context).hintColor : Theme.of(context).focusColor),
              ),
            ),
            Visibility(
              visible: (keyword ?? "").isNotEmpty,
              child: IconButton(
                padding: EdgeInsets.zero,
                onPressed: onClear,
                icon: Icon(
                  Icons.close_rounded,
                  color: Theme.of(context).iconTheme.color,
                  size: 20,
                ),
              ),
            ),
            if (onVoiceSearch != null)
              GestureDetector(
                onTap: onVoiceSearch,
                child: Container(
                  width: 40,
                  height: 40,
                  padding: EdgeInsets.all(4),
                  child: Image.asset(AppImages.icVoice, fit: BoxFit.fill),
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(20)),
                ),
              ),
          ],
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          border: Border.all(color: Color(0xffCCCCCC), width: 1),
          color: Colors.white
        ),
      ),
    );
  }
}
