import 'package:findnear/src/models/market.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/pages/profile_user/profile_user_view.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/services/setting_service.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'market_widget.dart';
import 'markets_loading_widget.dart';

class MarketsWidget extends StatelessWidget {
  final List<Market> marketsList;
  final bool loading;

  MarketsWidget({
    Key key,
    this.marketsList,
    this.loading = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return loading
        ? MarketsLoadingWidget()
        : Container(
            height: 55,
            child: ListView.separated(
                scrollDirection: Axis.horizontal,
                padding: EdgeInsets.symmetric(horizontal: 12),
                itemBuilder: (context, index) {
                  final item = marketsList[index];
                  return GestureDetector(
                    onTap: () {
                      if (!isLoggedIn) {
                        NavigatorUtils.toLogin();
                        return;
                      }
                      Get.find<SettingService>().mapDisplayDatingDirection.value = Get.find<SettingService>().mapDisplayDating.value;
                      NavigatorUtils.navigateToProfilePage(item.owner.id);
                    },
                    child: MarketWidget(market: item),
                  );
                },
                separatorBuilder: (context, index) {
                  return SizedBox(width: 8);
                },
                itemCount: marketsList.length),
          );
  }
}
