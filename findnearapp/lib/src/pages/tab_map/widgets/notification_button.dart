import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class NotificationButton extends StatelessWidget {
  final int badgeCounter;
  final VoidCallback onPressed;

  const NotificationButton({
    Key key,
    this.badgeCounter = 0,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final counter = badgeCounter > 99 ? 99 : badgeCounter;
    return Container(
      child: Stack(
        children: [
          Center(
            child: IconButton(
              icon: Theme.of(context).brightness == Brightness.dark ? Image.asset(AppImages.icNotificationDark) : AppImage(AppImages.icNotificationLightSvg),
              onPressed: onPressed,
            ),
          ),
          (counter ?? 0) > 0
              ? Positioned(
                  right: 10,
                  top: 12,
                  child: Container(
                    height: 16,
                    padding: EdgeInsets.symmetric(horizontal: 2),
                    constraints: BoxConstraints(
                      minWidth: 16,
                    ),
                    child: Center(
                      child: Text(
                        "${counter}",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 10,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    decoration: BoxDecoration(color: Colors.red, borderRadius: BorderRadius.all(Radius.circular(10))),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}
