import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/heart_clip_widget.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/market.dart';
import 'package:findnear/src/repository/settings_repository.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/services/setting_service.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import '/../../../generated/l10n.dart';

class MarketWidget extends StatelessWidget {
  final Market market;
  final settingService = Get.find<SettingService>();

  MarketWidget({Key key, this.market}) : super(key: key);

  bool get isDatingMode => settingService.mapDisplayDating.value;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 210,
      height: 55,
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          BoxShadow(
              color: Theme.of(context).focusColor.withOpacity(0.1),
              blurRadius: 15,
              offset: Offset(0, 5)),
        ],
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          SizedBox(width: 4),
          currentUser.value?.market?.isOpenForDating ?? false
              ? Container(
                  width: isDatingMode ? 33.65 : 45,
                  height: isDatingMode ? 29.189 : 45,
                  child: Stack(
                    children: [
                      isDatingMode
                          ? HeartClipWidget(
                              child: Container(
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: NetworkImage(market
                                                .owner?.image?.thumb ??
                                            "${GlobalConfiguration().getValue('base_url')}images/image_default.png"))),
                              ),
                            )
                          : CircleAvatar(
                              backgroundImage: NetworkImage(market
                                      .owner?.image?.thumb ??
                                  "${GlobalConfiguration().getValue('base_url')}images/image_default.png"),
                              radius: 45 / 2),
                      Positioned.fill(
                        child: Offstage(
                          offstage: !isDatingMode,
                          child: AppImage(market?.owner?.isActive ?? false
                              ? AppImages.icDatingOverlayGreen
                              : AppImages.icDatingOverlayRed),
                        ),
                      )
                    ],
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(45 / 2),
                    border: isDatingMode
                        ? Border()
                        : Border.all(color: Color(0xFFC83E37), width: 2),
                  ),
                )
              : Container(
                  width: 45,
                  height: 45,
                  child: CircleAvatar(
                      backgroundImage: NetworkImage(market
                              .owner?.image?.thumb ??
                          "${GlobalConfiguration().getValue('base_url')}images/image_default.png"),
                      radius: 45 / 2),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(45 / 2),
                    border: Border.all(color: Color(0xFFC83E37), width: 2),
                  ),
                ),

          SizedBox(width: 7),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  market.owner.name,
                  overflow: TextOverflow.fade,
                  softWrap: false,
                  style: Theme.of(context)
                      .textTheme
                      .caption
                      .copyWith(fontWeight: FontWeight.bold),
                ),
                // Text(Helper.skipHtml(market.owner.bio), overflow: TextOverflow.fade, softWrap: false, style: Theme.of(context).textTheme.caption, maxLines: 1),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        child: Text(
                          "ID: " + market.owner.code,
                          maxLines: 2,
                          style: Theme.of(context)
                              .textTheme
                              .caption
                              .copyWith(color: Color(0xFFD92D2D)),
                        ),
                      ),
                    ),
                    Container(
                      height: 18,
                      padding: EdgeInsets.symmetric(horizontal: 6),
                      decoration: BoxDecoration(
                          color: Color(0xFFFFD584),
                          borderRadius: BorderRadius.circular(11)),
                      child: Center(
                        child: Text(
                          Helper.getDistance(
                              market.distance,
                              Helper.of(context)
                                  .trans(text: setting.value.distanceUnit)),
                          style: Theme.of(context)
                              .textTheme
                              .overline
                              .copyWith(color: Color(0xFF333333)),
                        ),
                      ),
                    ),
                    SizedBox(width: 4),
                  ],
                ),
              ],
            ),
          ),
          // SizedBox(width: 15),
          // Expanded(
          //   child: Column(
          //     children: <Widget>[
          //       market.distance > 0
          //           ? Text(
          //               Helper.getDistance(market.distance, Helper.of(context).trans(text: setting.value.distanceUnit)),
          //               overflow: TextOverflow.fade,
          //               maxLines: 1,
          //               softWrap: false,
          //               style: TextStyle(fontSize: 10),
          //             )
          //           : SizedBox(height: 0)
          //     ],
          //   ),
          // ),
        ],
      ),
    );
  }
}
