import 'dart:async';

import 'package:findnear/src/commons/app_configs.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/commons/app_shadows.dart';
import 'package:findnear/src/models/entities/place_entity.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/market.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/pages/notification/notification_list/notification_list_view.dart';
import 'package:findnear/src/pages/search/search_view.dart';
import 'package:findnear/src/pages/tab_map/map_filter/map_filter_view.dart';
import 'package:findnear/src/pages/tab_map/widgets/markets_widget.dart';
import 'package:findnear/src/pages/voice_filter/voice_filter_view.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/services/notification_service.dart';
import 'package:findnear/src/services/setting_service.dart';
import 'package:findnear/src/services/speech_to_text_service.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '/../../generated/l10n.dart';
import 'map_setting/map_setting_view.dart';
import 'tab_map_logic.dart';
import 'tab_map_state.dart';
import 'widgets/notification_button.dart';
import 'widgets/search_bar_widget.dart';

class TabMapPage extends StatefulWidget {
  final RouteArgument routeArgument;
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  TabMapPage({Key key, this.routeArgument, this.parentScaffoldKey})
      : super(key: key);

  @override
  _TabMapPageState createState() => _TabMapPageState();
}

class _TabMapPageState extends State<TabMapPage>
    with AutomaticKeepAliveClientMixin {
  final TabMapLogic logic = Get.put(TabMapLogic());
  final TabMapState state = Get.find<TabMapLogic>().state;
  final NotificationService notificationService =
  Get.find<NotificationService>();
  final SpeechToTextService sttService = Get.find<SpeechToTextService>();

  final settingService = Get.find<SettingService>();
  final _mentionValue = "".obs;
  final _currentMessageDebounce = "".obs;

  Completer<GoogleMapController> mapController = Completer();
  GoogleMapController mGoogleMapController;

  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _endDrawerKey = new GlobalKey<MapFilterPageState>();
  var isShowingDialog = false;


  var worker;

  @override
  void initState() {
    super.initState();
    state.mapController = mapController;
    state.currentMarket.value = widget.routeArgument?.param as Market;
    state.scaffoldKey = _scaffoldKey;
    state.isDatingLoadingFirstTime.listen(onDatingLoadingFirstTimeStatusChange);
    state.isJobLoadingFirstTime.listen(onJobLoadingFirstTimeStatusChange);
    if (state.currentMarket.value?.latitude != null) {
      // user select a market
      logic.getMarketLocation();
      logic.getDirectionSteps();
    } else {
      logic.getCurrentLocation();
    }

    settingService.isDatingOnMapActive.listen((value) {
      logic.getMarketOfAreaV2();
    });

    worker = debounce(
      _mentionValue,
          (value) {
        logic.getMarketOfAreaV2();
      },
      time: Duration(milliseconds: 200),
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      key: _scaffoldKey,
      appBar: _buildAppBar(),
      body: Stack(
        alignment: AlignmentDirectional.topStart,
        children: [
          Stack(
            alignment: AlignmentDirectional.bottomStart,
            children: <Widget>[
              _buildMapWidget(),
              _buildMarket(),
              Positioned(child: _buildMenuAction(), right: 6.5, bottom: 83),
            ],
          ),
          // _buildSearchWidget(),
        ],
      ),
      endDrawer: MapFilterPage(
        key: _endDrawerKey,
        onApplyFilter: () {
          // logic.getMarketsOfArea();
          logic.getMarketOfAreaV2();
        },
      ),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0,
      centerTitle: true,
      leadingWidth: 52,
      leading: Obx(() {
        return state.currentMarket.value?.latitude == null
            ? new IconButton(
          icon: SvgPicture.asset(
            AppImages.icHamburger,
            color: Theme.of(context).accentColor,
          ),
          onPressed: () =>
              widget.parentScaffoldKey.currentState.openDrawer(),
        )
            : IconButton(
          icon: new Icon(Icons.arrow_back,
              color: Theme.of(context).hintColor),
          onPressed: () =>
              Navigator.of(context).pushNamed('/Pages', arguments: 1),
        );
      }),
      titleSpacing: 0,
      title: Obx(() {
        return SearchBarWidget(
          keyword: state.searchPosition.value?.description?.split(', ')?.first,
          onPressed: _showSearchPage,
          onClear: () {
            state.searchPosition.value = null;
          },
          onVoiceSearch:
          (sttService.isAvailable.value) ? _showVoiceFilter : null,
        );
      }),
      actions: [
        Obx(() {
          return NotificationButton(
            onPressed: _showNotification,
            badgeCounter: notificationService.unreadNotificationCounter.value,
          );
        }),
      ],
    );
  }

  Widget _buildMapWidget() {
    return Obx(() {
      Widget child;
      if (state.isMapLoading.value) {
        child = Center(child: LoadingView());
      } else {
        child = GoogleMap(
          mapToolbarEnabled: false,
          mapType: settingService.mapDisplaySatellite.value
              ? MapType.satellite
              : MapType.normal,
          buildingsEnabled: false,
          initialCameraPosition: state.cameraPosition.value,
          markers: Set.from(state.allMarkers),
          myLocationEnabled: true,
          zoomControlsEnabled: false,
          myLocationButtonEnabled: false,
          onMapCreated: (GoogleMapController controller) {
            mapController.complete(controller);
            this.mGoogleMapController = controller;
          },
          onCameraMove: (CameraPosition cameraPosition) {},
          onTap: (LatLng position) {
            if (state?.bottomSheetController != null) {
              state?.bottomSheetController?.close();
            }
          },
          onCameraIdle: () async {
            if(!state.isShowMarkerSearch.value){
              state.latLngBounds.value =
              await mGoogleMapController.getVisibleRegion();
              _mentionValue.value = state.latLngBounds.value.northeast.toString();
            }
          },
          polylines: Set<Polyline>.of(state.polyLines.values),
        );
      }
      return AnimatedSwitcher(
        duration: Duration(milliseconds: 1000),
        child: child,
      );
    });
  }

  Widget _buildMenuAction() {
    return Container(
      width: 74,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.min,
        children: [
          Obx(() {
            return (sttService.isAvailable.value)
                ? GestureDetector(
              onTap: _showFilterWithVoice,
              child: Container(
                width: 40,
                height: 40,
                child: Image.asset(AppImages.icVoice, fit: BoxFit.fill),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20)),
              ),
            )
                : Container();
          }),
          Obx(() {
            return GestureDetector(
              onTap: _toggleDatingModeMap,
              child: AnimatedSwitcher(
                duration: const Duration(milliseconds: 200),
                child: !settingService.mapDisplayDating.value
                    ? Column(
                  children: [
                    SizedBox(height: 10),
                    AppImage(AppImages.icEnableDatingModeMap),
                  ],
                )
                    : AppImage(AppImages.icEnableNormalModeMap),
              ),
            );
          }),
          Obx(() {
            return (!settingService.mapDisplayDating.value)
                ? Column(
              children: [
                SizedBox(height: 10),
                GestureDetector(
                  onTap: _showFilter,
                  child: Container(
                    width: 40,
                    height: 40,
                    child: IconButton(
                      icon: SvgPicture.asset(
                        AppImages.icFilter,
                        color: Theme.of(context).accentColor,
                      ),
                    ),
                    decoration: BoxDecoration(
                        boxShadow: AppShadow.boxShadow,
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.white),
                  ),
                ),
                SizedBox(height: 10),
              ],
            )
                : Container();
          }),
          currentUser.value?.apiToken != null
              ? GestureDetector(
            onTap: _showSetting,
            child: Container(
              width: 40,
              height: 40,
              margin: EdgeInsets.only(bottom: 10),
              child: Image.asset(AppImages.icSetting, fit: BoxFit.fill),
              decoration: BoxDecoration(
                  boxShadow: AppShadow.boxShadow,
                  borderRadius: BorderRadius.circular(20)),
            ),
          )
              : Container(),
          GestureDetector(
            onTap: logic.goCurrentLocation,
            child: Container(
              width: 40,
              height: 40,
              child: Image.asset(AppImages.icMyLocation, fit: BoxFit.fill),
              decoration: BoxDecoration(
                  boxShadow: AppShadow.boxShadow,
                  borderRadius: BorderRadius.circular(20)),
            ),
          ),
          SizedBox(height: 10),
          _buildMapTypeToggle(),
        ],
      ),
    );
  }

  Widget _buildMapTypeToggle() {
    return Obx(() {
      return GestureDetector(
        onTap: () {
          if (settingService.mapDisplaySatellite.value) {
            settingService.showMapNormal();
          } else {
            settingService.showMapSatellite();
          }
        },
        child: Container(
          width: 50,
          height: 50,
          child: Image.asset(
            settingService.mapDisplaySatellite.value
                ? AppImages.icMapNormal
                : AppImages.icMapSatellite,
            fit: BoxFit.fill,
          ),
          decoration: BoxDecoration(
            boxShadow: AppShadow.boxShadow,
            borderRadius: BorderRadius.circular(12),
            border: Border.all(color: Colors.white, width: 2),
          ),
        ),
      );
    });
  }

  Widget _buildMarket() {
    return Obx(() {
      return Container(
        margin: EdgeInsets.only(bottom: 18),
        child: MarketsWidget(
          loading: state.loadTopMarkets.value == LoadStatus.loading,
          marketsList: state.topMarkets,
        ),
      );
    });
  }

  Widget _buildSearchWidget() {
    return Obx(() {
      return Container(
        alignment: state.searchPosition == null
            ? Alignment.topRight
            : Alignment.topCenter,
        height: 60,
        decoration: BoxDecoration(
          boxShadow: state.searchPosition != null
              ? [
            BoxShadow(
                color: Theme.of(context).focusColor.withOpacity(0.3),
                blurRadius: 15,
                offset: Offset(0, 5)),
          ]
              : null,
        ),
        child: Padding(
          padding: EdgeInsets.fromLTRB(16, 16, 16, 0),
          child: GestureDetector(
            onTap: _showSearchPage,
            child: state.searchPosition == null
                ? Container(
              padding: EdgeInsets.all(10),
              child: Icon(Icons.search_outlined,
                  color: Theme.of(context).primaryColor),
              decoration: BoxDecoration(
                color: Theme.of(context).accentColor,
                borderRadius: BorderRadius.all(Radius.circular(25)),
                boxShadow: [
                  BoxShadow(
                      color:
                      Theme.of(context).focusColor.withOpacity(0.8),
                      blurRadius: 15,
                      offset: Offset(0, 5)),
                ],
              ),
            )
                : Container(
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.all(Radius.circular(25)),
                boxShadow: [
                  BoxShadow(
                      color:
                      Theme.of(context).focusColor.withOpacity(0.1),
                      blurRadius: 15,
                      offset: Offset(0, 5)),
                ],
              ),
              child: Row(
                children: [
                  SizedBox(
                    width: 10,
                  ),
                  Icon(Icons.location_on_sharp,
                      color: Theme.of(context).accentColor),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    child: Text(
                      state.searchPosition?.value != null
                          ? state.searchPosition.value?.description
                          ?.split(', ')[0]
                          : S.of(context).search,
                      style: TextStyle(
                          fontSize: 15.0,
                          color: state.searchPosition != null
                              ? Theme.of(context).hintColor
                              : Theme.of(context).focusColor),
                    ),
                  ),
                  VerticalDivider(),
                  if (state.searchPosition?.value != null)
                    IconButton(
                      icon: Icon(Icons.close,
                          color: Theme.of(context).focusColor),
                      onPressed: () {
                        state.searchPosition.value = null;
                      },
                    ),
                  SizedBox(
                    width: 10,
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }

  @override
  void dispose() {
    Get.delete<TabMapLogic>();
    worker.dispose();
    super.dispose();
  }

  @override
  bool get wantKeepAlive => true;

  ///Navigate
  void _showVoiceFilter() async {
    final result = await Get.bottomSheet(VoiceFilterPage());
    if ((result is String) && result.isNotEmpty) {
      final result2 = await Get.to(() => SearchPage(keyword: result.trim()));
      if (result2 is PlaceEntity) {
        if (result2.geometry.location != null) {
          final newCamera = CameraPosition(
            target: LatLng(
                result2.geometry.location.lat, result2.geometry.location.lng),
            zoom: AppConfigs.defaultMapZoom,
          );
          final GoogleMapController controller = await mapController.future;
          controller.animateCamera(CameraUpdate.newCameraPosition(newCamera));
        }
      }
    }
  }

  void _showSearchPage() async {
    final result = await Get.to(() => SearchPage());
    if (result is PlaceEntity) {
      state.isShowMarkerSearch.value = true;
      if (result.geometry.location != null) {
        final newCamera = CameraPosition(
          target: LatLng(
              result.geometry.location.lat, result.geometry.location.lng),
          zoom: 18,
        );
        final GoogleMapController controller = await mapController.future;
        controller.animateCamera(CameraUpdate.newCameraPosition(newCamera));
        logic.addMarkerFromSearch(result);
        Future.delayed(Duration(milliseconds: 2000), () {
          state.isShowMarkerSearch.value = false;
        });

      }
    }
  }

  void _showFilter() async {
    _scaffoldKey.currentState.openEndDrawer();
  }

  void _showFilterWithVoice() async {
    _scaffoldKey.currentState.openEndDrawer();
    await Future.delayed(Duration(milliseconds: 500));
    _endDrawerKey.currentState?.showVoiceFilter();
  }

  void _showSetting() async {
    Get.bottomSheet(MapSettingPage());
  }

  void _showNotification() {
    Get.to(() => NotificationListPage());
  }

  void _toggleDatingModeMap() async {
    if (state?.bottomSheetController != null) {
      state?.bottomSheetController?.close();
    }
    if(settingService.isAppFirstRun == true){
      logic.enableMapDating(true);
    }
    settingService.toggleDatingModeMap();
    if (settingService.mapDisplayDating.value) {
      logic.startLoadingFindLove();
    } else {
      logic.startLoadingFindJob();
    }
  }

  void onDatingLoadingFirstTimeStatusChange(bool isLoading) {
    if (!isShowingDialog && isLoading) {
      isShowingDialog = true;
      DialogHelper.showFindLoveLoadingDialog(dismissible: false);
    } else if (isShowingDialog && !isLoading) {
      isShowingDialog = false;
      Get.back();
    }
  }

  void onJobLoadingFirstTimeStatusChange(bool isLoading) {
    if (!isShowingDialog && isLoading) {
      isShowingDialog = true;
      DialogHelper.showFindJobLoadingDialog(dismissible: false);
    } else if (isShowingDialog && !isLoading) {
      isShowingDialog = false;
      Get.back();
    }
  }
}
