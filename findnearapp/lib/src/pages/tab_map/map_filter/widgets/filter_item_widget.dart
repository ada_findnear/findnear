import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class FilterItemWidget extends StatelessWidget {
  final bool isSelected;
  final String title;
  final String iconUrl;
  final ValueChanged<bool> onChanged;
  final bool showIcon;

  FilterItemWidget({
    Key key,
    this.isSelected,
    this.title,
    this.iconUrl,
    this.onChanged,
    this.showIcon = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onChanged?.call(!isSelected);
      },
      child: Container(
        height: 44,
        child: Row(
          children: [
            Container(
              width: 48,
              child: showIcon
                  ? CachedNetworkImage(
                      imageUrl: iconUrl,
                      width: 20,
                      height: 20,
                      fit: BoxFit.contain,
                      errorWidget: (context, url, error) {
                        return Container();
                      },
                      placeholder: (context, url) {
                        return Container();
                      },
                    )
                  : Container(),
            ),
            Expanded(
              child: Container(
                child: Text(
                  title,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText2
                      .copyWith(fontSize: 16, fontWeight: FontWeight.w700),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(right: 24, left: 8),
              child: Center(
                child: isSelected
                    ? Icon(
                        Icons.check_box_outlined,
                        color: Theme.of(context).accentColor,
                      )
                    : Icon(
                        Icons.check_box_outline_blank_outlined,
                        color: Theme.of(context).accentColor,
                      ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
