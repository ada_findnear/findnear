import 'package:findnear/src/widgets/shimmer/app_shimmer.dart';
import 'package:flutter/material.dart';

class LoadingFieldsWidget extends StatelessWidget {
  const LoadingFieldsWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 400,
      child: ListView.builder(
        itemBuilder: (context, index) {
          return Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 12),
            child: AppShimmer(
              height: 20,
              cornerRadius: 10,
            ),
          );
        },
        itemCount: 20,
        physics: NeverScrollableScrollPhysics(),
      ),
    );
  }
}
