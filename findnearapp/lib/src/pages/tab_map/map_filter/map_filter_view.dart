import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/commons/app_shadows.dart';
import 'package:findnear/src/elements/CircularLoadingWidget.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/filter.dart';
import 'package:findnear/src/pages/tab_map/map_filter/widgets/filter_item_widget.dart';
import 'package:findnear/src/pages/tab_map/map_filter/widgets/loading_fields_widget.dart';
import 'package:findnear/src/pages/voice_filter/voice_filter_view.dart';
import 'package:findnear/src/services/setting_service.dart';
import 'package:findnear/src/services/speech_to_text_service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'map_filter_logic.dart';
import 'map_filter_state.dart';

class MapFilterPage extends StatefulWidget {
  final VoidCallback onApplyFilter;
  final String keyword;

  MapFilterPage({Key key, this.onApplyFilter,this.keyword}) : super(key: key);

  @override
  MapFilterPageState createState() => MapFilterPageState();
}

class MapFilterPageState extends State<MapFilterPage> {
  final MapFilterLogic logic = Get.put(MapFilterLogic());
  final MapFilterState state = Get
      .find<MapFilterLogic>()
      .state;

  final SettingService settingService = Get.find<SettingService>();
  final SpeechToTextService sttService = Get.find<SpeechToTextService>();

  final _keySearch = "".obs;
  var delaySearch;

  @override
  void initState() {
    super.initState();
    state.textEditingController.text = widget.keyword;
    logic.initData();
    delaySearch = debounce(_keySearch, (value) {
      logic.runFilterField();
    }, time: Duration(milliseconds: 1000));
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Scaffold(
        appBar: AppBar(
          title: Text(S
              .of(context)
              .filter, style: Theme
              .of(context)
              .textTheme
              .headline6),
          automaticallyImplyLeading: false,
          centerTitle: false,
          actions: [
            Obx(() {
              return (sttService.isAvailable.value)
                  ? Container(
                width: 64,
                child: Center(
                  child: GestureDetector(
                    onTap: showVoiceFilter,
                    child: Container(
                      width: 32,
                      height: 32,
                      child: Image.asset(AppImages.icVoice,
                          fit: BoxFit.fill),
                      decoration: BoxDecoration(
                          boxShadow: AppShadow.boxShadow,
                          borderRadius: BorderRadius.circular(32)),
                    ),
                  ),
                ),
              )
                  : Container();
            }),
          ],
        ),
        body: SafeArea(
          child: Column(
            children: <Widget>[
              _buildMapTypeWidget(),
              SizedBox(height: 10,),
              _seachCategory(),
              SizedBox(height: 5,),
              Divider(color: Colors.grey, indent: 0, height: 1),
              Expanded(child: Obx(() {
                if (state.fetchFieldStatus == LoadStatus.loading) {
                  return _buildLoadingList();
                } else {
                  return _buildListView();
                }
              })),
              SizedBox(height: 10),
              _buildButtons(),
              SizedBox(height: 15),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildMapTypeWidget() {
    return Obx(() {
      return Container(
        margin: EdgeInsets.only(top: 4),
        height: 48,
        child: Row(
          children: [
            SizedBox(width: 12),
            Expanded(
              child: Text(
                "Xem theo ngành nghề",
                style: Theme
                    .of(context)
                    .textTheme
                    .bodyText1
                    .copyWith(fontSize: 14),
              ),
            ),
            Switch(
              value: settingService.displayMapInCareerRx.value,
              onChanged: (value) {
                settingService.displayMapByCareer(value);
              },
            )
          ],
        ),
      );
    });
  }

  Widget _seachCategory() {
    return Obx(() {
      return Container(
        height: 40,
        width: double.infinity,
        margin: EdgeInsets.only(right: 15, left: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: TextField(
                controller: state.textEditingController,
                textInputAction: TextInputAction.search,
                decoration: new InputDecoration(
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  contentPadding: EdgeInsets.symmetric(vertical: 4),
                  hintText: S
                      .of(context)
                      .search_category,
                  prefixIcon: Icon(
                    Icons.search_rounded,
                    color: Theme
                        .of(context)
                        .iconTheme
                        .color,
                    size: 20,
                  ),
                ),
                onChanged: (s) {
                  _keySearch.value = s;
                },
                onSubmitted: (text) {},
              ),
            ),
             Visibility(
                visible: (state.keyword.value ?? "").isNotEmpty,
                child: GestureDetector(
                  onTap: logic.clearKeyword,
                  child: Container(
                    width: 48,
                    child: Center(
                      child: Icon(
                        Icons.close_rounded,
                        color: Theme
                            .of(context)
                            .iconTheme
                            .color,
                        size: 20,
                      ),
                    ),
                  ),
                ),
              ),
          ],
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          border: Border.all(color: Colors.grey, width: 1),
        ),
      );
    });
  }

  Widget _buildLoadingList() {
    return LoadingFieldsWidget();
  }

  Widget _buildListView() {
    return Obx(() {
      return ListView.builder(
        padding: EdgeInsets.only(top: 8),
        itemBuilder: (context, index) {
          if (index == 0) {
            var isSelectedAll = true;
            try {
              isSelectedAll =
                  state.foundFields.firstWhere((element) => element.selected,
                      orElse: null) == null;
            } catch (e) {}
            return FilterItemWidget(
              title: S
                  .of(context)
                  .all,
              iconUrl: "",
              isSelected: isSelectedAll,
              showIcon: false,
              onChanged: (value) {
                logic.onClearFieldsFilter();
              },
            );
          }
          final item = state.foundFields.elementAt(index - 1);
          return FilterItemWidget(
            title: item.name,
            iconUrl: item.image?.icon,
            isSelected: item.selected,
            showIcon: item.showIcon,
            onChanged: (value) {
              logic.onChangeFieldsFilter(index - 1);
            },
          );
        },
        itemCount: state.foundFields.length + 1,
      );
    });
  }

  Widget _buildButtons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        MaterialButton(
          elevation: 0,
          onPressed: () {
            logic.onClearFieldsFilter();
          },
          height: 36,
          minWidth: 130,
          color: Theme
              .of(context)
              .accentColor,
          shape: StadiumBorder(),
          child: Text(
            S
                .of(context)
                .clear,
            textAlign: TextAlign.start,
            style: TextStyle(color: Theme
                .of(context)
                .primaryColor),
          ),
        ),
        MaterialButton(
          elevation: 0,
          onPressed: () {
            logic.saveFilter().whenComplete(() {
              widget.onApplyFilter?.call();
              Navigator.pop(context);
            });
          },
          height: 36,
          minWidth: 130,
          color: Theme
              .of(context)
              .accentColor,
          shape: StadiumBorder(),
          child: Text(
            S
                .of(context)
                .apply_filters,
            textAlign: TextAlign.start,
            style: TextStyle(color: Theme
                .of(context)
                .primaryColor),
          ),
        ),
      ],
    );
  }

  @override
  void dispose() {
    Get.delete<MapFilterLogic>();
    delaySearch.dispose();
    super.dispose();
  }

  ///Navigate
  void showVoiceFilter() async {
    final result = await Get.bottomSheet(VoiceFilterPage());
    if (result is String && (result ?? "").isNotEmpty) {
      logic.filterWithKeyword(result);
      logic.saveFilter().whenComplete(() {
        widget.onApplyFilter?.call();
        Navigator.pop(context);
      });
    }
  }
}
