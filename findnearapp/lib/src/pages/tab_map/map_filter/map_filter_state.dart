import 'package:findnear/src/models/cart.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/field.dart';
import 'package:findnear/src/models/filter.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MapFilterState {
  GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  List<Field> allFields = [];
  RxList<Field> foundFields = <Field>[].obs;

  // Rxn<Filter> filter = Rxn<Filter>();
  // Rxn<Cart> cart = Rxn<Cart>();
  Rx<LoadStatus> fetchFieldStatus = LoadStatus.initial.obs;

  TextEditingController textEditingController = TextEditingController();
  RxString keyword = "".obs;

  MapFilterState() {
    ///Initialize variables
     textEditingController.addListener(() {
           keyword.value = textEditingController.text;
     });
  }
}
