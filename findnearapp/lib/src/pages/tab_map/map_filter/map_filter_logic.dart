import 'dart:convert';

import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/field.dart';
import 'package:findnear/src/models/filter.dart';
import 'package:findnear/src/services/cache_service.dart';
import 'package:findnear/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../repository/field_repository.dart';

import '../../../../main.dart';
import 'map_filter_state.dart';

class MapFilterLogic extends GetxController {
  final state = MapFilterState();

  final cacheService = Get.find<CacheService>();

  void initData() async {
    _listenForFields();
  }

  Future<Filter> getSavedFilter() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return Filter.fromJSON(json.decode(prefs.getString('filter') ?? '{}'));
  }

  Future<void> saveFilter() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final filter = Filter();
    filter.fields = state.allFields.where((_f) => _f.selected).toList();
    prefs.setString('filter', json.encode(filter.toMap()));
  }

  void _listenForFields({String message}) async {
    final filter = await getSavedFilter();
    //Check cache
    if (cacheService.fields.isNotEmpty) {
      state.fetchFieldStatus.value = LoadStatus.loading;
      state.allFields = cacheService.fields;
      state.allFields.forEach((element) {
        if (filter.fields.contains(element)) {
          element.selected = true;
        } else {
          element.selected = false;
        }
      });
      state.foundFields.value = state.allFields;
      state.foundFields.refresh();

      state.fetchFieldStatus.value = LoadStatus.success;
      return;
    }
    //Fetch field
    state.allFields.clear();
    state.fetchFieldStatus.value = LoadStatus.loading;
    final Stream<Field> stream = await getFields();
    stream.listen(
      (Field _field) {
        if (filter.fields.contains(_field)) {
          _field.selected = true;
        } else {
          _field.selected = false;
        }
        state.allFields.add(_field);
      },
      onError: (a) {
        state.fetchFieldStatus.value = LoadStatus.failure;
        ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
          content: Text(S.current.verify_your_internet_connection),
        ));
      },
      onDone: () {
        state.foundFields.value =  state.allFields;
        state.foundFields.refresh();
        //Cache field
        cacheService.fields.clear();
        cacheService.fields.addAll(state.allFields);
        //Show message
        if (message != null) {
          ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
            content: Text(message),
          ));
        }
        state.fetchFieldStatus.value = LoadStatus.success;
      },
    );
  }

  void onClearFieldsFilter() {
    state.allFields.forEach((element) {
      element.selected = false;
    });

    state.foundFields.value = state.allFields;
    state.foundFields.refresh();
  }

  void onChangeFieldsFilter(int index) {
    state.foundFields.elementAt(index).selected =
    !state.foundFields.elementAt(index).selected;
    state.foundFields.refresh();
  }

  void filterWithKeyword(String keyword) {
    List<Field> fields = [];
    state.allFields.forEach((element) {
      if (Utils.isTextContainKeyword(text: keyword, keyword: element.name)) {
        fields.add(element);
      }
    });

    //
    if (fields.isEmpty) {
      ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
        content: Text("Yêu cầu không hợp lệ"),
      ));
      return;
    }
    state.allFields.forEach((element) {
      if (fields.contains(element)) {
        element.selected = true;
      } else {
        element.selected = false;
      }
    });

    //Update
    state.foundFields.value = state.allFields;
    state.foundFields.refresh();
  }

  void clearKeyword() {
    state.textEditingController.text = "";
    state.foundFields.value = state.allFields;
    state.foundFields.refresh();
  }

  void runFilterField() {
    String keyword = state.textEditingController.text;
    List<Field> results = [];
    if (keyword.isEmpty) {
      results = state.allFields;
    } else {
      results = state.allFields
          .where((element) =>
              element.name.toLowerCase().contains(keyword.toLowerCase()))
          .toList();
    }

    state.foundFields.value = results;
  }
}
