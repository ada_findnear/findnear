import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:video_player/video_player.dart';

import 'preview_video.dart';

class VideoWidget extends StatefulWidget {
  final String path;
  VideoWidget({Key key, this.path}) : super(key: key);
  @override
  _VideoWidgetState createState() => _VideoWidgetState();
}

class _VideoWidgetState extends State<VideoWidget> {
  VideoPlayerController _videoPlayerController;

  @override
  void initState() {
    _videoPlayerController = () {
      if (widget.path.startsWith("http"))
        return VideoPlayerController.network(widget.path);
      else
        return VideoPlayerController.file(File(widget.path));
    }()
      ..initialize().then((_) {
        // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
        setState(() {});
      });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        child: Stack(
      alignment: Alignment.center,
      children: <Widget>[
        AspectRatio(
          aspectRatio: _videoPlayerController.value.aspectRatio,
          child: VideoPlayer(_videoPlayerController),
        ),
        GestureDetector(
          onTap: () {
            _previewTopicVideo(widget.path);
          },
          child: Container(
            width: 60,
            height: 60,
            decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.4),
              borderRadius: BorderRadius.all(
                Radius.circular(60),
              ),
            ),
            child: Icon(
              Icons.play_arrow,
              color: Colors.white,
              size: 40,
            ),
          ),
        ),
      ],
    ));
  }

  void _previewTopicVideo(url) {
    Navigator.of(context).push(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) => PreviewVideoWidget(path: url),
      ),
    );
  }

  @override
  void dispose() {
    _videoPlayerController?.dispose();
    super.dispose();
  }
}

class VideoThumbWidget extends StatelessWidget {
  final String imageUrl;
  final String videoUrl;
  const VideoThumbWidget({Key key, this.imageUrl, this.videoUrl})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    print("${videoUrl}");
    return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
        ),
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(5),
              child: CachedNetworkImage(
                imageUrl: imageUrl,
                fit: BoxFit.fitWidth,
              ),
            ),
            GestureDetector(
              onTap: () => _previewTopicVideo(context, videoUrl),
              child: Container(
                width: 33.25,
                height: 33.25,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.black.withOpacity(0.4),
                ),
                padding: EdgeInsets.all(4),
                alignment: Alignment.center,
                child: SvgPicture.asset(
                  "assets/svg/ic_play.svg",
                  color: Colors.white,
                ),
              ),
            )
          ],
        ));
  }

  void _previewTopicVideo(BuildContext context, url) {
    Navigator.of(context).push(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) => PreviewVideoWidget(path: url),
      ),
    );
  }
}
