import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/settings/global_variable.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:velocity_x/velocity_x.dart';

class LoginRegisterPage extends StatelessWidget {
  static const String ROUTE_NAME = 'LoginRegisterPage';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Image.asset(
            AppImages.icLoginBg,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.cover,
          ),
          Image.asset(
            AppImages.icMaskGroup,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.cover,
          ),
          _buildBody(),
        ],
      ),
    );
  }

  Widget _buildBody() {
    return Column(
      children: [
        Expanded(child: _itemTopLogin()),
        Expanded(child: _itemBodyLogin()),
      ],
    );
  }

  Widget _itemTopLogin() {
    return Image.asset(AppImages.icLogin, width: 150, height: 150);
  }

  Widget _itemBodyLogin() {
    return Column(
      children: [
        _itemButtonLogin(),
        SizedBox(height: 18),
        _itemButtonRegister(),
        SizedBox(height: 18),
        Spacer(),
        _itemBottomLogin()
      ],
    );
  }

  Widget _itemBottomLogin() {
    return Container(
      margin: EdgeInsets.only(left: 28, right: 28, bottom: 28),
      child: Column(
        children: [
          Text.rich(
            TextSpan(
              children: [
                TextSpan(
                    text: S.of(Get.context).term_and_policy_des1,
                    style: TextStyle(
                        fontFamily: "Quicksand",
                        fontSize: 14,
                        color: AppColors.white,
                        fontWeight: FontWeight.normal)),
                TextSpan(
                  recognizer: new TapGestureRecognizer()
                    ..onTap = () async {
                      final privacyPolicyUrl =
                          "${GlobalConfiguration().getValue(privacyPolicy)}";
                      if (await canLaunch(privacyPolicyUrl)) {
                        await launch(privacyPolicyUrl);
                      }
                    },
                  text: S.of(Get.context).term_and_policy,
                  style: TextStyle(
                      fontFamily: "Quicksand",
                      fontSize: 14,
                      color: AppColors.term_yellow,
                      fontWeight: FontWeight.bold),
                ),
                TextSpan(
                    text: S.of(Get.context).term_and_policy_des2,
                    style: TextStyle(
                        fontFamily: "Quicksand",
                        fontSize: 14,
                        color: AppColors.white,
                        fontWeight: FontWeight.normal)),
              ],
            ),
          ),
          SizedBox(height: 17),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                AppImages.icContact,
                width: 21,
                height: 21,
                color: AppColors.white,
              ),
              SizedBox(width: 4),
              Text(S.of(Get.context).app_contact,
                  style: TextStyle(
                      fontFamily: "Quicksand",
                      fontSize: 14,
                      color: AppColors.white,
                      fontWeight: FontWeight.bold)),
            ],
          ),
        ],
      ),
    );
  }

  Widget _itemButtonLogin() => VxBox(
          child: S
              .of(Get.context)
              .login_button_content
              .text
              .size(16)
              .bold
              .color(AppColors.red)
              .make())
      .roundedLg
      .alignCenter
      .color(AppColors.white)
      .width(Get.width)
      .padding(EdgeInsets.symmetric(vertical: 15))
      .margin(EdgeInsets.symmetric(horizontal: 28))
      .make()
      .onTap(() => NavigatorUtils.toLoginStep2());

  Widget _itemButtonRegister() => VxBox(
          child: S
              .of(Get.context)
              .register_button_content
              .text
              .size(16)
              .bold
              .color(AppColors.white)
              .make())
      .roundedLg
      .alignCenter
      .color(AppColors.black)
      .width(Get.width)
      .padding(EdgeInsets.symmetric(vertical: 15))
      .margin(EdgeInsets.symmetric(horizontal: 32))
      .make()
      .onTap(() => NavigatorUtils.toRegister());
}
