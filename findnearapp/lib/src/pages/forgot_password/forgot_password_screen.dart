import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/change_password/app_text_input.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'forgot_password_logic.dart';
import 'forgot_password_state.dart';

class ForgotPasswordScreen extends StatefulWidget {
  static const String ROUTE_NAME = '/ForgotPasswordScreen';
  final User user;

  const ForgotPasswordScreen({Key key, this.user}) : super(key: key);

  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  final ForgotPasswordLogic logic = Get.put(ForgotPasswordLogic());
  final ForgotPasswordState state = Get.find<ForgotPasswordLogic>().state;
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    state.user = widget.user;
  }

  AppBar _getAppBar() {
    return AppBar(
      backgroundColor: AppColors.white,
      centerTitle: false,
      title: Text(
        S.of(context).forgotPassword,
        style: Theme.of(context).textTheme.headline2.merge(TextStyle(
              color: AppColors.textBlack,
              fontSize: 16,
            )),
      ),
    );
  }

  bool get validateAndSave {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Container(
        color: AppColors.white,
        child: SafeArea(
          child: Scaffold(
            backgroundColor: AppColors.grayBg,
            appBar: _getAppBar(),
            body: SingleChildScrollView(
              child: Obx(() {
                return Form(
                  autovalidateMode: state.autoValidateMode.value,
                  key: formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        margin: const EdgeInsets.symmetric(horizontal: 13, vertical: 17),
                        padding: const EdgeInsets.only(left: 15, top: 22, right: 15, bottom: 19),
                        decoration: BoxDecoration(
                          color: AppColors.white,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Text(
                              S.current.changePasswordDescription,
                              style: Theme.of(context).textTheme.bodyText2.copyWith(color: AppColors.red),
                            ),
                            const SizedBox(height: 10.5),
                            const Divider(
                              height: 1,
                              thickness: 1,
                              color: AppColors.dividerColor,
                            ),
                            const SizedBox(height: 16.5),
                            Obx(
                              () => AppTextInput(
                                title: S.of(context).newPassword,
                                hasPassword: true,
                                isPasswordVisible: state.isNewPasswordVisible.value,
                                onChangePasswordVisibility: () =>
                                    state.isNewPasswordVisible.value = !state.isNewPasswordVisible.value,
                                onChange: (data) => state.newPasswordInput.value = data,
                                validator: (input) =>
                                    input.length < 3 ? S.of(context).should_be_more_than_3_characters : null,
                              ),
                            ),
                            const SizedBox(height: 14),
                            Obx(
                              () => AppTextInput(
                                title: S.current.confirmPassword,
                                hasPassword: true,
                                isPasswordVisible: state.isConfirmPasswordVisible.value,
                                onChangePasswordVisibility: () =>
                                    state.isConfirmPasswordVisible.value = !state.isConfirmPasswordVisible.value,
                                onChange: (data) => state.confirmPasswordInput.value = data,
                                validator: (input) {
                                  if (input.length < 3) {
                                    return S.of(context).should_be_more_than_3_characters;
                                  } else {
                                    if (state.confirmPasswordInput.value != state.newPasswordInput.value) {
                                      return S.current.invalidConfirmPassword;
                                    } else {
                                      return null;
                                    }
                                  }
                                },
                              ),
                            ),
                            const SizedBox(height: 14),
                            AppTextInput(
                              title: S.of(Get.context).code,
                              textInputType: TextInputType.number,
                              onChange: (data) => state.code.value = data,
                              validator: (input) =>
                                  input.length < 6 ? S.of(context).should_be_more_than_6_letters : null,
                            ),
                            const SizedBox(height: 14),
                            AppTextInput(
                              title: S.current.authCodeForgotPasswordDescription,
                              textInputType: TextInputType.number,
                              onChange: (data) => state.authCode.value = data,
                              validator: (input) => input.length != 6 ? S.of(context).invalidPinCode : null,
                            ),
                            const SizedBox(height: 22),
                            InkWell(
                              onTap: () => onConfirmButtonTap(context),
                              child: Container(
                                height: 40,
                                decoration:
                                    BoxDecoration(color: AppColors.red, borderRadius: BorderRadius.circular(20)),
                                child: Center(
                                  child: Text(
                                    S.current.confirmation.toUpperCase(),
                                    style: Theme.of(context).textTheme.bodyText2.copyWith(
                                          color: AppColors.white,
                                          fontWeight: FontWeight.bold,
                                        ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              }),
            ),
          ),
        ),
      ),
    );
  }

  void onConfirmButtonTap(BuildContext context) {
    if (validateAndSave) {
      DialogHelper.showLoadingDialog(dismissible: false);

      logic.onConfirmForgotPassword(context);
    } else {
      state.autoValidateMode.value = AutovalidateMode.always;
    }
  }

  @override
  void dispose() {
    Get.delete<ForgotPasswordLogic>();
    super.dispose();
  }
}
