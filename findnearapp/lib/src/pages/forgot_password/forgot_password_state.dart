import 'package:findnear/src/models/user.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ForgotPasswordState {
  RxBool isNewPasswordVisible = false.obs;
  RxString newPasswordInput = "".obs;

  RxBool isConfirmPasswordVisible = false.obs;
  RxString confirmPasswordInput = "".obs;

  RxString authCode = "".obs;

  RxString code = "".obs;

  Rx<AutovalidateMode> autoValidateMode = AutovalidateMode.disabled.obs;

  User user;

  ChangePasswordState() {
    ///Initialize variables
  }
}
