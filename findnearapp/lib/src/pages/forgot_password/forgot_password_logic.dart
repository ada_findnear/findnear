import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/repository/authentication_repository.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'forgot_password_state.dart';

class ForgotPasswordLogic extends GetxController {
  final state = ForgotPasswordState();

  AuthenticationRepository repository = Get.find();

  ForgotPasswordLogic();

  void onConfirmForgotPassword(BuildContext context) async {
    try {
      final result = await repository.forgotPassword(
        password: state.newPasswordInput.value,
        passwordConfirmation: state.confirmPasswordInput.value,
        authCode: state.authCode.value,
        code: state.code.value,
      );
      if (result.success) {
        goBack();
        DialogHelper.showAlertDialog(
          context: context,
          description: S.current.resetPasswordSuccess,
          title: S.current.notifications,
          okText: 'OK',
          okFunction: () {
            goBack();
          },
        );
      } else {
        AppSnackbar.showError(title: S.of(context).error, message: result.message);
      }
    } catch (e) {
      goBack();
      AppSnackbar.showError(
        title: S.of(context).error,
        message: e.response?.data != null ? e.response?.data['message'] : S.current.somethingWentWrong,
      );
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
