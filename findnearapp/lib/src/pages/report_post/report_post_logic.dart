import 'package:findnear/main.dart';
import 'package:findnear/src/repository/post_repository.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'report_post_state.dart';

class ReportPostLogic extends GetxController {
  final state = ReportPostState();

  void sendReportPost({String postId}) async {
    final reason = state.textController.text;
    if (reason.isEmpty) {
      return;
    }
    try {
      final result = await reportPost(postId, reason);
      if (result) {
        ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
          content: Text(S.of(Get.context).success),
        ));
        Get.back();
      } else {
        ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
          content: Text(S.of(Get.context).failure),
        ));
      }
    } catch (e) {
      logger.e(e);
      ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
        content: Text(S.of(Get.context).failure),
      ));
    }
  }
}
