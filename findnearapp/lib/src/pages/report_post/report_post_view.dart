import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/post.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'report_post_logic.dart';
import 'report_post_state.dart';

class ReportPostPage extends StatefulWidget {
  final Post post;

  ReportPostPage({this.post});

  @override
  _ReportPostPageState createState() => _ReportPostPageState();
}

class _ReportPostPageState extends State<ReportPostPage> {
  final ReportPostLogic logic = Get.put(ReportPostLogic());
  final ReportPostState state = Get.find<ReportPostLogic>().state;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final isDark = Theme.of(context).brightness == Brightness.dark;
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: Image.asset(isDark ? AppImages.icBackDark : AppImages.icBackLight),
          onPressed: () {
            Get.back();
          },
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        centerTitle: false,
        leadingWidth: 40,
        titleSpacing: 0,
        elevation: 0,
        title: Text(
          S.of(context).report,
          style: Theme.of(context).textTheme.bodyText1.copyWith(fontWeight: FontWeight.bold),
        ),
        actions: [
          TextButton(
            child: Padding(
              padding: const EdgeInsets.only(bottom: 10),
              child: Text(
                S.of(context).send,
                style: TextStyle(fontSize: 20),
              ),
            ),
            onPressed: () {
              logic.sendReportPost(postId: widget.post.id);
            },
          ),
        ],
      ),
      body: _buildBodyWidget(),
    );
  }

  Widget _buildBodyWidget() {
    return Column(
      children: [
        Container(
          child: new ConstrainedBox(
            constraints: BoxConstraints(
              maxHeight: 200,
            ),
            child: TextField(
              controller: state.textController,
              autofocus: true,
              maxLines: null,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.symmetric(vertical: 20, horizontal: 15),
                hintStyle: TextStyle(fontSize: 18.0, color: Theme.of(context).hintColor),
                fillColor: Theme.of(context).primaryColor,
                filled: true,
                focusedBorder: OutlineInputBorder(
                  borderSide: new BorderSide(color: Colors.transparent, width: 0),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  void dispose() {
    state.textController.dispose();
    Get.delete<ReportPostLogic>();
    super.dispose();
  }
}
