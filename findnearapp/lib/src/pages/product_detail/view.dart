import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/response/product_response.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/pages/product/presentation/create_product/view.dart';
import 'package:findnear/src/pages/profile_user/profile_user_view.dart';
import 'package:findnear/src/pages/shop_detail/logic.dart';
import 'package:findnear/src/pages/shop_detail/view.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../main.dart';
import 'logic.dart';
import 'state.dart';

class ProductDetailPage extends StatefulWidget {
  static const String ROUTE_NAME = '/ProductDetailPage';
  final RouteArgument routeArgument;

  ProductDetailPage({this.routeArgument});

  @override
  _ProductDetailPageState createState() => _ProductDetailPageState();
}

class _ProductDetailPageState extends State<ProductDetailPage> {
  final ProductDetailLogic logic = Get.put(ProductDetailLogic());
  final ProductDetailState state = Get.find<ProductDetailLogic>().state;

  final ShopDetailLogic shopDetailLogic = Get.put(ShopDetailLogic());
  final ShopDetailLogic shopDetailState = Get.find<ShopDetailLogic>();

  bool get isMap => widget.routeArgument.param is Map;
  Product get product => isMap
      ? widget.routeArgument.param['product']
      : widget.routeArgument.param;
  bool get isRouteFromShop =>
      isMap ? widget.routeArgument.param['isRouteFromShop'] ?? false : false;
  Key key = UniqueKey();

  @override
  void initState() {
    logic.product = product;
    logic.fetchInitialData();
    super.initState();
  }

  Widget _buildImageSlide() {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: CarouselSlider(
        options: CarouselOptions(
          autoPlay: true,
          enableInfiniteScroll: logic.product.media.length > 1,
          autoPlayInterval: Duration(seconds: 3),
          height: 250.w,
          viewportFraction: 1.0,
        ),
        items: List<Widget>.generate(
          logic.product.media.length,
          (index) => Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image:
                    CachedNetworkImageProvider(logic.product.media[index].url),
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        key: key,
        fit: StackFit.expand,
        children: [
          Positioned(
            top: 0,
            child: _buildImageSlide(),
          ),
          Positioned(
            top: 210.w,
            left: 23.w,
            right: 23.w,
            bottom: 0,
            child: SingleChildScrollView(
              physics: ClampingScrollPhysics(),
              child: Column(
                children: [
                  _buildShopProfile(),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 17),
                    child: Text(
                      logic.product?.description ?? '',
                      style: TextStyle(
                        fontSize: 14,
                        color: const Color(0xff333333),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                colors: [AppColors.black, AppColors.black.withOpacity(0)],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              )),
              child: AppBar(
                iconTheme: IconThemeData(color: AppColors.white),
                backgroundColor: AppColors.transparent,
                elevation: 0,
              ),
            ),
          ),
          if (Get.find<LocalDataManager>().getMyShop()?.id ==
              logic.product.shop_id)
            Positioned(
              top: 35.w,
              right: 12.w,
              child: Row(
                children: [
                  InkWell(
                    onTap: () async {
                      final product = await Get.toNamed(
                        CreateProductPage.ROUTE_NAME,
                        arguments: RouteArgument(
                          param: {
                            'product': logic.product,
                            'categories': shopDetailLogic.categories
                          },
                        ),
                      );
                      if (product != null) {
                        logic.product = product;
                        setState(() {
                          key = UniqueKey();
                        });
                      }
                    },
                    child: Container(
                      width: 18.0,
                      height: 18.0,
                      child: SvgPicture.asset(AppImages.icEdit),
                    ),
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  InkWell(
                    onTap: () async {
                      logic.deleteProduct();
                    },
                    child: Container(
                      width: 18.0,
                      height: 18.0,
                      // padding: EdgeInsets.all(6),
                      child: SvgPicture.asset(AppImages.icDeleteOutline),
                    ),
                  ),
                ],
              ),
            ),
        ],
      ),
      bottomNavigationBar: Container(
        height: 60,
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        child: _buildBottomInfoBar(),
      ),
    );
  }

  Container _buildShopProfile() {
    var formatter = NumberFormat('#,###,000');
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            logic.product.name,
            style: TextStyle(
              fontSize: 16,
              color: const Color(0xff333333),
              fontWeight: FontWeight.w700,
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 9),
          Text(
            formatter.format(logic.product.price) + 'đ',
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 18,
              color: const Color(0xffff0000),
              fontWeight: FontWeight.w700,
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 6),
          if (Get.find<LocalDataManager>().getMyShop()?.id !=
              logic.product?.shop_id)
            Container(
              padding: EdgeInsets.symmetric(horizontal: 25, vertical: 7),
              child: InkWell(
                onTap: () {
                  if (!isLoggedIn) {
                    NavigatorUtils.toLogin();
                    return;
                  }
                  NavigatorUtils.navigateToProfilePage(
                      logic.product.userId.toString());
                },
                child: Text(
                  'Liên hệ để mua',
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 14,
                    color: const Color(0xffffffff),
                  ),
                  textHeightBehavior:
                      TextHeightBehavior(applyHeightToFirstAscent: false),
                  textAlign: TextAlign.left,
                ),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16.0),
                color: const Color(0xffff0000),
                border: Border.all(width: 1.0, color: const Color(0xffff0000)),
              ),
            ),
        ],
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20.0),
        color: const Color(0xffffffff),
        boxShadow: [
          BoxShadow(
            color: const Color(0x29000000),
            offset: Offset(0, 0),
            blurRadius: 6,
          ),
        ],
      ),
    );
  }

  Widget _buildBottomInfoBar() {
    var formatter = NumberFormat('#,###,000');
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          // InfoShop
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                // Circle image
                Container(
                  height: 28,
                  width: 28,
                  child: CircleAvatar(
                    radius: 30.0,
                    backgroundImage: NetworkImage(
                        (logic.product?.media?.isNotEmpty ?? false)
                            ? logic.product?.media?.first?.url
                            : ""),
                    backgroundColor: Colors.transparent,
                  ),
                ),
                SizedBox(
                  width: 10.0,
                ),
                // Name and rating
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        logic.product.name ?? "",
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontWeight: FontWeight.w700, fontSize: 12),
                      ),
                      Container(
                        // height: 20,
                        padding: EdgeInsets.zero,
                        child: Text(
                          formatter.format(logic.product.price) + 'đ',
                          style: TextStyle(
                            fontFamily: 'Quicksand',
                            fontSize: 12,
                            color: const Color(0xffff0000),
                            fontWeight: FontWeight.w700,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          _verticalSeparated(),
          _actionButton(AppImages.icShopBottom, S.of(context).shop_bottom,
              onPressed: () {
            int shopId = logic.product.shop_id;
            if (isRouteFromShop) {
              Get.until(
                (route) {
                  return Get.currentRoute == ShopDetailPage.ROUTE_NAME;
                },
              );
            } else {
              // NavigatorUtils.toShopDetailPage(shopId: shopId);
              Get.toNamed(ShopDetailPage.ROUTE_NAME,
                  arguments: RouteArgument(param: shopId));
            }
          }),
          _verticalSeparated(),
          Get.find<LocalDataManager>().getMyShop()?.id ==
                      logic.product.shop_id ??
                  false
              ? SizedBox.shrink()
              : _actionButton(AppImages.icChatCategory, S.of(context).messages,
                  onPressed: () {
                  logic.openChatRoom();
                }),
        ],
      ),
    );
  }

  Widget _verticalSeparated() {
    return Container(
      height: 32,
      width: 1,
      margin: EdgeInsets.symmetric(horizontal: 4.0),
      color: AppColors.grayDark,
    );
  }

  Widget _actionButton(String icon, String title,
      {Function onPressed,
      Color color,
      Color colorIcon,
      FontWeight fontWeight = FontWeight.normal}) {
    return Container(
      // height: 32,
      margin: EdgeInsets.symmetric(horizontal: 12.0),
      child: InkWell(
        onTap: onPressed,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset(icon, width: 14, height: 14, color: colorIcon),
            Text(
              title,
              style: TextStyle(
                  fontSize: 10,
                  color: color,
                  fontWeight: fontWeight,
                  height: 2),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    Get.delete<ProductDetailLogic>();
    super.dispose();
  }
}
