import 'package:findnear/src/database/conversations_dao.dart';
import 'package:findnear/src/global/global_event.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/communication/entities/group_config_entity.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/entities/communication/response/conversation_entity.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/bottom_sheet/change_bg_logic.dart';
import 'package:findnear/src/pages/shop_detail/logic.dart';
import 'package:findnear/src/repository/communication_repository.dart';
import 'package:findnear/src/repository/shop_repository.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:findnear/src/models/response/product_response.dart';
import 'package:global_configuration/global_configuration.dart';

import '../../../main.dart';
import 'state.dart';
import '../../models/user.dart' as userModel;
import '../../repository/user_repository.dart' as repository;

class ProductDetailLogic extends GetxController {
  final state = ProductDetailState();
  Product product = null;

  final ShopsRepository shopsRepository = Get.find();
  ShopDetailLogic shopDetailLogic = Get.find();
  CommunicationRepository _socketRepository =
  CommunicationApi(isSocketApi: true);
  final _conversationsDAO = Get.find<ConversationsDAO>();
  CommunicationRepository communicationRepository =
  Get.find<CommunicationRepository>();

  void deleteProduct() async {
    DialogHelper.showConfirmPopup(
      title: S.of(Get.context).deleteProduct,
      description: S.of(Get.context).msg_confirm_delete_product,
      okBtnPressed: () async {
        DialogHelper.showLoaderDialog();
        try {
          Product res = await shopsRepository.deleteProduct(
            productId: product == null ? 0 : product.id,
          );
          Get.back();
          if (res != null) {
            shopDetailLogic.getShopDetail(shopId: product?.shop_id);
            DialogHelper.showAlertDialog(
                context: Get.context,
                title: 'Thành công',
                okText: 'Đóng',
                okFunction: () {
                  print('res $res');
                  Get.back(result: res);
                },
                description: S.current.msg_delete_success);
          } else {
            Get.showSnackbar(GetBar(
              title: S.of(Get.context).an_error_occurred,
              message: S.of(Get.context).please_try_again_msg,
            ));
          }
        } catch (e) {
          Get.back();
          print(e);
          Get.showSnackbar(GetBar(
            title: S.of(Get.context).an_error_occurred,
            message: S.of(Get.context).please_try_again_msg,
          ));
        }
      },
    );
  }

  void fetchInitialData() async{
    try{
       shopDetailLogic.getCategories(shopId: product.shop_id);
    }catch(e){
      print("TuanLA - $e");
    }
  }

  ///Navigate
  void openChatRoom() async {
    try{
      if (!isLoggedIn) {
        NavigatorUtils.toLogin();
        return;
      }else{
        DialogHelper.showLoadingDialog();
        await listenForUser();
        await getConversation();
        Conversation _conversation;
        if (state.conversation != null) {
          _conversation = state.conversation;
        } else {
          ConversationEntity entity = await createConversation();
          _conversation = entity.toConversation;
        }
        Get.back();
        NavigatorUtils.navigateToConversationDetailPage(_conversation);
      }
    }catch(e){
      Get.back();
      print("TuanLA - $e");
    }
  }

  Future<ConversationEntity> createConversation() async {
    List<User> users = [];
    List<ShortUserEntity> shortUsers = [];

    User currentUser = Get.find<LocalDataManager>().currentUser;
    Conversation _conversation = new Conversation(users);

    _conversation.id = UniqueKey().toString();
    _conversation.lastMessageTime =
        DateTime.now().toUtc().millisecondsSinceEpoch;
    _conversation.name = state.userOwner.value.name;
    _conversation.readByUsers = [currentUser?.id ?? ""];
    _conversation.idAdmin = currentUser?.id ?? "";
    if (_conversation.visibleToUsers == null) {
      _conversation.visibleToUsers = [];
    }

    /// thêm short user bao gồm bản thân
    _conversation.users.add(currentUser);
    _conversation.visibleToUsers.add(currentUser.id);
    shortUsers.add(ShortUserEntity.fromUser(currentUser));

    /// thêm short user bao gồm user trong profile
    _conversation.users.add(state.userOwner.value);
    _conversation.visibleToUsers.add(state.userOwner.value.id);
    shortUsers.add(ShortUserEntity.fromUser(state.userOwner.value));

    _conversation.visibleToUsers.sort((a, b) => a.compareTo(b));
    _conversation.type = "PEEK";
    String currentUserId = currentUser.id;
    User user = users.firstWhere((element) => element.id != currentUserId);
    String imgUrl = user.image.thumb ??
        "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
    _conversation.groupAvatar = imgUrl;
    _conversation.config = GroupConfigEntity(
      groupAvatar: imgUrl,
      shortUsers: shortUsers,
      groupBackground: ChangeBgController.DEFAULT_BACKGROUND,
      privateBackground: ChangeBgController.DEFAULT_BACKGROUND,
    );

    ConversationEntity result =
    await _socketRepository.createConversations(_conversation);

    /// save vào local database
    await _conversationsDAO.put(result.toConversation);

    /// save lại vào conversation hiện tại
    state.conversation = result.toConversation;

    /// notify đến danh sách conversation cập nhật lại
    GlobalEvent.instance.onCreateConversationResponse.add(result);
    return result;
  }

  void getConversation() async {
    final userId = product.userId;
    if (userId == null) {
      return;
    }
    // state.conversation = await _conversationsDAO.getAllWithUserId(userId.toString());
    User _user = Get.find<LocalDataManager>().currentUser;
    List<ConversationEntity> result =
    await communicationRepository.findConversationByReceiver(
        receiver: state.userOwner.value.code, sender: _user.code);
    if (result.isNotEmpty && result.length >0) {
      state.conversation = result[0].toConversation;
    }
  }

  void listenForUser() async {
    final userModel.User data = await repository.getUser(product.userId);
    state.userOwner.value = data;
  }

}
