import 'dart:io';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/controllers/group_controller.dart';
import 'package:findnear/src/helpers/loadmore_text_builder.dart';
import 'package:findnear/src/models/entities/communication/response/conversation_entity.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/preview_media.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:loadmore/loadmore.dart';
import '../elements/MediaActionSheetWidget.dart';
import '../../generated/l10n.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import 'messages/conversation_list/widgets/search_member_widget.dart';

class GroupAddWidget extends StatefulWidget {
  Function onConversationCreated;

  GroupAddWidget({Key key, this.onConversationCreated}) : super(key: key);

  @override
  _GroupAddWidgetState createState() => _GroupAddWidgetState();
}

class _GroupAddWidgetState extends StateMVC<GroupAddWidget> {
  MediaActionSheetWidget _mediaActionSheetWidget;
  String pathImage;
  String nameGroup = '';
  String keyword = '';
  GroupController _groupController;
  List<User> listFriend = [];
  var _textController = TextEditingController();

  _GroupAddWidgetState() : super(GroupController()) {
    _groupController = controller;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _mediaActionSheetWidget = MediaActionSheetWidget(context: context);
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Image.asset(AppImages.icBackDark, color: Theme.of(context).hintColor),
            onPressed: () {
              Navigator.of(context).pop(false);
            },
          ),
          centerTitle: true,
          title: Text(
            "Tạo nhóm",
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 16,
              color: AppColors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          actions: <Widget>[
            GestureDetector(
              onTap: _handleCreateConversation,
              child: Container(
                margin: EdgeInsets.only(right: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(14.0),
                        color: const Color(0xffFF0000),
                      ),
                      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 7),
                      // margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                      child: Center(
                        child: Text(
                          "Hoàn tất",
                          style: TextStyle(
                            fontFamily: 'Quicksand',
                            fontSize: 12,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        body: SizedBox.expand(
          child: Container(
            child: Column(
              children: <Widget>[
                _addNameAndImage(),
                SizedBox(height: 20),
                editTextSearch(),
                SizedBox(height: 20),
                Expanded(
                    child: SingleChildScrollView(
                  padding: EdgeInsets.only(left: 15, right: 15),
                  child: Column(
                    children: [
                      getListFriend(),
                    ],
                  ),
                )),
                Align(
                  alignment: Alignment.bottomLeft,
                  child:
                      listFriend.length > 0 ? showAddedMembers() : SizedBox(),
                )
              ],
            ),
          ),
        ));
  }

  Widget _addNameAndImage() {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: TextField(
        decoration: InputDecoration(
          focusedBorder: OutlineInputBorder(
            borderSide: new BorderSide(color: Colors.transparent, width: 0),
          ),
          enabledBorder: OutlineInputBorder(
            // width: 0.0 produces a thin "hairline" border
            borderSide:
                BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
          ),
          border: OutlineInputBorder(),
          contentPadding: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
          hintText: S.of(context).nameGroup,
          hintStyle:
              TextStyle(fontSize: 16.0, color: Theme.of(context).hintColor),
          fillColor: Theme.of(context).primaryColor,
          filled: true,
          prefixIcon: ElevatedButton(
            onPressed: () {
              pathImage != null ? updateImage() : chooseImage();
            },
            child: pathImage != null
                ? new CircleAvatar(
                    foregroundImage: getImage(pathImage),
                  )
                : Icon(Icons.camera_alt_rounded),
            style: ElevatedButton.styleFrom(
                shape: CircleBorder(), primary: Colors.grey),
          ),
        ),
        onChanged: (value) => {
          setState(() {
            nameGroup = value;
          })
        },
      ),
    );
  }

  Widget editTextSearch() {
    return Container(
      height: 40,
      margin: EdgeInsets.only(bottom: 2, left: 10, right: 10),
      decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border.all(
            color: Color(0xffCCCCCC),
          ),
          borderRadius: BorderRadius.circular(20)),
      child: TextField(
        controller: _textController,
        textInputAction: TextInputAction.search,
        style: TextStyle(
          fontFamily: 'Quicksand',
          fontSize: 14,
          fontWeight: FontWeight.normal,
          color: Color(0xff333333),
        ),
        decoration: new InputDecoration(
          border: InputBorder.none,
          focusedBorder: InputBorder.none,
          enabledBorder: InputBorder.none,
          errorBorder: InputBorder.none,
          disabledBorder: InputBorder.none,
          hintText: S.of(context).searchForFriends,
          prefixIcon: Icon(Icons.search, color: Theme.of(context).focusColor),
          hintStyle: TextStyle(
            fontFamily: 'Quicksand',
            fontSize: 14,
            fontWeight: FontWeight.normal,
            color: Color(0xff333333),
          ),
        ),
        onChanged: (value) async {
          keyword = value != null ? value : '';
          await _groupController.setState(() {
            _groupController.currentOffset = 0;
            _groupController.canLoadMore = true;
          });
          await _groupController.listenForFriend(
              keyword: value, isClearData: true);
        },
        onSubmitted: (value) async {
          keyword = value != null ? value : '';
          await _groupController.setState(() {
            _groupController.currentOffset = 0;
            _groupController.canLoadMore = true;
          });
          await _groupController.listenForFriend(
              keyword: value, isClearData: true);
        },
      ),
    );
  }

  Widget getListFriend() {
    return LoadMore(
      isFinish: !_groupController.canLoadMore,
      child: ListView.separated(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemBuilder: (BuildContext context, int index) {
          return SearchMemberWidget(
            onChanged: (bool val) {
              if (listFriend.indexWhere((element) =>
                      element.id ==
                      _groupController.friends.elementAt(index).id) ==
                  -1) {
                setState(() {
                  listFriend.add(_groupController.friends.elementAt(index));
                });
              } else {
                setState(() {
                  listFriend.removeWhere((item) =>
                      item.id == _groupController.friends.elementAt(index).id);
                });
              }
            },
            avatar: _groupController.friends.elementAt(index).image.thumb,
            name: _groupController.friends.elementAt(index).name,
            value: listFriend.indexWhere((element) =>
                        element.id ==
                        _groupController.friends.elementAt(index).id) !=
                    -1
                ? true
                : false,
          );
        },
        separatorBuilder: (BuildContext context, int index) => const Divider(),
        itemCount: _groupController.friends.length,
      ),
      onLoadMore: _loadMore,
      whenEmptyLoad: false,
      delegate: DefaultLoadMoreDelegate(),
      textBuilder: CustomLoadMoreTextBuilder.getText,
    );
  }

  Future<bool> _loadMore() async {
    await Future.delayed(Duration(seconds: 2, milliseconds: 1000));
    await _groupController.listenForFriend(keyword: keyword);
    return true;
  }

  Widget showAddedMembers() {
    return Container(
      height: 60,
      padding: EdgeInsets.only(left: 10),
      child: ListView.separated(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            width: 40,
            child: Stack(
              children: [
                CircleAvatar(
                  foregroundImage:
                      getImage(listFriend.elementAt(index).image.thumb),
                ),
                Positioned(
                  top: -18,
                  left: 10,
                  child: IconButton(
                    onPressed: () {
                      setState(() {
                        listFriend.removeWhere((item) =>
                            item.id == listFriend.elementAt(index).id);
                      });
                    },
                    icon: Image.asset(AppImages.icAddMemberClose),
                    iconSize: 15,
                  ),
                )
              ],
            ),
          );
        },
        separatorBuilder: (BuildContext context, int index) => Container(width: 10),
        itemCount: listFriend.length,
      ),
    );
  }

  void chooseImage() async {
    _mediaActionSheetWidget.showActionSheet(S.of(context).chooseImage, [
      S.of(context).takePhoto,
      S.of(context).chooseFromGrallery
    ], (int index) {
      if (index == 0) {
        _openCamera((pickedFile) {
          pathImage = pickedFile.path;
        });
      }
      if (index == 1) {
        _openGrallery((pickedFile) {
          pathImage = pickedFile.path;
        });
      }
    });
  }

  void updateImage() async {
    _mediaActionSheetWidget.showActionSheet(S.of(context).chooseImage, [
      S.of(context).viewPicture,
      S.of(context).takePhoto,
      S.of(context).chooseFromGrallery
    ], (int index) {
      if (index == 0) {
        _previewTopicPhoto(pathImage);
      }
      if (index == 1) {
        _openCamera((pickedFile) {
          pathImage = pickedFile.path;
        });
      }
      if (index == 2) {
        _openGrallery((pickedFile) {
          pathImage = pickedFile.path;
        });
      }
    });
  }

  void _previewTopicPhoto(url) {
    Navigator.of(context).push(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) => PreviewMediaWidget(photoPath: url),
      ),
    );
  }

  void _openCamera(OnPickImageCallback onPickImageCallback) {
    _mediaActionSheetWidget.pickImageDialog(ImageSource.camera,
        (PickedFile pickedFile) {
      print("PickerFile:${pickedFile.path}");
      setState(() {
        onPickImageCallback(pickedFile);
      });
    });
  }

  void _openGrallery(OnPickImageCallback onPickImageCallback) {
    _mediaActionSheetWidget.pickImageDialog(ImageSource.gallery,
        (PickedFile pickedFile) {
      print("PickerFile:${pickedFile.path}");
      setState(() {
        onPickImageCallback(pickedFile);
      });
    });
  }

  ImageProvider getImage(url) {
    if (url.contains("http")) {
      return NetworkImage(url);
    } else {
      return FileImage(File(url));
    }
  }

  showAlertDialog(BuildContext context, String text) {
    // set up the button
    Widget okButton = TextButton(
      child: Text(S.of(context).ok),
      onPressed: () {
        Navigator.pop(context, true);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(S.of(context).notifications),
      content: Text(text),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void _handleCreateConversation() async {
    if (nameGroup.isEmpty && listFriend.length < 1) {
      showAlertDialog(context, S.of(context).errorCreateGroup);
    } else if (nameGroup.isNotEmpty && listFriend.length < 1) {
      showAlertDialog(context, S.of(context).errorCreateGroup1);
    } else if (nameGroup.isEmpty && listFriend.length == 1) {
      User currentUser = Get.find<LocalDataManager>().currentUser;
      ConversationEntity entity = await _groupController.createGroup(
          nameGroup: listFriend[0].name,
          listFriend: listFriend,
          pathImage: pathImage);
      widget.onConversationCreated(entity);
      Navigator.pop(context, true);
    } else if (nameGroup.isNotEmpty && listFriend.length == 1) {
      showAlertDialog(context, S.of(context).errorCreateGroup2);
    } else if (nameGroup.isEmpty && listFriend.length > 1) {
      showAlertDialog(context, S.of(context).errorCreateGroup3);
    } else if (nameGroup.isNotEmpty && listFriend.length > 1) {
      ConversationEntity entity = await _groupController.createGroup(
          nameGroup: nameGroup, listFriend: listFriend, pathImage: pathImage);
      widget.onConversationCreated(entity);
      Navigator.pop(context, true);
    }
  }
}
