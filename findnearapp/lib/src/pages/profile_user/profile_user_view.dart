import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/AddFriendWidget.dart';
import 'package:findnear/src/elements/AlbumItemWidget.dart';
import 'package:findnear/src/elements/MediaActionSheetWidget.dart';
import 'package:findnear/src/elements/NewsfeedItemWidget.dart';
import 'package:findnear/src/models/bottom_sheet_option.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/models/shop.dart';
import 'package:findnear/src/pages/create_post/create_post.controller.dart';
import 'package:findnear/src/pages/create_post/create_post.screen.dart';
import 'package:findnear/src/pages/detail_topic.dart';
import 'package:findnear/src/pages/map_direction/map_direction_view.dart';
import 'package:findnear/src/pages/messages/conversation_detail/conversation_detail_view.dart';
import 'package:findnear/src/pages/newsfeed/newsfeed_state.dart';
import 'package:findnear/src/pages/preview_media.dart';
import 'package:findnear/src/pages/profile_list_photo.dart';
import 'package:findnear/src/pages/profile_list_video.dart';
import 'package:findnear/src/pages/report_post/report_post_view.dart';
import 'package:findnear/src/pages/write_topic.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import 'profile_user_logic.dart';
import 'profile_user_state.dart';
import 'widgets/profile_action_button.dart';
import 'widgets/profile_user_banner_widget.dart';

class ProfileUserPage extends StatefulWidget {
  static const String ROUTE_NAME = '/ProfileUserPage';
  final RouteArgument routeArgument;

  ProfileUserPage({Key key, this.routeArgument})
      : super(key: key);

  @override
  _ProfileUserPageState createState() => _ProfileUserPageState();
}

class _ProfileUserPageState extends State<ProfileUserPage> {
  final ProfileUserLogic logic = Get.put(ProfileUserLogic());
  final ProfileUserState state = Get.find<ProfileUserLogic>().state;

  MediaActionSheetWidget _mediaActionSheetWidget;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final _scrollController = ScrollController();
  final _scrollThreshold = 200.0;

  bool _isMyProfile;

  @override
  void initState() {
    super.initState();
    state.userId.value = widget.routeArgument.param['userId'];
    _isMyProfile = state.userId.value == currentUser.value?.id;
    _scrollController.addListener(_onScroll);
    DialogHelper.initFToast(context: context);

    WidgetsBinding.instance.addPostFrameCallback((_) {
      state.message.listen(onMessage);
      state.buildContext = context;
      logic.fetchInitialData(state.userId.value);
    });
  }

  void onMessage(MessageStates message) {
    switch (message) {
      case MessageStates.savePostSuccessfully:
        DialogHelper.showToast(
          icon: AppImages.icBookmark,
          message: S.current.save_post_successfully,
        );
        break;
      case MessageStates.unsavePostSuccessfully:
        DialogHelper.showToast(
          icon: AppImages.icRemoveBookmark,
          message: S.of(context).unsave_post_successfully,
        );
        break;
      case MessageStates.hidePostSuccessfully:
        DialogHelper.showToast(
          icon: AppImages.icHide,
          message: S.of(context).hidePostSuccessfully,
        );
        break;
      case MessageStates.unhidePostSuccessfully:
        DialogHelper.showToast(
          icon: AppImages.icHide,
          message: S.of(context).unhidePostSuccessfully,
        );
        break;
      case MessageStates.idle:
        break;
    }
  }

  @override
  void dispose() {
    Get.delete<ProfileUserLogic>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    state.scaffoldKey = _scaffoldKey;
    _mediaActionSheetWidget = MediaActionSheetWidget(context: context);
    final isDark = Theme.of(context).brightness == Brightness.dark;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: isDark
          ? Theme.of(context).scaffoldBackgroundColor
          : Color(0xFFF8F8F8),
      appBar: _buildAppBar(),
      extendBodyBehindAppBar: true,
      body: _buildBodyWidget(),
      floatingActionButton: currentUser.value?.apiToken != null
          ? Obx(
              () => Visibility(
                visible: state.loadUserInfoComplete.value,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(right: 12),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          ProfileActionButton(
                            svgPath: "assets/svg/ic_direction.svg",
                            onPressed: () {
                              if (state.user.value != null) {
                                Get.to(() =>
                                    MapDirectionPage(user: state.user.value));
                              }
                            },
                          ),
                          if (!_isMyProfile &&
                              !(state.user.value.isBlocking ?? false))
                            ProfileActionButton(
                              svgPath: "assets/svg/ic_call.svg",
                              onPressed: _makeCall,
                            ),
                          if (!_isMyProfile &&
                              !(state.user.value.isBlocking ?? false))
                            ProfileActionButton(
                              svgPath: "assets/svg/ic_chat.svg",
                              onPressed: _openChat,
                            ),
                          SizedBox(height: 4),
                          if (!_isMyProfile)
                            Container(
                              margin: const EdgeInsets.only(right: 4),
                              child: _addFriendWidget(),
                            ),
                        ],
                      ),
                    ),
                    if (state.user.value.shop?.id != null &&
                        !_isMyProfile &&
                        state.user.value.canCreateShop == true)
                      IconButton(
                        iconSize: 72.w,
                        padding: EdgeInsets.zero,
                        icon: SvgPicture.asset(AppImages.icMyStore),
                        onPressed: () {
                          Shop myShop = state.user.value?.shop;
                          // Get.toNamed(ShopDetailPage.ROUTE_NAME,
                          //     arguments: RouteArgument(param: myShop));
                          NavigatorUtils.toShopDetailPage(shopId: myShop.id);
                        },
                      ),
                  ],
                ),
              ),
            )
          : Container(),
    );
  }

  PreferredSize _buildAppBar() {
    return PreferredSize(
      preferredSize: Size(Get.width, 40),
      child: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                // 10% of the width, so there are ten blinds.
                colors: <Color>[
                  Color(0xFF333333),
                  Color(0x00000000)
                ], // red to yellow
              ),
            ),
          ),
          AppBar(
            backgroundColor: Colors.transparent,
            centerTitle: true,
            elevation: 0,
            leadingWidth: 52,
            leading: IconButton(
              icon: Image.asset(
                AppImages.icBackDark,
                color: Theme.of(context).accentColor,
              ),
              onPressed: () => Get.back(),
            ),
            actions: [
              currentUser.value?.apiToken != null && !_isMyProfile
                  ? IconButton(
                      onPressed: _showUserMenu,
                      icon: SvgPicture.asset(
                        "assets/svg/ic_menu.svg",
                        color: Colors.white,
                      ),
                    )
                  : Container(),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildBodyWidget() {
    return _timelineContainer();
  }

  Widget _buildTopWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        _buildBannerWidget(),
        _mediaContainer(),
        _noDataContainer(),
        // _addTopicContainer(),
      ],
    );
  }

  Widget _noDataContainer() {
    return Visibility(
      visible: !(state.posts.length > 0),
      child: Container(
        height: 80.0,
        child: Center(
          child: Text(
              (state.posts.length > 0) ? "": S.of(context).no_action,
            style: TextStyle(fontSize: 14, color: Colors.grey),
          ),
        ),
      ),
    );
  }

  Widget _mediaContainer() {
    return Container(
      height: 80.0,
      margin: EdgeInsets.only(top: 11),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            AlbumItemWidget(
              icon: Icons.photo,
              title: S.of(context).photo,
              desc: S.of(context).seeAllPhoto,
              onSelected: () {
                Get.to(() => ProfileListPhotoWidget(userId: state.userId.value));
              },
              grStartColor: Color.fromRGBO(252, 36, 36, 1),
              grMiddleColor: Color.fromRGBO(252, 82, 82, 0.87),
              grEndColor: Color.fromRGBO(255, 181, 181, 0.3),
              imgAsset: "assets/svg/ic_photo.svg",
            ),
            SizedBox(
              width: 10,
            ),
            AlbumItemWidget(
              icon: Icons.videocam,
              title: S.of(context).video,
              desc: S.of(context).seeAllVideo,
              onSelected: () {
                Get.to(() => ProfileListVideoWidget(userId: state.userId.value));
              },
              grStartColor: Color.fromRGBO(0, 141, 76, 1),
              grMiddleColor: Color.fromRGBO(119, 180, 123, 0.87),
              grEndColor: Color.fromRGBO(255, 181, 181, 0.3),
              imgAsset: "assets/svg/ic_video_camera.svg",
            ),
          ],
        ),
      ),
    );
  }

  Widget _timelineContainer() {
    return Obx(() {
      return RefreshIndicator(
        onRefresh: _onRefreshData,
        child: ListView.separated(
          controller: _scrollController,
          padding: EdgeInsets.zero,
          itemCount: state.posts.length + 1,

          itemBuilder: (BuildContext context, int index) {
            if (index == 0) {
              return _buildTopWidget();
            }
            final postIndex = index - 1;
            final post = state.posts.elementAt(postIndex);
            return Container(
              color: Colors.white,
              padding: const EdgeInsets.only(top: 13, bottom: 17, left: 15, right: 15),
              child: Hero(
                tag: 'preview_${postIndex ?? 0}',
                child: NewsfeedItemWidget(
                  onPreviewPhoto: (url) {
                    print("onPreviewPhoto");
                    print(url);
                    _previewTopicPhoto(
                      url,
                    );
                  },
                  onMyPostMenuPressed: () async {
                    final indexTapped =
                        await DialogHelper.showBottomSheetOptions(
                      context: context,
                      options: [
                        BottomSheetOption(
                          iconPath: AppImages.icWrite,
                          text: S.of(context).edit_post,
                        ),
                        BottomSheetOption(
                          iconPath: AppImages.icTrash,
                          text: S.of(context).delete_post,
                        ),
                        // temporarily disable
                        // BottomSheetOption(
                        //   iconPath: AppImages.icBookmarkOutlined,
                        //   text: S.of(context).save_post,
                        // ),
                        BottomSheetOption(
                          icon: Icons.share,
                          text: S.of(context).shareThisPost,
                        ),
                      ],
                    );
                    switch (indexTapped) {
                      case 0:
                        _onClickEditPost(state.posts.elementAt(index - 1));
                        break;
                      case 1:
                        final isDeleting = await DialogHelper.showConfirmPopup(
                          title: S.current.deletePost,
                          description: S.current.confirmDeleteDescription,
                          okBtnText: S.current.deletePostShort,
                        );

                        if (isDeleting ?? false)
                          logic.deletePost(index: index - 1);
                        break;
                      case 2:
                        onClickSharePostOtherApp();
                        break;
                    }
                  },
                  onOtherUserPostMenuPressed: () async {
                    final tappedIndex =
                        await DialogHelper.showBottomSheetOptions(
                      context: context,
                      options: [
                        // temporarily disable
                        // BottomSheetOption(
                        //   iconPath: AppImages.icBookmarkOutlined,
                        //   text: S.of(context).save_post,
                        // ),
                        BottomSheetOption(
                          iconPath: AppImages.icHide,
                          text: S.of(context).hide_post,
                        ),
                        BottomSheetOption(
                          icon: Icons.share,
                          text: S.of(context).shareThisPost,
                        ),
                        BottomSheetOption(
                          icon: Icons.report,
                          text: S.of(context).reportThisPost,
                        ),
                      ],
                    );
                    switch (tappedIndex) {
                      case 0:
                        logic.hidePost(index - 1);
                        break;
                      case 1:
                        onClickSharePostOtherApp();
                        break;
                      case 2:
                        _onClickReport(post);
                        break;
                    }
                  },
                  post: post,
                  toggleLikePost: (postID) {
                    logic.toggleLikePost(postID);
                  },
                  onSharePost: (){
                    _onClickSharePost(post);
                  },
                  isMyProfile: _isMyProfile,
                  didUpdateCallback: () async {
                    await _onRefreshData();
                  },
                ),
              ),
            );
          },
          separatorBuilder: (context, index) => index == 0
              ? SizedBox(height: 16)
              : const Divider(
                  height: 8, thickness: 8, color: AppColors.divider),
          // shrinkWrap: true,
          scrollDirection: Axis.vertical,
        ),
      );
      // return state.isLoadingPost.value
      //     ? PostsLoaderWidget(type: "post")
      //     : (state.posts.length == 0)
      //         ? NoDataPostsWidget(isMyProfile: true)
      //         : GetBuilder<ProfileV2Controller>(
      //             builder: (logic) {
      //               return ;
      //             },
      //           );
    });
  }

  void _editTopic(BuildContext context, Post post) {
    Navigator.of(context).push(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) => WriteTopicWidget(
            isWriteTopic: true,
            handleSubmit: (post, pathImages) {
              logic.updatePost(context, post);
            },
            currentPost: post),
      ),
    );
  }

  void _onClickReport(Post post) {
    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    }
    ;
    DialogHelper.showReportPopup(post);
  }

  void _openTopicSetting(BuildContext context, Post post) async {
    await _mediaActionSheetWidget.showActionSheet("Options", [
      S.of(context).privacy,
      S.of(context).editCaption,
      S.of(context).deleteActivity
    ], (int index) {
      if (index == 0) {
        _mediaActionSheetWidget.showActionSheet(S.of(context).privacy, [
          S.of(context).viewerPrivate,
          S.of(context).viewerOnlyFriend,
          S.of(context).viewerPublic
        ], (int index) {
          logic.updatePost(
              context,
              new Post.fromJSON(
                  {'id': post.id, 'content': post.content, 'viewer': index}));
        });
      }
      if (index == 1) {
        _editTopic(context, post);
      }
      if (index == 2) {
        showDialog<String>(
            context: context,
            builder: (BuildContext context) => AlertDialog(
                  title: Text(S.of(context).confirm),
                  content: Text(S.of(context).confirmRemovePost),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () => Navigator.pop(context, 'Cancel'),
                      child: Text(S.of(context).cancel),
                    ),
                    TextButton(
                      onPressed: () => _submitRemovePost(context, post),
                      child: Text(S.of(context).ok),
                    ),
                  ],
                ));
      }
    });
  }

  void _onClickEditPost(Post post) async {
    Get.put(CreatePostController(post: post));
    final result =
        await Get.to(() => CreatePostScreen(user: currentUser.value));
    Get.delete<CreatePostController>();
    if (result != null) logic.fetchInitialData(state.userId.value);
  }

  void _submitRemovePost(BuildContext context, post) {
    Navigator.pop(context, 'OK');
    logic.removePost(context, post);
  }

  void _openGrallery(OnPickImageCallback onPickImageCallback) {
    _mediaActionSheetWidget.pickImageDialog(ImageSource.gallery,
        (PickedFile pickedFile) {
      // setState(() {
      onPickImageCallback(pickedFile);
      // });
    });
  }

  void _openCamera(OnPickImageCallback onPickImageCallback) {
    _mediaActionSheetWidget.pickImageDialog(ImageSource.camera,
        (PickedFile pickedFile) {
      // setState(() {
      onPickImageCallback(pickedFile);
      // });
    });
  }

  void _previewTopicPhoto(String url, {String thumbUrl}) {
    print("_previewTopicPhoto");
    String path = url;
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => PreviewMediaWidget(
          photoPath: path,
          thumbPath: thumbUrl,
        ),
      ),
    );
  }

  void _previewAvatar(String url) {
    Navigator.of(context).push(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) => PreviewMediaWidget(photoPath: url),
      ),
    );
  }

  void _previewCover(String url) {
    Navigator.of(context).push(CupertinoPageRoute(
      fullscreenDialog: true,
      builder: (context) => PreviewMediaWidget(photoPath: url),
    ));
  }

  void _openDetailTopic(BuildContext context,
      {post, profileController, userId, index}) async {
    print("_openDetailTopic");
    final result = await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext context) => DetailTopicWidget(
          post: post,
        ),
      ),
    );

    if (result != null) {
      await logic.listenForPost(context, post.id, index);
    }
  }

  Widget _buildBannerWidget() {
    return Obx(() {
      return ProfileUserBannerWidget(
        user: state.user.value,
        onBannerPressed: () {
          _previewCover(state.user.value.imageCover.url);
        },
        onAvatarPressed: () {
          _previewAvatar(state.user.value.image.url);
        },
      );
    });
  }

  Future<void> _onRefreshData() async {
    logic.fetchInitialData(state.userId.value);
  }

  void _onClickSharePost(Post post) async {
    final originalPost = post.isSharedPost ? post.originalPost : post;
    Get.put(CreatePostController(originalPost: originalPost));
    var res = await Get.to(() => CreatePostScreen(
          user: currentUser.value,
          originalPost: originalPost,
        ));
    Get.delete<CreatePostController>();
    if (res != null) logic.fetchInitialData(state.userId.value);
  }

  void onClickSharePostOtherApp() async {
    DialogHelper.showAlertDialog(
      context: context,
      description: "Chức năng đang trong quá trình hoàn thiện",
      title: S.current.notifications,
      okText: 'OK',
      okFunction: () {

      },
    );
  }

  Widget _addFriendWidget() {
    return Obx(() {
      return AddFriendWidget(
          userData: state.user.value,
          onPressed: () {
            if (state.user?.value?.isFriend == false) {
              if (state.user?.value?.has_friend_request_from == true) {
                _mediaActionSheetWidget.showActionSheet(
                    S.of(context).friendRequest,
                    [S.of(context).confirm, S.of(context).deny], (int index) {
                  if (index == 0) {
                    logic.addFriend(context, state.user.value.id, "confirm");
                  }
                  if (index == 1) {
                    logic.addFriend(context, state.user.value.id, "deny");
                  }
                });
              } else if (state.user?.value?.has_sent_friend_request_to ==
                  true) {
                logic.addFriend(context, state.user.value.id, "cancel");
              } else {
                logic.addFriend(context, state.user.value?.id, "add");
              }
            } else {
              _mediaActionSheetWidget.showActionSheet(
                  S.of(context).friend, [S.of(context).unfriend], (int index) {
                if (index == 0) {
                  showDialog<String>(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                            title: Text(S.of(context).confirm),
                            content: Text(S.of(context).wantToDeleteThisFriend),
                            actions: <Widget>[
                              TextButton(
                                onPressed: () =>
                                    Navigator.pop(context, 'Cancel'),
                                child: Text(S.of(context).cancel),
                              ),
                              TextButton(
                                onPressed: () => {
                                  Navigator.pop(context, 'OK'),
                                  logic.addFriend(
                                      context, state.user.value.id, "delete")
                                },
                                child: Text(S.of(context).ok),
                              ),
                            ],
                          ));
                }
              });
            }
          },
          iconColor: Theme.of(context).primaryColor,
          labelColor: Theme.of(context).hintColor);
    });
  }

  ///Navigate
  void _openChat() {
    logic.openChatRoom();
  }

  void _makeCall() {
    logic.makeCall();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      logic.fetchNextPageData();
    }
  }

  void _showUserMenu() {
    Get.bottomSheet(Container(
      padding: EdgeInsets.only(top: 10, bottom: Get.bottomBarHeight),
      decoration: BoxDecoration(
          color: Theme.of(context).scaffoldBackgroundColor,
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(20),
            topLeft: Radius.circular(20),
          )),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListTile(
            leading: Icon(
              Icons.block_rounded,
              color: Theme.of(context).accentColor,
            ),
            minLeadingWidth: 0,
            title: Text(
              (state.user.value.isBlocking == false)
                  ? S.of(context).block
                  : S.of(context).unblock,
              style: Theme.of(context).textTheme.bodyText1.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
            ),
            onTap: () {
              if (state.user.value.isBlocking == false) {
                logic.blockUser();
              } else {
                logic.unblockUser();
              }
              Get.back();
            },
          )
        ],
      ),
    ));
  }

  void _handleUserMenuPressed(Post post) async {
    final result = await Get.bottomSheet(Container(
      padding: EdgeInsets.only(top: 10, bottom: Get.bottomBarHeight),
      decoration: BoxDecoration(
          color: Theme.of(context).scaffoldBackgroundColor,
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(20),
            topLeft: Radius.circular(20),
          )),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListTile(
            leading: Icon(
              Icons.report_off_outlined,
              color: Theme.of(context).accentColor,
            ),
            minLeadingWidth: 0,
            title: Text(
              "Report",
              style: Theme.of(context)
                  .textTheme
                  .bodyText1
                  .copyWith(fontWeight: FontWeight.bold),
            ),
            onTap: () {
              Get.back(result: "report");
            },
          )
        ],
      ),
    ));
    if (result is String && result == "report") {
      Get.to(() => ReportPostPage(post: post));
    }
  }
}
