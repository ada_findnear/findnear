import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../../models/user.dart';

class ProfileUserBannerWidget extends StatelessWidget {
  final User user;
  final VoidCallback onBannerPressed;
  final VoidCallback onAvatarPressed;

  ProfileUserBannerWidget({
    Key key,
    this.user,
    this.onBannerPressed,
    this.onAvatarPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.transparent.withAlpha(0),
      child: Column(
        children: [
          Container(
            height: 250,
            child: Stack(
              children: [
                _coverWidget(context),
                Positioned.fill(
                  bottom: -170,
                  child: Center(
                    child: _avatarWidget(context),
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 20,right: 20),
            child: Text(
              user?.name ?? "",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18.0, color: Theme.of(context).hintColor, letterSpacing: 2.0, fontWeight: FontWeight.bold),
            ),
          ),
          Text(
            "ID: ${user?.code ?? ""}",
            style: Theme.of(context).textTheme.headline6.copyWith(fontSize: 12, fontWeight: FontWeight.normal),
          ),
          (user?.bio ?? "").isNotEmpty
              ? Text(user?.bio ?? "",
                  style: TextStyle(
                    fontSize: 12.0,
                    color: Theme.of(context).accentColor,
                  ))
              : Container(),
          SizedBox(
            height: 6,
          ),
        ],
      ),
    );
  }

  Widget _coverWidget(BuildContext context) {
    return GestureDetector(
      onTap: onBannerPressed,
      child: Container(
        height: 220,
        width: double.infinity,
        child: (user?.imageCover?.url?.isEmpty ?? true)
            ? Container(height: 220)
            : CachedNetworkImage(
                imageUrl: user?.imageCover?.url ?? "",
                fit: BoxFit.cover,
                height: 220,
                // width: double.infinity,
                errorWidget: (context, url, error) => Image.network(
                  user?.imageCover?.thumb,
                  fit: BoxFit.cover,
                  height: 220,
                ),
              ),
      ),
    );
  }

  Widget _avatarWidget(BuildContext context) {
    return Container(
      width: 80,
      height: 80,
      child: Stack(
        children: [
          Container(
            width: 80,
            height: 80,
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.white,
                width: 4,
              ),
              shape: BoxShape.circle,
            ),
            child: new GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: onAvatarPressed,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: CachedNetworkImage(
                  imageUrl: user?.image?.icon ?? "",
                  fit: BoxFit.cover,
                  width: 80,
                  height: 80,
                  errorWidget: (context, url, error) => ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: Image.network(
                      url,
                      fit: BoxFit.cover,
                      width: 80,
                      height: 80,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
