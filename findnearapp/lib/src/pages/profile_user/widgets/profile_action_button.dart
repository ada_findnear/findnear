import 'package:findnear/src/commons/app_shadows.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ProfileActionButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String svgPath;

  ProfileActionButton({
    Key key,
    this.onPressed,
    this.svgPath,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        width: 36,
        height: 36,
        margin: EdgeInsets.all(4),
        padding: EdgeInsets.all(9),
        decoration: BoxDecoration(
          boxShadow: AppShadow.boxShadow,
          color: Colors.white,
          shape: BoxShape.circle,
        ),
        child: SvgPicture.asset(
          svgPath ?? "assets/svg/ic_direction.svg",
          color: Theme.of(context).accentColor,
        ),
      ),
    );
  }
}
