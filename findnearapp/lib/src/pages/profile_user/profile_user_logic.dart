import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dio/dio.dart' as dio;
import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/database/conversations_dao.dart';
import 'package:findnear/src/global/global_event.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/communication/entities/group_config_entity.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/entities/communication/response/conversation_entity.dart';
import 'package:findnear/src/models/entities/state_usercall.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/enums/websocket_event.dart';
import 'package:findnear/src/models/media.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/models/twilio_target_user.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/bottom_sheet/change_bg_logic.dart';
import 'package:findnear/src/pages/messages/conversation_detail/conversation_detail_view.dart';
import 'package:findnear/src/pages/newsfeed/newsfeed_state.dart';
import 'package:findnear/src/pages/videocall/callsignaling.dart';
import 'package:findnear/src/repository/chat_repository.dart';
import 'package:findnear/src/repository/communication_repository.dart';
import 'package:findnear/src/repository/newsfeed_repository.dart';
import 'package:findnear/src/repository/notification_v2_repository.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/repository/user_repository.dart' as userRepo;
import 'package:findnear/src/services/websocket_service.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../models/user.dart' as userModel;
import '../../repository/friends_repository.dart' as friendRepo;
import '../../repository/post_repository.dart' as postRepo;
import '../../repository/user_repository.dart' as repository;
import 'profile_user_state.dart';

class ProfileUserLogic extends GetxController {
  final state = ProfileUserState();

  ChatRepository _chatRepository = ChatRepository();
  NewsfeedRepository newsfeedRepository = Get.find<NewsfeedRepository>();
  CommunicationRepository communicationRepository =
      Get.find<CommunicationRepository>();
  CommunicationRepository _socketRepository =
      CommunicationApi(isSocketApi: true);
  final _conversationsDAO = Get.find<ConversationsDAO>();

  ProfileUserLogic({ChatRepository chatRepository}) {}

  void fetchInitialData(String userId) {
    //Clear data
    state.posts.clear();
    state.recentOrders.clear();
    state.canLoadMore.value == true;
    state.currentOffset.value = 0;

    listenForUser();
    listenForPosts();
    getConversation();
  }

  void fetchNextPageData() {
    if (state.canLoadMore.value == false) {
      return;
    }
    listenForPosts();
  }

  void listenForPosts({String message}) async {
    if (state.fetchPostsStatus.value == LoadStatus.loading) {
      return;
    }

    state.fetchPostsStatus.value = LoadStatus.loading;
    final Stream<Post> stream = await postRepo.getPostV2(
      offset: state.currentOffset,
      userId: state.userId.value,
      limit: 10,
    );
    stream.listen(
      (Post _post) {
        state.posts.add(_post);
      },
      onError: (a) {
        state.fetchPostsStatus.value = LoadStatus.failure;
        ScaffoldMessenger.of(state.scaffoldKey.currentContext)
            .showSnackBar(SnackBar(
          content: Text(S.of(Get.context).verify_your_internet_connection),
        ));
      },
      onDone: () {
        if (state.currentOffset.value == state.posts.length) {
          state.canLoadMore.value = false;
        }
        state.currentOffset.value = state.posts.length;
        state.fetchPostsStatus.value = LoadStatus.success;

        if (message != null) {
          ScaffoldMessenger.of(state.scaffoldKey.currentContext)
              .showSnackBar(SnackBar(
            content: Text(message),
          ));
        }
      },
    );
  }

  void listenForPost(BuildContext context, postId, index,
      {String message}) async {
    final Stream<Post> stream = await postRepo.getPostById(postId);

    stream.listen((Post _post) {
      state.posts[index] = _post.copyWith(creator: state.user.value);
    }, onError: (a) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(S.of(context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (message != null) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  void getListMedia(BuildContext context, user_id, {String message}) async {
    final Stream<Post> stream = await postRepo.getPost(userId: user_id);
    stream.listen((Post _post) {
      if (_post.medias.length > 0) {
        _post.medias.forEach((element) {
          if (element.mime_type.indexOf("image") != -1) {
            state.listImage.add(element);
          } else if (element.mime_type.indexOf("video") != -1) {
            state.listVideo.add(element);
          }
        });
      }
    }, onError: (a) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(S.of(context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (message != null) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  void createPost(BuildContext context, Post post, pathImages) async {
    postRepo.addPost(post, pathImages).then((value) {
      if (value != Post.fromJSON({})) {
        state.posts.insert(0, value);
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(S.of(context).createPostSuccessful),
        ));
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(S.of(context).verify_your_internet_connection),
        ));
      }
    });
  }

  void updatePost(BuildContext context, Post post) async {
    postRepo.updatePost(post).then((value) {
      var dataPosts = state.posts;
      var indexOfPost =
          state.posts.indexWhere((element) => element.id == post.id);
      if (indexOfPost != -1) {
        dataPosts[indexOfPost] = value;
        state.posts = dataPosts;
      }
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(S.of(context).updatePostSuccessful),
      ));
    });
  }

  void listenForUser() async {
    final userModel.User data =
        await repository.getUser(int.parse(state.userId.value));
    state.user.value = data;
    state.loadUserInfoComplete.value = true;
  }

  void toggleLikePost(postID, {String message}) async {
    postRepo.toggleLikePost(postID).then((value) {
      var indexOfPost =
          state.posts.indexWhere((element) => element.id == postID);
      if (value) {
        state.posts[indexOfPost].likers_count =
            (int.parse(state.posts[indexOfPost].likers_count) + 1).toString();
        state.posts[indexOfPost].isLike = true;
      } else {
        state.posts[indexOfPost].likers_count =
            (int.parse(state.posts[indexOfPost].likers_count) - 1).toString();
        state.posts[indexOfPost].isLike = false;
      }
      state.posts.refresh();
    });
  }

  void removePost(BuildContext context, Post post) async {
    postRepo.removePost(post).then((value) {
      if (value) {
        var dataPosts = state.posts;
        var indexOfPost =
            state.posts.indexWhere((element) => element.id == post.id);
        if (indexOfPost != -1) {
          dataPosts.removeAt(indexOfPost);

          state.posts = dataPosts;
        }
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(S.of(context).removePostSuccessful),
        ));
      }
    });
  }

  void addFriend(BuildContext context, String idUserTarget, String type) async {
    print(type);
    friendRepo.addFriend(idUserTarget, type).then((value) {
      this.listenForUser();
      switch (type) {
        case "add":
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(S.of(context).friendRequestSentSuccessfully)));
          break;
        case "confirm":
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content:
                  Text(S.of(context).confirmationOfSuccessfulFriendRequest)));
          break;
        case "deny":
          ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text(S.of(context).friendRequestDenied)));
          break;
        case "cancel":
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(S.of(context).friendRequestHasBeenCanceled)));
          break;
        case "delete":
          ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text(S.of(context).successfulUnfriend)));
          break;
      }
    });
  }

  void updateShowBGHeader(double pixels) {
    state.showBackgroundHeader.value = pixels > 200;
  }

  void makeCall() async {
    // TODO xin cấp quyền
    Map<Permission, PermissionStatus> statuses = await [
      Permission.microphone,
    ].request();

    if (statuses[Permission.microphone].isGranted) {
      String userReceiverCall = state.user.value.code;
      String deceiveToken = state.user.value.deviceToken;

      Conversation _conversation;
      if (state.conversation != null) {
        _conversation = state.conversation;
      } else {
        ConversationEntity entity = await createConversation();
        _conversation = entity.toConversation;
      }

      NavigatorUtils.navigateToVoiceCallWidget(
          state.buildContext,
          ShortUserEntity(
              code: userReceiverCall,
              id: "",
              avatar: "",
              deceiveToken: deceiveToken),
          true,
          false,
          conversation: _conversation);
    } else {
      var error = "";
      print("TuanLA - $statuses[Permission.microphone]");
      if (statuses[Permission.microphone].isDenied ||
          statuses[Permission.microphone].isPermanentlyDenied) {
        error = "Bạn cần phải cấp quyền micro để thực hiện chức năng này";
      }

      Widget cancelButton = TextButton(
        child: Text("Cancel"),
        onPressed: () {
          Get.back();
        },
      );
      Widget settingButton = TextButton(
        child: Text("Open Settings"),
        onPressed: () async {
          await openAppSettings();
        },
      );
      AlertDialog alert = AlertDialog(
        title: Text("Thông báo"),
        content: Text(error),
        actions: [cancelButton, settingButton],
      );
      showDialog(
        context: Get.context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    }
  }

  void blockUser() async {
    try {
      final userModel.User data =
          await repository.blockUser(int.parse(state.userId.value));
      if (data != null) {
        state.user.value = data.copyWith(isBlocking: true);
        ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
          content: Text(S.of(Get.context).blockUserSuccessfully),
        ));
      } else {
        ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
          content: Text(S.of(Get.context).blockUserFailure),
        ));
      }
    } catch (e) {
      ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
        content: Text(S.of(Get.context).blockUserFailure),
      ));
    }
  }

  void unblockUser() async {
    try {
      final userModel.User data =
          await repository.unblockUser(int.parse(state.userId.value));
      if (data != null) {
        state.user.value = data;
        ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
          content: Text(S.of(Get.context).unblockUserSuccessfully),
        ));
      } else {
        ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
          content: Text(S.of(Get.context).unblockUserFailure),
        ));
      }
    } catch (e) {
      ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
        content: Text(S.of(Get.context).unblockUserFailure),
      ));
    }
  }

  void deletePost({int index}) async {
    final post = state.posts.elementAt(index);
    try {
      final result = await postRepo.removePost(post);
      if (result) {
        state.posts.removeAt(index);
        fetchInitialData(state.userId.value.toString());
      }
    } catch (e) {
      logger.e(e);
    }
  }

  void hidePost(int index) async {
    final Post post = state.posts.elementAt(index);
    try {
      final result = await newsfeedRepository.hidePost(post.id);
      if (result.success) {
        state.posts.removeAt(index);
        state.message.value = MessageStates.hidePostSuccessfully;
      }
    } catch (e) {
      print(e);
    }
  }

  void onReportPost(Post post) {}

  void getConversation() async {
    final userId = state.userId.value;
    if (userId == null) {
      return;
    }
    // state.conversation = await _conversationsDAO.getAllWithUserId(userId.toString());
    User _user = Get.find<LocalDataManager>().currentUser;
    List<ConversationEntity> result =
        await communicationRepository.findConversationByReceiver(
            receiver: state.user.value.code, sender: _user.code);
    if (result.isNotEmpty) state.conversation = result[0].toConversation;
    print('quan: ${state.conversation.id}');
  }

  Future<ConversationEntity> createConversation() async {
    List<User> users = [];
    List<ShortUserEntity> shortUsers = [];

    User currentUser = Get.find<LocalDataManager>().currentUser;
    Conversation _conversation = new Conversation(users);

    _conversation.id = UniqueKey().toString();
    _conversation.lastMessageTime =
        DateTime.now().toUtc().millisecondsSinceEpoch;
    _conversation.name = state.user.value.name;
    _conversation.readByUsers = [currentUser?.id ?? ""];
    _conversation.idAdmin = currentUser?.id ?? "";
    if (_conversation.visibleToUsers == null) {
      _conversation.visibleToUsers = [];
    }

    /// thêm short user bao gồm bản thân
    _conversation.users.add(currentUser);
    _conversation.visibleToUsers.add(currentUser.id);
    shortUsers.add(ShortUserEntity.fromUser(currentUser));

    /// thêm short user bao gồm user trong profile
    _conversation.users.add(state.user.value);
    _conversation.visibleToUsers.add(state.user.value.id);
    shortUsers.add(ShortUserEntity.fromUser(state.user.value));

    _conversation.visibleToUsers.sort((a, b) => a.compareTo(b));
    _conversation.type = "PEEK";
    String currentUserId = currentUser.id;
    User user = users.firstWhere((element) => element.id != currentUserId);
    String imgUrl = user.image.thumb ??
        "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
    _conversation.groupAvatar = imgUrl;
    _conversation.config = GroupConfigEntity(
      groupAvatar: imgUrl,
      shortUsers: shortUsers,
      groupBackground: ChangeBgController.DEFAULT_BACKGROUND,
      privateBackground: ChangeBgController.DEFAULT_BACKGROUND,
    );

    ConversationEntity result =
        await _socketRepository.createConversations(_conversation);

    /// save vào local database
    await _conversationsDAO.put(result.toConversation);

    /// save lại vào conversation hiện tại
    state.conversation = result.toConversation;

    /// notify đến danh sách conversation cập nhật lại
    GlobalEvent.instance.onCreateConversationResponse.add(result);
    return result;
  }

  ///Navigate
  void openChatRoom() async {
    try {
      if (!isLoggedIn) {
        NavigatorUtils.toLogin();
        return;
      } else {
        DialogHelper.showLoadingDialog();
        Conversation _conversation;
        if (state.conversation != null) {
          _conversation = state.conversation;
        } else {
          ConversationEntity entity = await createConversation();
          _conversation = entity.toConversation;
        }
        Get.back();
        NavigatorUtils.navigateToConversationDetailPage(_conversation);
      }
    } catch (e) {
      Get.back();
      print("TuanLA - $e");
    }
  }
}
