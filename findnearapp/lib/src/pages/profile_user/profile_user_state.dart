import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/enums/load_status.dart';
import 'package:findnear/src/models/media.dart';
import 'package:findnear/src/models/order.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/pages/newsfeed/newsfeed_state.dart';
import 'package:findnear/src/repository/chat_repository.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../models/user.dart' as userModel;
import '../../repository/friends_repository.dart' as friendRepo;
import '../../repository/post_repository.dart' as postRepo;
import '../../repository/user_repository.dart' as repository;

class ProfileUserState {
  RxList<Order> recentOrders = List<Order>.empty().obs;
  RxList<Post> posts = List<Post>.empty().obs;
  RxInt currentOffset = 0.obs;
  RxInt postCount = 0.obs;
  RxBool canLoadMore = true.obs;
  // RxBool isLoading = false.obs;
  Rx<LoadStatus> fetchPostsStatus = LoadStatus.initial.obs;

  GlobalKey<ScaffoldState> scaffoldKey;
  Rx<Post> post = Post.fromJSON({}).obs;
  // RxBool isLoadingPost = true.obs;
  RxList<Media> listImage = List<Media>.empty().obs;
  RxList<Media> listVideo = List<Media>.empty().obs;

  Rx<userModel.User> user = userModel.User.fromJSON({}).obs;
  RxBool showBackgroundHeader = false.obs;
  Rx<MessageStates> message = MessageStates.idle.obs;

  Conversation conversation;

  RouteArgument _routeArgument;

  ChatRepository _chatRepository = ChatRepository();

  RxBool loadUserInfoComplete = false.obs;

  BuildContext buildContext;

  RxString userId = "".obs;

  ProfileUserState() {}
}
