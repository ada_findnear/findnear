
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

class PreviewMultipleMediaWidget extends StatefulWidget {
  final int currentIndex;
  final List medias;
  PreviewMultipleMediaWidget({Key key, this.currentIndex, this.medias}) : super(key: key);
  @override
  _PreviewMultipleMediaWidgetState createState() => _PreviewMultipleMediaWidgetState();
}

class _PreviewMultipleMediaWidgetState extends State<PreviewMultipleMediaWidget> {
  int _currentIndex;
  PageController _pageController;
  List<String> listImage = [];

  @override
  void initState() {
    super.initState();
    _currentIndex = widget.currentIndex;
    _pageController = PageController(initialPage: _currentIndex);
    setListImage(widget.medias);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.close, color: Colors.white, ),
          onPressed: (){
            Navigator.pop(context, true);
          },
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.black,
        elevation: 0,

      ),
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    return PhotoViewGallery(
      pageOptions: listImage.map<PhotoViewGalleryPageOptions>(
          (String path) {
            return PhotoViewGalleryPageOptions(
              imageProvider: _getImage(path),
              minScale: PhotoViewComputedScale.contained*0.8,
              maxScale: PhotoViewComputedScale.covered*1.8
            );
          }
      ).toList(),
      scrollPhysics: const BouncingScrollPhysics(),
      pageController: _pageController,
    );
  }

  void setListImage(medias) {
    medias.forEach((element) {
      if (element.mime_type.indexOf("image") != -1) {
        setState(() {
          listImage.add(element.url,);
        });
      }
    });
  }

  ImageProvider _getImage(path) {
    if (path.contains("http")) {
      return NetworkImage(path);
    } else {
      return FileImage(File(path));
    }
  }
}