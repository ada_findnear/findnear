import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../controllers/settings_controller.dart';
import '../elements/CircularLoadingWidget.dart';
import '../elements/ProfileSettingsDialog.dart';
import '../helpers/helper.dart';
import '../repository/user_repository.dart';
import '../repository/settings_repository.dart' as settingRepo;

class SettingsWidget extends StatefulWidget {
  @override
  _SettingsWidgetState createState() => _SettingsWidgetState();
}

class _SettingsWidgetState extends StateMVC<SettingsWidget> {
  SettingsController _con;

  _SettingsWidgetState() : super(SettingsController()) {
    _con = controller;
  }

  @override
  void initState() {
    setState(() {
      _con.mapVisible = currentUser.value?.market?.visible ?? false;
      _con.positionFixed = currentUser.value?.market?.fixed_position ?? false;
      _con.address = currentUser.value?.market?.address ?? "";
    });
    super.initState();
  }

  Future<String> get _getLanguage => settingRepo.getDefaultLanguage(
      settingRepo.setting.value.mobileLanguage.value.languageCode);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<String>(
        future: _getLanguage,
        builder: (context, snapshot) {
          return Scaffold(
              key: _con.scaffoldKey,
              appBar: AppBar(
                backgroundColor: Colors.transparent,
                elevation: 0,
                centerTitle: true,
                title: Text(
                  S.of(context).profile_settings,
                  style: Theme.of(context)
                      .textTheme
                      .headline6
                      .merge(TextStyle(letterSpacing: 1.3)),
                ),
              ),
              body: currentUser.value?.id == null
                  ? CircularLoadingWidget(height: 500)
                  : SingleChildScrollView(
                      padding: EdgeInsets.symmetric(vertical: 7),
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 20),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Column(
                                    children: <Widget>[
                                      Text(
                                        currentUser.value?.name,
                                        textAlign: TextAlign.left,
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline3,
                                      ),
                                      Text(
                                        currentUser.value?.email,
                                        style:
                                            Theme.of(context).textTheme.caption,
                                      )
                                    ],
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                  ),
                                ),
                                SizedBox(
                                    width: 55,
                                    height: 55,
                                    child: InkWell(
                                      borderRadius: BorderRadius.circular(300),
                                      onTap: () {
                                        // Navigator.of(context).pushNamed('/Profile');
                                      },
                                      child: CircleAvatar(
                                        backgroundImage: NetworkImage(currentUser
                                                ?.value?.image?.thumb ??
                                            "${GlobalConfiguration().getValue('base_url')}images/image_default.png"),
                                      ),
                                    )),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: 20, vertical: 15),
                            decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.circular(6),
                              boxShadow: [
                                BoxShadow(
                                    color: Theme.of(context)
                                        .hintColor
                                        .withOpacity(0.15),
                                    offset: Offset(0, 3),
                                    blurRadius: 10)
                              ],
                            ),
                            child: ListView(
                              shrinkWrap: true,
                              primary: false,
                              children: <Widget>[
                                ListTile(
                                  leading: Icon(Icons.person_outline),
                                  title: Text(
                                    S.of(context).profile_settings,
                                    style:
                                    Theme.of(context).textTheme.bodyText1,
                                  ),
                                  trailing: ButtonTheme(
                                    padding: EdgeInsets.all(0),
                                    minWidth: 50.0,
                                    height: 25.0,
                                    child: ProfileSettingsDialog(
                                      user: currentUser.value,
                                      onChanged: () {
                                        _con.update(currentUser.value);
                                        //setState(() {});
                                      },
                                      fields: _con.fields,
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 50,
                                  child:  Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      SizedBox(width: 15,),
                                      Text(
                                        S.of(context).full_name,
                                        style:
                                        Theme.of(context).textTheme.bodyText2,
                                      ),
                                      SizedBox(width: 15,),
                                      Expanded(
                                        child: Text(
                                          Helper.limitString(
                                              currentUser.value.name, limit: 20),
                                          textAlign: TextAlign.right,
                                          style: TextStyle(
                                              color: Theme.of(context).focusColor),
                                        ),
                                      ),
                                      SizedBox(width: 15,),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 50,
                                  child:  Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      SizedBox(width: 15,),
                                      Text(
                                        S.of(context).email,
                                        style:
                                        Theme.of(context).textTheme.bodyText2,
                                      ),
                                      SizedBox(width: 15,),
                                      Expanded(
                                        child:Text(
                                          currentUser.value?.email,
                                          overflow: TextOverflow.ellipsis,
                                          textAlign: TextAlign.right,
                                          style: TextStyle(
                                              color: Theme.of(context).focusColor),
                                        ),
                                      ),
                                      SizedBox(width: 15,),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 50,
                                  child:  Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      SizedBox(width: 15,),
                                      Wrap(
                                        spacing: 8,
                                        crossAxisAlignment:
                                        WrapCrossAlignment.center,
                                        children: [
                                          Text(
                                            S.of(context).phone,
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText2,
                                          ),
                                          if (currentUser.value?.verifiedPhone ??
                                              false)
                                            Icon(
                                              Icons.check_circle_outline,
                                              color: Theme.of(context).accentColor,
                                              size: 22,
                                            )
                                        ],
                                      ),
                                      SizedBox(width: 15,),
                                      Expanded(
                                        child:Text(
                                          currentUser.value?.phone ?? "",
                                          overflow: TextOverflow.ellipsis,
                                          textAlign: TextAlign.right,
                                          style: TextStyle(
                                              color: Theme.of(context).focusColor),
                                        ),
                                      ),
                                      SizedBox(width: 15,),
                                    ],
                                  ),
                                ),
                                ListTile(
                                  onTap: () {},
                                  dense: true,
                                  title: Text(
                                    S.of(context).profession,
                                    style:
                                        Theme.of(context).textTheme.bodyText2,
                                  ),
                                  trailing: Text(
                                    Helper.limitString(
                                        currentUser.value?.field.name ??
                                            S.of(context).unknown, limit: 20),
                                    softWrap: false,
                                    style: TextStyle(
                                        color: Theme.of(context).focusColor),
                                  ),
                                ),
                                ListTile(
                                  onTap: () {},
                                  dense: true,
                                  title: Text(
                                    S.of(context).address,
                                    style:
                                        Theme.of(context).textTheme.bodyText2,
                                  ),
                                  trailing: Text(
                                    Helper.limitString(
                                        currentUser.value?.address ??
                                            S.of(context).unknown),
                                    overflow: TextOverflow.fade,
                                    softWrap: false,
                                    style: TextStyle(
                                        color: Theme.of(context).focusColor),
                                  ),
                                ),
                                ListTile(
                                  onTap: () {},
                                  dense: true,
                                  title: Text(
                                    S.of(context).about,
                                    style:
                                        Theme.of(context).textTheme.bodyText2,
                                  ),
                                  trailing: Text(
                                    Helper.limitString(currentUser.value?.bio),
                                    overflow: TextOverflow.fade,
                                    softWrap: false,
                                    style: TextStyle(
                                        color: Theme.of(context).focusColor),
                                  ),
                                ),
                                ListTile(
                                    onTap: () {},
                                    dense: true,
                                    title: Text(
                                      S.of(context).visibleOnMap,
                                      style:
                                          Theme.of(context).textTheme.bodyText2,
                                    ),
                                    trailing: Switch(
                                      onChanged: _con.toggleMapVisible,
                                      value: _con.mapVisible,
                                    )),
                                ListTile(
                                    onTap: () {},
                                    dense: true,
                                    title: Text(
                                      S.of(context).positionFixed,
                                      style:
                                          Theme.of(context).textTheme.bodyText2,
                                    ),
                                    trailing: Switch(
                                      onChanged: _con.togglePositionFixed,
                                      value: _con.positionFixed,
                                    )),
                                Visibility(
                                  visible: _con.positionFixed != null &&
                                      _con.positionFixed == true,
                                  child: ListTile(
                                    onTap: () {
                                      _con.updateFixedAddress();
                                    },
                                    dense: true,
                                    title: Text(
                                      _con.address != null
                                          ? _con.address
                                          : "Chọn vị trí",
                                      style:
                                          Theme.of(context).textTheme.bodyText2,
                                    ),
                                    trailing: Icon(Icons.chevron_right),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: 20, vertical: 15),
                            decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.circular(6),
                              boxShadow: [
                                BoxShadow(
                                    color: Theme.of(context)
                                        .hintColor
                                        .withOpacity(0.15),
                                    offset: Offset(0, 3),
                                    blurRadius: 10)
                              ],
                            ),
                            child: ListView(
                              shrinkWrap: true,
                              primary: false,
                              children: <Widget>[
                                ListTile(
                                  title: Row(
                                    children: <Widget>[
                                      Icon(
                                        Icons.settings_outlined,
                                        size: 22,
                                        color: Theme.of(context).focusColor,
                                      ),
                                      SizedBox(width: 10),
                                      Text(
                                        S.of(context).app_settings,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText2,
                                      ),
                                    ],
                                  ),
                                ),
                                ListTile(
                                  onTap: () {
                                    Navigator.of(context)
                                        .pushNamed('/Languages');
                                  },
                                  dense: true,
                                  title: Row(
                                    children: <Widget>[
                                      Icon(
                                        Icons.translate,
                                        size: 22,
                                        color: Theme.of(context).focusColor,
                                      ),
                                      SizedBox(width: 10),
                                      Text(
                                        S.of(context).languages,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText2,
                                      ),
                                    ],
                                  ),
                                  trailing: Text(
                                    snapshot.data == "vi" ? S.of(context).vietnamese : snapshot.data == "en" ? S.of(context).english : "",
                                    style: TextStyle(
                                        color: Theme.of(context).focusColor),
                                  ),
                                ),
                                ListTile(
                                  onTap: () {
                                    Navigator.of(context).pushNamed('/Help');
                                  },
                                  dense: true,
                                  title: Row(
                                    children: <Widget>[
                                      Icon(
                                        Icons.help_outline,
                                        size: 22,
                                        color: Theme.of(context).focusColor,
                                      ),
                                      SizedBox(width: 10),
                                      Text(
                                        S.of(context).help_support,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText2,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ));
        });
  }
}
