import 'dart:io';
import '../controllers/chat_controller.dart';
import '../models/conversation.dart';
import '../models/user.dart';
import '../pages/preview_media.dart';
import '../repository/user_repository.dart';
import 'package:image_picker/image_picker.dart';
import '../elements/MediaActionSheetWidget.dart';
import '../../generated/l10n.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import 'group_members.dart';

import 'group_add_user.dart';

class GroupEditWidget extends StatefulWidget {
  final Conversation conversation;
  final VoidCallback callback;

  GroupEditWidget({Key key, this.conversation, this.callback}) : super(key: key);

  @override
  _GroupEditWidgetState createState() => _GroupEditWidgetState();
}

class _GroupEditWidgetState extends StateMVC<GroupEditWidget> {
  MediaActionSheetWidget _mediaActionSheetWidget;
  String pathImage;
  String nameGroup;
  String keyword = '';
  ChatController chatController;
  List<User> listFriend = [];
  String memberGroupChat = '';
  var _groupNameController = TextEditingController();

  _GroupEditWidgetState() : super(ChatController()) {
    chatController = controller;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      pathImage = widget.conversation.groupAvatar;
      _groupNameController.text = widget.conversation.name;
      nameGroup = widget.conversation.name;
    });
  }

  @override
  Widget build(BuildContext context) {
    _mediaActionSheetWidget = MediaActionSheetWidget(context: context);
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios_outlined, color: Theme.of(context).hintColor),
            onPressed: () {
              Navigator.of(context).pop(false);
              widget.callback();
            },
          ),
          centerTitle: true,
          title: Text(S.of(context).editGroup)
        ),
        body: SizedBox.expand(
          child: Container(
            child: ListView(
              children: <Widget>[
                nameAndImageGroup(),
                settingOptional(),
              ],
            ),
          ),
        )
    );
  }

  Widget nameAndImageGroup() {
    return Container(
      padding: EdgeInsets.only(top: 10, bottom: 30),
      child: Column(
        children: [
          Stack(
            children: [
              ElevatedButton(
                onPressed: () {
                  updateImage();
                },
                child: pathImage != null ? new CircleAvatar(
                  foregroundImage: getImage(pathImage),
                  maxRadius: 50,
                )  : Icon(Icons.camera_alt_rounded),
                style: ElevatedButton.styleFrom(
                  shape: CircleBorder(),
                ),
              ),
              Positioned(
                top: 65,
                left: 85,
                child: IconButton(
                  onPressed: () {
                    updateImage();
                  },
                  icon: Icon(Icons.camera_alt_rounded),
                  iconSize: 15,
                ),
              )
            ],
          ),
          Align(
            alignment: Alignment.center,
            child: GestureDetector(
                onTap: () {
                  editGroupName(context);
                },
                child: Padding(
                    padding: const EdgeInsets.only(top: 10,left: 20.0, right: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Flexible(
                          fit: FlexFit.loose,
                          child: Padding(
                              padding: EdgeInsets.only(right: 5.0),
                              child: Text(
                                  widget.conversation.name,
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Theme.of(context).hintColor)
                              )
                          ),
                        ),
                        Icon(
                          Icons.edit,
                          size: 20,
                          color: Theme.of(context).accentColor,
                        ),
                      ],
                    )
                )
            ),
          )
        ],
      ),
    );
  }

  Widget settingOptional() {
    return Container(
      padding: EdgeInsets.only(bottom: 20),
      child: Column(
        children: [
          ElevatedButton(
            onPressed: () {
              widget.conversation.users.forEach((element) {
                setState(() {
                  memberGroupChat = memberGroupChat + ',' + element.id.toString();
                });
              });
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => GroupAddUserWidget(conversation: widget.conversation, memberGroupChat: memberGroupChat,))
              );
              print("Thêm thành viên");
            },
            child: Row (
              children: [
                Icon(
                  Icons.person_add_alt_outlined,
                  color: Theme.of(context).hintColor,
                ),
                SizedBox(width: 20, height: 45,),
                Text(
                  "Thêm thành viên",
                  style: TextStyle(
                    fontSize: 15,
                    color: Theme.of(context).hintColor
                  ),
                ),
              ],
            ),
            style: ElevatedButton.styleFrom( primary: Theme.of(context).primaryColor ),
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => GroupMembersWidget(conversation: widget.conversation))
              );
            },
            child: Row (
              children: [
                Icon(
                  Icons.group_outlined,
                  color: Theme.of(context).hintColor,
                ),
                SizedBox(width: 20, height: 45,),
                Text(
                  "Xem thành viên",
                  style: TextStyle(
                    fontSize: 15,
                    color: Theme.of(context).hintColor
                  )
                ),
              ],
            ),
            style: ElevatedButton.styleFrom(primary: Theme.of(context).primaryColor ),
          ),
          ElevatedButton(
            onPressed: () {
              outGroup();
            },
            child: Row (
              children: [
                Icon(
                  Icons.logout_rounded,
                  color: Colors.red,
                ),
                SizedBox(width: 20, height: 45,),
                Text(
                    "Rời khỏi nhóm",
                    style: TextStyle(
                        fontSize: 15,
                        color: Colors.red
                    )
                ),
              ],
            ),
            style: ElevatedButton.styleFrom( primary: Theme.of(context).primaryColor ),
          ),
          widget.conversation.idAdmin == currentUser.value?.id ? ElevatedButton(
            onPressed: () {
              removeGroup();
            },
            child: Row (
              children: [
                Icon(
                  Icons.delete_forever_outlined,
                  color: Colors.red,
                ),
                SizedBox(width: 20, height: 45,),
                Text(
                    "Xoá nhóm",
                    style: TextStyle(
                        fontSize: 15,
                        color: Colors.red
                    )
                ),
              ],
            ),
            style: ElevatedButton.styleFrom( primary: Theme.of(context).primaryColor ),
          ) : SizedBox()
        ],
      ),
    );
  }

  void updateImage() async {
    _mediaActionSheetWidget.showActionSheet(S.of(context).chooseImage, [
      S.of(context).viewPicture,
      S.of(context).takePhoto,
      S.of(context).chooseFromGrallery
    ], (int index) {
      if(index == 0) {
        _previewTopicPhoto(pathImage);
      }
      if(index == 1) {
        _openCamera((pickedFile) {
          pathImage = pickedFile.path;
          chatController.updateAvatar(conversation: widget.conversation, pathImage: pathImage);
        });
      }
      if(index == 2) {
        _openGrallery((pickedFile) {
          pathImage = pickedFile.path;
          chatController.updateAvatar(conversation: widget.conversation, pathImage: pathImage);
        });
      }
    });
  }

  void _previewTopicPhoto(url) {
    Navigator.of(context).push(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) => PreviewMediaWidget(photoPath: url),
      ),
    );
  }

  void _openCamera(OnPickImageCallback onPickImageCallback) {
    _mediaActionSheetWidget.pickImageDialog(ImageSource.camera,
            (PickedFile pickedFile) {
          print("PickerFile:${pickedFile.path}");
          setState(() {
            onPickImageCallback(pickedFile);
          });
        });
  }

  void _openGrallery(OnPickImageCallback onPickImageCallback) {
    _mediaActionSheetWidget.pickImageDialog(ImageSource.gallery,
            (PickedFile pickedFile) {
          print("PickerFile:${pickedFile.path}");
          setState(() {
            onPickImageCallback(pickedFile);
          });
        });
  }

  ImageProvider getImage(url) {
    if (url.contains("http")) {
      return NetworkImage(url);
    } else {
      return FileImage(File(url));
    }
  }

  showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = TextButton(
      child: Text(S.of(context).ok),
      onPressed: () {Navigator.pop(context, true);},
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(S.of(context).notifications),
      content: Text(S.of(context).groupNameIsNotEmpty),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  editGroupName(BuildContext context) {
    Widget cancelButton = TextButton(
      child: Text(S.of(context).cancel),
      onPressed: () {Navigator.pop(context, true);},
    );

    Widget okButton = TextButton(
      child: Text(S.of(context).save),
      onPressed: () {
        if (nameGroup == '' || nameGroup == null) {
          showAlertDialog(context);
        } else if (nameGroup != widget.conversation.name) {
          chatController.updateGroupName(conversation: widget.conversation, nameGroup: nameGroup);
          Navigator.pop(context, true);
        } else {
          Navigator.pop(context, true);
        }
      },
    );

    AlertDialog alert = AlertDialog(
      title: Text(S.of(context).nameGroup),
      content: TextField(
        controller: _groupNameController,
        decoration: InputDecoration(
          focusedBorder: OutlineInputBorder(
            borderSide: new BorderSide(color: Colors.black12, width: 0),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.black12, width: 1.0),
          ),
          border: OutlineInputBorder(),
          contentPadding: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
          hintText: S.of(context).nameGroup,
          hintStyle: TextStyle(fontSize: 18.0, color: Theme.of(context).hintColor),
          fillColor: Theme.of(context).primaryColor,
          filled: true,
        ),
        onChanged: (value) => {
          setState(() {
            nameGroup = value;
          })
        },
      ),
      actions: [
        cancelButton,
        okButton
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void outGroup() {
    showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: Text(S.of(context).confirm),
          content: Text(S.of(context).wantToGetOutTheGroup),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, 'Cancel'),
              child: Text(S.of(context).cancel),
            ),
            TextButton(
              onPressed: () => {
                Navigator.pop(context, 'OK'),
                chatController.removeUser(conversation: widget.conversation, userID: currentUser.value?.id ?? ''),
                backTwoScreen(),
              },
              child: Text(S.of(context).ok),
            ),
          ],
        )
    );
  }

  void removeGroup() {
    showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: Text(S.of(context).confirm),
          content: Text(S.of(context).wantToDeleteTheGroup),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, 'Cancel'),
              child: Text(S.of(context).cancel),
            ),
            TextButton(
              onPressed: () => {
                Navigator.pop(context, 'OK'),
                chatController.removeConversation(conversationId: widget.conversation.id),
                backTwoScreen(),
              },
              child: Text(S.of(context).ok),
            ),
          ],
        )
    );
  }

  void backTwoScreen() {
    int count = 0;
    Navigator.of(context).popUntil((_) => count++ >= 2);
  }
}