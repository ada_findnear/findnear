import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/settings/global_variable.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:webview_flutter/webview_flutter.dart';

class NeedLocationAccessWarningWidget extends StatefulWidget {
  @override
  State<NeedLocationAccessWarningWidget> createState() => _NeedLocationAccessWarningState();
}

class _NeedLocationAccessWarningState extends State<NeedLocationAccessWarningWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisSize: MainAxisSize.min,
      children: [
        Stack(
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(20, 40, 20, 20),
              margin: EdgeInsets.fromLTRB(30, 25, 30, 0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    S.of(context).access_location_description_title,
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      color: AppColors.black,
                    ),
                  ),
                  SizedBox(height: 10),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        S.of(context).access_location_description,
                        style: TextStyle(fontSize: 14, color: AppColors.black, fontWeight: FontWeight.w300, height: 1.4),
                      ),
                      Text(
                        S.of(context).access_location_description1,
                        style: TextStyle(fontSize: 14, color: AppColors.black, fontWeight: FontWeight.w300, height: 1.4),
                      ),
                      RichText(
                        textAlign: TextAlign.start,
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: S.of(context).access_location_description2,
                              style:
                              TextStyle(fontFamily: 'Quicksand',fontSize: 14, color: AppColors.black, fontWeight: FontWeight.w300, height: 1.4),
                            ),
                            TextSpan(
                              text: S.of(context).access_location_description3,
                              style:
                              TextStyle(fontFamily: 'Quicksand',fontSize: 14, color: AppColors.black, fontWeight: FontWeight.bold, height: 1.4),
                            ),
                          ],
                        ),
                      ),
                      RichText(
                        textAlign: TextAlign.start,
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: S.of(context).access_location_description4,
                              style:
                              TextStyle(fontFamily: 'Quicksand', fontSize: 14, color: AppColors.black, fontWeight: FontWeight.w300, height: 1.4),
                            ),
                            TextSpan(
                              text: S.of(context).access_location_description5,
                              style:
                              TextStyle(fontFamily: 'Quicksand', fontSize: 14, color: AppColors.black, fontWeight: FontWeight.bold, height: 1.4),
                            ),
                          ],
                        ),
                      ),
                      RichText(
                        textAlign: TextAlign.start,
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: S.of(context).access_location_description6,
                              style:
                              TextStyle(fontFamily: 'Quicksand', fontSize: 14, color: AppColors.black, fontWeight: FontWeight.w300, height: 1.4),
                            ),
                            TextSpan(
                              text: S.of(context).access_location_description7,
                              style:
                              TextStyle(fontFamily: 'Quicksand', fontSize: 14, color: AppColors.black, fontWeight: FontWeight.bold, height: 1.4),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Positioned.fill(
              top: 0,
              child: Align(
                alignment: Alignment.topCenter,
                child: AppImage(AppImages.icMail),
              ),
            ),
            Positioned(
              top: 30,
              right: 30,
              child: InkWell(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: AppImage('assets/img/ic_close.png'),
                ),
                onTap: () => Get.back(),
              ),
            )
          ],
        ),
      ],
    );
  }

  Widget _buildBodyWidget() {
    return Center(
      child: Text(
        "Để sử dụng tính năng này, vui lòng bật truy cập vị trí, vào Cài đặt > Findnear > Vị trí",
        style: TextStyle(
          fontFamily: 'Quicksand',
          fontSize: 16,
          color: Colors.black,
          fontWeight: FontWeight.w700,
        ),
      ),
    );
  }

}
