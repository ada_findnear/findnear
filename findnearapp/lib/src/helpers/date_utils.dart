import 'package:findnear/src/repository/settings_repository.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:flutter/material.dart';
import 'package:findnear/generated/l10n.dart';
import 'package:intl/intl.dart';

extension DateUtils on DateTime {
  String displayTimeAgo({BuildContext context}) {
    final year = this.year;
    final month = this.month;
    final day = this.day;
    final hour = this.hour;
    final minute = this.minute;

    final DateTime date = DateTime(year, month, day, hour, minute).add(DateTime.now().timeZoneOffset);
    final int diffInHours = DateTime.now().difference(date).inHours;

    String timeAgo = '';
    String timeUnit = '';
    int timeValue = 0;

    if (diffInHours < 1) {
      final diffInMinutes = DateTime.now().difference(date).inMinutes;
      timeValue = diffInMinutes;
      timeUnit = S.current.minute;
    } else if (diffInHours < 24) {
      timeValue = diffInHours;
      timeUnit = S.current.hour;
    } else if (diffInHours >= 24 && diffInHours < 24 * 7) {
      timeValue = (diffInHours / 24).floor();
      timeUnit = S.current.day;
    } else if (diffInHours >= 24 * 7 && diffInHours < 24 * 30) {
      timeValue = (diffInHours / (24 * 7)).floor();
      timeUnit = S.current.week;
    } else if (diffInHours >= 24 * 30 && diffInHours < 24 * 12 * 30) {
      timeValue = (diffInHours / (24 * 30)).floor();
      timeUnit = S.current.month;
    } else {
      timeValue = (diffInHours / (24 * 365)).floor();
      timeUnit = S.current.year;
    }

    timeAgo = timeValue.toString() + ' ' + timeUnit;
    if (Localizations.localeOf(context).countryCode == 'en') timeAgo += timeValue > 1 ? 's' : '';

    return "${timeAgo} ${S.current.ago}";
  }

  bool isSameDay(DateTime otherDay) {
    return year == otherDay.year && month == otherDay.month && day == otherDay.day;
  }
}

class AppDateFormatUtils {
  static const String HH_mm_yyyy_MM_dd = 'HH:mm | yyyy-MM-dd';

  static DateTime tryParseUTC(String utcString) {
    if (utcString == null || utcString.isEmpty) return null;
    var finalUTCString = utcString;
    if (!finalUTCString.endsWith('+00:00')) finalUTCString += '+00:00';
    return DateTime.tryParse(finalUTCString)?.toLocal();
  }

  static String formatUTCDate(String utcString, {String pattern = HH_mm_yyyy_MM_dd}) {
    final dateTime = tryParseUTC(utcString);
    var result = DateFormat(
      pattern,
      setting.value.mobileLanguage.value.toString(),
    ).format(dateTime);
    return result == null ? '' : result;
  }

  static String getVerboseDateTimeRepresentation(DateTime dateTime) {
    DateTime now = DateTime.now();
    DateTime justNow = DateTime.now().subtract(Duration(minutes: 1));
    DateTime localDateTime = dateTime.add(now.timeZoneOffset);
    if (!localDateTime.difference(justNow).isNegative) {
      return S.current.justNow;
    }
    String roughTimeString = DateFormat('jm', 'vi_VN').format(localDateTime);
    if (localDateTime.day == now.day && localDateTime.month == now.month && localDateTime.year == now.year) {
      return roughTimeString;
    }
    DateTime yesterday = now.subtract(Duration(days: 1));
    if (localDateTime.day == yesterday.day &&
        localDateTime.month == yesterday.month &&
        localDateTime.year == yesterday.year) {
      return S.current.yesterday_at + roughTimeString;
    }

    if (now.difference(localDateTime).inDays < 4) {
      String weekday = DateFormat('EEEE', 'vi_VN').format(localDateTime);
      return '$weekday, $roughTimeString';
    }
    return '${DateFormat('yMd', 'vi_VN').format(localDateTime)}, $roughTimeString';
  }
}

