import 'dart:async';
import 'dart:io';
import 'dart:ui' as ui;
import 'dart:typed_data';
import 'package:findnear/src/commons/app_images.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart' hide Response;
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart';

class MapsMarker {

  static Future<ui.Image> getImageFromPath(String imageUrl) async {
    final Response response = await get(Uri.parse(imageUrl));
    Uint8List imageBytes = response.bodyBytes;

    final Completer<ui.Image> completer = new Completer();

    ui.decodeImageFromList(imageBytes, (ui.Image img) {
      return completer.complete(img);
    });

    return completer.future;
  }

  static Future<BitmapDescriptor> getMarkerIcon(String imageUrl, {Color borderColor = null}) async {
    final devicePixelRatio = Get.context.devicePixelRatio;
    final size = Size(48 * devicePixelRatio, 48 * devicePixelRatio);
    final ui.PictureRecorder pictureRecorder = ui.PictureRecorder();
    final Canvas canvas = Canvas(pictureRecorder);

    final Radius radius = Radius.circular(size.width / 2);

    final double shadowWidth = 3.0 * devicePixelRatio;

    final Paint borderPaint = Paint()..color = Colors.white;
    final double borderWidth = 2.0 * devicePixelRatio;

    final double imageOffset = shadowWidth + borderWidth;

    if (borderColor != null) {
      final Paint shadowPaint = Paint()..color = borderColor;
      final Paint backgroundPaint = Paint()..color = Theme.of(Get.context).primaryColor;
      // Add shadow circle
      canvas.drawRRect(
          RRect.fromRectAndCorners(
            Rect.fromLTWH(
                0.0,
                0.0,
                size.width,
                size.height
            ),
            topLeft: radius,
            topRight: radius,
            // bottomLeft: radius,
            bottomRight: radius,
          ),
          backgroundPaint);

      // Add border circle
      canvas.drawRRect(
          RRect.fromRectAndCorners(
            Rect.fromLTWH(
                shadowWidth,
                shadowWidth,
                size.width - (shadowWidth * 2),
                size.height - (shadowWidth * 2)
            ),
            topLeft: radius,
            topRight: radius,
            bottomLeft: radius,
            bottomRight: radius,
          ),
          shadowPaint);
    }
    // Oval for the image
    Rect oval = Rect.fromLTWH(
        imageOffset,
        imageOffset,
        size.width - (imageOffset * 2),
        size.height - (imageOffset * 2)
    );

    // Add path for oval image
    canvas.clipPath(Path()
      ..addOval(oval));

    // Add image
    ui.Image image;
    if (imageUrl != null) {
      image = await getImageFromPath(imageUrl); // Alternatively use your own method to get the image
    } else {
      final bytes = await rootBundle.load(AppImages.icAvatarPlaceholder);
      final codec = await ui.instantiateImageCodec(Uint8List.view(bytes.buffer));
      image = (await codec.getNextFrame()).image;
    }
    if (image != null) {
      paintImage(
          canvas: canvas, image: image, rect: oval, fit: BoxFit.fitWidth);
    }

    // Convert canvas to image
    final ui.Image markerAsImage = await pictureRecorder.endRecording().toImage(
        size.width.toInt(),
        size.height.toInt()
    );

    // Convert image to bytes
    final ByteData byteData = await markerAsImage.toByteData(format: ui.ImageByteFormat.png);
    final Uint8List uint8List = byteData.buffer.asUint8List();

    return BitmapDescriptor.fromBytes(uint8List);
  }

  static Future<BitmapDescriptor> getDatingMarkerIcon(String imageUrl, {bool isGreen = false}) async {
    final devicePixelRatio = Get.context.devicePixelRatio;
    final size = Size(44.4 * devicePixelRatio, 38.5 * devicePixelRatio);
    final ui.PictureRecorder pictureRecorder = ui.PictureRecorder();
    final Canvas canvas = Canvas(pictureRecorder);

    final double imageOffset = 2.5 * devicePixelRatio;

    // Oval for the image
    Rect heartRect = Rect.fromLTWH(0, 0, size.width, size.height);
    Rect oval = Rect.fromLTWH(
        imageOffset,
        imageOffset,
        size.width - (imageOffset * 2),
        size.height - (imageOffset * 2)
    );

    // Add path for oval image
    final heartClip = getHeartClip(size);
    canvas.clipPath(heartClip);

    // Add image
    ui.Image image;
    if (imageUrl != null) {
      image = await getImageFromPath(imageUrl); // Alternatively use your own method to get the image
    } else {
      final bytes = await rootBundle.load(AppImages.icAvatarPlaceholder);
      final codec = await ui.instantiateImageCodec(Uint8List.view(bytes.buffer));
      image = (await codec.getNextFrame()).image;
    }
    if (image != null) {
      paintImage(
          canvas: canvas, image: image, rect: oval, fit: BoxFit.fitWidth);
    }

    //Paint the heart overlay
    final heartSvgPath = (isGreen ?? false) ? AppImages.icDatingOverlayGreen : AppImages.icDatingOverlayRed;
    final heartSvgBytes = await rootBundle.load(heartSvgPath);
    final DrawableRoot svgRoot = await svg.fromSvgBytes(Uint8List.view(heartSvgBytes.buffer), heartSvgPath);

    svgRoot.scaleCanvasToViewBox(canvas, size);
    svgRoot.draw(canvas, heartRect);

    // Convert canvas to image
    final ui.Image markerAsImage = await pictureRecorder.endRecording().toImage(
        size.width.toInt(),
        size.height.toInt()
    );

    // Convert image to bytes
    final ByteData byteData = await markerAsImage.toByteData(format: ui.ImageByteFormat.png);
    final Uint8List uint8List = byteData.buffer.asUint8List();

    return BitmapDescriptor.fromBytes(uint8List);
  }

  static Path getHeartClip(Size size) {
    Path path = Path();
    final double _xScaling = size.width / 33.654;
    final double _yScaling = size.height / 29.189;
    path.moveTo(16.826999999999998 * _xScaling,29.188999999999993 * _yScaling);
    path.cubicTo(16.556763422591644 * _xScaling,29.188970397065873 * _yScaling,16.294376795563124 * _xScaling,29.098104474362742 * _yScaling,16.081999999999994 * _xScaling,28.930999999999983 * _yScaling,);
    path.cubicTo(9.753 * _xScaling,24.81099999999998 * _yScaling,4.788999999999987 * _xScaling,21.658999999999992 * _yScaling,2.692999999999998 * _xScaling,18.581999999999994 * _yScaling,);
    path.cubicTo(2.692999999999998 * _xScaling,18.581999999999994 * _yScaling,2.378 * _xScaling,18.115999999999985 * _yScaling,2.378 * _xScaling,18.115999999999985 * _yScaling,);
    path.cubicTo(0.843551357161715 * _xScaling,15.617831511261159 * _yScaling,0.021244430596567554 * _xScaling,12.747710611537997 * _yScaling,0 * _xScaling,9.815999999999974 * _yScaling,);
    path.cubicTo(-0.0812702342963263 * _xScaling,4.478624243357473 * _yScaling,4.177681445606822 * _xScaling,0.08494390801021723 * _yScaling,9.515 * _xScaling,0 * _yScaling,);
    path.cubicTo(12.333652792590357 * _xScaling,0.007170705562288049 * _yScaling,14.999594754869264 * _xScaling,1.2817555641825038 * _yScaling,16.775000000000006 * _xScaling,3.4710000000000036 * _yScaling,);
    path.cubicTo(16.775000000000006 * _xScaling,3.4710000000000036 * _yScaling,16.828000000000003 * _xScaling,3.5360000000000014 * _yScaling,16.828000000000003 * _xScaling,3.5360000000000014 * _yScaling,);
    path.cubicTo(18.602268885589368 * _xScaling,1.3091174066991869 * _yScaling,21.29172648687036 * _xScaling,0.008348900951460791 * _yScaling,24.138999999999996 * _xScaling,0 * _yScaling,);
    path.cubicTo(29.47711168149965 * _xScaling,0.0849394164679893 * _yScaling,33.7363862293594 * _xScaling,4.479848303219086 * _yScaling,33.653999999999996 * _xScaling,9.818000000000012 * _yScaling,);
    path.cubicTo(33.598839624042455 * _xScaling,12.805881574483323 * _yScaling,32.72663990648843 * _xScaling,15.721629323022228 * _yScaling,31.132000000000005 * _xScaling,18.249000000000024 * _yScaling,);
    path.cubicTo(31.132000000000005 * _xScaling,18.249000000000024 * _yScaling,31.087999999999994 * _xScaling,18.315000000000026 * _yScaling,31.087999999999994 * _xScaling,18.315000000000026 * _yScaling,);
    path.cubicTo(31.087999999999994 * _xScaling,18.315000000000026 * _yScaling,30.906000000000006 * _xScaling,18.583000000000027 * _yScaling,30.906000000000006 * _xScaling,18.583000000000027 * _yScaling,);
    path.cubicTo(28.80600000000001 * _xScaling,21.660000000000025 * _yScaling,23.900000000000006 * _xScaling,24.811000000000035 * _yScaling,17.570999999999998 * _xScaling,28.932000000000016 * _yScaling,);
    path.cubicTo(17.358761339919837 * _xScaling,29.098534701947244 * _yScaling,17.096775934372147 * _xScaling,29.189031920319252 * _yScaling,16.826999999999998 * _xScaling,29.188999999999993 * _yScaling,);
    path.cubicTo(16.826999999999998 * _xScaling,29.188999999999993 * _yScaling,16.826999999999998 * _xScaling,29.188999999999993 * _yScaling,16.826999999999998 * _xScaling,29.188999999999993 * _yScaling,);
    return path;
  }

}