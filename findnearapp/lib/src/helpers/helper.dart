import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:dio/dio.dart';
import 'package:findnear/src/elements/MediaActionSheetWidget.dart';
import 'package:findnear/src/services/cache_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:html/parser.dart';
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';

import '../../generated/l10n.dart';
import '../elements/CircularLoadingWidget.dart';
import '../models/market.dart';
import '../repository/settings_repository.dart';
import 'app_config.dart' as config;
import 'maps_marker.dart';

class Helper {
  BuildContext context;
  DateTime currentBackPressTime;

  Helper.of(BuildContext _context) {
    this.context = _context;
  }

  // for mapping data retrieved form json array
  static getData(Map<String, dynamic> data) {
    return data['data'] ?? [];
  }

  static int getIntData(Map<String, dynamic> data) {
    return (data['data'] as int) ?? 0;
  }

  static bool getBoolData(Map<String, dynamic> data) {
    return (data['data'] as bool) ?? false;
  }

  static bool getBool(dynamic data) {
    return (data is bool) ? data : (data is int) ? data == 1 : true;
  }

  static getObjectData(Map<String, dynamic> data) {
    return data['data'] ?? new Map<String, dynamic>();
  }

  static Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

  static Future<Uint8List> getBytesFromUrl(String path, int width) async {
    final response = await get(Uri.parse(path));
    ui.Codec codec =
        await ui.instantiateImageCodec(response.bodyBytes, targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

  /// If isDatingMarker == true => don't care displayCareer
  static Future<List<Marker>> getMarker(Market market,
      {VoidCallback onPressed,
      bool displayCareer,
      bool isDatingMarker = false}) async {
    Map<String, dynamic> res = market.toMap();
    final cacheService = Get.find<CacheService>();
    final field = cacheService.fields.firstWhere(
        (element) => element.id == market?.firstField?.id,
        orElse: () => null);
    // final icon = isDatingMarker
    //     ? await MapsMarker.getDatingMarkerIcon(
    //         market?.owner?.image?.thumb,
    //         isGreen: market.owner.isActive,
    //       )
    //     : await MapsMarker.getMarkerIcon(
    //         displayCareer ? field?.image?.thumb : market?.owner?.image?.thumb,
    //         borderColor:
    //             market.owner.isFriend ? Colors.lightGreenAccent : Colors.red,
    //       );
    final Marker marker = Marker(
      onTap: onPressed,
      markerId: MarkerId(res['id']),
      icon: !isDatingMarker
          ? await MapsMarker.getMarkerIcon(
              displayCareer ? field?.image?.thumb : market?.owner?.image?.thumb,
              borderColor: market.owner.isFriend ? Colors.green : Colors.red,
            )
          : await MapsMarker.getDatingMarkerIcon(
              displayCareer ? field?.image?.thumb : market?.owner?.image?.thumb,
              isGreen: market.owner.isFriend),
      // icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
      anchor: Offset(0.5, 0.5),
      position: LatLng(
        double.parse(res['latitude'] != null ? res['latitude'] : '0'),
        double.parse(res['longitude'] != null ? res['longitude'] : '0'),
      ),
      consumeTapEvents: true,
    );

    // final Marker subMarker = Marker(
    //   onTap: () async {
    //     logic.onClickMarker(market);
    //   },
    //   markerId: MarkerId("sub-" + res['id']),
    //   icon: await MapsMarker.getMarkerIcon(
    //       market.firstField.image.thumb, Size(70, 70)),
    //   anchor: Offset(-0.25, 1.15),
    //   position: LatLng(
    //       double.parse(res['latitude'] != null ? res['latitude'] : '0'),
    //       double.parse(res['longitude'] != null ? res['longitude'] : '0')),
    //   zIndex: 1,
    // );

    List<Marker> list = <Marker>[];
    list.add(marker);
    // list.add(subMarker);

    return list;
  }

  static Future<Marker> buildMarkerFromMarket(Market market,
      {bool isDatingMarker = false}) async {
    Map<String, dynamic> res = market.toMap();
    final Marker marker = Marker(
      onTap: () {},
      markerId: MarkerId(res['id']),
      icon: !isDatingMarker
          ? await MapsMarker.getMarkerIcon(
              market.owner.image.thumb,
              borderColor: market.owner.isFriend ? Colors.green : Colors.red,
              // borderColor: market.owner.isFriend ? Colors.lightGreenAccent : Colors.red,
            )
          : await MapsMarker.getDatingMarkerIcon(
              market.owner.image.thumb,
              isGreen: market.owner.isFriend,
            ),
      anchor: Offset(0.5, 0.5),
      position: LatLng(
        double.parse(res['latitude'] != null ? res['latitude'] : '0'),
        double.parse(res['longitude'] != null ? res['longitude'] : '0'),
      ),
    );
    return marker;
  }

  static Future<Marker> getMyPositionMarker(
      double latitude, double longitude) async {
    final Uint8List markerIcon =
        await getBytesFromAsset('assets/img/my_marker.png', 120);
    final Marker marker = Marker(
        markerId: MarkerId(Random().nextInt(100).toString()),
        icon: BitmapDescriptor.fromBytes(markerIcon),
        anchor: Offset(0.5, 0.5),
        position: LatLng(latitude, longitude));

    return marker;
  }

  static List<Icon> getStarsList(double rate, {double size = 18}) {
    var list = <Icon>[];
    list = List.generate(rate.floor(), (index) {
      return Icon(Icons.star, size: size, color: Color(0xFFFFB24D));
    });
    if (rate - rate.floor() > 0) {
      list.add(Icon(Icons.star_half, size: size, color: Color(0xFFFFB24D)));
    }
    list.addAll(
        List.generate(5 - rate.floor() - (rate - rate.floor()).ceil(), (index) {
      return Icon(Icons.star_border, size: size, color: Color(0xFFFFB24D));
    }));
    return list;
  }

  static Widget getPrice(double myPrice, BuildContext context,
      {TextStyle style, String zeroPlaceholder = '-'}) {
    if (style != null) {
      style = style.merge(TextStyle(fontSize: style.fontSize + 2));
    }
    try {
      if (myPrice == 0) {
        return Text('-', style: style ?? Theme.of(context).textTheme.subtitle1);
      }
      return RichText(
        softWrap: false,
        overflow: TextOverflow.fade,
        maxLines: 1,
        text: setting.value?.currencyRight != null &&
                setting.value?.currencyRight == false
            ? TextSpan(
                text: setting.value?.defaultCurrency,
                style: style == null
                    ? Theme.of(context).textTheme.subtitle1.merge(
                          TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: Theme.of(context)
                                      .textTheme
                                      .subtitle1
                                      .fontSize -
                                  6),
                        )
                    : style.merge(TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: style.fontSize - 6)),
                children: <TextSpan>[
                  TextSpan(
                      text: myPrice.toStringAsFixed(
                              setting.value?.currencyDecimalDigits) ??
                          '',
                      style: style ?? Theme.of(context).textTheme.subtitle1),
                ],
              )
            : TextSpan(
                text: myPrice.toStringAsFixed(
                        setting.value?.currencyDecimalDigits) ??
                    '',
                style: style ?? Theme.of(context).textTheme.subtitle1,
                children: <TextSpan>[
                  TextSpan(
                    text: setting.value?.defaultCurrency,
                    style: style == null
                        ? Theme.of(context).textTheme.subtitle1.merge(
                              TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: Theme.of(context)
                                          .textTheme
                                          .subtitle1
                                          .fontSize -
                                      6),
                            )
                        : style.merge(TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: style.fontSize - 6)),
                  ),
                ],
              ),
      );
    } catch (e) {
      return Text('');
    }
  }

  static String getDistance(double distance, String unit) {
    String _unit = setting.value.distanceUnit;
    if (_unit == 'km') {
      distance *= 1.60934;
    }
    return distance != null ? distance.toStringAsFixed(2) + " " + unit : "";
  }

  static String skipHtml(String htmlString) {
    try {
      var document = parse(htmlString);
      String parsedString = parse(document.body.text).documentElement.text;
      return parsedString;
    } catch (e) {
      return '';
    }
  }

  static Html applyHtml(context, String html, {TextStyle style}) {
    return Html(
      data: html ?? '',
      style: {
        "*": Style(
          padding: EdgeInsets.all(0),
          margin: EdgeInsets.all(0),
          color: Theme.of(context).hintColor,
          fontSize: FontSize(16.0),
          display: Display.INLINE_BLOCK,
          width: config.App(context).appWidth(100),
        ),
        "h4,h5,h6": Style(
          fontSize: FontSize(18.0),
        ),
        "h1,h2,h3": Style(
          fontSize: FontSize.xLarge,
        ),
        "br": Style(
          height: 0,
        ),
        "p": Style(
          fontSize: FontSize(16.0),
        )
      },
    );
  }

  static OverlayEntry overlayLoader(context) {
    OverlayEntry loader = OverlayEntry(builder: (context) {
      final size = MediaQuery.of(context).size;
      return Positioned(
        height: size.height,
        width: size.width,
        top: 0,
        left: 0,
        child: Material(
          color: Theme.of(context).primaryColor.withOpacity(0.85),
          child: CircularLoadingWidget(height: 200),
        ),
      );
    });
    return loader;
  }

  static hideLoader(OverlayEntry loader) {
    Timer(Duration(milliseconds: 500), () {
      try {
        loader?.remove();
      } catch (e) {}
    });
  }

  static String limitString(String text,
      {int limit = 24, String hiddenText = "..."}) {
    return text.substring(0, min<int>(limit, text.length)) +
        (text.length > limit ? hiddenText : '');
  }

  static Uri getUri(String path) {
    String _path = Uri.parse(GlobalConfiguration().getValue('base_url')).path;
    if (!_path.endsWith('/')) {
      _path += '/';
    }
    Uri uri = Uri(
        scheme: Uri.parse(GlobalConfiguration().getValue('base_url')).scheme,
        host: Uri.parse(GlobalConfiguration().getValue('base_url')).host,
        port: Uri.parse(GlobalConfiguration().getValue('base_url')).port,
        path: _path + path);
    return uri;
  }

  Color getColorFromHex(String hex) {
    if (hex.contains('#')) {
      return Color(int.parse(hex.replaceAll("#", "0xFF")));
    } else {
      return Color(int.parse("0xFF" + hex));
    }
  }

  static BoxFit getBoxFit(String boxFit) {
    switch (boxFit) {
      case 'cover':
        return BoxFit.cover;
      case 'fill':
        return BoxFit.fill;
      case 'contain':
        return BoxFit.contain;
      case 'fit_height':
        return BoxFit.fitHeight;
      case 'fit_width':
        return BoxFit.fitWidth;
      case 'none':
        return BoxFit.none;
      case 'scale_down':
        return BoxFit.scaleDown;
      default:
        return BoxFit.cover;
    }
  }

  static AlignmentDirectional getAlignmentDirectional(
      String alignmentDirectional) {
    switch (alignmentDirectional) {
      case 'top_start':
        return AlignmentDirectional.topStart;
      case 'top_center':
        return AlignmentDirectional.topCenter;
      case 'top_end':
        return AlignmentDirectional.topEnd;
      case 'center_start':
        return AlignmentDirectional.centerStart;
      case 'center':
        return AlignmentDirectional.topCenter;
      case 'center_end':
        return AlignmentDirectional.centerEnd;
      case 'bottom_start':
        return AlignmentDirectional.bottomStart;
      case 'bottom_center':
        return AlignmentDirectional.bottomCenter;
      case 'bottom_end':
        return AlignmentDirectional.bottomEnd;
      default:
        return AlignmentDirectional.bottomEnd;
    }
  }

  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Fluttertoast.showToast(msg: S.of(context).tapAgainToLeave);
      return Future.value(false);
    }
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');

    return Future.value(true);
  }

  String trans({String text, String userName = ""}) {
    switch (text) {
      case "App\\Notifications\\StatusChangedOrder":
        return S.of(context).order_status_changed;
      case "App\\Notifications\\NewOrder":
        return S.of(context).new_order_from_client;
      case "km":
        return S.of(context).km;
      case "mi":
        return S.of(context).mi;
      case "App\\Notifications\\CommentPost":
        return userName + S.of(context).commentedYourPost;
      case "App\\Notifications\\LikePost":
        return userName + S.of(context).likedYourPost;
      case "App\\Notifications\\RequestAddFriend":
        return userName + S.of(context).sentYouFriendRequest;
      case "App\\Notifications\\AgreeAddFriend":
        return userName + S.of(context).acceptedYourFriendRequest;
      default:
        return "";
    }
  }

  String getMessage(message, {senderId = null, isMine = false}) {
    var messageDecode = -1;
    try {
      bool isCallMessage = jsonDecode(message)['typeEndCall'] != null;
      // if (senderId == null) {
      //   return S.of(context).audioCall;
      // }
      if (isCallMessage) {
        switch (jsonDecode(message)['typeEndCall']) {
          case 0:
            return S.of(context).audioCall +
                " (" +
                jsonDecode(message)['duration'] +
                ")";
          case 1:
            return isMine
                ? S.of(context).userDenyCall
                : S.of(context).youDenyCall;
          case 2:
            return isMine
                ? S.of(context).userMissCall
                : S.of(context).missedCall;
        }
      }
    } catch (e) {
      return message;
    }

    try {
      messageDecode = jsonDecode(message)['typeSystem'];
    } catch (e) {
      return message;
    }

    if (messageDecode != -1) {
      switch (jsonDecode(message)['typeSystem']) {
        case 0:
          return jsonDecode(message)['text'] + S.of(context).createdGroup;
          break;
        case 1:
          return jsonDecode(message)['text'] + S.of(context).hasAddedToTheGroup;
          break;
        case 2:
          return jsonDecode(message)['text'] +
              S.of(context).hasDeleteFromTheGroup;
          break;
        case 3:
          return jsonDecode(message)['text'] + S.of(context).hasLeftTheGroup;
          break;
        case 4:
          return jsonDecode(message)['text'] + S.of(context).sentAPhoto;
          break;
        case 5:
          return jsonDecode(message)['text'] + S.of(context).sentAVideo;
      }
    } else {
      return message;
    }

    return "";
  }

  MediaActionSheetWidget _mediaActionSheetWidget =
      MediaActionSheetWidget(context: Get.context);

  void openCamera(Function onPickImageCallback) {
    _mediaActionSheetWidget.pickImageDialog(ImageSource.camera,
        (PickedFile pickedFile) {
      onPickImageCallback(pickedFile);
    });
  }

  void openGallery(Function onPickImageCallback) {
    _mediaActionSheetWidget.pickImageDialog(ImageSource.gallery,
        (PickedFile pickedFile) {
      onPickImageCallback(pickedFile);
    });
  }

  void openChooseImage({Function onCameraChoose, Function onGalleryChoose}) {
    _mediaActionSheetWidget.showActionSheet(S.of(context).chooseImage, [
      S.of(context).takePhoto,
      S.of(context).chooseFromGrallery
    ], (int index) {
      if (index == 0) {
        openCamera((pickedFile) {
          if (pickedFile.path != null) {
            String imgUrl = pickedFile.path;
            onCameraChoose(imgUrl);
          }
        });
      }
      if (index == 1) {
        openGallery((pickedFile) async {
          if (pickedFile.path != null) {
            String imgUrl = pickedFile.path;
            onGalleryChoose(imgUrl);
          }
        });
      }
    });
  }

  static void showErrorMessage(dynamic e) {
    print(e);
    if (e is DioError) {
      print(e.response);
    }

    var errorMsg = "Something went wrong";
    if (e is DioError) {
      final response = e.response;
      if (response != null) {
        final errorData = response.data;
        if (errorData is Map<String, dynamic>) {
          if (errorData["error"] is String) {
            errorMsg = errorData["error"];
          }
        }
      }
    }
    ScaffoldMessenger.of(Get.key.currentContext).showSnackBar(SnackBar(
      content: Text(errorMsg),
    ));
  }
}
