import 'dart:convert';
import 'dart:io';

import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class AuthenticatedHttpClient extends http.BaseClient {
  GlobalKey<ScaffoldState> scaffoldKey;

  @override
  Future<http.StreamedResponse> send(http.BaseRequest request) async {
    request.headers.addAll({HttpHeaders.contentTypeHeader: 'application/json'});
    request.headers.addAll({HttpHeaders.acceptHeader: 'application/json'});
    _logRequest(method: "SEND", url: request.url, headers: request.headers);
    final res = await request.send();
    this.checkErrorAuthenticate(res.statusCode);
    await _logResponse2(res);
    return res;
  }

  @override
  Future<http.Response> put(url,
      {Map<String, String> headers, body, Encoding encoding}) async {
    _logRequest(method: "PUT", url: url, headers: headers);
    final res =
        await super.put(url, headers: headers, body: body, encoding: encoding);
    this.checkErrorAuthenticate(res.statusCode);
    _logResponse(res);
    this.checkErrorAuthenticate(res.statusCode);
    return res;
  }

  @override
  Future<http.Response> post(url,
      {Map<String, String> headers, body, Encoding encoding}) async {
    _logRequest(method: "POST", url: url, headers: headers);
    final res =
        await super.post(url, headers: headers, body: body, encoding: encoding);
    this.checkErrorAuthenticate(res.statusCode);
    _logResponse(res);
    this.checkErrorAuthenticate(res.statusCode);
    return res;
  }

  @override
  Future<http.Response> delete(Uri url,
      {Map<String, String> headers, Object body, Encoding encoding}) async {
    _logRequest(method: "DELETE", url: url, headers: headers);
    final res = await super.delete(url, headers: headers);
    this.checkErrorAuthenticate(res.statusCode);
    _logResponse(res);
    this.checkErrorAuthenticate(res.statusCode);
    return res;
  }

  @override
  Future<http.Response> get(url, {Map<String, String> headers}) async {
    _logRequest(method: "GET", url: url, headers: headers);
    final res = await super.get(url, headers: headers);
    _logResponse(res);
    this.checkErrorAuthenticate(res.statusCode);
    return res;
  }

  void checkErrorAuthenticate(int statusCode) {
    if (statusCode == 401 || statusCode == 403 || statusCode == 404) {
      logout().then((value) {
        currentUser.value = null;
        setCurrentUser(null);
        this.onLogOut(scaffoldKey?.currentContext);
      });
    }
  }

  void _logRequest({String method, Uri url, Map<String, String> headers}) {
    logger.log(
        "✈️ REQUEST[$method] => PATH: $url \n headers: ${json.encode(headers)}",
        printFullText: true);
  }

  void _logResponse(http.Response response) {
    final statusCode = response.statusCode;
    final uri = response.request.url.toString();
    final data = jsonEncode(response.body);
    logger.log("✅ RESPONSE[$statusCode] => PATH: $uri\n DATA: $data");
  }

  void _logResponse2(http.StreamedResponse response) async {
    final statusCode = response.statusCode;
    final uri = response.request.url.toString();
    final data = ""; //Todo
    logger.log("✅ RESPONSE[$statusCode] => PATH: $uri\n DATA: $data");
  }

  void onLogOut(BuildContext context) {
    if (context != null) {
      Get.key.currentState.pushNamedAndRemoveUntil(
          '/Pages', (Route<dynamic> route) => false,
          arguments: 2);
    }
  }
}
