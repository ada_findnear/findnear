extension FormatDuration on Duration {
  String toFormatString({String separatorSymbol = ":"}) {
    if (inSeconds <= 0) return "00${separatorSymbol}00";

    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitHours = twoDigits(inHours);
    String twoDigitMinutes = twoDigits(inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(inSeconds.remainder(60));

    if (twoDigitHours == "00") {
      return "$twoDigitMinutes$separatorSymbol$twoDigitSeconds";
    } else {
      return "$twoDigitHours$separatorSymbol$twoDigitMinutes$separatorSymbol$twoDigitSeconds";
    }
  }
}
