import 'package:images_picker/images_picker.dart';

extension ValidateMedia on Media {
  bool get isVideo {
    return path.toLowerCase().endsWith(".mov") || path.toLowerCase().endsWith(".mp4");
  }
}

extension MediaPath on String {
  bool get isVideoPath {
    return toLowerCase().endsWith(".mov") || toLowerCase().endsWith(".mp4");
  }
}
