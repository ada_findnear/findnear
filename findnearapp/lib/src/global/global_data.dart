
import '../models/entities/offer_call_entity.dart';
import '../models/twilio_token.dart';

class GlobalData {
  GlobalData._privateConstructor() {}

  static final GlobalData instance = GlobalData._privateConstructor();
  bool isCallShowing = false;

  OfferCallEntity offer = null;
  bool isAcceptCallKitIos = false;

  TwilioToken twilioToken;

  String documentPath;
}
