import 'dart:async';

import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/answer_call_entity.dart';
import 'package:findnear/src/models/entities/candidate_call_entity.dart';
import 'package:findnear/src/models/entities/communication/response/conversation_entity.dart';
import 'package:findnear/src/models/entities/message_chat/entities/message_chat.dart';
import 'package:findnear/src/models/entities/offer_call_entity.dart';
import 'package:findnear/src/models/response/comment_response.dart';
import 'package:findnear/src/models/response/member_kick_response.dart';
import 'package:findnear/src/models/response/viewer_count_response.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';
import 'package:rxdart/subjects.dart';

class GlobalEvent {

  GlobalEvent._privateConstructor();

  static final GlobalEvent instance = GlobalEvent._privateConstructor();

  // ignore: close_sinks
  var onCreateConversationResponse = StreamController<ConversationEntity>.broadcast();

  var onUpdateConversationResponse = StreamController<Conversation>.broadcast();

  var onLeaveConversationResponse = StreamController<ConversationEntity>.broadcast();

  var onRemoveMemberResponse = StreamController<ConversationEntity>.broadcast();

  var onDeleteConversationResponse = StreamController<ConversationEntity>.broadcast();

  var onPinnedConversationFromSetting = StreamController<Conversation>.broadcast();

  var onUnpinnedConversationFromSetting = StreamController<Conversation>.broadcast();

  var onPinCodeUpdated = StreamController<Conversation>.broadcast();

  var onVisibleConversationFromSetting = StreamController<Conversation>.broadcast();

  var onHideConversationFromSetting = StreamController<Conversation>.broadcast();

  var onEnableCheckboxMode = StreamController<bool>.broadcast();

  var onSelectAllConversation = StreamController<bool>.broadcast();

  final onReceiveMessageResponse = PublishSubject<MessageChat>();

  final onDeleteMessageResponse = PublishSubject<MessageChat>();

  var onUpdateUnreadResponse = StreamController<Conversation>.broadcast();

  var onUpdateAvatarAndNameResponse = StreamController<Conversation>.broadcast();

  var onUpdateBackgroundResponse = StreamController<Conversation>.broadcast();

  // video call

  var onReceiveOfferCall = StreamController<OfferCallEntity>.broadcast();
  var onReceiveOfferCallIOS = StreamController<OfferCallEntity>.broadcast();
  var onEndVideoCall = StreamController<bool>.broadcast();
  var onAnswerCall = StreamController<AnswerCallEntity>.broadcast();
  var onCandidateCall = StreamController<CandidateCallEntity>.broadcast();
  var onReadyCall = StreamController<OfferCallEntity>.broadcast();

  final onPushCallEvent = BehaviorSubject<OfferCallEntity>();

  final onCloseOverlayCall = StreamController<bool>.broadcast();

  /// comment in livestream
  var onNewCommentResponse = StreamController<CommentResponse>.broadcast();

  var onLikeCommentResponse = StreamController<CommentResponse>.broadcast();

  var onUpdateCountJoinLivestream = StreamController<ViewerCountResponse>.broadcast();

  var onKickMember = StreamController<MemberKickResponse>.broadcast();

}