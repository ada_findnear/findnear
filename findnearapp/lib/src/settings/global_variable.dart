// Global variables
import 'package:geolocator/geolocator.dart';

Position current_location =
    new Position(latitude: 21.0228161, longitude: 105.8019441);
const String privacyPolicy = "privacy_policy";
const String privacyPolicyEN = "privacy_policy_en";
const String luckyWheelRules = "lucky_wheel_rules";
const String lotteryRules = "lucky_pair_of_number_rules";
const String findXuRules = "find_xu_rules";

const String gifTag = "@GIF ";
