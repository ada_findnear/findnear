import 'package:findnear/src/models/city.dart';
import 'package:findnear/src/models/district.dart';
import 'package:findnear/src/models/field.dart';
import 'package:findnear/src/models/ward.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import '../repository/user_repository.dart' as userRepo;

class CacheService extends GetxService {
  RxList<Field> fields = <Field>[].obs;
  RxList<Field> shopFields = <Field>[].obs;
  RxList<City> cities = <City>[].obs;
  RxList<District> districts = <District>[].obs;
  RxList<Ward> wards = <Ward>[].obs;

  Future<CacheService> init() async {
    return this;
  }

  List<Map<String, dynamic>> get fieldsAsMap {
    List<Map<String, dynamic>> results = [];
    if(fields.isNotEmpty){
      results = fields.map((element) => {
        'value': element.id,
        'label': element.name,
        'icon': Container(
          padding: const EdgeInsets.all(10),
          child: ClipOval(
            child: Image.network(
              element.image.url,
              width: 30,
              height: 30,
              fit: BoxFit.cover,
            ),
          ),
        ),
        'selected': userRepo.currentUser.value?.field == element,
        'is_locked': element.isLocked == 1 && userRepo.currentUser.value?.canCreateShop == false ? 1 : 0,
      }).toList();
    }

    return results;
  }

  List<Map<String, dynamic>> get shopFieldsAsMap {
    List<Map<String, dynamic>> results = [];
    if(shopFields.isNotEmpty){
      results = shopFields.map((element) => {
        'value': element.id,
        'label': element.name,
        'icon': Container(
          padding: const EdgeInsets.all(10),
          child: ClipOval(
            child: Image.network(
              element.image.url,
              width: 30,
              height: 30,
              fit: BoxFit.cover,
            ),
          ),
        ),
        'selected': userRepo.currentUser.value?.field == element,
        'is_locked': element.isLocked == 1 && userRepo.currentUser.value?.canCreateShop == false ? 1 : 0,
      }).toList();
    }

    return results;
  }

}
