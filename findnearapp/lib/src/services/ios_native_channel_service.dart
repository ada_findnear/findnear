import 'package:flutter/services.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class IosNativeChannelService extends GetxService {
  final channel = const MethodChannel("vn.tada.findnear/iosNative");

  Future<IosNativeChannelService> init() async {
    channel.setMethodCallHandler((call) async {
      if (call.method == "somethink") {
      } else {
        print("Method not implemented: ${call.method}");
      }
    });
    return this;
  }
}
