
import 'package:findnear/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingService extends GetxService {
  RxBool mapDisplaySatellite = false.obs;
  RxBool mapDisplayDating = false.obs;
  RxBool isDatingOnMapActive = false.obs;
  RxBool mapDisplayDatingDirection = false.obs;

  RxBool isAppFirstRun = true.obs;


  // Theme
  final Rx<ThemeMode> currentThemeMode = ThemeMode.system.obs;

  final RxBool displayMapInCareerRx = true.obs;

  SharedPreferences prefs;

  Future<SettingService> init() async {
    prefs = await SharedPreferences.getInstance();

    ///ThemeMode
    String themeModeCode = prefs.getString("themeModeCode") ?? Utils.themeMode2Code(ThemeMode.light);
    final themeMode = Utils.code2ThemeMode(themeModeCode);
    currentThemeMode.value = themeMode;
    Get.changeThemeMode(themeMode);

    ///Map career
    displayMapInCareerRx.value = prefs.getBool("displayMapInCareer") ?? true;
    isAppFirstRun.value = prefs.getBool("isAppFirstRun") ?? true;

    return this;
  }

  void showMapSatellite() {
    mapDisplaySatellite.value = true;
  }

  void showMapNormal() {
    mapDisplaySatellite.value = false;
  }

  void changeThemeMode(ThemeMode themeMode) async {
    prefs.setString('themeModeCode', Utils.themeMode2Code(themeMode));
    currentThemeMode.value = themeMode;
    Get.changeThemeMode(themeMode);
  }

  void displayMapByCareer(bool ok) async {
    displayMapInCareerRx.value = ok;
    prefs.setBool('displayMapInCareer', ok);
  }

  void toggleDatingModeMap() {
    mapDisplayDating.value = !mapDisplayDating.value;
    if( isAppFirstRun.value == true){
      isAppFirstRun.value = false;
      prefs.setBool('isAppFirstRun', false);
    }
  }

  bool get displayMapInCareer => !mapDisplayDating.value && displayMapInCareerRx.value;
}
