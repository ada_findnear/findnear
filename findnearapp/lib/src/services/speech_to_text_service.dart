import 'dart:io';

import 'package:get/get.dart';
import 'package:speech_to_text/speech_recognition_result.dart';
import 'package:speech_to_text/speech_to_text.dart';

enum _IosSpeechResultDebounceState { Started, Listening, Ended }

class _IosSpeechResultDebounce {
  final _IosSpeechResultDebounceState state;
  final SpeechRecognitionResult result;
  final SpeechResultListener callback;
  _IosSpeechResultDebounce({this.state, this.result, this.callback}) {}
}

class SpeechToTextService extends GetxService {
  final _iosStopListeningDebounceTime = Duration(milliseconds: 1000);
  final _iosStopIdleDebounceTime = Duration(seconds: 5);

  RxBool isAvailable = false.obs;
  RxBool isListening = false.obs;

  SpeechToText speech = SpeechToText();

  // to manually handle the listening state on iOS
  Worker _iosListeningWorker;
  Worker _iosIdleWorker;
  final _iosSpeechResultDebounce = Rx<_IosSpeechResultDebounce>(null);

  Future<SpeechToTextService> init() async {
    try {
      isAvailable.value = await speech.initialize(
        onStatus: _statusListener,
        debugLogging: false,
        finalTimeout: Duration(milliseconds: 0),
        options: [SpeechToText.androidIntentLookup],
      );

      // debounce stop listening
      _iosListeningWorker = debounce(
        _iosSpeechResultDebounce,
        (_IosSpeechResultDebounce value) {
          if (value != null &&
              value.state == _IosSpeechResultDebounceState.Listening) {
            _finishIosSpeechListen(value);
          }
        },
        time: _iosStopListeningDebounceTime,
      );

      // debouce stop idle
      // => stop after _iosStopIdleDebounceTime when startListening() but user doesn't say anything
      _iosIdleWorker = debounce(
        _iosSpeechResultDebounce,
        (_IosSpeechResultDebounce value) {
          if (value != null &&
              value.state == _IosSpeechResultDebounceState.Started) {
            _finishIosSpeechListen(value);
          }
        },
        time: _iosStopIdleDebounceTime,
      );
    } catch (e) {
      isAvailable.value = false;
    }
    return this;
  }

  @override
  void onClose() {
    _iosListeningWorker.dispose();
    _iosIdleWorker.dispose();
    super.onClose();
  }

  void startListening({SpeechResultListener onResult}) async {
    final localeNames = await speech.locales();
    final vietnameseLocal = localeNames.firstWhere((element) {
      return element.localeId == "vi_VN" || element.localeId == "vi-VN";
    }, orElse: null);

    if (Platform.isIOS) {
      // set state "Started"
      _iosSpeechResultDebounce.value = _IosSpeechResultDebounce(
        state: _IosSpeechResultDebounceState.Started,
        result: null,
        callback: null,
      );
    }

    speech.listen(
      onResult: Platform.isIOS
          ? ((result) => _handleIosSpeechResult(
                result,
                onResult,
              ))
          : onResult,
      listenFor: Duration(seconds: 30),
      partialResults: true,
      localeId: vietnameseLocal.localeId,
      cancelOnError: true,
      listenMode: ListenMode.confirmation,
    );
  }

  void stopListening() {
    speech.cancel();
    if (Platform.isIOS) {
      // set state "Ended"
      _iosSpeechResultDebounce.value = _IosSpeechResultDebounce(
        state: _IosSpeechResultDebounceState.Ended,
        result: null,
        callback: null,
      );
    }
  }

  void _statusListener(String status) {
    final isListening = status == "listening";
    this.isListening.value = isListening;
  }

  // manually handle the listening state on iOS
  void _handleIosSpeechResult(
    SpeechRecognitionResult result,
    SpeechResultListener callback,
  ) {
    // execute callback to update UI
    callback?.call(result);

    // set state "Listening"
    _iosSpeechResultDebounce.value = _IosSpeechResultDebounce(
      state: _IosSpeechResultDebounceState.Listening,
      result: result,
      callback: callback,
    );
  }

  // finish listen and pass result to callback
  void _finishIosSpeechListen(_IosSpeechResultDebounce speechResultDebounce) {
    if (speechResultDebounce == null) return;

    switch (speechResultDebounce.state) {
      case _IosSpeechResultDebounceState.Started:
        // stop listening when "idle" timeout
        stopListening();
        break;

      case _IosSpeechResultDebounceState.Listening:
        // set "finalResult" = true and pass to callback
        final resultJson = speechResultDebounce.result.toJson();
        resultJson["finalResult"] = true;
        speechResultDebounce.callback
            ?.call(SpeechRecognitionResult.fromJson(resultJson));

        // set state "Ended"
        _iosSpeechResultDebounce.value = _IosSpeechResultDebounce(
          state: _IosSpeechResultDebounceState.Ended,
          result: null,
          callback: null,
        );
        break;

      default:
        break;
    }
  }
}
