
import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:findnear/src/commons/app_audios.dart';
import 'package:get/get.dart';

class AudioAppService extends GetxService{

  Future<AudioAppService> init() async {
    return this;
  }

  AudioPlayer advancedPlayerCallComing = AudioPlayer();
  AudioCache audioCacheCallComing = AudioCache();
  AudioPlayer advancedPlayerCallOut = AudioPlayer();
  AudioCache audioCacheCallOut = AudioCache();

  Future<void> loadAudioCall({bool isMuted = false}) async {
    if (Platform.isAndroid) {
      final bytes = await (File.fromUri(await audioCacheCallComing.load(AppAudios.callSound))).readAsBytes();
      advancedPlayerCallComing = await audioCacheCallComing.playBytes(bytes, loop: true, stayAwake: true);
      advancedPlayerCallComing.setVolume(1);
    } else {
      advancedPlayerCallComing = await audioCacheCallComing.loop(AppAudios.callSound, stayAwake: true);
      advancedPlayerCallComing.setVolume(1);
    }
  }

   Future<void> loadAudioCallOut({bool isMuted = false}) async {
    if (Platform.isAndroid) {
      final bytes = await (File.fromUri(await audioCacheCallOut.load(AppAudios.callOUtSound))).readAsBytes();
      advancedPlayerCallOut = await audioCacheCallOut.playBytes(bytes, loop: true, stayAwake: true);
      advancedPlayerCallOut.setVolume(1);
    } else {
      advancedPlayerCallOut = await audioCacheCallOut.loop(AppAudios.callOUtSound, stayAwake: true);
      advancedPlayerCallOut.setVolume(1);
    }
  }
}