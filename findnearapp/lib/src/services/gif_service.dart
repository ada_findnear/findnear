import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:tenor/tenor.dart';

import '../models/entities/gif_category_entity.dart';
import '../utils/logger.dart';

class GifService extends GetxService {
  static final _apiKey = '4GIA4YXEAPYR';
  final _api = Tenor(
    apiKey: '$_apiKey',
    language: TenorLanguage.Vietnamese,
    domainRedirect: GlobalConfiguration().getValue('api_base_url'),
  );

  List<GifCategoryEntity> listGifCategory = [];

  Future<GifService> init() async {
    await _prepareData();
    return this;
  }

  void _prepareData() async {
    List<TenorCategories> listTenorCategories = [];
    try {
      listTenorCategories = await _api.requestCategories();
    } catch (e) {
      logger.e("GifService - error: $e");
    }

    if (listTenorCategories.isEmpty) return;

    for (var tenorCategories in listTenorCategories) {
      listGifCategory.add(GifCategoryEntity(
          tenorCategories: tenorCategories, tenorResponse: null));
    }
    await getTenorResponse(listTenorCategories.first.name);
  }

  Future<TenorResponse> getTenorResponse(String categoryName) async {
    int index = listGifCategory
        .indexWhere((element) => element.tenorCategories.name == categoryName);
    if (index == -1) return null;

    if (listGifCategory[index].tenorResponse != null) {
      return listGifCategory[index].tenorResponse;
    } else {
      final tenorResponse =
          await search(listGifCategory[index].tenorCategories.searchTerm);
      listGifCategory[index] = listGifCategory[index]
          .copyWith(tenorCategories: null, tenorResponse: tenorResponse);
      return tenorResponse;
    }
  }

  Future<TenorResponse> search(String keyword) async {
    try {
      return await _api.searchGIF(keyword, limit: 50);
    } catch (e) {
      logger.e(e.toString());
      return null;
    }
  }
}
