import 'dart:async';
import 'dart:io';

import 'package:findnear/src/global/global_data.dart';
import 'package:findnear/src/global/global_event.dart';
import 'package:findnear/src/models/entities/answer_call_entity.dart';
import 'package:findnear/src/models/entities/candidate_call_entity.dart';
import 'package:findnear/src/models/entities/communication/response/base_socket_response.dart';
import 'package:findnear/src/models/entities/communication/response/conversation_entity.dart';
import 'package:findnear/src/models/entities/message_chat/entities/message_chat.dart';
import 'package:findnear/src/models/entities/offer_call_entity.dart';
import 'package:findnear/src/models/enums/websocket_event.dart';
import 'package:findnear/src/models/response/comment_response.dart';
import 'package:findnear/src/models/response/member_kick_response.dart';
import 'package:findnear/src/models/response/viewer_count_response.dart';
import 'package:findnear/src/services/ios_native_channel_service.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_disposable.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:socket_io_client/socket_io_client.dart';

class WebsocketService extends GetxService {
  Socket socket;

  Future<WebsocketService> init() async {
    return this;
  }

  void connect(String username) {
    socket = io(
        GlobalConfiguration().getValue('base_socket_url'),
        OptionBuilder().setTransports(['websocket']) // for Flutter or Dart VM
            // .disableAutoConnect()  // disable auto-connection
            // .setExtraHeaders({'foo': 'bar'}) // optional
            .build());

    socket.onConnect((_) async {
      await _updateUserName(username);
    });

    socket.onReconnect((_) async {
      await _updateUserName(username);
    });

    /// đăng ký nhận respone từ websocket cho event "updateUsername"
    socket.on(WebsocketEvent.MESSAGE_SERVER.value, (data) {
      logger.log(
          "✅ WS: RESPONSE[200] => PATH: ${WebsocketEvent.MESSAGE_SERVER.value}\n DATA: $data");
      WebsocketAction action =
          WebsocketActionExtension.fromActionName(data["action"]);

      WebsocketMessageType messageType =
          WebsocketMessageTypeExtension.fromMessageTypeName(
              data["messageType"]);

      WebsocketVideoCallType videocallType =
          WebsocketVideoCallTypeExtension.fromMessageTypeName(data["type"]);

      WebSocketCommentType commentType =
          WebSocketCommentTypeExtension.fromMessageTypeName(data["messageType"]);

      switch (action) {
        case WebsocketAction.CHAT:
          switch (messageType) {
            case WebsocketMessageType.CREATE_CONVERSATION:
              final response = BaseSocketResponse<ConversationEntity>.fromJson(
                  data, (e) => ConversationEntity.fromJson(e));
              GlobalEvent.instance.onCreateConversationResponse
                  .add(response.data);
              break;
            case WebsocketMessageType.LEFT_GROUP:
              final response = BaseSocketResponse<ConversationEntity>.fromJson(
                  data, (e) => ConversationEntity.fromJson(e));
              GlobalEvent.instance.onRemoveMemberResponse.add(response.data);
              break;
            case WebsocketMessageType.DELETE_CONVERSATION:
              final response = BaseSocketResponse<ConversationEntity>.fromJson(
                  data, (e) => ConversationEntity.fromJson(e));
              // GlobalEvent.instance.onDeleteConversationResponse
              //     .add(response.data);
              break;
            case WebsocketMessageType.RECEIVE_MESSAGE:
              final response = BaseSocketResponse<MessageChat>.fromJson(
                  data, (e) => MessageChat.fromJson(e));
              GlobalEvent.instance.onReceiveMessageResponse.add(response.data);
              break;
            case WebsocketMessageType.DELETE_MESSAGE:
              final response = BaseSocketResponse<MessageChat>.fromJson(
                  data, (e) => MessageChat.fromJson(e));
              GlobalEvent.instance.onDeleteMessageResponse.add(response.data);
              break;
            // case WebsocketMessageType.ADD_USER_TO_GROUP:
            //   final response = BaseSocketResponse<ConversationEntity>.fromJson(
            //       data, (e) => ConversationEntity.fromJson(e));
            //   GlobalEvent.instance.onUpdateConversationResponse
            //       .add(response.data);
            //   break;
          }
          break;
        case WebsocketAction.VIDEO_CALL:
          switch (videocallType) {
            case WebsocketVideoCallType.LOGIN:
              break;
            case WebsocketVideoCallType.OFFER:
              final response = OfferCallEntity.fromJson(data);
              GlobalEvent.instance.onReceiveOfferCall.add(response);
              if(Platform.isIOS){
                GlobalData.instance.offer = response;
              }
              break;
            case WebsocketVideoCallType.ANSWER:
              final response = AnswerCallEntity.fromJson(data);
              GlobalEvent.instance.onAnswerCall.add(response);
              break;
            case WebsocketVideoCallType.CANDIDATE:
              final response = CandidateCallEntity.fromJson(data);
              GlobalEvent.instance.onCandidateCall.add(response);
              break;
            case WebsocketVideoCallType.LEAVE:
              GlobalEvent.instance.onEndVideoCall.add(true);
              if(Platform.isIOS){
                GlobalData.instance.offer = null;
              }
              break;
            case WebsocketVideoCallType.UPDATESTATUS:
              break;
            case WebsocketVideoCallType.READY:
              try {
                final response = OfferCallEntity.fromJson(data);
                GlobalEvent.instance.onReadyCall.add(response);
              } catch (e) {
                print("NamNH - socket ready error - $e");
              }
              break;
          }
          break;
        case WebsocketAction.COMMENT_LIVE_STREAM:
          switch (commentType) {
            case WebSocketCommentType.NEW_COMMENT:
              final response = BaseSocketResponse<CommentResponse>.fromJson(
                  data, (e) => CommentResponse.fromJson(e));
              GlobalEvent.instance.onNewCommentResponse
                  .add(response.data);
              break;
            case WebSocketCommentType.LIKE_COMMENT:
              final response = BaseSocketResponse<CommentResponse>.fromJson(
                  data, (e) => CommentResponse.fromJson(e));
              GlobalEvent.instance.onLikeCommentResponse
                  .add(response.data);
              break;
            case WebSocketCommentType.UPDATE_COUNT_JOIN_LIVESTREAM:
              final response = BaseSocketResponse<ViewerCountResponse>.fromJson(
                  data, (e) => ViewerCountResponse.fromJson(e));
              GlobalEvent.instance.onUpdateCountJoinLivestream
                  .add(response.data);
              break;
            case WebSocketCommentType.KICK_MEMBER:
              final response = BaseSocketResponse<MemberKickResponse>.fromJson(
                  data, (e) => MemberKickResponse.fromJson(e));
              GlobalEvent.instance.onKickMember
                  .add(response.data);
              break;
          }
          break;
      }
    });

    socket.onDisconnect((_) {
      logger.log("✅ WS: Disconnect to web socket successfully!");
    });

    socket.connect();
  }

  void disconnect() {
    socket.dispose();
    // Fluttertoast.showToast(
    //     msg: "Ngắt kết nối tới socket thành công",
    //     backgroundColor: AppColors.black,
    //     textColor: AppColors.white,
    //     toastLength: Toast.LENGTH_LONG,
    //     gravity: ToastGravity.CENTER,
    //     timeInSecForIosWeb: 1);
  }

  void subscribe(String event, [data]) {
    WebsocketEvent wsEvent = WebsocketEventExtension.fromEventName(event);
    switch (wsEvent) {
      case WebsocketEvent.UPDATE_USERNAME:

        /// khởi tạo mới đối với StreamController đã close
        if (GlobalEvent.instance.onCreateConversationResponse.isClosed)
          GlobalEvent.instance.onCreateConversationResponse =
              StreamController<ConversationEntity>.broadcast();

        /// emit event
        socket.emit(WebsocketEvent.UPDATE_USERNAME.value, data);
        break;

      case WebsocketEvent.MESSAGE:

        /// offer cuoc goi video
        socket.emit(WebsocketEvent.MESSAGE.value, data);
        break;

      default:
        socket.emitWithAck(event, data);
        break;
    }
  }

  void unsubscribe(String event) {
    WebsocketEvent wsEvent = WebsocketEventExtension.fromEventName(event);
    switch (wsEvent) {
      case WebsocketEvent.UPDATE_USERNAME:
        GlobalEvent.instance.onCreateConversationResponse.close();
    }
  }

  void resubscribe(List<String> urls) {
    /// todo resubscribe to subscribed channel collections
  }

  void _updateUserName(String username) async {
    logger.log("✅️ WS: Connect to web socket successfully!",
        printFullText: true);
    // Fluttertoast.showToast(
    //     msg: "Connect tới socket thành công",
    //     backgroundColor: AppColors.black,
    //     textColor: AppColors.white,
    //     toastLength: Toast.LENGTH_LONG,
    //     gravity: ToastGravity.CENTER,
    //     timeInSecForIosWeb: 1);

    /// sau khi connect thành công thì emit vào event "updateUsername"
    logger.log(
      "✈️ WS: REQUEST[EMIT] => PATH: ${WebsocketEvent.UPDATE_USERNAME.value}, username: $username",
      printFullText: true,
    );
    var map = new Map<String, dynamic>();
    map["userName"] = username;
    map["devicePlatform"] = Platform.isAndroid ? "ANDROID" : "IOS";

    final fcmToken = await FirebaseMessaging.instance?.getToken();
    if (Platform.isAndroid) {
      map["token"] = {
        "fcmToken": fcmToken ?? "",
        "deviceToken": "",
      };
    } else {
      final deviceToken = await FlutterCallkitIncoming.getDevicePushTokenVoIP();
      map["token"] = {
        "fcmToken": fcmToken ?? "",
        "deviceToken": deviceToken ?? "",
      };
    }
    print("WebsocketService - onConnect - updateUserName: $map");
    subscribe(WebsocketEvent.UPDATE_USERNAME.value, map);
  }
}
