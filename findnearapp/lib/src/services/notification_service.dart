import 'dart:async';

import 'package:findnear/src/models/response/message_count_response.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:get/get.dart';
import 'package:findnear/src/repository/notification_repository.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';

import '../models/notification.dart' as model;

class NotificationService extends GetxService {
  RxInt unreadNotificationCounter = 0.obs;
  List<model.Notification> notifications = [];
  bool isAppBadgeSupported = false;

  static const FETCH_NOTIFICATION_INTERVAL = 15; //Second

  Future<NotificationService> init() async {
    isAppBadgeSupported = await FlutterAppBadger.isAppBadgeSupported();
    fetchUnreadMessage();
    return this;
  }

  /*void fetchUnreadMessage() async {
    User _user = Get.find<LocalDataManager>().currentUser;
    if (_user?.apiToken == null) return;
    notifications.clear();
    final Stream<model.Notification> stream = await getNotifications();
    stream.listen(
      (model.Notification notification) {
        notifications.add(notification);
      },
      onError: logger.e,
      onDone: () {
        if (Get.find<LocalDataManager>().isLogin) {
          unreadNotificationCounter.value = notifications
              .where((element) {
                return element?.read == false;
              })
              .toList()
              .length;
          if (isAppBadgeSupported) {
            FlutterAppBadger.updateBadgeCount(unreadNotificationCounter.value);
          }
        }
      },
    );
  }*/

  @override
  void onClose() {
    super.onClose();
  }

  void fetchUnreadMessage() async {
    User _user = Get.find<LocalDataManager>().currentUser;
    if (_user?.apiToken == null) return;
    int count = 0;
    final Stream<MessageCountResponse> stream = await getNotificationsCount();
    stream.listen(
          (MessageCountResponse notification) {
          count = notification.unReadCount;
      },
      onError: logger.e,
      onDone: () {
        if (Get.find<LocalDataManager>().isLogin) {
          unreadNotificationCounter.value = count;
          if (isAppBadgeSupported) {
            FlutterAppBadger.updateBadgeCount(unreadNotificationCounter.value);
          }
        }
      },
    );
  }
}
