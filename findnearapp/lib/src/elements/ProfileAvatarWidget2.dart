import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../generated/l10n.dart';
import '../models/user.dart';

class ProfileAvatarWidget2 extends StatelessWidget {
  final User user;
  final VoidCallback onEditBioSelected;
  final VoidCallback onEditAvatarSelected;
  final VoidCallback onEditCoverSelected;
final bool isMyProfile;
  ProfileAvatarWidget2({
    Key key,
    this.user,
    this.onEditBioSelected,
    this.onEditAvatarSelected,
    this.onEditCoverSelected,  this.isMyProfile,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.transparent.withAlpha(0),
      child: Column(
        children: [
          Container(
            height: 250,
            child: Stack(
              children: [
                _coverWidget(context),
                Positioned.fill(
                  bottom: -170,
                  child: Center(
                    child: _avatarWidget(context),
                  ),
                ),
              ],
            ),
          ),
          // Text(
          //   user?.name ?? "",
          //   style: TextStyle(
          //       fontSize: 18.0,
          //       color: Theme.of(context).hintColor,
          //       letterSpacing: 2.0,
          //       fontWeight: FontWeight.bold),
          // ),
          SizedBox(
            height: 6,
          ),
          (user?.bio?.isEmpty ?? true && onEditBioSelected != null)
              ? _bioEmpty(context)
              : _bioText(context),
        ],
      ),
    );
  }

  Widget _bioText(BuildContext context) {
    return new GestureDetector(
      onTap: () {
        if (onEditBioSelected != null) {
          onEditBioSelected();
        }
      },
      child: Padding(
        padding: const EdgeInsets.only(left: 20.0, right: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              fit: FlexFit.loose,
              child: Padding(
                padding:
                    EdgeInsets.only(right: onEditBioSelected != null ? 5 : 0),
                child: Text(
                  user?.bio ?? "",
                  style: TextStyle(
                    fontSize: 12.0,
                    color: Theme.of(context).accentColor,
                  ),
                ),
              ),
            ),
            isMyProfile
                ? SvgPicture.asset(
                    "assets/svg/ic_pencil.svg",
                    color: Theme.of(context).accentColor,
                    width: 10,
                    height: 10,
                    fit: BoxFit.contain,
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }

  Widget _bioEmpty(BuildContext context) {
    return new GestureDetector(
        onTap: () {
          if (onEditBioSelected != null) {
            onEditBioSelected();
          }
        },
        child: Container(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            isMyProfile?SvgPicture.asset(
              "assets/svg/ic_pencil.svg",
              color: Theme.of(context).accentColor,
              width: 10,
              height: 10,
              fit: BoxFit.contain,
            ): SizedBox(),
            SizedBox(
              width: 4,
            ),
            Text(
              S.of(context).add_your_bio,
              style: TextStyle(
                fontSize: 12.0,
                color: Theme.of(context).accentColor,
              ),
            )
          ],
        ),),);
  }

  Widget _coverWidget(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (onEditCoverSelected != null) {
          onEditCoverSelected();
        }
      },
      child: Stack(
        children: [
          Container(
            height: 220,
            width: double.infinity,
            child: CachedNetworkImage(
              imageUrl: user?.imageCover?.thumb ?? "",
              fit: BoxFit.cover,
              height: 220,
              // width: double.infinity,
              errorWidget: (context, url, error) => Image.network(
                user?.imageCover?.thumb,
                fit: BoxFit.cover,
                height: 220,
              ),
            ),
          ),
          isMyProfile
              ? Positioned(
                  top: 40,
                  right: 35,
                  child: _iconCameraWidget(context,
                      iconColor: Colors.red,
                      bg: Colors.white.withOpacity(0.797)),
                )
              : SizedBox()
        ],
      ),
    );
  }

  Widget _avatarWidget(BuildContext context) {
    return Container(
      width: 80,
      height: 80,
      child: Stack(
        children: [
          Container(
            width: 80,
            height: 80,
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.white,
                width: 4,
              ),
              shape: BoxShape.circle,
            ),
            child: new GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () {
                if (onEditAvatarSelected != null) {
                  onEditAvatarSelected();
                }
              },
              child: ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: CachedNetworkImage(
                  imageUrl: user?.image?.icon ?? "",
                  fit: BoxFit.cover,
                  width: 80,
                  height: 80,
                  errorWidget: (context, url, error) => ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: Image.network(
                      url,
                      fit: BoxFit.cover,
                      width: 80,
                      height: 80,
                    ),
                  ),
                ),
              ),
            ),
          ),
          isMyProfile?Positioned(
            bottom: 0,
            right: 5,
            child: _iconCameraWidget(context),
          ): SizedBox()
        ],
      ),
    );
  }

  Widget _iconCameraWidget(BuildContext context, {Color bg, Color iconColor}) {
    return Container(
      width: 26,
      height: 26,
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: (bg ?? Colors.red).withOpacity(0.767),
      ),
      child: SvgPicture.asset(
        "assets/svg/ic_photo_camera.svg",
        color: iconColor ?? Colors.white,
        fit: BoxFit.contain,
      ),
    );
  }
}
