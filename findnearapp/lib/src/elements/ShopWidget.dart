import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../generated/l10n.dart';
import '../helpers/helper.dart';
import '../models/shop.dart';
import '../models/route_argument.dart';
import '../repository/settings_repository.dart';

// ignore: must_be_immutable
class ShopWidget extends StatelessWidget {
  Shop shop;
  String heroTag;
  Shop currentShop;

  ShopWidget({Key key, this.shop, this.heroTag, this.currentShop}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: this.heroTag == 'home_top_shops' ? 292 : this.heroTag == 'map_shops' && currentShop != null && shop.id == currentShop.id ? MediaQuery.of(context).size.width - 40 : 200,
      height: this.heroTag == 'home_top_shops' || (this.heroTag == 'map_shops' && currentShop != null && shop.id == currentShop.id) ? 200 : 90,
      margin: EdgeInsets.only(left: 20, right: 20, top: 15, bottom: 20),
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          BoxShadow(color: Theme.of(context).focusColor.withOpacity(0.1), blurRadius: 15, offset: Offset(0, 5)),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          currentShop != null && shop.id == currentShop.id ?
          // Image of the card
          Stack(
            fit: StackFit.loose,
            alignment: AlignmentDirectional.bottomStart,
            children: <Widget>[
              Hero(
                tag: this.heroTag + shop.id.toString(),
                child: ClipRRect(
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                  child: CachedNetworkImage(
                    height: 100,
                    width: double.infinity,
                    fit: BoxFit.cover,
                    imageUrl: this.heroTag == 'home_top_shops' ? shop.image : 'assets/img/loading.gif',
                    placeholder: (context, url) => Image.asset(
                      'assets/img/loading.gif',
                      fit: BoxFit.cover,
                      width: double.infinity,
                      height: 100,
                    ),
                    errorWidget: (context, url, error) => Icon(Icons.error_outline),
                  ),
                ),
              ),
              // Visibility(
              //   visible: this.heroTag == 'home_top_shops',
              //   child: Row(
              //     children: <Widget>[
              //       Container(
              //         margin: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
              //         padding: EdgeInsets.symmetric(horizontal: 12, vertical: 3),
              //         decoration: BoxDecoration(color: shop.closed ? Colors.grey : Colors.green, borderRadius: BorderRadius.circular(24)),
              //         child: shop.closed
              //             ? Text(
              //           S.of(context).closed,
              //           style: Theme.of(context).textTheme.caption.merge(TextStyle(color: Theme.of(context).primaryColor)),
              //         )
              //             : Text(
              //           S.of(context).open,
              //           style: Theme.of(context).textTheme.caption.merge(TextStyle(color: Theme.of(context).primaryColor)),
              //         ),
              //       ),
              //       Container(
              //         margin: EdgeInsets.symmetric(horizontal: 0, vertical: 8),
              //         padding: EdgeInsets.symmetric(horizontal: 12, vertical: 3),
              //         decoration: BoxDecoration(color: Helper.canDelivery(shop) ? Colors.green : Colors.orange, borderRadius: BorderRadius.circular(24)),
              //         child: Helper.canDelivery(shop)
              //             ? Text(
              //           S.of(context).delivery,
              //           style: Theme.of(context).textTheme.caption.merge(TextStyle(color: Theme.of(context).primaryColor)),
              //         )
              //             : Text(
              //           S.of(context).pickup,
              //           style: Theme.of(context).textTheme.caption.merge(TextStyle(color: Theme.of(context).primaryColor)),
              //         ),
              //       ),
              //     ],
              //   ),
              // ),
              Visibility(
                visible: this.heroTag == 'map_shops' && currentShop != null && shop.id == currentShop.id,
                child: Row(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                      padding: EdgeInsets.symmetric(horizontal: 12, vertical: 3),
                      decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(24)),
                      child: Text(
                        shop.owner.gender == "Male" ? S.of(context).male : shop.owner.gender == "Female" ? S.of(context).female : S.of(context).other,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 0, vertical: 8),
                      padding: EdgeInsets.symmetric(horizontal: 12, vertical: 3),
                      decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(24)),
                      child: Text(
                          shop.owner.age != "" ? shop.owner.age : "0",
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ) : SizedBox(height: 0),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        shop.title != null ? shop.title : "",
                        overflow: TextOverflow.fade,
                        softWrap: false,
                        style: Theme.of(context).textTheme.subtitle1,
                      ),
                      Text(
                        Helper.skipHtml((shop.description != null ? shop.description : "") ),
                        overflow: TextOverflow.fade,
                        softWrap: false,
                        style: Theme.of(context).textTheme.caption,
                        maxLines: 1
                      ),
                      SizedBox(height: 5),
                      Row(
                        children: Helper.getStarsList(shop.vote),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 15),
                Expanded(
                  child: Column(
                    children: <Widget>[
                      CircleAvatar(
                        backgroundImage: NetworkImage(shop.image),
                      ),
                      shop.distance > 0
                          ? Text(
                              Helper.getDistance(shop.distance, Helper.of(context).trans(text: setting.value.distanceUnit)),
                              overflow: TextOverflow.fade,
                              maxLines: 1,
                              softWrap: false,
                              style: TextStyle(fontSize: 10),
                            )
                          : SizedBox(height: 0)
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
