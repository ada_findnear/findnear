import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/helpers/app_config.dart';
import 'package:flutter/material.dart';

class DashLineWidget extends StatelessWidget {
  static const double dashWidth = 4;
  final double height;
  final Color color;

  const DashLineWidget({this.height = 1, this.color = AppColors.grayLight});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final boxWidth = constraints.constrainWidth();
        final dashCount = (boxWidth / (2 * dashWidth)).floor();
        return Flex(
          children: List.generate(dashCount, (_) {
            return SizedBox(
              width: dashWidth,
              height: height,
              child: DecoratedBox(
                decoration: BoxDecoration(color: color),
              ),
            );
          }),
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          direction: Axis.horizontal,
        );
      },
    );
  }
}