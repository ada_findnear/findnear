import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../models/user.dart';

class AddFriendWidget extends StatelessWidget {
  final User userData;
  final Color iconColor;
  final Color labelColor;
  final VoidCallback onPressed;

  AddFriendWidget({
    this.userData,
    this.onPressed,
    this.iconColor,
    this.labelColor,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        width: 36,
        height: 36,
        child: Stack(
          alignment: AlignmentDirectional.center,
          children: <Widget>[
            Icon(
              Icons.group,
              color: Colors.red,
              size: 20,
            ),
            if (userData.isFriend != null && !userData.isFriend)
              Positioned(
                top: 8,
                right: 2,
                child: Container(
                  child: Icon(
                    userData.has_friend_request_from != null && userData.has_friend_request_from ? Icons.check : (userData.has_sent_friend_request_to != null && userData.has_sent_friend_request_to ? Icons.arrow_forward : Icons.add),
                    color: Colors.red,
                    size: 12,
                  ),
                  // constraints: BoxConstraints(minWidth: 6, maxWidth: 6, minHeight: 20, maxHeight: 20),
                ),
              ),
          ],
        ),
        decoration: BoxDecoration(color: Colors.white, shape: BoxShape.circle, boxShadow: [
          BoxShadow(color: Theme.of(context).accentColor.withOpacity(0.25), blurRadius: 6),
        ]),
      ),
    );
  }
}
