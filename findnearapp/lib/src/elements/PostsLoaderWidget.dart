import 'package:flutter/material.dart';

// ignore: must_be_immutable
class PostsLoaderWidget extends StatelessWidget {
  final String type;
  PostsLoaderWidget({Key key, this.type}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 400,
      child: ListView.builder(
        padding: EdgeInsets.only(top: 20),
        itemCount: 3,
        itemBuilder: (context, index) {
          return Container(
              padding: EdgeInsets.all(15.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(15),
                child: Image.asset(
                  type == "post" ? 'assets/img/loading_post.gif' : 'assets/img/loading.gif',
                  fit: BoxFit.fill,
                ),
            ),
          );
        },
      ),
    );
  }
}
