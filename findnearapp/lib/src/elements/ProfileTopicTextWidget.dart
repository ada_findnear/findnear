import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/models/bottom_sheet_option.dart';
import 'package:findnear/src/widgets/dialog_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:readmore/readmore.dart';

class ProfileTopicTextWidget extends StatelessWidget {
  final bool expandText;
  final String content;

  ProfileTopicTextWidget({Key key, this.expandText, this.content});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onLongPress: onLongPress,
      child: expandText ? _showExpandText(context) : _showLessText(context),
    );
  }

  Widget _showLessText(context) {
    return Container(
        child: ReadMoreText(
      content ?? "",
      trimLines: 3,
      // colorClickableText: Colors.blue,
      trimMode: TrimMode.Line,
      trimCollapsedText: 'Xem thêm',
      trimExpandedText: '',
      moreStyle: Theme.of(context).textTheme.bodyText1.copyWith(
          fontSize: 16, fontWeight: FontWeight.w500, color: Colors.blue),
      lessStyle: Theme.of(context).textTheme.bodyText1.copyWith(
          fontSize: 16, fontWeight: FontWeight.w500, color: Colors.blue),
      style: Theme.of(context).textTheme.bodyText1.copyWith(
            color: Color(0xFF333333),
            fontSize: 16.0,
            fontWeight: FontWeight.w500,
          ),
    ));
  }

  Widget _showExpandText(context) {
    return Container(
      alignment: Alignment.topLeft,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 8.5),
        child: Text(
          content ?? "",
          style: Theme.of(context).textTheme.bodyText1.copyWith(
                color: Theme.of(context).hintColor,
                fontSize: 17.0,
                fontWeight: FontWeight.w500,
              ),
        ),
      ),
    );
  }

  void onLongPress() async {
    final indexTapped = await DialogHelper.showBottomSheetOptions(
      context: Get.context,
      options: [BottomSheetOption(icon: Icons.copy, text: S.current.copyText)],
    );
    switch (indexTapped) {
      case 0:
        if (content?.isNotEmpty ?? false) {
          Clipboard.setData(ClipboardData(text: content));
          AppSnackbar.showInfo(message: S.current.textCopied);
        }
        break;
    }
  }
}
