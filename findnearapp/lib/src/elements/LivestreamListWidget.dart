import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/LivestreamItemWidget.dart';
import 'package:findnear/src/models/entities/livestream_info_entity.dart';
import 'package:findnear/src/models/shop.dart';
import 'package:findnear/src/pages/find_near_page/find_near_controller.dart';
import 'package:findnear/src/pages/list_livestream/presenter/livestream_item_presenter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../main.dart';
import 'ShopItemWidget.dart';

class LivestreamListWidget extends StatefulWidget {
  String title;
  Function onClickLoadMore;
  Function onItemClick;

  LivestreamListWidget(this.title, this.onClickLoadMore, this.onItemClick);

  @override
  _LivestreamListWidgetState createState() => _LivestreamListWidgetState();
}

class _LivestreamListWidgetState extends State<LivestreamListWidget> {
  FindNearController _controller = Get.find();
  int type = -1;
  int SERVICE_NEAR_ME = 0;
  int RECOMMEND_FOR_YOU = 1;
  int FINDNEAR_LIVE = 2;

  @override
  void initState() {
    super.initState();
    _controller.getLivestreamShop();
  }

  Widget build(BuildContext context) {
    return Obx(() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (_controller.livestreamListRx.value.length >
              0) _getHeaderWidget(),
          if (_controller.livestreamListRx.value.length >
              0) _getLiveListWidget(),
        ],
      );
    });
  }

  Widget _getHeaderWidget() {
    return Container(
      padding: EdgeInsets.all(20),
      child: Row(
        children: [
          Image.asset(
            AppImages.icLsTitleInMarket,
            fit: BoxFit.none,
          ),
          SizedBox(width: 4),
          Stack(
            children: [
              Container(
                padding: EdgeInsets.only(top: 4, right: 8),
                child: Text(
                  widget.title,
                  style: TextStyle(
                    color: Color(0xff000000),
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Positioned.fill(
                child: Align(
                  alignment: Alignment.topRight,
                  child: Image.asset(
                    AppImages.icLsBroadcast,
                    fit: BoxFit.none,
                  ),
                ),
              ),
            ],
          ),
          Spacer(),
          InkWell(
            child: Text(
              'Xem thêm',
              style: TextStyle(
                color: Color(0xffff0000),
                fontSize: 14,
                fontWeight: FontWeight.normal,
              ),
            ),
            onTap: () {
              widget.onClickLoadMore(widget.title);
            },
          )
        ],
      ),
    );
  }

  Widget _getLiveListWidget() {
    return Container(
        height: 211.0,
        width: double.infinity,
        padding: EdgeInsets.only(left: 20),
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: _controller.livestreamListRx.value.length,
            itemBuilder: (BuildContext context, int index) {
              return Padding(
                padding: EdgeInsets.only(right: 20),
                child:
                // Text(index.toString()),
                LivestreamItemWidget(LivestreamItemPresenter.fromLSInfo(_controller.livestreamListRx.value[index]),
                    widget.onItemClick),
              );
            }));
  }
}
