import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:velocity_x/velocity_x.dart';

import '../../main.dart';

class AppBarCreate extends StatelessWidget {
  final String title;
  final Color color;
  final VoidCallback onClick;
  final bool isEdit;

  AppBarCreate({this.title, this.color, this.onClick, this.isEdit = false});

  @override
  Widget build(BuildContext context) {
    return _itemAppbar();
  }

  Widget _itemAppbar() {
    final textWidget = isEdit
        ? S.of(Get.context).update_post.text.medium.white.make()
        : S.of(Get.context).button_submit_post.text.medium.white.make();
    return HStack([
        Icon(
          Icons.arrow_back_ios,
          size: 20,
          color: Colors.black,
        ).paddingAll(16).onTap(goBack),
        title.text.bold.black.size(16).make().expand(),
        VxBox(child: textWidget)
            .padding(EdgeInsets.symmetric(vertical: 4, horizontal: 16))
            .roundedLg
            .margin(EdgeInsets.symmetric(horizontal: 16))
            .color(color)
            .make()
            .onTap(onClick),
      ]).box.color(Vx.white).outerShadow.make();
  }
}
