import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/shop.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/svg.dart';
import 'package:global_configuration/global_configuration.dart';

class ShopItemWidget extends StatefulWidget {
  ShopItemPresenter shop;
  Function onClick;

  ShopItemWidget(this.shop, this.onClick);

  @override
  _ShopItemWidgetState createState() => _ShopItemWidgetState();
}

class _ShopItemWidgetState extends State<ShopItemWidget> {
  Widget build(BuildContext context) {
    return _getServiceListWidget();
  }

  Widget _getServiceListWidget() {
    return _getShopItem(widget.shop);
  }

  Widget _getShopItem(ShopItemPresenter shop) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InkWell(
          onTap: () {
            widget.onClick(shop);
          },
          child: Stack(
            children: [
              Container(
                width: 140.0,
                height: 140.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  image: DecorationImage(
                    image: CachedNetworkImageProvider(shop.imgUrl),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              shop.isMonopoly
                  ? Positioned(
                      bottom: 8,
                      right: 4,
                      child: SvgPicture.asset(AppImages.icFn))
                  : SizedBox.shrink()
            ],
          ),
        ),
        Container(
          width: 140.0,
          child: Text(
            shop.name,
            textAlign: TextAlign.left,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 14,
              color: const Color(0xff333333),
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        Text(
          "ID: ${shop.code}",
          textAlign: TextAlign.left,
          style: TextStyle(
            fontFamily: 'Quicksand',
            fontSize: 14,
            color: AppColors.red500,
          ),
        ),
        Text(
          shop.distance,
          textAlign: TextAlign.left,
          style: TextStyle(
            fontFamily: 'Quicksand',
            fontSize: 14,
            color: const Color(0xff707070),
          ),
        ),
      ],
    );
  }
}

class ShopItemPresenter {
  int id;
  String imgUrl;
  String name;
  String code;
  bool isMonopoly;
  String distance;

  ShopItemPresenter({
    this.id,
    this.imgUrl,
    this.name,
    this.code,
    this.distance,
    this.isMonopoly,
  });

  static ShopItemPresenter fromShop(Shop shop) {
    String imgUrl =
        "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
    if (shop.img_url != null) imgUrl = shop.img_url;

    String distance = shop.distance.toStringAsFixed(1) + 'km';
    String name = shop.title ?? "";
    String code = shop.user_code ?? "";
    int id = shop.id;
    bool isMonopoly = shop.isMonopoly;

    return ShopItemPresenter(
        id: id,
        imgUrl: imgUrl,
        name: name,
        distance: distance,
        code: code,
        isMonopoly: isMonopoly);
  }
}
