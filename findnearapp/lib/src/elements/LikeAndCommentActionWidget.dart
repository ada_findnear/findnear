import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LikeAndCommentActionWidget extends StatelessWidget {
  final VoidCallback onSelectFavourite;
  final VoidCallback onSelectComment;
  final VoidCallback onSharePost;

  final String likers_count;
  final String comment_count;
  final bool isLike;

  LikeAndCommentActionWidget({
    Key key,
    this.likers_count,
    this.comment_count,
    this.onSelectFavourite,
    this.onSelectComment,
    this.onSharePost,
    this.isLike,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Container(
        color: AppColors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              child: InkWell(
                onTap: () => onSelectFavourite?.call(),
                child: Center(
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        padding: EdgeInsets.only(right: 5, top: 15, bottom: 15),
                        child: AnimatedSwitcher(
                          duration: const Duration(milliseconds: 200),
                          child: isLike
                              ? SvgPicture.asset(
                                  AppImages.icLike,
                                  color: AppColors.red,
                                )
                              : SvgPicture.asset(AppImages.icLikeOutlined),
                        ),
                      ),
                      Text(
                        S.of(context).care,
                        style: Theme.of(context)
                            .textTheme
                            .bodyText2
                            .merge(TextStyle(
                              color: isLike ? AppColors.red : AppColors.textBlack,
                              fontSize: 14.0,
                            )),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              child: InkWell(
                onTap: () => onSelectComment?.call(),
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SvgPicture.asset(AppImages.icComment, fit: BoxFit.contain),
                        const SizedBox(width: 5),
                        Text(
                          S.of(context).comment,
                          style: Theme.of(context)
                              .textTheme
                              .bodyText2
                              .merge(TextStyle(
                                color: AppColors.textBlack,
                                fontSize: 14.0,
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Container(
              child: InkWell(
                onTap: () => onSharePost?.call(),
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SvgPicture.asset(AppImages.icSharePost, fit: BoxFit.contain),
                        const SizedBox(width: 5),
                        Text(
                          S.of(context).label_share,
                          style: Theme.of(context)
                              .textTheme
                              .bodyText2
                              .merge(TextStyle(
                            color: AppColors.textBlack,
                            fontSize: 14.0,
                          )),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
}
