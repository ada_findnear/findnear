import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/elements/CommentBarWidget.dart';
import 'package:findnear/src/elements/GroupNewsFeedInfoWidget.dart';
import 'package:findnear/src/elements/NewsfeedItemWidget.dart';
import 'package:findnear/src/pages/newsfeed/details/newsfeed_post_detail_view.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../models/post.dart';
import 'LikeAndCommentActionWidget.dart';
import 'LikeAndCommentStatsWidget.dart';
import 'ProfileTopicPhotosWidget.dart';
import 'ProfileTopicTextWidget.dart';
import 'TopicItemWidget.dart';

class GroupNewsFeedItemWidget extends StatelessWidget {
  final TopicItemPreviewPhotoCallback onPreviewPhoto;
  final VoidCallback onMyPostMenuPressed;
  final VoidCallback onSharePost;
  final ToggleLikeButton toggleLikePost;
  final Post post;
  final DetailTopicCallback openDetailTopic;
  final bool isMyProfile;
  final bool hasActionBars;
  final bool isSharedPost;
  final VoidCallback onPressed;
  final VoidCallback onOtherUserPostMenuPressed;
  final VoidCallback didUpdateCallback;

  GroupNewsFeedItemWidget({
    Key key,
    this.onPreviewPhoto,
    this.onMyPostMenuPressed,
    this.onSharePost,
    this.post,
    this.toggleLikePost,
    this.openDetailTopic,
    this.isMyProfile,
    this.hasActionBars = true,
    this.isSharedPost = false,
    this.onPressed,
    this.onOtherUserPostMenuPressed,
    this.didUpdateCallback,
  });

  bool get hasSharedPost => post.isSharedPost;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 0),
          child: GroupNewsFeedInfoWidget(
            post: post,
            hasMenuButton: !isSharedPost,
            onMenuTap: () {
              if (!isLoggedIn) {
                NavigatorUtils.toLogin();
                return;
              }
              if (isMyProfile) {
                onMyPostMenuPressed?.call();
              } else {
                onOtherUserPostMenuPressed?.call();
              }
            },
          ),
        ),
        GestureDetector(
          onTap: onPressed,
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
            ),
            // padding: EdgeInsets.only(top: 12, left: 15, right: 13),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.only(
                            top: 5, bottom: 10, left: 0, right: 0),
                        child: ProfileTopicTextWidget(
                          expandText: false,
                          content: post?.content ?? "",
                        ),
                      ),
                    ),
                  ],
                ),
                if (hasSharedPost)
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      border: Border.all(color: AppColors.divider),
                    ),
                    padding:
                        const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                    child: NewsfeedItemWidget(
                      post: post.originalPost,
                      isSharedPost: true,
                    ),
                  )
                else
                  ProfileTopicPhotosWidget(
                    onClickPhoto: (index) {
                      print(index);
                      onPreviewPhoto?.call(index);
                    },
                    post: post,
                    onExpandClicked: () => openDetailTopic?.call(),
                    maxImages: 4,
                  ),
                if (!isSharedPost) ...[
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 0),
                    child: LikeAndCommentStatsWidget(
                      showComment: true,
                      like_count: post.likers_count,
                      isLike: post?.isLike ?? false,
                      comment_count: post.comments_count,
                      // onSelectFavourite: () => toggleLikePost?.call(post.id),
                      likers: post.likers,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 0),
                    child: const Divider(
                        height: 1, thickness: 1, color: AppColors.divider),
                  ),
                  if (hasActionBars)
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 0),
                      child: LikeAndCommentActionWidget(
                        isLike: post?.isLike ?? false,
                        comment_count: post.comments_count,
                        onSelectFavourite: () {
                          if (!isLoggedIn) {
                            NavigatorUtils.toLogin();
                            return;
                          }
                          ;
                          toggleLikePost?.call(post.id);
                        },
                        onSelectComment: _handleDidUpdatePostCallback,
                        onSharePost: () {
                          onSharePost();
                        },
                      ),
                    ),
                  if (hasActionBars)
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 0),
                      child: CommentBarWidget(
                        onTap: _handleDidUpdatePostCallback,
                      ),
                    ),
                ]
              ],
            ),
          ),
        )
      ],
    );
  }

  void _handleDidUpdatePostCallback() async {
    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    }
    ;
    await Get.to(
      () => NewsfeedPostDetailsPage(post: post),
    );
    didUpdateCallback?.call();
  }
}
