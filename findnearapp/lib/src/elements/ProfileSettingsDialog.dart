import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../generated/l10n.dart';
import '../models/user.dart';
import 'package:select_form_field/select_form_field.dart';

class ProfileSettingsDialog extends StatefulWidget {
  final User user;
  final VoidCallback onChanged;
  final List<Map<String, dynamic>> fields;

  ProfileSettingsDialog({Key key, this.user, this.onChanged, this.fields}) : super(key: key);

  @override
  _ProfileSettingsDialogState createState() => _ProfileSettingsDialogState();
}

class _ProfileSettingsDialogState extends State<ProfileSettingsDialog> {
  GlobalKey<FormState> _profileSettingsFormKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      elevation: 0,
      onPressed: () {
        showDialog(
            context: context,
            builder: (context) {
              return SimpleDialog(
                contentPadding: EdgeInsets.symmetric(horizontal: 20),
                titlePadding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                title: Row(
                  children: <Widget>[
                    Icon(Icons.person_outline),
                    SizedBox(width: 10),
                    Text(
                      S.of(context).profile_settings,
                      style: Theme.of(context).textTheme.bodyText1,
                    )
                  ],
                ),
                children: <Widget>[
                  Form(
                    key: _profileSettingsFormKey,
                    child: Column(
                      children: <Widget>[
                        new TextFormField(
                          style: TextStyle(color: Theme.of(context).hintColor),
                          keyboardType: TextInputType.text,
                          decoration: getInputDecoration(hintText: S.of(context).john_doe, labelText: S.of(context).full_name),
                          initialValue: widget.user.name,
                          validator: (input) => input.trim().length < 3 ? S.of(context).not_a_valid_full_name : null,
                          onSaved: (input) => widget.user.name = input,
                        ),
                        new TextFormField(
                          style: TextStyle(color: Theme.of(context).hintColor),
                          keyboardType: TextInputType.emailAddress,
                          decoration: getInputDecoration(hintText: 'johndo@gmail.com', labelText: S.of(context).email_address),
                          initialValue: widget.user.email,
                          validator: (input) => input.isEmpty ? null : (!input.contains('@') ? S.of(context).not_a_valid_email : null),
                          onSaved: (input) => widget.user.email = input,
                        ),
                        new TextFormField(
                          style: TextStyle(color: Theme.of(context).hintColor),
                          keyboardType: TextInputType.phone,
                          decoration: getInputDecoration(hintText: '+136 269 9765', labelText: S.of(context).phoneNumber),
                          initialValue: widget.user.phone,
                          validator: (input) => input.isEmpty ? null : (input.trim().length < 3 ? S.of(context).not_a_valid_phone : null),
                          onSaved: (input) => widget.user.phone = input,
                          onChanged: (input) => {widget.user.verifiedPhone = false, print(widget.user.verifiedPhone)},
                        ),
                        new SelectFormField(
                          style: TextStyle(color: Theme.of(context).hintColor),
                          labelText: 'Shape',
                          initialValue: widget.user.field.id,
                          decoration: getInputDecoration(hintText: S.of(context).chooseProfession, labelText: S.of(context).profession),
                          changeIcon: true,
                          enableSearch: true,
                          items: widget.fields,
                          validator: (input) => null,
                          onSaved: (input) => {
                            if (input != '') {
                              widget.user.fieldId = input
                            }
                          },
                          itemIsLocked: () {
                              showDialog(
                                context: Get.context,
                                barrierDismissible: true,
                                builder: (BuildContext context) =>
                                    contactUsDialog(),
                              );
                          },
                        ),
                        new TextFormField(
                          style: TextStyle(color: Theme.of(context).hintColor),
                          keyboardType: TextInputType.text,
                          decoration: getInputDecoration(hintText: S.of(context).your_address, labelText: S.of(context).address),
                          initialValue: widget.user.address,
                          validator: (input) => input.isEmpty ? null : (input.trim().length < 3 ? S.of(context).not_a_valid_address : null),
                          onSaved: (input) => widget.user.address = input,
                        ),
                        new TextFormField(
                          style: TextStyle(color: Theme.of(context).hintColor),
                          keyboardType: TextInputType.text,
                          decoration: getInputDecoration(hintText: S.of(context).your_biography, labelText: S.of(context).about),
                          initialValue: widget.user.bio,
                          maxLength: 100,
                          validator: (input) => input.isEmpty ? null : (input.trim().length < 3 ? S.of(context).not_a_valid_biography : null),
                          onSaved: (input) => widget.user.bio = input,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Row(
                    children: <Widget>[
                      MaterialButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text(S.of(context).cancel),
                      ),
                      MaterialButton(
                        onPressed: _submit,
                        child: Text(
                          S.of(context).save,
                          style: TextStyle(color: Theme.of(context).accentColor),
                        ),
                      ),
                    ],
                    mainAxisAlignment: MainAxisAlignment.end,
                  ),
                  SizedBox(height: 10),
                ],
              );
            });
      },
      child: Text(
        S.of(context).edit,
        style: Theme.of(context).textTheme.bodyText2,
      ),
    );
  }

  InputDecoration getInputDecoration({String hintText, String labelText}) {
    return new InputDecoration(
      hintText: hintText,
      labelText: labelText,
      hintStyle: Theme.of(context).textTheme.bodyText2.merge(
            TextStyle(color: Theme.of(context).focusColor),
          ),
      enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).hintColor.withOpacity(0.2))),
      focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).hintColor)),
      floatingLabelBehavior: FloatingLabelBehavior.auto,
      labelStyle: Theme.of(context).textTheme.bodyText2.merge(
            TextStyle(color: Theme.of(context).hintColor),
          ),
    );
  }

  void _submit() {
    if (_profileSettingsFormKey.currentState.validate()) {
      _profileSettingsFormKey.currentState.save();
      widget.onChanged();
      Navigator.pop(context);
    }
  }

  Dialog contactUsDialog() {
    return Dialog(
      backgroundColor: Colors.transparent,
      insetPadding: EdgeInsets.zero,
      child: Column(
        children: [
          Spacer(),
          Stack(
            children: [
              Container(
                padding: EdgeInsets.fromLTRB(20, 40, 20, 20),
                margin: EdgeInsets.fromLTRB(30, 25, 30, 0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: Column(
                  children: <Widget>[
                    Text(
                      S.of(context).job_is_not_available,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: AppColors.black,
                      ),
                    ),
                    SizedBox(height: 10),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text:
                            S.of(context).job_is_not_available_content_1,
                            style: TextStyle(
                                fontSize: 14,
                                color: AppColors.black,
                                fontWeight: FontWeight.w300,
                                height: 1.4),
                          ),
                          TextSpan(
                            text:
                                '0985725511',
                            style: TextStyle(
                                fontSize: 14,
                                color: AppColors.red,
                                fontWeight: FontWeight.bold,
                                height: 1.4),
                          ),
                          TextSpan(
                            text: S.of(context).job_is_not_available_content_2,
                            style: TextStyle(
                                fontSize: 14,
                                color: AppColors.black,
                                fontWeight: FontWeight.w300,
                                height: 1.4),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Positioned.fill(
                top: 0,
                child: Align(
                  alignment: Alignment.topCenter,
                  child: AppImage(AppImages.icMail),
                ),
              ),
              Positioned(
                top: 30,
                right: 30,
                child: InkWell(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AppImage('assets/img/ic_close.png'),
                  ),
                  onTap: () => Get.back(),
                ),
              )
            ],
          ),
          Spacer(),
        ],
      ),
    );
  }
}
