import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/pages/view_media/view_media_slide.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';

import '../models/media.dart';

typedef ProfileTopicPhotosCallback(Media media);

class ProfileTopicPhotosWidget extends StatefulWidget {
  final ProfileTopicPhotosCallback onClickPhoto;
  final int maxImages;
  final Post post;
  final Function onExpandClicked;

  ProfileTopicPhotosWidget(
      {@required this.onExpandClicked,
      this.maxImages = 4,
      this.onClickPhoto,
      this.post,
      Key key})
      : super(key: key);

  @override
  createState() => _ProfileTopicPhotosWidgetState();
}

class _ProfileTopicPhotosWidgetState extends State<ProfileTopicPhotosWidget> {
  @override
  Widget build(BuildContext context) {
    if (widget.post.medias.length == 0) {
      return SizedBox(height: 0);
    }
    if (widget.post.medias.length == 1) {
      return _buildSingleMediaWidget(context);
    }
    var itemCount = (widget.maxImages <= widget.post.medias.length)
        ? widget.maxImages
        : widget.post.medias.length;
    return GridView.builder(
        physics: new NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: itemCount,
        padding: const EdgeInsets.symmetric(horizontal: 0),
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 200,
          crossAxisSpacing: 4,
          mainAxisSpacing: 4,
        ),
        itemBuilder: _buildMediaWidget);
  }

  Widget _buildSingleMediaWidget(BuildContext context) {
    final media = widget?.post?.medias.first;
    bool isImage = media?.isPhoto;
    String imageUrl = isImage ? media?.url : media?.thumb;
    return GestureDetector(
        onTap: () =>
            _previewMultipleMedia(currentIndex: 0, post: widget.post),
        child: Center(
          child: Stack(
            alignment: Alignment.center,
            children: [
              CachedNetworkImage(
                imageUrl: imageUrl,
                errorWidget: (context, s, i) =>
                    isImage ? _buildLoadImageError() : _buildLoadVideoError(),
                placeholder: (context, url) => Image.asset(
                  'assets/img/loading.gif',
                  fit: BoxFit.cover,
                  width: double.infinity,
                  height: 200.w,
                ),
                fit: BoxFit.contain,
              ),
              Visibility(
                visible: !isImage,
                child: Container(
                  width: 33.25.w,
                  height: 33.25.w,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.black.withOpacity(0.4),
                  ),
                  padding: EdgeInsets.all(4.w),
                  alignment: Alignment.center,
                  child: SvgPicture.asset(
                    "assets/svg/ic_play.svg",
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  Widget _buildMediaWidget(BuildContext context, int index) {
    String imageUrl = widget?.post?.medias[index]?.thumb ?? widget?.post?.medias[index]?.url;
    bool isImage =
        widget?.post?.medias?.elementAt(index)?.mime_type?.indexOf("image") != -1;
    int remaining = widget?.post?.medias.length - widget.maxImages;
    return GestureDetector(
        onTap: () =>
            _previewMultipleMedia(currentIndex: index, post: widget?.post),
        child: Stack(
          alignment: Alignment.center,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(5),
              child: CachedNetworkImage(
                imageUrl: imageUrl,
                errorWidget: (context, s, i) =>
                    isImage ? _buildLoadImageError() : _buildLoadVideoError(),
                placeholder: (context, url) => Image.asset(
                  'assets/img/loading.gif',
                  fit: BoxFit.cover,
                  width: double.infinity,
                  height: 200.w,
                ),
                fit: BoxFit.cover,
              ),
            ),
            Visibility(
              visible: !isImage,
              child: Container(
                width: 33.25.w,
                height: 33.25.w,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.black.withOpacity(0.4),
                ),
                padding: EdgeInsets.all(4.w),
                alignment: Alignment.center,
                child: SvgPicture.asset(
                  "assets/svg/ic_play.svg",
                  color: Colors.white,
                ),
              ),
            ),
            (remaining > 0 && index == widget.maxImages - 1)
                ? Positioned.fill(
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Colors.black.withOpacity(0.4),
                      ),
                      alignment: Alignment.center,
                      child: Text(
                        '+' + remaining.toString(),
                        style: TextStyle(fontSize: 23, color: Colors.white),
                      ),
                    ),
                  )
                : SizedBox()
          ],
        ));
  }

  Widget _buildLoadImageError() {
    return Container(
      width: double.infinity,
      alignment: Alignment.center,
      height: 160.w,
      child: Text(S.of(context).load_image_failed_message),
    );
  }

  Widget _buildLoadVideoError() {
    return Container(
        color: Colors.black,
        width: double.infinity,
        alignment: Alignment.center,
        height: 260.w);
  }

  void _previewMultipleMedia({currentIndex, Post post}) {
    ViewMediaSlide.show(context, post, currentIndex);
  }
}
