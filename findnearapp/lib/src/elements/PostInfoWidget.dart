import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/helpers/app_config.dart';
import 'package:findnear/src/helpers/date_utils.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/post.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../repository/settings_repository.dart' as settingRepo;

import 'CircularAvatarWidget.dart';

class PostInfoWidget extends StatelessWidget {
  final Post post;
  final VoidCallback onMenuTap;
  final bool hasMenuButton;

  const PostInfoWidget({
    Key key,
    this.post,
    this.onMenuTap,
    this.hasMenuButton = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Row(
        children: [
          CircularAvatarWidget(
            user: post?.creator,
            size: 42,
            hasBorder: post?.creator?.isOnline,
            hasLiveIndicator: post?.creator?.isLiveStreaming,
          ),
          const SizedBox(width: 9),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(
                  Helper.limitString(
                      post?.creator?.name,limit: 20),
                  style: Theme.of(context).textTheme.headline6.merge(TextStyle(
                        color: AppColors.textBlack,
                        fontSize: 16,
                        fontFamily: 'Quicksand',
                      )),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(height: 6,),
                Text(
                  "ID: ${post?.creator?.code ?? ""}",
                  style: Theme.of(context).textTheme.bodyText1.merge(TextStyle(
                        color: AppColors.red500,
                        fontSize: 14,
                        fontFamily: 'Quicksand',
                      )),
                ),
              ],
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Builder(
                builder: (context) {
                  final dateTime = DateTime.tryParse(post?.created_at ?? "");
                  return Text(
                    dateTime != null
                        ? AppDateFormatUtils.getVerboseDateTimeRepresentation(
                            dateTime)
                        : "",
                    style:
                        Theme.of(context).textTheme.bodyText2.merge(TextStyle(
                              color: AppColors.gray2,
                              fontSize: 14,
                            )),
                  );
                },
              ),
              if (hasMenuButton)
                GestureDetector(
                  onTap: () => onMenuTap?.call(),
                  child: Container(
                    padding: const EdgeInsets.only(top: 0, left: 16),
                    child: Center(
                      child: Icon(
                        Icons.more_horiz,
                        color: AppColors.grayLight,
                      ),
                    ),
                  ),
                ),
            ],
          ),
        ],
      );
}
