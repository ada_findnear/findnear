import 'package:findnear/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/html_parser.dart';
import 'package:flutter_html/style.dart';

// ignore: must_be_immutable
class NoDataPostsWidget extends StatelessWidget {
  final bool isMyProfile;
  NoDataPostsWidget({Key key, this.isMyProfile}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 400,
      child: Padding(
        padding: const EdgeInsets.only(top: 50, bottom: 10, left: 50, right: 50),
        child: Text(
          isMyProfile ? S.of(context).noDataPostMyProfile : S.of(context).noDataPostUserProfile,
          style: TextStyle(fontSize: 15),
          textAlign: TextAlign.center,
        ),
      )
    );
  }
}
