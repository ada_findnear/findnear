import 'package:flutter/material.dart';

import '../helpers/helper.dart';
import '../models/faq.dart';

class FaqItemWidget extends StatefulWidget {
  final Faq faq;

  FaqItemWidget({Key key, this.faq}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _FagItemWidgetState();
  }
}

class _FagItemWidgetState extends State<FaqItemWidget> {
  bool _showContent = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        InkWell(
          onTap: () {
            setState(() {
              _showContent = !_showContent;
            });
          },
          child: Container(
            width: double.infinity,
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(5), topRight: Radius.circular(5))),
            child: Text(
              Helper.skipHtml(widget.faq.question),
              style: Theme.of(context).textTheme.bodyText1.merge(TextStyle(
                  color: Colors.black87, fontWeight: FontWeight.bold)),
            ),
          ),
        ),
        Visibility(
          visible: _showContent,
          child: Container(
            width: double.infinity,
            padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
            child: Text(
              Helper.skipHtml(widget.faq.answer),
              style: Theme.of(context).textTheme.bodyText2,
            ),
          ),
        ),
      ],
    );
  }
}
