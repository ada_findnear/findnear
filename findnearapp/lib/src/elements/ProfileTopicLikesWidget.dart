import 'package:findnear/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ProfileTopicLikesWidget extends StatelessWidget {
  VoidCallback onOpenSetting;
  VoidCallback onSelectFavourite;

  final bool showComment;
  final String likers_count;
  final String comment_count;
  final bool isLike;
  final bool isMyProfile;
  ProfileTopicLikesWidget(
      {Key key,
      this.showComment,
      this.onOpenSetting,
      this.likers_count,
      this.comment_count,
      this.onSelectFavourite,
      this.isLike,
      this.isMyProfile})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return showComment == true
        ? _showLikeComment(context)
        : _showOnlyLike(context);
  }

  Widget _showLikeComment(context) {
    return Container(
      color: Colors.white,
      child: Row(
        children: [
          InkWell(
            child: Row(
              children: [
                Container(
                  padding: EdgeInsets.only(right: 5, top: 15, bottom: 15),
                  child: SvgPicture.asset(
                    "assets/svg/ic_heart.svg",
                    color: isLike ? Colors.red : Theme.of(context).hintColor,
                  ),
                ),
                Text(
                  S.of(context).care_count("${(likers_count != null && likers_count != "0") ? "${likers_count}" : ""}"),
                  style: TextStyle(color: isLike ? Colors.red : Theme.of(context).hintColor, fontSize: 12.0),
                ),
              ],
            ),
            onTap: onSelectFavourite,
          ),
          Spacer(),
          SvgPicture.asset("assets/svg/ic_message.svg", fit: BoxFit.contain,),
          SizedBox(
            width: 5,
          ),
          Text(S.of(context).comment_count(comment_count ?? ""),
              style:
                  TextStyle(color: Color.fromRGBO(51, 51, 51, 1), fontSize: 12.0)),
          Spacer(),
          Spacer(),
        ],
      ),
    );
  }

  Widget _showOnlyLike(context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          width: 5,
        ),
        Icon(
          Icons.favorite,
          color: Theme.of(context).hintColor,
          size: 20,
        ),
        SizedBox(
          width: 5,
        ),
        Text(
          likers_count,
          style: TextStyle(color: Theme.of(context).hintColor, fontSize: 16.0),
        ),
      ],
    );
  }
}
