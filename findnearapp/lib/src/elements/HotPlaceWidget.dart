import 'dart:ui';

import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/models/field.dart';
import 'package:findnear/src/models/media.dart';
import 'package:findnear/src/models/response/field_response.dart';
import 'package:findnear/src/pages/find_near_page/find_near_controller.dart';
import 'package:findnear/src/widgets/ui_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';

class HotPlaceWidget extends StatefulWidget {
  final List<Field> fields;
  final Function onClick;

  @override
  _HotPlaceWidgetState createState() => _HotPlaceWidgetState();

  HotPlaceWidget(this.fields, this.onClick);
}

class _HotPlaceWidgetState extends State<HotPlaceWidget> {
  static const int _ITEM_PER_PAGE = 8;
  static const int _COLUMN_PER_PAGE = 4;
  var _currentPage = 0;
  FindNearController _controller = Get.find();

  @override
  void initState() {
    super.initState();
    _controller.getFields();
  }

  @override
  Widget build(BuildContext context) {
    return _getHotPlacesWidget();
  }

  Widget _getHotPlacesWidget() {
    var items = _getAllFieldShops();
    var pageCount = items.length ~/ _ITEM_PER_PAGE;
    if (pageCount * _ITEM_PER_PAGE < items.length) {
      pageCount++;
    }
    return Container(
        width: double.infinity,
        height: 190.w,
        color: Colors.white,
        child: Column(
          children: [
            Expanded(
              child: PageView(
                onPageChanged: (index) {
                  setState(() {
                    _currentPage = index;
                  });
                },
                scrollDirection: Axis.horizontal,
                children: List.generate(
                    pageCount,
                    (index) => _buildItemsPage(items.sublist(
                        index * _ITEM_PER_PAGE,
                        index == pageCount - 1
                            ? items.length
                            : (index * _ITEM_PER_PAGE + _ITEM_PER_PAGE)))),
              ),
            ),
            Container(
              alignment: Alignment.center,
              width: double.infinity,
              height: 3.w,
              child: _buildPageIndicator(pageCount, _currentPage),
            ),
            SizedBox(height: 8.w)
          ],
        ));
  }

  Widget _buildPageIndicator(int itemCount, int currentPageIndex) {
    return ListView.separated(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemBuilder: (c, i) => Container(
              width: 10.w,
              color:
                  currentPageIndex == i ? AppColors.red : AppColors.grayLight,
            ),
        separatorBuilder: (c, i) => SizedBox(width: 4.w),
        itemCount: itemCount);
  }

  Widget _buildItemsPage(List<Field> items) {
    return GridView.count(
        physics: NeverScrollableScrollPhysics(),
        crossAxisCount: _COLUMN_PER_PAGE,
        padding: EdgeInsets.all(12.w),
        children: items.map((e) => _getHotPlaceWidget(e)).toList());
  }

  List<Field> _getAllFieldShops() {
    List<Field> result = <Field>[];
    result.add(Field(
        image: Media(url: AppImages.icNearMe),
        name: S.of(context).near_me_button));
    if (widget.fields != null) {
      result.addAll(widget.fields);
    }
    return result;
  }

  Widget _getHotPlaceWidget(Field item) {
    return InkWell(
        onTap: () {
          widget.onClick(item);
        },
        child: Column(
          children: [
            Container(
              width: 40,
              height: 40,
              child: UIHelper.showImage(item.image.url),
            ),
            SizedBox(
              height: 4,
            ),
            Text(
              item.name,
              textAlign: TextAlign.center,
              style: TextStyle(color: Color(0xff000000), fontSize: 12),
            ),
          ],
        ));
  }
}
