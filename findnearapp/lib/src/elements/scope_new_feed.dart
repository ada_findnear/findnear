import 'package:findnear/src/utils/navigator_utils.dart';

import '../commons/app_colors.dart';

import '../../generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:velocity_x/velocity_x.dart';

class ScopeNewFeed extends StatelessWidget {
  final int index;

  final List<dynamic> listScope;

  final Function(int) onClick;

  ScopeNewFeed({this.index, this.listScope, this.onClick});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: listScope.length,
        padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
        shrinkWrap: true,
        itemBuilder: (_, i) => _itemDetailScope(i, i == index));
  }

  Widget _itemDetailScope(int indexItem, bool status) => HStack([
        _getIcons(listScope[indexItem].toString(), status),
        16.widthBox,
        listScope[indexItem]
            .toString()
            .text
            .color(status ? AppColors.red : AppColors.textBlack)
            .size(17)
            .bold
            .make()
            .expand(),
        Radio(
            value: indexItem,
            toggleable: status,
            activeColor: AppColors.red,
            groupValue: index,
            onChanged: (v) {
              _onItemSelected(indexItem);
            })
      ]).paddingSymmetric(horizontal: 16, vertical: 8).onTap(() {
        _onItemSelected(indexItem);
      });

  void _onItemSelected(int indexItem) {
    onClick(indexItem);
    goBack();
  }

  Widget _getIcons(String title, bool status) {
    var ic;
    if (S.of(Get.context).viewerOnlyFriend == title)
      ic = Icons.person;
    else if (S.of(Get.context).viewerPublic == title)
      ic = Icons.public;
    else
      ic = Icons.lock;
    return Icon(ic, size: 30, color: status ? AppColors.red : AppColors.gray);
  }
}
