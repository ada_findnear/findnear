import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:findnear/src/pages/product_detail/view.dart';
import 'package:get/get.dart';

import '../helpers/helper.dart';
import '../models/response/product_response.dart';
import '../models/route_argument.dart';

class VNPProductItemWidget extends StatelessWidget {
  final String heroTag;
  final Product product;

  const VNPProductItemWidget({Key key, this.product, this.heroTag}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return InkWell(
      splashColor: Theme.of(context).accentColor,
      focusColor: Theme.of(context).accentColor,
      highlightColor: Theme.of(context).primaryColor,
      onTap: () {
        Get.toNamed(ProductDetailPage.ROUTE_NAME,
          arguments: RouteArgument(param: product));
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor.withOpacity(0.9),
          boxShadow: [
            BoxShadow(color: Theme.of(context).focusColor.withOpacity(0.1), blurRadius: 5, offset: Offset(0, 2)),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Hero(
              tag: heroTag + product.id.toString(),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                child: CachedNetworkImage(
                  height: 60,
                  width: 60,
                  fit: BoxFit.cover,
                  imageUrl: product.firstImage,
                  placeholder: (context, url) => Image.asset(
                    'assets/img/loading.gif',
                    fit: BoxFit.cover,
                    height: 60,
                    width: 60,
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error_outline),
                ),
              ),
            ),
            SizedBox(width: 15),
            Flexible(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          product.name,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: Theme.of(context).textTheme.subtitle1,
                        ),
                        Row(
                          //update rate later
                          children: Helper.getStarsList(0),
                        ),
                        Text(
                          product.description,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: Theme.of(context).textTheme.caption,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(width: 8),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Helper.getPrice(
                        product.price,
                        context,
                        style: Theme.of(context).textTheme.headline4,
                      ),
                      // Helper.getPrice(product.price, context,
                      //         style: Theme.of(context).textTheme.bodyText2.merge(TextStyle(decoration: TextDecoration.lineThrough)))
                              // ,
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
