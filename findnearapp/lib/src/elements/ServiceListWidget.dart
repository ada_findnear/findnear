import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/models/shop.dart';
import 'package:findnear/src/pages/find_near_page/find_near_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../main.dart';
import 'ShopItemWidget.dart';

class ServiceListWidget extends StatefulWidget {
  String title;
  Function onClickLoadMore;
  Function onItemClick;

  ServiceListWidget(this.title, this.onClickLoadMore, this.onItemClick);

  @override
  _ServiceListWidgetState createState() => _ServiceListWidgetState();
}

class _ServiceListWidgetState extends State<ServiceListWidget> {
  FindNearController _controller = Get.find();
  int type = -1;
  int SERVICE_NEAR_ME = 0;
  int RECOMMEND_FOR_YOU = 1;
  List<Shop> shopList;

  @override
  void initState() {
    super.initState();
    _controller.getNearMeShop();
  }

  Widget build(BuildContext context) {
    return Obx(() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _getHeaderWidget(),
          if (_controller.nearMeShopListRx.value.length > 0)
            _getServiceListWidget(),
        ],
      );
    });
  }

  Widget _getHeaderWidget() {
    return Container(
      padding: EdgeInsets.all(20),
      child: Row(
        children: [
          Image.asset(
            'assets/img/premium-quality.png',
            fit: BoxFit.none,
          ),
          SizedBox(width: 4),
          Text(
            widget.title,
            style: TextStyle(
              color: Color(0xff000000),
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          Spacer(),
          InkWell(
            child: Text(
              'Xem thêm',
              style: TextStyle(
                color: Color(0xffff0000),
                fontSize: 14,
                fontWeight: FontWeight.normal,
              ),
            ),
            onTap: () {
              widget.onClickLoadMore(widget.title);
            },
          )
        ],
      ),
    );
  }

  Widget _getServiceListWidget() {
    return Container(
        height: 211.0,
        width: double.infinity,
        padding: EdgeInsets.only(left: 20),
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: _controller.nearMeShopListRx.value.length,
            itemBuilder: (BuildContext context, int index) {
              return Padding(
                padding: EdgeInsets.only(right: 20),
                child:
                    // Text(index.toString()),
                    ShopItemWidget(
                        ShopItemPresenter.fromShop(
                            _controller.nearMeShopListRx.value[index]),
                        widget.onItemClick),
              );
            }));
  }
}
