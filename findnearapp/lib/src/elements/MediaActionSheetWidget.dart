import 'dart:async';
import 'dart:io';
import '../../generated/l10n.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:video_player/video_player.dart';


typedef void CallbackSelectActionSheet(int value);
typedef void OnPickImageCallback(PickedFile pickedFile);
class MediaActionSheetWidget {
  final BuildContext context;
  final ImagePicker _picker = ImagePicker();
  final TextEditingController maxWidthController = TextEditingController();
  final TextEditingController maxHeightController = TextEditingController();
  final TextEditingController qualityController = TextEditingController();

  MediaActionSheetWidget({Key key, this.context});
  void showActionSheet(String title, List<String> actions, CallbackSelectActionSheet onSelectAction) async {
    List<CupertinoActionSheetAction> listActions = [];
    actions.asMap().forEach((index,element) {
      listActions.add(CupertinoActionSheetAction(
        child: Text(element),
        onPressed: () {
          Navigator.pop(context);
          onSelectAction(index);
        },
      ));
    });
    return showCupertinoModalPopup(
      context: this.context,
      builder: (BuildContext context) => CupertinoActionSheet(
          title: Text(title),
          actions: listActions,
          cancelButton: CupertinoActionSheetAction(
            child: Text(S.of(context).cancel),
            onPressed: () {
              print("Cancel!");
              Navigator.pop(context);
            },
          )),
    );
  }

  void pickImageDialog(ImageSource source, OnPickImageCallback onPickImageCallback) async {
    await _picker.getImage(
        source: source,
        imageQuality: 80,
        maxHeight: 2048,
        maxWidth: 2048).then((pickedFile) {
      onPickImageCallback(pickedFile);
    });
  }


}

