import 'package:findnear/src/helpers/maps_marker.dart';
import 'package:flutter/material.dart';

class HeartClipWidget extends StatefulWidget {
  final Widget child;

  const HeartClipWidget({Key key, this.child}) : super(key: key);

  @override
  _HeartClipWidgetState createState() => _HeartClipWidgetState();
}

class _HeartClipWidgetState extends State<HeartClipWidget> {
  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: HeartClipper(),
      child: widget.child,
    );
  }
}

class HeartClipper extends CustomClipper<Path> {
  @override
  bool shouldReclip(oldClipper) => false;

  @override
  Path getClip(Size size) {
    final path = MapsMarker.getHeartClip(size);
    return path;
  }
}
