import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AlbumItemWidget extends StatelessWidget {
  final IconData icon;
  final String title;
  final String desc;
  final Color grStartColor;
  final Color grMiddleColor;
  final Color grEndColor;
  final VoidCallback onSelected;
  final int itemCount;
  final String imgAsset;
  final String backgroundUrl;

  AlbumItemWidget(
      {Key key,
      this.icon,
      this.title,
      this.desc,
      this.grStartColor,
      this.grEndColor,
      this.onSelected,
      this.grMiddleColor,
      this.itemCount,
      this.imgAsset,
      this.backgroundUrl})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
        onTap: () {
          if (onSelected != null) {
            onSelected();
          }
        },
        child: Container(
            width: 160,
            height: 80,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: backgroundUrl != null
                    ? CachedNetworkImageProvider(backgroundUrl)
                    : ExactAssetImage('assets/img/img_media_bg.png'),
                fit: BoxFit.cover,
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 14, horizontal: 16),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.topRight,
                    colors: [
                      grStartColor,
                      grMiddleColor,
                      grEndColor,
                    ],
                    stops: [
                      -1,
                      0.767,
                      1
                    ]),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      SvgPicture.asset(
                        imgAsset ?? "assets/svg/ic_photo.svg",
                        color: Colors.white,
                      ),
                      SizedBox(
                        width: 7.44,
                      ),
                      Expanded(
                        child: Text(
                          "${title ?? ""} ${itemCount != null && itemCount > 0 ? itemCount : ""}",
                          style: Theme.of(context).textTheme.caption.copyWith(
                                fontWeight: FontWeight.normal,
                                color: Colors.white,
                              ),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ],
                  ),
                  Text(
                    desc ?? "",
                    maxLines: 2,
                    style: Theme.of(context).textTheme.caption.copyWith(
                          fontWeight: FontWeight.normal,
                          color: Colors.white,
                        ),
                  ),
                ],
              ),
            )));
  }
}
