import 'package:findnear/main.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/PreviewUserListWidget.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LikeAndCommentStatsWidget extends StatelessWidget {
  final VoidCallback onSelectFavourite;
  final VoidCallback onSelectComment;

  final bool showComment;
  final String like_count;
  final String comment_count;
  final bool isLike;
  final List<User> likers;

  LikeAndCommentStatsWidget({
    Key key,
    this.showComment,
    this.like_count,
    this.comment_count,
    this.onSelectFavourite,
    this.onSelectComment,
    this.isLike,
    this.likers,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) =>
      Container(
        color: AppColors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            InkWell(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: EdgeInsets.only(right: 5, top: 15, bottom: 15),
                    child: AnimatedSwitcher(
                      duration: const Duration(milliseconds: 200),
                      child: SvgPicture.asset(AppImages.icLikeCircular),
                    ),
                  ),
                  Text(
                    S.of(context).care_count(like_count?.isEmpty ?? true ? "0" : like_count),
                    style: Theme
                        .of(context)
                        .textTheme
                        .bodyText2
                        .merge(TextStyle(
                      color: isLike ? AppColors.red : AppColors.textBlack,
                      fontSize: 14.0,
                    )),
                  ),
                ],
              ),
              onTap: onSelectFavourite,
            ),
            const Spacer(),
            Text(
              S.of(context).comment_count(comment_count ?? ""),
              style: Theme
                  .of(context)
                  .textTheme
                  .bodyText2
                  .merge(TextStyle(
                color: AppColors.textBlack,
                fontSize: 14.0,
              )),
            ),
            const Spacer(),
            GestureDetector(
              onTap: (){
                NavigatorUtils.navigateToListUserReact(likers: likers);
              },
              child: PreviewUserListWidget(likers: likers),
            ),
          ],
        ),
      );
}
