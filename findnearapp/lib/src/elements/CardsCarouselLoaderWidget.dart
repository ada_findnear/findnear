import 'package:flutter/material.dart';

// ignore: must_be_immutable
class CardsCarouselLoaderWidget extends StatelessWidget {
  String heroTag;

  CardsCarouselLoaderWidget({Key key, this.heroTag}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: this.heroTag == 'home_top_markets' ? 288 : 120,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: 3,
        itemBuilder: (context, index) {
          return Container(
            width: this.heroTag == 'home_top_markets' ? 292 : 200,
            margin: EdgeInsets.only(left: 20, right: 20, top: 15, bottom: 20),
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
              borderRadius: BorderRadius.all(Radius.circular(10)),
              boxShadow: [
                BoxShadow(color: Theme.of(context).focusColor.withOpacity(0.1), blurRadius: 15, offset: Offset(0, 5)),
              ],
            ),
            child: this.heroTag == 'home_top_markets' ?
            Image.asset(
              'assets/img/loading_card.gif',
              fit: BoxFit.contain,
            ) :
            Image.asset(
              'assets/img/loading_card_sm.gif',
              fit: BoxFit.contain,
            ),
          );
        },
      ),
    );
  }
}
