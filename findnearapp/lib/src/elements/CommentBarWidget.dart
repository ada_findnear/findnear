import 'dart:io';

import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:images_picker/images_picker.dart';
import 'package:velocity_x/velocity_x.dart';

import '../../generated/l10n.dart';

class CommentBarWidget extends StatelessWidget {
  final VoidCallback onTap;
  final VoidCallback onTapSticker;
  final ValueChanged<String> onSend;
  final bool canInput;
  final TextEditingController controller;
  final FocusNode focusNode;
  final Media media;
  final VoidCallback onTapRemoveMedia;
  final bool hideMedia;
  final bool isLoading;

  const CommentBarWidget({
    Key key,
    this.onTap,
    this.onTapSticker,
    this.canInput = false,
    this.onSend,
    this.controller,
    this.focusNode,
    this.media,
    this.onTapRemoveMedia,
    this.hideMedia,
    this.isLoading = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final padding = EdgeInsets.only(left: canInput ? 12 : 20, top: 3,bottom: 3);
    final boxDecoration = canInput
        ? BoxDecoration(
            color: Colors.transparent,
            border: Border.all(
              color: Color(0xFFDBDBDB),
            ),
          )
        : BoxDecoration(
            color: Colors.transparent,
            border: Border.all(
              color: Color(0xFFDBDBDB),
            ),
            borderRadius: BorderRadius.circular(20),
          );

    final mainWidget = canInput
        ? TextField(
            controller: controller,
            focusNode: focusNode,
            decoration: InputDecoration(
                border: InputBorder.none,
                hintText: S.of(context).writeComment,
                hintStyle: Theme.of(context)
                    .textTheme
                    .bodyText2
                    .merge(TextStyle(fontSize: 16))),
            minLines: 1,
            maxLines: 5,
            style: Theme.of(context)
                .textTheme
                .bodyText2
                .merge(TextStyle(fontSize: 16)),
          )
        : Text(
            S.of(context).writeComment,
            maxLines: 1,
            softWrap: false,
            overflow: TextOverflow.fade,
            style: Theme.of(context)
                .textTheme
                .caption
                .merge(TextStyle(fontSize: 16, color: Color(0XFF707070), fontWeight: FontWeight.w500)),
          );
    Widget content = InkWell(
      onTap: () {
        if (!canInput) {
          onTap?.call();
        }
      },
      child: Container(
        padding: padding,
        decoration: boxDecoration,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: <Widget>[
                Expanded(child: mainWidget),
                InkWell(
                  onTap: () {
                    if (!canInput) {
                      onTap?.call();
                    } else {
                      if (!isLoading) onTapSticker?.call();
                    }
                  },
                  child: Container(
                    padding: const EdgeInsets.only(
                        left: 12.5, top: 8, right: 6, bottom: 8),
                    child: AppImage(
                      AppImages.icImage,
                      height: 20,
                      color: Color(0xFF707070),
                    ),
                  ),
                ),
                AnimatedSwitcher(
                  duration: const Duration(milliseconds: 1000),
                  child: InkWell(
                    onTap: () {
                      if (!canInput) {
                        onTap?.call();
                      } else {
                        if (!isLoading) onSend?.call(controller?.text);
                      }
                    },
                    child: Container(
                      padding: const EdgeInsets.only(
                          left: 6, top: 8, right: 12, bottom: 8),
                      child: isLoading
                          ? LoadingView()
                          : AppImage(
                              AppImages.icSend,
                              color: Colors.red,
                              height: 20,
                            ),
                    ),
                  ),
                ),
              ],
            ),
            if (canInput && (media?.path?.isNotEmpty ?? false) && !hideMedia)
              Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  width: Get.size.width / 2,
                  child: ZStack([
                    ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: Image.file(
                        File(media.path),
                        fit: BoxFit.cover,
                        width: Get.size.width / 2,
                      ),
                    ),
                    Align(
                        child: VxBox(
                                child: Icon(
                          Icons.close,
                          color: Vx.white,
                          size: 15,
                        ))
                            .roundedFull
                            .color(Vx.red300)
                            .padding(EdgeInsets.all(2))
                            .margin(EdgeInsets.all(8))
                            .make()
                            .onTap(onTapRemoveMedia),
                        alignment: Alignment.topRight)
                  ]).paddingAll(4),
                ),
              )
          ],
        ),
      ),
    );

    // if (!canInput)
    //   content = InkWell(
    //     onTap: () {
    //       onTap?.call();
    //     },
    //     child: content,
    //   );
    return content;
  }
}
