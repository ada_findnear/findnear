
import 'package:global_configuration/global_configuration.dart';
import 'package:intl/date_symbol_data_local.dart';  //for date locale
import 'package:intl/intl.dart';

import '../models/post.dart';
import '../repository/user_repository.dart';
import '../repository/settings_repository.dart' as settingRepo;
import 'package:flutter/material.dart';

class ProfileTopicAvatarWidget extends StatelessWidget {
  final Post post;
  ProfileTopicAvatarWidget({Key key, this.post});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListTile(
      leading: CircleAvatar(
        foregroundImage: currentUser.value?.getAvatarImage() ?? NetworkImage("${GlobalConfiguration().getValue('base_url')}images/image_default.png"),
      ),
      title: Text(currentUser.value?.name ?? ""),
      subtitle: Text(
          DateFormat("HH:mm", settingRepo.setting.value.mobileLanguage.value.toString()).format(DateTime.parse(post.created_at)) + " | " +
              DateFormat("yMMMMEEEEd", settingRepo.setting.value.mobileLanguage.value.toString()).format(DateTime.parse(post.created_at))
      ),
    );
  }

}