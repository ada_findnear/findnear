import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/pages/profile_user/profile_user_view.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/services/setting_service.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:get/get.dart';
import '../models/route_argument.dart';
import '../helpers/helper.dart';
import '../repository/settings_repository.dart';
import '../models/market.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../generated/l10n.dart';
import '../helpers/app_config.dart' as config;

class CardUserBottomSheetWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Market market;

  CardUserBottomSheetWidget({Key key, this.scaffoldKey, this.market}) : super(key: key);

  @override
  _CardUserBottomSheetWidgetState createState() => _CardUserBottomSheetWidgetState();
}

class _CardUserBottomSheetWidgetState extends State<CardUserBottomSheetWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap:() {
        if (!isLoggedIn) {
          NavigatorUtils.toLogin();
          return;
        }
        Get.find<SettingService>().mapDisplayDatingDirection.value = Get.find<SettingService>().mapDisplayDating.value;
        NavigatorUtils.navigateToProfilePage(widget.market.owner.id);
      },
      child: Container(
        height: 240,
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.only(topRight: Radius.circular(20), topLeft: Radius.circular(20)),
          boxShadow: [
            BoxShadow(color: Theme.of(context).focusColor.withOpacity(0.4), blurRadius: 30, offset: Offset(0, -30)),
          ],
        ),
        child: Stack(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(0),
              child: ListView(
                padding: EdgeInsets.all(0),
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Stack(
                        fit: StackFit.loose,
                        alignment: AlignmentDirectional.bottomStart,
                        children: <Widget>[
                          Hero(
                            tag: widget.market.owner.id,
                            child: ClipRRect(
                              borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                              child: CachedNetworkImage(
                                height: 120,
                                width: double.infinity,
                                fit: BoxFit.cover,
                                imageUrl: widget.market.owner.imageCover?.thumb ?? "",
                                placeholder: (context, url) => Image.asset(
                                  'assets/img/loading.gif',
                                  fit: BoxFit.cover,
                                  width: double.infinity,
                                  height: 120,
                                ),
                                errorWidget: (context, url, error) => Icon(Icons.error_outline),
                              ),
                            ),
                          ),
                          Row(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                                padding: EdgeInsets.symmetric(horizontal: 12, vertical: 3),
                                decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(24)),
                                child: Text(
                                  widget.market.owner.gender == "Male" ? S.of(context).male : widget.market.owner.gender == "Female" ? S.of(context).female : S.of(context).other,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 0, vertical: 8),
                                padding: EdgeInsets.symmetric(horizontal: 12, vertical: 3),
                                decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(24)),
                                child: Text(
                                  widget.market.owner.age != "" ? widget.market.owner.age : "0",
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Expanded(
                              flex: 3,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    widget.market.owner.name,
                                    overflow: TextOverflow.fade,
                                    softWrap: false,
                                    style: Theme.of(context).textTheme.subtitle1,
                                  ),
                                  Text(
                                    "ID: ${widget.market.owner.code}",
                                    style: Theme.of(context).textTheme.headline6.copyWith(fontSize: 14, fontWeight: FontWeight.normal),
                                  ),
                                  Text(
                                    Helper.skipHtml(widget.market.owner.bio),
                                    overflow: TextOverflow.fade,
                                    softWrap: false,
                                    style: Theme.of(context).textTheme.caption,
                                  ),
                                  SizedBox(height: 5),
                                  Row(
                                    children: Helper.getStarsList(double.parse(widget.market.rate)),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(width: 15),
                            Expanded(
                              child: Column(
                                children: <Widget>[
                                  CircleAvatar(
                                    backgroundImage: NetworkImage(widget.market.owner.image.thumb),
                                  ),
                                  widget.market.distance > 0
                                      ? Text(
                                    Helper.getDistance(widget.market.distance, Helper.of(context).trans(text: setting.value.distanceUnit)),
                                    overflow: TextOverflow.fade,
                                    maxLines: 1,
                                    softWrap: false,
                                    style: TextStyle(fontSize: 10),
                                  )
                                      : SizedBox(height: 0)
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            Container(
              height: 30,
              width: double.infinity,
              padding: EdgeInsets.symmetric(vertical: 13, horizontal: config.App(context).appWidth(42)),
              decoration: BoxDecoration(
                color: Theme.of(context).focusColor.withOpacity(0.05),
                borderRadius: BorderRadius.only(topRight: Radius.circular(20), topLeft: Radius.circular(20)),
              ),
              child: Container(
                width: 30,
                decoration: BoxDecoration(
                  color: Theme.of(context).focusColor.withOpacity(0.8),
                  borderRadius: BorderRadius.circular(3),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
