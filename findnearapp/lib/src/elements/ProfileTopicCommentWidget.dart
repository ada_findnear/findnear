import 'package:findnear/src/models/comment.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart'; //for date format
import 'package:cached_network_image/cached_network_image.dart';
import '../repository/settings_repository.dart' as settingRepo;

class ProfileTopicCommentWidget extends StatelessWidget {
  final Comment comment;
  final ValueChanged<String> previewTopicPhoto;
  final bool allowDelete;
  final VoidCallback onDeletePressed;

  ProfileTopicCommentWidget({
    Key key,
    this.comment,
    this.previewTopicPhoto,
    this.allowDelete,
    this.onDeletePressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: ListTile(
        leading: CircleAvatar(
          foregroundImage: comment.getAvatarImage(),
        ),
        title: Text(comment.creatorName),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(comment.content,
                style: TextStyle(
                  color: Theme.of(context).hintColor,
                )),
            comment.image.id != null
                ? GestureDetector(
                    onTap: () {
                      previewTopicPhoto(comment?.image?.url);
                    },
                    child: Container(
                        child: CachedNetworkImage(
                      imageUrl: comment?.image?.url,
                      placeholder: (context, url) => CircularProgressIndicator(),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    )),
                  )
                : Text(''),
            Row(
              children: [
                Expanded(
                  child: Text(
                    DateFormat("HH:mm", settingRepo.setting.value.mobileLanguage.value.toString()).format(DateTime.parse(comment.createdAt)) +
                        " | " +
                        DateFormat("yMMMMEEEEd", settingRepo.setting.value.mobileLanguage.value.toString()).format(
                          DateTime.parse(comment.createdAt),
                        ),
                    style: TextStyle(fontSize: 13.0, color: Theme.of(context).hintColor, fontWeight: FontWeight.w100),
                  ),
                ),
                // GestureDetector(
                //   onTap: () {},
                //   child: Container(
                //     height: 10,
                //     color: Colors.red,
                //     child: Icon(Icons.delete),
                //   ),
                // )
              ],
            )
          ],
        ),
        contentPadding: EdgeInsets.only(left: 15),
        trailing: allowDelete == true
            ? GestureDetector(
                onTap: onDeletePressed,
                child: Container(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                  child: Icon(
                    Icons.delete,
                    size: 16,
                    color: Theme.of(context).hintColor,
                  ),
                ),
              )
            : null,
      ),
    );
  }
}
