import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../generated/l10n.dart';

class ProfileWriteTopicWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 50,
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.circular(5),
        ),
        padding: EdgeInsets.symmetric(vertical: 16, horizontal: 12),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Text(
                S.of(context).whatIsOnYourMind,
                style: TextStyle(
                  fontSize: 14.0,
                  color: Color.fromRGBO(51, 51, 51, 1),
                ),
              ),
            ),
            SvgPicture.asset(
              "assets/svg/ic_photo.svg",
              color: Color(0xFF00a00c),
            ),
            SizedBox(
              width: 1,
            )
          ],
        ));
  }

  ProfileWriteTopicWidget();
}
