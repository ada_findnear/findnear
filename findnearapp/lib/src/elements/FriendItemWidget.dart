import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/models/media.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../controllers/profile_controller.dart';
import '../models/user.dart';
typedef showDialogUnfriendCallBack(String);
typedef openChatWithFriend(User);
typedef OnRemoveUserInGroupChat();

class FriendItemWidget extends StatefulWidget {
  final User user;
  final bool isListFriend;
  final showDialogUnfriendCallBack onShowDialogUnfriend;
  final openChatWithFriend onOpenChatWithFriend;
  final OnRemoveUserInGroupChat onRemoveUserInGroupChat;

  FriendItemWidget({Key key, this.user, this.isListFriend, this.onShowDialogUnfriend, this.onOpenChatWithFriend, this.onRemoveUserInGroupChat})
      : super(key: key);

  @override
  _FriendItemWidgetState createState() => _FriendItemWidgetState();
}

class _FriendItemWidgetState extends StateMVC<FriendItemWidget> {
  ProfileController _con;
  final String result_request = '';
  final List ListDeniedFriend = [];
  final List ListAcceptedFriend = [];
  bool isSubmitRequest = false;


  _FriendItemWidgetState() : super(ProfileController()) {
    _con = controller;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container (
      color: Colors.transparent,
      padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 8.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Stack(
            children: [
              SizedBox(
                width: 50.w,
                height: 50.w,
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(60)),
                  child: CachedNetworkImage(
                    height: 60.w,
                    width: 60.w,
                    fit: BoxFit.cover,
                    imageUrl: widget.user.image.thumb,
                    placeholder: (context, url) => Image.asset(
                      'assets/img/loading.gif',
                      fit: BoxFit.cover,
                      width: double.infinity,
                      height: 140.w,
                    ),
                    errorWidget: (context, url, error) => CachedNetworkImage(
                      imageUrl: (new Media()).thumb,
                      fit: BoxFit.cover,
                      width: double.infinity,
                      height: 140.w,
                    ),
                  ),
                ),
              ),
              Positioned(
                bottom: 3.w,
                right: 3.w,
                width: 12.w,
                height: 12.w,
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                ),
              )
            ],
          ),
          SizedBox(width: 15.w),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        widget.user.name,
                        overflow: TextOverflow.fade,
                        softWrap: false,
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1
                            .merge(TextStyle(fontWeight: FontWeight.w800)),
                      ),
                    ),
                  ],
                ),
                (ListAcceptedFriend.contains(widget.user.id) || ListDeniedFriend.contains(widget.user.id)) ? Row(
                  children: [
                    Text(
                      ListAcceptedFriend.contains(widget.user.id) ? S.of(context).requestAccepted :
                      (ListDeniedFriend.contains(widget.user.id) ? S.of(context).requestRemoved : ''),
                      style: TextStyle(color: Theme.of(context).hintColor),

                    )
                  ],
                ) : SizedBox(height: 8.w),
                (ListAcceptedFriend.contains(widget.user.id) || ListDeniedFriend.contains(widget.user.id) || widget.isListFriend) ? SizedBox(height: 0,) : Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(textStyle: TextStyle(fontSize: 14.sp)),
                        onPressed: () {
                          _con.addFriend(widget.user.id, "confirm");
                          setState(() {
                            this.ListAcceptedFriend.add(widget.user.id);
                          });
                        },
                        child: Text(S.of(context).confirm,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    SizedBox(width: 10.w),
                    Expanded(
                      flex: 1,
                       child: ElevatedButton(
                         style: ElevatedButton.styleFrom(textStyle: TextStyle(fontSize: 14.sp), primary: Colors.black12),
                         onPressed: () {
                           _con.addFriend(widget.user.id, "deny");
                           setState(() {
                             this.ListDeniedFriend.add(widget.user.id);
                           });
                         },
                         child: Text(S.of(context).delete,
                             style: TextStyle(fontWeight: FontWeight.bold)
                         ),
                       ),
                    )
                  ],
                )
              ],
            ),
          ),
          widget.isListFriend ? Row(
            children: [
              if (widget.onShowDialogUnfriend != null)
              IconButton(
                icon: Icon(
                  Icons.person_remove,
                  size: 24.w,
                  color: Theme.of(context).focusColor,
                ),
                onPressed: () {
                  widget.onShowDialogUnfriend(widget.user.id);
                },
              ),

            ],
          ) : SizedBox(height: 0, width: 0,),
          if (widget.onRemoveUserInGroupChat != null)
          IconButton(
            icon: Icon(
              Icons.remove_circle_outline,
              size: 24.w,
              color: Theme.of(context).focusColor,
            ),
            onPressed: () {
              widget.onRemoveUserInGroupChat();
            },
          ),
        ],
      ),
    );
  }

}