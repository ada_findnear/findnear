import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:findnear/src/pages/find_near_page/find_near_controller.dart';
import 'package:findnear/src/pages/shop_detail/view.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:findnear/src/widgets/dialog/html_content_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../helpers/app_config.dart' as config;
import '../helpers/helper.dart';
import '../models/route_argument.dart';
import '../models/slide.dart';
import 'HomeSliderLoaderWidget.dart';

class HomeSliderWidget extends StatefulWidget {
  final List<Slide> slides;

  @override
  _HomeSliderWidgetState createState() => _HomeSliderWidgetState();

  HomeSliderWidget({Key key, this.slides}) : super(key: key);
}

class _HomeSliderWidgetState extends State<HomeSliderWidget> {
  int _current = 0;
  AlignmentDirectional _alignmentDirectional;
  FindNearController _controller = Get.find();

  @override
  void initState() {
    super.initState();
    _controller.getForSlides();
  }

  @override
  Widget build(BuildContext context) {
    return widget.slides == null || widget.slides.isEmpty
        ? HomeSliderLoaderWidget()
        : Stack(
            alignment: _alignmentDirectional ?? AlignmentDirectional.topStart ,
            fit: StackFit.passthrough,
            children: <Widget>[
              CarouselSlider(
                options: CarouselOptions(
                  autoPlay: true,
                  autoPlayInterval: Duration(seconds: 5),
                  height: 180.w,
                  viewportFraction: 0.8,
                  onPageChanged: (index, reason) {
                    setState(() {
                      _current = index;
                    });
                  },
                ),
                items: widget.slides.map((Slide slide) {
                  return Builder(
                    builder: (BuildContext context) {
                      return InkWell(
                        onTap: () {
                          print('quanth: press item: id = ${slide.id}');
                          print('quanth: press item: refId = ${slide.refId}');
                          print('quanth: press item: refType = ${slide.refType}');
                          print('quanth: press item: img = ${slide.imageUrl}');
                          NavigatorUtils.toShopDetailPage(shopId: int.tryParse(slide.refId));
                        },
                        child: Container(
                          margin: EdgeInsets.symmetric(
                              vertical: 20.w, horizontal: 10.w),
                          height: 140.w,
                          decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                  color: Theme.of(context)
                                      .focusColor
                                      .withOpacity(0.15),
                                  blurRadius: 15,
                                  offset: Offset(0, 2)),
                            ],
                          ),
                          child: Stack(
                            children: [
                              ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                child: CachedNetworkImage(
                                  height: 140.w,
                                  width: double.infinity,
                                  fit: BoxFit.cover,/*Helper.getBoxFit(slide.imageUrl),*/
                                  imageUrl: slide.imageUrl,
                                  placeholder: (context, url) => Image.asset(
                                    'assets/img/loading.gif',
                                    fit: BoxFit.cover,
                                    width: double.infinity,
                                    height: 140.w,
                                  ),
                                  errorWidget: (context, url, error) {
                                    print('quanth errorWidget: url = ${url}');
                                      return Icon(Icons.error_outline);
                                  }
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  );
                }).toList(),
              ),
            ],
          );
  }
}
