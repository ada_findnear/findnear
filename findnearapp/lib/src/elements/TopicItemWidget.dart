import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart'; //for date format

import '../models/post.dart';
import '../repository/settings_repository.dart' as settingRepo;
import 'ProfileTopicLikesWidget.dart';
import 'ProfileTopicPhotosWidget.dart';
import 'ProfileTopicTextWidget.dart';

typedef TopicItemPreviewPhotoCallback(int);
typedef ToggleLikeButton(int);
typedef DetailTopicCallback();

class TopicItemWidget extends StatelessWidget {
  final TopicItemPreviewPhotoCallback onPreviewPhoto;
  final VoidCallback onOpenSetting;
  final ToggleLikeButton toggleLikePost;
  final Post post;
  final DetailTopicCallback openDetailTopic;
  final bool isMyProfile;
  final VoidCallback onPressed;
  final VoidCallback onUserMenuPressed;

  TopicItemWidget({
    Key key,
    this.onPreviewPhoto,
    this.onOpenSetting,
    this.post,
    this.toggleLikePost,
    this.openDetailTopic,
    this.isMyProfile,
    this.onPressed,
    this.onUserMenuPressed,
  });

  @override
  Widget build(BuildContext context) {
    print("TopicItemWidget Build");
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          decoration: BoxDecoration(
            color: Color.fromRGBO(255, 219, 219, 1),
            borderRadius: BorderRadius.circular(6),
          ),
          padding: EdgeInsets.symmetric(vertical: 4, horizontal: 10),
          child: Text(
            post?.created_at != null
                ? DateFormat(
                    "MMMMd",
                    settingRepo.setting.value.mobileLanguage.value.toString(),
                  ).format(DateTime.parse(post.created_at))
                : "",
            style: TextStyle(
              color: Color(0xFF333333),
              fontSize: 10.0,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(
          height: 9.0,
        ),
        GestureDetector(
          onTap: onPressed,
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
            ),
            // padding: EdgeInsets.only(top: 12, left: 15, right: 13),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(left: 15, right: 15, top: 12, bottom: 10),
                        child: ProfileTopicTextWidget(expandText: false, content: post.content ?? ""),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        if (isMyProfile) {
                          onOpenSetting?.call();
                        } else {
                          onUserMenuPressed?.call();
                        }
                      },
                      child: Container(
                        width: 48,
                        height: 40,
                        child: Center(
                          child: Icon(
                            Icons.more_horiz,
                            color: Color(0xFF333333),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(left: 15, right: 15),
                  child: ProfileTopicPhotosWidget(
                    onClickPhoto: (index) {
                      print(index);
                      if (onPreviewPhoto != null) {
                        onPreviewPhoto(index);
                      }
                    },
                    post: post,
                    onExpandClicked: () => {openDetailTopic()},
                    maxImages: 4,
                  ),
                ),
                // SizedBox(
                //   height: 17,
                // ),
                Container(
                  margin: EdgeInsets.only(left: 15, right: 15),
                  child: ProfileTopicLikesWidget(
                      showComment: true,
                      likers_count: post.likers_count,
                      isLike: post?.isLike ?? false,
                      comment_count: post.comments_count,
                      onOpenSetting: () {
                        if (onOpenSetting != null) {
                          onOpenSetting();
                        }
                      },
                      onSelectFavourite: () {
                        this.toggleLikePost(post.id);
                      },
                      isMyProfile: isMyProfile),
                )
              ],
            ),
          ),
        )
      ],
    );
  }
}
