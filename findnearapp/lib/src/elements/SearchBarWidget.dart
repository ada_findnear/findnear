import 'package:flutter/material.dart';

import '../../generated/l10n.dart';
import '../elements/SearchWidget.dart';
import '/src/helpers/app_config.dart' as config;
import '../repository/search_repository.dart';
class SearchBarWidget extends StatefulWidget {
  ValueChanged onClickFilter;
  SearchBarWidget({ Key key, ValueChanged onClickFilter}) : super(key: key);

  @override
  SearchBarWidgetState createState() => SearchBarWidgetState();
}

class SearchBarWidgetState extends State<SearchBarWidget> {

  String searchText = "";

  @override
  void initState() {
    super.initState();
    getSearchText();
  }
  void getSearchText() async {
    String search = await getRecentSearch();
    setState(() {
      searchText = search;
    });
  }
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(SearchModal());
      },
      child: Container(
        padding: EdgeInsets.all(9),
        decoration: BoxDecoration(
            color: Colors.transparent,
            border: Border.all(
              color: Theme.of(context).focusColor.withOpacity(0.2),
            ),
            borderRadius: BorderRadius.circular(24)),
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 12, left: 0),
              child: Icon(Icons.search, color: Theme.of(context).focusColor),
            ),
            Expanded(
              child: Text(
                searchText != '' ? searchText : "Tìm kiếm cửa hàng hoặc sản phẩm",
                maxLines: 1,
                softWrap: false,
                overflow: TextOverflow.fade,
                style: Theme.of(context).textTheme.caption.merge(TextStyle(fontSize: 13)),
              ),
            ),
            // SizedBox(width: 8),
            // InkWell(
            //   onTap: () {
            //     onClickFilter('e');
            //   },
            //   child: Container(
            //     padding: const EdgeInsets.only(right: 10, left: 10, top: 5, bottom: 5),
            //     decoration: BoxDecoration(
            //       borderRadius: BorderRadius.all(Radius.circular(5)),
            //       color: Theme.of(context).focusColor.withOpacity(0.1),
            //     ),
            //     child: Wrap(
            //       crossAxisAlignment: WrapCrossAlignment.center,
            //       spacing: 4,
            //       children: [
            //         Text(
            //           S.of(context).filter,
            //           style: TextStyle(color: Theme.of(context).hintColor),
            //         ),
            //         Icon(
            //           Icons.filter_list,
            //           color: Theme.of(context).hintColor,
            //           size: 21,
            //         ),
            //       ],
            //     ),
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}
