import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/helpers/date_utils.dart';
import 'package:findnear/src/models/newsfeed_comment.dart';
import 'package:findnear/src/pages/preview_media.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../repository/settings_repository.dart' as settingRepo;

import 'CircularAvatarWidget.dart';

typedef OnReplyCallback = void Function(NewsfeedComment reply);

class CommentReplyWidget extends StatelessWidget {
  final NewsfeedComment comment;
  final VoidCallback onLikePress;
  final VoidCallback onLongPress;
  final OnReplyCallback onReplyCallback;

  const CommentReplyWidget({
    Key key,
    this.comment,
    this.onLikePress,
    this.onLongPress,
    this.onReplyCallback,
  }) : super(key: key);

  bool get isAReply => !comment.isReply;

  @override
  Widget build(BuildContext context) => GestureDetector(
    onLongPress: onLongPress,
    child: Container(
          padding:
              EdgeInsets.only(top: 8, bottom: 8, left: isAReply ? 0 : 50),
          child: IntrinsicHeight(
            child: Row(
              children: [
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    margin: const EdgeInsets.only(top: 4),
                    child: CircularAvatarWidget(
                      user: comment?.creator,
                      imageUrl: comment?.creator?.image?.icon ?? "",
                      size: 38,
                      hasBorder: true,
                      hasLiveIndicator: comment?.creator?.isLiveStreaming ?? false,
                    ),
                  ),
                ),
                const SizedBox(width: 9),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              comment?.creator?.name ?? "",
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6
                                  .merge(TextStyle(
                                    color: AppColors.textBlack,
                                    fontSize: 15,
                                  )),
                            ),
                          ),
                        ],
                      ),
                      Text(
                        comment?.content ?? "",
                        style:
                            Theme.of(context).textTheme.bodyText2.merge(TextStyle(
                                  fontSize: 17, // needs more adjustment
                                  color: AppColors.textBlack,
                                )),
                      ),
                      const SizedBox(height: 1),
                      Row(
                        children: [
                          Builder(
                            builder: (context) {
                              String text = "";
                              if (comment?.createdAt != null) {
                                final date = DateTime.tryParse(comment.createdAt);
                                if (date != null)
                                  text = AppDateFormatUtils
                                      .getVerboseDateTimeRepresentation(date);
                              }
                              return Text(
                                text,
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1
                                    .merge(TextStyle(
                                      color: AppColors.gray2,
                                      fontSize: 12,
                                    )),
                              );
                            },
                          ),
                          GestureDetector(
                            behavior: HitTestBehavior.opaque,
                            onTap: () => onReplyCallback?.call(comment),
                            child: Container(
                              padding: const EdgeInsets.symmetric(horizontal: 10),
                              child: Text(
                                "Trả lời",
                                style: Theme.of(context).textTheme.subtitle1.merge(
                                    TextStyle(
                                        color: AppColors.textBlack, fontSize: 12)),
                              ),
                            ),
                          ),
                        ],
                      ),
                      if (comment?.media?.isNotEmpty ?? false)
                        GestureDetector(
                          onTap: () {
                            _previewTopicPhoto(context, comment.media.first.url,
                                thumbUrl: comment.media.first.thumb);
                          },
                          child: Container(
                            width: double.infinity,
                            margin: const EdgeInsets.only(top: 8),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(5),
                              child: CachedNetworkImage(
                                  imageUrl: comment.media.first.url),
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
                GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () => onLikePress?.call(),
                  child: Container(
                    padding: const EdgeInsets.all(16),
                    child: Center(
                      child: AnimatedSwitcher(
                        duration: const Duration(milliseconds: 200),
                        child: (comment?.isLike ?? false)
                            ? AppImage(
                          AppImages.icLike,
                          color: AppColors.red,
                        )
                            : AppImage(AppImages.icLikeOutlined),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
  );

  void _previewTopicPhoto(BuildContext context, String url, {String thumbUrl}) {
    String path = url;
    Get.to(
      () => PreviewMediaWidget(
        photoPath: path,
        thumbPath: thumbUrl,
      ),
    );
  }
}
