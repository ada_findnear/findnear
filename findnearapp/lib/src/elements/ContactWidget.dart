import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../generated/l10n.dart';
import 'package:url_launcher/url_launcher.dart';

// ignore: must_be_immutable
class ContactWidget extends StatelessWidget {
  dynamic contact;

  ContactWidget({Key key, this.contact}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(15, 14, 15, 14),
      width: double.infinity,
      margin: EdgeInsets.only(left: 0, right: 0, top: 0, bottom: 0),
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.all(Radius.circular(20)),
        boxShadow: [
          BoxShadow(
              color: Theme.of(context).focusColor.withOpacity(0.1),
              blurRadius: 15,
              offset: Offset(0, 5)),
        ],
      ),
      child: Row(
        children: [
          SizedBox(
            width: 50,
            height: 50,
            child: CircleAvatar(
              radius: 40,
              backgroundImage: AssetImage(AppImages.icSupporterAvatar),
            ),
          ),
          SizedBox(width: 8),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  contact['name'],
                  style: TextStyle(
                      color: Colors.black87,
                      fontSize: 14,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 5),
                Text(
                  contact['description'],
                  style: TextStyle(color: Colors.black87, fontSize: 12),
                ),
              ],
            ),
          ),
          InkWell(
            onTap: () async {
              if (await canLaunch("tel:" + contact["phone"])) {
                launch("tel:" + contact["phone"]);
              }
            },
            child: AppImage(
              AppImages.icCallPhone,
              width: 35,
              height: 35,
            ),
          ),
          SizedBox(width: 10),
          InkWell(
              onTap: () async {
                if (await canLaunch(contact["zalo"])) {
                  launch(contact["zalo"]);
                }
              },
              child: AppImage(AppImages.icZalo, width: 35, height: 35)),
        ],
      ),
    );
  }
}
