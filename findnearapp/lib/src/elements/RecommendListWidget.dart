import 'dart:ui';

import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/models/shop.dart';
import 'package:findnear/src/pages/find_near_page/find_near_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'ShopItemWidget.dart';

typedef OnChangeRecommendType(int id);

class RecommendListWidget extends StatefulWidget {
  String title;
  Function onClickLoadMore;
  Function onItemClick;
  final OnChangeRecommendType onChangeRecommendType;

  RecommendListWidget(this.title, this.onClickLoadMore, this.onItemClick,
      this.onChangeRecommendType);

  @override
  _RecommendListWidgetState createState() => _RecommendListWidgetState();
}

class _RecommendListWidgetState extends State<RecommendListWidget> {
  FindNearController _controller = Get.find();
  int type = -1;
  int SERVICE_NEAR_ME = 0;
  int RECOMMEND_FOR_YOU = 1;
  List<Shop> shopList;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      await _controller.getRecommend();
      _controller.getRecommendedShop();
    });
  }

  Widget build(BuildContext context) {
    return Obx(() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _getHeaderWidget(),
          if (_controller.recommendTypes.length > 0) _getCategoriesWidget(),
          if (_controller.recommendedShopListRx.length > 0)
            _getServiceListWidget(),
        ],
      );
    });
  }

  Widget _getHeaderWidget() {
    return Container(
      padding: EdgeInsets.all(20),
      child: Row(
        children: [
          Image.asset(
            'assets/img/premium-quality.png',
            fit: BoxFit.none,
          ),
          SizedBox(width: 4),
          Text(
            widget.title,
            style: TextStyle(
              color: Color(0xff000000),
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          Spacer(),
          InkWell(
            child: Text(
              'Xem thêm',
              style: TextStyle(
                color: Color(0xffff0000),
                fontSize: 14,
                fontWeight: FontWeight.normal,
              ),
            ),
            onTap: () {
              widget.onClickLoadMore(widget.title);
            },
          )
        ],
      ),
    );
  }

  Widget _getServiceListWidget() {
    return Container(
        height: 211.0,
        width: double.infinity,
        padding: EdgeInsets.only(left: 20),
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: _controller.recommendedShopListRx.length,
            itemBuilder: (BuildContext context, int index) {
              return Padding(
                padding: EdgeInsets.only(right: 20),
                child:
                    // Text(index.toString()),
                    ShopItemWidget(
                        ShopItemPresenter.fromShop(
                            _controller.recommendedShopListRx[index]),
                        widget.onItemClick),
              );
            }));
  }

  Widget _getCategoriesWidget() {
    return Container(
      height: 32,
      margin: EdgeInsets.only(bottom: 16.0),
      child: ListView.separated(
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.only(left: 20),
        itemBuilder: (context, index) {
          final recommendType = _controller?.recommendTypes[index];
          return Obx(() => TextButton(
                onPressed: () {
                  _controller.setCurrentCategory(recommendType);
                  widget.onChangeRecommendType(recommendType?.id);
                },
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(
                      _controller.currentRecommend == recommendType
                          ? AppColors.red500
                          : AppColors.grayDark),
                  padding: MaterialStateProperty.all<EdgeInsets>(
                      EdgeInsets.symmetric(horizontal: 8.0)),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                  ),
                ),
                child: Container(
                  child: Text(
                    recommendType?.name ?? "",
                    style: TextStyle(
                      color: _controller.currentRecommend == recommendType
                          ? AppColors.white
                          : AppColors.textBlack,
                    ),
                  ),
                ),
              ));
        },
        separatorBuilder: (context, index) {
          return SizedBox(
            width: 8.0,
          );
        },
        itemCount: _controller?.recommendTypes?.length,
      ),
    );
  }
}
