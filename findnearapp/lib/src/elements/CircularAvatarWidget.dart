import 'package:cached_network_image/cached_network_image.dart';
import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/helpers/app_config.dart' as config;
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/profile_user/profile_user_view.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/navigator_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CircularAvatarWidget extends StatelessWidget {
  final String imageUrl;
  final User user;
  final double size;
  final bool hasLiveIndicator;
  final bool hasBorder;
  final Color borderColor;
  static const double liveIndicatorAdditionalRadius = 8;
  final bool replace;

  const CircularAvatarWidget({
    Key key,
    this.imageUrl,
    this.user,
    this.size = 38,
    this.hasLiveIndicator = false,
    this.hasBorder = false,
    this.borderColor = AppColors.red,
    this.replace = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget circularImage = ClipRRect(
      borderRadius: BorderRadius.circular(size / 2),
      child: Builder(
        builder: (context) {
          var url = "";
          if(imageUrl != null){
            url = imageUrl;
          }else if(user?.avatar != null && user?.avatar != "null"){
            url = user.avatar;
          }else if(user?.image?.url != null){
            url = user?.image?.url;
          }

          if (url.isEmpty)
            return AvatarPlaceholder(size: size);
          return CachedNetworkImage(
            imageUrl: url,
            fit: BoxFit.cover,
            width: size,
            height: size,
            placeholder: (context, url) => CupertinoActivityIndicator(),
            errorWidget: (context, url, error) => AvatarPlaceholder(size: size),
          );
        },
      ),
    );

    if (hasBorder ?? false) {
      circularImage = Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(size),
          border: Border.all(color: borderColor),
        ),
        child: circularImage,
      );
    }
    if (hasLiveIndicator ?? false) {
      circularImage = Stack(
        clipBehavior: Clip.none,
        alignment: Alignment.center,
        children: [
          Positioned(child: circularImage),
          Container(
            decoration: BoxDecoration(
              borderRadius:
                  BorderRadius.circular(size + liveIndicatorAdditionalRadius),
              border: Border.all(color: AppColors.red, width: 2),
            ),
            width: size + liveIndicatorAdditionalRadius,
            height: size + liveIndicatorAdditionalRadius,
          ),
          Positioned(
            top: size - 2,
            child: Container(
              decoration: BoxDecoration(
                color: config.CustomColors().mainColor(1),
                borderRadius: BorderRadius.circular(3),
              ),
              padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 2),
              child: Text(
                "LIVE",
                style: Theme.of(context).textTheme.headline6.merge(TextStyle(
                      color: AppColors.white,
                      fontSize: 10,
                    )),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ],
      );
    }
    return GestureDetector(
      onTap: (!(user?.isMe() ?? true)) ? _navigateToProfile : null,
      child: circularImage,
    );
  }

  void _navigateToProfile() {
    if (!isLoggedIn) {
      NavigatorUtils.toLogin();
      return;
    };
    if (user != null && !user.isMe()) {
      NavigatorUtils.navigateToProfilePage(user.id, isReplace: replace);
    }
  }
}

class AvatarPlaceholder extends StatelessWidget {
  final double size;
  const AvatarPlaceholder({Key key, this.size = 38}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size,
      height: size,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(size / 2),
        child: Image.asset(AppImages.icAvatarPlaceholder),
      ),
    );
  }
}

