import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/elements/CircularAvatarWidget.dart';
import 'package:findnear/src/models/user.dart';
import 'package:flutter/material.dart';

class PreviewUserListWidget extends StatelessWidget {
  static const double radius = 9;
  static const double overlappingPixels = 5;
  final List<User> likers;

  const PreviewUserListWidget({Key key, this.likers = const []})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final int count = likers.length > 3 ? 3 : likers.length;
    double firstItemOffset = 0;
    double secondItemOffset = 0;
    if (count == 2) {
      firstItemOffset = radius * 2 - overlappingPixels;
    } else  if (count > 2) {
      secondItemOffset = radius * 2 - overlappingPixels;
      firstItemOffset = secondItemOffset + radius * 2 - overlappingPixels;
    }

    return Container(
      width: 3 * 2 * radius - (3 - 1) * overlappingPixels,
      height: radius * 2 + 1,
      child: Stack(
        alignment: Alignment.centerLeft,
        clipBehavior: Clip.none,
        children: [
          if (likers.length > 0)
            Positioned(
              right: firstItemOffset,
              child: CircularAvatarWidget(
                imageUrl: likers.elementAt(0).image.url,
                size: 2 * radius,
                hasBorder: true,
                borderColor: AppColors.white,
              ),
            ),
          if (likers.length > 1)
            Positioned(
              right: secondItemOffset,
              child: CircularAvatarWidget(
                imageUrl: likers.elementAt(1).image.url,
                size: 2 * radius,
                hasBorder: true,
                borderColor: AppColors.white,
              ),
            ),
          if (likers.length > 2)
            Positioned(
              right: 0,
              child: _buildMoreUsersWidget(context),
            ),
        ],
      ),
    );
  }

  Widget _buildMoreUsersWidget(BuildContext context) => Container(
        width: 2 * radius,
        height: 2 * radius,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(radius),
          color: AppColors.gray2,
        ),
        child: Center(
          child: Text(
            "+${likers.length - 2}",
            style: Theme.of(context)
                .textTheme
                .bodyText1
                .merge(TextStyle(fontSize: 10, color: AppColors.white)),
          ),
        ),
      );
}
