import 'package:findnear/src/commons/app_images.dart';
import 'package:findnear/src/elements/SearchWidget.dart';
import 'package:findnear/src/pages/search/search_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../generated/l10n.dart';

class SearchBarNewsfeedWidget extends StatelessWidget {

  const SearchBarNewsfeedWidget({Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Get.to(() => SearchPage());
      },
      child: Container(
        height: 40,
        padding: const EdgeInsets.symmetric(horizontal: 12),
        decoration: BoxDecoration(
            color: Colors.transparent,
            border: Border.all(
              color: Theme.of(context).focusColor.withOpacity(0.2),
            ),
            borderRadius: BorderRadius.circular(20)),
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 8, left: 0),
              child: Icon(Icons.search, color: Theme.of(context).focusColor),
            ),
            Expanded(
              child: Text(
                S.of(context).searchForFriends,
                maxLines: 1,
                softWrap: false,
                overflow: TextOverflow.fade,
                style: Theme.of(context)
                    .textTheme
                    .caption
                    .merge(TextStyle(fontSize: 13)),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
