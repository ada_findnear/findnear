import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:findnear/src/global/global_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_html/shims/dart_ui_real.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get_storage/get_storage.dart';
import 'package:package_info/package_info.dart';
import 'package:permission_handler/permission_handler.dart';

import 'replacement_map.dart';

const ASCII_START = 33;
const ASCII_END = 126;
const NUMERIC_START = 48;
const NUMERIC_END = 57;

class Utils {
  Utils._() {}

  static double radiusFromCameraZoom(double zoom) {
    return 40000 / pow(2, zoom) * 2;
  }

  static double distanceBetween2Location(lat1, lng1, lat2, lng2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lng2 - lng1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }

  ///Search
  static bool isTextContainKeyword({String text = "", String keyword = ""}) {
    final newText =
        String.fromCharCodes(replaceCodeUnits(text.codeUnits)).toLowerCase();
    final newKeyword =
        String.fromCharCodes(replaceCodeUnits(keyword.codeUnits)).toLowerCase();
    final isContain = newText.contains(newKeyword);
    return isContain;
  }

  ///Theme
  static String themeMode2Code(ThemeMode themeMode) {
    switch (themeMode) {
      case ThemeMode.light:
        return 'light';
      case ThemeMode.dark:
        return 'dark';
      default:
        return 'system';
    }
  }

  static ThemeMode code2ThemeMode(String code) {
    switch (code) {
      case 'light':
        return ThemeMode.light;
      case 'dark':
        return ThemeMode.dark;
      default:
        return ThemeMode.system;
    }
  }

  /// Determine the current position of the device.
  ///
  /// When the location services are not enabled or permissions
  /// are denied the `Future` will return an error.
  static Future<Position> determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return await Geolocator.getCurrentPosition();
  }

  static Future<String> getAppId() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    return packageInfo.packageName;
  }

  static Future<Uint8List> captureWidget(RenderRepaintBoundary boundary) async {
    if (boundary == null) return null;

    var image = await boundary.toImage();
    ByteData byteData = await image.toByteData(format: ImageByteFormat.png);
    return byteData.buffer.asUint8List();
  }

  static int randomBetween(int from, int to) {
    if (from > to) throw Exception('$from cannot be > $to');
    var rand = Random();
    return ((to - from) * rand.nextDouble()).toInt() + from;
  }

  static String randomString(int length,
      {int from: ASCII_START, int to: ASCII_END}) {
    return String.fromCharCodes(
        List.generate(length, (index) => randomBetween(from, to)));
  }

  static String randomNumeric(int length) =>
      randomString(length, from: NUMERIC_START, to: NUMERIC_END);

  static Future<Map> getTurnCredential(String host, int port) async {
    HttpClient client = HttpClient(context: SecurityContext());
    client.badCertificateCallback =
        (X509Certificate cert, String host, int port) {
      print(
          'getTurnCredential: Allow self-signed certificate => $host:$port. ');
      return true;
    };
    var url =
        'https://$host:$port/api/turn?service=turn&username=flutter-webrtc';
    var request = await client.getUrl(Uri.parse(url));
    var response = await request.close();
    var responseBody = await response.transform(Utf8Decoder()).join();
    print('getTurnCredential:response => $responseBody.');
    Map data = JsonDecoder().convert(responseBody);
    return data;
  }

  static String switchDomainTenor(String gifUrl) {
    if (gifUrl == null || gifUrl.isEmpty) return gifUrl;
    if (!gifUrl.contains("media.tenor.com")) return gifUrl;
    return gifUrl.replaceFirst("media.tenor.com", "media-ten.z-cdn.me");
  }

  static void checkMicrophoneGrant(
      {VoidCallback granted, VoidCallback notGranted}) async {
    // You can can also directly ask the permission about its status.
    if (await Permission.microphone.isGranted) {
      granted.call();
    } else
      notGranted.call();
  }

  static downloadFile(String path, Function(int percent) onProgress) async {
    final fileName = path.split('/').last;
    final savePath = '${GlobalData.instance.documentPath}/$fileName';
    Dio().downloadUri(Uri.parse(path), savePath,
        onReceiveProgress: (count, total) {
      onProgress(count * 100 ~/ total);
    }, deleteOnError: true);
  }

  static Future<Directory> getAndroidDownloadDir() async {
    Directory directory;
    try {
      if (await Permission.storage.request().isGranted) {
        directory = Directory('/storage/emulated/0/Download');
        if (!await directory.exists())
          directory = await getExternalStorageDirectory();
      }
    } catch (err) {
      print("Cannot get download folder path");
    }
    return directory;
  }
}

enum LoadingState {
  idle,
  loading,
  success,
  failure,
}

enum MessageStates {
  idle,
  savePostSuccessfully,
  unsavePostSuccessfully,
  hidePostSuccessfully,
  unhidePostSuccessfully,
}

InputDecoration getInputDecoration(
  BuildContext context, {
  String hintText,
  String labelText,
}) {
  return new InputDecoration(
    hintText: hintText,
    labelText: labelText,
    hintStyle: Theme.of(context).textTheme.bodyText2.merge(
          TextStyle(color: Theme.of(context).focusColor),
        ),
    enabledBorder: UnderlineInputBorder(
      borderSide: BorderSide(
        color: Theme.of(context).hintColor.withOpacity(0.2),
      ),
    ),
    focusedBorder: UnderlineInputBorder(
      borderSide: BorderSide(color: Theme.of(context).hintColor),
    ),
    floatingLabelBehavior: FloatingLabelBehavior.auto,
    labelStyle: Theme.of(context).textTheme.bodyText2.merge(
          TextStyle(color: Theme.of(context).hintColor),
        ),
  );
}
