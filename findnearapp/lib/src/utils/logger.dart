import 'dart:developer' as developer;

import 'package:flutter/foundation.dart';
import 'package:logger/logger.dart';

var logger = MyLogger();
var prettyLog = Logger(
  filter: null, // Use the default LogFilter (-> only log in debug mode)
  printer: PrettyPrinter(), // Use the PrettyPrinter to format and print log
  output: null,
);

class MyLogger {
  /// Log a message at level verbose.
  void v(dynamic message) {
    prettyLog.v(message);
  }

  /// Log a message at level debug.
  void d(dynamic message) {
    prettyLog.d(message);
  }

  /// Log a message at level info.
  void i(dynamic message) {
    prettyLog.i(message);
  }

  /// Log a message at level warning.
  void w(dynamic message) {
    prettyLog.w(message);
  }

  /// Log a message at level error.
  void e(dynamic message) {
    prettyLog.e(message);
  }

  void log(dynamic message,
      {bool printFullText = false, StackTrace stackTrace}) {
    prettyLog.d(message, null, stackTrace);
  }
}
