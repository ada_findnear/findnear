import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/entities/message_chat/enums/message_text_style_type.dart';
import '../models/filter.dart';
import '../models/my_shop.dart';
import '../models/user.dart';
import '../repository/user_repository.dart' as UserRepository;
import '../services/notification_service.dart';

class LocalDataManager {
  LocalDataManager._();

  static const String MY_SHOP = 'MY_SHOP';
  static const String CURRENT_USER = 'current_user';
  static const String ON_LAUNCH_MESSAGE = 'on_launch_message';
  static const String IS_VISIBLE_ON_THE_MAP = 'is_visible_on_the_map';
  static const String PREVIOUSLY_LOGGED_IN_ID = 'previously_logged_in_id';
  static const String MESSAGE_TEXT_STYLE = 'message_text_style';
  static const String INCOMING_CALL_ID_FCM = 'incoming_call_id_fcm';
  static SharedPreferences _sharedPreferences;

  static Future<LocalDataManager> init() async {
    _sharedPreferences ??= await SharedPreferences.getInstance();
    return LocalDataManager._();
  }

  Future<bool> saveMyShop(MyShop myShop) {
    return _sharedPreferences.setString(MY_SHOP, jsonEncode(myShop.toJson()));
  }

  Future<bool> removeMyShop() {
    return _sharedPreferences.remove(MY_SHOP);
  }

  MyShop getMyShop() {
    final myShopStr = _sharedPreferences.getString(MY_SHOP);
    if (myShopStr == null || myShopStr == '') {
      return null;
    }
    return MyShop.fromJson(jsonDecode(myShopStr));
  }

  Future<bool> saveLoginId(String id) {
    return _sharedPreferences.setString(PREVIOUSLY_LOGGED_IN_ID, id);
  }

  Future<bool> removeLoginId() {
    return _sharedPreferences.remove(PREVIOUSLY_LOGGED_IN_ID);
  }

  String get getLoginId {
    return _sharedPreferences.getString(PREVIOUSLY_LOGGED_IN_ID) ?? '';
  }

  Future<bool> setIsVisibleOnMap(bool isVisible) {
    return _sharedPreferences.setBool(IS_VISIBLE_ON_THE_MAP, isVisible);
  }

  bool get isVisibleOnMap =>
      _sharedPreferences.getBool(IS_VISIBLE_ON_THE_MAP) ?? false;

  Future<bool> saveCurrentUser(Map<String, dynamic> user) {
    return _sharedPreferences.setString(CURRENT_USER, jsonEncode(user));
  }

  Future<bool> setCurrentUser(User user) {
    UserRepository.currentUser.value =
        user; //todo should be removed after refactoring
    return _sharedPreferences.setString(CURRENT_USER, jsonEncode(user.toMap()));
  }

  User get currentUser {
    final json = _sharedPreferences.getString(CURRENT_USER);
    if(json == null || json == ''){
      return null;
    }
    return User.fromJSON(jsonDecode(json));
  }

  bool get isLogin => currentUser.apiToken?.isNotEmpty == true;

  Future<bool> saveOnLaunchMessage(Map<String, dynamic> message) {
    return _sharedPreferences.setString(ON_LAUNCH_MESSAGE, jsonEncode(message));
  }

  Map<String, dynamic> get getOnLaunchMessage {
    final json = _sharedPreferences.getString(ON_LAUNCH_MESSAGE);
    if (json == null) return null;
    final Map<String, dynamic> map = jsonDecode(json);
    if (map is Map) {
      return map;
    }
    return null;
  }

  Future<void> removeOnLaunchMessage() =>
      _sharedPreferences.remove(ON_LAUNCH_MESSAGE);

  Filter get filter => Filter.fromJSON(
      json.decode(_sharedPreferences.getString('filter') ?? '{}'));

  Future<void> logout() async {
    Get.find<NotificationService>().unreadNotificationCounter.value = 0;
    await _sharedPreferences.remove(MY_SHOP);
    await _sharedPreferences.remove(CURRENT_USER);
    await _sharedPreferences.remove(IS_VISIBLE_ON_THE_MAP);
    UserRepository.currentUser.value?.shop = null;
    UserRepository.currentUser.value?.apiToken = null;
  }

  Future<bool> setMessageTextStyleFromType(MessageTextStyleType styleType) {
    return _sharedPreferences.setString(MESSAGE_TEXT_STYLE, describeEnum(styleType));
  }

  Future<bool> setMessageTextStyleFromId(String styleId) {
    return _sharedPreferences.setString(MESSAGE_TEXT_STYLE, styleId);
  }

  MessageTextStyleType getMessageTextStyle() {
    final styleId = _sharedPreferences.getString(MESSAGE_TEXT_STYLE) ?? describeEnum(MessageTextStyleType.unknown);
    return styleId.toMessageTextStyleType();
  }

  Future<bool> setIncomingCallIdFcm(String id) {
    return _sharedPreferences.setString(INCOMING_CALL_ID_FCM, id);
  }

  String getIncomingCallIdFcm() {
    return _sharedPreferences.getString(INCOMING_CALL_ID_FCM);
  }


  String getString(String key) => _sharedPreferences.getString(key);
}
