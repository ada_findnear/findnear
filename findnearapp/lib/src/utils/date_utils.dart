import 'package:intl/intl.dart';

import '../../main.dart';

class DateUtils {
  ///Local
  static const dateTimeFormat = 'dd/MM/yyyy, HH:mm:ss';
  static const iso8601Format = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
  static const dateReservedTimeFormat = 'HH:mm:ss, dd/MM/yyyy';

  /// input - "2021-01-26T07:08:04.142Z"
  /// output - "26/01/2021, 07:08:04"
  static String changeIso8601ToLocalString(String iso8601String) {
    try {
      DateTime dateTime = DateFormat(iso8601Format).parse(iso8601String, true).toLocal();
      return DateFormat(dateTimeFormat).format(dateTime);
    } catch (e) {
      return "";
    }
  }

  /// input - "2021-01-26T07:08:04.142Z"
  /// output - "07:08:04, 26/01/2021"
  static String changeIso8601StringToDateStringFM3(String iso8601String) {
    try {
      DateTime dateTime = DateFormat(iso8601Format).parse(iso8601String, true).toLocal();
      return DateFormat(dateReservedTimeFormat).format(dateTime);
    } catch (e) {
      return "";
    }
  }

  /// input - "2021-01-26T07:08:04.142Z"
  /// output - "07:08:04, 26/01/2021"
  static DateTime changeIso8601StringToDatetime(String iso8601String) {
    try {
      DateTime dateTime = DateFormat(iso8601Format).parse(iso8601String, true).toLocal();
      return dateTime;
    } catch (e) {
      return null;
    }
  }

  static int timeInMilisFromDateTime(String date) {
    try {
      return DateFormat(iso8601Format).parse(date).millisecondsSinceEpoch;
    } catch (e) {
      return -1;
    }
  }

  static String changeIso8601StringToDateTime(DateTime dateTime) {
    try {
      return DateFormat(dateTimeFormat).format(dateTime.toLocal());
    } catch (e) {
      return "";
    }
  }

  static String getVerboseDateTimeRepresentation(DateTime dateTime) {
    DateTime now = DateTime.now();
    DateTime justNow = DateTime.now().subtract(Duration(minutes: 1));
    DateTime localDateTime = dateTime.add(now.timeZoneOffset);
    if (!localDateTime.difference(justNow).isNegative) {
      return S.current.justNow;
    }
    String roughTimeString = DateFormat('jm', 'vi_VN').format(localDateTime);
    if (localDateTime.day == now.day && localDateTime.month == now.month && localDateTime.year == now.year) {
      return roughTimeString;
    }
    DateTime yesterday = now.subtract(Duration(days: 1));
    if (localDateTime.day == yesterday.day &&
        localDateTime.month == yesterday.month &&
        localDateTime.year == yesterday.year) {
      return S.current.yesterday_at + roughTimeString;
    }

    if (now.difference(localDateTime).inDays < 4) {
      String weekday = DateFormat('EEEE', 'vi_VN').format(localDateTime);
      return '$weekday, $roughTimeString';
    }
    return '${DateFormat('yMd', 'vi_VN').format(localDateTime)}, $roughTimeString';
  }

}
