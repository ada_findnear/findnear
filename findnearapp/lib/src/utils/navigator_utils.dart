import 'dart:io';

import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/global/global_data.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/live_token_entity.dart';
import 'package:findnear/src/models/entities/place_entity.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/entities/offer_call_entity.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/place_entity.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/models/twilio_target_user.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/control_panel/control_panel_view.dart';
import 'package:findnear/src/pages/create_post_v2/create_post_screenv2.dart';
import 'package:findnear/src/pages/crop_image/crop_image_view.dart';
import 'package:findnear/src/pages/draw_image/draw_image_view.dart';
import 'package:findnear/src/pages/list_livestream/list_livestream_view.dart';
import 'package:findnear/src/pages/list_user_react/list_user_react_view.dart';
import 'package:findnear/src/pages/livestream/audience/audience_livestream_view.dart';
import 'package:findnear/src/pages/login_register.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/list_user_react/list_user_react_view.dart';
import 'package:findnear/src/pages/control_panel/control_panel_view.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/members/chat_group_members_view.dart';
import 'package:findnear/src/pages/messages/conversation_detail/conversation_detail_view.dart';
import 'package:findnear/src/pages/messages/create_conversation/create_conversation_view.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/chat_group_setting_view.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/params/chat_group_params.dart';
import 'package:findnear/src/pages/messages/edit_conversation/edit_conversation_view.dart';
import 'package:findnear/src/pages/messages/image_file_summary/image_file_summary_view.dart';
import 'package:findnear/src/pages/messages/list_search_conversation/list_search_conversation_view.dart';
import 'package:findnear/src/pages/messages/pin_message/list_message_pinned/list_message_pinned_view.dart';
import 'package:findnear/src/pages/messages/pin_message/pin_message_view.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/chat_group_setting_view.dart';
import 'package:findnear/src/pages/messages/poll/create_poll/create_poll_view.dart';
import 'package:findnear/src/pages/messages/send_name_card/send_name_card_view.dart';
import 'package:findnear/src/pages/livestream/broadcast_livestream_view.dart';
import 'package:findnear/src/pages/newsfeed/details/newsfeed_post_detail_view.dart';
import 'package:findnear/src/pages/pass_code/setting_pin_code/setting_pin_code_view.dart';
import 'package:findnear/src/pages/preview_document.dart';
import 'package:findnear/src/pages/profile_user/profile_user_view.dart';
import 'package:findnear/src/pages/register_shop_page/register_shop_page.dart';
import 'package:findnear/src/pages/search/auto_complete/search_auto_complete.dart';
import 'package:findnear/src/pages/shop_detail/product_by_category/view.dart';
import 'package:findnear/src/pages/shop_detail/view.dart';
import 'package:findnear/src/pages/sign_up/signup.dart';
import 'package:findnear/src/pages/videocall/callcoming/view.dart';
import 'package:findnear/src/pages/videocall/view.dart';
import 'package:findnear/src/pages/voice_call_v2/voice_call_screenv2.dart';
import 'package:findnear/src/pages/voice_call_v2/voicecomming/voice_comming_screen.dart';
import 'package:findnear/src/repository/settings_repository.dart'
    as settingRepo;
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:images_picker/images_picker.dart';

goTo({@required dynamic screen, dynamic argument}) =>
    Get.to(screen, arguments: argument);

goToNamed(String routeName, dynamic argument) =>
    Get.toNamed(routeName, arguments: argument);

goToAndRemove({@required dynamic screen, dynamic argument}) =>
    Get.off(screen, arguments: argument);

goToAndRemoveAll({@required dynamic screen, dynamic argument}) =>
    Get.offAll(screen, arguments: argument);

goBack({dynamic argument}) => Get.back(result: argument);

class NavigatorUtils {
  static void handleNavigationOnResume(Map<String, dynamic> message) {
    var id = message['id'];
    switch (id) {
      case "user_post_moment":
        Get.key.currentState
            .pushNamedAndRemoveUntil('/Pages', (route) => true, arguments: 0);
        break;
      case "orders":
        Get.key.currentState.pushReplacementNamed('/Pages', arguments: 3);
        break;
      case "messages":
        Get.key.currentState.pushReplacementNamed('/Pages', arguments: 4);
        break;
      case "request_add_friend":
        Get.key.currentState.pushReplacementNamed('/FriendsRequest');
        break;
      case "comments":
      case "comment_posts":
      case "posts":
        //Navigate to post

        String postId = message['post_id'];

        if (postId is String && (postId?.isNotEmpty ?? false))
          Get.to(
            () => NewsfeedPostDetailsPage(
              post: Post(id: postId),
              isPostContainsOnlyId: true,
            ),
          );
        break;
    }
  }

  static void handleNavigationOnLaunch(Map<String, dynamic> message) async {
    String id = message['id'];
    switch (id) {
      case "user_post_moment":
        Get.key.currentState.pushReplacementNamed('/Pages', arguments: 0);
        break;
      case "orders":
        String messageId = await settingRepo.getMessageId();
        if (messageId != message['google.message_id']) {
          await settingRepo.saveMessageId(message['google.message_id']);
          Get.key.currentState.pushReplacementNamed('/Pages', arguments: 3);
        }
        break;
      case "messages":
        Get.key.currentState.pushReplacementNamed('/Pages', arguments: 4);
        break;
      case "request_add_friend":
        Get.key.currentState.pushReplacementNamed('/FriendsRequest');
        break;
      case "comments":
      case "comment_posts":
      case "posts":
        //Navigate to post
        String postId = message['post_id'];

        if (postId is String && (postId?.isNotEmpty ?? false))
          Get.to(
            () => NewsfeedPostDetailsPage(
              post: Post(id: postId),
              isPostContainsOnlyId: true,
            ),
          );
        break;
    }
  }

  static void toLoginStep2({bool isReplace = false}) {
    if (isReplace) {
      Get.key.currentState.pushReplacementNamed("/Login");
    } else {
      Get.key.currentState.pushNamed("/Login");
    }
  }

  static void toLogin({bool isReplace = false}) {
    if (isReplace) {
      Get.key.currentState.pushReplacementNamed(LoginRegisterPage.ROUTE_NAME);
    } else {
      Get.key.currentState.pushNamed(LoginRegisterPage.ROUTE_NAME);
    }
  }

  static void navigateToPagesWidget() {
    if (navigator.canPop()) {
      navigator.popUntil(ModalRoute.withName('/Pages'));
    }

    navigator.pushReplacementNamed('/Pages', arguments: 2);
  }

  static void navigateToListUserReact({List<User> likers}) {
    Get.toNamed(ListUserReactPage.ROUTE_NAME,
        arguments: RouteArgument(param: likers ?? []));
    // navigator.pushNamed(ListUserReactPage.ROUTE_NAME);
  }

  static void toRegister() {
    Navigator.of(Get.context).pushNamed(SignUpWidget.ROUTE_NAME);
  }

  static void toShopDetailPage({int shopId}) {
    Navigator.of(Get.context).pushNamed(ShopDetailPage.ROUTE_NAME,
        arguments: RouteArgument(param: shopId));
  }

  static void toRegisterShopPage() {
    Navigator.of(Get.context).pushNamed(RegisterShopPage.ROUTE_NAME);
  }

  static void toSearchProductByCategory(
      int shopId, int categoryId, String title) {
    Navigator.of(Get.context).pushNamed(ProductByCategoryView.ROUTE_NAME,
        arguments: RouteArgument(param: {
          'shopId': shopId,
          'categoryId': categoryId,
          'title': title
        }));
  }

  static void navigateToSettingChatGroupWidget(ChatGroupParams params,
      {bool isReplace = false}) {
    if (isReplace) {
      Get.key.currentState.pushReplacementNamed(ChatGroupSettingPage.ROUTE_NAME,
          arguments: RouteArgument(param: params));
    } else {
      Get.key.currentState.pushNamed(ChatGroupSettingPage.ROUTE_NAME,
          arguments: RouteArgument(param: params));
    }
  }

  static void navigateToSettingPinCodeScreen(String id) {
    navigator.pushReplacementNamed(SettingPinCodePage.ROUTE_NAME,
        arguments: RouteArgument(param: id));
  }

  static navigateToCropImageScreen(String imageUrl) async {
    return await Get.key.currentState.pushNamed(
      CropImagePage.ROUTE_NAME,
      arguments: RouteArgument(param: imageUrl),
    );
  }

  static navigateToSendNameCardScreen(Conversation conversation) {
    Get.key.currentState.pushNamed(
      SendNameCardPage.ROUTE_NAME,
      arguments: RouteArgument(param: conversation),
    );
  }

  static navigateToCreatePollScreen(Conversation conversation) {
    Get.key.currentState.pushNamed(
      CreatePollPage.ROUTE_NAME,
      arguments: RouteArgument(param: {
        "conversation": conversation,
      }),
    );
  }

  static navigateToDrawImageScreen() async {
    return await Get.key.currentState.pushNamed(DrawImagePage.ROUTE_NAME);
  }

  static void navigateToPinMessage({bool isReplace = false}) {
    if (isReplace) {
      Get.key.currentState.pushReplacementNamed(PinMessagePage.ROUTE_NAME);
    } else {
      Get.key.currentState.pushNamed(PinMessagePage.ROUTE_NAME);
    }
  }

  static void navigateToControlPanel({bool isReplace = false}) {
    if (isReplace) {
      Get.key.currentState.pushReplacementNamed(ControlPanelPage.ROUTE_NAME);
    } else {
      Get.key.currentState.pushNamed(ControlPanelPage.ROUTE_NAME);
    }
  }

  static void navigateToVoiceCallComingWidget(
      BuildContext mContext, OfferCallEntity targetUser) {
    try {
      var overlayEntry;
      overlayEntry = OverlayEntry(builder: (context) {
        final size = MediaQuery.of(context).size;
        print(size.width);
        return Positioned(
          height: size.height,
          width: size.width,
          top: 0,
          left: 0,
          child: Material(
            child: VoiceCallComingPage(
              routeArgument: RouteArgument(param: {
                'targetUser': targetUser,
              }),
              endCallCallback: () {
                overlayEntry?.remove();
                overlayEntry = null;
              },
              acceptCallCallback: () {
                overlayEntry?.remove();
                overlayEntry = null;
              },
            ),
          ),
        );
      });
      Overlay.of(mContext).insert(overlayEntry);
    } catch (e) {
      print("TuanLA - $e");
    }
  }

  static void navigateToVoiceCallWidget(
    BuildContext mContext,
    ShortUserEntity targetUser,
    bool isMakeCall,
    bool isAnswer, {
    Conversation conversation,
    bool fromCallkit = false,
    OfferCallEntity offerCallEntity,
  }) {
    try {
      var overlayEntry;
      overlayEntry = OverlayEntry(builder: (context) {
        final size = MediaQuery.of(context).size;
        print(size.width);
        return Positioned(
          height: size.height,
          width: size.width,
          top: 0,
          left: 0,
          child: Material(
            child: VoiceCallPage(
              routeArgument: RouteArgument(param: {
                'targetUser': targetUser,
                "isMakeCall": isMakeCall,
                "isAnswer": isAnswer,
                "conversation": conversation,
                "callkit": fromCallkit,
                "offerCallEntity": offerCallEntity,
              }),
              endCallCallback: () {
                overlayEntry?.remove();
                overlayEntry = null;
              },
            ),
          ),
        );
      });
      Overlay.of(mContext).insert(overlayEntry);
    } catch (e) {
      print("TuanLA -$e");
    }
  }

  static void navigateToVideoCallWidget(BuildContext mContext,
      ShortUserEntity targetUser, bool isMakeCall, bool isAnswer,
      {bool busy = false,
      Conversation conversation,
      bool fromCallkit,
      bool isUserOnline = false,
      OfferCallEntity offerCallEntity}) {
    try {
      var overlayEntry;
      overlayEntry = OverlayEntry(builder: (context) {
        final size = MediaQuery.of(context).size;
        print(size.width);
        return Positioned(
          height: size.height,
          width: size.width,
          top: 0,
          left: 0,
          child: Material(
            child: VideoCallPage(
              routeArgument: RouteArgument(param: {
                'targetUser': targetUser,
                'isMakeCall': isMakeCall,
                "isAnswer": isAnswer,
                "conversation": conversation,
                "callkit": fromCallkit,
                "offerCallEntity": offerCallEntity,
              }),
              endCallCallback: () {
                overlayEntry?.remove();
                overlayEntry = null;
              },
            ),
          ),
        );
      });
      Overlay.of(mContext).insert(overlayEntry);
    } catch (e) {
      print("TuanLA - $e");
    }
  }

  static void navigateToVideoCallComingWidget(
      BuildContext mContext, OfferCallEntity targetUser) {
    try {
      var overlayEntry;
      overlayEntry = OverlayEntry(builder: (context) {
        final size = MediaQuery.of(context).size;
        print(size.width);
        return Positioned(
          height: size.height,
          width: size.width,
          top: 0,
          left: 0,
          child: Material(
            child: VideoCallComingPage(
              routeArgument: RouteArgument(param: {
                'targetUser': targetUser,
              }),
              endCallCallback: () {
                overlayEntry?.remove();
                overlayEntry = null;
              },
              acceptCallCallback: () {
                overlayEntry?.remove();
                overlayEntry = null;
              },
            ),
          ),
        );
      });
      Overlay.of(mContext).insert(overlayEntry);
    } catch (e) {
      print("TuanLA - $e");
    }
  }

  static void navigateToPinnedMessage() {
    navigator.pushNamed(ListMessagePinnedPage.ROUTE_NAME);
  }

  static void navigateToListSearchConversation() {
    Get.key.currentState.pushNamed(ListSearchConversationPage.ROUTE_NAME);
  }

  static void toPlaceCompletePage({Function callback}) {
    Navigator.of(Get.context).pushNamed(AutoCompletePlacePage.ROUTE_NAME,
        arguments: RouteArgument(param: callback));
  }

  static void navigateToCreateConversationWidget(
      {List<ShortUserEntity> members,
      Function callback,
      bool isReplace = false}) {
    if (isReplace) {
      Get.key.currentState.pushReplacementNamed(
        CreateConversationPage.ROUTE_NAME,
        arguments: RouteArgument(
          param: {
            'conversation_edited': callback,
            'members': members,
          },
        ),
      );
    } else {
      Get.key.currentState.pushNamed(
        CreateConversationPage.ROUTE_NAME,
        arguments: RouteArgument(
          param: {
            'conversation_edited': callback,
            'members': members,
          },
        ),
      );
    }
  }

  static void navigateToImageFileSummaryPage({Conversation conversation}) {
    Navigator.of(Get.context).pushNamed(ImageFileSummaryPage.ROUTE_NAME,
        arguments: RouteArgument(param: conversation));
  }

  static void navigateToBroadcastLivestreamWidget(
      LiveTokenEntity token, LiveTokenEntity rtmToken, int fromTab,
      {bool isReplace = false}) {
    if (isReplace) {
      Get.key.currentState
          .pushReplacementNamed(BroadcastLivestreamPage.ROUTE_NAME,
              arguments: RouteArgument(param: {
                'token': token,
                'rtm_token': rtmToken,
                'from_tab': fromTab,
              }));
    } else {
      Get.key.currentState.pushNamed(BroadcastLivestreamPage.ROUTE_NAME,
          arguments: RouteArgument(param: {
            'token': token,
            'rtm_token': rtmToken,
            'from_tab': fromTab,
          }));
    }
  }

  static void navigateToAudienceLivestreamWidget(LiveTokenEntity entity,
      int broadcasterID, LiveTokenEntity rtmToken, int fromTab,
      {bool isReplace = false}) {
    if (isReplace) {
      Get.key.currentState
          .pushReplacementNamed(AudienceLivestreamPage.ROUTE_NAME,
              arguments: RouteArgument(param: {
                'token': entity,
                'broadcaster_id': broadcasterID,
                'rtm_token': rtmToken,
                'from_tab': fromTab,
              }));
    } else {
      Get.key.currentState.pushNamed(AudienceLivestreamPage.ROUTE_NAME,
          arguments: RouteArgument(param: {
            'token': entity,
            'broadcaster_id': broadcasterID,
            'rtm_token': rtmToken,
            'from_tab': fromTab,
          }));
    }
  }

  static void navigateToConversationDetailPage(Conversation conversation,
      {bool isReplace = false}) {
    if (isReplace) {
      Get.key.currentState.pushReplacementNamed(
          ConversationDetailPage.ROUTE_NAME,
          arguments: RouteArgument(param: conversation));
    } else {
      Get.key.currentState.pushNamed(ConversationDetailPage.ROUTE_NAME,
          arguments: RouteArgument(param: conversation));
    }
  }

  static void navigateToChatGroupMembersPage(
      Conversation conversation, List<ShortUserEntity> shortUserEntities,
      {bool isReplace = false}) {
    if (isReplace) {
      Get.key.currentState.pushReplacementNamed(ChatGroupMembersPage.ROUTE_NAME,
          arguments: RouteArgument(param: {
            "members": shortUserEntities,
            "conversation": conversation
          }));
    } else {
      Get.key.currentState.pushNamed(ChatGroupMembersPage.ROUTE_NAME,
          arguments: RouteArgument(param: {
            "members": shortUserEntities,
            "conversation": conversation
          }));
    }
  }

  static void navigateToEditConversationPage(
      {Conversation conversation,
      List<ShortUserEntity> members,
      Function callback,
      bool isReplace = false}) {
    if (isReplace) {
      Get.key.currentState.pushReplacementNamed(
        EditConversationPage.ROUTE_NAME,
        arguments: RouteArgument(
          param: {
            'conversation_edited': callback,
            'members': members,
            'conversation': conversation,
          },
        ),
      );
    } else {
      Get.key.currentState.pushNamed(
        EditConversationPage.ROUTE_NAME,
        arguments: RouteArgument(
          param: {
            'conversation_edited': callback,
            'members': members,
            'conversation': conversation,
          },
        ),
      );
    }
  }

  static void navigateToListLivestreamPage({bool isReplace = false}) {
    if (isReplace) {
      Get.key.currentState.pushReplacementNamed(ListLivestreamPage.ROUTE_NAME);
    } else {
      Get.key.currentState.pushNamed(ListLivestreamPage.ROUTE_NAME);
    }
  }

  static void navigateToProfilePage(String userId, {bool isReplace = false}) {
    if (isReplace) {
      Get.key.currentState.pushReplacementNamed(ProfileUserPage.ROUTE_NAME,
          arguments: RouteArgument(param: {
            "userId": userId,
          }));
    } else {
      Get.key.currentState.pushNamed(ProfileUserPage.ROUTE_NAME,
          arguments: RouteArgument(param: {
            "userId": userId,
          }));
    }
  }

  static void navigateToPreviewDocumentPage(String fileUrl) {
    goToNamed(PreviewDocumentPage.ROUTE_NAME, RouteArgument(param: fileUrl));
  }

  static void navigateToCreatePostV2({
    bool isReplace = false,
    User user,
    List<Media> medias,
    Post originalPost,
  }) {
    if (isReplace) {
      Get.key.currentState.pushReplacementNamed(CreatePostPageV2.ROUTE_NAME,
          arguments: RouteArgument(param: {
            "user": user,
            "medias":medias,
            "originalPost":originalPost,
          }));
    } else {
      Get.key.currentState.pushNamed(CreatePostPageV2.ROUTE_NAME,
          arguments: RouteArgument(param: {
            "user": user,
            "medias":medias,
            "originalPost":originalPost,
          }));
    }
  }
}
