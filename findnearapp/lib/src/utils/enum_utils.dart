import 'package:findnear/src/commons/app_images.dart';
import 'package:flutter/material.dart';

/// ------------------------------------
enum InviteUserToGroupStatus {
  pending,

  /// Join group
  invited,

  ///
  approved,

  /// Approval member to group
  rejected,

  /// Reject member out group
  kicked,

  /// Kich member
}

extension InviteUserToGroupStatusExtension on InviteUserToGroupStatus {
  int get toParam {
    switch (this) {
      case InviteUserToGroupStatus.pending:
        return 0;
      case InviteUserToGroupStatus.invited:
        return 1;
      case InviteUserToGroupStatus.approved:
        return 2;
      case InviteUserToGroupStatus.rejected:
        return 3;
      case InviteUserToGroupStatus.kicked:
        return 4;
      default:
        return -1;
    }
  }
}

/// ------------------------------------

enum GroupViewerStatus {
  private,
  onlyFriend,
  public,
}

extension StringExtension on String {
  GroupViewerStatus get toGroupViewerStatus {
    print("this ${this}");
    switch (this) {
      case "0":
        return GroupViewerStatus.private;
      case "1":
        return GroupViewerStatus.onlyFriend;
      case "2":
        return GroupViewerStatus.public;
      default:
        return null;
    }
  }
}

extension GroupViewerStatusExtension on GroupViewerStatus {
  String get toParam {
    switch (this) {
      case GroupViewerStatus.private:
        return "0";
      case GroupViewerStatus.onlyFriend:
        return "1";
      case GroupViewerStatus.public:
        return "2";
      default:
        return null;
    }
  }

  String get display {
    switch (this) {
      case GroupViewerStatus.private:
        return "Nhóm kín";
      case GroupViewerStatus.onlyFriend:
        return "Chỉ bạn bè";
      case GroupViewerStatus.public:
        return "Nhóm công khai";
      default:
        return "";
    }
  }

  IconData get icon {
    switch (this) {
      case GroupViewerStatus.private:
        return Icons.lock_outline;
      case GroupViewerStatus.onlyFriend:
        return Icons.people;
      case GroupViewerStatus.public:
        return Icons.public_sharp;
      default:
        return Icons.lock_outline;
    }
  }

  String get desc {
    switch (this) {
      case GroupViewerStatus.private:
        return "Chỉ thành viên mới nhìn thấy mọi người trong nhóm và những gì họ đăng";
      case GroupViewerStatus.onlyFriend:
        return "Chỉ bạn bè mới nhìn thấy mọi người trong nhóm và những gì họ đăng";
      case GroupViewerStatus.public:
        return "Bất kỳ ai cũng có thể nhìn thấy mọi người trong nhóm và những bài viết họ đăng";
      default:
        return "";
    }
  }
}

/// ----------------------------------------------------------------------------

enum GroupDetailSettingFeature {
  /// Quản trị thành viên
  member_administration,

  /// Quản trị nội dung
  content_management,

  /// Quyền & người kiểm duyệt
  permission_censorship,

  /// Quyền riêng tư
  privacy,

  /// Rời khỏi nhóm
  out_group,
}

extension GroupDetailSettingFeatureExtension on GroupDetailSettingFeature {
  String get title {
    switch (this) {
      case GroupDetailSettingFeature.member_administration:
        return "Quản trị thành viên";
      case GroupDetailSettingFeature.content_management:
        return "Quản trị nội dung";
      case GroupDetailSettingFeature.permission_censorship:
        return "Quyền & người kiểm duyệt";
      case GroupDetailSettingFeature.out_group:
        return "Rời khỏi nhóm";
      case GroupDetailSettingFeature.privacy:
        return "Quyền riêng tư";
      default:
        return "";
    }
  }

  String get icon {
    switch (this) {
      case GroupDetailSettingFeature.member_administration:
        return AppImages.ic_member_administration;
      case GroupDetailSettingFeature.content_management:
        return AppImages.ic_content_management;
      case GroupDetailSettingFeature.permission_censorship:
        return AppImages.ic_permission_censorship;
      case GroupDetailSettingFeature.out_group:
        return AppImages.ic_out_group;
      case GroupDetailSettingFeature.privacy:
        return AppImages.ic_privacy;
      default:
        return "";
    }
  }
}

/// ----------------------------------------------------------------------------

enum PostGroupStatus {
  pending,
  approved,
  reject,
  banned,
}

extension PostGroupStatusExtension on PostGroupStatus {
  int get toParam {
    switch (this) {
      case PostGroupStatus.pending:
        return 1;
      case PostGroupStatus.approved:
        return 2;
      case PostGroupStatus.reject:
        return 3;
      case PostGroupStatus.banned:
        return 4;
      default:
        return -1;
    }
  }
}
