import 'dart:convert';

import 'package:dio/dio.dart' as dio;
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';

import '../commons/app_snackbar.dart';
import '../models/entities/state_usercall.dart';
import '../repository/user_repository.dart';
import '../services/notification_service.dart';
import 'local_data_manager.dart';
import 'logger.dart';
import 'navigator_utils.dart';

/// Create a [AndroidNotificationChannel] for heads up notifications
AndroidNotificationChannel channel;

FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

class FirebaseMessageManager {
  FirebaseMessaging firebaseMessaging;

  static Future<FirebaseMessageManager> initialize() async {
    final f = FirebaseMessageManager();
    await f.initFirebase();
    return f;
  }

  Future<void> initFirebase() async {
    //Notification
    firebaseMessaging = FirebaseMessaging.instance;
    await firebaseMessaging?.setAutoInitEnabled(true);
    firebaseMessaging?.getToken()?.then((token) {
      prettyLog.d(
          "=== FcmToken:\n$token\n===END OF FCM TOKEN"); // Print the Token in Console
      Get.find<NotificationService>().fetchUnreadMessage();
    });
    FirebaseMessaging.onMessage.listen(notificationOnMessage);
    FirebaseMessaging.onBackgroundMessage(notificationOnBackgroundMessage);
    FirebaseMessaging.onMessageOpenedApp.listen(notificationOnOpenApp);
    if (!kIsWeb) {
      channel = const AndroidNotificationChannel(
        'vn.tada.findnear', // id
        'FindNear default channel', // title
        //  'FindNear default channel', // description
        importance: Importance.high,
      );

      flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

      /// Create an Android Notification Channel.
      ///
      /// We use this channel in the `AndroidManifest.xml` file to override the
      /// default FCM channel to enable heads up notifications.
      await flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<
              AndroidFlutterLocalNotificationsPlugin>()
          ?.createNotificationChannel(channel);
    }
  }

  Future<dynamic> notificationOnOpenApp(RemoteMessage message) async {
    prettyLog.d("notificationOnResume: \n\t\t${message.data.toString()}");
    NavigatorUtils.handleNavigationOnResume(message.data);
  }

  Future<void> notificationOnMessage(RemoteMessage message) async {
    prettyLog.d("notificationOnMessage: ${message.data}");

    // Force logout
    if (message.data["isForceLogout"] == "true") {
      logout();
      return;
    }

    String title = message?.notification?.title ?? "";
    if (title.isEmpty) {
      title = "Thông báo";
    }
    final body = message?.notification?.body;
    if (body != null && body.isNotEmpty) {
      AppSnackbar.showNotification(
        title: title,
        message: body,
      );
      Get.find<NotificationService>().fetchUnreadMessage();
    }
  }

  static Future<dynamic> notificationOnBackgroundMessage(
      RemoteMessage message) async {
    await Firebase.initializeApp();
    prettyLog.d('notificationOnBackgroundMessage: ${message.data}');

    // Force logout
    if (message.data["isForceLogout"] == "true") {
    }

    try {
      if (message.data["action"] == "VIDEO_CALL" &&
              message.data["type"] == "income") {
        // tạo param truyền vào call kit
        final userInfoMakeCall = jsonDecode(message.data["userInfoMakeCall"]);
        final id = message.data["messageId"];
        var params = <String, dynamic>{
          'id': id,
          'nameCaller': userInfoMakeCall["name"],
          'number': userInfoMakeCall["userId"],
          'handle': userInfoMakeCall["userId"],
          'type': message.data["typeCall"] == "VIDEO_CALL" ? 1 : 0,
          'duration': 60000,
          'android': <String, dynamic>{
            'isCustomNotification': false,
            'isShowLogo': false,
            'ringtonePath': 'ringtone_default',
            'backgroundColor': '#0955fa',
            'actionColor': '#4CAF50'
          },
        };

        // kiểm tra điều kiện trước khi hiển thị màn call coming
        // 2. user online không
        var checkOnline = false;
        var status = await getUserStatus(userInfoMakeCall["userId"]);
        if (status.status == true) {
          // user online
          checkOnline = true;
        }
        if (checkOnline == true) {
          (await LocalDataManager.init()).setIncomingCallIdFcm(id);
          await FlutterCallkitIncoming.showCallkitIncoming(params);
        }
      }
    } catch (e) {
      print(
          "FirebaseMessageManager - notificationOnBackgroundMessage - error: $e");
    }

    if (message.data.containsKey('data')) {
      // Handle data message
      final dynamic data = message.data['data'];
    }

    if (message.data.containsKey('notification')) {
      // Handle notification message
      final dynamic notification = message.data['notification'];
    }
    Get.find<NotificationService>().fetchUnreadMessage();
  }

  void handleOnLaunchMessage() async {
    RemoteMessage initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();
    if (initialMessage != null) {
      NavigatorUtils.handleNavigationOnLaunch(initialMessage.data);
    }
    /*try {
      final localDataManager = Get.find<LocalDataManager>();
      final message = localDataManager.getOnLaunchMessage;
      prettyLog.d("handleOnLaunchMessage: $message");
      if (message != null) {
        localDataManager.removeOnLaunchMessage();
        NavigatorUtils.handleNavigationOnLaunch(message);
      }
    } catch (e) {
      logger.e(e);
    }*/
  }

  void signOut() async {
    await firebaseMessaging?.setAutoInitEnabled(false);
    //  await firebaseMessaging.deleteInstanceID();
  }

  static Future<StateUserCall> getUserStatus(String code) async {
    final localDataManager = await LocalDataManager.init();

    dio.Response response;
    var dioClient = dio.Dio(); // with default Options
    dioClient.options.baseUrl = 'https://api.findnear.vn';
    dioClient.options.connectTimeout = 30000; //30s
    dioClient.options.receiveTimeout = 60000; //60s

    final apiKey = localDataManager.currentUser?.apiToken ?? "";
    print("TuanLA - apiKey: $apiKey");
    dioClient.options.headers = {'Authorization': 'Bearer ${apiKey}'};

    try {
      response = await dioClient.get('/api/user/status/${code}');
      final data = response.data as Map<String, dynamic>;
      final obj = StateUserCall.fromJson(data);
      return obj;
    } catch (e, s) {
      logger.e(e);
      return StateUserCall();
    }
  }
}
