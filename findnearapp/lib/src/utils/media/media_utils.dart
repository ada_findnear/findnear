import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:findnear/src/models/media.dart' as m;
import 'package:findnear/src/utils/logger.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:images_picker/images_picker.dart';
import 'package:video_compress/video_compress.dart';

import '../../extensions/media_ext.dart';
import 'video_compressor.dart';

class MediaUtils {
  static Future<AppMediaInfo> getMediaInfo(String path) async {
    var result = await VideoCompress.getMediaInfo(path);
    return AppMediaInfo.from(result);
  }

  static Future<String> getVideoThumbnail(String path) async {
    if (path.isVideoPath) {
      var result = await VideoCompress.getFileThumbnail(path);
      return result.path;
    } else
      return null;
  }

  static Future<List<String>> optimizeMedias(List<Media> medias) async {
    var tasks = <Future<AppMediaInfo>>[];
    var paths = <String>[];
    for (var i = 0; i < medias.length; i++) {
      Media element = medias[i];
      if (element.isVideo) {
        tasks.add(VideoCompressor.compress(element.path));
      } else
        paths.add(element.path);
    }
    if (tasks.isNotEmpty) {
      var compressedFiles = await Future.wait(tasks);
      compressedFiles.forEach((element) {
        if (element != null && element.path != null) paths.add(element.path);
      });
    }
    return paths;
  }

  static Future<List<Media>> optimizeMediasV2(List<Media> medias) async {
    var tasks = <Future<AppMediaInfo>>[];
    var result = <Media>[];
    var listWillBeProcessed = <Media>[];
    for (var i = 0; i < medias.length; i++) {
      Media element = medias[i];
      if (element.isVideo) {
        tasks.add(VideoCompressor.compress(element.path));
        listWillBeProcessed.add(element);
      } else
        result.add(element);
    }
    if (tasks.isNotEmpty) {
      var compressedFiles = await Future.wait(tasks);
      for (var i = 0; i < compressedFiles.length; i++) {
        var element = compressedFiles[i];
        if (element != null && element.path != null) {
          var media = listWillBeProcessed[i];
          media.path = element.path;
          result.add(media);
        }
      }
    }
    return result;
  }

  static Future<bool> saveMedia(m.Media media) async {
    final dio = Dio();
    try {
      if (media.isPhoto) {
        final response = await dio.get(media.url,
            options: Options(responseType: ResponseType.bytes));
        final result = await ImageGallerySaver.saveImage(
          Uint8List.fromList(response.data),
          name: media.name,
        );
        if (result is Map) {
          dio.close();
          return result['isSuccess'];
        } else {
          prettyLog.e("saveImage - Unknown error");
          dio.close();
          return false;
        }
      } else {
        final directory = await getTemporaryDirectory();
        String savePath = directory.path + "/${media.fileName}";
        await dio.download(media.url, savePath);
        final result = await ImageGallerySaver.saveFile(savePath);
        if (result is Map) {
          dio.close();
          return result['isSuccess'];
        } else {
          prettyLog.e("saveImage - Unknown error");
          dio.close();
          return false;
        }
      }
    } catch (e) {
      prettyLog.e(e);
      dio.close();
      return false;
    }
  }
}
