import 'package:findnear/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:image_picker/image_picker.dart';
import 'package:images_picker/images_picker.dart';

import '../../extensions/media_ext.dart';
import 'media_utils.dart';

class MediaPickerUtils {
  static const MAX_IMAGES_PER_POST = 9;
  static const MAX_IMAGES_PER_STORY = 1;
  static const MAX_IMAGES_PER_COMMENT = 1;
  static const MAX_IMAGES_PER_PRODUCT = 5;
  static const MAX_VIDEO_PER_POST = 1;
  static const MAX_MEDIA_SIZE_KB = 1024 * 10;
  static const MAX_VIDEO_SIZE_MB = 160;
  static const MAX_VIDEO_DURATION_IN_SECOND_PER_FEED = 180 * 1000;

  /**
   * Use this function to pick photo only
   */
  static Future<List<Media>> pickPhotoForFeed(int selectedCount) async {
    var images = await ImagesPicker.pick(
      count: MAX_IMAGES_PER_POST - selectedCount,
      pickType: PickType.image,
      language: Language.System,
      quality: 0.8,
      maxSize: 1500,
    );
    try {
      _requiredValidSize(images);
      return images;
    } catch (e) {
      _showError(e.toString());
      return <Media>[];
    }
  }

  static Future<List<Media>> pickPhotoAndVideoForFeed(int selectedCount) async {
    var images = await ImagesPicker.pick(
      count: MAX_IMAGES_PER_POST - selectedCount,
      pickType: PickType.all,
      language: Language.System,
      quality: 0.8,
      maxSize: 1500,
    );
    try {
      if (images != null && images.isNotEmpty){
        for (var i = 0; i < images.length; i++) {
          var element = images[i];
          if (element.isVideo) {
            var meta = await MediaUtils.getMediaInfo(element.path);
            if (meta.duration > MAX_VIDEO_DURATION_IN_SECOND_PER_FEED)
              throw S.of(Get.context).over_video_duration_message;
          }else{
            if (element.size > MediaPickerUtils.MAX_MEDIA_SIZE_KB) {
              throw S.of(Get.context).image_should_be_less_than_10MB;
            }
          }
        }
        return images;
      }
    } catch (e) {
      _showError(e.toString());
      return <Media>[];
    }
  }

  static void _requiredValidSize(List<Media> medias) {
    if (medias == null || medias.isEmpty) return;
    medias.forEach((element) {
      if (element.size > MediaPickerUtils.MAX_MEDIA_SIZE_KB) {
        throw S.of(Get.context).image_should_be_less_than_10MB;
      }
    });
  }

  static void _requiredValidDuration(List<Media> medias) async {
    if (medias == null || medias.isEmpty) return;
    for (var i = 0; i < medias.length; i++) {
      var element = medias[i];
      if (element.isVideo) {
        var meta = await MediaUtils.getMediaInfo(element.path);
        if (meta.duration > MAX_VIDEO_DURATION_IN_SECOND_PER_FEED)
          throw S.of(Get.context).over_video_duration_message;
      }
    }
  }

  static void _showError(String msg) {
    ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
      content: Text(msg),
    ));
  }

  /**
   * Use this function to pick video only
   */
  static Future<List<Media>> pickVideoForFeed(int selectedCount) async {
    var videos = await ImagesPicker.pick(
      count: MAX_VIDEO_PER_POST - selectedCount,
      pickType: PickType.video,
      language: Language.System,
      quality: 0.8,
      maxSize: 1500,
    );
    try {
      await _requiredValidDuration(videos);
      return videos;
    } catch (e) {
      _showError(e.toString());
      return <Media>[];
    }
  }

  static Future<List<Media>> pickVideoForFeedV2() async {
    final ImagePicker _picker = ImagePicker();
    final XFile videoFile =
        await _picker.pickVideo(source: ImageSource.gallery);

    List<Media> videos = [];
    var meta = await MediaUtils.getMediaInfo(videoFile.path);
    var thumbnail = await MediaUtils.getVideoThumbnail(videoFile.path);
    int sizeInBytes = meta.file.lengthSync();
    double sizeInMb = sizeInBytes / (1024 * 1024);
    videos
        .add(Media(path: videoFile.path, size: sizeInMb, thumbPath: thumbnail));
    try {
      await _requiredValidDuration(videos);
      return videos;
    } catch (e) {
      _showError(e.toString());
      return <Media>[];
    }
  }

  /**
   * Use this function to pick photo only - just use for story
   */
  static Future<List<Media>> pickPhotoForStory(int selectedCount) async {
    var images = await ImagesPicker.pick(
      count: MAX_IMAGES_PER_STORY - selectedCount,
      pickType: PickType.image,
      language: Language.System,
      quality: 0.8,
      maxSize: 1500,
    );
    try {
      _requiredValidSize(images);
      return images;
    } catch (e) {
      _showError(e.toString());
      return <Media>[];
    }
  }

  /**
   * Use this function to pick photo only - just use for create / update production
   */
  static Future<List<Media>> pickPhotoForProduct(int selectedCount) async {
    var images = await ImagesPicker.pick(
      count: MAX_IMAGES_PER_PRODUCT - selectedCount,
      pickType: PickType.image,
      language: Language.System,
      quality: 0.8,
      maxSize: 1500,
    );
    try {
      _requiredValidSize(images);
      return images;
    } catch (e) {
      _showError(e.toString());
      return <Media>[];
    }
  }
}
