import 'dart:convert';
import 'dart:io';

import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/route_argument.dart';
import 'package:findnear/src/pages/videocall/view.dart';
import 'package:findnear/src/pages/voice_call_v2/voice_call_screenv2.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:flutter/material.dart';

import '../global/global_data.dart';
import '../global/global_event.dart';
import '../models/entities/offer_call_entity.dart';
import '../models/enums/websocket_event.dart';
import '../models/response/call_notification_response.dart';
import '../pages/videocall/callsignaling.dart';
import '../services/websocket_service.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';
import 'package:get/get.dart';

class ListenerEventCallKit {
  AppLifecycleState state;

  void updateState(AppLifecycleState state) {
    this.state = state;
  }

  Future<void> listenerEventCallKit() async {
    try {
      print("NamNH - listenerEventCallKit: ${state}");
      FlutterCallkitIncoming.onEvent.listen((event) async {
        print("NamNH - event: ${event.name}");
        // if (!mounted) return;
        switch (event.name) {
          case CallEvent.ACTION_CALL_INCOMING:
            var data = CallNotiResponse.fromJson(event.body);
            // update lại trạng thái user đang trong cuộc gọi
            int type = 0;
            if (data.messageType == "call_video") {
              type = 1;
            }
            updateUserStatus(type, currentUser.value.code, true);
            break;
          case CallEvent.ACTION_CALL_START:
            break;
          case CallEvent.ACTION_CALL_ACCEPT:
            if (Platform.isIOS) {
              if (GlobalData.instance.offer != null) {
                // onbackground
                final offer = GlobalData.instance.offer.copyWith();
                GlobalEvent.instance.onReceiveOfferCallIOS.add(offer);
              } else {
                // kill app
                GlobalData.instance.isAcceptCallKitIos = true;
                try {
                  final eventJson = event.body;
                  final data = {
                    "action": "VIDEO_CALL",
                    "type": "ready",
                    "userMakeCall": eventJson["handle"],
                    "typeCall":
                        eventJson["type"] == 1 ? "VIDEO_CALL" : "VOICE_CALL",
                  };
                  Get.find<WebsocketService>().subscribe(
                    "message",
                    data,
                  );
                } catch (e) {
                  print("TuanLA - $e");
                }
              }
            } else {
              if (state == AppLifecycleState.paused ||
                  state == AppLifecycleState.resumed) {
                // crete object offer call;
                OfferEntity _offerEntity;
                if (event.body["extra"] is String) {
                  _offerEntity =
                      OfferEntity.fromJson(jsonDecode(event.body["extra"]));
                } else if (event.body["extra"] is Map) {
                  _offerEntity = OfferEntity.fromJson(
                      Map<String, dynamic>.from(event.body["extra"]));
                }

                if (event.body["type"] == 1) {
                  OfferCallEntity offerCallEntity = OfferCallEntity();
                  offerCallEntity.offerEntity = _offerEntity;
                  offerCallEntity.type = "offer";
                  offerCallEntity.action = "VIDEO_CALL";
                  offerCallEntity.userMakeCall = event.body["number"];
                  offerCallEntity.typeCall = "video";
                  GlobalEvent.instance.onPushCallEvent.add(offerCallEntity);
                } else {
                  OfferCallEntity offerCallEntity = OfferCallEntity();
                  offerCallEntity.offerEntity = _offerEntity;
                  offerCallEntity.type = "offer";
                  offerCallEntity.action = "VIDEO_CALL";
                  offerCallEntity.userMakeCall = event.body["number"];
                  offerCallEntity.typeCall = "audio";
                  GlobalEvent.instance.onPushCallEvent.add(offerCallEntity);
                }
              }
            }
            break;
          case CallEvent.ACTION_CALL_DECLINE:
            print("TuanLA - ACTION_CALL_DECLINE ");
            var data = CallNotiResponse.fromJson(event.body);
            try {
              if (Platform.isIOS) {
                GlobalData.instance.offer = null;
                userLeaveCall(event.body["type"], data.number);
                updateUserStatus(
                    event.body["type"], currentUser.value.code, false);
              } else {
                if (state == AppLifecycleState.paused) {
                  userLeaveCall(event.body["type"], data.number);
                  updateUserStatus(
                      event.body["type"], currentUser.value.code, false);
                }
              }
            } catch (e) {
              print("TuanLA - $e");
            }
            break;
          case CallEvent.ACTION_CALL_ENDED:
            break;
          case CallEvent.ACTION_CALL_TIMEOUT:
            break;
          case CallEvent.ACTION_CALL_CALLBACK:
            // TODO: only Android - click action `Call back` from missed call notification
            break;
          case CallEvent.ACTION_CALL_TOGGLE_HOLD:
            // TODO: only iOS
            break;
          case CallEvent.ACTION_CALL_TOGGLE_MUTE:
            print("TuanLA - ACTION_CALL_TOGGLE_MUTE ");
            break;
          case CallEvent.ACTION_CALL_TOGGLE_DMTF:
            print("TuanLA - ACTION_CALL_TOGGLE_DMTF ");
            break;
          case CallEvent.ACTION_CALL_TOGGLE_GROUP:
            print("TuanLA - ACTION_CALL_TOGGLE_GROUP ");
            break;
          case CallEvent.ACTION_CALL_TOGGLE_AUDIO_SESSION:
            print("TuanLA - ACTION_CALL_TOGGLE_AUDIO_SESSION ");
            break;
        }
      });
    } on Exception {}
  }

  void updateUserStatus(int type, String code, bool status) async {
    var update = new Map<String, dynamic>();
    if (type == 1) {
      update["action"] = describeEnum(CallType.VIDEO_CALL);
    } else {
      update["action"] = describeEnum(CallType.VOICE_CALL);
    }
    update["type"] = 'updateStatus';
    update["userName"] = code;
    update["isCalling"] = status;
    await Get.find<WebsocketService>()
        .subscribe(WebsocketEvent.MESSAGE.value, update);
  }

  void userLeaveCall(int type, String code) async {
    var map = new Map<String, dynamic>();
    if (type == 1) {
      map["action"] = describeEnum(CallType.VIDEO_CALL);
    } else {
      map["action"] = describeEnum(CallType.VOICE_CALL);
    }
    map["type"] = 'leave';
    map["userName"] = code;
    await Get.find<WebsocketService>()
        .subscribe(WebsocketEvent.MESSAGE.value, map);
  }

  void showCallVoiceOverlay(BuildContext context, OfferCallEntity data) {
    final userCode = data.userMakeCall;
    final targetUser = ShortUserEntity(code: userCode, id: "", avatar: "");
    var overlayEntry;
    overlayEntry = OverlayEntry(builder: (context) {
      final size = MediaQuery.of(context).size;
      print(size.width);
      return Positioned(
        height: size.height,
        width: size.width,
        top: 0,
        left: 0,
        child: Material(
          child: VoiceCallPage(
            routeArgument: RouteArgument(param: {
              'targetUser': targetUser,
              'isMakeCall': false,
              "isAnswer": true,
              "callkit": true,
              "offerCallEntity": data,
            }),
            endCallCallback: () {
              overlayEntry?.remove();
              overlayEntry = null;
              GlobalData.instance.isAcceptCallKitIos = false;
              FlutterCallkitIncoming.endAllCalls();
            },
          ),
        ),
      );
    });
    Overlay.of(context).insert(overlayEntry);
  }

  void showCallVideoOverlay(BuildContext context, OfferCallEntity data) {
    final userCode = data.userMakeCall;
    final targetUser = ShortUserEntity(code: userCode, id: "", avatar: "");
    var overlayEntry;
    overlayEntry = OverlayEntry(builder: (context) {
      final size = MediaQuery.of(context).size;
      print(size.width);
      return Positioned(
        height: size.height,
        width: size.width,
        top: 0,
        left: 0,
        child: Material(
          child: VideoCallPage(
            routeArgument: RouteArgument(param: {
              'targetUser': targetUser,
              'isMakeCall': false,
              "isAnswer": true,
              "callkit": true,
              "offerCallEntity": data,
            }),
            endCallCallback: () {
              overlayEntry?.remove();
              overlayEntry = null;
              GlobalData.instance.isAcceptCallKitIos = false;
              FlutterCallkitIncoming.endAllCalls();
            },
          ),
        ),
      );
    });
    Overlay.of(context).insert(overlayEntry);
  }

  void pushNotiReady(String code, String type) {
    final data = {
      "action": "VIDEO_CALL",
      "type": "ready",
      "userMakeCall": code,
      "typeCall": type,
    };
    Get.find<WebsocketService>().subscribe(
      "message",
      data,
    );
  }
}
