import 'package:get/get.dart';

import 'base_controller.dart';
import 'package:flutter/widgets.dart';

import 'base_state.dart';

abstract class BaseWidgetState<W extends StatefulWidget,
    C extends BaseController, S extends BaseState> extends State<W> {
  C _controller;

  C get controller {
    if (_controller == null) {
      _controller = createController();
    }
    return _controller;
  }

  C createController();

  S get state => controller.state;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      onReady();
      _handleShowError();
      _handleShowProcessLoader();
    });
    super.initState();
  }

  //was called when all widget is rendered
  void onReady() {}

  //auto showing the error popup
  void _handleShowError() {
    ever(state.errorRx, (error) {
      //todo show general error, network error
    });
  }

  //show a process loader to block ui
  void _handleShowProcessLoader() {
    ever(state.isSubmittingRx, (isSubmitting) {
      //todo
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
