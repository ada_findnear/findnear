import 'package:findnear/src/base/base_controller.dart';
import 'package:findnear/src/base/base_widget_state.dart';
import 'package:findnear/src/widgets/common.dart';
import 'package:flutter/cupertino.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:get/get.dart';

import 'base_state.dart';

mixin RefreshLoadMoreWidgetMixin<W extends StatefulWidget,
    C extends BaseController, S extends BaseState> on BaseWidgetState<W, C, S> {
  RefreshController refreshController =
      RefreshController(initialRefresh: false);

  Widget buildList({@required Widget child}) {
    return SmartRefresher(
      header: ClassicHeader(
        releaseText: "",
        completeText: "",
        refreshingText: "",
        failedText: "",
        idleText: "",
        refreshingIcon: LoadingView(),
        completeIcon: SizedBox(),
        completeDuration: Duration(),
      ),
      child: child,
      footer: CustomFooter(
        builder: (BuildContext context, LoadStatus mode) {
          Widget footer  = SizedBox();
          if(mode==LoadStatus.idle){
      //      footer =  Text("pull up load");
          }
          else if(mode==LoadStatus.loading){
            footer =  SizedBox();
          }
          else if(mode == LoadStatus.failed){
        //    footer = Text("Load Failed!Click retry!");
          }
          else if(mode == LoadStatus.canLoading){
            //footer = Text("release to load more");
          }
          else{
            footer = Text("No more Data");
          }
          return Container(
            height: 40.0,
            child: Center(child:footer),
          );
        },
      ),
      enablePullUp:
          (controller as RefreshLoadMoreControllerMixin).canLoadMore,
      controller: refreshController,
      onRefresh: () async {
        await (controller as RefreshLoadMoreControllerMixin).refreshList();
        refreshController.refreshCompleted();
      },
      onLoading: () =>
          (controller as RefreshLoadMoreControllerMixin).loadList(),
    );
  }
}

mixin RefreshLoadMoreControllerMixin<S extends BaseState> on BaseController<S> {
  final _canLoadMore = true.obs;

  bool get canLoadMore => _canLoadMore.value;

  set canLoadMore(bool canLoadMore) => _canLoadMore.value = canLoadMore;
  int currentPage = 1;

  void refreshList() async {
    currentPage = 1;
    await loadList();
  }

  void loadList();
}
