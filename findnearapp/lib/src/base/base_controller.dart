import 'package:get/get.dart';

import 'base_state.dart';


abstract class BaseController<S extends BaseState> extends GetxController {
  S _state;

  S get state {
    if (_state == null) {
      _state = createState();
    }
    return _state;
  }

  S createState();
}
