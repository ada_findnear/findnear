import 'package:get/get.dart';

abstract class BaseState {
  //any error likes a string message, a exception or something else
  Rx<dynamic> errorRx = Rx<dynamic>(null);

  //loading for a page, the loading indicator should be shown inside a specify page
  RxBool isLoadingRx = true.obs;

  //the loading indicator should be shown over screen and blocks ui
  RxBool isSubmittingRx = false.obs;


  dynamic get error => errorRx.value;
  set error(dynamic error) => errorRx.value = error;

  bool get isLoading => isLoadingRx.value;
  set isLoading(bool isLoading) => isLoadingRx.value = isLoading;

  bool get isSubmitting => isSubmittingRx.value;
  set isSubmitting(bool isLoading) => isSubmittingRx.value = isLoading;
}
