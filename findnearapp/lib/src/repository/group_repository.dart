import 'dart:convert';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;
import '../helpers/helper.dart';
import '../models/user.dart';
import '../repository/user_repository.dart' as userRepo;
import '../helpers/http.dart';

final client = new AuthenticatedHttpClient();

class GroupRepository {
  Future<Stream<User>> getListFriend({String keyword = '', String ignoreId = '', int currentOffset = 0}) async {
    User _user = userRepo.currentUser.value;
    if (_user.apiToken == null) {
      return new Stream.value(null);
    }
    final String url =
        '${GlobalConfiguration().getValue('api_base_url')}users/list-friends?keyword=${keyword}&ignore_id=${ignoreId}&limit=15&offset=${currentOffset}';

    var request = http.Request('get', Uri.parse(url));
    // TODO refactor lại sau
    request.headers.addAll({'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}'});

    final streamedRest = await client.send(request);
    return streamedRest.stream.transform(utf8.decoder).transform(json.decoder).map((data) => Helper.getData(data)).expand((data) => (data as List)).map((data) {
      return User.fromJSON(data);
    });
  }
}
