import 'package:global_configuration/global_configuration.dart';

import '../models/entities/message_chat/entities/message_poll.dart';
import '../utils/logger.dart';
import 'base/base_network.dart';
import 'base/base_repository.dart';

abstract class MessagePollRepository {
  Future<MessagePoll> createPoll(
    String conversationOriginalId,
    MessagePoll messagePoll,
  );

  Future<MessagePoll> getPoll(String pollId);

  Future<MessagePoll> addOption(
    String pollId,
    String optionName,
  );

  Future<MessagePoll> lockPoll(String pollId);

  Future<MessagePoll> votePoll(
    String pollId,
    List<String> optionIds,
  );
}

class MessagePollApi extends BaseApi implements MessagePollRepository {
  MessagePollApi() {
    final baseUrl = GlobalConfiguration().getValue('base_socket_url');
    dio = BaseNetwork.getBaseDio(baseUrl: "$baseUrl/api/");
  }

  @override
  Future<MessagePoll> createPoll(
    String conversationOriginalId,
    MessagePoll messagePoll,
  ) async {
    if (conversationOriginalId == null ||
        conversationOriginalId.isEmpty ||
        messagePoll == null) {
      return null;
    }
    try {
      final response = await sendRequest(
        POST('polls/conversation/$conversationOriginalId'),
        body: messagePoll.toCreateRequest(),
        authenticate: true,
      );
      return MessagePoll.fromJson(response);
    } catch (e) {
      logger.e("MessagePollRepository - createPoll - error: $e");
      return null;
    }
  }

  Future<MessagePoll> getPoll(String pollId) async {
    if (pollId == null || pollId.isEmpty) {
      return null;
    }
    try {
      final response = await sendRequest(
        GET('polls/$pollId'),
        authenticate: true,
      );
      return MessagePoll.fromJson(response);
    } catch (e) {
      logger.e("MessagePollRepository - getPoll - error: $e");
      return null;
    }
  }

  Future<MessagePoll> addOption(
    String pollId,
    String optionName,
  ) async {
    if (pollId == null ||
        pollId.isEmpty ||
        optionName == null ||
        optionName.isEmpty) {
      return null;
    }
    try {
      final response = await sendRequest(
        POST('polls/$pollId/addOption'),
        body: {
          "optionName": optionName,
        },
        authenticate: true,
      );
      return MessagePoll.fromJson(response);
    } catch (e) {
      logger.e("MessagePollRepository - addOption - error: $e");
      return null;
    }
  }

  Future<MessagePoll> lockPoll(String pollId) async {
    if (pollId == null || pollId.isEmpty) {
      return null;
    }
    try {
      final response = await sendRequest(
        POST('polls/$pollId/lock'),
        authenticate: true,
      );
      return MessagePoll.fromJson(response);
    } catch (e) {
      logger.e("MessagePollRepository - lockPoll - error: $e");
      return null;
    }
  }

  Future<MessagePoll> votePoll(
    String pollId,
    List<String> optionIds,
  ) async {
    if (pollId == null ||
        pollId.isEmpty ||
        optionIds == null ||
        optionIds.isEmpty) {
      return null;
    }
    try {
      final response = await sendRequest(
        POST('polls/$pollId/vote'),
        body: {
          "optionIds": optionIds,
        },
        authenticate: true,
      );
      return MessagePoll.fromJson(response);
    } catch (e) {
      logger.e("MessagePollRepository - votePoll - error: $e");
      return null;
    }
  }
}
