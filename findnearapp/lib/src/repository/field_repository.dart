import 'dart:convert';

import 'package:findnear/src/helpers/custom_trace.dart';
import 'package:findnear/src/models/city.dart';
import 'package:findnear/src/models/response/base_response.dart';
import 'package:findnear/src/repository/base/base_repository.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;

import '../helpers/helper.dart';
import '../models/field.dart';
import '../helpers/http.dart';

final client = new AuthenticatedHttpClient();

Future<Stream<Field>> getFields() async {
  final String url = '${GlobalConfiguration().getValue('api_base_url')}fields?orderBy=position&sortedBy=asc';
  int start = DateTime.now().millisecondsSinceEpoch;
  final streamedRest = await client.send(http.Request('get', Uri.parse(url)));
  int end = DateTime.now().millisecondsSinceEpoch;
  print('quanth: timer2 splash - getFields = ${end-start} milis');
  int start2 = DateTime.now().millisecondsSinceEpoch;
  return streamedRest.stream.transform(utf8.decoder).transform(json.decoder).map((data) => Helper.getData(data)).expand((data) => (data as List)).map((data) {
    int end2 = DateTime.now().millisecondsSinceEpoch;
    print('quanth: timer2 splash - getFields 2 = ${end2-start2} milis');
    return Field.fromJSON(data);
  });
}

Future<Stream<City>> getLocalAddress() async {
  Uri uri = Helper.getUri('api/local');
  Map<String, dynamic> _queryParams = {};
  uri = uri.replace(queryParameters: _queryParams);
  try {
    final streamedRest = await client.send(http.Request('get', uri));
    return streamedRest.stream
        .transform(utf8.decoder)
        .transform(json.decoder)
        .map((data) => Helper.getData(data))
        .expand((data) => (data as List))
        .map((data) => City.fromJSON(data));
  } catch (e) {
    print(
        CustomTrace(StackTrace.current, message: uri.toString()).toString());
    return new Stream.value(new City.fromJSON({}));
  }
}

Future<Stream<Field>> getShopFields() async {
  String url = '${GlobalConfiguration().getValue('api_base_url')}fields?offset=0&limit=100&search=is_shop_field:1&searchFields=is_shop_field:=';

  final streamedRest = await client.send(http.Request('get', Uri.parse(url)));

  return streamedRest.stream.transform(utf8.decoder).transform(json.decoder).map((data) => Helper.getData(data)).expand((data) => (data as List)).map((data) {
    return Field.fromJSON(data);
  });
}

abstract class FieldRepository {
  Future<List<Field>> getShopFields();
  Future<List<Field>> getFields();
}

class FieldRepositoryApi extends BaseApi
    implements FieldRepository {

  @override
  Future<List<Field>> getShopFields() async {
    Map<String, dynamic> _queryParams = {};
    _queryParams['offset'] = "0";
    _queryParams['limit'] = "100";
    _queryParams['search'] = "is_shop_field:1";
    _queryParams['searchFields'] = "is_shop_field:=";

    final response = await sendRequest(
        GET('fields'),
        authenticate: true,
        queryParameters: _queryParams);

    return BaseResponse<List<Field>>.fromJson(
      response,
          (data) =>
      data is List
          ? data.map((e) => Field.fromJSON(e)).toList(growable: false)
          : <Field>[],
    ).data;
  }

  Future<List<Field>> getFields() async {
    int start = DateTime.now().millisecondsSinceEpoch;
    Map<String, dynamic> _queryParams = {};
    _queryParams['orderBy'] = "position";
    _queryParams['sortedBy'] = "asc";

    final response = await sendRequest(
        GET('fields'),
        authenticate: true,
        queryParameters: _queryParams);
    int end = DateTime.now().millisecondsSinceEpoch;
    print('quanth: timer2 splash - getFields = ${end-start} milis');
    return BaseResponse<List<Field>>.fromJson(
      response,
          (data) =>
      data is List
          ? data.map((e) => Field.fromJSON(e)).toList(growable: false)
          : <Field>[],
    ).data;
  }

}


