import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:findnear/src/models/category.dart';
import 'package:findnear/src/models/order_history.dart';
import 'package:findnear/src/models/response/base_response.dart';
import 'package:findnear/src/models/response/category_response.dart';
import 'package:findnear/src/models/response/order_history_response.dart';
import 'package:findnear/src/models/response/product_response.dart';
import 'package:findnear/src/models/response/rating_response.dart';
import 'package:findnear/src/models/response/recommend_type_response.dart';
import 'package:findnear/src/models/response/shop_detail_response.dart';
import 'package:findnear/src/models/response/shop_response.dart';
import 'package:findnear/src/models/shop.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart' as getX;
import 'package:get/get_connect/http/src/request/request.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;
import '../models/address.dart';
import '../helpers/custom_trace.dart';
import '../helpers/http.dart';
import '../helpers/helper.dart';

import 'base/base_repository.dart';

final client = new AuthenticatedHttpClient();

Future<Stream<Shop>> searchShops(String search, Address address) async {
  Uri uri = Helper.getUri('api/shops');
  var request = http.Request('get', uri);
  Map<String, dynamic> _queryParams = {};
  _queryParams['keyword'] = search;
  _queryParams['limit'] = '20';
  if (!address.isUnknown()) {
    _queryParams['myLon'] = address.longitude.toString();
    _queryParams['myLat'] = address.latitude.toString();
    _queryParams['areaLon'] = address.longitude.toString();
    _queryParams['areaLat'] = address.latitude.toString();
  }

  /// Nếu có token thì mới push lên cùng api
  if (currentUser.value.apiToken.isNotEmpty) {
    request.headers.addAll({
      'Authorization':
          'Bearer ${getX.Get.find<LocalDataManager>().currentUser?.apiToken}'
    });
  }
  uri = uri.replace(queryParameters: _queryParams);
  try {
    final streamedRest = await client.send(request);

    return streamedRest.stream
        .transform(utf8.decoder)
        .transform(json.decoder)
        .map((data) {
      return (data as Map<String, dynamic>)['data']['data'] ?? [];
    }).expand((data) {
      return (data as List);
    }).map((data) {
      return Shop.fromJson(data);
    });
  } catch (e) {
    print(CustomTrace(StackTrace.current, message: uri.toString()).toString());
    return new Stream.value(new Shop.fromJson({}));
  }
}

abstract class ShopsRepository {
  Future<ShopsResponse> getRecommendedShops(
      {int page = 1, int perPage = 20, int recommendTypeId});

  Future<ShopsResponse> getNearShops(
      {int page = 1,
      int perPage = 20,
      double long,
      double lat,
      String fieldID = ''});

  Future<ProductsResponse> getProducts(
      {int page = 1,
      int perPage = 10,
      int shopId,
      int categoryId,
      String keyword});

  Future<ShopsDetailResponse> getShopDetail({
    int shopId,
    double long,
    double lat,
    int limit = 40,
  });

  Future<Product> deleteProduct({
    int productId,
  });

  Future<RecommendTypeResponse> getRecommendType();

  Future<Product> createProduct(
      {int id = 0,
      String name,
      String description,
      String price,
      List<int> categoriesId,
      List<String> imagesPath = null});

  /**
   * Refactor later
   * Service api is too messy :(
   * Why ???????????
   */
  Future<Product> updateProduct(Product product, List<String> addImages,
      List<String> deleteImages, List<int> categoriesId);

  Future<CategoryResponse> getCategories({int shopId});

  Future<RatingResponse> getRating(int shopId);
  Future<OrderHistoryResponse> getOrderHistory(int shopId);
}

class ShopsMock extends ShopsRepository {
  @override
  Future<ShopsResponse> getRecommendedShops(
      {int page = 1,
      int perPage = 20,
      double long,
      double lat,
      int recommendTypeId}) async {
    await Future.delayed(Duration(seconds: 1));
    return ShopsResponse();
  }

  @override
  Future<ShopsResponse> getNearShops(
      {int page = 1,
      int perPage = 20,
      double long,
      double lat,
      String fieldID = ''}) async {
    await Future.delayed(Duration(seconds: 1));
    return ShopsResponse();
  }

  @override
  Future<ProductsResponse> getProducts(
      {int page = 1,
      int perPage = 10,
      int shopId,
      int categoryId,
      String keyword}) async {
    return ProductsResponse();
  }

  @override
  Future<Product> createProduct(
      {int id = 0,
      String name,
      String description,
      String price,
      List<int> categoriesId,
      List<String> imagesPath = null}) async {
    return Product();
  }

  @override
  Future<Product> updateProduct(Product product, List<String> addImages,
      List<String> deleteImages, List<int> categoriesId) {
    // TODO: implement updateProduct
    throw UnimplementedError();
  }

  @override
  Future<ShopsDetailResponse> getShopDetail({
    int limit = 40,
    int shopId,
    double long,
    double lat,
  }) {
    /*final response = await sendRequest(
      GET('shops'),
      queryParameters: {
        'limit': perPage,
        'page': page,
        'recommended': 1,
        'myLon': long,
        'myLat': lat,
        'login_user_id': currentUser.value.id,
      },
    );

    return ProductsResponse.fromJson(response);*/
  }

  @override
  Future<CategoryResponse> getCategories({int shopId}) async {
    // TODO: implement getCategories
    return CategoryResponse();
  }

  @override
  Future<RatingResponse> getRating(int shopId) async {
    // TODO: implement getRating
    return RatingResponse();
  }

  @override
  Future<OrderHistoryResponse> getOrderHistory(int shopId) async {
    // TODO: implement getOrderHistory
    return OrderHistoryResponse();
  }

  @override
  Future<RecommendTypeResponse> getRecommendType() {
    // TODO: implement getRecommend
    throw UnimplementedError();
  }

  @override
  Future<Product> deleteProduct({int productId}) {
    // TODO: implement deleteProduct
    throw UnimplementedError();
  }
}

class ShopsApi extends BaseApi implements ShopsRepository {
  @override
  Future<ShopsResponse> getRecommendedShops(
      {int page = 1, int perPage = 20, int recommendTypeId}) async {
    final response = await sendRequest(
      GET('v2/shops'),
      queryParameters: {
        'limit': perPage,
        'page': page,
        'recommended': recommendTypeId != null ? 0 : 1,
        'recommendation_type_id': recommendTypeId,
        'login_user_id': currentUser.value?.id ?? "",
      },
    );

    return ShopsResponse.fromJson(response);
  }

  @override
  Future<ShopsResponse> getNearShops(
      {int page = 1,
      int perPage = 20,
      double long,
      double lat,
      String fieldID = ''}) async {
    double radius = 2000.0;

    final response = await sendRequest(
      GET('v2/shops'),
      queryParameters: {
        'limit': perPage,
        'page': page,
        'login_user_id': currentUser.value?.id,
        'myLon': long,
        'myLat': lat,
        'radius': radius,
        if (fieldID.isNotEmpty) 'field_id': fieldID,
      },
    );
    return ShopsResponse.fromJson(response);
  }

  @override
  Future<ProductsResponse> getProducts(
      {int page = 1,
      int perPage = 10,
      int shopId,
      int categoryId,
      String keyword}) async {
    final String url =
        '${GlobalConfiguration().getValue('api_base_url')}v2/vnp_products?${keyword == null ? '' : 'keyword=$keyword&'}limit=$perPage&offset=$page&shop_id=$shopId${categoryId != null ? '&category_id=$categoryId' : ''}';
    log(url);
    final client = new http.Client();
    final response = await client.get(
      Uri.parse(url),
      headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    );
    logger.log("URL $url ");
    logger.log("REQUEST ");
    logger.log("RESPONSE: ${response.body}");
    if (response.statusCode != 200) throw 'Error';
    log(response.body);
    return ProductsResponse.fromJson(jsonDecode(response.body));
  }

  @override
  Future<Product> createProduct(
      {int id = 0,
      String name,
      String description,
      String price,
      List<int> categoriesId,
      List<String> imagesPath = null}) async {
    String url =
        '${GlobalConfiguration().getValue('api_base_url')}v2/vnp_products';

    /// Nếu có token thì mới truyền lên cùng
    if (currentUser.value.apiToken.isNotEmpty)
      url = '${GlobalConfiguration().getValue('api_base_url')}v2/vnp_products';
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.fields.addAll({
      'id': id.toString(),
      'name': name,
      'price': price,
      'description': description,
    });
    request.headers.addAll({
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Bearer ${getX.Get.find<LocalDataManager>().currentUser?.apiToken}'
    });
    if (imagesPath != null && imagesPath.isNotEmpty) {
      imagesPath.forEach((element) {
        if (!element.startsWith('http'))
          request.files.add(http.MultipartFile(
              "files[]",
              File(element).readAsBytes().asStream(),
              File(element).lengthSync(),
              filename: element.split("/").last));
      });
    }
    if (categoriesId != null && categoriesId.isNotEmpty) {
      var categoriesIdField = Map.fromIterable(categoriesId,
          key: (e) => "categories_id[$e]", value: (e) => "$e");
      print(categoriesIdField);
      request.fields.addAll(categoriesIdField);
    }
    final client = new http.Client();
    http.Response response =
        await http.Response.fromStream(await client.send(request));
    logger.log("URL $url \n ${request.headers}");
    logger.log("REQUEST ${request.fields}");
    logger.log("RESPONSE: ${response.body}");
    if (response?.statusCode != 200) throw 'Error';
    return Product.fromJson(jsonDecode(response.body)['data']);
  }

  @override
  Future<Product> updateProduct(Product product, List<String> addImages,
      List<String> deleteImages, List<int> categoriesId) async {
    var task = <Future>[];
    if (deleteImages != null && deleteImages.isNotEmpty) {
      deleteImages.forEach((element) {
        task.add(removeImagesFromProduct(element));
      });
    }
    if (addImages != null && addImages.isNotEmpty) {
      task.add(addImagesToProduct(product.id.toString(), addImages));
    }
    final formData = FormData();

    formData.fields.addAll([
      MapEntry('name', product.name),
      MapEntry('price', '${product.price}'),
      MapEntry('description', '${product.description}'),
    ]);
    // formData.fields.add(entryDescription);
    if (categoriesId != null && categoriesId.isNotEmpty) {
      categoriesId.forEach((id) {
        final entry = MapEntry('categories_id[]', '$id');
        formData.fields.add(entry);
      });
    }
    await Future.wait(task);
    // vnp_products
    final response = await sendRequest(
      POST('v2/vnp_products/').addPath(product.id.toString()),
      body: formData,
      authenticate: true,
    );
    return BaseResponse<Product>.fromJson(
        response, (data) => Product.fromJson(data)).data;
  }

  /**
   * Don't ask me why! Ask service
   */
  Future<bool> addImagesToProduct(
      String productId, List<String> imagePaths) async {
    final formData = FormData();
    if ((imagePaths?.length ?? 0) != 0) {
      imagePaths.forEach((path) {
        final entry = MapEntry(
          'files[]',
          MultipartFile.fromFileSync(path, filename: path.split("/").last),
        );
        formData.files.add(entry);
      });
    }
    final response = await sendRequest(
      POST('vnp_products/$productId/media'),
      authenticate: true,
      body: formData,
    );
    return response['success'];
  }

  /**
   * Don't ask me why! Ask service
   */
  Future<bool> removeImagesFromProduct(String mediaId) async {
    final response = await sendRequest(
      DELETE('media/$mediaId'),
      authenticate: true,
    );
    return response['success'];
  }

  @override
  Future<ShopsDetailResponse> getShopDetail({
    int limit = 40,
    int shopId,
    double long,
    double lat,
  }) async {
    final response = await sendRequest(
      GET('v2/shops/${shopId}'),
      queryParameters: {
        'myLat': lat,
        'myLon': long,
        'limit': limit,
        'login_user_id': currentUser.value?.id ?? "",
      },
    );

    return ShopsDetailResponse.fromJson(response);
  }

  @override
  Future<CategoryResponse> getCategories({int shopId}) async {
    final response = await sendRequest(
      GET('v2/vnp_categories'),
      queryParameters: {'shop_id': shopId},
    );

    return CategoryResponse.fromJson(response);
  }

  @override
  Future<RatingResponse> getRating(int shopId) async {
    final response = await sendRequest(
      GET('v2/shops/ratings'),
      queryParameters: {'shop_id': shopId},
    );

    return RatingResponse.fromJson(response);
  }

  @override
  Future<OrderHistoryResponse> getOrderHistory(int shopId) async {
    final response = await sendRequest(GET('v2/vnp_orders'),
        queryParameters: {'shop_id': shopId},
        options: Options(headers: {
          'Authorization':
              'Bearer  ${getX.Get.find<LocalDataManager>().currentUser?.apiToken}'
        }));
    return OrderHistoryResponse.fromJson(response);
  }

//shops/recommendation-types
  @override
  Future<RecommendTypeResponse> getRecommendType() async {
    final response = await sendRequest(
      GET('v2/shops/recommendation-types'),
    );
    return RecommendTypeResponse.fromJson(response);
  }

  @override
  Future<Product> deleteProduct({int productId}) async {
    final response = await sendRequest(
      DELETE('v2/vnp_products/${productId ?? ''}'),
      options: Options(
        headers: {
          'Authorization':
              'Bearer  ${getX.Get.find<LocalDataManager>().currentUser?.apiToken}'
        },
      ),
    );
    return BaseResponse<Product>.fromJson(
        response, (data) => Product.fromJson(data)).data;
  }
}
