import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:findnear/src/models/Business.dart';
import 'package:findnear/src/models/city.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/utils/local_data_manager.dart';

import 'package:findnear/src/utils/logger.dart';
import 'package:get/get.dart';

import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;
import 'package:get/get.dart' as getX;
import '../repository/user_repository.dart' as userRepo;

import '../helpers/custom_trace.dart';
import '../helpers/helper.dart';

import '../helpers/http.dart';
import 'base/base_repository.dart';
import 'user_repository.dart';

abstract class RegisterShopRepository {
  Future<Business> getBusiness();

  Future<Stream<City>> getLocalAddress();

  Future<http.Response> registerShop(
    String title,
    String user_id,
    String city,
    String district,
    String ward,
    String street,
    String description,
    String pathMedia,
    String fieldId,
    double latitude,
    double longitude,
  );
}

class RegisterShopApi extends BaseApi implements RegisterShopRepository {
  final client = new AuthenticatedHttpClient();

  @override
  Future<Business> getBusiness() async {
    final String url =
        '${GlobalConfiguration().getValue('api_base_url')}vnp_categories';
    final client = new http.Client();
    final response = await client.get(
      Uri.parse(url),
      headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    );
    if (response.statusCode != 200) throw 'Error';

    Business business = Business.fromJSON(jsonDecode(response.body));
    return business;
  }

  @override
  Future<Stream<City>> getLocalAddress() async {
    Uri uri = Helper.getUri('api/local');
    Map<String, dynamic> _queryParams = {};
    uri = uri.replace(queryParameters: _queryParams);
    try {
      final streamedRest = await client.send(http.Request('get', uri));

      return streamedRest.stream
          .transform(utf8.decoder)
          .transform(json.decoder)
          .map((data) => Helper.getData(data))
          .expand((data) => (data as List))
          .map((data) => City.fromJSON(data));
    } catch (e) {
      print(
          CustomTrace(StackTrace.current, message: uri.toString()).toString());
      return new Stream.value(new City.fromJSON({}));
    }
  }

  @override
  Future<http.Response> registerShop(
    String title,
    String user_id,
    String city,
    String district,
    String ward,
    String street,
    String description,
    String pathMedia,
    String fieldId,
    double latitude,
    double longitude,
  ) async {
    final String url = '${GlobalConfiguration().getValue('api_base_url')}shops';
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.fields.addAll({"title": title});
    request.fields.addAll({"user_id": user_id});
    request.fields.addAll({"city": city});
    request.fields.addAll({"district": district});
    request.fields.addAll({"ward": ward});
    request.fields.addAll({"street": street});
    request.fields.addAll({"latitude": latitude.toString()});
    request.fields.addAll({"longitude": longitude.toString()});
    if (fieldId != null && fieldId.isNotEmpty) {
      request.fields.addAll({"field_id": fieldId});
    }
    if (description != null && description.isNotEmpty) {
      request.fields.addAll({"description": description});
    }
    User _user = Get.find<LocalDataManager>().currentUser;
    final String _apiToken = '${_user.apiToken}';
    if (_user.apiToken == null) {
      request.headers.addAll({
        HttpHeaders.contentTypeHeader: 'application/json',
      });
    } else
      request.headers.addAll({
        HttpHeaders.contentTypeHeader: 'application/json',
        'Authorization': 'Bearer $_apiToken'
      });
    if (pathMedia != null && !pathMedia.startsWith('http')) {
      logger.log('pathMedia != null');
      request.files.add(http.MultipartFile(
          'image',
          File(pathMedia).readAsBytes().asStream(),
          File(pathMedia).lengthSync(),
          filename: pathMedia.split("/").last));
    }
    logger.log(
        "✈️ REQUEST['POST'] => PATH: $url \n headers: ${request.headers} image:  ${request.files}",
        printFullText: true);
    logger.log('dbdegas: ' + request.fields.toString());
    http.Response response =
        await http.Response.fromStream(await client.send(request));
    logger.log("✅ aaaaaaaaaaa" + response.body);
    return response;
  }
}
