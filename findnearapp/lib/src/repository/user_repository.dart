import 'dart:convert';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../database/conversations_dao.dart';
import '../helpers/helper.dart';
import '../helpers/http.dart';
import '../models/market.dart';
import '../models/user.dart' as userModel;
import '../pages/videocall/callsignaling.dart';
import '../services/websocket_service.dart';
import '../settings/global_variable.dart' as GlobalVar;
import '../utils/FirebaseMessageManager.dart';
import '../utils/local_data_manager.dart';
import '../utils/logger.dart';
import 'user_v2_repository.dart';

final client = new AuthenticatedHttpClient();

ValueNotifier<userModel.User> currentUser = new ValueNotifier(userModel.User());

bool get isLoggedIn => currentUser.value?.apiToken?.isNotEmpty ?? false;

Future<userModel.User> login(userModel.User user) async {
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}v2/login';
  final response = await client.post(
    Uri.parse(url),
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(user.toMap()),
  );
  prettyLog.d(json.encode(user.toMap()));
  prettyLog.d(response.body);
  if (response.statusCode == 200) {
    setCurrentUser(response.body);
    currentUser.value =
        userModel.User.fromJSON(json.decode(response.body)['data']);

    Get.find<LocalDataManager>().saveLoginId(currentUser.value?.code);
    saveCurrentLocation();
  } else {
    prettyLog.e(response.body);
    throw new Exception(response.body);
  }

  return currentUser.value;
}

Future<userModel.User> register(userModel.User user) async {
  final String url =
      '${GlobalConfiguration().getValue('api_base_manager_url')}code/register';
  final bodyParam = {'dealer_code': user.dealerCode, 'phone': user.phone};
  var body = json.encode(bodyParam);
  final response = await client.post(
    Uri.parse(url),
    body: body,
  );
  prettyLog.e(response.body);
  if (response.statusCode == 200) {
    setCurrentUser(response.body);
    currentUser.value =
        userModel.User.fromJSON(json.decode(response.body)['data']);
    saveCurrentLocation();
  } else {
    final body = json.decode(response.body);
    var messsage = body['message'];
    throw new Exception(messsage);
  }

  return currentUser.value;
}

Future<bool> resetPassword(userModel.User user) async {
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}send_reset_link_email';
  final response = await client.post(
    Uri.parse(url),
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(user.toMap()),
  );
  if (response.statusCode == 200) {
    return true;
  } else {
    throw new Exception(response.body);
  }
}

Future<void> logout() async {
  try {
    Get.find<WebsocketService>().disconnect();
    if (currentUser.value == null) {
      return;
    }
    final apiToken = currentUser.value.apiToken;
    final String url =
        '${GlobalConfiguration().getValue('api_base_url')}logout';
    new http.Client().get(Uri.parse(url), headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer ${apiToken}'
    });
    await Future.delayed(Duration(milliseconds: 500));
    // clear conversations cache and remove all chat caches
    Get.find<ConversationsDAO>().clearIncludeChats();
    currentUser.value = new userModel.User();
    await Get.find<LocalDataManager>()?.logout();
    await Get.find<FirebaseMessageManager>()?.signOut();
    await Get.find<CallSignaling>().close();
  } catch (e) {
    print("UserRepository - logout - error: $e");
  }
}

void setCurrentUser(jsonString) async {
  if (jsonString != null && json.decode(jsonString)['data'] != null) {
    Get.find<LocalDataManager>()
        .saveCurrentUser(json.decode(jsonString)['data']);
  } else {
    Get.find<LocalDataManager>().logout();
  }
}

Future<userModel.User> getCurrentUser() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  //prefs.clear();
  if (currentUser.value?.auth == null && prefs.containsKey('current_user')) {
    currentUser.value =
        userModel.User.fromJSON(json.decode(await prefs.get('current_user')));
    currentUser.value?.auth = true;
  } else {
    currentUser.value?.auth = false;
  }
  // ignore: invalid_use_of_visible_for_testing_member, invalid_use_of_protected_member
  currentUser.notifyListeners();
  return currentUser.value;
}

Future<userModel.User> getUser(int idUser) async {
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}users/$idUser';
  var request = http.MultipartRequest('GET', Uri.parse(url));
  request.headers.addAll({
    'Authorization':
        'Bearer ${Get.find<LocalDataManager>().currentUser?.apiToken}'
  });
  http.Response response =
      await http.Response.fromStream(await client.send(request));
  if (int.tryParse(currentUser.value?.id) == idUser) {
    try {
      var apiToken = currentUser.value.apiToken;
      currentUser.value.apiToken = apiToken;
      userModel.User userNew = userModel.User.fromJSON(json.decode(response.body)['data']);
      userNew.apiToken = apiToken;
      currentUser.value = userNew;
      var map = Map<String,String>();
      map["data"] = jsonEncode(userNew.toMap()).toString();
      setCurrentUser(jsonEncode(map));

    } catch (e) {
      print('xxxxxxxxx--------------------------->');
      print('try to get user: $idUser');
      print(e);
    }
  }
  return userModel.User.fromJSON(json.decode(response.body)['data']);
}

Future<userModel.User> updateUserWhenUpdateMarket(int idUser) async {
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}users/$idUser';
  var request = http.MultipartRequest('GET', Uri.parse(url));
  request.headers.addAll({
    'Authorization':
    'Bearer ${Get.find<LocalDataManager>().currentUser?.apiToken}'
  });
  http.Response response =
  await http.Response.fromStream(await client.send(request));
  var apiToken = currentUser.value.apiToken;
  userModel.User userNew = userModel.User.fromJSON(json.decode(response.body)['data']);
  userNew.apiToken = apiToken;
  currentUser.value = userNew;
  var map = Map<String,String>();
  map["data"] = jsonEncode(userNew.toMap()).toString();
  return userModel.User.fromJSON(json.decode(jsonEncode(map))['data']);
}

Future<userModel.User> blockUser(int idUser) async {
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}users/$idUser/block';
  var request = http.MultipartRequest('POST', Uri.parse(url));
  // TODO refactor lại sau
  request.headers.addAll({
    'Authorization':
        'Bearer ${Get.find<LocalDataManager>().currentUser?.apiToken}'
  });
  http.Response response =
      await http.Response.fromStream(await client.send(request));
  return userModel.User.fromJSON(json.decode(response.body)['data']);
}

Future<userModel.User> unblockUser(int idUser) async {
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}users/$idUser/unblock';
  var request = http.MultipartRequest('POST', Uri.parse(url));
  // TODO refactor lại sau
  request.headers.addAll({
    'Authorization':
        'Bearer ${Get.find<LocalDataManager>().currentUser?.apiToken}'
  });
  http.Response response =
      await http.Response.fromStream(await client.send(request));
  return userModel.User.fromJSON(json.decode(response.body)['data']);
}

Future<Stream<userModel.User>> getBlockingUsers() async {
  final String url = 'api/my-blockings';
  Uri uri = Helper.getUri(url);

  Map<String, dynamic> _queryParams = {};
  uri = uri.replace(queryParameters: _queryParams);
  // TODO refactor lại sau
  var request = http.Request('get', uri);
  request.headers.addAll({
    'Authorization':
        'Bearer ${Get.find<LocalDataManager>().currentUser?.apiToken}'
  });

  try {
    final streamedRest = await client.send(request);
    return streamedRest.stream
        .transform(utf8.decoder)
        .transform(json.decoder)
        .map((data) => Helper.getData(data))
        .expand((data) => (data as List))
        .map((data) {
      return userModel.User.fromJSON(data);
    });
  } catch (e) {
    return new Stream.value(new userModel.User.fromJSON({}));
  }
}

Future<userModel.User> update(
    {userModel.User user,
    String pathImage = null,
    String imageType = null}) async {
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}users/${currentUser.value.id}';
  var request = http.MultipartRequest('POST', Uri.parse(url));
  request.fields.addAll(user.toUpdateUserModel());
  request.headers.addAll({HttpHeaders.contentTypeHeader: 'application/json'});
  // TODO refactor lại sau
  request.headers.addAll({
    'Authorization':
        'Bearer ${Get.find<LocalDataManager>().currentUser?.apiToken}'
  });
  if (pathImage != null) {
    request.files.add(http.MultipartFile(imageType,
        File(pathImage).readAsBytes().asStream(), File(pathImage).lengthSync(),
        filename: pathImage.split("/").last));
  }
  http.Response response =
      await http.Response.fromStream(await client.send(request));
  var apiToken = currentUser.value.apiToken;
  currentUser.value.apiToken = apiToken;
  userModel.User userNew = userModel.User.fromJSON(json.decode(response.body)['data']);
  userNew.apiToken = apiToken;
  currentUser.value = userNew;
  var map = Map<String,String>();
  map["data"] = jsonEncode(userNew.toMap()).toString();
  setCurrentUser(jsonEncode(map));
  return currentUser.value;
}

Future<void> saveCurrentLocation() async {
  userModel.User _user = new userModel.User();
  Market market = new Market();
  Position position;
  try {
    position = GlobalVar.current_location = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
  } on Exception catch (_) {
    position = GlobalVar.current_location;
  }
  if (currentUser.value?.id != null) {
    _user = await getUser(int.parse(currentUser.value?.id));
    market = _user.market;
  }
  if (Get.find<LocalDataManager>().currentUser != null &&
      market.id != null &&
      position.latitude != null &&
      position.longitude != null &&
      market != null &&
      !market.fixed_position) {
    market.latitude = '${position.latitude}';
    market.longitude = '${position.longitude}';
    final String url =
        '${GlobalConfiguration().getValue('api_base_url')}v2/markets/${market.id}';
    await client.put(Uri.parse(url),
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.authorizationHeader:
              'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}'
        },
        body: market.toMap());
  }
}

Future<void> checkExpiredApiToken() async {
  final currentUser = await getCurrentUser();
  if (currentUser.code == null) return;

  final user =
      (await Get.find<UserV2Repository>().getUser(currentUser.code)).data.first;
  final fcmToken = await FirebaseMessaging.instance?.getToken();
  if (user == null || fcmToken == null) return;

  if (fcmToken != user.deviceToken) {
    await logout();
  }
}
