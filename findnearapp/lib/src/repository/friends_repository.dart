import 'dart:convert';
import 'dart:io';

import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';

import '../models/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../helpers/helper.dart';
import '../repository/user_repository.dart' as userRepo;
import '../models/user.dart' as userModel;
import '../helpers/http.dart';

final client = new AuthenticatedHttpClient();


Future<Stream<User>> getFriendsRequest({offset = '', limit = ''}) async {
  User _user = userRepo.currentUser.value;
  if (_user.apiToken == null) {
    return new Stream.value(null);
  }
  final String url = '${GlobalConfiguration().getValue('api_base_url')}users/list-friends-request?limit=${limit}&offset=${offset}';

  var request = http.Request('get', Uri.parse(url));
  // TODO refactor lại sau
  request.headers.addAll({'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}'});
  final streamedRest = await client.send(request);
  print(streamedRest.statusCode);
  return streamedRest.stream.transform(utf8.decoder).transform(json.decoder).map((data) => Helper.getData(data)).expand((data) => (data as List)).map((data) {
    return User.fromJSON(data['sender']);
  });
}

Future<Stream<User>> getListFriend({String keyword = '', int currentOffset = 0}) async {
  User _user = userRepo.currentUser.value;
  if (_user.apiToken == null) {
    return new Stream.value(null);
  }
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}users/list-friends?keyword=${keyword}&limit=15&offset=${currentOffset}';

  var request = http.Request('get', Uri.parse(url));
  // TODO refactor lại sau
  request.headers.addAll({'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}'});
  final streamedRest = await client.send(request);
  return streamedRest.stream.transform(utf8.decoder).transform(json.decoder).map((data) => Helper.getData(data)).expand((data) => (data as List)).map((data) {
    return User.fromJSON(data);
  });
}

Future<userModel.User> addFriend(String idUserTarget, String type) async {
  User _user = userRepo.currentUser.value;
  var uri = "add-friend";
  var body = {};
  switch (type) {
    case "confirm":
      body = {
        "accept": true
      };
      uri = "confirm-friend";
      break;
    case "deny":
      body = {
        "accept": false
      };
      uri = "confirm-friend";
      break;
    case "cancel":
      uri = "cancel-friend-request";
      break;
    case "delete":
      uri = "unfriend";
  }
  final String url = '${GlobalConfiguration().getValue('api_base_url')}users/$uri/$idUserTarget';
  final response = await client.post(
      Uri.parse(url),
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}' // TODO refactor lại sau
      },
      body: json.encode(body)
  );
  if (response.statusCode != 200) {
    throw new Exception(response.body);
  }
  return userModel.User.fromJSON(json.decode(response.body)['data']);
}