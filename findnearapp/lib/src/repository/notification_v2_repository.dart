import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:findnear/src/models/entities/offer_call_entity.dart';
import 'package:findnear/src/models/response/call_notification_response.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/videocall/callsignaling.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:uuid/uuid.dart';

import '../utils/logger.dart';
import 'base/base_network.dart';
import 'base/base_repository.dart';
import 'settings_repository.dart';

enum TypeNotification {
  MESSAGE,
  CALL,
}

extension TypeNotificationExtension on TypeNotification {
  static TypeNotification fromEventName(String eventName) {
    if (eventName == "message") {
      return TypeNotification.MESSAGE;
    } else if (eventName == "call") {
      return TypeNotification.CALL;
    }
    return null;
  }

  String get value {
    switch (this) {
      case TypeNotification.MESSAGE:
        return "message";
      case TypeNotification.CALL:
        return "call";
      default:
        return "";
    }
  }
}

abstract class NotificationV2Repository {
  Future<void> sendNotification(String title, String body, String deviceToken);

  Future<void> sendCallNotification(
      String typeMessage, String deviceToken, String action,
      {String userCallName,
      String userAvatar,
      String userCode,
      OfferCallEntity offerCallEntity});
}

class NotificationV2Api extends BaseApi implements NotificationV2Repository {
  NotificationV2Api() {
    dio = BaseNetwork.getBaseDio(
      baseUrl: "https://fcm.googleapis.com/fcm/send",
    );
  }

  @override
  Future<void> sendNotification(
      String title, String body, String deviceToken) async {
    if (deviceToken == null || deviceToken.isEmpty) return;
    final data = {
      "notification": {"title": "$title", "body": "$body", "sound": "default"},
      "priority": "high",
      "data": {
        "click_action": "FLUTTER_NOTIFICATION_CLICK",
        "id": "messages",
        "status": "done"
      },
      "to": "$deviceToken",
    };
    try {
      await sendRequest(
        POST(""),
        options: Options(
          headers: {
            "Content-Type": "application/json",
            "Authorization": "key=${setting.value.fcmKey}",
          },
        ),
        body: data,
      );
    } catch (e) {
      logger.e("NotificationV2Repository - sendNotification - error: $e");
    }
  }

  @override
  Future<void> sendCallNotification(
      String typeMessage, String deviceToken, String action,
      {String userCallName,
      String userAvatar,
      String userCode,
      OfferCallEntity offerCallEntity}) async {
    if (deviceToken == null || deviceToken.isEmpty) return;
    CallSignaling signaling = Get.find<CallSignaling>();

    try {
      var params = <String, dynamic>{
        "messageType": typeMessage,
        'id': signaling.currentUuid,
        'nameCaller': userCallName ?? "",
        'action': action,
        'appName': 'Findnear',
        'avatar': userAvatar ?? "",
        "timeSend": (DateTime.now().millisecondsSinceEpoch),
        'codeCaller': userCode,
        'number': userCode,
        'handle': userCode,
      }; //

      final data = {
        "priority": "high",
        "content_available": true,
        "to": "${deviceToken}",
        "data": params
      };

      await sendRequest(
        POST(""),
        options: Options(
          headers: {
            "Content-Type": "application/json",
            "Authorization": "key=${setting.value.fcmKey}",
          },
        ),
        body: data,
      );
    } catch (e) {
      logger.e("NotificationV2Repository - sendNotification - error: $e");
    }
  }
}
