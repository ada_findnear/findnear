import 'dart:convert';
import 'dart:io';

import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/helpers/http.dart';
import 'package:findnear/src/models/conversation.dart';
import 'package:findnear/src/models/entities/communication/entities/short_user_entity.dart';
import 'package:findnear/src/models/entities/communication/entities/unread_message_count_entity.dart';
import 'package:findnear/src/models/entities/communication/params/check_conversation_params.dart';
import 'package:findnear/src/models/entities/communication/params/conversation_params.dart';
import 'package:findnear/src/models/entities/communication/params/create_conversation_params.dart';
import 'package:findnear/src/models/entities/communication/params/create_pin_code_params.dart';
import 'package:findnear/src/models/entities/communication/params/delete_conversation_params.dart';
import 'package:findnear/src/models/entities/communication/params/pinned_conversation_params.dart';
import 'package:findnear/src/models/entities/communication/params/unread_message_count_params.dart';
import 'package:findnear/src/models/entities/communication/params/update_conversation_params.dart';
import 'package:findnear/src/models/entities/communication/response/conversation_entity.dart';
import 'package:findnear/src/models/entities/communication/response/delete_conversation_response.dart';
import 'package:findnear/src/models/entities/communication/response/delete_many_conversation_response.dart';
import 'package:findnear/src/models/entities/communication/response/pinned_conversation_response.dart';
import 'package:findnear/src/models/entities/state_usercall.dart';
import 'package:findnear/src/models/media.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/bottom_sheet/change_bg_logic.dart';
import 'package:findnear/src/repository/base/base_network.dart';
import 'package:findnear/src/services/websocket_service.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';

import 'base/base_repository.dart';
import 'package:http/http.dart' as http;

final client = new AuthenticatedHttpClient();

abstract class CommunicationRepository {
  Future<List<ConversationEntity>> getConversations();

  Future<ConversationEntity> createConversations(Conversation conversation);

  Future<DeleteConversationResponse> leaveConversation(String uuid);

  Future<DeleteConversationResponse> deleteConversation(String uuid);

  Future<DeleteManyConversationResponse> deleteManyConversation(
      List<String> uuids);

  Future<PinnedConversationResponse> pinnedConversation(String uuid);

  Future<PinnedConversationResponse> unPinnedConversation(String uuid);

  Future<PinnedConversationResponse> createPinCode(
      {String uuid, String pinCode});

  Future<List<ConversationEntity>> searchConversations(String text);

  Future<PinnedConversationResponse> updateConversation(
      Conversation conversation);

  void updateUnreadMessage({String conversationId, int numberRead});

  Future<Media> uploadFile(String pathMedia, String collection);

  Future<PinnedConversationResponse> updateBackgroundImage(
      {Conversation conversation, String path, bool enableBothTwoSide = true});

  Future<PinnedConversationResponse> updateBackgroundImageWithoutUpload(
      {Conversation conversation, String url, bool enableBothTwoSide = true});

  Future<PinnedConversationResponse> resetBackgroundImage(
      {Conversation conversation, String path, bool enableBothTwoSide = true});

  Future<List<User>> searchFriend({String keyword = '', String ignoreId = '', int currentOffset = 0, int limit = 20});

  Future<List<ConversationEntity>> findConversationByReceiver(
      {String receiver, String sender});

  Future<List<UnreadMessageCountEntity>> getUnreadMessageCount({String sender});

  Future<PinnedConversationResponse> addMoreMember(
      {Conversation conversation, List<ShortUserEntity> members});
}

class CommunicationApi extends BaseApi implements CommunicationRepository {

  CommunicationApi({bool isSocketApi = true}) {
    if(isSocketApi){
      final baseUrl = GlobalConfiguration().getValue("base_socket_url");
      dio = BaseNetwork.getBaseDio(baseUrl: "$baseUrl");
    }
  }

  @override
  Future<List<ConversationEntity>> getConversations() async {
    User currentUser = Get.find<LocalDataManager>().currentUser;
    final response = await sendRequest(
      GET('/api/conversation/getConversationByCreateUser'),
      authenticate: true,
      queryParameters: {
        'user': currentUser.code,
        'isDisplay': true,
      },
    );
    return (response as List)
        .map((e) => ConversationEntity.fromJson(e))
        .toList();
  }

  @override
  Future<ConversationEntity> createConversations(
      Conversation conversation) async {
    CreateConversationParam param =
        CreateConversationParam.fromConversation(conversation);
    final response = await sendRequest(
      POST('/api/conversation'),
      authenticate: true,
      body: param,
    );
    if(response != null)
      return ConversationEntity.fromJson(response);
    return null;
  }

  @override
  Future<DeleteConversationResponse> deleteConversation(String uuid) async {
    final response = await sendRequest(
      DELETE('/api/conversation/$uuid'),
      authenticate: true,
    );
    return DeleteConversationResponse.fromJson(response);
  }

  @override
  Future<DeleteConversationResponse> leaveConversation(String uuid) async {
    final response = await sendRequest(
      DELETE('/api/conversation/leftGroup/$uuid'),
      authenticate: true,
    );
    return DeleteConversationResponse.fromJson(response);
  }

  @override
  Future<PinnedConversationResponse> pinnedConversation(String uuid) async {
    PinnedConversationParam param =
        PinnedConversationParam(uuid: uuid, pinTop: true);
    final response = await sendRequest(
      PUT('/api/conversation/setPinTop'),
      authenticate: true,
      body: param,
    );
    return PinnedConversationResponse.fromJson(response);
  }

  @override
  Future<PinnedConversationResponse> unPinnedConversation(String uuid) async {
    PinnedConversationParam param =
        PinnedConversationParam(uuid: uuid, pinTop: false);
    final response = await sendRequest(
      PUT('/api/conversation/setPinTop'),
      authenticate: true,
      body: param,
    );
    return PinnedConversationResponse.fromJson(response);
  }

  @override
  Future<PinnedConversationResponse> createPinCode(
      {String uuid, String pinCode}) async {
    CreatePinCodeParam param = CreatePinCodeParam(uuid: uuid, pinCode: pinCode);
    final response = await sendRequest(
      PUT('/api/conversation/setPin'),
      authenticate: true,
      body: param,
    );
    return PinnedConversationResponse.fromJson(response);
  }

  @override
  Future<List<ConversationEntity>> searchConversations(String text) async {
    User currentUser = Get.find<LocalDataManager>().currentUser;
    final response = await sendRequest(
      GET('/api/conversation/search'),
      authenticate: true,
      queryParameters: {
        'user': currentUser.code,
        'search': text,
      },
    );
    return (response as List)
        .map((e) => ConversationEntity.fromJson(e))
        .toList();
  }

  @override
  Future<PinnedConversationResponse> updateConversation(
      Conversation conversation) async {
    ConversationParam param = ConversationParam.fromConversation(conversation);
    final response = await sendRequest(
      PUT('/api/conversation'),
      authenticate: true,
      body: param,
    );
    return PinnedConversationResponse.fromJson(response);
  }

  @override
  Future<DeleteManyConversationResponse> deleteManyConversation(
      List<String> uuids) async {
    DeleteConversationParam param = DeleteConversationParam(ids: uuids);
    final response = await sendRequest(
      POST('/api/conversation/deleteMany'),
      authenticate: true,
      body: param,
    );
    return DeleteManyConversationResponse.fromJson(response);
  }

  @override
  void updateUnreadMessage({String conversationId, int numberRead}) {
    var data = new Map<String, dynamic>();
    data["conversationId"] = conversationId;
    data["numberRead"] = numberRead;

    var param = new Map<String, dynamic>();
    param["action"] = "CHAT";
    param["type"] = "UPDATE_UNREAD_MESSAGE";
    param["data"] = data;

    Get.find<WebsocketService>().subscribe("message", param);
  }

  @override
  Future<Media> uploadFile(String pathMedia, String collection) async {
    final String url =
        '${GlobalConfiguration().getValue('api_base_url')}upload';
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.fields.addAll({"collection_name": "messages"});
    request.headers.addAll({HttpHeaders.contentTypeHeader: 'application/json'});
    request.headers.addAll({HttpHeaders.authorizationHeader: 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}'});
    if (pathMedia != null) {
      request.files.add(http.MultipartFile(
          'file',
          File(pathMedia).readAsBytes().asStream(),
          File(pathMedia).lengthSync(),
          filename: pathMedia.split("/").last));
    }
    http.Response response =
        await http.Response.fromStream(await client.send(request));
    return Media.fromJSON(json.decode(response.body)['data']['media'][0]);
  }

  @override
  Future<PinnedConversationResponse> updateBackgroundImage(
      {Conversation conversation, String path, bool enableBothTwoSide = true}) async {
    try {
      /// upload image
      var image = await uploadFile(path, "group_avatar");
      if (image?.url?.isNotEmpty ?? false) {
        if(enableBothTwoSide){
          conversation.config.groupBackground = image.url;
        } else {
          conversation.config.privateBackground = image.url;
        }
        ConversationParam param =
            ConversationParam.fromConversation(conversation);
        final response = await sendRequest(
          PUT('/api/conversation'),
          authenticate: true,
          body: param,
        );
        return PinnedConversationResponse.fromJson(response);
      }
      return null;
    } catch (e) {
      return null;
    }
  }

  @override
  Future<PinnedConversationResponse> updateBackgroundImageWithoutUpload(
      {Conversation conversation, String url, bool enableBothTwoSide = true}) async {
    try {
      /// upload image
      if(enableBothTwoSide){
        conversation.config.groupBackground = url;
      } else {
        conversation.config.privateBackground = url;
      }
      ConversationParam param =
          ConversationParam.fromConversation(conversation);
      final response = await sendRequest(
        PUT('/api/conversation'),
        authenticate: true,
        body: param,
      );
      return PinnedConversationResponse.fromJson(response);
    } catch (e) {
      return null;
    }
  }

  @override
  Future<PinnedConversationResponse> resetBackgroundImage(
      {Conversation conversation, String path, bool enableBothTwoSide = true}) async {
    try {
      /// reset background
      if(enableBothTwoSide){
        conversation.config.groupBackground = ChangeBgController.DEFAULT_BACKGROUND;
      } else {
        conversation.config.privateBackground = ChangeBgController.DEFAULT_BACKGROUND;
      }
      ConversationParam param =
      ConversationParam.fromConversation(conversation);
      final response = await sendRequest(
        PUT('/api/conversation'),
        authenticate: true,
        body: param,
      );
      return PinnedConversationResponse.fromJson(response);
    } catch (e) {
      return null;
    }
  }
  Future<List<User>> searchFriend({String keyword = '', String ignoreId = '', int currentOffset = 0, int limit = 20}) async {
    final response = await sendRequest(
      GET('/users/list-friends'),
      authenticate: true,
      queryParameters: {
        'keyword': keyword,
        'ignore_id': '$ignoreId',
        'limit': '$limit',
        'offset': '${currentOffset}'
      },
    );
    return (response['data'] as List)
        .map((e) => User.fromJSON(e))
        .toList();
  }

  @override
  Future<List<ConversationEntity>> findConversationByReceiver({String receiver, String sender}) async{
    CheckConversationParam param = CheckConversationParam(receiver: receiver, sender: sender);
    final response = await sendRequest(
      POST('/api/conversation/findConversationByReceiver'),
      authenticate: true,
      body: param,
    );
    return (response as List).isEmpty ? [] : (response as List)
        .map((e) => ConversationEntity.fromJson(e))
        .toList();
  }

  @override
  Future<List<UnreadMessageCountEntity>> getUnreadMessageCount({String sender}) async{
    UnreadMessageCountParam param = UnreadMessageCountParam(createBy: sender);
    final response = await sendRequest(
      POST('/api/conversation/totalMessageUnRead'),
      authenticate: true,
      body: param,
    );
    return (response as List).isEmpty ? [] : (response as List)
        .map((e) => UnreadMessageCountEntity.fromJson(e))
        .toList();
  }

  @override
  Future<PinnedConversationResponse> addMoreMember({Conversation conversation, List<ShortUserEntity> members}) async{
    try{
      UpdateConversationParam param =
      UpdateConversationParam.fromConversation(conversation: conversation, members: members);
      final response = await sendRequest(
        PUT('/api/conversation/addUserToGroup'),
        authenticate: true,
        body: param,
      );
      if(response != null)
        return PinnedConversationResponse.fromJson(response);
    } catch (e){
      Fluttertoast.showToast(
          msg: "Có lỗi xảy ra",
          toastLength: Toast.LENGTH_SHORT,
          backgroundColor: AppColors.black,
          textColor: AppColors.white,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1);
    }


    return null;
  }

}
