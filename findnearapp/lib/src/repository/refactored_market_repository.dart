import 'dart:convert';

import 'package:findnear/src/helpers/custom_trace.dart';
import 'package:findnear/src/helpers/helper.dart';
import 'package:findnear/src/models/address.dart';
import 'package:findnear/src/models/entities/place_entity.dart';
import 'package:findnear/src/models/filter.dart';
import 'package:findnear/src/models/market.dart';
import 'package:findnear/src/models/response/base_response.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/repository/base/base_repository.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import '../settings/global_variable.dart' as GlobalVar;
import 'package:http/http.dart' as http;

abstract class RefactoredMarketRepository {
  Future<List<Market>> getNearMarkets(Address myLocation, Address areaLocation,
      {bool onlySelling = false, double radius = 0});

  Future<Market> updateMarkets(Market market);

  Future<List<Market>> getDatingListNearby(LatLngBounds latLngBounds);

  Future<User> updateIsOpenForDating(bool open);

  Future<List<Market>> getNearMarketsV2(LatLngBounds latLngBounds);
}

class RefactoredMarketApi extends BaseApi
    implements RefactoredMarketRepository {
  @override
  Future<List<Market>> getNearMarkets(Address myLocation, Address areaLocation,
      {bool onlySelling = false, double radius = 0}) async {
    Filter filter = Get
        .find<LocalDataManager>()
        .filter;

    Position position;
    try {
      position = GlobalVar.current_location =
      await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
    } on Exception catch (_) {
      position = GlobalVar.current_location;
    }
    final isValidPositionAndArea = (position.longitude != null &&
        position.latitude != null &&
        !areaLocation.isUnknown());
    final response = await sendRequest(
      GET('markets'),
      authenticate: true,
      queryParameters: {
        'limit': '20',
        if (isValidPositionAndArea) 'myLon': position.longitude.toString(),
        if (isValidPositionAndArea) 'myLat': position.latitude.toString(),
        if (isValidPositionAndArea)
          'areaLon': areaLocation.longitude.toString(),
        if (isValidPositionAndArea) 'areaLat': areaLocation.latitude.toString(),
        if (onlySelling) 'only_selling': 'true',
      }
        ..addAll(filter.toQuery()),
    );
    return BaseResponse<List<Market>>.fromJson(
      response,
          (data) =>
      data is List
          ? data.map((e) => Market.fromJSON(e)).toList(growable: false)
          : <Market>[],
    ).data;
  }

  @override
  Future<List<Market>> getNearMarketsV2(LatLngBounds latLngBounds) async {
    Map<String, dynamic> _queryParams = {};
    Position position = GlobalVar.current_location;
    Filter filter = Get
        .find<LocalDataManager>()
        .filter;

    if (position.longitude != null && position.latitude != null) {
      _queryParams['myLon'] = position.longitude.toString();
      _queryParams['myLat'] = position.latitude.toString();
      _queryParams['areaLon'] = position.longitude.toString();
      _queryParams['areaLat'] = position.latitude.toString();
    }

    if (latLngBounds != null) {
      LatLng northeast = latLngBounds.northeast;
      LatLng southwest = latLngBounds.southwest;

      double swLat = southwest.latitude;
      double swLon = southwest.longitude;
      double neLat = northeast.latitude;
      double neLon = northeast.longitude;

      _queryParams['swLat'] = swLat.toString();
      _queryParams['swLon'] = swLon.toString();
      _queryParams['neLat'] = neLat.toString();
      _queryParams['neLon'] = neLon.toString();
    }

    _queryParams['limit'] = '20';

    final response = await sendRequest(
      GET('v2/markets'),
      authenticate: true,
      queryParameters: _queryParams..addAll(filter.toQuery()),
    );

    return BaseResponse<List<Market>>.fromJson(
      response,
          (data) =>
      data is List
          ? data.map((e) => Market.fromJSON(e)).toList(growable: false)
          : <Market>[],
    ).data;
  }

  Future<Market> updateMarkets(Market market) async {
    final response = await sendRequest(
      PUT('markets/${market.id}'),
      authenticate: true,
      body: json.encode(market.toMap()),
    );
    return Market.fromJSON(response);
  }

  @override
  Future<List<Market>> getDatingListNearby(LatLngBounds latLngBounds) async {
    Map<String, dynamic> _queryParams = {};
    Position position = GlobalVar.current_location;
    Filter filter = Get
        .find<LocalDataManager>()
        .filter;

    if (position.longitude != null && position.latitude != null) {
      _queryParams['myLon'] = position.longitude.toString();
      _queryParams['myLat'] = position.latitude.toString();
      _queryParams['areaLon'] = position.longitude.toString();
      _queryParams['areaLat'] = position.latitude.toString();
    }

    if (latLngBounds != null) {
      LatLng northeast = latLngBounds.northeast;
      LatLng southwest = latLngBounds.southwest;

      double swLat = southwest.latitude;
      double swLon = southwest.longitude;
      double neLat = northeast.latitude;
      double neLon = northeast.longitude;

      _queryParams['swLat'] = swLat.toString();
      _queryParams['swLon'] = swLon.toString();
      _queryParams['neLat'] = neLat.toString();
      _queryParams['neLon'] = neLon.toString();
    }

    _queryParams['limit'] = '20';
    _queryParams['is_open_for_dating'] = 1;

    final response = await sendRequest(
    GET('v2/markets'),
    authenticate: true,
    queryParameters: _queryParams..addAll(filter.toQuery()),
    );

    return BaseResponse<List<Market>>.fromJson(
    response,
    (data) => data is List
    ? data.map((e) => Market.fromJSON(e)).toList(growable: false)
        : <Market>[],
    ).data;

  }

  Future<User> updateIsOpenForDating(bool open) async {
    final response = await sendRequest(
      POST('user/dating-settings'),
      authenticate: true,
      body: jsonEncode({"status": open ? 1 : 0}),
    );
    return User.fromJSON(response['data']);
  }
}
