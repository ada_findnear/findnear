import 'dart:convert';
import 'dart:io';

import 'package:findnear/src/helpers/http.dart';
import 'package:findnear/src/models/rating.dart';
import 'package:findnear/src/models/response/rating_order_response.dart';
import 'package:findnear/src/models/response/rating_response.dart';
import 'package:findnear/src/repository/base/base_repository.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;
import 'package:get/get.dart' as getX;

final client = new AuthenticatedHttpClient();

abstract class RatingResposity {
  Future<RatingOrderResponse> rating(
      {List<String> imagesPath = null,
      String comment,
      double rating,
      int orderId});
}

class RatingAPI extends BaseApi implements RatingResposity {
  @override
  Future<RatingOrderResponse> rating(
      {List<String> imagesPath = null,
      String comment,
      double rating,
      int orderId}) async {
    // TODO: implement rating
    String url =
        '${GlobalConfiguration().getValue('api_base_url')}v2/vnp_orders/rating';

    /// Nếu có token thì mới truyền lên cùng
    if (currentUser.value.apiToken.isNotEmpty)
      url =
          '${GlobalConfiguration().getValue('api_base_url')}v2/vnp_orders/rating';
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.fields.addAll({
      'order_id': '$orderId',
      'rating': ' $rating',
      'comment': comment,
    });
    request.headers.addAll({
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Bearer ${getX.Get.find<LocalDataManager>().currentUser?.apiToken}'
    });
    logger.log("URL $url");
    logger.log("REQUEST ${request.fields}");
    if (imagesPath != null && imagesPath.isNotEmpty) {
      imagesPath.forEach((element) {
        if (!element.startsWith('http'))
          request.files.add(http.MultipartFile(
              "files[]",
              File(element).readAsBytes().asStream(),
              File(element).lengthSync(),
              filename: element.split("/").last));
      });
    }
    final client = new http.Client();
    http.Response response =
        await http.Response.fromStream(await client.send(request));
    logger.log("hearder: ${request.headers}");
    logger.log("RESPONSE: ${response.body}");
    if (response.statusCode != 200) throw 'Error';
    return RatingOrderResponse.fromJson(jsonDecode(response.body));
  }
}
