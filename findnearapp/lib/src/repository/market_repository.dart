import 'dart:convert';
import 'dart:io';

import 'package:findnear/src/models/entities/place_entity.dart';
import 'package:findnear/src/models/search_user_entity.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';

import '../models/user.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:geolocator/geolocator.dart';
import '../helpers/custom_trace.dart';
import '../helpers/helper.dart';
import '../models/address.dart';
import '../models/filter.dart';
import '../models/market.dart';
import '../repository/user_repository.dart';
import '../repository/user_repository.dart' as userRepo;
import '../helpers/http.dart';
import '../settings/global_variable.dart' as GlobalVar;

final client = new AuthenticatedHttpClient();

Future<Stream<Market>> getSearchMarkets({String keyword}) async {
  Uri uri = Helper.getUri('api/markets');
  Map<String, dynamic> _queryParams = {};
  Position position = GlobalVar.current_location;
  if (position.longitude != null && position.latitude != null) {
    _queryParams['myLon'] = position.longitude.toString();
    _queryParams['myLat'] = position.latitude.toString();
    _queryParams['areaLon'] = position.longitude.toString();
    _queryParams['areaLat'] = position.latitude.toString();
  }
  _queryParams['limit'] = '20';
  _queryParams['keyword'] = keyword ?? "";
  uri = uri.replace(queryParameters: _queryParams);
  try {
    var request = http.Request('get', uri);
    // TODO refactor lại sau
    request.headers["Authorization"]= 'Bearer ${Get.find<LocalDataManager>().currentUser?.apiToken}';

    final streamedRest = await client.send(request);
    return streamedRest.stream.transform(utf8.decoder).transform(json.decoder).map((data) => Helper.getData(data)).expand((data) => (data as List)).map((data) {
      return Market.fromJSON(data);
    });
  } catch (e) {
    print(CustomTrace(StackTrace.current, message: uri.toString()).toString());
    return new Stream.value(new Market.fromJSON({}));
  }
}

Future<Market> updateMarkets(Market market) async {
  User _user = Get.find<LocalDataManager>().currentUser;
  if (_user.apiToken == null) {
    return new Market();
  }
  final String url = '${GlobalConfiguration().getValue('api_base_url')}v2/markets/updateMarket/${market.id}';
  final response = await client.post(
      Uri.parse(url),
    headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}' // TODO refactor lại sau
    },
    body: json.encode(market.toMap()),
  );

  return Market.fromJSON(json.decode(response.body)['data']);
}

Future<Stream<Market>> getSearchLocation({PlaceEntity placeEntity}) async {
  Uri uri = Helper.getUri('api/markets');
  Map<String, dynamic> _queryParams = {};
  Position position = GlobalVar.current_location;

  if (position.longitude != null && position.latitude != null) {
    _queryParams['myLon'] = position.longitude.toString();
    _queryParams['myLat'] = position.latitude.toString();
    _queryParams['areaLon'] = position.longitude.toString();
    _queryParams['areaLat'] = position.latitude.toString();
  }
  if(placeEntity != null) {
    double swLat = placeEntity.geometry.viewport.southwest.lat;
    double swLon = placeEntity.geometry.viewport.southwest.lng;
    double neLat = placeEntity.geometry.viewport.northeast.lat;
    double neLon = placeEntity.geometry.viewport.northeast.lng;
    _queryParams['swLat'] = swLat.toString();
    _queryParams['swLon'] = swLon.toString();
    _queryParams['neLat'] = neLat.toString();
    _queryParams['neLon'] = neLon.toString();
  }

  _queryParams['limit'] = '20';
  uri = uri.replace(queryParameters: _queryParams);
  try {
    var request = http.Request('get', uri);
    // TODO refactor lại sau
    request.headers.addAll({'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser?.apiToken}'});

    final streamedRest = await client.send(request);
    return streamedRest.stream.transform(utf8.decoder).transform(json.decoder).map((data) => Helper.getData(data)).expand((data) => (data as List)).map((data) {
      return Market.fromJSON(data);
    });
  } catch (e) {
    print(CustomTrace(StackTrace.current, message: uri.toString()).toString());
    return new Stream.value(new Market.fromJSON({}));
  }
}

Future<Stream<SearchUserEntity>> getSearchFriends({String keyword}) async {
  Uri uri = Helper.getUri('api/v2/users');
  Map<String, dynamic> _queryParams = {};
  Position position = GlobalVar.current_location;
  if (position.longitude != null && position.latitude != null) {
    _queryParams['myLon'] = position.longitude.toString();
    _queryParams['myLat'] = position.latitude.toString();
  }
  _queryParams['limit'] = '20';
  _queryParams['search'] = keyword ?? "";
  _queryParams['searchJoin'] = "or";
  // _queryParams['is_friend'] = "";
  uri = uri.replace(queryParameters: _queryParams);
  try {
    var request = http.Request('get', uri);
    // TODO refactor lại sau
    request.headers.addAll({'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser?.apiToken}'});

    final streamedRest = await client.send(request);
    return streamedRest.stream.transform(utf8.decoder).transform(json.decoder).map((data) => Helper.getData(data)).expand((data) => (data as List)).map((data) {
      return SearchUserEntity.fromJSON(data);
    });
  } catch (e) {
    print(CustomTrace(StackTrace.current, message: uri.toString()).toString());
    return new Stream.value(new SearchUserEntity.fromJSON({}));
  }
}

Future<Stream<SearchUserEntity>> getUserFromCode({String code}) async {
  Uri uri = Helper.getUri('api/v2/users');
  Map<String, dynamic> _queryParams = {};
  _queryParams['limit'] = '20';
  _queryParams['search'] = code ?? "";
  uri = uri.replace(queryParameters: _queryParams);
  try {
    final streamedRest = await client.send(http.Request('get', uri));
    return streamedRest.stream.transform(utf8.decoder).transform(json.decoder).map((data) => Helper.getData(data)).expand((data) => (data as List)).map((data) {
      return SearchUserEntity.fromJSON(data);
    });
  } catch (e) {
    print(CustomTrace(StackTrace.current, message: uri.toString()).toString());
    return new Stream.value(new SearchUserEntity.fromJSON({}));
  }
}
