import 'package:findnear/src/api/group_api.dart';
import 'package:findnear/src/models/base/data_reponse.dart';
import 'package:findnear/src/models/entities/group/group_entity.dart';
import 'package:findnear/src/models/entities/group/group_topic_entity.dart';
import 'package:findnear/src/models/input/GetPostsOfGroupInput.dart';
import 'package:findnear/src/models/input/UpdateGroupCoverInput.dart';
import 'package:findnear/src/models/input/UpdatePostGroupStatusInput.dart';
import 'package:findnear/src/models/input/group_input.dart';
import 'package:findnear/src/models/input/group_member_input.dart';
import 'package:findnear/src/models/input/invite_user_to_group_input.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/models/response/base_response.dart';
import 'package:findnear/src/models/user.dart';

import '../helpers/http.dart';

final client = new AuthenticatedHttpClient();

abstract class FindNearGroupRepository {
  Future<BaseResponse<GroupEntity>> createGroup(GroupInput input);
  Future<BaseResponse<GroupEntity>> updateGroup(GroupInput input);
  Future<BaseResponse<GroupEntity>> updateGroupCover(
      UpdateGroupCoverInput input);

  Future<ArrayDataResponse<GroupTopicEntity>> getGroupTopic();
  Future inviteUserToGroup(InviteUserToGroupInput input);
  Future<ArrayDataResponse<GroupEntity>> getGroupsJoined();
  Future<ArrayDataResponse<Post>> getGroupPostsJoined({
    offset = 0,
    limit = 10,
  });

  Future<ArrayDataResponse<GroupEntity>> getMyGroups({
    offset = 0,
    limit = 10,
  });
  Future<ArrayDataResponse<Post>> getMyGroupPosts({
    offset = 0,
    limit = 10,
  });
  Future<ArrayDataResponse<Post>> getPostsOfGroup(
    GetPostsOfGroupInput input, {
    offset = 0,
    limit = 10,
  });
  Future<void> updatePostGroupStatus(UpdatePostGroupStatusInput input);

  Future<ArrayDataResponse<GroupEntity>> getDiscoverGroup({
    offset = 0,
    limit = 10,
  });

  Future<BaseResponse<GroupEntity>> getGroupDetail(int id);
  Future<ArrayDataResponse<User>> getGroupMembers(
    GroupMemberInput input, {
    offset = 0,
    limit = 10,
  });

  Future<ArrayDataResponse<GroupEntity>> getSuggestGroups({
    offset = 0,
    limit = 10,
  });

  Future<BaseResponse<GroupEntity>> outGroup(int groupId);
}

class FindNearGroupRepositoryImpl extends FindNearGroupRepository {
  GroupApi _api;

  FindNearGroupRepositoryImpl(this._api);

  Future<BaseResponse<GroupEntity>> createGroup(GroupInput input) async {
    return _api.createGroup(input);
  }

  @override
  Future<ArrayDataResponse<GroupTopicEntity>> getGroupTopic() {
    return _api.getGroupTopic();
  }

  @override
  Future inviteUserToGroup(InviteUserToGroupInput input) {
    return _api.inviteUserToGroup(input);
  }

  @override
  Future<ArrayDataResponse<GroupEntity>> getGroupsJoined() {
    return _api.getGroupsJoined();
  }

  @override
  Future<ArrayDataResponse<GroupEntity>> getMyGroups({offset = 0, limit = 10}) {
    return _api.getMyGroups(offset: offset, limit: limit);
  }

  @override
  Future<ArrayDataResponse<Post>> getGroupPostsJoined(
      {offset = 0, limit = 10}) {
    return _api.getGroupPostsJoined(offset: offset, limit: limit);
  }

  @override
  Future<ArrayDataResponse<Post>> getMyGroupPosts({offset = 0, limit = 10}) {
    return _api.getMyGroupPosts(offset: offset, limit: limit);
  }

  @override
  Future<ArrayDataResponse<GroupEntity>> getDiscoverGroup(
      {offset = 0, limit = 10}) {
    return _api.getDiscoverGroup(offset: offset, limit: limit);
  }

  @override
  Future<BaseResponse<GroupEntity>> updateGroupCover(
      UpdateGroupCoverInput input) {
    return _api.updateGroupCover(input);
  }

  @override
  Future<BaseResponse<GroupEntity>> getGroupDetail(int id) async {
    return _api.getGroupDetail(id);
  }

  @override
  Future<ArrayDataResponse<User>> getGroupMembers(
    GroupMemberInput input, {
    offset = 0,
    limit = 10,
  }) async {
    return _api.getGroupMembers(input, offset: offset, limit: limit);
  }

  @override
  Future<ArrayDataResponse<GroupEntity>> getSuggestGroups({
    offset = 0,
    limit = 10,
  }) async {
    return _api.getSuggestGroups(offset: offset, limit: limit);
  }

  @override
  Future<BaseResponse<GroupEntity>> updateGroup(GroupInput input) async {
    return _api.updateGroup(input);
  }

  @override
  Future<ArrayDataResponse<Post>> getPostsOfGroup(
    GetPostsOfGroupInput input, {
    offset = 0,
    limit = 10,
  }) {
    return _api.getPostsOfGroup(input, offset: offset, limit: limit);
  }

  @override
  Future<void> updatePostGroupStatus(UpdatePostGroupStatusInput input) async {
    return _api.updatePostGroupStatus(input);
  }

  @override
  Future<BaseResponse<GroupEntity>> outGroup(int groupId) async {
    return _api.outGroup(groupId);
  }
}
