import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:just_audio/just_audio.dart';

import '../extensions/duration_ext.dart';
import '../models/base/data_reponse.dart';
import '../models/conversation.dart';
import '../models/entities/communication/entities/short_user_entity.dart';
import '../models/entities/message_chat/entities/message_background.dart';
import '../models/entities/message_chat/entities/message_call.dart';
import '../models/entities/message_chat/entities/message_chat.dart';
import '../models/entities/message_chat/entities/message_chat_config.dart';
import '../models/entities/message_chat/entities/message_chat_config_data.dart';
import '../models/entities/message_chat/entities/message_file.dart';
import '../models/entities/message_chat/entities/message_gif.dart';
import '../models/entities/message_chat/entities/message_image.dart';
import '../models/entities/message_chat/entities/message_initialization.dart';
import '../models/entities/message_chat/entities/message_invite_member.dart';
import '../models/entities/message_chat/entities/message_leave_conversation.dart';
import '../models/entities/message_chat/entities/message_location.dart';
import '../models/entities/message_chat/entities/message_name_card.dart';
import '../models/entities/message_chat/entities/message_poll.dart';
import '../models/entities/message_chat/entities/message_reply.dart';
import '../models/entities/message_chat/entities/message_sticker.dart';
import '../models/entities/message_chat/entities/message_text_style.dart';
import '../models/entities/message_chat/entities/message_voice.dart';
import '../models/entities/message_chat/enums/message_call_type.dart';
import '../models/entities/message_chat/enums/message_chat_type.dart';
import '../models/entities/message_chat/enums/message_poll_type.dart';
import '../models/entities/message_chat/enums/message_text_style_type.dart';
import '../models/user.dart';
import '../services/websocket_service.dart';
import '../utils/local_data_manager.dart';
import '../utils/logger.dart';
import '../utils/utils.dart';
import 'base/base_network.dart';
import 'base/base_repository.dart';
import 'file_repository.dart';
import 'notification_v2_repository.dart';
import 'user_repository.dart';
import 'user_v2_repository.dart';

abstract class MessageRepository {
  Future<ArrayDataResponse<MessageChat>> getMessages(
    String originId,
    int pageIndex,
    int pageSize,
  );

  Future<ArrayDataResponse<MessageChat>> getImageMessage(
    String originId,
    int pageIndex,
    int pageSize,
  );

  Future<ArrayDataResponse<MessageChat>> getLinkMessage(
    String originId,
    int pageIndex,
    int pageSize,
  );

  Future<ArrayDataResponse<MessageChat>> getFileMessage(
    String originId,
    int pageIndex,
    int pageSize,
  );

  Future<void> deleteMessage(
    String uuid,
    String userCode,
  );

  void sendText(
    String message,
    Conversation conversation,
  );

  void sendImage(
    String imagePath,
    Conversation conversation,
  );

  void sendVideo(
    String videoPath,
    Conversation conversation,
  );

  void sendSticker(
    String stickerId,
    Conversation conversation,
  );

  void sendGif(
    MessageGif messageGif,
    Conversation conversation,
  );

  void sendLocation(
    String locationUrl,
    Conversation conversation,
  );

  void sendVoice(
    String voicePath,
    Conversation conversation,
  );

  void replyMessage(
    String message,
    MessageChat messageChat,
    Conversation conversation,
  );

  void forwardMessage(
    MessageChat messageChat,
    Conversation conversation,
  );

  void sendNameCard(
    MessageNameCard messageNameCard,
    Conversation conversation,
  );

  void changeBackground(
    Conversation conversation,
    String path,
    bool enableBothTwoSide,
  );

  void sendFile(
    Conversation conversation,
    String path,
  );

  void sendCallMessage(
    MessageCall messageCall,
    Conversation conversation,
  );

  void sendInitialization(
    Conversation conversation,
  );

  void sendInviteMember(
    Conversation conversation,
    List<ShortUserEntity> newMember,
  );

  void sendLeaveConversation(
    Conversation conversation,
    ShortUserEntity leaveMember,
  );

  void sendPoll(
    MessagePollType type,
    MessagePoll messagePoll,
    Conversation conversation,
  );

  Future<bool> sendMakeCallNotification(
    String userReceiverCall,
    String messageId,
    String typeCall,
  );
}

class MessageApi extends BaseApi implements MessageRepository {
  MessageApi() {
    final baseUrl = GlobalConfiguration().getValue('base_socket_url');
    dio = BaseNetwork.getBaseDio(baseUrl: "$baseUrl/api/");
  }

  @override
  Future<ArrayDataResponse<MessageChat>> getMessages(
    String originId,
    int pageIndex,
    int pageSize,
  ) async {
    print("api/messages - originId: $originId - pageIndex: $pageIndex");
    if (currentUser.value == null || originId == null)
      return ArrayDataResponse<MessageChat>();
    try {
      final response = await sendRequest(
        GET('messages'),
        queryParameters: {
          'originId': originId,
          'pageIndex': pageIndex,
          'pageSize': pageSize,
        },
        authenticate: true,
      );
      return ArrayDataResponse.fromJson(
        response,
        (item) => MessageChat.fromJson(item),
      );
    } catch (e) {
      logger.e("MessageRepository - getMessages - error: $e");
      return ArrayDataResponse<MessageChat>();
    }
  }

  Future<void> deleteMessage(
    String uuid,
    String userCode,
  ) async {
    if (currentUser.value == null) return;
    try {
      await sendRequest(
        DELETE('messages'),
        queryParameters: {
          'id': uuid,
          'userName': userCode,
        },
        authenticate: true,
      );
    } catch (e) {
      logger.e("MessageRepository - deleteMessage - error: $e");
    }
  }

  @override
  void sendText(
    String message,
    Conversation conversation,
  ) {
    if (currentUser.value == null || message == null || message.isBlank) return;

    MessageChatConfig config = null;
    final messageTextStyle = Get.find<LocalDataManager>().getMessageTextStyle();
    if (messageTextStyle != MessageTextStyleType.unknown) {
      config = MessageChatConfig(
        data: MessageChatConfigData(
          messageTextStyle: MessageTextStyle(style: messageTextStyle),
        ),
      );
    }

    final Map<String, dynamic> data = _getMessageData(
      message,
      MessageChatType.text,
      config,
      conversation,
    );

    Get.find<WebsocketService>().subscribe("message", data);
    _sendNotification(message, conversation);
  }

  @override
  void sendImage(
    String imagePath,
    Conversation conversation,
  ) async {
    if (currentUser.value == null) return;

    final fileRepository = Get.find<FileRepository>();
    final media = await fileRepository.uploadFile(
      filePath: imagePath,
      collectionName: FileCollectionName.mediaMessage,
    );
    if (media == null) return;

    File image = new File(imagePath);
    var decodedImage = await decodeImageFromList(image.readAsBytesSync());
    final messageImage = MessageImage(
      media: media,
      width: decodedImage.width ?? 0,
      height: decodedImage.height ?? 0,
    );

    final config = MessageChatConfig(
      data: MessageChatConfigData(
        image: messageImage,
      ),
    );

    final Map<String, dynamic> data = _getMessageData(
      "[Ảnh]",
      MessageChatType.image,
      config,
      conversation,
    );

    Get.find<WebsocketService>().subscribe("message", data);
    _sendNotification("Gửi ảnh cho bạn", conversation);
  }

  @override
  void sendVideo(
    String videoPath,
    Conversation conversation,
  ) async {
    if (currentUser.value == null) return;

    final fileRepository = Get.find<FileRepository>();
    final media = await fileRepository.uploadFile(
      filePath: videoPath,
      collectionName: FileCollectionName.mediaMessage,
    );
    if (media == null) return;

    final config = MessageChatConfig(
      data: MessageChatConfigData(
        video: media,
      ),
    );

    final Map<String, dynamic> data = _getMessageData(
      "[Video]",
      MessageChatType.video,
      config,
      conversation,
    );

    Get.find<WebsocketService>().subscribe("message", data);
    _sendNotification("Gửi video cho bạn", conversation);
  }

  @override
  void sendVoice(
    String voicePath,
    Conversation conversation,
  ) async {
    if (currentUser.value == null) return;

    final fileRepository = Get.find<FileRepository>();
    final media = await fileRepository.uploadFile(
      filePath: voicePath,
      collectionName: FileCollectionName.mediaMessage,
    );
    if (media == null) return;
    final player = AudioPlayer();
    final duration = await player.setUrl(media.url);

    final config = MessageChatConfig(
      data: MessageChatConfigData(
        voice: MessageVoice(
          url: media.url,
          duration: duration,
        ),
      ),
    );

    final Map<String, dynamic> data = _getMessageData(
      "[Tin nhắn thoại]",
      MessageChatType.voice,
      config,
      conversation,
    );

    Get.find<WebsocketService>().subscribe("message", data);
    _sendNotification("Gửi tin nhắn thoại cho bạn", conversation);
  }

  @override
  void sendSticker(
    String stickerId,
    Conversation conversation,
  ) {
    if (currentUser.value == null) return;

    final config = MessageChatConfig(
      data: MessageChatConfigData(
        sticker: MessageSticker(stickerId: stickerId),
      ),
    );

    final Map<String, dynamic> data = _getMessageData(
      "[Sticker]",
      MessageChatType.sticker,
      config,
      conversation,
    );

    Get.find<WebsocketService>().subscribe("message", data);
    _sendNotification("Gửi sticker cho bạn", conversation);
  }

  @override
  void sendGif(
    MessageGif messageGif,
    Conversation conversation,
  ) {
    if (currentUser.value == null) return;

    final config = MessageChatConfig(
      data: MessageChatConfigData(
        gif: messageGif,
      ),
    );

    final Map<String, dynamic> data = _getMessageData(
      "[GIF]",
      MessageChatType.gif,
      config,
      conversation,
    );

    Get.find<WebsocketService>().subscribe("message", data);
    _sendNotification("Gửi ảnh GIF cho bạn", conversation);
  }

  @override
  void sendLocation(
    String locationUrl,
    Conversation conversation,
  ) async {
    if (currentUser.value == null) return;

    final currentPosition = await Utils.determinePosition();

    final config = MessageChatConfig(
      data: MessageChatConfigData(
        location: MessageLocation(
          url: locationUrl,
          lat: currentPosition.latitude,
          lng: currentPosition.longitude,
        ),
      ),
    );

    final Map<String, dynamic> data = _getMessageData(
      "[Vị trí]",
      MessageChatType.location,
      config,
      conversation,
    );

    Get.find<WebsocketService>().subscribe("message", data);
    _sendNotification("Gửi vị trí cho bạn", conversation);
  }

  @override
  void replyMessage(
    String message,
    MessageChat messageChat,
    Conversation conversation,
  ) {
    if (currentUser.value == null) return;

    final messageReply = MessageReply(
      owner: messageChat.user.name,
      message: messageChat,
    );

    final messageTextStyle = Get.find<LocalDataManager>().getMessageTextStyle();
    final config = MessageChatConfig(
      data: MessageChatConfigData(
        reply: messageReply,
        messageTextStyle: MessageTextStyle(style: messageTextStyle),
      ),
    );

    final Map<String, dynamic> data = _getMessageData(
      message,
      MessageChatType.reply,
      config,
      conversation,
    );

    Get.find<WebsocketService>().subscribe("message", data);
    _sendNotification(message, conversation);
  }

  @override
  void forwardMessage(
    MessageChat messageChat,
    Conversation conversation,
  ) {
    if (currentUser.value == null) return;

    final type = messageChat.type == MessageChatType.reply
        ? MessageChatType.text
        : messageChat.type;

    Map<String, dynamic> data = {
      "content": messageChat.content,
      "type": describeEnum(type),
      "sender": currentUser.value.code,
    };

    if (messageChat.config != null && type != MessageChatType.reply) {
      data["config"] = messageChat.config.toJson(messageChat.type);
    }

    final Map<String, dynamic> emitData = {
      "action": "CHAT",
      "type": "SEND_MESSAGE",
      "conversation": conversation.toJson(),
      "data": data
    };

    Get.find<WebsocketService>().subscribe("message", emitData);
    _sendNotification(messageChat.content, conversation);
  }

  @override
  void sendNameCard(
    MessageNameCard messageNameCard,
    Conversation conversation,
  ) {
    if (currentUser.value == null) return;

    final config = MessageChatConfig(
      data: MessageChatConfigData(
        nameCard: messageNameCard,
      ),
    );

    final Map<String, dynamic> data = _getMessageData(
      "[Danh thiếp]",
      MessageChatType.nameCard,
      config,
      conversation,
    );

    Get.find<WebsocketService>().subscribe("message", data);
    _sendNotification("Gửi danh thiếp cho bạn", conversation);
  }

  @override
  void changeBackground(
    Conversation conversation,
    String url,
    bool enableBothTwoSide,
  ) {
    if (currentUser.value == null) return;

    MessageChatConfig config = null;
    if (enableBothTwoSide) {
      conversation.config.groupBackground = url;
      config = MessageChatConfig(
        data: MessageChatConfigData(
          background: MessageBackground(groupBackground: url),
        ),
      );
    } else {
      conversation.config.privateBackground = url;
      config = MessageChatConfig(
        data: MessageChatConfigData(
          background: MessageBackground(privateBackground: url),
        ),
      );
    }

    final Map<String, dynamic> data = _getMessageData(
      "[Hình nền đã được thay đổi]",
      MessageChatType.changeBackground,
      config,
      conversation,
    );

    Get.find<WebsocketService>().subscribe("message", data);
  }

  @override
  void sendCallMessage(MessageCall messageCall, Conversation conversation) {
    if (currentUser.value == null) return;
    final config = MessageChatConfig(
      data: MessageChatConfigData(
        messageCall: messageCall,
      ),
    );

    String title = '';
    switch (messageCall.messageCallType) {
      case MessageCallType.audio:
        // cuộc gọi thoai
        if (messageCall.messageCallStateType == MessageCallStateType.cancel) {
          title = 'Cuộc gọi thoại';
        } else if (messageCall.messageCallStateType ==
            MessageCallStateType.accept) {
          title =
              'Cuộc gọi thoai ${messageCall.duration.toFormatString()} giây';
        }
        break;
      case MessageCallType.video:
        // cuộc gọi video
        if (messageCall.messageCallStateType == MessageCallStateType.cancel) {
          title = 'Cuộc gọi video';
        } else if (messageCall.messageCallStateType ==
            MessageCallStateType.accept) {
          title =
              'Cuộc gọi video ${messageCall.duration.toFormatString()} giây';
        }
        break;
      default:
        break;
    }

    final Map<String, dynamic> data = _getMessageData(
      title,
      MessageChatType.call,
      config,
      conversation,
    );

    Get.find<WebsocketService>().subscribe("message", data);
  }

  @override
  void sendPoll(
    MessagePollType type,
    MessagePoll messagePoll,
    Conversation conversation,
  ) {
    if (currentUser.value == null || conversation == null) return;

    final newMessagePoll = messagePoll.copyWith(type: type);
    final config = MessageChatConfig(
      data: MessageChatConfigData(
        poll: newMessagePoll,
      ),
    );

    final messageContent =
        newMessagePoll.getMessageContent(currentUser.value.name);
    final Map<String, dynamic> data = _getMessageData(
      messageContent,
      MessageChatType.poll,
      config,
      conversation,
    );

    Get.find<WebsocketService>().subscribe("message", data);
    _sendNotification(messageContent, conversation);
  }

  @override
  void sendFile(Conversation conversation, String filePath) async {
    if (currentUser.value == null) return;

    final fileRepository = Get.find<FileRepository>();
    final media = await fileRepository.uploadFile(
      filePath: filePath,
      collectionName: FileCollectionName.mediaMessage,
    );
    if (media == null) return;

    final config = MessageChatConfig(
        data: MessageChatConfigData(messageFile: MessageFile.fromMedia(media)));

    final Map<String, dynamic> data = _getMessageData(
      "[Tài liệu]",
      MessageChatType.file,
      config,
      conversation,
    );

    Get.find<WebsocketService>().subscribe("message", data);
  }

  @override
  Future<ArrayDataResponse<MessageChat>> getFileMessage(
      String originId, int pageIndex, int pageSize) async {
    if (currentUser.value == null || originId == null)
      return ArrayDataResponse<MessageChat>();
    try {
      final response = await sendRequest(
        GET('messages'),
        queryParameters: {
          'originId': originId,
          'pageIndex': pageIndex,
          'pageSize': pageSize,
          'type': "file",
        },
        authenticate: true,
      );
      return ArrayDataResponse.fromJson(
        response,
        (item) => MessageChat.fromJson(item),
      );
    } catch (e) {
      logger.e("MessageRepository - getMessages - error: $e");
      return ArrayDataResponse<MessageChat>();
    }
  }

  @override
  Future<ArrayDataResponse<MessageChat>> getImageMessage(
      String originId, int pageIndex, int pageSize) async {
    if (currentUser.value == null || originId == null)
      return ArrayDataResponse<MessageChat>();
    try {
      final response = await sendRequest(
        GET('messages'),
        queryParameters: {
          'originId': originId,
          'pageIndex': pageIndex,
          'pageSize': pageSize,
          'type': "image",
        },
        authenticate: true,
      );
      return ArrayDataResponse.fromJson(
        response,
        (item) => MessageChat.fromJson(item),
      );
    } catch (e) {
      logger.e("MessageRepository - getMessages - error: $e");
      return ArrayDataResponse<MessageChat>();
    }
  }

  @override
  Future<ArrayDataResponse<MessageChat>> getLinkMessage(
      String originId, int pageIndex, int pageSize) async {
    if (currentUser.value == null || originId == null)
      return ArrayDataResponse<MessageChat>();
    try {
      final response = await sendRequest(
        GET('messages'),
        queryParameters: {
          'originId': originId,
          'pageIndex': pageIndex,
          'pageSize': pageSize,
          'type': "link",
        },
        authenticate: true,
      );
      return ArrayDataResponse.fromJson(
        response,
        (item) => MessageChat.fromJson(item),
      );
    } catch (e) {
      logger.e("MessageRepository - getMessages - error: $e");
      return ArrayDataResponse<MessageChat>();
    }
  }

  @override
  void sendInitialization(Conversation conversation) {
    if (currentUser.value == null) return;

    MessageChatConfig config = null;
    config = MessageChatConfig(
      data: MessageChatConfigData(
        messageInit: MessageInitialization.fromConversation(conversation),
      ),
    );

    String createdUser = conversation.createBy;

    final Map<String, dynamic> data = _getMessageData(
      "[${createdUser} đã tạo nhóm]",
      MessageChatType.initialization,
      config,
      conversation,
    );

    Get.find<WebsocketService>().subscribe("message", data);
  }

  @override
  void sendInviteMember(
    Conversation conversation,
    List<ShortUserEntity> newMember,
  ) {
    User _currentUser = Get.find<LocalDataManager>().currentUser;
    if (_currentUser == null) return;

    MessageChatConfig config = null;
    config = MessageChatConfig(
      data: MessageChatConfigData(
        messageInvite: MessageInviteMember.fromConversation(
          conversation,
          newMember,
          _currentUser.code,
        ),
      ),
    );

    final Map<String, dynamic> data = _getMessageData(
      "[${_currentUser.name} đã thêm thành viên mới]",
      MessageChatType.inviteMember,
      config,
      conversation,
    );

    Get.find<WebsocketService>().subscribe("message", data);
  }

  @override
  void sendLeaveConversation(
      Conversation conversation, ShortUserEntity leaveMember) {
    if (currentUser.value == null) return;

    MessageChatConfig config = null;
    config = MessageChatConfig(
      data: MessageChatConfigData(
        messageLeave: MessageLeaveConversation.fromConversation(
            conversation, leaveMember),
      ),
    );

    final Map<String, dynamic> data = _getMessageData(
      "[${leaveMember.name} đã rời khỏi cuộc trò chuyện]",
      MessageChatType.leave,
      config,
      conversation,
    );

    Get.find<WebsocketService>().subscribe("message", data);
  }

  // ------------------ PRIVATE FUNCTIONS ------------------
  Map<String, dynamic> _getMessageData(
    String content,
    MessageChatType type,
    MessageChatConfig config,
    Conversation conversation,
  ) {
    Map<String, dynamic> data = {
      "content": content,
      "type": describeEnum(type),
      "sender": currentUser.value.code,
    };

    if (config != null) {
      data["config"] = config.toJson(type);
    }

    return {
      "action": "CHAT",
      "type": "SEND_MESSAGE",
      "conversation": conversation.toJson(),
      "data": data
    };
  }

  void _sendNotification(String message, Conversation conversation) {
    if (currentUser.value == null) return;

    final listUserCode = conversation.userIds
      ..removeWhere((element) => element == currentUser.value.code);
    if (listUserCode.isEmpty) return;

    final userV2Repository = Get.find<UserV2Repository>();
    final notificationV2Repository = Get.find<NotificationV2Repository>();

    for (final userCode in listUserCode) {
      userV2Repository.getUser(userCode).then((user) {
        if (user.data.first != null && user.data.first.deviceToken.isNotEmpty) {
          notificationV2Repository.sendNotification(
            currentUser.value.name,
            message ?? "",
            user.data.first.deviceToken,
          );
        }
      });
    }
  }

  Future<bool> sendMakeCallNotification(
    String userReceiverCall,
    String messageId,
    String typeCall,
  ) async {
    try {
      // typeCall: "VIDEO_CALL/VOICE_CALL/END_CALL"
      final body = {
          "userReceiverCall": userReceiverCall,
          "messageId": messageId,
          "userInfoMakeCall": {
            "userId": currentUser.value.code,
            "name": currentUser.value.name,
          },
          "typeCall": typeCall,
        };
        print("NamNH - body: $body");
      final response = await sendRequest(
        POST('/call/push'),
        body: body,
        authenticate: true,
      );
      
      return response["status"] as bool;
    } catch (e) {
      logger.e("MessageRepository - sendMakeCallNotification - error: $e");
      return false;
    }
  }
}
