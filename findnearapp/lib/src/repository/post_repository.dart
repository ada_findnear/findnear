import 'dart:convert';
import 'dart:io';

import 'package:findnear/src/models/create_post.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/media/media_utils.dart';
import 'package:get/get.dart';

import '../helpers/custom_trace.dart';
import '../helpers/http.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;

import '../helpers/helper.dart';
import '../models/post.dart';
import '../models/user.dart';
import '../repository/user_repository.dart' as userRepo;

final client = new AuthenticatedHttpClient();

Future<Stream<Post>> getPost({offset = '', userId, limit = ''}) async {
  User _user = Get.find<LocalDataManager>().currentUser;
  if (_user.apiToken == null) {
    return new Stream.value(null);
  }
  userId = userId == null ? '' : userId;
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}my-posts/${userId}?limit=${limit}&offset=${offset}';

  var request = http.Request('get', Uri.parse(url));
  // TODO refactor lại sau
  request.headers.addAll({'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}'});

  final streamedRest = await client.send(request);
  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) => Helper.getData(data))
      .expand((data) => (data as List))
      .map((data) {
    return Post.fromJSON(data);
  });
}

Future<Stream<Post>> getPostV2({offset = '', userId, limit = ''}) async {
  User _user = userRepo.currentUser.value;
  if (_user.apiToken == null) {
    return new Stream.value(null);
  }
  userId = userId == null ? '' : userId;
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}v2/my-posts/${userId}?limit=${limit}&offset=${offset}';

  var request = http.Request('get', Uri.parse(url));
  // TODO refactor lại sau
  request.headers.addAll({'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}'});

  final streamedRest = await client.send(request);
  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) => Helper.getData(data))
      .expand((data) => (data as List))
      .map((data) {
    return Post.fromJSON(data);
  });
}

Future<Post> addPost(Post post, List pathImages) async {
  User _user = userRepo.currentUser.value;
  if (_user.apiToken == null) {
    return new Post();
  }
  Map<String, dynamic> decodedJSON = {};
  post.userId = _user.id;
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}posts';
  var request = http.MultipartRequest('POST', Uri.parse(url));
  request.fields.addAll(post.toMap());
  // TODO refactor lại sau
  request.headers.addAll({'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}'});
  if (pathImages.length != 0) {
    for (int i = 0; i < pathImages.length; i++) {
      String element = pathImages[i];
      request.files.add(http.MultipartFile("files[]",
          File(element).readAsBytes().asStream(), File(element).lengthSync(),
          filename: element.split("/").last));
      // get thumbnail, not good, refactor later
      var thumbnail = await MediaUtils.getVideoThumbnail(element);
      if (thumbnail != null) {
        request.files.add(http.MultipartFile("thumbnail",
            File(thumbnail).readAsBytes().asStream(), File(thumbnail).lengthSync(),
            filename: thumbnail.split("/").last));
      }
    };
  }
  http.Response response =
      await http.Response.fromStream(await client.send(request));
  if (response.statusCode == 200) {
    decodedJSON = json.decode(response.body)['data'] as Map<String, dynamic>;
    return Post.fromJSON(decodedJSON);
  } else {
    print(CustomTrace(StackTrace.current, message: response.body).toString());
    return Post.fromJSON({});
  }
}

Future<bool> reportPost(String postId, String reason) async {
  User _user = userRepo.currentUser.value;
  if (_user.apiToken == null) {
    return false;
  }
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}posts/${postId}/report';
  var request = http.MultipartRequest('POST', Uri.parse(url));
  // TODO refactor lại sau
  request.headers.addAll({'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}'});
  request.fields.addAll({"reason": reason});
  http.Response response =
      await http.Response.fromStream(await client.send(request));
  if (response.statusCode >= 200 && response.statusCode <= 299) {
    return true;
  } else {
    return false;
  }
}

Future<Post> updatePost(Post post) async {
  User _user = userRepo.currentUser.value;
  if (_user.apiToken == null) {
    return new Post();
  }
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}posts/${post.id}';
  final response = await client.put(
    Uri.parse(url),
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}' // TODO refactor lại sau
    },
    body: json.encode(post.toMap()),
  );
  return Post.fromJSON(json.decode(response.body)['data']);
}

Future<bool> removePost(Post post) async {
  User _user = userRepo.currentUser.value;
  if (_user.apiToken == null) {
    return false;
  }
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}posts/${post.id}';
  final response = await client.delete(
    Uri.parse(url),
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}' // TODO refactor lại sau
    },
  );
  return Helper.getBoolData(json.decode(response.body));
}

Future<bool> toggleLikePost(String postID) async {
  User _user = userRepo.currentUser.value;
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}posts/${postID}/toggle-like';
  final response = await client.post(
    Uri.parse(url),
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}' // TODO refactor lại sau
    },
  );
  if (json.decode(response.body)['success']) {
    return json.decode(response.body)['data']['isLike'];
  }
}

Future<Stream<Post>> getPostById(postId) async {
  User _user = userRepo.currentUser.value;
  if (_user.apiToken == null) {
    return new Stream.value(null);
  }
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}posts/${postId}';

  var request = http.Request('get', Uri.parse(url));
  // TODO refactor lại sau
  request.headers.addAll({'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}'});

  final streamedRest = await client.send(request);
  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) => Helper.getData(data))
      .map((data) => Post.fromJSON(data));
}
