import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:findnear/src/commons/app_colors.dart';
import 'package:findnear/src/models/entities/meta_entity.dart';
import 'package:findnear/src/models/param/comment_param.dart';
import 'package:findnear/src/models/response/base_response.dart';
import 'package:findnear/src/models/response/create_comment_channel_reponse.dart';
import 'package:findnear/src/models/response/fetch_comment_response.dart';
import 'package:findnear/src/models/response/leave_live_response.dart';
import 'package:findnear/src/models/response/comment_response.dart';
import 'package:findnear/src/models/response/user_live_info.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

import '../models/response/create_live_response.dart';
import '../models/response/join_live_response.dart';
import '../models/response/list_stream_response.dart';
import '../models/response/live_token_reponse.dart';
import 'base/base_network.dart';
import 'user_repository.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;

import 'base/base_repository.dart';

abstract class LiveStreamRepository {
  Future<LiveTokenResponse> getTokenBroadcaster();

  Future<LiveTokenResponse> getTokenAudience(int user_id);

  Future<CreateLiveResponse> createLiveStream(String des);
  
  Future<CreateLiveResponse> endLiveStream(String des);

  Future<JoinLiveResponse> joinLiveStream(int create_id);

  Future<LeaveLiveResponse> leaveLiveStream(int create_id);

  Future<ListStreamResponse> getListLiveStream();

  Future<UserLiveResponse> getUserLiveInfo(int id);

  Future<User> getUserByCode(String code);

  Future<LiveTokenResponse> getRTMToken();

  /// RTM
  Future<CreateCommentChannelResponse> createCommentChannel(String broadcastId);

  Future<bool> stopCommentChannel(String broadcastId);

  Future<bool> joinCommentChannel(String broadcastId);

  Future<bool> leaveCommentChannel(String broadcastId);

  Future<CommentResponse> sendComment({String broadcastId, String text, MetaEntity metaEntity});

  Future<CommentResponse> reactComment({String broadcastId, String commentId});

  Future<FetchCommentResponse> fetchComments(String broadcastId);
}

class LiveStreamApi extends BaseApi implements LiveStreamRepository {
  LiveStreamApi({bool isSocketApi = false}) {
    if(!isSocketApi){
      final baseUrl = GlobalConfiguration().getValue("api_base_url");
      dio = BaseNetwork.getBaseDio(baseUrl: "$baseUrl");
    } else {
      final baseUrl = GlobalConfiguration().getValue("base_socket_url");
      dio = BaseNetwork.getBaseDio(baseUrl: "$baseUrl");
    }

  }

  @override
  Future<LiveTokenResponse> getTokenAudience(int user_id) async {
    var param = new Map<String, dynamic>();
    param["user_id"] = user_id;

    final String url =
        '${GlobalConfiguration().getValue('api_base_url')}v2/livestreams/get-token-audience';
    log(url);
    final client = new http.Client();
    final response = await client.post(
      Uri.parse(url),
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser?.apiToken}'
      },
        body:json.encode(param)
    );

    if (response.statusCode == 200) {
      LiveTokenResponse liveTokenResponse =
      LiveTokenResponse.fromJson(jsonDecode(response.body));
      return liveTokenResponse;
    } else {
      return LiveTokenResponse(success: false);
    }
  }

  @override
  Future<LiveTokenResponse> getTokenBroadcaster() async {
    final String url =
        '${GlobalConfiguration().getValue('api_base_url')}v2/livestreams/get-token-broadcaster';
    log(url);
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.headers.addAll({'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser?.apiToken}'});
    http.Response response =
    await http.Response.fromStream(await client.send(request));

    if (response.statusCode == 200) {
      LiveTokenResponse liveTokenResponse =
      LiveTokenResponse.fromJson(jsonDecode(response.body));
      return liveTokenResponse;
    } else {
      return LiveTokenResponse(success: false);
    }
  }

  @override
  Future<CreateLiveResponse> createLiveStream(String des) async{
    var param = new Map<String, dynamic>();
    param["description"] = des;

    final String url =
        '${GlobalConfiguration().getValue('api_base_url')}v2/livestreams/create-livestream';
    log(url);
    final client = new http.Client();
    final response = await client.post(
        Uri.parse(url),
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser?.apiToken}'
        },
        body:json.encode(param)
    );

    if (response.statusCode == 200) {
      CreateLiveResponse createLiveResponse =
      CreateLiveResponse.fromJson(jsonDecode(response.body));
      return createLiveResponse;
    } else {
      return CreateLiveResponse(success: false);
    }
  }

  @override
  Future<CreateLiveResponse> endLiveStream(String des) async{
    var param = new Map<String, dynamic>();
    param["description"] = des;

    final String url =
        '${GlobalConfiguration().getValue('api_base_url')}v2/livestreams/end-livestream';
    log(url);
    final client = new http.Client();
    final response = await client.post(
        Uri.parse(url),
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser?.apiToken}'
        },
        body:json.encode(param)
    );

    if (response.statusCode == 200) {
      CreateLiveResponse endLiveResponse =
      CreateLiveResponse.fromJson(jsonDecode(response.body));
      return endLiveResponse;
    } else {
      return CreateLiveResponse(success: false);
    }
  }

  @override
  Future<JoinLiveResponse> joinLiveStream(int create_id) async{
    var param = new Map<String, dynamic>();
    param["user_id"] = create_id;

    final String url =
        '${GlobalConfiguration().getValue('api_base_url')}v2/livestreams/join-livestream';
    log(url);
    final client = new http.Client();
    final response = await client.post(
        Uri.parse(url),
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser?.apiToken}'
        },
        body:json.encode(param)
    );

    if (response.statusCode == 200) {
      JoinLiveResponse joinLiveResponse =
      JoinLiveResponse.fromJson(jsonDecode(response.body));
      return joinLiveResponse;
    } else {
      return JoinLiveResponse(success: false);
    }
  }

  @override
  Future<LeaveLiveResponse> leaveLiveStream(int create_id) async{
    var param = new Map<String, dynamic>();
    param["user_id"] = create_id;

    final String url =
        '${GlobalConfiguration().getValue('api_base_url')}v2/livestreams/leave-livestream';
    log(url);
    final client = new http.Client();
    final response = await client.post(
        Uri.parse(url),
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser?.apiToken}'
        },
        body:json.encode(param)
    );

    if (response.statusCode == 200) {
      LeaveLiveResponse leaveLiveResponse =
        LeaveLiveResponse.fromJson(jsonDecode(response.body));
      return leaveLiveResponse;
    } else {
      return LeaveLiveResponse(success: false);
    }
  }

  @override
  Future<ListStreamResponse> getListLiveStream() async{
    final String url =
        '${GlobalConfiguration().getValue('api_base_url')}v2/livestreams';
    log(url);
    var request = http.MultipartRequest('GET', Uri.parse(url));
    request.headers.addAll({'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser?.apiToken}'});
    http.Response response =
        await http.Response.fromStream(await client.send(request));

    if (response.statusCode == 200) {
      ListStreamResponse listStream =
      ListStreamResponse.fromJson(jsonDecode(response.body));
    return listStream;
    } else {
    return ListStreamResponse(success: false);
    }
  }

  @override
  Future<UserLiveResponse> getUserLiveInfo(int id) async{
    final String url =
        '${GlobalConfiguration().getValue('api_base_url')}v2/users/${id}';
    log(url);
    var request = http.MultipartRequest('GET', Uri.parse(url));
    http.Response response =
    await http.Response.fromStream(await client.send(request));

    if (response.statusCode == 200) {
      UserLiveResponse user =
      UserLiveResponse.fromJson(jsonDecode(response.body));
      return user;
    }else{
      return UserLiveResponse(success: false);
    }
  }

  @override
  Future<LiveTokenResponse> getRTMToken() async {
    final String url =
        '${GlobalConfiguration().getValue('api_base_url')}v2/livestreams/get-rtm-token';
    log(url);
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.headers.addAll({'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser?.apiToken}'});
    http.Response response =
    await http.Response.fromStream(await client.send(request));

    if (response.statusCode == 200) {
      try{
        LiveTokenResponse liveTokenResponse =
        LiveTokenResponse.fromJson(jsonDecode(response.body));
        return liveTokenResponse;
      } catch (e){
        return null;
      }
    } else {
      return LiveTokenResponse(success: false);
    }
  }

  @override
  Future<CreateCommentChannelResponse> createCommentChannel(String broadcastId) async{
    try {
      final response = await sendRequest(
        POST('/api/livestream/${broadcastId}/start'),
        authenticate: true,
      );
      return CreateCommentChannelResponse.fromJson(response);
    } catch (e) {
      return null;
    }
  }

  @override
  Future<bool> joinCommentChannel(String broadcastId) async{
    try {
      final response = await sendRequest(
        POST('/api/livestream/${broadcastId}/join'),
        authenticate: true,
      );
      return response;
    } catch (e) {
      return null;
    }
  }

  @override
  Future<bool> leaveCommentChannel(String broadcastId) async{
    try {
      final response = await sendRequest(
        POST('/api/livestream/${broadcastId}/leave'),
        authenticate: true,
      );
      return response;
    } catch (e) {
      return null;
    }
  }

  @override
  Future<CommentResponse> sendComment({String broadcastId, String text, MetaEntity metaEntity}) async{
    try {
      CommentParam param;
      if(metaEntity == null)
        param = CommentParam(
          commentType: "MSG",
          content: text,
        );
      else
        param = CommentParam(
          commentType: "MSG",
          content: text,
          meta: metaEntity,
        );

      final response = await sendRequest(
        POST('/api/livestream/${broadcastId}/comments'),
        authenticate: true,
        body: param.toJson(),
      );
      return CommentResponse.fromJson(response);
    } catch (e) {
      return null;
    }
  }

  @override
  Future<FetchCommentResponse> fetchComments(String broadcastId) async{
    try {
      final response = await sendRequest(
        GET('/api/livestream/${broadcastId}/comments'),
        authenticate: true,
      );
      return FetchCommentResponse.fromJson(response);
    } catch (e) {
      return null;
    }
  }

  @override
  Future<bool> stopCommentChannel(String broadcastId) async{
    try {
      final response = await sendRequest(
        POST('/api/livestream/${broadcastId}/end'),
        authenticate: true,
      );
      return response;
    } catch (e) {
      return null;
    }
  }

  @override
  Future<CommentResponse> reactComment({String broadcastId, String commentId}) async{
    try {
      final response = await sendRequest(
        POST('/api/livestream/${broadcastId}/comments/${commentId}/like'),
        authenticate: true,
      );
      return CommentResponse.fromJson(response);
    } catch (e) {
      return null;
    }
  }

  @override
  Future<User> getUserByCode(String code) async{
    try {
      BaseResponse<User> response = await sendRequest(
        POST('/api/v2/users'),
        authenticate: true,
        queryParameters: {
          'search': code,
        },
      );
      return response.data;
    } catch (e) {
      return null;
    }
  }

}
