import 'package:dio/dio.dart';

import '../models/media.dart';
import '../utils/logger.dart';
import 'base/base_network.dart';
import 'base/base_repository.dart';

enum FileCollectionName {
  mediaMessage,
  groupAvatar,
}

extension FileCollectionNameExtension on FileCollectionName {
  String get name {
    return [
      "media_message",
      "group_avatar",
    ][index];
  }
}

abstract class FileRepository {
  Future<Media> uploadFile({
    String filePath,
    FileCollectionName collectionName,
  });
}

class FileApi extends BaseApi implements FileRepository {
  FileApi() {
    final baseUrl = "https://app.findnear.vn/api/";
    dio = BaseNetwork.getBaseDio(baseUrl: baseUrl);
  }

  @override
  Future<Media> uploadFile({
    String filePath,
    FileCollectionName collectionName,
  }) async {
    final fileName = filePath.split('/').last;
    final formData = FormData.fromMap(
      {
        "file": await MultipartFile.fromFile(
          filePath,
          filename: fileName,
        ),
        "collection_name": collectionName.name,
      },
    );
    try {
      final response = await sendRequest(
        POST('upload'),
        body: formData,
        authenticate: true,
      );
      logger.e("FileRepository - uploadFile - response: $response");
      return Media.fromJSON(
        Map<String, dynamic>.from(response)["data"]["media"][0],
      );
    } catch (e) {
      logger.e("FileRepository - uploadFile - error: $e");
      return null;
    }
  }
}
