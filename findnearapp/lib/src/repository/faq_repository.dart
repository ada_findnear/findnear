import 'dart:convert';

import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;

import '../helpers/helper.dart';
import '../models/faq_category.dart';
import '../models/user.dart';
import '../repository/user_repository.dart';
import '../helpers/http.dart';

final client = new AuthenticatedHttpClient();

Future<Stream<FaqCategory>> getFaqCategories() async {
  User _user = currentUser.value;
  final String url = '${GlobalConfiguration().getValue('api_base_url')}faq_categories?with=faqs';

  var request = http.Request('get', Uri.parse(url));
  // TODO refactor lại sau
  request.headers.addAll({'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}'});
  final streamedRest = await client.send(request);

  return streamedRest.stream.transform(utf8.decoder).transform(json.decoder).map((data) => Helper.getData(data)).expand((data) => (data as List)).map((data) {
    return FaqCategory.fromJSON(data);
  });
}
