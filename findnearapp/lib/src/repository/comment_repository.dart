import 'dart:convert';
import 'dart:io';

import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;

import '../helpers/helper.dart';
import '../models/comment.dart';
import '../models/user.dart';
import '../repository/user_repository.dart' as userRepo;
import '../helpers/http.dart';

final client = new AuthenticatedHttpClient();

class CommentRepository {
  Future<Stream<Comment>> getListComment(postId, offset) async {
    User _user = userRepo.currentUser.value;
    if (_user.apiToken == null) {
      return new Stream.value(null);
    }
    final String url =
        '${GlobalConfiguration().getValue('api_base_url')}comments?post_id=${postId}&limit=10&offset=${offset}';

    // TODO refactor lại sau
    var request = http.Request('get', Uri.parse(url));
    request.headers.addAll({'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}'});
    
    final streamedRest = await client.send(request);
    return streamedRest.stream.transform(utf8.decoder).transform(json.decoder).map((data) => Helper.getData(data)).expand((data) => (data as List)).map((data) {
      return Comment.fromJSON(data);
    });
  }

  Future<Comment> addComment({Comment comment, pathImage = null}) async {
    User _user = userRepo.currentUser.value;
    if (_user.apiToken == null) {
      return new Comment();
    }
    Map<String, dynamic> decodedJSON = {};
    final String url = '${GlobalConfiguration().getValue('api_base_url')}comments';
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.fields.addAll(comment.toMap(withoutMedia: true));
    request.headers.addAll({HttpHeaders.contentTypeHeader: 'application/json'});
    // TODO refactor lại sau
    request.headers.addAll({'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}'});
    if (pathImage != null) {
      request.files.add(
          http.MultipartFile(
              "files[]",
              File(pathImage).readAsBytes().asStream(),
              File(pathImage).lengthSync(),
              filename: pathImage.split("/").last
          )
      );
    }
    http.Response response = await http.Response.fromStream(await client.send(request));
    try {
      decodedJSON = json.decode(response.body)['data'] as Map<String, dynamic>;
    } on FormatException catch (e) {
      print(e);
    }
    return Comment.fromJSON(decodedJSON);
  }

  Future<bool> deleteComment({String commentId}) async {
    User _user = userRepo.currentUser.value;
    if (_user.apiToken == null) {
      return false;
    }
    final String url = '${GlobalConfiguration().getValue('api_base_url')}comments/${commentId}';
    var request = http.MultipartRequest('DELETE', Uri.parse(url));
    // TODO refactor lại sau
    request.headers.addAll({'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}'});
    http.Response response = await http.Response.fromStream(await client.send(request));
    if (response.statusCode >= 200 && response.statusCode <= 299) {
      return true;
    } else {
      return false;
    }
  }
}
