import 'dart:async';
import 'dart:async';

import 'package:findnear/src/models/base/data_reponse.dart';
import 'package:findnear/src/models/game.dart';
import 'package:findnear/src/models/lotte_game_result.dart';
import 'package:findnear/src/models/response/leader_board_response.dart';
import 'package:intl/intl.dart';

import '../models/user.dart';

import 'base/base_repository.dart';

abstract class GameRepository {
  Future<User> playGame({int id, int point, List<int> data, int phase});

  Future<User> earnPoint({int gameId, int point, String description});

  Future<User> enablePushNotification(bool isEnable);

  Future<List<User>> getLeaderBoard({int gameId, int limit, bool isToday});

  Future<List<LotteGameResult>> getLotteryWinners();

  Future<List<Game>> getGameList();

  Future<List<GameHistory>> getGameHistories(String id,
      {DateTime startDate, String search, String searchFields});

  Future<User> playFindnearWheelGame({int id, int point, List<String> data, int phase});

  Future<List<GameHistory>> getFNWGameHistories(String id);
}

//class GameRepositoryMock extends GameRepository {}

class GameRepositoryApi extends BaseApi implements GameRepository {
  @override
  Future<List<User>> getLeaderBoard(
      {int gameId, int limit, bool isToday = false}) async {
    String start;
    if (isToday) {
      final now = DateTime.now();
      DateTime startDate = DateTime(now.year, now.month, now.day, 7, 0).toUtc();
      print('==== ${startDate} ${startDate.timeZoneOffset}');
      start = DateFormat('yyyy-MM-ddTHH:mm:ss+00:00').format(startDate);
    }
    final response = await sendRequest(
      GET('leader-board'),
      queryParameters: {
        if (gameId != null) 'game_id': gameId,
        'offset': 0,
        if (limit != null) 'limit': limit,
        if (isToday) 'start': start,
      },
    );
    return LeaderBoardResponse.fromJson(response).data;
  }

  @override
  Future<List<LotteGameResult>> getLotteryWinners() async {
    final response = await sendRequest(GET('game_histories'),
        queryParameters: {
          'game_id': 2,
          'search': 'won:1',
          'searchFields': 'won:=',
          'with': 'user'
        },
        authenticate: true);
    var data = response["data"];
    if (data == null) return List.empty();
    try { // update code when release, try catch to make sure it not crash
      var result = (data as List).map((e) => LotteGameResult.fromMap(e)).toList();
      return result;
    } catch (e) {
      print(e.toString());
      return List.empty();
    }
  }

  @override
  Future<List<Game>> getGameList() async {
    final now = DateTime.now();
    DateTime startDate = DateTime(now.year, now.month, now.day, 7, 0).toUtc();
    print('==== ${startDate} ${startDate.timeZoneOffset}');
    final start = DateFormat('yyyy-MM-ddTHH:mm:ss+00:00').format(startDate);
    final response = await sendRequest(
      GET('games'),
      queryParameters: {
        'start': start,
      },
      authenticate: true,
    );
    return ArrayDataResponse.fromJson(response, Game.fromJson).data;
  }

  @override
  Future<User> playGame({int id, int point, List<int> data, int phase}) async {
    final response = await sendRequest(
      POST('games/$id/play'),
      body: {
        if (phase != null) 'phase': phase,
        if (point != null) 'point': point,
        if (data != null && data.isNotEmpty) 'data': data,
      },
      authenticate: true,
    );
    return DataResponse.fromJson(response, (json) => User.fromJSON(json)).data;
  }

  @override
  Future<User> playFindnearWheelGame({int id, int point, List<String> data, int phase}) async {
    final response = await sendRequest(
      POST('games/$id/play'),
      body: {
        if (phase != null) 'phase': phase,
        if (point != null) 'point': point,
        if (data != null && data.isNotEmpty) 'data': data,
      },
      authenticate: true,
    );
    return DataResponse.fromJson(response, (json) => User.fromJSON(json)).data;
  }

  @override
  Future<User> earnPoint({int gameId, int point, String description}) async {
    final response = await sendRequest(
      POST('earn-point'),
      body: {
        'game_id': gameId,
        'point': point,
        'description': description,
      },
      authenticate: true,
    );
    return DataResponse.fromJson(response, (json) => User.fromJSON(json)).data;
  }

  @override
  Future<User> enablePushNotification(bool isEnable) async {
    final response = await sendRequest(
      POST('update-game-notification'),
      body: {'allow': isEnable},
      authenticate: true,
    );
    return DataResponse.fromJson(response, (json) => User.fromJSON(json)).data;
  }

  @override
  Future<List<GameHistory>> getGameHistories(String id,
      {DateTime startDate, String search, String searchFields}) async {
    final now = DateTime.now();

    DateTime endDate = DateTime(now.year, now.month, now.day, 23, 0).toUtc();
    startDate ??= DateTime(now.year, now.month, now.day, 7, 0).toUtc();
    final start = DateFormat('yyyy-MM-ddTHH:mm:ss+00:00').format(startDate);
    final end = DateFormat('yyyy-MM-ddTHH:mm:ss+00:00').format(endDate);
    final response = await sendRequest(
      GET('my-games'),
      queryParameters: {
        'game_id': id,
        'start': start,
        'end': end,
        if (search != null) 'search': search,
        if (searchFields != null) 'searchFields': searchFields,
      },
      authenticate: true,
    );
    return ArrayDataResponse.fromJson(
        response, (json) => GameHistory.fromJson(json)).data;
  }

  @override
  Future<List<GameHistory>> getFNWGameHistories(String id) async {
    final response = await sendRequest(
      GET('my-games'),
      queryParameters: {
        'game_id': id,
        'orderBy': 'created_at',
        'sortedBy': 'desc'
      },
      authenticate: true,
    );
    return ArrayDataResponse<GameHistory>.fromJson(
        response, (json) => GameHistory.fromJson(json)).data;
  }
}
