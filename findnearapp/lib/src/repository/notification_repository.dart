import 'dart:convert';
import 'dart:io';

import 'package:findnear/src/models/response/message_count_response.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;

import '../helpers/helper.dart';
import '../models/notification.dart';
import '../models/user.dart';
import '../repository/user_repository.dart' as userRepo;
import 'settings_repository.dart';
import '../helpers/http.dart';

final client = new AuthenticatedHttpClient();

Future<Stream<Notification>> getNotifications() async {
  User _user = Get.find<LocalDataManager>().currentUser;
  if (_user.apiToken == null) {
    return new Stream.value(null);
  }
  final String url = '${GlobalConfiguration().getValue('api_base_url')}notifications?search=notifiable_id:${_user.id}&searchFields=notifiable_id:=&orderBy=created_at&sortedBy=desc&limit=100';

  var request = http.Request('get', Uri.parse(url));
  // TODO refactor lại sau
  request.headers.addAll({'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}'});

  final streamedRest = await client.send(request);

  return streamedRest.stream.transform(utf8.decoder).transform(json.decoder).map((data) => Helper.getData(data)).expand((data) => (data as List)).map((data) {
    return Notification.fromJSON(data);
  });
}

Future<void> markAllAsReadNotifications() async {
  User _user = userRepo.currentUser.value;
  if (_user.apiToken == null) {
    return;
  }
  final String url = '${GlobalConfiguration().getValue('api_base_url')}read-all-notifications';
  final response = await client.post(
    Uri.parse(url),
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}' // TODO refactor lại sau
    },
  );
  print("[${response.statusCode}] NotificationRepository markAsReadNotifications");
  return;
}

Future<Notification> markAsReadNotifications(Notification notification) async {
  User _user = userRepo.currentUser.value;
  if (_user.apiToken == null) {
    return new Notification();
  }
  final String url = '${GlobalConfiguration().getValue('api_base_url')}notifications/${notification.id}';
  final response = await client.put(
    Uri.parse(url),
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}' // TODO refactor lại sau
    },
    body: json.encode(notification.markReadMap()),
  );
  print("[${response.statusCode}] NotificationRepository markAsReadNotifications");
  return Notification.fromJSON(json.decode(response.body)['data']);
}

Future<Notification> removeNotification(Notification cart) async {
  User _user = userRepo.currentUser.value;
  if (_user.apiToken == null) {
    return new Notification();
  }
  final String url = '${GlobalConfiguration().getValue('api_base_url')}notifications/${cart.id}';
  final response = await client.delete(
    Uri.parse(url),
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}' // TODO refactor lại sau
    },
  );
  print("[${response.statusCode}] NotificationRepository removeCart");
  return Notification.fromJSON(json.decode(response.body)['data']);
}

Future<void> sendNotification(String body, String title, User user) async {
  final data = {
    "notification": {"body": "$body", "title": "$title", "sound": "default"},
    "priority": "high",
    "data": {"click_action": "FLUTTER_NOTIFICATION_CLICK", "id": "messages", "status": "done"},
    "to": "${user.deviceToken}"
  };
  final String url = 'https://fcm.googleapis.com/fcm/send';
  final client = new http.Client();
  prettyLog.d("key=${setting.value.fcmKey}\n"
      "user.deviceToken = ${user.deviceToken} - ${user.name}"
      "data = $data\n"
      "");
  final response = await client.post(
    Uri.parse(url),
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: "key=${setting.value.fcmKey}",
    },
    body: json.encode(data),
  );
  if (response.statusCode != 200) {
    print('notification sending failed');
  }
}

Future<Stream<MessageCountResponse>> getNotificationsCount() async {
  User _user = Get.find<LocalDataManager>().currentUser;
  if (_user.apiToken == null) {
    return new Stream.value(null);
  }
  final String _apiToken = '${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}notifications/count_unseen_notify';

  var request = http.Request('get', Uri.parse(url));
  // TODO refactor lại sau
  request.headers.addAll({'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}'});

  final streamedRest = await client.send(request);

  return streamedRest.stream.transform(utf8.decoder).transform(json.decoder).map((data) {
    return MessageCountResponse.fromJSON(data);
  });

}

Future<void> clearNotificationsCount() async {
  User _user = userRepo.currentUser.value;
  if (_user.apiToken == null) {
    return;
  }
  final String url = '${GlobalConfiguration().getValue('api_base_url')}notifications/seen_notify_all';
  final response = await client.post(
    Uri.parse(url),
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}' // TODO refactor lại sau
    },
  );
  print("[${response.statusCode}] NotificationRepository clear notifications count");
  return;
}
