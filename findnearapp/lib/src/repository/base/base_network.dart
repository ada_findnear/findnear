import 'package:dio/dio.dart';
import 'app_http_interceptor.dart';
import 'package:global_configuration/global_configuration.dart';

class BaseNetwork {
  BaseNetwork._();

  static BaseNetwork _instance;
  Dio baseDio;

  static BaseNetwork init() {
    _instance ??= BaseNetwork._();
    _instance.baseDio = getBaseDio();
    return _instance;
  }

  static getBaseDio({String baseUrl}) {
    BaseOptions options;
    options = BaseOptions(
      connectTimeout: 45000,
      receiveTimeout: 45000,
      baseUrl: baseUrl ?? GlobalConfiguration().getValue('api_base_url'),
    );
    Dio dio = Dio(options);
    dio.interceptors.add(AppHttpInterceptor());
    return dio;
  }
}
