import 'package:dio/dio.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:flutter/material.dart';
import 'base_network.dart';
import 'package:get/get.dart' as GetPackage;

abstract class BaseApi {
  Dio dio = GetPackage.Get.find<BaseNetwork>().baseDio;

  Future sendRequest(
    Method method, {
    dynamic body,
    Map<String, dynamic> queryParameters = const {},
    Options options,
    authenticate = false,
  }) async {
    final finalQueryParameters = Map<String, dynamic>();
    if (authenticate) {
      if (GetPackage.Get.find<LocalDataManager>().currentUser?.apiToken != null) {
        options ??= Options();
        final apiToken =
            GetPackage.Get.find<LocalDataManager>().currentUser?.apiToken;
        logger.d("BaseApi - sendRequest - apiToken: $apiToken");
        options.headers = {
          'Authorization': 'Bearer ${apiToken}' // TODO refactor lại sau
        };
      }
    }
    if (queryParameters?.isNotEmpty ?? false) {
      finalQueryParameters.addAll(queryParameters);
    }
    logger.d("Body requsst: $body");
    Response rawResponse = await _send(
      method: method,
      body: body,
      queryParameters: finalQueryParameters,
      options: options,
    );
    return rawResponse.data;
  }

  Future sendRequestWithoutToken(
    Method method, {
    dynamic body,
    Map<String, dynamic> queryParameters = const {},
    authenticate = false,
  }) async {
    final finalQueryParameters = Map<String, dynamic>();
    if (queryParameters?.isNotEmpty ?? false) {
      finalQueryParameters.addAll(queryParameters);
    }
    Response rawResponse = await _send(
      method: method,
      body: body,
      queryParameters: finalQueryParameters,
    );
    return rawResponse.data;
  }

  Future<Response> _send({
    Method method,
    dynamic body,
    Map<String, dynamic> queryParameters,
    Options options,
  }) async {
    Future<Response> response;
    if (method is POST) {
      response = dio.post(method.path,
          data: body, queryParameters: queryParameters, options: options);
    } else if (method is GET) {
      response = dio.get(method.path,
          queryParameters: queryParameters, options: options);
    } else if (method is PUT) {
      response = dio.put(method.path,
          data: body, queryParameters: queryParameters, options: options);
    } else if (method is DELETE) {
      response = dio.delete(method.path,
          data: body, queryParameters: queryParameters, options: options);
    } else if (method is PATCH) {
      response = dio.patch(method.path,
          data: body, queryParameters: queryParameters, options: options);
    } else {
      throw 'no support method ${method.runtimeType}';
    }
    return response;
  }
}

abstract class Method {
  String path;

  Method(this.path);

  //String get path => _path;

  void setPath(String key, String value) {
    path = path.replaceAll(key, value);
  }

  Method addPath(String path) {
    this.path += path;
    return this;
  }
}

class POST extends Method {
  POST(String url) : super(url);
}

class GET extends Method {
  GET(String url) : super(url);
}

class PUT extends Method {
  PUT(String url) : super(url);
}

class PATCH extends Method {
  PATCH(String url) : super(url);
}

class DELETE extends Method {
  DELETE(String url) : super(url);
}
