import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:findnear/generated/l10n.dart';
import 'package:findnear/src/commons/app_snackbar.dart';
import 'package:findnear/src/pages/login.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:get/get.dart' hide Response;

class AppHttpInterceptor extends Interceptor {
  LocalDataManager localDataManager = Get.find();

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    // prettyLog
    //     .d('Request: ${options.method} ${options.uri}\n${json.encode(options.data ?? '')}');
    // logLargeString('Request: ${options.method} ${options.uri}\n${json.encode(options.data ?? '')}');
    super.onRequest(options, handler);
  }

  void logLargeString(String data) {
    final int CHUNK_SIZE = 4076;  // Typical max logcat payload.
    int offset = 0;
    while (offset + CHUNK_SIZE <= data.length) {
      logger.log(data.substring(offset, offset += CHUNK_SIZE));
    }
    if (offset < data.length) {
      logger.log(data.substring(offset));
    }
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    prettyLog.d(response.data,
        'Response: ${response.statusCode} ${response.requestOptions.method} /${response.requestOptions.uri}');
    super.onResponse(response, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    prettyLog.e(err.requestOptions.uri);
    prettyLog.e(err.response?.data ?? "response empty", err.error);
    /// Ẩn toàn bộ thông báo lỗi bắn về trên điện thoại, "too many request, 409,..."
    /// - Message từ hệ thống, lỗi không xác định
    //showError(err);
    super.onError(err, handler);
  }

  void showError(DioError err) {
    final data = err.response?.data;
    String message;
    try {
      if (err.response?.data != null) {
        message = data['message'];
      }
    } catch (e) {
      // parse data failed
    }
    final statusCode = err.response?.statusCode ?? 0;
    if (message == null) {
      switch (statusCode) {
        case 401:
          message = 'Vui lòng đăng nhập lại';
          break;
        case 500:
          message = S.current.an_error_occurred;
          break;
        case 429:
          message = 'Too many request';
          break;
        default:
          message = S.current.an_error_occurred;
      }
    }

    if (statusCode == 401 || localDataManager.currentUser?.apiToken == null) {
      Get.key.currentState.pushReplacementNamed(LoginWidget.ROUTE_NAME);
    }

    AppSnackbar.showError(message: message);
  }
}
