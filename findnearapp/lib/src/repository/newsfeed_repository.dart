import 'dart:math';

import 'package:dio/dio.dart';
import 'package:findnear/src/extensions/media_ext.dart';
import 'package:findnear/src/models/create_post.dart';
import 'package:findnear/src/models/media.dart' as MediaModel;
import 'package:findnear/src/models/newsfeed_comment.dart';
import 'package:findnear/src/models/post.dart';
import 'package:findnear/src/models/response/base_response.dart';
import 'package:findnear/src/models/response/newsfeed_response.dart';
import 'package:findnear/src/models/response/post_base_response.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:findnear/src/utils/media/media_utils.dart';
import 'package:flutter/material.dart';
import 'package:images_picker/images_picker.dart';

import 'base/base_repository.dart';

abstract class NewsfeedRepository {
  Future<List<Post>> getNewsfeed({offset = 0, limit = 10});

  Future<PostBaseResponse> toggleLikePost(String postID);

  Future<PostBaseResponse> hidePost(String postID);

  Future<PostBaseResponse> unhidePost(String postID);

  Future<PostBaseResponse> savePost(String postID);

  Future<PostBaseResponse> removeSavePost(String postID);

  Future<List<NewsfeedComment>> getNewsfeedPostComments(String postID);

  Future<Post> getPostDetails(String postID);

  Future<NewsfeedComment> createComment({
    @required String content,
    @required String postId,
    String parentCommentId,
    String imagePath,
  });

  Future<NewsfeedComment> toggleLikeComment({@required String commentId});

  Future<NewsfeedComment> unlikeComment({@required String commentId});

  Future<BaseResponse<bool>> deleteComment({@required String commentId});

  Future<BaseResponse<Post>> createPost(CreatePost post);

  Future<BaseResponse<Post>> updatePost(String postId, CreatePost post);

  Future<bool> addImagesToPost(String postId, List<Media> medias);

  Future<bool> removeImagesFromPost(String mediaId);

  Future<bool> removeMediaFromPost(MediaModel.Media media);

  Future<bool> sendReport({
    @required String content,
    @required String postId,
  });
}

class NewsfeedApi extends BaseApi implements NewsfeedRepository {
  Future<List<Post>> getNewsfeed({offset = 0, limit = 10}) async {
    var response;
    if (currentUser.value?.apiToken != null) {
      response = await sendRequest(
        GET('new-feed'),
        authenticate: true,
        queryParameters: {
          'limit': limit,
          'offset': offset,
        },
      );
    } else {
      response = await sendRequest(
        GET('home-feed'),
        authenticate: false,
        queryParameters: {
          'limit': limit,
          'offset': offset,
        },
      );
    }

    return NewsfeedResponse.fromJson(response).data;
  }

  Future<PostBaseResponse> toggleLikePost(String postID) async {
    final response = await sendRequest(
      POST('posts/$postID/toggle-like'),
      authenticate: true,
    );
    return PostBaseResponse.fromJson(response);
  }

  Future<PostBaseResponse> hidePost(String postID) async {
    final response = await sendRequest(
      POST('hide-post/$postID'),
      authenticate: true,
    );
    return PostBaseResponse.fromJson(response);
  }

  Future<PostBaseResponse> unhidePost(String postID) async {
    final response = await sendRequest(
      POST('unhide-post/$postID'),
      authenticate: true,
    );
    return PostBaseResponse.fromJson(response);
  }

  Future<PostBaseResponse> savePost(String postID) async {
    final response = await sendRequest(
      POST('save-post/$postID'),
      authenticate: true,
    );
    return PostBaseResponse.fromJson(response);
  }

  Future<PostBaseResponse> removeSavePost(String postID) async {
    final response = await sendRequest(
      POST('remove-save-post/$postID'),
      authenticate: true,
    );
    return PostBaseResponse.fromJson(response);
  }

  Future<List<NewsfeedComment>> getNewsfeedPostComments(String postID) async {
    final response = await sendRequest(
      GET('comments'),
      authenticate: true,
      queryParameters: {
        'post_id': postID,
      },
    );
    return BaseResponse<List<NewsfeedComment>>.fromJson(
      response,
      (data) => data is List
          ? data.map((e) => NewsfeedComment.fromMap(e)).toList(growable: false)
          : <NewsfeedComment>[],
    ).data;
  }

  Future<Post> getPostDetails(String postID) async {
    final response = await sendRequest(
      GET('posts/$postID'),
      authenticate: true,
    );
    return BaseResponse<Post>.fromJson(
      response,
      (data) => Post.fromMap(data),
    ).data;
  }

  Future<NewsfeedComment> createComment({
    @required String content,
    @required String postId,
    String parentCommentId,
    String imagePath,
  }) async {
    final formData = FormData.fromMap({
      "content": content,
      "post_id": postId,
      if (parentCommentId?.isNotEmpty ?? false) "parent_id": parentCommentId,
    });
    if (imagePath?.isNotEmpty ?? false) {
      var entry = MapEntry(
        'files[]',
        MultipartFile.fromFileSync(
          imagePath,
          filename: imagePath.split("/").last,
        ),
      );
      formData.files.add(entry);
    }

    final response =
        await sendRequest(POST('comments'), authenticate: true, body: formData);
    return BaseResponse<NewsfeedComment>.fromJson(
        response, (data) => NewsfeedComment.fromMap(data)).data;
  }

  Future<NewsfeedComment> toggleLikeComment(
      {@required String commentId}) async {
    final response = await sendRequest(
      POST('comment/$commentId/like'),
      authenticate: true,
    );
    return BaseResponse<NewsfeedComment>.fromJson(
        response, (data) => NewsfeedComment.fromMap(data)).data;
  }

  Future<NewsfeedComment> unlikeComment({@required String commentId}) async {
    final response = await sendRequest(
      POST('comment/$commentId/unlike'),
      authenticate: true,
    );
    return NewsfeedComment.fromMap(response);
  }

  Future<BaseResponse<bool>> deleteComment({@required String commentId}) async {
    final response = await sendRequest(
      DELETE('comments/$commentId'),
      authenticate: true,
    );
    return BaseResponse<bool>.fromJson(
      response,
      (data) => data is bool ? data : false,
    );
  }

  Future<BaseResponse<Post>> createPost(CreatePost post) async {
    print("TuanLA - post: ${post.toMap()}");
    var formData = FormData.fromMap(post.toMap());
    if ((post.medias?.length ?? 0) != 0) {
      for (int i = 0; i < post.medias.length; i++) {
        Media element = post.medias[i];
        var entry = MapEntry(
          'files[]',
          MultipartFile.fromFileSync(element.path,
              filename: element.path.split("/").last),
        );
        formData.files.add(entry);
        // get thumbnail, not good, refactor later
        if (element.isVideo) {
          var entry = MapEntry(
            'thumbnail',
            MultipartFile.fromFileSync(element.thumbPath,
                filename: element.thumbPath.split("/").last),
          );
          formData.files.add(entry);
        }
      }
    }
    final response = await sendRequest(
      POST('posts'),
      body: formData,
      authenticate: true,
    );
    return BaseResponse<Post>.fromJson(response, (data) => Post.fromMap(data));
  }

  Future<BaseResponse<Post>> createPostV2(CreatePost post) async {
    var formData = FormData.fromMap(post.toMap());
    if ((post.medias?.length ?? 0) != 0) {
      for (int i = 0; i < post.medias.length; i++) {
        Media element = post.medias[i];
        var entry = MapEntry(
          'files[]',
          MultipartFile.fromFileSync(element.path,
              filename: "image${DateTime.now().millisecondsSinceEpoch}"),
        );
        formData.files.add(entry);
        // get thumbnail, not good, refactor later
        if (element.isVideo) {
          var entry = MapEntry(
            'thumbnail',
            MultipartFile.fromFileSync(element.thumbPath,
                filename: "video${DateTime.now().millisecondsSinceEpoch}"),
          );
          formData.files.add(entry);
        }
      }
    }
    final response = await sendRequest(
      POST('posts'),
      body: formData,
      authenticate: true,
    );
    return BaseResponse<Post>.fromJson(response, (data) => Post.fromMap(data));
  }

  Future<BaseResponse<Post>> updatePost(String postId, CreatePost post) async {
    final response = await sendRequest(
      PUT('posts/').addPath(postId),
      queryParameters: {
        "content": post.content,
        "viewer": post.viewer,
      },
      authenticate: true,
    );
    return BaseResponse<Post>.fromJson(response, (data) => Post.fromMap(data));
  }

  Future<bool> addImagesToPost(String postId, List<Media> medias) async {
    final formData = FormData();
    if ((medias?.length ?? 0) != 0) {
      for (int i = 0; i < medias.length; i++) {
        Media media = medias[i];
        final entry = MapEntry(
          'files[]',
          MultipartFile.fromFileSync(media.path,
              filename: media.path.split("/").last),
        );
        formData.files.add(entry);
        // get thumbnail, not good, refactor later
        if (media.isVideo) {
          logger.d('upload video thumbnail: ${media.thumbPath}');
          var entry = MapEntry(
            'thumbnail',
            MultipartFile.fromFileSync(media.thumbPath,
                filename: media.thumbPath.split("/").last),
          );
          formData.files.add(entry);
        }
      }
      ;
    }
    final response = await sendRequest(
      POST('my-posts/$postId/media'),
      authenticate: true,
      body: formData,
    );
    return response['success'];
  }

  Future<bool> removeImagesFromPost(String mediaId) async {
    final response = await sendRequest(
      DELETE('media/$mediaId'),
      authenticate: true,
    );
    return response['success'];
  }

  @override
  Future<bool> removeMediaFromPost(MediaModel.Media media) async {
    if (media.isVideo && media.thumbId != null && media.thumbId.isNotEmpty) {
      await removeImagesFromPost(media.thumbId);
    }
    return removeImagesFromPost(media.id);
  }

  @override
  Future<bool> sendReport({
    @required String content,
    @required String postId,
  }) async {
    final formData = {
      "reason": content,
    };

    final response = await sendRequest(
      POST('posts/$postId/report'),
      body: formData,
      authenticate: true,
    );
    return BaseResponse<bool>.fromJson(response, (data) => data != null).success;
  }
}
