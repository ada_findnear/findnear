import 'package:findnear/src/models/entities/state_usercall.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';

import '../models/base/data_reponse.dart';
import '../models/response/user_v2_response.dart';
import '../models/user.dart';
import '../utils/logger.dart';
import 'base/base_repository.dart';
import 'package:dio/dio.dart' as dio;

abstract class UserV2Repository {
  Future<ArrayDataResponse<UserV2Response>> getUser(String userCode);

  Future<ArrayDataResponse<User>> fetchFriends({
    String keyword = '',
    String ignoreId = '',
    int limit = 20,
    int currentOffset = 0,
  });

  Future<StateUserCall> getUserStatus(String code);
}

class UserV2Api extends BaseApi implements UserV2Repository {
  @override
  Future<ArrayDataResponse<UserV2Response>> getUser(String userCode) async {
    if (userCode == null) return ArrayDataResponse<UserV2Response>();
    try {
      final response = await sendRequest(
        GET('v2/users'),
        queryParameters: {
          'search': userCode,
        },
        authenticate: true,
      );
      return ArrayDataResponse.fromJson(
        response,
        (item) => UserV2Response.fromJson(item),
      );
    } catch (e) {
      logger.e("UserV2Repository - getUser - error: $e");
      return ArrayDataResponse<UserV2Response>();
    }
  }

  @override
  Future<ArrayDataResponse<User>> fetchFriends({
    String keyword = '',
    String ignoreId = '',
    int limit = 20,
    int currentOffset = 0,
  }) async {
    try {
      final response = await sendRequest(
        GET('users/list-friends'),
        queryParameters: {
          'keyword': keyword,
          'ignore_id': ignoreId,
          'limit': limit,
          'offset': currentOffset,
        },
        authenticate: true,
      );
      return ArrayDataResponse.fromJson(
        response,
        (item) => User.fromJSON(item),
      );
    } catch (e) {
      logger.e("UserV2Repository - fetchFriends - error: $e");
      return ArrayDataResponse<User>();
    }
  }

  @override
  Future<StateUserCall> getUserStatus(String code) async {
    try{
      dio.Response response;
      var dioClient = dio.Dio(); // with default Options
      dioClient.options.baseUrl = 'https://api.findnear.vn';
      dioClient.options.connectTimeout = 30000; //30s
      dioClient.options.receiveTimeout = 60000; //60s
      dioClient.options.headers = {
        'Authorization':
        'Bearer ${Get.find<LocalDataManager>().currentUser?.apiToken}'
      };
      try {
        response = await dioClient.get('/api/user/status/${code}');
        final data = response.data as Map<String, dynamic>;
        final obj = StateUserCall.fromJson(data);
        return obj;
      } catch (e, s) {
        logger.e(e);
        return StateUserCall();
      }
    }catch(e){
      print("TuanLA - $e");
    }
  }
}
