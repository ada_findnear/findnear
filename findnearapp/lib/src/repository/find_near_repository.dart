import   'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:findnear/src/models/response/field_response.dart';
import 'package:findnear/src/models/response/my_shop_response.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;

import '../helpers/custom_trace.dart';
import '../helpers/helper.dart';
import '../helpers/http.dart';
import '../models/slide.dart';
import 'base/base_repository.dart';

abstract class FindNearRepository {
  Future<Stream<Slide>> getSlides();

  Future<FieldResponse> getFields(
      {int limit = 100, bool getFeature = true});

  Future<FieldResponse> getFieldsShop(
      {int limit = 100, bool getFeature = true});

  Future<MyShopResponse> getHasShop();

  Future<Stream<Slide>> getTodaySuggests();
}

class FindNearApi extends BaseApi implements FindNearRepository {
  final client = new AuthenticatedHttpClient();

  @override
  Future<Stream<Slide>> getSlides() async {
    Uri uri = Helper.getUri('api/v2/banners');
    Map<String, dynamic> _queryParams = {
      'offset': '0',
      'limit': '40',
    };
    uri = uri.replace(queryParameters: _queryParams);
    try {
      final streamedRest = await client.send(http.Request('get', uri));

      return streamedRest.stream
          .transform(utf8.decoder)
          .transform(json.decoder)
          .map((data) => Helper.getData(data))
          .expand((data) => (data as List))
          .map((data) => Slide.fromJSON(data));
    } catch (e) {
      print(
          CustomTrace(StackTrace.current, message: uri.toString()).toString());
      return new Stream.value(new Slide.fromJSON({}));
    }
  }

  @override
  Future<FieldResponse> getFields(
      {int limit = 100, bool getFeature = true}) async {
    try {
      String url = '${GlobalConfiguration().getValue('api_base_url')}fields?offset=0&limit=$limit&search=is_shop_field:1&searchFields=is_shop_field:=';
      log(url);
      final client = new http.Client();
      final response = await client.get(
        Uri.parse(url),
        headers: {HttpHeaders.contentTypeHeader: 'application/json'},
      );
      return FieldResponse.fromJson(jsonDecode(response.body));
    } catch (e) {}
    return null;
  }

  @override
  Future<FieldResponse> getFieldsShop(
      {int limit = 100, bool getFeature = true}) async {
    try {
      String url = '${GlobalConfiguration().getValue('api_base_url')}fields?offset=0&limit=$limit&search=is_shop_field:1&searchFields=is_shop_field:=';
      log(url);
      final client = new http.Client();
      final response = await client.get(
        Uri.parse(url),
        headers: {HttpHeaders.contentTypeHeader: 'application/json'},
      );
      return FieldResponse.fromJson(jsonDecode(response.body));
    } catch (e) {}
    return null;
  }

  @override
  Future<MyShopResponse> getHasShop() async {
    try {
      final String url =
          '${GlobalConfiguration().getValue('api_base_url')}my-shop';
      log(url);
      final client = new http.Client();
      final response = await client.get(
        Uri.parse(url),
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          'Authorization': 'Bearer ${Get.find<LocalDataManager>().currentUser?.apiToken}' // TODO refactor lại sau
        },
      );
      if (response.statusCode == 200) {
        MyShopResponse myShopResponse =
            MyShopResponse.fromJson(jsonDecode(response.body));
        await Get.find<LocalDataManager>().saveMyShop(myShopResponse.data);
        return myShopResponse;
      } else {
        await Get.find<LocalDataManager>().removeMyShop();
        return null;
      }
    } catch (e) {}
    return null;
  }

  @override
  Future<Stream<Slide>> getTodaySuggests() async{
    Uri uri = Helper.getUri('api/v2/banners');
    Map<String, dynamic> _queryParams = {
      'offset': '0',
      'limit': '10',
      'type': 'monopoly',
    };
    uri = uri.replace(queryParameters: _queryParams);
    try {
      final streamedRest = await client.send(http.Request('get', uri));

      return streamedRest.stream
          .transform(utf8.decoder)
          .transform(json.decoder)
          .map((data) => Helper.getData(data))
          .expand((data) => (data as List))
          .map((data) => Slide.fromJSON(data));
    } catch (e) {
      print(
          CustomTrace(StackTrace.current, message: uri.toString()).toString());
      return new Stream.value(new Slide.fromJSON({}));
    }
  }
}
