import 'dart:convert';
import 'dart:io';

import 'package:findnear/src/helpers/http.dart';
import 'package:findnear/src/models/category.dart';
import 'package:findnear/src/repository/base/base_repository.dart';
import 'package:findnear/src/repository/user_repository.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;
import 'package:get/get.dart' as getX;

final client = new AuthenticatedHttpClient();

abstract class CategoryResposity {
  Future<Category> createCategory(
    String name,
  );
  Future<Category> updateCategory(String name, int categoryId);
  Future<bool> deleteCategory(int categoryId);
}

class CategoryAPI extends BaseApi implements CategoryResposity {
  @override
  Future<Category> createCategory(String name) async {
    final String _apiToken =
        '${getX.Get.find<LocalDataManager>().currentUser?.apiToken}';
    final String url =
        '${GlobalConfiguration().getValue('api_base_url')}v2/vnp_categories';
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.headers.addAll({
      HttpHeaders.contentTypeHeader: 'application/json',
      'Authorization': 'Bearer $_apiToken'
    });
    logger.log(
        "✈️ REQUEST['POST'] => PATH: $url \n headers: ${request.headers}",
        printFullText: true);
    logger.log('dbdegas: ' + request.fields.toString());
    request.fields.addAll({"name": name});
    http.Response response =
        await http.Response.fromStream(await client.send(request));
    logger.log("✅ Response:" + response.body);

    return Category.fromJSON(jsonDecode(response.body)['data']);
  }

  @override
  Future<Category> updateCategory(String name, int categoryId) async {
    // final response = await sendRequest(
    //   POST('v2/shops/vnp_categories/$categoryId'),
    //   body: {'name': name},
    // );
    final String _apiToken =
        '${getX.Get.find<LocalDataManager>().currentUser?.apiToken}';
    final String url =
        '${GlobalConfiguration().getValue('api_base_url')}v2/vnp_categories/$categoryId';
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.headers.addAll({
      HttpHeaders.contentTypeHeader: 'application/json',
      'Authorization': 'Bearer $_apiToken'
    });
    logger.log(
        "✈️ REQUEST['POST'] => PATH: $url \n headers: ${request.headers}",
        printFullText: true);
    logger.log('dbdegas: ' + request.fields.toString());
    request.fields.addAll({"name": name});
    http.Response response =
        await http.Response.fromStream(await client.send(request));
    logger.log("✅ Response:" + response.body);
    return Category.fromJSON(jsonDecode(response.body)['data']);
  }

  @override
  Future<bool> deleteCategory(int categoryId) async {
    final response = await sendRequest(
      DELETE('v2/vnp_categories/${categoryId ?? ''}'),
      authenticate: true,
    );
    return response['success'];
  }
}
