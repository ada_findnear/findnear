import 'package:dio/dio.dart';
import 'package:findnear/src/models/response/base_response.dart';
import 'package:findnear/src/models/user.dart';

import 'base/base_repository.dart';

abstract class AuthenticationRepository {
  Future<BaseResponse<User>> changePassword({
    String password,
    String authCode,
    String name,
  });

  Future<BaseResponse<dynamic>> forgotPassword({
    String password,
    String passwordConfirmation,
    String authCode,
    String code,
  });
}

class AuthenticationApi extends BaseApi implements AuthenticationRepository {
  Future<BaseResponse<User>> changePassword({
    String password,
    String authCode,
    String name,
  }) async {
    final response = await sendRequest(POST('change-password'), authenticate: true, body: {
      "password": password,
      "auth_code": authCode,
      "name": name,
    });
    return BaseResponse<User>.fromJson(response, (data) => User.fromJSON(data));
  }

  @override
  Future<BaseResponse<dynamic>> forgotPassword({
    String password,
    String passwordConfirmation,
    String authCode,
    String code,
  }) async {
    final response = await sendRequest(POST('reset-password'), authenticate: true, body: {
      "password": password,
      "password_confirmation": passwordConfirmation,
      "auth_code": authCode,
      "code": code,
    });
    return BaseResponse.fromJson(response, (data) => data);
  }
}
