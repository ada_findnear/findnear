import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:findnear/src/models/entities/communication/params/create_conversation_params.dart';
import 'package:findnear/src/models/entities/communication/response/conversation_entity.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';

import '../models/chat.dart';
import '../models/conversation.dart';
import '../models/media.dart';
import '../repository/user_repository.dart' as userRepo;
import '../models/user.dart' as userModel;
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;
import '../helpers/http.dart';

final client = new AuthenticatedHttpClient();

class ChatRepository {
  factory ChatRepository.init() => ChatRepository();
  AuthenticatedHttpClient client;

  ChatRepository() {
    client = new AuthenticatedHttpClient();
  }

  // Create Conversation
  Future<void> createConversation(Conversation conversation) {
    return FirebaseFirestore.instance
        .collection("conversations")
        .doc(conversation.id)
        .set(conversation.toMap())
        .catchError((e) {
      print(e);
    });
  }

  Future<Stream<QuerySnapshot>> getUserConversations(String userId) async {
    return await FirebaseFirestore.instance
        .collection("conversations")
        .where('visible_to_users', arrayContains: userId)
        //.orderBy('time', descending: true)
        .snapshots();
  }

  Future<Stream<QuerySnapshot>> getUserConversation(
      String userId, String partnerId) async {
    List<String> users = [userId, partnerId];
    users.sort((a, b) => a.compareTo(b));
    return await FirebaseFirestore.instance
        .collection("conversations")
        .where('visible_to_users', isEqualTo: users)
        .where('id_admin', isNull: true)
        .snapshots();
  }

  Future<Stream<DocumentSnapshot>> getConversation(String id) async {
    return await FirebaseFirestore.instance
        .collection("conversations")
        .doc(id)
        .snapshots();
  }

  Future<Stream<QuerySnapshot>> getChats(Conversation conversation) async {
    return updateConversation(
            conversation.id, {'read_by_users': conversation.readByUsers})
        .then((value) async {
      return await FirebaseFirestore.instance
          .collection("conversations")
          .doc(conversation.id)
          .collection("chats")
          .orderBy('time', descending: true)
          .snapshots();
    });
  }

  Future<void> addMessage(Conversation conversation, Chat chat) {
    return FirebaseFirestore.instance
        .collection("conversations")
        .doc(conversation.id)
        .collection("chats")
        .add(chat.toMap())
        .whenComplete(() {
      updateConversation(conversation.id, conversation.toUpdatedMap());
    }).catchError((e) {
      print(e.toString());
    });
  }

  Future<void> deleteMessage(
      Conversation conversation, Chat chat, Chat previousChat) {
    return FirebaseFirestore.instance
        .collection("conversations")
        .doc(conversation.id)
        .collection("chats")
        .doc(chat.documentId)
        .delete()
        .whenComplete(() {
      conversation.lastMessage = previousChat?.text ?? "";
      updateConversation(conversation.id, conversation.toUpdatedMap());
    }).catchError((e) {
      print(e.toString());
    });
  }

  Future<void> updateConversation(
      String conversationId, Map<String, dynamic> conversation) {
    return FirebaseFirestore.instance
        .collection("conversations")
        .doc(conversationId)
        .update(conversation)
        .catchError((e) {
      print(e.toString());
    });
  }

  Future<void> removeUserConversation(
      String conversationId, Map<String, dynamic> conversation, int indexUser) {
    return FirebaseFirestore.instance
        .collection("conversations")
        .doc(conversationId)
        .update({
      'visible_to_users':
          FieldValue.arrayRemove([conversation['users'][indexUser]["id"]])
    }).catchError((e) {
      print(e.toString());
    });
  }

  Future<void> removeConversation(String conversationId) {
    return FirebaseFirestore.instance
        .collection("conversations")
        .doc(conversationId)
        .delete()
        .catchError((e) {
      print(e.toString());
    });
  }

  Future<void> turnOnNotification(Conversation conversation, String userId) {
    final mutedUsers = conversation.mutedUsers..remove(userId);
    return FirebaseFirestore.instance
        .collection("conversations")
        .doc(conversation.id)
        .update({"muted_users": mutedUsers});
  }

  Future<void> turnOffNotification(Conversation conversation, String userId) {
    final mutedUsers = conversation.mutedUsers
      ..remove(userId)
      ..add(userId);
    return FirebaseFirestore.instance
        .collection("conversations")
        .doc(conversation.id)
        .update({"muted_users": mutedUsers});
  }

  Future<Media> uploadFile(String pathMedia, String collection) async {
    final String url =
        '${GlobalConfiguration().getValue('api_base_url')}upload';
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.fields.addAll({"collection_name": "messages"});
    request.headers.addAll({HttpHeaders.contentTypeHeader: 'application/json'});
    request.headers.addAll({HttpHeaders.authorizationHeader: 'Bearer ${Get.find<LocalDataManager>().currentUser.apiToken}'});
    if (pathMedia != null) {
      request.files.add(http.MultipartFile(
          'file',
          File(pathMedia).readAsBytes().asStream(),
          File(pathMedia).lengthSync(),
          filename: pathMedia.split("/").last));
    }
    http.Response response =
        await http.Response.fromStream(await client.send(request));
    return Media.fromJSON(json.decode(response.body)['data']['media'][0]);
  }

  // update current Device Token to all user's conversations in Firebase Firestore
  Future<void> updateDeviceToken(String userId) async {
    try {
      final deviceToken = await FirebaseMessaging.instance.getToken();
      await _updateDeviceToken(userId, deviceToken);
    } catch (e) {
      logger.e('ChatRepository - updateDeviceToken fail: $e');
    }
  }

  // remove Device Token of all user's conversations in Firebase Firestore
  Future<void> removeDeviceToken(String userId) async {
    await _updateDeviceToken(userId, "");
  }

  // execute update Device Token
  Future<void> _updateDeviceToken(String userId, String deviceToken) async {
    // check userId not null or empty
    if (userId?.isNotEmpty == true) {
      String currentDeviceToken = "";
      if (deviceToken?.isNotEmpty != true) {
        // get current Device Token for remove Device Token in Firebase Firestore
        try {
          currentDeviceToken = await FirebaseMessaging.instance.getToken();
        } catch (e) {
          logger.e('ChatRepository - _updateDeviceToken - getToken fail: $e');
        }
      }

      final snapshots = await FirebaseFirestore.instance
          .collection("conversations")
          .where('visible_to_users', arrayContains: userId)
          .get();

      final batch = FirebaseFirestore.instance.batch();

      for (var document in snapshots.docs) {
        try {
          final conversation = Conversation.fromJSON(document.data());
          for (var i = 0; i < conversation.users.length; i++) {
            if (userId == conversation.users[i].id) {
              // Can change Device Token when:
              // 1. Update communication Device Token when logged in
              // 2. Remove Device Token when FireStore's Device Token equal current Device Token (avoid remove Device Token from other device)
              if (deviceToken?.isNotEmpty == true ||
                  (deviceToken?.isNotEmpty != true &&
                      conversation.users[i].deviceToken ==
                          currentDeviceToken)) {
                conversation.users[i].deviceToken = deviceToken;
              }
            }
          }
          batch.update(document.reference, conversation.toUsersMap());
        } catch (e) {
          logger.e(
              "ChatRepository - _updateDeviceToken - document.id: ${document.id} - error: $e");
        }
      }

      await batch.commit();
    } else {
      logger.e("ChatRepository - _updateDeviceToken - userId null");
    }
  }

  //////////////////

  /// Get conversations
  Future<List<ConversationEntity>> getConversations() async {
    User currentUser = Get.find<LocalDataManager>().currentUser;
    Map<String, dynamic> _queryParams = {};
    _queryParams['user'] = currentUser.code;
    _queryParams['isDisplay'] = true.toString(); // base_socket_host
    var url = Uri.https("https://api.findnear.vn", "/api/conversation/getConversationByCreateUser", _queryParams);
    final client = new http.Client();
    final response = await client.get(
      url,
      headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    );
    if (response.statusCode != 200) throw 'Error';
    return List<ConversationEntity>.from(jsonDecode(response.body).map((x) => ConversationEntity.fromJson(x)));
  }

  /// Create Conversation
  Future<ConversationEntity> createConversations(Conversation conversation) async{
    try {
      CreateConversationParam param = CreateConversationParam.fromConversation(conversation);
      final String url =
          '${GlobalConfiguration().getValue('base_socket_url')}/api/conversation';
      final client = new http.Client();
      final response = await client.post(
        Uri.parse(url),
        headers: {HttpHeaders.contentTypeHeader: 'application/json'},
        body: json.encode(param.toJson()),
      );
      if (response.statusCode != 200) throw 'Error';
      // log(url);
      // log(response.body);
      return ConversationEntity.fromJson(json.decode(response.body));
    } catch(e) {
      return null;
    }
  }

  /// Leave Conversation
  Future<bool> leaveConversation(String uuid) async{
    final String url =
        '${GlobalConfiguration().getValue('base_socket_url')}/api/conversation/leftGroup/${uuid}';
    final client = new http.Client();
    final response = await client.delete(
      Uri.parse(url),
      headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    );
    if (response.statusCode != 200) return false;
    return true;
  }

  /// Delete Conversation
  Future<bool> deleteConversation(String uuid) async{
    final String url =
        '${GlobalConfiguration().getValue('base_socket_url')}/api/conversation/${uuid}';
    final client = new http.Client();
    final response = await client.delete(
      Uri.parse(url),
      headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    );
    if (response.statusCode != 200) return false;
    return true;
  }

}
