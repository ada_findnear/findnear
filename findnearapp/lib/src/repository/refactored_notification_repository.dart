import 'package:findnear/src/models/notification.dart';
import 'package:findnear/src/models/response/base_response.dart';
import 'package:findnear/src/repository/base/base_repository.dart';

abstract class NewNotificationRepository {
  Future<List<Notification>> getMyNotifications({int offset = 0, int limit});
}

class NewNotificationApi extends BaseApi
    implements NewNotificationRepository {
  Future<List<Notification>> getMyNotifications({
    int offset = 0,
    int limit,
  }) async {
    final response = await sendRequest(
      GET('notification-my'),
      authenticate: true,
      queryParameters: {
        'limit': limit,
        if (offset != null) 'offset': offset,
      },
    );
    return BaseResponse<List<Notification>>.fromJson(
      response,
      (data) => data is List
          ? data.map((e) => Notification.fromJSON(e)).toList(growable: false)
          : <Notification>[],
    ).data;
  }
}
