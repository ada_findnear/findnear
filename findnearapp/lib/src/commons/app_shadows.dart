import 'package:flutter/material.dart';

class AppShadow {
  static final boxShadow = [
    BoxShadow(
      color: Color(0x25606060),
      blurRadius: 3,
      offset: Offset(0, 0),
    ),
  ];
}
