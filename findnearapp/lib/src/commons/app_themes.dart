import 'package:findnear/src/commons/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '/src/helpers/app_config.dart' as config;

class AppThemes {
  AppThemes._();

  ///Light theme
  static final ThemeData lightTheme = ThemeData(
    fontFamily: 'Quicksand',
    primaryColor: Colors.white,
    floatingActionButtonTheme: FloatingActionButtonThemeData(
        elevation: 0, foregroundColor: Colors.white),
    brightness: Brightness.light,
    accentColor: config.CustomColors().mainColor(1),
    dividerColor: config.CustomColors().accentColor(0.1),
    focusColor: config.CustomColors().accentColor(1),
    hintColor: config.CustomColors().secondColor(1),
    appBarTheme: AppBarTheme(
      backgroundColor: AppColors.white,
      iconTheme: IconThemeData(color: AppColors.black),
      systemOverlayStyle: SystemUiOverlayStyle.dark,
    ),
    iconTheme: IconThemeData(color: Color(0xFF818181)),
    textTheme: TextTheme(
      headline5: TextStyle(
          fontSize: 22.0,
          color: config.CustomColors().secondColor(1),
          height: 1.3),
      headline4: TextStyle(
          fontSize: 20.0,
          fontWeight: FontWeight.w700,
          color: config.CustomColors().secondColor(1),
          height: 1.3),
      headline3: TextStyle(
          fontSize: 22.0,
          fontWeight: FontWeight.w700,
          color: config.CustomColors().secondColor(1),
          height: 1.3),
      headline2: TextStyle(
          fontSize: 24.0,
          fontWeight: FontWeight.w700,
          color: config.CustomColors().mainColor(1),
          height: 1.4),
      headline1: TextStyle(
          fontSize: 26.0,
          fontWeight: FontWeight.w300,
          color: config.CustomColors().secondColor(1),
          height: 1.4),
      subtitle1: TextStyle(
          fontSize: 17.0,
          fontWeight: FontWeight.w500,
          color: config.CustomColors().secondColor(1),
          height: 1.2),
      headline6: TextStyle(
          fontSize: 17.0,
          fontWeight: FontWeight.w700,
          color: config.CustomColors().mainColor(1),
          height: 1.3),
      bodyText2: TextStyle(
          fontSize: 14.0,
          fontWeight: FontWeight.w400,
          color: config.CustomColors().secondColor(1),
          height: 1.2),
      bodyText1: TextStyle(
          fontSize: 15.0,
          fontWeight: FontWeight.w400,
          color: config.CustomColors().secondColor(1),
          height: 1.3),
      caption: TextStyle(
          fontSize: 14.0,
          fontWeight: FontWeight.w300,
          color: config.CustomColors().accentColor(1),
          height: 1.2),
    ),
  );

  ///Dark theme
  static final ThemeData darkTheme = ThemeData(
    fontFamily: 'Quicksand',
    primaryColor: Color(0xFF252525),
    brightness: Brightness.dark,
    scaffoldBackgroundColor: Color(0xFF2C2C2C),
    accentColor: config.CustomColors().mainDarkColor(1),
    dividerColor: config.CustomColors().accentColor(0.1),
    hintColor: config.CustomColors().secondDarkColor(1),
    focusColor: config.CustomColors().accentDarkColor(1),
    iconTheme: IconThemeData(color: Color(0xFFCCCCCC)),
    appBarTheme: AppBarTheme(
      backgroundColor: AppColors.gray,
      iconTheme: IconThemeData(color: AppColors.white),
      systemOverlayStyle: SystemUiOverlayStyle.light,
    ),
    textTheme: TextTheme(
      headline5: TextStyle(
          fontSize: 22.0,
          color: config.CustomColors().secondDarkColor(1),
          height: 1.3),
      headline4: TextStyle(
          fontSize: 20.0,
          fontWeight: FontWeight.w700,
          color: config.CustomColors().secondDarkColor(1),
          height: 1.3),
      headline3: TextStyle(
          fontSize: 22.0,
          fontWeight: FontWeight.w700,
          color: config.CustomColors().secondDarkColor(1),
          height: 1.3),
      headline2: TextStyle(
          fontSize: 24.0,
          fontWeight: FontWeight.w700,
          color: config.CustomColors().mainDarkColor(1),
          height: 1.4),
      headline1: TextStyle(
          fontSize: 26.0,
          fontWeight: FontWeight.w300,
          color: config.CustomColors().secondDarkColor(1),
          height: 1.4),
      subtitle1: TextStyle(
          fontSize: 17.0,
          fontWeight: FontWeight.w500,
          color: config.CustomColors().secondDarkColor(1),
          height: 1.2),
      headline6: TextStyle(
          fontSize: 17.0,
          fontWeight: FontWeight.w700,
          color: config.CustomColors().mainDarkColor(1),
          height: 1.3),
      bodyText2: TextStyle(
          fontSize: 14.0,
          fontWeight: FontWeight.w400,
          color: config.CustomColors().secondDarkColor(1),
          height: 1.2),
      bodyText1: TextStyle(
          fontSize: 15.0,
          fontWeight: FontWeight.w400,
          color: config.CustomColors().secondDarkColor(1),
          height: 1.3),
      caption: TextStyle(
          fontSize: 14.0,
          fontWeight: FontWeight.w300,
          color: config.CustomColors().secondDarkColor(0.6),
          height: 1.2),
    ),
  );
}
