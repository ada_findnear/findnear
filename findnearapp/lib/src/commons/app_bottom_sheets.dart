import 'package:findnear/src/commons/app_colors.dart';
import 'package:flutter/material.dart';

Future<T> showAppBottomSheet<T>({
  @required BuildContext context,
  @required Widget child,
}) {
  return showModalBottomSheet<T>(
    context: context,
    clipBehavior: Clip.antiAliasWithSaveLayer,
    backgroundColor: AppColors.transparent,
    builder: (context) => AppButtonSheet(child: child),
  );
}

class AppButtonSheet extends StatelessWidget {
  final Widget child;

  const AppButtonSheet({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(15),
        child: child,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.0),
            topRight: Radius.circular(20.0),
          ),
          color: const Color(0xffffffff),
          boxShadow: [
            BoxShadow(
              color: const Color(0x29000000),
              offset: Offset(0, 0),
              blurRadius: 6,
            ),
          ],
        ),
      ),
    );
  }
}
