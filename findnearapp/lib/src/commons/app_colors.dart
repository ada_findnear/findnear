import 'package:flutter/material.dart';

class AppColors{
  static const Color dividerColor = Color(0xFFEDEDED);
  static const Color transparent = Color(0x00000000);
  static const Color white = Color(0xFFFFFFFF);
  static const Color black = Color(0xFF000000);
  static const Color textBlack = Color(0xFF333333);
  static const Color red = Color(0xFFff0000);
  static const Color red500 = Color(0xFFFF8383);
  static const Color red400 = Color(0xFFFFB5B5);
  static const Color red200 = Color(0xFFFFEAEA);
  static const Color divider = Color(0xFFEDEDED);
  static const Color green = Color(0xFF00A00C);
  static const Color green2 = Color(0xFF00aa41);
  static const Color gray = Color(0xFF707070);
  static const Color gray2 = Color(0xFF818181);
  static const Color grayBg = Color(0xFFF8F8F8);
  static const Color grayBorder = Color(0xFFA8A8A8);
  static const Color grayLight = Color(0xFFD1D1D1);
  static const Color pink = Color(0xFFFFDBDB);
  static const Color yellow = Color(0xFFFFA800);
  static const Color greyBg = Color(0xffF1F1F1);
  static const Color term_yellow = Color(0xFFFFD584);
  static const Color seperateColor = Color(0xFFF0F0F0);
  static const Color newChatColor = Color(0xffFFF4F4);
  static const Color grayDark = Color(0xFFDDDDDD);

  /// Livestream
  static const Color unionBg = Color(0xFFFFA900);
}
