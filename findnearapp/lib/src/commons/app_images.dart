class AppImages {
  /*******************SVG**************************/

  /// Common
  static const icHamburger = 'assets/svg/ic_hamburger.svg';
  static const icFilter = 'assets/svg/ic_filter.svg';
  static const icSearch = 'assets/svg/ic_search.svg';
  static const icImage = 'assets/svg/ic_image.svg';
  static const icImageUpload = 'assets/svg/ic_image_photo.svg';
  static const icLive = 'assets/svg/ic_live_video_camera.svg';
  static const icAlbum = 'assets/svg/ic_album.svg';
  static const icLike = 'assets/svg/ic_heart.svg';
  static const icLikeOutlined = 'assets/svg/ic_like_outlined.svg';
  static const icLikeCircular = 'assets/svg/ic_like_circular.svg';
  static const icComment = 'assets/svg/ic_comment.svg';
  static const icSend = 'assets/svg/ic_send.svg';
  static const icStickers = 'assets/svg/ic_stickers.svg';
  static const icNotificationLightSvg = 'assets/svg/ic_notification_light.svg';
  static const icWrite = 'assets/svg/ic_write.svg';
  static const icTrash = 'assets/svg/ic_trash.svg';
  static const icBookmarkOutlined = 'assets/svg/ic_bookmark_outlined.svg';
  static const icBookmark = 'assets/svg/ic_bookmark.svg';
  static const icRemoveBookmark = 'assets/svg/ic_remove_bookmark.svg';
  static const icCloseSvg = 'assets/svg/ic_close.svg';
  static const icHide = 'assets/svg/ic_hide.svg';
  static const icTimeline = 'assets/svg/ic_timeline.svg';
  static const icClock = 'assets/svg/ic_clock.svg';
  static const icClockSelected = 'assets/svg/ic_clock_selected.svg';
  static const icCamera = 'assets/svg/ic_photo_camera.svg';
  static const icAddPerson = 'assets/svg/ic_add_person.svg';
  static const icFriendRequestAccepted =
      'assets/svg/ic_accept_friend_request.svg';
  static const icMessage = 'assets/svg/ic_message.svg';
  static const icHistoryCall = 'assets/svg/history_call.svg';
  static const icMissedCall = 'assets/svg/miss_call.svg';
  static const icInComingCall = 'assets/svg/incoming_call.svg';
  static const icOutGoingCall = 'assets/svg/outgoing_call.svg';
  static const icMyStore = 'assets/svg/my_store.svg';
  static const icAddStore = 'assets/svg/add_store.svg';
  static const icPolicy = 'assets/svg/ic_policy.svg';
  static const icShopSelected = 'assets/svg/ic_shop_selected.svg';
  static const icShopBottom = 'assets/svg/ic_shop_bottom.svg';
  static const icBrush = 'assets/svg/ic_brush.svg';
  static const icColorPalette = 'assets/svg/ic_color_palette.svg';
  static const icDelete = 'assets/svg/ic_delete.svg';
  static const icEraser = 'assets/svg/ic_eraser.svg';
  static const icUndo = 'assets/svg/ic_undo.svg';
  static const icDate = 'assets/svg/ic_date.svg';
  static const icEnableDatingModeMap =
      'assets/svg/ic_enable_dating_mode_map.svg';
  static const icEnableNormalModeMap =
      'assets/svg/ic_enable_normal_mode_map.svg';
  static const icDatingOverlayRed = 'assets/svg/ic_dating_overlay_red.svg';
  static const icDatingOverlayGreen = 'assets/svg/ic_dating_overlay_green.svg';
  static const icNdSound = 'assets/svg/ic_nd_sound.svg';
  static const icNdStore = 'assets/svg/ic_nd_store.svg';
  static const icCategory = 'assets/svg/ic_category.svg';
  static const icFn = 'assets/svg/ic_fn.svg';
  static const icPhoto = 'assets/svg/ic_photo.svg';
  static const icChatCategory = 'assets/svg/ic_chat_bubles.svg';
  static const icStar = 'assets/svg/ic_star.svg';
  static const icStarOutline = 'assets/svg/ic_star_outline.svg';
  static const icPhone = 'assets/svg/ic_phone.svg';
  static const icArrowBack = 'assets/svg/ic_back.svg';
  static const icEditImage = 'assets/svg/ic_edit_image.svg';
  static const icDownload = 'assets/svg/ic_download.svg';
  static const icPoll = 'assets/svg/ic_poll.svg';
  static const icPollMultiChoice = 'assets/svg/ic_poll_multi_choice.svg';
  static const icPollLock = 'assets/svg/ic_poll_lock.svg';
  static const icEdit = 'assets/svg/ic_edit_outline.svg';
  static const icDeleteOutline = 'assets/svg/ic_delete_outline.svg';

  /*******************PNG**************************/
  static final icMapSatellite = 'assets/img/ic_map_satellite.png';
  static final icClose = 'assets/img/ic_close.png';
  static final icGpsDark = 'assets/img/ic_gps_dark.png';
  static final icGpsLight = 'assets/img/ic_gps_light.png';
  static final icBackLight = 'assets/img/ic_back_light.png';
  static final icBackDark = 'assets/img/ic_back_dark.png';
  static final icNotificationLight = 'assets/img/ic_notification_light.png';
  static final icNotificationDark = 'assets/img/ic_notification_dark.png';
  static final icVisibleDark = 'assets/img/ic_visible_dark.png';
  static final icVisibleLight = 'assets/img/ic_visible_light.png';
  static final icMyLocation = 'assets/img/ic_my_location.png';
  static final icMapNormal = 'assets/img/ic_map_normal.png';
  static final icSetting = 'assets/img/ic_setting.png';
  static final icVoice = 'assets/img/ic_voice.png';
  static final icCallTiny = 'assets/img/ic_call_tiny.png';
  static final icMessageTiny = 'assets/img/ic_message_tiny.png';
  static final bgLanding = 'assets/img/bg_landing.png';
  static final icPostImage = 'assets/img/ic_post_image.png';
  static final icLockLarge = 'assets/img/ic_lock_large.png';
  static final icLogin = 'assets/img/ic_login.png';
  static final icMarkLogin = 'assets/img/ic_mark_login.png';
  static final icEnViewer = 'assets/img/ic_en_viewer.png';
  static final icViewer = 'assets/img/ic_viewer.png';
  static final icEndCall = 'assets/img/ic_end_call.png';
  static final icCancelCall = 'assets/img/ic_cancel_call.png';
  static const icNearMe = 'assets/img/ic_near_me.png';
  static const imgHelpHeader = 'assets/img/img_help_header.png';
  static const icCallPhone = 'assets/img/ic_call_phone.png';
  static const icZalo = 'assets/img/ic_zalo.png';
  static const icSupporterAvatar = 'assets/img/ic_avatar_support.png';
  static const icBannerIphoneEvent = 'assets/img/banner_iphone_event.png';
  static const icMail = 'assets/img/ic_mail.png';
  static const icLogo = 'assets/img/logo.png';
  static const loadingFindLove = 'assets/img/loading_find_love.png';

  ///Side menu
  static final icMenuChat = 'assets/img/ic_menu_chat.png';
  static final icMenuFriendRequest = 'assets/img/ic_menu_friend_request.png';
  static final icMenuFriends = 'assets/img/ic_menu_friends.png';
  static final icMenuLanguage = 'assets/img/ic_menu_language.png';
  static final icMenuLogout = 'assets/img/ic_menu_logout.png';
  static final icMenuNotification = 'assets/img/ic_menu_notifications.png';
  static final icMenuProfile = 'assets/img/ic_menu_profile.png';
  static final icMenuShop = 'assets/img/ic_menu_shop.png';
  static final icMenuSupport = 'assets/img/ic_menu_support.png';
  static final icMenuTheme = 'assets/img/ic_menu_theme.png';
  static final icMenuBlocking = 'assets/img/ic_menu_blocking.png';
  static final icAvatarPlaceholder = 'assets/img/ic_avatar_placeholder.png';

  ///Login
  static final icLoginBg = 'assets/img/ic_login_bg.png';
  static final icLoginUser = 'assets/img/ic_login_user.png';
  static final icLoginPass = 'assets/img/ic_login_pass.png';
  static final icMaskGroup = 'assets/img/ic_mask_group.png';
  static final icContact = 'assets/img/ic_contact.png';
  static final icLoadingFindJob = 'assets/img/ic_loading_find_job.png';
  static final icExpandMore = 'assets/img/ic_expand_more.png';
  static final icTodaySuggest = 'assets/img/ic_today_suggest.png';

  ///Nhâm dần quay xe
  static final icNdBanner = 'assets/img/ic_nd_banner.png';
  static final icNdBg = 'assets/img/ic_nd_bg.png';
  static final icNdHistory = 'assets/img/ic_nd_history.png';
  static final icNdNote = 'assets/img/ic_nd_note.png';
  static final icNdCoin = 'assets/img/ic_nd_coin.png';
  static final icNdBannerLarge = 'assets/img/ic_nd_banner_large.png';
  static final icNdClose = 'assets/img/ic_nd_close.png';
  static final icNdResultBanner = 'assets/img/ic_nd_term_banner.png';
  static final icNdTermBottom = 'assets/img/ic_nd_term_bottom.png';
  static final icNdTermLight = 'assets/img/ic_nd_term_light.png';
  static final icNdTermBanner = 'assets/img/ic_nd_result_banner.png';
  static final icNdFinearWheel = 'assets/img/ic_nd_finear_wheel.png';
  static final icNdRoll = 'assets/img/ic_nd_roll.png';
  static final icNdInfoBanner = 'assets/img/ic_nd_info_banner.png';
  static final icNdInfoBottom = 'assets/img/ic_nd_info_bottom.png';
  static final icDeletePinDigit = 'assets/img/delete_pin_digit.png';

  /// Setting chat group
  static final icAvatarGroup = 'assets/img/ic_avatar_group.png';
  static final icChooseAvatar = 'assets/img/ic_choose_avatar.png';
  static final icGroupAddFriend = 'assets/img/ic_group_add_friend.png';
  static final icGroupNotification = 'assets/img/ic_group_notification.png';
  static final icGroupProfile = 'assets/img/ic_group_profile.png';
  static final icGroupSearch = 'assets/img/ic_group_search.png';
  static final icChangeImage = 'assets/img/ic_change_image.png';
  static final icChangeName = 'assets/img/ic_change_name.png';
  static final icGroupHistory = 'assets/img/ic_group_history.png';
  static final icGroupLibrary = 'assets/img/ic_group_library.png';
  static final icHideConversation = 'assets/img/ic_hide_conversation.png';
  static final icShowConversation = 'assets/img/ic_show_conversation.png';
  static final icMoreImage = 'assets/img/ic_more_image.png';
  static final icPinMessage = 'assets/img/ic_pin_message.png';
  static final icGroupMoreImg = 'assets/img/ic_group_more_img.png';
  static final icGroupLeave = 'assets/img/ic_group_leave.png';
  static final icGroupMembers = 'assets/img/ic_group_members.png';
  static final icGroupTickGray = 'assets/img/ic_group_tick_gray.png';
  // ic_group_tick_gray

  /// Control panel
  static final icBoxCopy = 'assets/img/ic_box_copy.png';
  static final icBoxCrying = 'assets/img/ic_box_crying.png';
  static final icBoxDirect = 'assets/img/ic_box_direct.png';
  static final icBoxHeart = 'assets/img/ic_box_heart.png';
  static final icBoxLaughing = 'assets/img/ic_box_laughing.png';
  static final icBoxLike = 'assets/img/ic_box_like.png';
  static final icBoxLove = 'assets/img/ic_box_love.png';
  static final icBoxPin = 'assets/img/ic_box_pin.png';
  static final icBoxQuote = 'assets/img/ic_box_quote.png';
  static final icBoxShocked = 'assets/img/ic_box_shocked.png';
  static final icBoxTrash = 'assets/img/ic_box_trash.png';
  static final icBoxVampire = 'assets/img/ic_box_vampire.png';

  /// Pin message
  static final icGroupDropdown = 'assets/img/ic_group_dropdown.png';
  static final icGroupNormalPin = 'assets/img/ic_group_normal_pin.png';
  static final icGroupListUnpin = 'assets/img/ic_group_list_unpin.png';
  static final icGroupUnpin = 'assets/img/ic_group_unpin.png';
  static final icGroupMenu = 'assets/img/ic_menu.png';
  static final icClosePinMessage = 'assets/img/ic_close_pin_message.png';
  static final icPinnedMessage = 'assets/img/ic_pinned_message.png';
  static final icGroupMore = 'assets/img/ic_group_more.png';

  /// call video
  static const icMicEnable = 'assets/svg/ic_call_mic.svg';
  static const icMicDisable = 'assets/svg/ic_call_mic_dis.svg';
  static const icSwitchCamera = 'assets/svg/ic_switch_camera.svg';
  static const icCallCameraEnable = 'assets/svg/ic_call_camera.svg';
  static const icCallCameraDisable = 'assets/svg/ic_call_camera_dis.svg';
  static const icCallVideoEnd = 'assets/svg/ic_end_call_video.svg';
  static const icCallPip = 'assets/svg/ic_call_pip.svg';
  static const icDecline = 'assets/svg/ic_decline_call.svg';
  static const icAccept = 'assets/svg/ic_accept_call.svg';

  /// Location
  static final icLocationBorder = 'assets/img/ic_location_border.png';
  static final icMarkerLocation = 'assets/img/ic_marker_location.png';

  /// Send message
  static final icChatEmoji = 'assets/img/ic_chat_emoji.png';
  static final icChatImg = 'assets/img/ic_chat_img.png';
  static final icChatMore = 'assets/img/ic_chat_more.png';
  static final icChatSend = 'assets/img/ic_chat_send.png';
  static final icChatCard = 'assets/img/ic_chat_card.png';
  static final icChatDocument = 'assets/img/ic_chat_document.png';
  static final icChatFont = 'assets/img/ic_chat_font.png';
  static final icChatGif = 'assets/img/ic_chat_gif.png';
  static final icChatPaint = 'assets/img/ic_chat_paint.png';
  static final icChatSchedule = 'assets/img/ic_chat_schedule.png';
  static final icChatSticker = 'assets/img/ic_chat_sticker.png';
  static final icChatVote = 'assets/img/ic_chat_vote.png';
  static final icChatMap = 'assets/img/ic_chat_map.png';
  static final icChatAttachFile = 'assets/img/ic_chat_attach_file.png';
  static final icChatDownloadFile = 'assets/img/ic_chat_download_file.png';
  static final icCheckedBox = 'assets/img/ic_checkedbox.png';
  static final icUncheckedBox = 'assets/img/ic_uncheckedbox.png';
  static final icChooseBgCam = 'assets/img/ic_choose_bg_cam.png';
  static final icChooseBgTick = 'assets/img/ic_choose_bg_tick.png';

  static final icLocationGray = 'assets/svg/ic_location_search.svg';
  static final icDirectionSearch = 'assets/svg/ic_direction_search.svg';

  static final icSharePost = 'assets/svg/ic_share.svg';
  static final icImageGrey = 'assets/svg/ic_image_grey.svg';
  static final icAddMemberClose = 'assets/img/ic_add_member_close.png';
  static final icGroupAddFriend2 = 'assets/img/ic_group_add_friend2.png';
  static final icGroupCalling = 'assets/img/ic_group_calling.png';
  static final icSummaryFile = 'assets/img/ic_summary_file.png';

  ///Livestream
  static final icCjndy = 'assets/img/ic_cjndy.png';
  static final icUnion = 'assets/img/ic_union.png';
  static final icEye = 'assets/img/ic_eye.png';
  static final icBack = 'assets/img/ic_back_white.png';
  static final icGroup = 'assets/img/ic_group.png';
  static final icViewer1 = 'assets/img/ic_viewer_1.png';
  static final icViewer2 = 'assets/img/ic_viewer_2.png';
  static final icViewer3 = 'assets/img/ic_viewer_3.png';
  static final icLiving = 'assets/img/ic_live.png';
  static final icShare = 'assets/img/ic_share.png';
  static final icComments = 'assets/img/ic_comment.png';
  static final icGift = 'assets/img/ic_gift.png';
  static final icCoin = 'assets/img/ic_coin.png';
  static final icClaimGift = 'assets/img/ic_claim_gift.png';
  static final icRep = 'assets/img/ic_rep.png';
  static final icLikeComment = 'assets/img/ic_like.png';
  static final icNewsfeedAlbum = 'assets/img/ic_album.png';

  /// Group
  static final myGroupEmpty = 'assets/img/my_group_empty.png';
  static final addGroupCover = 'assets/img/add_group_cover.png';
  static final newsFeedGroupEmpty = 'assets/img/group_newsfeed_empty.png';

  static final ic_member_administration =
      'assets/img/ic_member_administration.png';
  static final ic_content_management = 'assets/img/ic_content_management.png';
  static final ic_permission_censorship =
      'assets/img/ic_permission_censorship.png';
  static final ic_out_group = 'assets/img/ic_out_group.png';
  static final ic_privacy = 'assets/img/ic_privacy.png';
  static final ic_group_appbar = 'assets/img/ic_group_appbar.png';
  static final ic_admin_medal = 'assets/img/ic_admin_medal.png';

  static final icLsSwitchCam = 'assets/img/ic_ls_switch_cam.png';
  static final icLsStart = 'assets/img/ic_ls_start.png';
  static final icLsInvite = 'assets/img/ic_ls_invite.png';
  static final icLsMember = 'assets/img/ic_ls_member.png';
  static final icLsMessage = 'assets/img/ic_ls_message.png';
  static final icLsEnd = 'assets/img/ic_ls_end.png';
  static final icLsCoin = 'assets/img/ic_ls_coin.png';
  static final icLsTitleInMarket = 'assets/img/ic_ls_title_in_market.png';
  static final icLsBroadcast = 'assets/img/ic_ls_broadcast.png';
  static final icLsFlame = 'assets/img/ic_ls_flame.png';
  static final icLsCreateLivestream = 'assets/img/ic_ls_create_livestream.png';
}
