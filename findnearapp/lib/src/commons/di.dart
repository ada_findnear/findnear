import 'package:findnear/src/api/group_api.dart';
import 'package:findnear/src/database/conversations_dao.dart';
import 'package:findnear/src/database/livestreams_dao.dart';
import 'package:findnear/src/models/user.dart';
import 'package:findnear/src/pages/game/lottery/controller.dart';
import 'package:findnear/src/pages/list_livestream/list_livestream_logic.dart';
import 'package:findnear/src/pages/list_shop/controller.dart';
import 'package:findnear/src/pages/livestream/broadcast_livestream_logic.dart';
import 'package:findnear/src/pages/livestream/config/livestream_rtm.dart';
import 'package:findnear/src/pages/livestream/config/livestreamsignling.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/bottom_sheet/change_bg_logic.dart';
import 'package:findnear/src/pages/messages/chat_group_setting/chat_group_setting_logic.dart';
import 'package:findnear/src/pages/messages/list_search_conversation/list_search_conversation_logic.dart';
import 'package:findnear/src/pages/rating/rating_order/logic.dart';
import 'package:findnear/src/pages/register_shop_page/register_shop_controller.dart';
import 'package:findnear/src/pages/pass_code/setting_pin_code/setting_pin_code_logic.dart';
import 'package:findnear/src/pages/register_shop_page/register_shop_controller.dart';
import 'package:findnear/src/pages/shop_detail/logic.dart';
import 'package:findnear/src/pages/story/data/api/story_api.dart';
import 'package:findnear/src/pages/story/data/story_repository.dart';
import 'package:findnear/src/pages/videocall/callsignaling.dart';
import 'package:findnear/src/repository/authentication_repository.dart';
import 'package:findnear/src/repository/base/base_network.dart';
import 'package:findnear/src/repository/category_reposity.dart';
import 'package:findnear/src/repository/field_repository.dart';
import 'package:findnear/src/repository/communication_repository.dart';
import 'package:findnear/src/repository/field_repository.dart';
import 'package:findnear/src/repository/file_repository.dart';
import 'package:findnear/src/repository/find_near_repository.dart';
import 'package:findnear/src/repository/findnear_group_repository.dart';
import 'package:findnear/src/repository/game_repository.dart';
import 'package:findnear/src/repository/livestream_repository.dart';
import 'package:findnear/src/repository/message_poll_repository.dart';
import 'package:findnear/src/repository/message_repository.dart';
import 'package:findnear/src/repository/newsfeed_repository.dart';
import 'package:findnear/src/repository/notification_v2_repository.dart';
import 'package:findnear/src/repository/rating_repository.dart';
import 'package:findnear/src/repository/refactored_market_repository.dart';
import 'package:findnear/src/repository/refactored_notification_repository.dart';
import 'package:findnear/src/repository/register_shop_repository.dart';
import 'package:findnear/src/repository/shop_repository.dart';
import 'package:findnear/src/repository/user_v2_repository.dart';
import 'package:findnear/src/services/audio_app_services.dart';
import 'package:findnear/src/services/cache_service.dart';
import 'package:findnear/src/services/gif_service.dart';
import 'package:findnear/src/services/ios_native_channel_service.dart';
import 'package:findnear/src/services/notification_service.dart';
import 'package:findnear/src/services/setting_service.dart';
import 'package:findnear/src/services/speech_to_text_service.dart';
import 'package:findnear/src/services/websocket_service.dart';
import 'package:findnear/src/utils/FirebaseMessageManager.dart';
import 'package:findnear/src/utils/local_data_manager.dart';
import 'package:findnear/src/utils/logger.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:get/get.dart';

Future injectDependencies() async {
  //for all dependencies init asynchronously
  await Future.wait([
    Get.putAsync(() => SettingService().init()),
    Get.putAsync(() => SpeechToTextService().init()),
    Get.putAsync(() => CacheService().init()),
    Get.putAsync(() => NotificationService().init()),
    Get.putAsync(() => Firebase.initializeApp()),
    Get.putAsync(() => LocalDataManager.init()),
    Get.putAsync(() => GifService().init()),
    Get.putAsync(() => WebsocketService().init()),
    Get.putAsync(() => ConversationsDAO().init()),
    Get.putAsync(() => CallSignaling().init()),
    Get.putAsync(() => AudioAppService().init()),
    Get.putAsync(() => LiveStreamSignaling().init()),
    Get.putAsync(() => LiveStreamRTM().init()),
    Get.putAsync(() => IosNativeChannelService().init()),
    Get.putAsync(() => LivestreamsDAO().init()),
  ]);
  await Get.putAsync(() => FirebaseMessageManager.initialize());
  Get.put(BaseNetwork.init());

  _injectRepositories();
  _injectControllers();

  /// kiểm tra thông tin currentUser.
  User currentUser = Get.find<LocalDataManager>().currentUser;

  /// check xem đã đăng nhập chưa? nếu rồi thì connect vào socket.
  if (currentUser != null) {
    logger.log("✅️ WS: User has already logged in!", printFullText: true);
    Get.find<WebsocketService>().connect(currentUser.code);
  } else
    logger.log("✅️ WS: User has not login yet!", printFullText: true);
}

void _injectRepositories() {
  Get.lazyPut<ShopsRepository>(() => ShopsApi());
  Get.lazyPut<NewsfeedRepository>(() => NewsfeedApi(), fenix: true);
  Get.lazyPut<AuthenticationRepository>(() => AuthenticationApi());
  Get.lazyPut<NewNotificationRepository>(() => NewNotificationApi(),
      fenix: true);
  Get.lazyPut<FindNearRepository>(() => FindNearApi());
  Get.lazyPut<RegisterShopRepository>(() => RegisterShopApi());
  Get.lazyPut<GameRepository>(() => GameRepositoryApi());
  Get.lazyPut<StoryApi>(() => StoryApiImpl());
  Get.lazyPut<StoryRepository>(() => StoryRepositoryImpl(Get.find()));
  Get.lazyPut<RefactoredMarketRepository>(() => RefactoredMarketApi(), fenix:true);
  Get.lazyPut<MessageRepository>(() => MessageApi(), fenix: true);
  Get.lazyPut<CommunicationRepository>(() => CommunicationApi(), fenix: true);
  Get.lazyPut<FileRepository>(() => FileApi(), fenix: true);
  Get.lazyPut<FieldRepository>(() => FieldRepositoryApi(), fenix: true);
  Get.lazyPut<UserV2Repository>(() => UserV2Api(), fenix: true);
  Get.lazyPut<NotificationV2Repository>(() => NotificationV2Api(), fenix: true);
  Get.lazyPut<LiveStreamRepository>(() => LiveStreamApi(), fenix: true);
  Get.lazyPut<FindNearGroupRepository>(
      () => FindNearGroupRepositoryImpl(GroupApiImpl()),
      fenix: true);
  Get.lazyPut<CategoryResposity>(() => CategoryAPI());
  Get.lazyPut<RatingResposity>(() => RatingAPI(), fenix: true);
  Get.lazyPut<MessagePollRepository>(() => MessagePollApi(), fenix:true);
}

void _injectControllers() {
  //todo need to refactor
  Get.lazyPut(() => ListShopController(Get.find()));
  Get.lazyPut(() => LotteryController(Get.find()));
  Get.lazyPut(() => RegisterShopSController());
  Get.lazyPut(() => SettingPinCodeLogic());
  Get.lazyPut(() => ChatGroupSettingLogic());
  Get.lazyPut(() => ListSearchConversationLogic());
  Get.lazyPut(() => ShopDetailLogic(), fenix: true);
  Get.lazyPut(() => ChangeBgController());
  Get.lazyPut(() => ListLivestreamLogic());
}
