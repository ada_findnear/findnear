<?php

return [
    'max_distance' => env('MAP_MAX_DISTANCE', '2500.0'),
];
