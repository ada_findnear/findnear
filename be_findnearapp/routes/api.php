<?php

/**
 * File name: api.php
 * Last modified: 2020.10.31 at 12:40:48
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

use Illuminate\Support\Facades\File;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['logger']], function () {
    Route::prefix('driver')->group(function () {
        Route::post('login', 'API\Driver\UserAPIController@login');
        Route::post('register', 'API\Driver\UserAPIController@register');
        Route::post('send_reset_link_email', 'API\UserAPIController@sendResetLinkEmail');
        Route::get('user', 'API\Driver\UserAPIController@user');
        Route::get('logout', 'API\Driver\UserAPIController@logout');
        Route::get('settings', 'API\Driver\UserAPIController@settings');
    });

    Route::prefix('manager')->group(function () {
        Route::post('login', 'API\Manager\UserAPIController@login');
        Route::post('register', 'API\Manager\UserAPIController@register');
        Route::post('send_reset_link_email', 'API\UserAPIController@sendResetLinkEmail');
        Route::get('user', 'API\Manager\UserAPIController@user');
        Route::get('logout', 'API\Manager\UserAPIController@logout');
        Route::get('settings', 'API\Manager\UserAPIController@settings');
    });


    Route::post('login', 'API\UserAPIController@login');
    Route::post('register', 'API\UserAPIController@register');
    Route::post('send_reset_link_email', 'API\UserAPIController@sendResetLinkEmail');
    Route::get('user', 'API\UserAPIController@user');
    Route::get('logout', 'API\UserAPIController@logout');
    Route::get('settings', 'API\UserAPIController@settings');

    Route::resource('fields', 'API\FieldAPIController');
    Route::resource('categories', 'API\CategoryAPIController');
    Route::resource('markets', 'API\MarketAPIController');

    Route::resource('faq_categories', 'API\FaqCategoryAPIController');
    Route::get('products/categories', 'API\ProductAPIController@categories');
    Route::resource('products', 'API\ProductAPIController');
    Route::resource('galleries', 'API\GalleryAPIController');
    Route::resource('product_reviews', 'API\ProductReviewAPIController');


    Route::resource('faqs', 'API\FaqAPIController');
    Route::resource('market_reviews', 'API\MarketReviewAPIController');
    Route::resource('currencies', 'API\CurrencyAPIController');
    Route::resource('slides', 'API\SlideAPIController')->except([
        'show'
    ]);

    Route::resource('option_groups', 'API\OptionGroupAPIController');

    Route::resource('options', 'API\OptionAPIController');

    Route::post('directions', 'API\GoogleMapAPIController@getDirectionsPoints');

    Route::group(['middleware' => ['auth:api']], function () {
        Route::group(['middleware' => ['role:driver']], function () {
            Route::prefix('driver')->group(function () {
                Route::resource('orders', 'API\OrderAPIController');
                Route::resource('notifications', 'API\NotificationAPIController');
                Route::post('users/{id}', 'API\UserAPIController@update');
                Route::resource('faq_categories', 'API\FaqCategoryAPIController');
                Route::resource('faqs', 'API\FaqAPIController');
            });
        });
        Route::group(['middleware' => ['role:manager']], function () {
            Route::prefix('manager')->group(function () {
                Route::post('users/{id}', 'API\UserAPIController@update');
                Route::get('users/drivers_of_market/{id}', 'API\Manager\UserAPIController@driversOfMarket');
                Route::get('dashboard/{id}', 'API\DashboardAPIController@manager');
                Route::resource('markets', 'API\Manager\MarketAPIController');
                Route::resource('notifications', 'API\NotificationAPIController');
            });
        });

        Route::get('posts/all-posts', 'API\PostAPIController@allPosts');
        Route::post('posts/{post}/report', 'API\PostAPIController@report');
        Route::resource('posts', 'API\PostAPIController');
        Route::get('my-posts/{id?}', 'API\PostAPIController@myPosts');
        Route::post('posts/{id}/toggle-like', 'API\PostAPIController@toggleLike');
        Route::resource('comments', 'API\CommentAPIController');
        Route::get('posts/list-user-like/{id}', 'API\PostAPIController@listUserLike');
        Route::post('upload', 'API\UploadAPIController@store');

        Route::get('users/list-friends-request', 'API\UserAPIController@listFriendsRequest');
        Route::get('users/list-friends', 'API\UserAPIController@listFriends');
        Route::post('users/{id}', 'API\UserAPIController@update');
        Route::post('users/{id}/block', 'API\UserAPIController@block');
        Route::post('users/{id}/unblock', 'API\UserAPIController@unblock');
        Route::get('users/{id}', 'API\UserAPIController@show');
        Route::post('users/add-friend/{id}', 'API\UserAPIController@sendAddFriend');
        Route::post('users/confirm-friend/{id}', 'API\UserAPIController@confirmFriend');
        Route::post('users/cancel-friend-request/{id}', 'API\UserAPIController@cancelFriendRequest');
        Route::post('users/unfriend/{id}', 'API\UserAPIController@unfriend');

        Route::resource('order_statuses', 'API\OrderStatusAPIController');

        Route::get('payments/byMonth', 'API\PaymentAPIController@byMonth')->name('payments.byMonth');
        Route::resource('payments', 'API\PaymentAPIController');

        Route::get('favorites/exist', 'API\FavoriteAPIController@exist');
        Route::resource('favorites', 'API\FavoriteAPIController');

        Route::resource('orders', 'API\OrderAPIController');

        Route::resource('product_orders', 'API\ProductOrderAPIController');

        Route::resource('notifications', 'API\NotificationAPIController');
        Route::post('notifications/{id}/read', 'API\NotificationAPIController@read');
        Route::post('notifications/{id}/unread', 'API\NotificationAPIController@unread');
        Route::post('read-all-notifications', 'API\NotificationAPIController@readAll');

        Route::get('carts/count', 'API\CartAPIController@count')->name('carts.count');
        Route::resource('carts', 'API\CartAPIController');

        Route::resource('delivery_addresses', 'API\DeliveryAddressAPIController');

        Route::resource('drivers', 'API\DriverAPIController');

        Route::resource('earnings', 'API\EarningAPIController');

        Route::resource('driversPayouts', 'API\DriversPayoutAPIController');

        Route::resource('marketsPayouts', 'API\MarketsPayoutAPIController');

        Route::get('near-markets', 'API\MarketAPIController@index')->name('near-markets');
        Route::resource('coupons', 'API\CouponAPIController')->except([
            'show'
        ]);
        Route::resource('shops', 'API\ShopAPIController')->only(['store', 'update']);
        Route::post('shops/{id}/recommend', 'API\ShopAPIController@recommend')->name('recommend-shop');
        Route::resource('vnp_products', 'API\VnpProductAPIController')->only(['store', 'update']);
        Route::resource('votes', 'API\VoteAPIController')->only(['store']);
        Route::resource('reports', 'API\ReportAPIController')->only(['index']);
        Route::get('my-shop', 'API\ShopAPIController@myShop')->name('my-shop');
        Route::get('my-blockings', 'API\UserAPIController@blockings')->name('blockings');
        Route::get('my-blockers', 'API\UserAPIController@blockers')->name('blockers');
        Route::resource('banners', 'API\BannerAPIController')->only(['store']);
        Route::post('earn-point', 'API\PointHistoryAPIController@earn')->name('aern-point');
        Route::resource('vnp_categories', 'API\VnpCategoryAPIController')->only(['store','update']);
    });
    Route::resource('vnp_categories', 'API\VnpCategoryAPIController')->only(['index']);
    Route::resource('banners', 'API\BannerAPIController')->only(['index']);

    Route::resource('shops', 'API\ShopAPIController')->only(['index']);

    Route::resource('vnp_products', 'API\VnpProductAPIController')->only(['index']);

    Route::resource('votes', 'API\VoteAPIController')->only(['index']);
    
    Route::get('local', 'API\MasterAPIController@local')->name('master.local');
    
    // Route::resource('blocks', 'API\BlockAPIController');
});



// Route::resource('games', 'API\GameAPIController');


// Route::resource('point_histories', 'API\PointHistoryAPIController');
