<?php

namespace App\Notifications;

use App\Models\User;
use Benwilkins\FCM\FcmMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class RequestAddFriend extends Notification
{
    use Queueable;

    private $sendUser;

    private $receiveUser;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $sendUser, User $receiveUser)
    {
        $this->sendUser = $sendUser;
        $this->receiveUser = $receiveUser;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'fcm'];
    }

    /**
     * @param $notifiable
     * @return FcmMessage
     */
    public function toFcm($notifiable)
    {
        $message = new FcmMessage();
        $notification = [
            'title' => trans('lang.request_add_friend', ['user_name' => $this->sendUser->getAttribute('name')]),
            'text' => '',
            'image' => $this->sendUser->getFirstMediaUrl('avatar'),
        ];
        $data = [
            'click_action' => "FLUTTER_NOTIFICATION_CLICK",
            'sound' => 'default',
            'id' => 'request_add_friend',
            'send_user_id' => $this->sendUser->getAttribute('id'),
            'receive_user_id' => $this->receiveUser->getAttribute('id'),
            'user_name' => $this->sendUser->getAttribute('name'),
            'message' => $notification,
        ];
        $message->content($notification)->data($data)->priority(FcmMessage::PRIORITY_HIGH);

        return $message;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'send_user_id' => $this->sendUser->getAttribute('id'),
            'receive_user_id' => $this->receiveUser->getAttribute('id'),
            'user_name' => $this->sendUser->getAttribute('name'),
        ];
    }
}
