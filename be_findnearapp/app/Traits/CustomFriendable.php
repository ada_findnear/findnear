<?php

namespace App\Traits;

use Multicaret\Acquaintances\Interaction;
use Multicaret\Acquaintances\Models\Friendship;
use Multicaret\Acquaintances\Status;
use Multicaret\Acquaintances\Traits\Friendable;

/**
 * Trait CustomFriendable
 * @package App\Traits
 */
trait CustomFriendable
{
    use Friendable {
        Friendable::getOrPaginate as parentGetOrPaginate;
        Friendable::findFriendships as parentFindFriendships;
    }
    
    /**
     * @return mixed
     */
    public function getFriendRequestsWithSender()
    {
        $friendshipModelName = Interaction::getFriendshipModelName();
        $model = $friendshipModelName::whereRecipient($this)->with('sender')->with('sender.market')->whereStatus(Status::PENDING);
        return $this->limitOffset($model);
    }
    
    /**
     * @return mixed
     */
    public function getFriendsQueryBuilderByKey()
    {
        $friendships = $this->parentFindFriendships(Status::ACCEPTED)->get(['sender_id', 'recipient_id']);
        $recipients = $friendships->pluck('recipient_id')->all();
        $senders = $friendships->pluck('sender_id')->all();

        $ignoreId = [];
        if (request()->has('ignore_id')) {
            $ignoreId = explode(',', request()->get('ignore_id'));
        }
        
        $friends = $this->where('id', '!=', $this->getKey())
            ->whereIn('id', array_merge($recipients, $senders))
            ->whereNotIn('id', $ignoreId);

        if (request()->has('keyword')) {
            $friends = $friends->where('name', 'like', '%' . request()->get('keyword') . '%');
        }
        
        return $this->limitOffset($friends)->with('market');
    }

    private function limitOffset($model)
    {
        $limit = request()->get('limit', null);
        $offset = request()->get('offset', null);

        if ($limit) {
            $model->limit($limit);
        }

        if ($offset && $limit) {
            $model->skip($offset);
        }

        return $model;
    }
    
}
