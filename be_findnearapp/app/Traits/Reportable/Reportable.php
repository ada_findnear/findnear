<?php

namespace App\Traits\Reportable;
use App\Models\Report;

trait Reportable{
    public function report(){
        return $this->hasOne(Report::class,'refer_id')->where('refer_type', get_class($this));
    }
}