<?php

namespace App\Traits\Reportable;
use App\Models\Report;
use Illuminate\Support\Facades\Auth;

trait ReportableRepository{
    public function report($model, $reason= ''){
        return Report::create([
            'ref_id' => $model->id,
            'ref_type' => get_class($model),
            'reason' => $reason,
            'user_id' => Auth::id(),
        ]);
    }
}
