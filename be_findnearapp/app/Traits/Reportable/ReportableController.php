<?php

namespace App\Traits\Reportable;
use Illuminate\Http\Request;

trait ReportableController{
    public function report(Request $request, $id){
        $reason = $request->get('reason','');
        $model = $this->repository->find($id);
        return $this->repository->report($model, $reason);
    }
}