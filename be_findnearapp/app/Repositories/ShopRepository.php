<?php

namespace App\Repositories;

use App\Models\Shop;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ShopRepository
 * @package App\Repositories
 * @version July 23, 2021, 5:17 pm UTC
 *
 * @method Shop findWithoutFail($id, $columns = ['*'])
 * @method Shop find($id, $columns = ['*'])
 * @method Shop first($columns = ['*'])
*/
class ShopRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'user_id',
        'address',
        'latitude',
        'category_id',
        'description',
        'recommended',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Shop::class;
    }
}
