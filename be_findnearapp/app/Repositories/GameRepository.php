<?php

namespace App\Repositories;

use App\Models\Game;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class GameRepository
 * @package App\Repositories
 * @version July 28, 2021, 4:24 pm UTC
 *
 * @method Game findWithoutFail($id, $columns = ['*'])
 * @method Game find($id, $columns = ['*'])
 * @method Game first($columns = ['*'])
*/
class GameRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'total_player',
        'spent_points'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Game::class;
    }
}
