<?php

namespace App\Repositories;

use App\Models\Vote;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VoteRepository
 * @package App\Repositories
 * @version July 24, 2021, 2:49 am UTC
 *
 * @method Vote findWithoutFail($id, $columns = ['*'])
 * @method Vote find($id, $columns = ['*'])
 * @method Vote first($columns = ['*'])
*/
class VoteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'votes',
        'message',
        'ref_id',
        'ref_type'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Vote::class;
    }
}
