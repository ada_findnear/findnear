<?php

namespace App\Repositories;

use App\Models\Block;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BlockRepository
 * @package App\Repositories
 * @version July 24, 2021, 2:49 pm UTC
 *
 * @method Block findWithoutFail($id, $columns = ['*'])
 * @method Block find($id, $columns = ['*'])
 * @method Block first($columns = ['*'])
*/
class BlockRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'blocker_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Block::class;
    }
}
