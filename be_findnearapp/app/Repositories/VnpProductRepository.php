<?php

namespace App\Repositories;

use App\Models\VnpProduct;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VnpProductRepository
 * @package App\Repositories
 * @version July 24, 2021, 2:04 am UTC
 *
 * @method VnpProduct findWithoutFail($id, $columns = ['*'])
 * @method VnpProduct find($id, $columns = ['*'])
 * @method VnpProduct first($columns = ['*'])
*/
class VnpProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'user_id',
        'shop_id',
        'category_id',
        'price',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return VnpProduct::class;
    }
}
