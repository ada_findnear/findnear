<?php

namespace App\Repositories;

use App\Models\PointHistory;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PointHistoryRepository
 * @package App\Repositories
 * @version July 28, 2021, 4:25 pm UTC
 *
 * @method PointHistory findWithoutFail($id, $columns = ['*'])
 * @method PointHistory find($id, $columns = ['*'])
 * @method PointHistory first($columns = ['*'])
*/
class PointHistoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'game_id',
        'point',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PointHistory::class;
    }
}
