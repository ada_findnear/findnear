<?php

namespace App\Repositories;

use App\Models\VnpCategory;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VnpCategoryRepository
 * @package App\Repositories
 * @version July 24, 2021, 2:07 am UTC
 *
 * @method VnpCategory findWithoutFail($id, $columns = ['*'])
 * @method VnpCategory find($id, $columns = ['*'])
 * @method VnpCategory first($columns = ['*'])
*/
class VnpCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'parent_id',
        'description',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return VnpCategory::class;
    }
}
