<?php
/**
 * File name: Comment.php
 * Last modified: 2021.05.25 at 19:56:12
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 */

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Post
 * @package App\Models
 */
class Comment extends Model implements HasMedia
{
    use HasMediaTrait;
    use SoftDeletes;
    public $table = 'comments';

    public $fillable = [
        'content',
        'user_id',
        'post_id',
    ];

    protected $with = ['media'];

    /**
     * @return BelongsTo post
     */
    public function post(): BelongsTo
    {
        return $this->belongsTo(Post::class);
    }

    /**
     * @return BelongsTo creator
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'content' => 'required_without:files',
        'files.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:' . MAX_FILE_SIZE,
        'files' => 'max:1',
        'post_id' => 'required',
    ];

    /**
     * Add Media to api results
     * @return bool
     */
    public function getHasMediaAttribute()
    {
        return $this->hasMedia('image_post_comment');
    }

    /**
     * @param Media|null $media
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public function registerMediaConversions(\Spatie\MediaLibrary\Models\Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_CROP, 200, 200)
            ->sharpen(10);

        $this->addMediaConversion('icon')
            ->fit(Manipulations::FIT_CROP, 100, 100)
            ->sharpen(10);
    }
    
}
