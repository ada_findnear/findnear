<?php
/**
 * File name: Post.php
 * Last modified: 2021.05.25 at 19:56:12
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 */

namespace App\Models;

use App\Traits\Reportable\Reportable;
use Eloquent as Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Multicaret\Acquaintances\Traits\CanBeLiked;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Post
 * @property User creator
 * @package App\Models
 */
class Post extends Model implements HasMedia
{
    use HasMediaTrait {
        getMedia as protected getFirstMediaUrlTrait;
    }
    use Reportable;
    use SoftDeletes;
    use CanBeLiked;
    
    public $table = 'posts';

    public const PRIVATE_VIEW = '0';
    public const ONLY_FRIENDS_VIEW = '1';
    public const PUBLIC_VIEW = '2';

    public const REJECT_STATUS = '0';
    public const PENDING_STATUS = '1';
    public const APPROVE_STATUS = '2';

    public $fillable = [
        'content',
        'viewer',
        'status',
        'user_id'
    ];
    
    protected $casts = [
        'viewer' => 'integer',
        'status' => 'integer'
    ];

    /**
     * New Attributes
     *
     * @var array
     */
    protected $appends = [
        'isLike'
    ];
    
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'content' => 'required_without:files',
        'files.*' => 'file|max:' . MAX_FILE_SIZE,
        'files' => 'max:10',
    ];
    
    /**
     * Add Media to api results
     * @return bool
     */
    public function getHasMediaAttribute()
    {
        return $this->hasMedia('image_post');
    }

    /**
     * @return HasMany comments
     */
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * @return BelongsTo creator
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return bool
     */
    public function getIsLikeAttribute(): bool
    {
        return $this->isLikedBy(Auth::user());
    }
    
    /**
     * @param Media|null $media
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public function registerMediaConversions(\Spatie\MediaLibrary\Models\Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_CROP, 200, 200)
            ->sharpen(10);
        
        $this->addMediaConversion('icon')
            ->fit(Manipulations::FIT_CROP, 100, 100)
            ->sharpen(10);
    }
}
