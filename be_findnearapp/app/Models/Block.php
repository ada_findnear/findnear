<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Block
 * @package App\Models
 * @version July 24, 2021, 2:49 pm UTC
 *
 * @property integer user_id
 * @property integer blocker_id
 */
class Block extends Model
{

    public $table = 'blocks';
    
    public $fillable = [
        'user_id',
        'blocker_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'blocker_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
