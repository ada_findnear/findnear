<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Cart
 * @package App\Models
 * @version September 4, 2019, 3:38 pm UTC
 *
 * @property \App\Models\Product product
 * @property \App\Models\User user
 * @property \Illuminate\Database\Eloquent\Collection options
 * @property integer product_id
 * @property integer user_id
 * @property integer quantity
 */
class TDistrict extends Model
{

    public $table = 't_district';

    public function wards(){
        return $this->hasMany(TWard::class,'district_id','idx');
    }
    public function province(){
        return $this->belongsTo(TProvince::class,'province_id','idx');
    }
}
