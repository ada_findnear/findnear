<?php

namespace App\Models;

use App\Traits\CustomFriendable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Cashier\Billable;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Permission\Traits\HasRoles;
use Multicaret\Acquaintances\Traits\CanLike;

/**
 * Class User
 * @package App\Models
 * @version July 10, 2018, 11:44 am UTC
 *
 * @property \App\Models\Cart[] cart
 * @property string name
 * @property string code
 * @property string email
 * @property string password
 * @property string api_token
 * @property string device_token
 * @property Carbon expired_at
 * @property bool|mixed active
 */
class User extends Authenticatable implements HasMedia
{
    use Notifiable;
    use Billable;
    use HasMediaTrait {
        getFirstMediaUrl as protected getFirstMediaUrlTrait;
    }
    use HasRoles;
    use CustomFriendable;
    use CanLike;

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:users',
        'gender' => 'nullable|in:Male,Female,Other',
        // 'password' => 'required',
    ];
    public $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [
        'name',
        'email',
        'password',
        'api_token',
        'device_token',
        'code',
        'current_point',
        'total_point',
        'spent_point',
        'expired_at',
        'active',
    ];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'api_token' => 'string',
        'device_token' => 'string',
        'remember_token' => 'string',
        'current_point' => 'integer',
        'total_point' => 'integer',
        'spent_point' => 'integer',
        'active' => 'boolean',
    ];
    /**
     * New Attributes
     *
     * @var array
     */
    protected $appends = [
        'custom_fields',
        'has_media',
        'is_friend',
        'age',
        'has_friend_request_from',
        'has_sent_friend_request_to',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
        'expired_at',
    ];

    /**
     * Specifies the user's FCM token
     *
     * @return string
     */
    public function routeNotificationForFcm($notification)
    {
        return $this->device_token;
    }

    /**
     * @return bool
     */
    public function getIsFriendAttribute(): bool
    {
        if (request()->has("api_token") && $authUser = $this->where('api_token', request()->get("api_token"))->first()) {
            return $this->isFriendWith($authUser);
        }

        return false;
    }

    /**
     * @return bool
     */
    public function getHasFriendRequestFromAttribute()
    {
        $me = Auth::user();
        if ($me) {
            return $me->hasFriendRequestFrom($this);
        }

        return false;
    }

    /**
     * @return bool
     */
    public function getHasSentFriendRequestToAttribute()
    {
        $me = Auth::user();
        if ($me) {
            return $me->hasSentFriendRequestTo($this);
        }

        return false;
    }

    /**
     * @return int
     */
    public function getAgeAttribute()
    {
        if ($this->getAttribute('birth_date')) {
            return Carbon::parse($this->getAttribute('birth_date'))->age;
        }
        return 0;
    }

    /**
     * @param Media|null $media
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public function registerMediaConversions(\Spatie\MediaLibrary\Models\Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_CROP, 200, 200)
            ->sharpen(10);

        $this->addMediaConversion('icon')
            ->fit(Manipulations::FIT_CROP, 100, 100)
            ->sharpen(10);
    }

    /**
     * to generate media url in case of fallback will
     * return the file type icon
     * @param string $conversion
     * @return string url
     */
    public function getFirstMediaUrl($collectionName = 'default', $conversion = '')
    {
        $url = $this->getFirstMediaUrlTrait($collectionName);
        if ($url) {
            $array = explode('.', $url);
            $extension = strtolower(end($array));
            if (in_array($extension, config('medialibrary.extensions_has_thumb'))) {
                return asset($this->getFirstMediaUrlTrait($collectionName, $conversion));
            } else {
                return asset(config('medialibrary.icons_folder') . '/' . $extension . '.png');
            }
        }else{
            return asset('images/avatar_default.png');
        }
    }

    public function getCustomFieldsAttribute()
    {
        $hasCustomField = in_array(static::class, setting('custom_field_models', []));
        if (!$hasCustomField) {
            return [];
        }
        $array = $this->customFieldsValues()
            ->join('custom_fields', 'custom_fields.id', '=', 'custom_field_values.custom_field_id')
//            ->where('custom_fields.in_table', '=', true)
            ->select(['value', 'view', 'name'])
            ->get()->toArray();

        return convertToAssoc($array, 'name');
    }

    public function customFieldsValues()
    {
        return $this->morphMany('App\Models\CustomFieldValue', 'customizable');
    }

    /**
     * Add Media to api results
     * @return bool
     */
    public function getHasMediaAttribute()
    {
        return $this->hasMedia('avatar') ? true : false;
    }

    /**
     * @return HasOne
     **/
    public function market()
    {
        return $this->hasOne(Market::class, 'owner_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function markets()
    {
        return $this->belongsToMany(\App\Models\Market::class, 'user_markets');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function cart()
    {
        return $this->hasMany(\App\Models\Cart::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function shop()
    {
        return $this->hasOne(\App\Models\Shop::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function blockings()
    {
        return $this->belongsToMany(\App\Models\User::class, 'blocks','blocker_id','user_id','id', 'id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function blockers()
    {
        return $this->belongsToMany(\App\Models\User::class, 'blocks','user_id','blocker_id','id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function point_history()
    {
        return $this->hasMany(\App\Models\PointHistory::class, 'user_id','id');
    }
}
