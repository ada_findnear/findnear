<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Game
 * @package App\Models
 * @version July 28, 2021, 4:24 pm UTC
 *
 * @property string name
 * @property string description
 * @property bigInteger total_player
 * @property bigInteger spent_points
 */
class Game extends Model
{

    public $table = 'games';
    


    public $fillable = [
        'name',
        'description',
        'total_player',
        'spent_points'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    ];

    /**
     * New Attributes
     *
     * @var array
     */
    protected $appends = [
        'custom_fields',
        
    ];

    public function customFieldsValues()
    {
        return $this->morphMany('App\Models\CustomFieldValue', 'customizable');
    }

    public function getCustomFieldsAttribute()
    {
        $hasCustomField = in_array(static::class,setting('custom_field_models',[]));
        if (!$hasCustomField){
            return [];
        }
        $array = $this->customFieldsValues()
            ->join('custom_fields','custom_fields.id','=','custom_field_values.custom_field_id')
            ->where('custom_fields.in_table','=',true)
            ->get()->toArray();

        return convertToAssoc($array,'name');
    }

    
    
}
