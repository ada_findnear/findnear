<?php

namespace App\Criteria\Markets;

use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ActiveCriteria.
 *
 * @package namespace App\Criteria\Markets;
 */
class SearchCriteria implements CriteriaInterface
{

    /**
     * @var array
     */
    private $request;

    /**
     * NearCriteria constructor.
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $keyword = $this->request->get('keyword', '');
        if ($keyword != '') {
            return $model->leftJoin('users', 'users.id', 'markets.owner_id')
                ->where(function ($query) use ($keyword) {
                    return $query->where('users.code', $keyword)
                        ->orWhere('users.name', 'LIKE', '%' . $keyword . '%');
                });
        }
        return $model;
    }
}
