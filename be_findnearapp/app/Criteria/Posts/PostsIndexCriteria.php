<?php
/**
 * File name: PostsIndexCriteria.php
 * Last modified: 2020.05.04 at 09:04:18
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Criteria\Posts;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class PostsIndexCriteria.
 *
 * @package namespace App\Criteria\Categories;
 */
class PostsIndexCriteria implements CriteriaInterface
{

    /**
     * @var array
     */
    private $request;

    /**
     * CategoriesOfFieldsCriteria constructor.
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $user = Auth::user();
        $friends = $user->getFriends(null, null, ['id'])->pluck('id')->toArray();
        
        return $model->with('media')
            ->withCount('comments')
            ->withCount('likers')
            ->where('user_id', $user->id)
            ->orWhere('viewer', Post::PUBLIC_VIEW)
            ->orWhere( function ($query) use ($friends) {
                $query->whereIn('user_id', $friends);
                $query->where('viewer', Post::ONLY_FRIENDS_VIEW);
            })
            ->orderByDesc('created_at');
    }
}
