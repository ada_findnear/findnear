<?php
/**
 * File name: MyPostsCriteria.php
 * Last modified: 2020.05.04 at 09:04:18
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Criteria\Posts;

use App\Models\Post;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class MyPostsCriteria.
 *
 * @package namespace App\Criteria\Categories;
 */
class MyPostsCriteria implements CriteriaInterface
{

    /**
     * @var array
     */
    private $request;
    private $userRepository;

    /**
     * CategoriesOfFieldsCriteria constructor.
     */
    public function __construct(Request $request, UserRepository $userRepository)
    {
        $this->request = $request;
        $this->userRepository = $userRepository;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $userId = Auth::id();
        $isFriend = null;
        if ($this->request->route('id') && $this->request->route('id') != $userId) {
            $friend = $this->userRepository->findWithoutFail($this->request->route('id'));
            $isFriend = Auth::user()->isFriendWith($friend);
            $userId = $this->request->route('id');
        }
        
        $post =  $model->with('media')
            ->withCount('comments')
            ->withCount('likers')
            ->where('user_id', $userId);
        if (!is_null($isFriend)) {
            $post = $post->whereNotIn('viewer', $isFriend ? [Post::PRIVATE_VIEW] : [Post::ONLY_FRIENDS_VIEW, Post::PRIVATE_VIEW]);
        }
    
        return $post->orderByDesc('created_at');
    }
}
