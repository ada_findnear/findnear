<?php

namespace App\Http\Middleware;


use Illuminate\Cookie\Middleware\EncryptCookies as Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;


class Logger extends Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $str = $request->url()."\r\n";
        if (Auth::guard($guard)->check()) {
            $str .= 'User: '. json_encode(Auth::user()->toArray())."\r\n";
        }
        $str .= 'Request: '. json_encode($request->all())."\r\n";
        // $str .= 'Token: '. $request->bearerToken();
        $str .=  '-----------------------------------------------';
        // Log::useFiles(storage_path().'logs/static'.date('Y_m_d').'.log');
        Log::channel('daily')->debug($str);
        return $next($request);
    }
}
