<?php

namespace App\Http\Controllers;

use App\DataTables\VnpCategoryDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateVnpCategoryRequest;
use App\Http\Requests\UpdateVnpCategoryRequest;
use App\Repositories\VnpCategoryRepository;
use App\Repositories\CustomFieldRepository;

use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Prettus\Validator\Exceptions\ValidatorException;

class VnpCategoryController extends Controller
{
    /** @var  VnpCategoryRepository */
    private $vnpCategoryRepository;

    /**
     * @var CustomFieldRepository
     */
    private $customFieldRepository;

    

    public function __construct(VnpCategoryRepository $vnpCategoryRepo, CustomFieldRepository $customFieldRepo )
    {
        parent::__construct();
        $this->vnpCategoryRepository = $vnpCategoryRepo;
        $this->customFieldRepository = $customFieldRepo;
        
    }

    /**
     * Display a listing of the VnpCategory.
     *
     * @param VnpCategoryDataTable $vnpCategoryDataTable
     * @return Response
     */
    public function index(VnpCategoryDataTable $vnpCategoryDataTable)
    {
        return $vnpCategoryDataTable->render('vnp_categories.index');
    }

    /**
     * Show the form for creating a new VnpCategory.
     *
     * @return Response
     */
    public function create()
    {
        
        
        $hasCustomField = in_array($this->vnpCategoryRepository->model(),setting('custom_field_models',[]));
            if($hasCustomField){
                $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->vnpCategoryRepository->model());
                $html = generateCustomField($customFields);
            }
        return view('vnp_categories.create')->with("customFields", isset($html) ? $html : false);
    }

    /**
     * Store a newly created VnpCategory in storage.
     *
     * @param CreateVnpCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateVnpCategoryRequest $request)
    {
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->vnpCategoryRepository->model());
        try {
            $vnpCategory = $this->vnpCategoryRepository->create($input);
            $vnpCategory->customFieldsValues()->createMany(getCustomFieldsValues($customFields,$request));
            
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.saved_successfully',['operator' => __('lang.vnp_category')]));

        return redirect(route('vnpCategories.index'));
    }

    /**
     * Display the specified VnpCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $vnpCategory = $this->vnpCategoryRepository->findWithoutFail($id);

        if (empty($vnpCategory)) {
            Flash::error('Vnp Category not found');

            return redirect(route('vnpCategories.index'));
        }

        return view('vnp_categories.show')->with('vnpCategory', $vnpCategory);
    }

    /**
     * Show the form for editing the specified VnpCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $vnpCategory = $this->vnpCategoryRepository->findWithoutFail($id);
        
        

        if (empty($vnpCategory)) {
            Flash::error(__('lang.not_found',['operator' => __('lang.vnp_category')]));

            return redirect(route('vnpCategories.index'));
        }
        $customFieldsValues = $vnpCategory->customFieldsValues()->with('customField')->get();
        $customFields =  $this->customFieldRepository->findByField('custom_field_model', $this->vnpCategoryRepository->model());
        $hasCustomField = in_array($this->vnpCategoryRepository->model(),setting('custom_field_models',[]));
        if($hasCustomField) {
            $html = generateCustomField($customFields, $customFieldsValues);
        }

        return view('vnp_categories.edit')->with('vnpCategory', $vnpCategory)->with("customFields", isset($html) ? $html : false);
    }

    /**
     * Update the specified VnpCategory in storage.
     *
     * @param  int              $id
     * @param UpdateVnpCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVnpCategoryRequest $request)
    {
        $vnpCategory = $this->vnpCategoryRepository->findWithoutFail($id);

        if (empty($vnpCategory)) {
            Flash::error('Vnp Category not found');
            return redirect(route('vnpCategories.index'));
        }
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->vnpCategoryRepository->model());
        try {
            $vnpCategory = $this->vnpCategoryRepository->update($input, $id);
            
            
            foreach (getCustomFieldsValues($customFields, $request) as $value){
                $vnpCategory->customFieldsValues()
                    ->updateOrCreate(['custom_field_id'=>$value['custom_field_id']],$value);
            }
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.updated_successfully',['operator' => __('lang.vnp_category')]));

        return redirect(route('vnpCategories.index'));
    }

    /**
     * Remove the specified VnpCategory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $vnpCategory = $this->vnpCategoryRepository->findWithoutFail($id);

        if (empty($vnpCategory)) {
            Flash::error('Vnp Category not found');

            return redirect(route('vnpCategories.index'));
        }

        $this->vnpCategoryRepository->delete($id);

        Flash::success(__('lang.deleted_successfully',['operator' => __('lang.vnp_category')]));

        return redirect(route('vnpCategories.index'));
    }

        /**
     * Remove Media of VnpCategory
     * @param Request $request
     */
    public function removeMedia(Request $request)
    {
        $input = $request->all();
        $vnpCategory = $this->vnpCategoryRepository->findWithoutFail($input['id']);
        try {
            if($vnpCategory->hasMedia($input['collection'])){
                $vnpCategory->getFirstMedia($input['collection'])->delete();
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
