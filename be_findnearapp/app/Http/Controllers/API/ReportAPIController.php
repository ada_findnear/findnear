<?php

namespace App\Http\Controllers\API;


use App\Models\Report;
use App\Repositories\ReportRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Support\Facades\Response;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class ReportController
 * @package App\Http\Controllers\API
 */

class ReportAPIController extends Controller
{
    /** @var  ReportRepository */
    private $reportRepository;

    public function __construct(ReportRepository $reportRepo)
    {
        $this->reportRepository = $reportRepo;
    }

    /**
     * Display a listing of the Report.
     * GET|HEAD /reports
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try{
            $this->reportRepository->pushCriteria(new RequestCriteria($request));
            $this->reportRepository->pushCriteria(new LimitOffsetCriteria($request));
        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }
        $reports = $this->reportRepository->all();

        return $this->sendResponse($reports->toArray(), 'Reports retrieved successfully');
    }

    /**
     * Display the specified Report.
     * GET|HEAD /reports/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        /** @var Report $report */
        if (!empty($this->reportRepository)) {
            $report = $this->reportRepository->findWithoutFail($id);
        }

        if (empty($report)) {
            return $this->sendError('Report not found');
        }

        return $this->sendResponse($report->toArray(), 'Report retrieved successfully');
    }
}
