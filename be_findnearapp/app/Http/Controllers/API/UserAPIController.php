<?php
/**
 * File name: UserAPIController.php
 * Last modified: 2020.10.29 at 17:03:54
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\ConfirmFriendRequest;
use App\Http\Requests\API\UpdateUserRequest;
use App\Models\Market;
use App\Models\User;
use App\Notifications\AgreeAddFriend;
use App\Notifications\RequestAddFriend;
use App\Repositories\CustomFieldRepository;
use App\Repositories\RoleRepository;
use App\Repositories\UploadRepository;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Password;
use Multicaret\Acquaintances\Status;
use Prettus\Validator\Exceptions\ValidatorException;

class UserAPIController extends Controller
{
    private $userRepository;
    private $uploadRepository;
    private $roleRepository;
    private $customFieldRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepository, UploadRepository $uploadRepository, RoleRepository $roleRepository, CustomFieldRepository $customFieldRepo)
    {
        $this->userRepository = $userRepository;
        $this->uploadRepository = $uploadRepository;
        $this->roleRepository = $roleRepository;
        $this->customFieldRepository = $customFieldRepo;
    }

    function login(Request $request)
    {
        try {
            $this->validate($request, [
                'code' => 'required',
                'password' => 'required',
            ]);
            if (auth()->attempt(['code' => $request->input('code'), 'password' => $request->input('password')])) {
                $user = auth()->user();
                if ($user->expired_at && $user->expired_at->gt(Carbon::now()) && $user->active) {
                    // Authentication passed...
                    $user->device_token = $request->input('device_token', '');
                    $user->api_token = str_random(60);
                    $user->save();
                    return $this->sendResponse($user, 'User retrieved successfully');
                }
            }
            return $this->sendError(__('auth.failed'), 401);
        } catch (Exception $e) {
            return $this->sendError($e->getMessage(), 401);
        }

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return
     */
    function register(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required',
                'code' => 'required|size:8',
                'password' => 'required',
            ]);
            
            DB::beginTransaction();
            
            /** @var User $user */
            $user = $this->userRepository->findWhere([
                'code' => $request->get('code'),
            ])->first();
            
            if (!$user) {
                $user = new User();
                $user->code = $request->input('code');
            } elseif ($user->active) {
                return $this->sendError('Can not register with this code', 401);
            }
            
            $user->name = $request->input('name');
            $user->device_token = $request->input('device_token', '');
            $user->password = Hash::make($request->input('password'));
            $user->api_token = str_random(60);
            $user->active = true;
            $user->expired_at = Carbon::now()->addYear();
            $user->save();
    
            $market = new Market();
            $market->active = true;
            $user->market()->save($market);

            $defaultRoles = $this->roleRepository->findByField('default', '1');
            $defaultRoles = $defaultRoles->pluck('name')->toArray();
            $user->assignRole($defaultRoles);

            $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model());

            foreach (getCustomFieldsValues($customFields, $request) as $value) {
                $user->customFieldsValues()
                    ->updateOrCreate(['custom_field_id' => $value['custom_field_id']], $value);
            }


            if (copy(public_path('images/avatar_default.png'), public_path('images/avatar_default_temp.png'))) {
                $user->addMedia(public_path('images/avatar_default_temp.png'))
                    ->withCustomProperties(['uuid' => bcrypt(str_random())])
                    ->toMediaCollection('avatar');
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return $this->sendError($e->getMessage(), 401);
        }


        return $this->sendResponse($user->load('market'), 'User retrieved successfully');
    }

    function logout(Request $request)
    {
        $user = $this->userRepository->findByField('api_token', $request->input('api_token'))->first();
        if (!$user) {
            return $this->sendError('User not found', 401);
        }
        try {
            $user = $this->userRepository->update(['device_token' => ''], $user->id);
            auth()->logout();
        } catch (Exception $e) {
            $this->sendError($e->getMessage(), 401);
        }
        return $this->sendResponse($user['name'], 'User logout successfully');

    }

    function user(Request $request)
    {
        $user = $this->userRepository->findByField('api_token', $request->input('api_token'))->first();

        if (!$user) {
            return $this->sendError('User not found', 401);
        }

        return $this->sendResponse($user->load('market.fields'), 'User retrieved successfully');
    }

    function block($id)
    {
        $user = User::find($id);
        if (!$user || $user->blockers()->where('blocker_id', Auth::id())->count() > 0) {
            return $this->sendError('User not found', 401);
        }
        $user->blockers()->attach(Auth::id());
        return $this->sendResponse($user->load('market.fields'), 'User block successfully');
    }
    
    function unblock($id)
    {
        $user = Auth::user()->blockings()->where('user_id', $id)->first();
        if (!$user) {
            return $this->sendError('User not found', 401);
        }
        Auth::user()->blockings()->detach($user->id);
        return $this->sendResponse($user->load('market.fields'), 'User unblock successfully');
    }
    
    /**
     * Show profile user.
     *
     * @param $id
     * @return JsonResponse|mixed
     */
    function show($id)
    {
        $friend = $this->userRepository->findWithoutFail($id);
    
        if(Auth::check()){
            $friend->is_blocking = Auth::user()->blockings()->where('user_id', $id)->first() != null;
            $friend->is_blocker = Auth::user()->blockings()->where('blocker_id', $id)->first() != null;
        }
        if (!$friend) {
            return $this->sendError('User not found', 401);
        }
    
        return $this->sendResponse($friend->load('market.fields'), 'User retrieved successfully');
    }

    function settings(Request $request)
    {
        $settings = setting()->all();
        $settings = array_intersect_key($settings,
            [
                'default_tax' => '',
                'default_currency' => '',
                'default_currency_decimal_digits' => '',
                'app_name' => '',
                'currency_right' => '',
                'enable_paypal' => '',
                'enable_stripe' => '',
                'enable_razorpay' => '',
                'main_color' => '',
                'main_dark_color' => '',
                'second_color' => '',
                'second_dark_color' => '',
                'accent_color' => '',
                'accent_dark_color' => '',
                'scaffold_dark_color' => '',
                'scaffold_color' => '',
                'google_maps_key' => '',
                'fcm_key' => '',
                'mobile_language' => '',
                'app_version' => '',
                'enable_version' => '',
                'distance_unit' => '',
                'home_section_1'=> '',
                'home_section_2'=> '',
                'home_section_3'=> '',
                'home_section_4'=> '',
                'home_section_5'=> '',
                'home_section_6'=> '',
                'home_section_7'=> '',
                'home_section_8'=> '',
                'home_section_9'=> '',
                'home_section_10'=> '',
                'home_section_11'=> '',
                'home_section_12'=> '',
            ]
        );

        if (!$settings) {
            return $this->sendError('Settings not found', 401);
        }

        return $this->sendResponse($settings, 'Settings retrieved successfully');
    }

    /**
     * Update the specified User in storage.
     *
     * @param int $id
     * @param UpdateUserRequest $request
     * @return JsonResponse|mixed
     */
    public function update($id, UpdateUserRequest $request)
    {
        $user = $this->userRepository->findWithoutFail($id);
        $market = $user->market;

        if (empty($user)) {
            return $this->sendResponse([
                'error' => true,
                'code' => 404,
            ], 'User not found');
        }
        $input = $request->except(['password', 'api_token']);
        try {
            if ($request->has('device_token')) {
                $user = $this->userRepository->update($request->only('device_token'), $id);
            } else {
                $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model());
                $user = $this->userRepository->update($input, $id);
                if ($request->has('fieldId')) {
                    $market->fields()->sync([$request->get('fieldId')]);
                }

                foreach (getCustomFieldsValues($customFields, $request) as $value) {
                    $user->customFieldsValues()
                        ->updateOrCreate(['custom_field_id' => $value['custom_field_id']], $value);
                }
            }
            if ($request->hasFile('avatar')) {
                $user->clearMediaCollection('avatar');
                $user->addMediaFromRequest('avatar')->toMediaCollection('avatar');
            }
            if ($request->hasFile('cover')) {
                $user->clearMediaCollection('cover');
                $user->addMediaFromRequest('cover')->toMediaCollection('cover');
            }
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage(), 401);
        }

        return $this->sendResponse($user->load('market.fields'), __('lang.updated_successfully', ['operator' => __('lang.user')]));
    }

    function sendResetLinkEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $response = Password::broker()->sendResetLink(
            $request->only('email')
        );

        if ($response == Password::RESET_LINK_SENT) {
            return $this->sendResponse(true, 'Reset link was sent successfully');
        } else {
            return $this->sendError('Reset link not sent', 401);
        }

    }

    /**
     * List and search friends.
     *
     * @return mixed
     */
    public function listFriends(Request $request)
    {
        $friends = Auth::user()->getFriendsQueryBuilderByKey(Status::ACCEPTED)->get();
    
        return $this->sendResponse($friends, "Retrieve list friends successfully");
    }

    /**
     * @return mixed
     */
    public function listFriendsRequest()
    {
        $user = Auth::user();
        $friends = $user->getFriendRequestsWithSender()->get();

        return $this->sendResponse($friends, 'Retrieve list request friends successfully');
    }

    /**
     * @param $id
     * @return JsonResponse|mixed
     */
    public function sendAddFriend($id)
    {
        $sendUser = Auth::user();
        $receiveUser = $this->userRepository->findWithoutFail($id);
    
        if (!$receiveUser || $id == $sendUser->getAttribute('id')) {
            return $this->sendError('User not found', 404);
        }

        try {
            if ($sendUser->hasFriendRequestFrom($receiveUser)) {
                $sendUser->acceptFriendRequest($receiveUser);
            } else {
                $sendUser->befriend($receiveUser);
                Notification::send([$receiveUser], new RequestAddFriend($sendUser, $receiveUser));
            }
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage(), 401);
        }
    
        return $this->sendResponse(Auth::user(), __('lang.friend_request_sent_successfully'));
    }

    /**
     * @param $id
     * @return JsonResponse|mixed
     */
    public function confirmFriend($id, ConfirmFriendRequest $request)
    {
        $receiveUser = Auth::user();
        $sendUser = $this->userRepository->findWithoutFail($id);

        if (!$sendUser || $id == $receiveUser->getAttribute('id') || !$receiveUser->hasFriendRequestFrom($sendUser)) {
            return $this->sendError('User not found', 404);
        }

        try {
            if ($request->get('accept')) {
                $receiveUser->acceptFriendRequest($sendUser);
                Notification::send([$sendUser], new AgreeAddFriend($sendUser, $receiveUser));
            } else {
                $receiveUser->denyFriendRequest($sendUser);
            }
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage(), 401);
        }

        return $this->sendResponse(Auth::user(), __($request->get('accept') ? 'lang.friend_accept_request' : 'lang.friend_deny_request'));
    }
    
    /**
     * Cancel friend request.
     *
     * @param $id
     * @return JsonResponse|mixed
     */
    public function cancelFriendRequest($id)
    {
        /** @var User $sendUser */
        $sendUser = Auth::user();
        $receiveUser = $this->userRepository->findWithoutFail($id);
        
        if (!$receiveUser || $id == $sendUser->getAttribute('id') || !$receiveUser->hasFriendRequestFrom($sendUser)) {
            return $this->sendError('User not found', 404);
        }
    
        try {
            $receiveUser->denyFriendRequest($sendUser);
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage(), 401);
        }
    
        return $this->sendResponse(Auth::user(), __('lang.cancel_friend_request_successfully'));
    }
    
    /**
     * Unfriend
     *
     * @param $id
     * @return JsonResponse|mixed
     */
    public function unfriend($id)
    {
        $sendUser = Auth::user();
        $receiveUser = $this->userRepository->findWithoutFail($id);
        
        if (!$receiveUser || $id == $sendUser->getAttribute('id') || !$sendUser->isFriendWith($receiveUser)) {
            return $this->sendError('User not found', 404);
        }
    
        try {
            $sendUser->unfriend($receiveUser);
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage(), 401);
        }
    
        return $this->sendResponse(Auth::user(), __('lang.un_friend_request_successfully'));
    }
    
    /**
     * @return mixed
     */
    public function blockings()
    {
        $user = Auth::user();
        $friends = $user->blockings;

        return $this->sendResponse($friends, 'Retrieve list blockings successfully');
    }
    /**
     * @return mixed
     */
    public function blockers()
    {
        $user = Auth::user();
        $friends = $user->blockers;

        return $this->sendResponse($friends, 'Retrieve list blockings successfully');
    }
}
