<?php

namespace App\Http\Controllers\API;

use App\Criteria\StatusCriteria;
use App\Criteria\UserCriteria;
use App\Criteria\VnpProducts\FilterCriteria;
use App\Models\VnpProduct;
use App\Repositories\VnpProductRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateVnpProductRequest;
use App\Http\Requests\UpdateVnpProductRequest;
use Illuminate\Support\Facades\Auth;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Support\Facades\Response;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class VnpProductController
 * @package App\Http\Controllers\API
 */

class VnpProductAPIController extends Controller
{
    /** @var  VnpProductRepository */
    private $vnpProductRepository;

    public function __construct(VnpProductRepository $vnpProductRepo)
    {
        $this->vnpProductRepository = $vnpProductRepo;
    }

    /**
     * Display a listing of the VnpProduct.
     * GET|HEAD /vnpProducts
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try{
            $this->vnpProductRepository->pushCriteria(new RequestCriteria($request));
            $this->vnpProductRepository->pushCriteria(new StatusCriteria($request));
            $this->vnpProductRepository->pushCriteria(new UserCriteria($request));
            $this->vnpProductRepository->pushCriteria(new FilterCriteria($request));
        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }
        $vnpProducts = $this->vnpProductRepository->paginate($request->get('limit',10));
        return $this->sendResponse($vnpProducts->toArray(), 'Products retrieved successfully');
    }

    /**
     * Display the specified VnpProduct.
     * GET|HEAD /vnpProducts/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        /** @var VnpProduct $vnpProduct */
        if (!empty($this->vnpProductRepository)) {
            $vnpProduct = $this->vnpProductRepository->findWithoutFail($id);
        }

        if (empty($vnpProduct)) {
            return $this->sendError('Product not found');
        }

        return $this->sendResponse($vnpProduct->toArray(), 'Product retrieved successfully');
    }
    
    /**
     * Store a newly created VnpProduct in storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateVnpProductRequest $request)
    {
        $input = $request->all();
        try {
            $user = Auth::user();
            $input['user_id'] = $user->id;
            $input['shop_id'] = $user->shop->id;
            $vnpProduct = $this->vnpProductRepository->create($input);
            if (isset($input['image']) && $input['image']) {
                $vnpProduct->media()->delete();
                $vnpProduct->addMedia($input['image'])->toMediaCollection('image_product');
            }
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($vnpProduct->toArray(),__('lang.saved_successfully', ['operator' => __('lang.product')]));
    }

    /**
     * Update the specified VnpProduct in storage.
     *
     * @param int $id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, UpdateVnpProductRequest $request)
    {
        $vnpProduct = $this->vnpProductRepository->findWithoutFail($id);

        if (empty($vnpProduct) || $vnpProduct->user_id != auth()->id()) {
            return $this->sendError('Product not found');
        }
        $input = $request->all();
        try {
            $vnpProduct = $this->vnpProductRepository->update($input, $id);
            if (isset($input['image']) && $input['image']) {
                $vnpProduct->media->delete();
                $vnpProduct->addMedia($input['image'])->toMediaCollection('image_product');
            }
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($vnpProduct->toArray(),__('lang.updated_successfully', ['operator' => __('lang.product')]));
    }
}
