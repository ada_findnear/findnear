<?php

namespace App\Http\Controllers\API;


use App\Models\VnpCategory;
use App\Repositories\VnpCategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Support\Facades\Response;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class VnpCategoryController
 * @package App\Http\Controllers\API
 */

class VnpCategoryAPIController extends Controller
{
    /** @var  VnpCategoryRepository */
    private $vnpCategoryRepository;

    public function __construct(VnpCategoryRepository $vnpCategoryRepo)
    {
        $this->vnpCategoryRepository = $vnpCategoryRepo;
    }

    /**
     * Display a listing of the VnpCategory.
     * GET|HEAD /vnpCategories
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try{
            $this->vnpCategoryRepository->pushCriteria(new RequestCriteria($request));
            $this->vnpCategoryRepository->pushCriteria(new LimitOffsetCriteria($request));
        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }
        $vnpCategories = $this->vnpCategoryRepository->all();

        return $this->sendResponse($vnpCategories->toArray(), 'Vnp Categories retrieved successfully');
    }

    /**
     * Display the specified VnpCategory.
     * GET|HEAD /vnpCategories/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        /** @var VnpCategory $vnpCategory */
        if (!empty($this->vnpCategoryRepository)) {
            $vnpCategory = $this->vnpCategoryRepository->findWithoutFail($id);
        }

        if (empty($vnpCategory)) {
            return $this->sendError('Vnp Category not found');
        }

        return $this->sendResponse($vnpCategory->toArray(), 'Vnp Category retrieved successfully');
    }
    
    /**
     * Store a newly created Product in storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();
        try {
            $id = $request->get('id', 0);
            if($id > 0){
                $vnpCategory = $this->vnpCategoryRepository->findWithoutFail($id);
                if (empty($vnpCategory)) {
                    return $this->sendError('Product not found');
                }
                $vnpCategory = $this->vnpCategoryRepository->update($input, $id);
            }
            else{
                $vnpCategory = $this->vnpCategoryRepository->create($input);
            }
            if (isset($input['image']) && $input['image']) {
                $vnpCategory->media()->delete();
                $vnpCategory->addMedia($input['image'])->toMediaCollection('image_category');
            }
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($vnpCategory->toArray(), __('lang.saved_successfully', ['operator' => __('lang.category')]));
    }
    
    /**
     * Update the specified Product in storage.
     *
     * @param int $id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        $vnpCategory = $this->vnpCategoryRepository->findWithoutFail($id);

        if (empty($vnpCategory)) {
            return $this->sendError('Product not found');
        }
        $input = $request->all();
        try {
            $vnpCategory = $this->vnpCategoryRepository->update($input, $id);

            if (isset($input['image']) && $input['image']) {
                $vnpCategory->media()->delete();
                $vnpCategory->addMedia($input['image'])->toMediaCollection('image_category');
            }
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($vnpCategory->toArray(), __('lang.updated_successfully', ['operator' => __('lang.category')]));

    }
}
