<?php

namespace App\Http\Controllers\API;

use App\Criteria\IsEnabledCriteria;
use App\Criteria\StatusCriteria;
use App\Models\Banner;
use App\Repositories\BannerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Support\Facades\Response;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class BannerController
 * @package App\Http\Controllers\API
 */

class BannerAPIController extends Controller
{
    /** @var  BannerRepository */
    private $bannerRepository;

    public function __construct(BannerRepository $bannerRepo)
    {
        $this->bannerRepository = $bannerRepo;
    }

    /**
     * Display a listing of the Banner.
     * GET|HEAD /banners
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try{
            $this->bannerRepository->pushCriteria(new RequestCriteria($request));
            $this->bannerRepository->pushCriteria(new LimitOffsetCriteria($request));
            $this->bannerRepository->pushCriteria(new IsEnabledCriteria($request));
        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }
        $banners = $this->bannerRepository->all();

        return $this->sendResponse($banners->toArray(), 'Banners retrieved successfully');
    }

    /**
     * Display the specified Banner.
     * GET|HEAD /banners/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        /** @var Banner $banner */
        if (!empty($this->bannerRepository)) {
            $banner = $this->bannerRepository->findWithoutFail($id);
        }

        if (empty($banner)) {
            return $this->sendError('Banner not found');
        }

        return $this->sendResponse($banner->toArray(), 'Banner retrieved successfully');
    }
    /**
     * Store a newly created VnpProduct in storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();
        try {
            $banner = $this->bannerRepository->create($input);
            if (isset($input['image']) && $input['image']) {
                $banner->media()->delete();
                $banner->addMedia($input['image'])->toMediaCollection('image_banner');
            }
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($banner->load('media')->toArray(),__('lang.saved_successfully', ['operator' => __('lang.product')]));
    }
}
