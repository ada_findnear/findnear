<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\TProvince;
/**
 * Class VoteController
 * @package App\Http\Controllers\API
 */

class MasterAPIController extends Controller
{
    public function local(){
        $province = TProvince::with(['districts','districts.wards'])->get();
        return $this->sendResponse($province->toArray(), 'Retrieve local successfully!');
    }
}
