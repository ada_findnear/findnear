<?php

namespace App\Http\Controllers\API;

use App\Criteria\Shops\NearCriteria;
use App\Criteria\Shops\RecommendedCriteria;
use App\Criteria\StatusCriteria;
use App\Models\Shop;
use App\Repositories\ShopRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\CreateShopAPIRequest;
use App\Http\Requests\API\UpdateShopAPIRequest;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Support\Facades\Response;
use Prettus\Repository\Exceptions\RepositoryException;
use App\Repositories\UploadRepository;
use Illuminate\Support\Facades\Auth;

/**
 * Class ShopController
 * @package App\Http\Controllers\API
 */

class ShopAPIController extends Controller
{
    /** @var  ShopRepository */
    private $shopRepository;

    /**
     * @var UploadRepository
     */
    private $uploadRepository;
    public function __construct(ShopRepository $shopRepo, UploadRepository $uploadRepo)
    {
        $this->shopRepository = $shopRepo;
        $this->uploadRepository = $uploadRepo;
    }

    /**
     * Display a listing of the Shop.
     * GET|HEAD /shops
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $this->shopRepository->pushCriteria(new RequestCriteria($request));
            $this->shopRepository->pushCriteria(new StatusCriteria($request));
            $this->shopRepository->pushCriteria(new RecommendedCriteria($request));
            $this->shopRepository->pushCriteria(new NearCriteria($request));
        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }
        $shops = $this->shopRepository->paginate($request->get('limit', 10));

        return $this->sendResponse($shops->toArray(), 'Shops retrieved successfully');
    }

    /**
     * Display the specified Shop.
     * GET|HEAD /shops/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function myShop()
    {
        /** @var Shop $shop */
        $shop = Auth::user()->shop;

        if (empty($shop)) {
            return $this->sendError('Shop not found');
        }

        return $this->sendResponse($shop->load('products')->toArray(), 'Shop retrieved successfully');
    }


    /**
     * Store a newly created Shop in storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateShopAPIRequest $request)
    {
        $input = $request->all();
        $user = auth()->user();
        try {
            if ($user->shop != null) {
                $shop = $user->shop;
                $shop = $this->shopRepository->update($input, $shop->id);
            } else {
                $input['user_id'] = auth()->id();
                $shop = $this->shopRepository->create($input);
            }
            if (isset($input['image']) && $input['image']) {
                $shop->media()->delete();
                $shop->addMedia($input['image'])->toMediaCollection('image_shop');
            }
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($shop->toArray(), __('lang.saved_successfully', ['operator' => __('lang.shop')]));
    }

    /**
     * Update the specified Shop in storage.
     *
     * @param int $id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, UpdateShopAPIRequest $request)
    {
        $user = Auth::user();
        $shopId = $user->shop->id;
        $shop = $this->shopRepository->findWithoutFail($id);

        if (empty($shop) || $shopId != $id) {
            return $this->sendError('Shop not found');
        }
        $input = $request->all();
        try {
            $shop = $this->shopRepository->update($input, $id);
            if (isset($input['image']) && $input['image']) {
                $shop->media->delete();
                $shop->addMedia($input['image'])->toMediaCollection('image_post');
            }
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($shop->toArray(), __('lang.updated_successfully', ['operator' => __('lang.shop')]));
    }
    public function recommend($id)
    {

        $shop = $this->shopRepository->findWithoutFail($id);

        if (empty($shop) || $shop->id != $id) {
            return $this->sendError('Shop not found');
        }
        try {
            $shop->recommended = 1;
            $shop->save();
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($shop->toArray(), __('lang.updated_successfully', ['operator' => __('lang.shop')]));
    }
}
