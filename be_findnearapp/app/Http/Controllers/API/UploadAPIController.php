<?php
/**
 * File name: UploadAPIController.php
 * Last modified: 2020.05.04 at 09:04:19
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\UploadAPIRequest;
use App\Repositories\UploadRepository;
use Prettus\Validator\Exceptions\ValidatorException;
use Ramsey\Uuid\Uuid;

class UploadAPIController extends Controller
{
    private $uploadRepository;

    /**
     * UploadController constructor.
     * @param UploadRepository $uploadRepository
     */
    public function __construct(UploadRepository $uploadRepository)
    {
        parent::__construct();
        $this->uploadRepository = $uploadRepository;
    }

    /**
     * @param UploadAPIRequest $request
     */
    public function store(UploadAPIRequest $request)
    {
        $input = $request->all();
        try {
            $upload = $this->uploadRepository->create($input);
            $upload->addMedia($input['file'])
                ->withCustomProperties(['uuid' => Uuid::uuid4(), 'user_id' => auth()->id()])
                ->toMediaCollection($input['collection_name']);

            return $this->sendResponse($upload->load('media'), "Upload file successfully");
        } catch (\Exception $e) {
            return $this->sendResponse(false, $e->getMessage());
        }
    }

}
