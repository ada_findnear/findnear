<?php
/**
 * File name: PostAPIController.php
 * Last modified: 2020.10.29 at 17:03:54
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Http\Controllers\API;

use App\Criteria\Comments\CommentsInPostCriteria;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\CreateCommentRequest;
use App\Http\Requests\API\UpdateCommentRequest;
use App\Notifications\CommentPost;
use App\Repositories\CommentRepository;
use App\Repositories\PostRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Exceptions\RepositoryException;
use Prettus\Validator\Exceptions\ValidatorException;

class CommentAPIController extends Controller
{
    private $commentRepository;
    private $postRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CommentRepository $commentRepository, PostRepository $postRepository)
    {
        $this->commentRepository = $commentRepository;
        $this->postRepository = $postRepository;
    }
    
    /**
     * Display a listing of the Comment.
     *
     * @param Request $request
     * @return JsonResponse|mixed
     */
    public function index(Request $request)
    {
        $input = $request->all();
        $post = $this->postRepository->findWithoutFail($input['post_id']);
        if (empty($post)) {
            return $this->sendError('Post not found');
        }
        
        try{
            $this->commentRepository->pushCriteria(new RequestCriteria($request));
            $this->commentRepository->pushCriteria(new LimitOffsetCriteria($request));
            $this->commentRepository->pushCriteria(new CommentsInPostCriteria($request));
        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }
        
        $comments = $this->commentRepository->all();

        return $this->sendResponse($comments->toArray(), 'Post retrieved successfully');
    }

    /**
     * Store a newly created Comment.
     *
     * @param CreateCommentRequest $request
     *
     * @return JsonResponse
     */
    public function store(CreateCommentRequest $request)
    {
        $input = $request->all();
        $user = Auth::user();
        $input['user_id'] = $user->id;
        try {
            $post = $this->postRepository->findWithoutFail($input['post_id']);
            if (empty($post)) {
                return $this->sendError('Post not found');
            }

            $comment = $this->commentRepository->create($input);
            if ($request->has('files')) {
                foreach ($input['files'] as $file)
                {
                    $comment->addMedia($file)->toMediaCollection('image_post_comment');
                }
            }

            if ($post->creator->getAttribute('id') != $user->id) {
                Notification::send([$post->creator], new CommentPost($post, $user, $comment));
            }

        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($comment->load('media')->toArray(),__('lang.saved_successfully',['operator' => __('lang.comment')]));
    }

    /**
     * Update the specified Comment in storage.
     *
     * @param $id
     * @param UpdateCommentRequest $request
     * @return JsonResponse|mixed
     */
    public function update($id, UpdateCommentRequest $request)
    {
        $input = $request->only(['content', 'post_id']);
        $post = $this->postRepository->findWithoutFail($input['post_id']);
        if (empty($post)) {
            return $this->sendError('Post not found');
        }

        $comment = $this->commentRepository->findWithoutFail($id);
        if (!$comment->hasMedia('image_post_comment') && !$request->has('content'))
        {
            return $this->sendResponse([
                'error' => true,
                'code' => 404,
            ], __('validation.required', ['attribute' => 'content']));
        }

        if (empty($comment) || $comment->user_id != Auth::id()) {
            return $this->sendResponse([
                'error' => true,
                'code' => 404,
            ], 'Comment not found');
        }

        try {
            $comment = $this->commentRepository->update($input, $id);
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage(), 401);
        }

        return $this->sendResponse($comment->load('media'), __('lang.updated_successfully', ['operator' => __('lang.comment')]));
    }

    /**
     * Remove the specified Comment.
     *
     * @param  int $id
     *
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $comment = $this->commentRepository->findWithoutFail($id);

        if (empty($comment) || $comment->user_id != Auth::id()) {
            if($comment->post->user_id != Auth::id()){
                return $this->sendError('Comment not found');
            }
        }

        $comment = $this->commentRepository->delete($id);

        return $this->sendResponse($comment,__('lang.deleted_successfully',['operator' => __('lang.comment')]));
    }
}
