<?php

namespace App\Http\Controllers\API;


use App\Models\Vote;
use App\Repositories\VoteRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Support\Facades\Response;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class VoteController
 * @package App\Http\Controllers\API
 */

class VoteAPIController extends Controller
{
    /** @var  VoteRepository */
    private $voteRepository;

    public function __construct(VoteRepository $voteRepo)
    {
        $this->voteRepository = $voteRepo;
    }

    /**
     * Display a listing of the Vote.
     * GET|HEAD /votes
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try{
            $this->voteRepository->pushCriteria(new RequestCriteria($request));
            $this->voteRepository->pushCriteria(new LimitOffsetCriteria($request));
        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }
        $votes = $this->voteRepository->all();

        return $this->sendResponse($votes->toArray(), 'Votes retrieved successfully');
    }

    /**
     * Display the specified Vote.
     * GET|HEAD /votes/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        /** @var Vote $vote */
        if (!empty($this->voteRepository)) {
            $vote = $this->voteRepository->findWithoutFail($id);
        }

        if (empty($vote)) {
            return $this->sendError('Vote not found');
        }

        return $this->sendResponse($vote->toArray(), 'Vote retrieved successfully');
    }
    
    /**
     * Store a newly created VnpProduct in storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateVnpProductRequest $request)
    {
        $input = $request->all();
        try {
            $input['user_id'] = auth()->id();
            $vnpProduct = $this->vnpProductRepository->create($input);
            if (isset($input['image']) && $input['image']) {
                $vnpProduct->media()->delete();
                $vnpProduct->addMedia($input['image'])->toMediaCollection('image_post');
            }
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($vnpProduct->toArray(),__('lang.saved_successfully', ['operator' => __('lang.product')]));
    }
}
