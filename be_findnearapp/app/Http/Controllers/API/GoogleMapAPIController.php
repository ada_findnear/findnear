<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\DirectionsRequest;
use GuzzleHttp\Client;

/**
 * Class CartController
 * @package App\Http\Controllers\API
 */

class GoogleMapAPIController extends Controller
{
    public function getDirectionsPoints(DirectionsRequest $request)
    {
        try {
            $settings = setting()->all();
            $settings = array_intersect_key($settings, [
                    'google_maps_key' => '',
                ]
            );

            $client = new Client();
            $response = $client->get("https://maps.googleapis.com/maps/api/directions/json", [
                "query" => [
                    'origin' => $request->get('origin'),
                    'destination' => $request->get('destination'),
                    'key' => $settings['google_maps_key'],
                ],
            ]);

            $data = json_decode($response->getBody());
            if ($data->status === "OK") {
                return $this->sendResponse($data, "Get direction successfully");
            }

            return $this->sendError($data, 400);
        } catch (\Exception $exception) {
            return $this->sendError($exception->getMessage(), $exception->getCode());
        }

    }
}
