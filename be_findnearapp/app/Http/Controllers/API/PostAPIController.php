<?php
/**
 * File name: PostAPIController.php
 * Last modified: 2020.10.29 at 17:03:54
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Http\Controllers\API;

use App\Criteria\Posts\MyPostsCriteria;
use App\Criteria\Posts\PostsIndexCriteria;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\CreatePostRequest;
use App\Http\Requests\API\UpdatePostRequest;
use App\Models\Post;
use App\Models\User;
use App\Notifications\LikePost;
use App\Repositories\PostRepository;
use App\Repositories\UserRepository;
use App\Traits\Reportable\ReportableController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Exceptions\RepositoryException;
use Prettus\Validator\Exceptions\ValidatorException;
use FFMpeg\FFMpeg;
use FFMpeg\Coordinate\TimeCode;
use Storage;

class PostAPIController extends Controller
{
    private $repository;
    private $userRepository;
    use ReportableController;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PostRepository $repository, UserRepository $userRepository)
    {
        $this->repository = $repository;
        $this->userRepository = $userRepository;
    }
    
    /**
     * Display a listing of the Post.
     *
     * @param Request $request
     * @return JsonResponse|mixed
     */
    public function index(Request $request)
    {
        try{
            $this->repository->pushCriteria(new RequestCriteria($request));
            $this->repository->pushCriteria(new LimitOffsetCriteria($request));
            $this->repository->pushCriteria(new PostsIndexCriteria($request));
        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }
        
        $post = $this->repository->all();
    
        return $this->sendResponse($post->toArray(), 'Post retrieved successfully');
    }
    
    /**
     * Get my posts.
     *
     * @param Request $request
     * @return JsonResponse|mixed
     */
    public function myPosts(Request $request)
    {
        try{
            $this->repository->pushCriteria(new RequestCriteria($request));
            $this->repository->pushCriteria(new LimitOffsetCriteria($request));
            $this->repository->pushCriteria(new MyPostsCriteria($request, $this->userRepository));
        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }
        
        $post = $this->repository->all();
        
        return $this->sendResponse($post->toArray(), 'Post retrieved successfully');
    }
    
    /**
     * Store a newly created post.
     *
     * @param CreatePostRequest $request
     *
     * @return JsonResponse
     */
    public function store(CreatePostRequest $request)
    {
        $input = $request->all();
        $input['user_id'] = Auth::id();
        try {
            $post = $this->repository->create($input);
            if ($request->has('files')) {
                foreach ($input['files'] as $file)
                {
                    $post->addMedia($file)->toMediaCollection('image_post');
                }
            }
            // $medias = $post->media;
            // foreach($medias as $k => $media){
            //     $type = $media->mime_type;
            //     if(Str::startsWith($type,'video')){
            //         $storage_path = 'public/' . $post->id . '/' . $media->file_name;
            //         if(Storage::exists($storage_path)){
            //             //create thumbnail
            //             $path = 'app/' . $storage_path;
            //             $imgpth = 'app/public/' . $post->id . '/thumbnail.jpg';
            //             $ffmpeg = FFMpeg::create();
            //             $video = $ffmpeg->open(storage_path($path));
            //             $video
            //                 ->filters()
            //                 ->synchronize();
            //             $video
            //                 ->frame(TimeCode::fromSeconds(10))
            //                 ->save(storage_path($imgpth));
            //             dd($imgpth);
            //         }
            //         else{
            //             dd($storage_path);
            //         }
            //     }
            // }
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }
        return $this->sendResponse($post->load('media')->loadCount('likers')->toArray(),__('lang.saved_successfully',['operator' => __('lang.post')]));
    }
    
    /**
     * Update the specified Post in storage.
     *
     * @param $id
     * @param UpdatePostRequest $request
     * @return JsonResponse|mixed
     */
    public function update($id, UpdatePostRequest $request)
    {
        $post = $this->repository->findWithoutFail($id);
        $input = $request->only(['content', 'viewer']);
        if (!$post->hasMedia('image_post') && !$request->has('content'))
        {
            return $this->sendResponse([
                'error' => true,
                'code' => 404,
            ], __('validation.required', ['attribute' => 'content']));
        }
        
        if (empty($post) || $post->user_id != Auth::id()) {
            return $this->sendResponse([
                'error' => true,
                'code' => 404,
            ], 'Post not found');
        }
        
        try {
            $post = $this->repository->update($input, $id);
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage(), 401);
        }
        
        return $this->sendResponse($post->load('media')->loadCount('likers'), __('lang.updated_successfully', ['operator' => __('lang.post')]));
    }
    
    /**
     * Display the specified Post.
     * GET|HEAD /categories/{id}
     *
     * @param  int $id
     *
     * @return JsonResponse
     */
    public function show($id)
    {
        /** @var Post $post */
        if (!empty($this->repository)) {
            $post = $this->repository
                ->withCount('comments')
                ->withCount('likers')
                ->with('media')
                ->findWithoutFail($id);
        }
        
        if (empty($post)) {
            return $this->sendError('Post not found');
        }
        
        return $this->sendResponse($post->toArray(), 'Post retrieved successfully');
    }
    
    /**
     * Remove the specified Post.
     *
     * @param  int $id
     *
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $post = $this->repository->findWithoutFail($id);
        
        if (empty($post) || $post->user_id != Auth::id()) {
            return $this->sendError('Post not found');
        }
        
        $post = $this->repository->delete($id);
        
        return $this->sendResponse($post,__('lang.deleted_successfully',['operator' => __('lang.post')]));
    }

    /**
     * Toggle like post with current auth user
     *
     * @param $id
     * @return JsonResponse|mixed
     */
    public function toggleLike($id)
    {
        /** @var Post $post */
        $post = $this->repository->findWithoutFail($id);

        if (empty($post)) {
            return $this->sendError('Post not found');
        }

        try {
            $user = Auth::user();
            $user->toggleLike($post);
            if ($post->isLikedBy($user) && $post->creator->getAttribute('id') != $user->id) {
                Notification::send([$post->creator], new LikePost($post, $user));
            }
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($post->toArray(), 'Toggle like post successfully');
    }

    public function listUserLike($id)
    {
        /** @var Post $post */
        $post = $this->repository->findWithoutFail($id);

        if (empty($post)) {
            return $this->sendError('Post not found');
        }

        try {
            $likers = $post->likers()->paginate();
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($likers->toArray(), 'Toggle like post successfully');
    }
}
