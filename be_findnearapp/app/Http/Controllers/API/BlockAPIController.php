<?php

namespace App\Http\Controllers\API;


use App\Models\Block;
use App\Repositories\BlockRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Support\Facades\Response;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class BlockController
 * @package App\Http\Controllers\API
 */

class BlockAPIController extends Controller
{
    /** @var  BlockRepository */
    private $blockRepository;

    public function __construct(BlockRepository $blockRepo)
    {
        $this->blockRepository = $blockRepo;
    }

    /**
     * Display a listing of the Block.
     * GET|HEAD /blocks
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try{
            $this->blockRepository->pushCriteria(new RequestCriteria($request));
            $this->blockRepository->pushCriteria(new LimitOffsetCriteria($request));
        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }
        $blocks = $this->blockRepository->all();

        return $this->sendResponse($blocks->toArray(), 'Blocks retrieved successfully');
    }

    /**
     * Display the specified Block.
     * GET|HEAD /blocks/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        /** @var Block $block */
        if (!empty($this->blockRepository)) {
            $block = $this->blockRepository->findWithoutFail($id);
        }

        if (empty($block)) {
            return $this->sendError('Block not found');
        }

        return $this->sendResponse($block->toArray(), 'Block retrieved successfully');
    }
}
