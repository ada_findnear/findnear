<?php

namespace App\Http\Controllers\API;


use App\Models\PointHistory;
use App\Repositories\PointHistoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Support\Facades\Response;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class PointHistoryController
 * @package App\Http\Controllers\API
 */

class PointHistoryAPIController extends Controller
{
    /** @var  PointHistoryRepository */
    private $pointHistoryRepository;

    public function __construct(PointHistoryRepository $pointHistoryRepo)
    {
        $this->pointHistoryRepository = $pointHistoryRepo;
    }

    /**
     * Display a listing of the PointHistory.
     * GET|HEAD /pointHistories
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try{
            $this->pointHistoryRepository->pushCriteria(new RequestCriteria($request));
            $this->pointHistoryRepository->pushCriteria(new LimitOffsetCriteria($request));
        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }
        $pointHistories = $this->pointHistoryRepository->all();

        return $this->sendResponse($pointHistories->toArray(), 'Point Histories retrieved successfully');
    }

    /**
     * Display the specified PointHistory.
     * GET|HEAD /pointHistories/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        /** @var PointHistory $pointHistory */
        if (!empty($this->pointHistoryRepository)) {
            $pointHistory = $this->pointHistoryRepository->findWithoutFail($id);
        }

        if (empty($pointHistory)) {
            return $this->sendError('Point History not found');
        }

        return $this->sendResponse($pointHistory->toArray(), 'Point History retrieved successfully');
    }
    /**
     * EarnPoint.
     * GET|HEAD /pointHistories/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function earn(Request $request)
    {
        /** @var PointHistory $pointHistory */
        $user = Auth::user();
        if (empty($user)) {
            return $this->sendError('User not found');
        }
        $input = $request->only(['game_id','point', 'description']);
        $user->point_history()->create($input);
        $user->current_point += $input['point'];
        $user->total_point += $input['point'];

        return $this->sendResponse($user->toArray(), 'Point History retrieved successfully');
    }
}
