<?php

namespace App\Http\Controllers\API;


use App\Models\Game;
use App\Repositories\GameRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Support\Facades\Response;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class GameController
 * @package App\Http\Controllers\API
 */

class GameAPIController extends Controller
{
    /** @var  GameRepository */
    private $gameRepository;

    public function __construct(GameRepository $gameRepo)
    {
        $this->gameRepository = $gameRepo;
    }

    /**
     * Display a listing of the Game.
     * GET|HEAD /games
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try{
            $this->gameRepository->pushCriteria(new RequestCriteria($request));
            $this->gameRepository->pushCriteria(new LimitOffsetCriteria($request));
        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }
        $games = $this->gameRepository->all();

        return $this->sendResponse($games->toArray(), 'Games retrieved successfully');
    }

    /**
     * Display the specified Game.
     * GET|HEAD /games/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        /** @var Game $game */
        if (!empty($this->gameRepository)) {
            $game = $this->gameRepository->findWithoutFail($id);
        }

        if (empty($game)) {
            return $this->sendError('Game not found');
        }

        return $this->sendResponse($game->toArray(), 'Game retrieved successfully');
    }
}
