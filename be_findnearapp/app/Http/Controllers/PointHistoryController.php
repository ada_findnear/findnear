<?php

namespace App\Http\Controllers;

use App\DataTables\PointHistoryDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePointHistoryRequest;
use App\Http\Requests\UpdatePointHistoryRequest;
use App\Repositories\PointHistoryRepository;
use App\Repositories\CustomFieldRepository;

use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Prettus\Validator\Exceptions\ValidatorException;

class PointHistoryController extends Controller
{
    /** @var  PointHistoryRepository */
    private $pointHistoryRepository;

    /**
     * @var CustomFieldRepository
     */
    private $customFieldRepository;

    

    public function __construct(PointHistoryRepository $pointHistoryRepo, CustomFieldRepository $customFieldRepo )
    {
        parent::__construct();
        $this->pointHistoryRepository = $pointHistoryRepo;
        $this->customFieldRepository = $customFieldRepo;
        
    }

    /**
     * Display a listing of the PointHistory.
     *
     * @param PointHistoryDataTable $pointHistoryDataTable
     * @return Response
     */
    public function index(PointHistoryDataTable $pointHistoryDataTable)
    {
        return $pointHistoryDataTable->render('point_histories.index');
    }

    /**
     * Show the form for creating a new PointHistory.
     *
     * @return Response
     */
    public function create()
    {
        
        
        $hasCustomField = in_array($this->pointHistoryRepository->model(),setting('custom_field_models',[]));
            if($hasCustomField){
                $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->pointHistoryRepository->model());
                $html = generateCustomField($customFields);
            }
        return view('point_histories.create')->with("customFields", isset($html) ? $html : false);
    }

    /**
     * Store a newly created PointHistory in storage.
     *
     * @param CreatePointHistoryRequest $request
     *
     * @return Response
     */
    public function store(CreatePointHistoryRequest $request)
    {
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->pointHistoryRepository->model());
        try {
            $pointHistory = $this->pointHistoryRepository->create($input);
            $pointHistory->customFieldsValues()->createMany(getCustomFieldsValues($customFields,$request));
            
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.saved_successfully',['operator' => __('lang.point_history')]));

        return redirect(route('pointHistories.index'));
    }

    /**
     * Display the specified PointHistory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pointHistory = $this->pointHistoryRepository->findWithoutFail($id);

        if (empty($pointHistory)) {
            Flash::error('Point History not found');

            return redirect(route('pointHistories.index'));
        }

        return view('point_histories.show')->with('pointHistory', $pointHistory);
    }

    /**
     * Show the form for editing the specified PointHistory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pointHistory = $this->pointHistoryRepository->findWithoutFail($id);
        
        

        if (empty($pointHistory)) {
            Flash::error(__('lang.not_found',['operator' => __('lang.point_history')]));

            return redirect(route('pointHistories.index'));
        }
        $customFieldsValues = $pointHistory->customFieldsValues()->with('customField')->get();
        $customFields =  $this->customFieldRepository->findByField('custom_field_model', $this->pointHistoryRepository->model());
        $hasCustomField = in_array($this->pointHistoryRepository->model(),setting('custom_field_models',[]));
        if($hasCustomField) {
            $html = generateCustomField($customFields, $customFieldsValues);
        }

        return view('point_histories.edit')->with('pointHistory', $pointHistory)->with("customFields", isset($html) ? $html : false);
    }

    /**
     * Update the specified PointHistory in storage.
     *
     * @param  int              $id
     * @param UpdatePointHistoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePointHistoryRequest $request)
    {
        $pointHistory = $this->pointHistoryRepository->findWithoutFail($id);

        if (empty($pointHistory)) {
            Flash::error('Point History not found');
            return redirect(route('pointHistories.index'));
        }
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->pointHistoryRepository->model());
        try {
            $pointHistory = $this->pointHistoryRepository->update($input, $id);
            
            
            foreach (getCustomFieldsValues($customFields, $request) as $value){
                $pointHistory->customFieldsValues()
                    ->updateOrCreate(['custom_field_id'=>$value['custom_field_id']],$value);
            }
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.updated_successfully',['operator' => __('lang.point_history')]));

        return redirect(route('pointHistories.index'));
    }

    /**
     * Remove the specified PointHistory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pointHistory = $this->pointHistoryRepository->findWithoutFail($id);

        if (empty($pointHistory)) {
            Flash::error('Point History not found');

            return redirect(route('pointHistories.index'));
        }

        $this->pointHistoryRepository->delete($id);

        Flash::success(__('lang.deleted_successfully',['operator' => __('lang.point_history')]));

        return redirect(route('pointHistories.index'));
    }

        /**
     * Remove Media of PointHistory
     * @param Request $request
     */
    public function removeMedia(Request $request)
    {
        $input = $request->all();
        $pointHistory = $this->pointHistoryRepository->findWithoutFail($input['id']);
        try {
            if($pointHistory->hasMedia($input['collection'])){
                $pointHistory->getFirstMedia($input['collection'])->delete();
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
