<?php

namespace App\Http\Controllers;

use App\DataTables\VnpProductDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateVnpProductRequest;
use App\Http\Requests\UpdateVnpProductRequest;
use App\Repositories\VnpProductRepository;
use App\Repositories\CustomFieldRepository;

use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Prettus\Validator\Exceptions\ValidatorException;

class VnpProductController extends Controller
{
    /** @var  VnpProductRepository */
    private $vnpProductRepository;

    /**
     * @var CustomFieldRepository
     */
    private $customFieldRepository;

    

    public function __construct(VnpProductRepository $vnpProductRepo, CustomFieldRepository $customFieldRepo )
    {
        parent::__construct();
        $this->vnpProductRepository = $vnpProductRepo;
        $this->customFieldRepository = $customFieldRepo;
        
    }

    /**
     * Display a listing of the VnpProduct.
     *
     * @param VnpProductDataTable $vnpProductDataTable
     * @return Response
     */
    public function index(VnpProductDataTable $vnpProductDataTable)
    {
        return $vnpProductDataTable->render('vnp_products.index');
    }

    /**
     * Show the form for creating a new VnpProduct.
     *
     * @return Response
     */
    public function create()
    {
        
        
        $hasCustomField = in_array($this->vnpProductRepository->model(),setting('custom_field_models',[]));
            if($hasCustomField){
                $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->vnpProductRepository->model());
                $html = generateCustomField($customFields);
            }
        return view('vnp_products.create')->with("customFields", isset($html) ? $html : false);
    }

    /**
     * Store a newly created VnpProduct in storage.
     *
     * @param CreateVnpProductRequest $request
     *
     * @return Response
     */
    public function store(CreateVnpProductRequest $request)
    {
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->vnpProductRepository->model());
        try {
            $vnpProduct = $this->vnpProductRepository->create($input);
            $vnpProduct->customFieldsValues()->createMany(getCustomFieldsValues($customFields,$request));
            
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.saved_successfully',['operator' => __('lang.vnp_product')]));

        return redirect(route('vnpProducts.index'));
    }

    /**
     * Display the specified VnpProduct.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $vnpProduct = $this->vnpProductRepository->findWithoutFail($id);

        if (empty($vnpProduct)) {
            Flash::error('Vnp Product not found');

            return redirect(route('vnpProducts.index'));
        }

        return view('vnp_products.show')->with('vnpProduct', $vnpProduct);
    }

    /**
     * Show the form for editing the specified VnpProduct.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $vnpProduct = $this->vnpProductRepository->findWithoutFail($id);
        
        

        if (empty($vnpProduct)) {
            Flash::error(__('lang.not_found',['operator' => __('lang.vnp_product')]));

            return redirect(route('vnpProducts.index'));
        }
        $customFieldsValues = $vnpProduct->customFieldsValues()->with('customField')->get();
        $customFields =  $this->customFieldRepository->findByField('custom_field_model', $this->vnpProductRepository->model());
        $hasCustomField = in_array($this->vnpProductRepository->model(),setting('custom_field_models',[]));
        if($hasCustomField) {
            $html = generateCustomField($customFields, $customFieldsValues);
        }

        return view('vnp_products.edit')->with('vnpProduct', $vnpProduct)->with("customFields", isset($html) ? $html : false);
    }

    /**
     * Update the specified VnpProduct in storage.
     *
     * @param  int              $id
     * @param UpdateVnpProductRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVnpProductRequest $request)
    {
        $vnpProduct = $this->vnpProductRepository->findWithoutFail($id);

        if (empty($vnpProduct)) {
            Flash::error('Vnp Product not found');
            return redirect(route('vnpProducts.index'));
        }
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->vnpProductRepository->model());
        try {
            $vnpProduct = $this->vnpProductRepository->update($input, $id);
            
            
            foreach (getCustomFieldsValues($customFields, $request) as $value){
                $vnpProduct->customFieldsValues()
                    ->updateOrCreate(['custom_field_id'=>$value['custom_field_id']],$value);
            }
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.updated_successfully',['operator' => __('lang.vnp_product')]));

        return redirect(route('vnpProducts.index'));
    }

    /**
     * Remove the specified VnpProduct from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $vnpProduct = $this->vnpProductRepository->findWithoutFail($id);

        if (empty($vnpProduct)) {
            Flash::error('Vnp Product not found');

            return redirect(route('vnpProducts.index'));
        }

        $this->vnpProductRepository->delete($id);

        Flash::success(__('lang.deleted_successfully',['operator' => __('lang.vnp_product')]));

        return redirect(route('vnpProducts.index'));
    }

        /**
     * Remove Media of VnpProduct
     * @param Request $request
     */
    public function removeMedia(Request $request)
    {
        $input = $request->all();
        $vnpProduct = $this->vnpProductRepository->findWithoutFail($input['id']);
        try {
            if($vnpProduct->hasMedia($input['collection'])){
                $vnpProduct->getFirstMedia($input['collection'])->delete();
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
