<?php

namespace App\Http\Controllers;

use App\DataTables\GameDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateGameRequest;
use App\Http\Requests\UpdateGameRequest;
use App\Repositories\GameRepository;
use App\Repositories\CustomFieldRepository;

use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Prettus\Validator\Exceptions\ValidatorException;

class GameController extends Controller
{
    /** @var  GameRepository */
    private $gameRepository;

    /**
     * @var CustomFieldRepository
     */
    private $customFieldRepository;

    

    public function __construct(GameRepository $gameRepo, CustomFieldRepository $customFieldRepo )
    {
        parent::__construct();
        $this->gameRepository = $gameRepo;
        $this->customFieldRepository = $customFieldRepo;
        
    }

    /**
     * Display a listing of the Game.
     *
     * @param GameDataTable $gameDataTable
     * @return Response
     */
    public function index(GameDataTable $gameDataTable)
    {
        return $gameDataTable->render('games.index');
    }

    /**
     * Show the form for creating a new Game.
     *
     * @return Response
     */
    public function create()
    {
        
        
        $hasCustomField = in_array($this->gameRepository->model(),setting('custom_field_models',[]));
            if($hasCustomField){
                $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->gameRepository->model());
                $html = generateCustomField($customFields);
            }
        return view('games.create')->with("customFields", isset($html) ? $html : false);
    }

    /**
     * Store a newly created Game in storage.
     *
     * @param CreateGameRequest $request
     *
     * @return Response
     */
    public function store(CreateGameRequest $request)
    {
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->gameRepository->model());
        try {
            $game = $this->gameRepository->create($input);
            $game->customFieldsValues()->createMany(getCustomFieldsValues($customFields,$request));
            
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.saved_successfully',['operator' => __('lang.game')]));

        return redirect(route('games.index'));
    }

    /**
     * Display the specified Game.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $game = $this->gameRepository->findWithoutFail($id);

        if (empty($game)) {
            Flash::error('Game not found');

            return redirect(route('games.index'));
        }

        return view('games.show')->with('game', $game);
    }

    /**
     * Show the form for editing the specified Game.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $game = $this->gameRepository->findWithoutFail($id);
        
        

        if (empty($game)) {
            Flash::error(__('lang.not_found',['operator' => __('lang.game')]));

            return redirect(route('games.index'));
        }
        $customFieldsValues = $game->customFieldsValues()->with('customField')->get();
        $customFields =  $this->customFieldRepository->findByField('custom_field_model', $this->gameRepository->model());
        $hasCustomField = in_array($this->gameRepository->model(),setting('custom_field_models',[]));
        if($hasCustomField) {
            $html = generateCustomField($customFields, $customFieldsValues);
        }

        return view('games.edit')->with('game', $game)->with("customFields", isset($html) ? $html : false);
    }

    /**
     * Update the specified Game in storage.
     *
     * @param  int              $id
     * @param UpdateGameRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGameRequest $request)
    {
        $game = $this->gameRepository->findWithoutFail($id);

        if (empty($game)) {
            Flash::error('Game not found');
            return redirect(route('games.index'));
        }
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->gameRepository->model());
        try {
            $game = $this->gameRepository->update($input, $id);
            
            
            foreach (getCustomFieldsValues($customFields, $request) as $value){
                $game->customFieldsValues()
                    ->updateOrCreate(['custom_field_id'=>$value['custom_field_id']],$value);
            }
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.updated_successfully',['operator' => __('lang.game')]));

        return redirect(route('games.index'));
    }

    /**
     * Remove the specified Game from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $game = $this->gameRepository->findWithoutFail($id);

        if (empty($game)) {
            Flash::error('Game not found');

            return redirect(route('games.index'));
        }

        $this->gameRepository->delete($id);

        Flash::success(__('lang.deleted_successfully',['operator' => __('lang.game')]));

        return redirect(route('games.index'));
    }

        /**
     * Remove Media of Game
     * @param Request $request
     */
    public function removeMedia(Request $request)
    {
        $input = $request->all();
        $game = $this->gameRepository->findWithoutFail($input['id']);
        try {
            if($game->hasMedia($input['collection'])){
                $game->getFirstMedia($input['collection'])->delete();
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
