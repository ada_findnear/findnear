<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class UpdateUserRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:' . MAX_FILE_SIZE,
            'cover' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:' . MAX_FILE_SIZE,
            'gender' => 'nullable|in:Male,Female,Other',
        ];
    }
}
