<div class='btn-group btn-group-sm'>
  @can('vnpProducts.show')
  <a data-toggle="tooltip" data-placement="bottom" title="{{trans('lang.view_details')}}" href="{{ route('vnpProducts.show', $id) }}" class='btn btn-link'>
    <i class="fa fa-eye"></i>
  </a>
  @endcan

  @can('vnpProducts.edit')
  <a data-toggle="tooltip" data-placement="bottom" title="{{trans('lang.vnp_product_edit')}}" href="{{ route('vnpProducts.edit', $id) }}" class='btn btn-link'>
    <i class="fa fa-edit"></i>
  </a>
  @endcan

  @can('vnpProducts.destroy')
{!! Form::open(['route' => ['vnpProducts.destroy', $id], 'method' => 'delete']) !!}
  {!! Form::button('<i class="fa fa-trash"></i>', [
  'type' => 'submit',
  'class' => 'btn btn-link text-danger',
  'onclick' => "return confirm('Are you sure?')"
  ]) !!}
{!! Form::close() !!}
  @endcan
</div>
