@if($customFields)
<h5 class="col-12 pb-4">{!! trans('lang.main_fields') !!}</h5>
@endif
<div class="d-flex flex-column col-sm-12 col-md-6">
<!-- Name Field -->
<div class="form-group row ">
  {!! Form::label('name', trans("lang.vnp_product_name"), ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    {!! Form::text('name', null,  ['class' => 'form-control','placeholder'=>  trans("lang.vnp_product_name_placeholder")]) !!}
    <div class="form-text text-muted">
      {{ trans("lang.vnp_product_name_help") }}
    </div>
  </div>
</div>

<!-- User Id Field -->
<div class="form-group row ">
  {!! Form::label('user_id', trans("lang.vnp_product_user_id"), ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    {!! Form::number('user_id', null,  ['class' => 'form-control','placeholder'=>  trans("lang.vnp_product_user_id_placeholder")]) !!}
    <div class="form-text text-muted">
      {{ trans("lang.vnp_product_user_id_help") }}
    </div>
  </div>
</div>

<!-- Shop Id Field -->
<div class="form-group row ">
  {!! Form::label('shop_id', trans("lang.vnp_product_shop_id"), ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    {!! Form::number('shop_id', null,  ['class' => 'form-control','placeholder'=>  trans("lang.vnp_product_shop_id_placeholder")]) !!}
    <div class="form-text text-muted">
      {{ trans("lang.vnp_product_shop_id_help") }}
    </div>
  </div>
</div>
</div>
<div class="d-flex flex-column col-sm-12 col-md-6">

<!-- Category Id Field -->
<div class="form-group row ">
  {!! Form::label('category_id', trans("lang.vnp_product_category_id"), ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    {!! Form::number('category_id', null,  ['class' => 'form-control','placeholder'=>  trans("lang.vnp_product_category_id_placeholder")]) !!}
    <div class="form-text text-muted">
      {{ trans("lang.vnp_product_category_id_help") }}
    </div>
  </div>
</div>

<!-- Price Field -->
<div class="form-group row ">
  {!! Form::label('price', trans("lang.vnp_product_price"), ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    {!! Form::number('price', null,  ['class' => 'form-control','placeholder'=>  trans("lang.vnp_product_price_placeholder")]) !!}
    <div class="form-text text-muted">
      {{ trans("lang.vnp_product_price_help") }}
    </div>
  </div>
</div>

<!-- Status Field -->
<div class="form-group row ">
  {!! Form::label('status', trans("lang.vnp_product_status"), ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    {!! Form::text('status', null,  ['class' => 'form-control','placeholder'=>  trans("lang.vnp_product_status_placeholder")]) !!}
    <div class="form-text text-muted">
      {{ trans("lang.vnp_product_status_help") }}
    </div>
  </div>
</div>
</div>
@if($customFields)
<div class="clearfix"></div>
<div class="col-12 custom-field-container">
  <h5 class="col-12 pb-4">{!! trans('lang.custom_field_plural') !!}</h5>
  {!! $customFields !!}
</div>
@endif
<!-- Submit Field -->
<div class="form-group col-12 text-right">
  <button type="submit" class="btn btn-{{setting('theme_color')}}" ><i class="fa fa-save"></i> {{trans('lang.save')}} {{trans('lang.vnp_product')}}</button>
  <a href="{!! route('vnpProducts.index') !!}" class="btn btn-default"><i class="fa fa-undo"></i> {{trans('lang.cancel')}}</a>
</div>
