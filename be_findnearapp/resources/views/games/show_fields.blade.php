<!-- Id Field -->
<div class="form-group row col-6">
  {!! Form::label('id', 'Id:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $game->id !!}</p>
  </div>
</div>

<!-- Name Field -->
<div class="form-group row col-6">
  {!! Form::label('name', 'Name:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $game->name !!}</p>
  </div>
</div>

<!-- Description Field -->
<div class="form-group row col-6">
  {!! Form::label('description', 'Description:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $game->description !!}</p>
  </div>
</div>

<!-- Total Player Field -->
<div class="form-group row col-6">
  {!! Form::label('total_player', 'Total Player:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $game->total_player !!}</p>
  </div>
</div>

<!-- Spent Points Field -->
<div class="form-group row col-6">
  {!! Form::label('spent_points', 'Spent Points:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $game->spent_points !!}</p>
  </div>
</div>

<!-- Created At Field -->
<div class="form-group row col-6">
  {!! Form::label('created_at', 'Created At:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $game->created_at !!}</p>
  </div>
</div>

<!-- Updated At Field -->
<div class="form-group row col-6">
  {!! Form::label('updated_at', 'Updated At:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $game->updated_at !!}</p>
  </div>
</div>

