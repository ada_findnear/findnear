<!-- Id Field -->
<div class="form-group row col-6">
  {!! Form::label('id', 'Id:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $vnpCategory->id !!}</p>
  </div>
</div>

<!-- Title Field -->
<div class="form-group row col-6">
  {!! Form::label('title', 'Title:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $vnpCategory->title !!}</p>
  </div>
</div>

<!-- Parent Id Field -->
<div class="form-group row col-6">
  {!! Form::label('parent_id', 'Parent Id:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $vnpCategory->parent_id !!}</p>
  </div>
</div>

<!-- Description Field -->
<div class="form-group row col-6">
  {!! Form::label('description', 'Description:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $vnpCategory->description !!}</p>
  </div>
</div>

<!-- Status Field -->
<div class="form-group row col-6">
  {!! Form::label('status', 'Status:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $vnpCategory->status !!}</p>
  </div>
</div>

<!-- Created At Field -->
<div class="form-group row col-6">
  {!! Form::label('created_at', 'Created At:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $vnpCategory->created_at !!}</p>
  </div>
</div>

<!-- Updated At Field -->
<div class="form-group row col-6">
  {!! Form::label('updated_at', 'Updated At:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $vnpCategory->updated_at !!}</p>
  </div>
</div>

