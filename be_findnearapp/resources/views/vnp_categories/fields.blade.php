@if($customFields)
<h5 class="col-12 pb-4">{!! trans('lang.main_fields') !!}</h5>
@endif
<div class="d-flex flex-column col-sm-12 col-md-6">
<!-- Title Field -->
<div class="form-group row ">
  {!! Form::label('title', trans("lang.vnp_category_title"), ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    {!! Form::text('title', null,  ['class' => 'form-control','placeholder'=>  trans("lang.vnp_category_title_placeholder")]) !!}
    <div class="form-text text-muted">
      {{ trans("lang.vnp_category_title_help") }}
    </div>
  </div>
</div>

<!-- Parent Id Field -->
<div class="form-group row ">
  {!! Form::label('parent_id', trans("lang.vnp_category_parent_id"), ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    {!! Form::number('parent_id', null,  ['class' => 'form-control','placeholder'=>  trans("lang.vnp_category_parent_id_placeholder")]) !!}
    <div class="form-text text-muted">
      {{ trans("lang.vnp_category_parent_id_help") }}
    </div>
  </div>
</div>
</div>
<div class="d-flex flex-column col-sm-12 col-md-6">

<!-- Description Field -->
<div class="form-group row ">
  {!! Form::label('description', trans("lang.vnp_category_description"), ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    {!! Form::text('description', null,  ['class' => 'form-control','placeholder'=>  trans("lang.vnp_category_description_placeholder")]) !!}
    <div class="form-text text-muted">
      {{ trans("lang.vnp_category_description_help") }}
    </div>
  </div>
</div>

<!-- Status Field -->
<div class="form-group row ">
  {!! Form::label('status', trans("lang.vnp_category_status"), ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    {!! Form::text('status', null,  ['class' => 'form-control','placeholder'=>  trans("lang.vnp_category_status_placeholder")]) !!}
    <div class="form-text text-muted">
      {{ trans("lang.vnp_category_status_help") }}
    </div>
  </div>
</div>
</div>
@if($customFields)
<div class="clearfix"></div>
<div class="col-12 custom-field-container">
  <h5 class="col-12 pb-4">{!! trans('lang.custom_field_plural') !!}</h5>
  {!! $customFields !!}
</div>
@endif
<!-- Submit Field -->
<div class="form-group col-12 text-right">
  <button type="submit" class="btn btn-{{setting('theme_color')}}" ><i class="fa fa-save"></i> {{trans('lang.save')}} {{trans('lang.vnp_category')}}</button>
  <a href="{!! route('vnpCategories.index') !!}" class="btn btn-default"><i class="fa fa-undo"></i> {{trans('lang.cancel')}}</a>
</div>
