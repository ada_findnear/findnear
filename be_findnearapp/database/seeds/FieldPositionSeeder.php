<?php

use App\Models\Field;
use Illuminate\Database\Seeder;

class FieldPositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Field::where('name', 'Nhà hàng')->update(['position' => 1]);
        Field::where('name', 'Khách sạn')->update(['position' => 2]);
        // Field::where('name','Cửa hàng')->update(['position', 3]);//
        Field::where('name', 'Siêu thị')->update(['position' => 4]);
        Field::where('name', 'Quán cafe')->update(['position' => 5]);
        // Field::where('name','Trạm y tế')->update(['position', 6]);//
        Field::where('name', 'Nhà thuốc')->update(['position' => 7]);
        Field::insert(
            [
                [
                    'name' => 'Cửa hàng',
                    'description' => 'Cửa hàng',
                    'position' => 3
                ],
                [
                    'name' => 'Cơ sở y tế',
                    'description' => 'Cơ sở y tế',
                    'position' => 6
                ]
            ]
        );
    }
}
