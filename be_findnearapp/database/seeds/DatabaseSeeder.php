<?php
/*
 * File name: DatabaseSeeder.php
 * Last modified: 2021.03.20 at 13:16:14
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2021
 *
 */

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(AppSettingsTableSeeder::class);
        $this->call(CartOptionsTableSeeder::class);
        $this->call(CartsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(CouponPermission::class);
        $this->call(CreateMarketPermission::class);
        $this->call(CurrenciesTableSeeder::class);
        $this->call(CustomFieldsTableSeeder::class);
        $this->call(CustomFieldValuesTableSeeder::class);
        $this->call(DeliveryAddressesTableSeeder::class);
        $this->call(DemoPermissionsPermissionsTableSeeder::class);
        $this->call(MarketsTableSeeder::class);
        $this->call(DriverMarketsTableSeeder::class);
        $this->call(DriversPayoutsTableSeeder::class);
        $this->call(DriversTableSeeder::class);
        $this->call(EarningsTableSeeder::class);
        $this->call(FaqCategoriesTableSeeder::class);
        $this->call(FaqsTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(OptionGroupsTableSeeder::class);
        $this->call(OptionsTableSeeder::class);
        $this->call(FavoritesTableSeeder::class);
        $this->call(FavoriteOptionsTableSeeder::class);
        $this->call(FieldsTableSeeder::class);
        $this->call(GalleriesTableSeeder::class);
        $this->call(MarketFieldsTableSeeder::class);
        $this->call(MarketReviewsTableSeeder::class);
        $this->call(MarketsPayoutsTableSeeder::class);
        $this->call(MediaTableSeeder::class);
        $this->call(ModelHasPermissionsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(ModelHasRolesTableSeeder::class);
        $this->call(NotificationsTableSeeder::class);
        $this->call(OrdersTableSeeder::class);
        $this->call(OrderStatusesTableSeeder::class);
        $this->call(PasswordResetsTableSeeder::class);
        $this->call(PaymentsTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(ProductOrderOptionsTableSeeder::class);
        $this->call(ProductOrdersTableSeeder::class);
        $this->call(ProductReviewsTableSeeder::class);
        $this->call(RefreshPermissionsSeeder::class);
        $this->call(RequestedMarketsPermission::class);
        $this->call(RoleHasPermissionsTableSeeder::class);
        $this->call(SlidesSeeder::class);
        $this->call(UploadsTableSeeder::class);
        $this->call(UserMarketsTableSeeder::class);
    }
}
