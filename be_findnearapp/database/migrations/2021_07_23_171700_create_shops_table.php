<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShopsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vnp_shops', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255);
            $table->integer('user_id')->unsigned();
            $table->decimal('vote', 2, 1)->unsigned();
            $table->string('address', 255)->nullable();
            $table->integer('city')->nullable();
            $table->integer('district')->nullable();
            $table->integer('ward')->nullable();
            $table->string('street', 255)->nullable();
            $table->string('latitude',20)->nullable();
            $table->string('longitude',20)->nullable();
            $table->integer('category_id')->unsigned()->default(0);
            $table->integer('field_id')->unsigned()->default(0);
            $table->string('description', 500)->nullable();
            $table->boolean('recommended')->default(0);
            $table->enum('status', ['active','inactive'])->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vnp_shops');
    }
}
