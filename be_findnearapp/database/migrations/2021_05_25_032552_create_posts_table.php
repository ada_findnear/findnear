<?php

use App\Models\Post;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id');

            $table->text('content')->nullable();
            // TODO: modify default value after make review post feature
            $table->enum('status', [Post::REJECT_STATUS, Post::PENDING_STATUS, Post::APPROVE_STATUS])
                ->default(Post::APPROVE_STATUS)
                ->comment('0: reject, 1: pending, 2: approve');
            $table->enum('viewer', [Post::PRIVATE_VIEW, Post::ONLY_FRIENDS_VIEW, Post::PUBLIC_VIEW])
                ->default(Post::PUBLIC_VIEW)
                ->comment('0: private, 1: only friends, 2: public');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
