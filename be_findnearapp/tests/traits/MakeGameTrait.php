<?php

use Faker\Factory as Faker;
use App\Models\Game;
use App\Repositories\GameRepository;

trait MakeGameTrait
{
    /**
     * Create fake instance of Game and save it in database
     *
     * @param array $gameFields
     * @return Game
     */
    public function makeGame($gameFields = [])
    {
        /** @var GameRepository $gameRepo */
        $gameRepo = App::make(GameRepository::class);
        $theme = $this->fakeGameData($gameFields);
        return $gameRepo->create($theme);
    }

    /**
     * Get fake instance of Game
     *
     * @param array $gameFields
     * @return Game
     */
    public function fakeGame($gameFields = [])
    {
        return new Game($this->fakeGameData($gameFields));
    }

    /**
     * Get fake data of Game
     *
     * @param array $postFields
     * @return array
     */
    public function fakeGameData($gameFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'description' => $fake->word,
            'total_player' => $fake->word,
            'spent_points' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $gameFields);
    }
}
