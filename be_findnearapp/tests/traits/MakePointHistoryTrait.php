<?php

use Faker\Factory as Faker;
use App\Models\PointHistory;
use App\Repositories\PointHistoryRepository;

trait MakePointHistoryTrait
{
    /**
     * Create fake instance of PointHistory and save it in database
     *
     * @param array $pointHistoryFields
     * @return PointHistory
     */
    public function makePointHistory($pointHistoryFields = [])
    {
        /** @var PointHistoryRepository $pointHistoryRepo */
        $pointHistoryRepo = App::make(PointHistoryRepository::class);
        $theme = $this->fakePointHistoryData($pointHistoryFields);
        return $pointHistoryRepo->create($theme);
    }

    /**
     * Get fake instance of PointHistory
     *
     * @param array $pointHistoryFields
     * @return PointHistory
     */
    public function fakePointHistory($pointHistoryFields = [])
    {
        return new PointHistory($this->fakePointHistoryData($pointHistoryFields));
    }

    /**
     * Get fake data of PointHistory
     *
     * @param array $postFields
     * @return array
     */
    public function fakePointHistoryData($pointHistoryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'user_id' => $fake->word,
            'game_id' => $fake->randomDigitNotNull,
            'point' => $fake->randomDigitNotNull,
            'description' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $pointHistoryFields);
    }
}
