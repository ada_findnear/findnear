<?php

use Faker\Factory as Faker;
use App\Models\VnpCategory;
use App\Repositories\VnpCategoryRepository;

trait MakeVnpCategoryTrait
{
    /**
     * Create fake instance of VnpCategory and save it in database
     *
     * @param array $vnpCategoryFields
     * @return VnpCategory
     */
    public function makeVnpCategory($vnpCategoryFields = [])
    {
        /** @var VnpCategoryRepository $vnpCategoryRepo */
        $vnpCategoryRepo = App::make(VnpCategoryRepository::class);
        $theme = $this->fakeVnpCategoryData($vnpCategoryFields);
        return $vnpCategoryRepo->create($theme);
    }

    /**
     * Get fake instance of VnpCategory
     *
     * @param array $vnpCategoryFields
     * @return VnpCategory
     */
    public function fakeVnpCategory($vnpCategoryFields = [])
    {
        return new VnpCategory($this->fakeVnpCategoryData($vnpCategoryFields));
    }

    /**
     * Get fake data of VnpCategory
     *
     * @param array $postFields
     * @return array
     */
    public function fakeVnpCategoryData($vnpCategoryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title' => $fake->word,
            'parent_id' => $fake->randomDigitNotNull,
            'description' => $fake->word,
            'status' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $vnpCategoryFields);
    }
}
