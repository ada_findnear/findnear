<?php

use Faker\Factory as Faker;
use App\Models\VnpProduct;
use App\Repositories\VnpProductRepository;

trait MakeVnpProductTrait
{
    /**
     * Create fake instance of VnpProduct and save it in database
     *
     * @param array $vnpProductFields
     * @return VnpProduct
     */
    public function makeVnpProduct($vnpProductFields = [])
    {
        /** @var VnpProductRepository $vnpProductRepo */
        $vnpProductRepo = App::make(VnpProductRepository::class);
        $theme = $this->fakeVnpProductData($vnpProductFields);
        return $vnpProductRepo->create($theme);
    }

    /**
     * Get fake instance of VnpProduct
     *
     * @param array $vnpProductFields
     * @return VnpProduct
     */
    public function fakeVnpProduct($vnpProductFields = [])
    {
        return new VnpProduct($this->fakeVnpProductData($vnpProductFields));
    }

    /**
     * Get fake data of VnpProduct
     *
     * @param array $postFields
     * @return array
     */
    public function fakeVnpProductData($vnpProductFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'user_id' => $fake->randomDigitNotNull,
            'shop_id' => $fake->randomDigitNotNull,
            'category_id' => $fake->randomDigitNotNull,
            'price' => $fake->word,
            'status' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $vnpProductFields);
    }
}
