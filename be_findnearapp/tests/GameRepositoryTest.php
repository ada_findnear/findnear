<?php

use App\Models\Game;
use App\Repositories\GameRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GameRepositoryTest extends TestCase
{
    use MakeGameTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var GameRepository
     */
    protected $gameRepo;

    public function setUp()
    {
        parent::setUp();
        $this->gameRepo = App::make(GameRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateGame()
    {
        $game = $this->fakeGameData();
        $createdGame = $this->gameRepo->create($game);
        $createdGame = $createdGame->toArray();
        $this->assertArrayHasKey('id', $createdGame);
        $this->assertNotNull($createdGame['id'], 'Created Game must have id specified');
        $this->assertNotNull(Game::find($createdGame['id']), 'Game with given id must be in DB');
        $this->assertModelData($game, $createdGame);
    }

    /**
     * @test read
     */
    public function testReadGame()
    {
        $game = $this->makeGame();
        $dbGame = $this->gameRepo->find($game->id);
        $dbGame = $dbGame->toArray();
        $this->assertModelData($game->toArray(), $dbGame);
    }

    /**
     * @test update
     */
    public function testUpdateGame()
    {
        $game = $this->makeGame();
        $fakeGame = $this->fakeGameData();
        $updatedGame = $this->gameRepo->update($fakeGame, $game->id);
        $this->assertModelData($fakeGame, $updatedGame->toArray());
        $dbGame = $this->gameRepo->find($game->id);
        $this->assertModelData($fakeGame, $dbGame->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteGame()
    {
        $game = $this->makeGame();
        $resp = $this->gameRepo->delete($game->id);
        $this->assertTrue($resp);
        $this->assertNull(Game::find($game->id), 'Game should not exist in DB');
    }
}
