<?php

use App\Models\VnpCategory;
use App\Repositories\VnpCategoryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class VnpCategoryRepositoryTest extends TestCase
{
    use MakeVnpCategoryTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var VnpCategoryRepository
     */
    protected $vnpCategoryRepo;

    public function setUp()
    {
        parent::setUp();
        $this->vnpCategoryRepo = App::make(VnpCategoryRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateVnpCategory()
    {
        $vnpCategory = $this->fakeVnpCategoryData();
        $createdVnpCategory = $this->vnpCategoryRepo->create($vnpCategory);
        $createdVnpCategory = $createdVnpCategory->toArray();
        $this->assertArrayHasKey('id', $createdVnpCategory);
        $this->assertNotNull($createdVnpCategory['id'], 'Created VnpCategory must have id specified');
        $this->assertNotNull(VnpCategory::find($createdVnpCategory['id']), 'VnpCategory with given id must be in DB');
        $this->assertModelData($vnpCategory, $createdVnpCategory);
    }

    /**
     * @test read
     */
    public function testReadVnpCategory()
    {
        $vnpCategory = $this->makeVnpCategory();
        $dbVnpCategory = $this->vnpCategoryRepo->find($vnpCategory->id);
        $dbVnpCategory = $dbVnpCategory->toArray();
        $this->assertModelData($vnpCategory->toArray(), $dbVnpCategory);
    }

    /**
     * @test update
     */
    public function testUpdateVnpCategory()
    {
        $vnpCategory = $this->makeVnpCategory();
        $fakeVnpCategory = $this->fakeVnpCategoryData();
        $updatedVnpCategory = $this->vnpCategoryRepo->update($fakeVnpCategory, $vnpCategory->id);
        $this->assertModelData($fakeVnpCategory, $updatedVnpCategory->toArray());
        $dbVnpCategory = $this->vnpCategoryRepo->find($vnpCategory->id);
        $this->assertModelData($fakeVnpCategory, $dbVnpCategory->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteVnpCategory()
    {
        $vnpCategory = $this->makeVnpCategory();
        $resp = $this->vnpCategoryRepo->delete($vnpCategory->id);
        $this->assertTrue($resp);
        $this->assertNull(VnpCategory::find($vnpCategory->id), 'VnpCategory should not exist in DB');
    }
}
