<?php

use App\Models\VnpProduct;
use App\Repositories\VnpProductRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class VnpProductRepositoryTest extends TestCase
{
    use MakeVnpProductTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var VnpProductRepository
     */
    protected $vnpProductRepo;

    public function setUp()
    {
        parent::setUp();
        $this->vnpProductRepo = App::make(VnpProductRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateVnpProduct()
    {
        $vnpProduct = $this->fakeVnpProductData();
        $createdVnpProduct = $this->vnpProductRepo->create($vnpProduct);
        $createdVnpProduct = $createdVnpProduct->toArray();
        $this->assertArrayHasKey('id', $createdVnpProduct);
        $this->assertNotNull($createdVnpProduct['id'], 'Created VnpProduct must have id specified');
        $this->assertNotNull(VnpProduct::find($createdVnpProduct['id']), 'VnpProduct with given id must be in DB');
        $this->assertModelData($vnpProduct, $createdVnpProduct);
    }

    /**
     * @test read
     */
    public function testReadVnpProduct()
    {
        $vnpProduct = $this->makeVnpProduct();
        $dbVnpProduct = $this->vnpProductRepo->find($vnpProduct->id);
        $dbVnpProduct = $dbVnpProduct->toArray();
        $this->assertModelData($vnpProduct->toArray(), $dbVnpProduct);
    }

    /**
     * @test update
     */
    public function testUpdateVnpProduct()
    {
        $vnpProduct = $this->makeVnpProduct();
        $fakeVnpProduct = $this->fakeVnpProductData();
        $updatedVnpProduct = $this->vnpProductRepo->update($fakeVnpProduct, $vnpProduct->id);
        $this->assertModelData($fakeVnpProduct, $updatedVnpProduct->toArray());
        $dbVnpProduct = $this->vnpProductRepo->find($vnpProduct->id);
        $this->assertModelData($fakeVnpProduct, $dbVnpProduct->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteVnpProduct()
    {
        $vnpProduct = $this->makeVnpProduct();
        $resp = $this->vnpProductRepo->delete($vnpProduct->id);
        $this->assertTrue($resp);
        $this->assertNull(VnpProduct::find($vnpProduct->id), 'VnpProduct should not exist in DB');
    }
}
