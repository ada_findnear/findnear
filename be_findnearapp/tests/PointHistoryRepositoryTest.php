<?php

use App\Models\PointHistory;
use App\Repositories\PointHistoryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PointHistoryRepositoryTest extends TestCase
{
    use MakePointHistoryTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PointHistoryRepository
     */
    protected $pointHistoryRepo;

    public function setUp()
    {
        parent::setUp();
        $this->pointHistoryRepo = App::make(PointHistoryRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePointHistory()
    {
        $pointHistory = $this->fakePointHistoryData();
        $createdPointHistory = $this->pointHistoryRepo->create($pointHistory);
        $createdPointHistory = $createdPointHistory->toArray();
        $this->assertArrayHasKey('id', $createdPointHistory);
        $this->assertNotNull($createdPointHistory['id'], 'Created PointHistory must have id specified');
        $this->assertNotNull(PointHistory::find($createdPointHistory['id']), 'PointHistory with given id must be in DB');
        $this->assertModelData($pointHistory, $createdPointHistory);
    }

    /**
     * @test read
     */
    public function testReadPointHistory()
    {
        $pointHistory = $this->makePointHistory();
        $dbPointHistory = $this->pointHistoryRepo->find($pointHistory->id);
        $dbPointHistory = $dbPointHistory->toArray();
        $this->assertModelData($pointHistory->toArray(), $dbPointHistory);
    }

    /**
     * @test update
     */
    public function testUpdatePointHistory()
    {
        $pointHistory = $this->makePointHistory();
        $fakePointHistory = $this->fakePointHistoryData();
        $updatedPointHistory = $this->pointHistoryRepo->update($fakePointHistory, $pointHistory->id);
        $this->assertModelData($fakePointHistory, $updatedPointHistory->toArray());
        $dbPointHistory = $this->pointHistoryRepo->find($pointHistory->id);
        $this->assertModelData($fakePointHistory, $dbPointHistory->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePointHistory()
    {
        $pointHistory = $this->makePointHistory();
        $resp = $this->pointHistoryRepo->delete($pointHistory->id);
        $this->assertTrue($resp);
        $this->assertNull(PointHistory::find($pointHistory->id), 'PointHistory should not exist in DB');
    }
}
